package com.unomic.dulink.chart.service;

import java.util.List;

import com.unomic.dulink.chart.domain.ChartVo;
import com.unomic.dulink.chart.domain.TimeChartVo;
import com.unomic.dulink.svg.domain.SVGVo;

public interface ChartService {
	public String getStatusData(ChartVo chartVo) throws Exception;
	public void mobile_token_update(String token) throws Exception;
	
	public void pushAlarmToMobile() throws Exception;
	
	public ChartVo getCurrentDvcData(ChartVo chartVo) throws Exception;

	public void deleteUser(String models) throws Exception;
	
	public String updateUser(String models) throws Exception;
	
	public void addData() throws Exception;

	public String getDvcName(ChartVo chartVo) throws Exception;

	public String getAllDeliverer(ChartVo chartVo) throws Exception;

	public String updateHistory(String val) throws Exception;

	public String getUserList(String shopId) throws Exception;
	
	public String createUser(String models) throws Exception;
	
	
	public String addDeliveryDate(ChartVo chartVo) throws Exception;

	public String addDeliveryHst(String val) throws Exception;

	public String addPrdComplHist(String val) throws Exception;

	public String updateMachnieWorker(String val) throws Exception;

	public String addNonOpHst(String val) throws Exception;

	public String setCirclePos(ChartVo chartVo) throws Exception;

	public String delWorker(ChartVo chartVo) throws Exception;

	public String getmstmat(ChartVo chartVo) throws Exception;

	public void mstmatDelete(String delname) throws Exception;

	public String mstmatUpdate(String val) throws Exception;

	public String mstmatInsert(ChartVo chartVo, int length, String val, int maxlength) throws Exception;

	public String mstmatCount(ChartVo chartVo, String length) throws Exception;

	public String delAttendanceHistory(String id) throws Exception;

	public ChartVo getPrcNSpec(ChartVo chartVo) throws Exception;

	public String updatRouting(String val) throws Exception;

	public String traceManagerUpdate(String val) throws Exception;

	public void traceManagerDelete(String delname) throws Exception;

	public String traceManagerCount(ChartVo chartVo, String length) throws Exception;

	public String traceManagerInsert(ChartVo chartVo, int length, String val, int maxlength) throws Exception;

	public String addRouting(String val) throws Exception;

	public String delRouting(ChartVo chartVo) throws Exception;

	public String addAttendanceHst(String val) throws Exception;

	public String delNonOpHistory(ChartVo chartVo) throws Exception;

	public String getAllDvcStatus(ChartVo chartVo) throws Exception;

	public String getMachineInfo(SVGVo svgVo) throws Exception;

	public String getOprNo4Routing(ChartVo chartVo) throws Exception;

	public String getDvc(ChartVo chartVo) throws Exception;

	public String getRoutingInfo(ChartVo chartVo) throws Exception;

	public String getCdGroup1(ChartVo chartVo) throws Exception;

	public String getCdGroup2(ChartVo chartVo) throws Exception;

	public String updateNonOpHist(ChartVo chartVo) throws Exception;

	public String updateAttendance(ChartVo chartVo) throws Exception;

	public String getCdGroup3(ChartVo chartVo) throws Exception;

	public String getAttendanceHist(ChartVo chartVo) throws Exception;

	public String getAllDvcId(ChartVo chartVo) throws Exception;

	public ChartVo getCurrentProg(ChartVo chartVo) throws Exception;

	public String delDeliverHistory(ChartVo chartVo) throws Exception;

	public ChartVo getNonOpData(ChartVo chartVo) throws Exception;

	public void resetTool(ChartVo chartVo) throws Exception;

	public String setCalcTy(ChartVo chartVo) throws Exception;

	public String getComName(ChartVo chartVo) throws Exception;

	public String getProgInfo(ChartVo chartVo) throws Exception;

	public String getDeliverInfo(ChartVo chartVo) throws Exception;

	public String getProgCd(ChartVo chartVo) throws Exception;

	public String getStockInfoByTable(ChartVo chartVo) throws Exception;

	public String getStockInfoByTableLine(ChartVo chartVo) throws Exception;

	public String transferOprCnt(String val) throws Exception;

	public String getDeliveryHistory(ChartVo chartVo) throws Exception;

	public String getDeliveryNo(ChartVo chartVo) throws Exception;

	public String getReOpList(ChartVo chartVo) throws Exception;

	public String getToolInfo(ChartVo chartVo) throws Exception;

	public String getDeliverer(ChartVo chartVo) throws Exception;

	public String getJSGroupCode(ChartVo chartVo) throws Exception;

	public String getOprNoList(ChartVo chartVo) throws Exception;

	public String getOutLotTracer(ChartVo chartVo) throws Exception;

	public String chkLogin(ChartVo chartVo) throws Exception;

	public String getLotNo(ChartVo chartVo) throws Exception;

	public String getShipLotNo(ChartVo chartVo) throws Exception;

	public String getChkStandardList(ChartVo chartVo) throws Exception;

	public String addPrdData(String val) throws Exception;

	public String showPopup(ChartVo chartVo) throws Exception;

	public String delChkStandard(ChartVo chartVo) throws Exception;

	public String getLotTracer(ChartVo chartVo) throws Exception;

	public String getPrdData(ChartVo chartVo) throws Exception;

	public String getOprNo(ChartVo chartVo) throws Exception;

	public String getReleaseInfoHist(ChartVo chartVo) throws Exception;

	public String saveRow(String val) throws Exception;

	public String faultUpdate(String val) throws Exception;

	public String addCheckStandard(String val) throws Exception;

	public String addCheckStandardList(String val) throws Exception;

	public String getCheckList(ChartVo chartVo) throws Exception;

	public String getOprNm(ChartVo chartVo) throws Exception;

	public String setPrgSet(String val) throws Exception;

	public String getDvcNameList(ChartVo chartVo) throws Exception;

	public String saveUpdatedStock(String val) throws Exception;

	public String setSpdLoad(ChartVo chartVo) throws Exception;

	public String getOprNmList(ChartVo chartVo) throws Exception;

	public ChartVo getSpdLoadInfo(ChartVo chartVo) throws Exception;

	public ChartVo getCalcTy(ChartVo chartVo) throws Exception;

	public String getLotNoByPrdNo(ChartVo chartVo) throws Exception;

	public String updateNonOp(ChartVo chartVo) throws Exception;

	public String updateAttendance(String val) throws Exception;

	public String addWorkTimeHist(ChartVo chartVo) throws Exception;

	public String addWorkEndTimeHist(ChartVo chartVo) throws Exception;

	public String setPrdCmplData(ChartVo chartVo) throws Exception;

	public String getOprNameList(ChartVo chartVo) throws Exception;

	public String getCheckType(ChartVo chartVo) throws Exception;

	public String okDelNonOp(ChartVo chartVo) throws Exception;

	public String getPrdNo(ChartVo chartVo) throws Exception;

	public String getLotInfo(ChartVo chartVo) throws Exception;

	public String getNonOpTy(ChartVo chartVo) throws Exception;

	public String getDvcListByPrdNo(ChartVo chartVo) throws Exception;

	public String getDvcIdForSign(ChartVo chartVo) throws Exception;

	public String addNonOp(ChartVo chartVo) throws Exception;

	public String getMatInfo(ChartVo chartVo) throws Exception;

	public String getRcvInfo(ChartVo chartVo) throws Exception;

	public String getSLCR(ChartVo chartVo) throws Exception;

	public String getAdtList(ChartVo chartVo) throws Exception;

	public String getNonOpInfo(ChartVo chartVo) throws Exception;

	public String addStock(String val) throws Exception;

	public String okDelStock(ChartVo chartVo) throws Exception;

	public String updateStock(ChartVo chartVo) throws Exception;

	public ChartVo getStockInfo(ChartVo chartVo) throws Exception;

	public ChartVo getNewMatInfo(ChartVo chartVo) throws Exception;

	public String getAllDvc(ChartVo chartVo) throws Exception;

	public String okDel(ChartVo chartVo) throws Exception;

	public String addMachine(ChartVo chartVo) throws Exception;

	public String getMachine(ChartVo chartVo) throws Exception;

	public String updateBGImg(ChartVo chartVo) throws Exception;

	public void setMachinePos_edit(ChartVo chartVo) throws Exception;

	public String delMachine(ChartVo chartVo) throws Exception;

	public String getBGImg(ChartVo chartVo) throws Exception;

	public String getPrgInfo(ChartVo chartVo) throws Exception;

	public String getCnt(ChartVo chartVo) throws Exception;

	public String getDevieList(ChartVo chartVo) throws Exception;

	public String addFaulty(ChartVo chartVo) throws Exception;

	public String getBarChartDvcId(ChartVo chartVo) throws Exception;

	public String getChecker(ChartVo chartVo) throws Exception;

	public String updateCd(ChartVo chartVo) throws Exception;

	public String getFaultList(ChartVo chartVo) throws Exception;

	public String getLeadTime(ChartVo chartVo) throws Exception;

	public String getFaultyList(ChartVo chartVo) throws Exception;

	public String getCheckTyList(ChartVo chartVo) throws Exception;

	public String getGroup(ChartVo chartVo) throws Exception;

	public String getTemplate(ChartVo ChartVo) throws Exception;

	public ChartVo getBanner(ChartVo chartVo) throws Exception;

	public String getPrgCdList(ChartVo chartVo) throws Exception;

	public String getPerformanceAgainstGoal(ChartVo chartVo) throws Exception;

	public String polling1() throws Exception;

	public void addBanner(ChartVo chartVo) throws Exception;

	public void cancelBanner(ChartVo chartVo) throws Exception;

	public String getTargetData(ChartVo chartVo) throws Exception;

	public void setLimit(ChartVo chartVo) throws Exception;

	public String setVideo(String id) throws Exception;

	public String getTraceHist(ChartVo chartVo) throws Exception;

	public String getDvcId(ChartVo chartVo) throws Exception;

	public String getFacilitiesStatus(ChartVo chartVo) throws Exception;

	public String getToolList(ChartVo chartVo) throws Exception;

	public void initVideoPolling() throws Exception;

	public void initPiePolling() throws Exception;

	public ChartVo getData(ChartVo chartVo) throws Exception;

	public String piePolling1() throws Exception;

	public String getTimeData(ChartVo chartVo) throws Exception;

	public String getMachineName(ChartVo chartVo) throws Exception;

	public String piePolling2() throws Exception;

	public String setChart1(String id) throws Exception;

	public String setChart2(String id) throws Exception;

	public String getRepairList(ChartVo chartVo) throws Exception;

	public String getStartTime(ChartVo chartVo) throws Exception;

	public void delVideoMachine(String id) throws Exception;

	public void delChartMachine(String id) throws Exception;

	public String getAdapterId(ChartVo chartVo) throws Exception;

	public String getReleaseInfo(ChartVo chartVo) throws Exception;

	public ChartVo getWorkerInfo(ChartVo chartVo) throws Exception;

	public String addPrdCmpl(ChartVo chartVo) throws Exception;

	public String getDetailBlockData(ChartVo chartVo) throws Exception;

	public String addCmplStockHistory(String val) throws Exception;

	public String getStockStatus(ChartVo chartVo) throws Exception;

	public String getLotInfoHistory(ChartVo chartVo) throws Exception;

	public String showDetailEvent(ChartVo chartVo) throws Exception;

	public String getNextWorkerId(ChartVo chartVo) throws Exception;

	public String addWorker(ChartVo chartVo) throws Exception;

	public String searchWorker(ChartVo chartVo) throws Exception;

	public String getComList(ChartVo chartVo) throws Exception;

	public String login_worker(ChartVo chartVo) throws Exception;

	public String getNonOpHist(ChartVo chartVo) throws Exception;

	public void test(ChartVo chartVo) throws Exception;

	public String getDetailStatus(ChartVo chartVo) throws Exception;

	public String getAlarmList(ChartVo chartVo) throws Exception;

	public List<TimeChartVo> getTimeChartData(ChartVo chartVo);

	public ChartVo getOtherChartsData(ChartVo chartVo) throws Exception;

	public List<ChartVo> testTimeChartData(ChartVo inputVo);

	public String getJicList(ChartVo chartVo) throws Exception;

	public String getWcList(ChartVo chartVo) throws Exception;

	public String getTableData(ChartVo chartVo) throws Exception;

	public String getWcData(ChartVo chartVo) throws Exception;

	public String getWcDataByDvc(ChartVo chartVo) throws Exception;

	public String getDvcList(String shopId) throws Exception;

	public String setLampExpression(ChartVo chartVo) throws Exception;
	
	public String getStatusExpression(String dvcId) throws Exception;
	
	public String getLampExpression() throws Exception;
	
	public String getComStartTime(String shopId) throws Exception;
	
	public String getUtc(ChartVo chartVo) throws Exception;
	
	public String getDivision(String shopId) throws Exception;
	
	public String setUtc(ChartVo chartVo) throws Exception;
	
	public String getAlarmData(ChartVo chartVo) throws Exception;

	public String getJigList4Report(ChartVo chartVo) throws Exception;

	public String getTodayWorkByName(ChartVo chartVo) throws Exception;

	public String getWorkerWatingTime(ChartVo chartVo) throws Exception;

	public String getMaintenanceReport(ChartVo chartVo) throws Exception;

	public void setMaintenanceReport(ChartVo chartVo) throws Exception;

	public String getMaintenanceReportList(int empCd) throws Exception;

	public String getMaintenanceReportListdvcId(ChartVo chartVo) throws Exception;

	public void delMaintenanceReport(ChartVo chartVo) throws Exception;

	public String cancelInvoice(String val) throws Exception;

	public String getAttendant_List(ChartVo chartVo) throws Exception;

	public String upDateWorkTimeHist(ChartVo chartVo) throws Exception;

	public String addWorkTimeHist_Correct(ChartVo chartVo) throws Exception;

	public String addNonOpHst_One(String val) throws Exception;

	public String getmstmatno(String rWMATNO) throws Exception;

	public String getbertmstmatno(String rWMATNO) throws Exception;

	public String getmstitmprd(String rWMATNO) throws Exception;

	public String barcodebulchul(ChartVo chartVo) throws Exception;

	public String addDeliveryDate(String val)throws Exception;

	public String insertWaitTime(String val, String date) throws Exception;

	public String getNonopHistory(String idx) throws Exception;

	public String approveAttendence(String id) throws Exception;

	public String takeAwayApprove(String id, String msg) throws Exception;

	public String cancelApprove(String id) throws Exception;
	
	public String getAlarmAction(String val) throws Exception;
	
	public String getReCount(ChartVo chartVo) throws Exception;

	public String alarmExcept(String val) throws Exception;

	public String alarmreturn(ChartVo chartVo) throws Exception;
	
	public String getOuterStock(ChartVo chartVo) throws Exception;
	
	public String MaintenanceSave(String val) throws Exception;
	
	public String selectMenu() throws Exception;

	public String getworkingList() throws Exception;

	public String getTransList(ChartVo chartVo) throws Exception;

	public String getExportList(ChartVo chartVo) throws Exception;

	public String getImportList(ChartVo chartVo) throws Exception;

	public String getShipList(ChartVo chartVo) throws Exception;

	public String getStockList(ChartVo chartVo) throws Exception;

	public String getItem(ChartVo chartVo) throws Exception;

	public String stockTotalCntCheck(ChartVo chartVo) throws Exception;

	public String getInhistory(ChartVo chartVo) throws Exception;
	
	public String getToolLifeList(ChartVo chartVo) throws Exception;

	public String toolResetSave(ChartVo chartVo,String val) throws Exception;

	public String toolLimitSave(String val) throws Exception;

	public String checkPrdctStandardSave(String val) throws Exception;

	public String getSelectList(ChartVo chartVo) throws Exception;

	public String processCnt(ChartVo chartVo) throws Exception;
	
	public String getTattooList(ChartVo chartVo) throws Exception;

	public String saveTattoo(String val) throws Exception;
	
	public String getMachineInformation(ChartVo chartVo) throws Exception;

	public String dayFaulty(ChartVo chartVo) throws Exception;

	public String saveDvcMove(String val) throws Exception;

	public String getInstrument(ChartVo chartVo) throws Exception;

	public String saveInstrument(ChartVo chartVo) throws Exception;

	public String saveImgPath(ChartVo chartVo) throws Exception;

	public String detailInstrument(ChartVo chartVo) throws Exception;

	public String detailInstrumentSave(String val) throws Exception;

	public String getSelectItemList(ChartVo chartVo) throws Exception;
	
	public String saveSelectItem(String val) throws Exception;

	public String getRecordItemList(ChartVo chartVo) throws Exception;

	public String deleteManagementItem(ChartVo chartVo) throws Exception;

	public String deleteRecordItem(ChartVo chartVo) throws Exception;

	public String saveImgPathDetail(ChartVo chartVo) throws Exception;

	public String getInlist(ChartVo chartVo) throws Exception;

	public String getOutlist(ChartVo chartVo) throws Exception;
	
	public String getRelist(ChartVo chartVo) throws Exception;
	
	public String login(ChartVo chartVo) throws Exception;
};
