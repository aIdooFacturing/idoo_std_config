package com.unomic.dulink.chart.controller;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.krysalis.barcode4j.impl.code128.Code128Bean;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;
import org.krysalis.barcode4j.tools.UnitConv;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.unomic.dulink.chart.domain.BarChartVo;
import com.unomic.dulink.chart.domain.ChartVo;
import com.unomic.dulink.chart.domain.DataVo;
import com.unomic.dulink.chart.domain.TimeChartVo;
import com.unomic.dulink.chart.service.ChartService;
import com.unomic.dulink.common.domain.CommonFunction;
import com.unomic.dulink.svg.domain.SVGVo;

/**
 * Handles requests for the application home page.
 */
/**
 * Handles requests for the application home page.
 */
@RequestMapping(value = "/chart")
@Controller
public class ChartController {

	private static final Logger logger = LoggerFactory.getLogger(ChartController.class);
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@Autowired
	private ChartService chartService;

	
	
	@RequestMapping(value = "createUser", produces = "text/html; charset=UTF8")
	@ResponseBody
	//public Object createUser(String models) {
	public String createUser(String models) {
		try {
			models = chartService.createUser(models);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/html; charset=utf-8");
		//return new ResponseEntity(models, responseHeaders, HttpStatus.CREATED);
		return "";
	}

	

	
	@RequestMapping(value = "deleteUser")
	@ResponseBody
	public void deleteUser(String models) {
		try {
			chartService.deleteUser(models);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "updateUser")
	@ResponseBody
	public String updateUser(String models) {
		try {
			models = chartService.updateUser(models);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return models;
	}

	//@RequestMapping(value = "getUserList", produces = "text/html; charset=UTF8")
	@RequestMapping(value = "getUserList")
	@ResponseBody
	public Object getUserList(String shopId) {
		String str = "";

		try {
			str = chartService.getUserList(shopId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/html; charset=utf-8");
	//	return new ResponseEntity(str, responseHeaders, HttpStatus.CREATED);
		 return str;

	};
	
	@RequestMapping(value = "jstlPage")
	public String jstlPage() {
		return "jstlTest";
	}
	

	@RequestMapping(value = "status-Setting")
	public String statusSetting() {
		return "chart/status-Setting";
	}
	
	
	
	@RequestMapping(value = "account_Setting")
	public String account_Setting() {
		return "chart/account_Setting";
	};
	
	@RequestMapping(value = "mobileInventory")
	public String mobileInventory() {
		return "chart/mobileInventory";
	};
	
	@ResponseBody
	@RequestMapping(value = "getInlist")
	public String getInlist(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getInlist(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}; 
	
	@ResponseBody
	@RequestMapping(value = "getOutlist")
	public String getOutlist(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getOutlist(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@ResponseBody
	@RequestMapping(value = "getRelist")
	public String getRelist(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getRelist(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	
	
	@RequestMapping(value = "mobileInMove")
	public String mobileInMove() {
		return "chart/mobileInMove";
	};
	@RequestMapping(value = "mobileOutMove")
	public String mobileOutMove() {
		return "chart/mobileOutMove";
	};
	@RequestMapping(value = "mobileReInMove")
	public String mobileReInMove() {
		return "chart/mobileReInMove";
	};
	
	
	@RequestMapping(value="chkIp")
	@ResponseBody 
	public String chkIp(String ip){
		String rtn = "" ;
		
		URL whatismyip;
		String server_ip = "";
		try {
			whatismyip = new URL("http://checkip.amazonaws.com");
			
			BufferedReader in = new BufferedReader(new InputStreamReader(
	                whatismyip.openStream()));
			//이게 톰캣 설치된 서버 ip
			server_ip = in.readLine(); //you get the IP as a String
			

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		//logger.debug();
		//server_ip : 톰캣 설치된 서버 ip
		//ip : 사이트 요청하는 클라이언트 ip
		//두개를 비교해서 true 이면 로그인창 안띄움
		if(server_ip.equals(ip)) {
			rtn = "true";
		}else if(ip.equals("106.240.234.116") || ip.equals("112.221.229.27") || ip.equals("125.143.194.117")){
			//클라이언트 ip가 116 이랑 rck2,5 ip이면 로그인창 띄우지마라
			rtn = "true";
		}else {
			rtn = "false";
		}
		
		return rtn;
	}
	
	@RequestMapping(value = "getDvc1")
	@ResponseBody
	public ModelAndView getDvc1(ChartVo chartVo,Model model) {
		ModelAndView view= new ModelAndView();
		view.addObject("name1","TESTNAME1");
		view.addObject("name", "TESTNAME2");
		view.addObject("name", "TESTNAME3");
		return view;
	};

	@RequestMapping(value = "transferLine")
	public String transfereLine() {
		return "chart/transferLine";
	};

	@RequestMapping(value = "ZindexT")
	public String ZindexT() {
		return "test/ZindexT";
	};
	
	@RequestMapping(value = "push_test")
	@ResponseBody
	public void push_test() {
		try {
			chartService.pushAlarmToMobile();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	@RequestMapping(value = "delivery")
	public String delivery() {
		return "chart/delivery";
	}
	
	@RequestMapping(value = "mobile_token_update")
	@ResponseBody
	public void mobile_token_update(String token) {
		try {
			chartService.mobile_token_update(token);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	@RequestMapping(value = "checkPrdct_temp")
	public String checkPrdct_temp() {
		return "chart/checkPrdct_temp";
	}

	@RequestMapping(value = "traceManagerUpdate")
	@ResponseBody
	public String traceManagerUpdate(ChartVo chartVo, String val, String length) {
		String str = "";
		String aa = "";
		try {
			str = chartService.traceManagerUpdate(val);
			aa = chartService.traceManagerCount(chartVo, length);
			int Intlength = Integer.parseInt(length);
			int Intaa = Integer.parseInt(aa);
			if (Intlength > Intaa) {
				chartService.traceManagerInsert(chartVo, Intaa, val, Intlength);
				str = chartService.traceManagerUpdate(val);
			}
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return str;
	}

	@RequestMapping(value = "mstmat")
	public String mstmat() {
		return "chart/mstmat";
	}

	@RequestMapping(value = "getmstmat")
	@ResponseBody
	public String getgetmstmat(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getmstmat(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "mstmatDelete")
	@ResponseBody
	public void mstmatDelete(String delname) {
		try {
			chartService.mstmatDelete(delname);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@RequestMapping(value = "mstmatUpdate")
	@ResponseBody
	public String mstmatUpdate(ChartVo chartVo, String val, String length) {
		String str = "";
		String aa = "";
		try {
			str = chartService.mstmatUpdate(val);
			aa = chartService.mstmatCount(chartVo, length);
			int Intlength = Integer.parseInt(length);
			int Intaa = Integer.parseInt(aa);
			if (Intlength > Intaa) {
				chartService.mstmatInsert(chartVo, Intaa, val, Intlength);
				str = chartService.mstmatUpdate(val);
			}
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return str;
	}

	@RequestMapping(value = "traceManagerDelete")
	@ResponseBody
	public void traceManagerDelete(String delname) {
		try {
			chartService.traceManagerDelete(delname);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@RequestMapping(value = "routingManager")
	public String routingManager() {
		return "chart/routingManager";
	}

	@RequestMapping(value = "setCirclePos")
	@ResponseBody
	public String setCirclePos(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.setCirclePos(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "updatRouting")
	@ResponseBody
	public String updatRouting(String val) {
		String str = "";
		try {
			str = chartService.updatRouting(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "delRouting")
	@ResponseBody
	public String delRouting(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.delRouting(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "delWorker")
	@ResponseBody
	public String delWorker(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.delWorker(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "addRouting")
	@ResponseBody
	public String addRouting(String val) {
		String str = "";
		try {
			str = chartService.addRouting(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "delNonOpHistory")
	@ResponseBody
	public String delNonOpHistory(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.delNonOpHistory(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "delAttendanceHistory")
	@ResponseBody
	public String delAttendanceHistory(String id) {
		String str = "";
		try {
			str = chartService.delAttendanceHistory(id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getNonOpHist")
	@ResponseBody
	public String getNonOpHist(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getNonOpHist(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "login_worker")
	@ResponseBody
	public String login_worker(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.login_worker(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "addDeliveryDate")
	@ResponseBody
	public String addDeliveryDate(String val) {
		String str = "";
		try {
			str = chartService.addDeliveryDate(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "updateNonOpHist")
	@ResponseBody
	public String updateNonOpHist(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.updateNonOpHist(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "updateAttendance")
	@ResponseBody
	public String updateAttendance(String val) {
		String str = "";
		try {
			str = chartService.updateAttendance(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value = "takeAwayApprove")
	@ResponseBody
	public String takeAwayApprove(String id, String msg) {
		String str = "";
		try {
			str = chartService.takeAwayApprove(id, msg);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value = "cancelApprove")
	@ResponseBody
	public String cancelApprove(String id) {
		String str = "";
		try {
			str = chartService.cancelApprove(id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "updateMachnieWorker")
	@ResponseBody
	public String updateMachnieWorker(String val) {
		String str = "";
		try {
			str = chartService.updateMachnieWorker(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "addPrdComplHist")
	@ResponseBody
	public String addPrdComplHist(String val) {
		String str = "";
		try {
			str = chartService.addPrdComplHist(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "addAttendanceHst")
	@ResponseBody
	public String addAttendanceHst(String val) {
		String str = "";
		try {
			str = chartService.addAttendanceHst(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "addNonOpHst")
	@ResponseBody
	public String addNonOpHst(String val) {
		String str = "";
		try {
			str = chartService.addNonOpHst(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "addDeliveryHst")
	@ResponseBody
	public String addDeliveryHst(String val) {
		String str = "";
		try {
			str = chartService.addDeliveryHst(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getCdGroup1")
	@ResponseBody
	public String getCdGroup1(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getCdGroup1(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getCdGroup2")
	@ResponseBody
	public String getCdGroup2(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getCdGroup2(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getCdGroup3")
	@ResponseBody
	public String getCdGroup3(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getCdGroup3(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "addWorkTimeHist")
	@ResponseBody
	public String addWorkTimeHist(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.addWorkTimeHist(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "addWorkEndTimeHist")
	@ResponseBody
	public String addWorkEndTimeHist(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.addWorkEndTimeHist(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getAttendanceHist")
	@ResponseBody
	public String getAttendanceHist(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getAttendanceHist(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value = "approveAttendence")
	@ResponseBody
	public String approveAttendence(String id) {
		String str = "";
		try {
			str = chartService.approveAttendence(id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	// @RequestMapping(value="updateAttendance")
	// @ResponseBody
	// public String updateAttendance(String val){
	// String str = "";
	// try {
	// str = chartService.updateAttendance(val);
	// } catch (Exception e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// return str;
	// }
	//
	// @RequestMapping(value="updateNonOpHist")
	// @ResponseBody
	// public String updateNonOpHist(String val){
	// String str = "";
	// try {
	// str = chartService.updateNonOpHist(val);
	// } catch (Exception e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// return str;
	// }
	//

	@RequestMapping(value = "getAllDeliverer")
	@ResponseBody
	public String getAllDeliverer(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getAllDeliverer(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "updateHistory")
	@ResponseBody
	public String updateHistory(String val) {
		String str = "";
		try {
			str = chartService.updateHistory(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "delDeliverHistory")
	@ResponseBody
	public String delDeliverHistory(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.delDeliverHistory(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getDeliverInfo")
	@ResponseBody
	public String getDeliverInfo(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getDeliverInfo(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getDeliveryNo")
	@ResponseBody
	public String getDeliveryNo(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getDeliveryNo(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "printInvoice")
	public String printInvoice(String json, HttpServletRequest request) {
		request.setAttribute("jsonData", json);
		System.out.println("json ::"+json);
		return "chart/invoice";
	}
	@RequestMapping(value = "printExport")
	public String printExport(String json, HttpServletRequest request) {
		request.setAttribute("jsonData", json);
//		System.out.println("json ::"+json);
		return "chart/export";
	}

	@RequestMapping(value = "createBarcode")
	@ResponseBody
	public String barcode4j(String deliveryCd) throws IOException {
		String str = "";
		String fileName = deliveryCd;
		// Create the barcode bean
		Code128Bean bean = new Code128Bean();

		final int dpi = 600;

		// Configure the barcode generator
		bean.setModuleWidth(UnitConv.in2mm(2.0f / 90)); // makes the narrow bar
														// width exactly one pixel

		bean.setFontSize(3.0);
		// bean.setWideFactor(3);
		bean.doQuietZone(false);

		// File dir = new File("/Users/jeongwan/Desktop");
		File dir = new File(System.getProperty("catalina.base") + "/webapps/barcode");

		if (!dir.exists()) {
			dir.mkdirs();
		}
		;

		File file = new File(dir + "/" + fileName + ".png");

		OutputStream out = new FileOutputStream(file);

		try {
			// Set up the canvas provider for monochrome PNG output
			BitmapCanvasProvider canvas = new BitmapCanvasProvider(out, "image/x-png", dpi,
					BufferedImage.TYPE_BYTE_BINARY, false, 0);

			// Generate the barcode
			bean.generateBarcode(canvas, fileName);

			// Signal end of generation
			canvas.finish();
			str = "success";
		} catch (Exception e) {
			str = "fail";
		} finally {
			out.close();
		}
		return str;
	}

	@RequestMapping(value = "invoice")
	public String invoice() {
		return "chart/invoice";
	}

	@RequestMapping(value = "getDeliveryHistory")
	@ResponseBody
	public String getDeliveryHistory(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getDeliveryHistory(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getDeliverer")
	@ResponseBody
	public String getDeliverer(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getDeliverer(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "transferOprCnt")
	@ResponseBody
	public String transferOprCnt(String val) {
		String str = "";
		try {
			str = chartService.transferOprCnt(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "excel")
	@ResponseBody
	public void excel() {
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("Java Books");

		Object[][] bookData = { { "Head First Java", "Kathy Serria", 79 }, { "Effective Java", "Joshua Bloch", 36 },
				{ "Clean Code", "Robert martin", 42 }, { "Thinking in Java", "Bruce Eckel", 35 }, };

		int rowCount = 0;

		for (Object[] aBook : bookData) {
			Row row = sheet.createRow(++rowCount);

			int columnCount = 0;

			for (Object field : aBook) {
				Cell cell = row.createCell(++columnCount);
				if (field instanceof String) {
					cell.setCellValue((String) field);
				} else if (field instanceof Integer) {
					cell.setCellValue((Integer) field);
				}
			}

		}

		try (FileOutputStream outputStream = new FileOutputStream("/Users/jeongwan/Desktop/JavaBooks.xlsx")) {
			workbook.write(outputStream);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	};

	@RequestMapping(value = "getStockInfoByTable")
	@ResponseBody
	public String getStockInfoByTable(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getStockInfoByTable(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getStockInfoByTableLine")
	@ResponseBody
	public String getStockInfoByTableLine(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getStockInfoByTableLine(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getReleaseInfoHist")
	@ResponseBody
	public String getReleaseInfoHist(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getReleaseInfoHist(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "transferOpr")
	public String transferOpr() {
		return "chart/transferOpr";
	};

	@RequestMapping(value = "transferOpr2")
	public String transferOpr2() {
		return "chart/transferOpr2";
	};

	@RequestMapping(value = "returnStock")
	public String returnStock() {
		return "chart/returnStock";
	};
	
	@RequestMapping(value = "searchWorker")
	@ResponseBody
	public String searchWorker(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.searchWorker(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getReleaseInfo")
	@ResponseBody
	public String getReleaseInfo(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getReleaseInfo(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getLotInfoHistory")
	@ResponseBody
	public String getLotInfoHistory(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getLotInfoHistory(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getStockStatus")
	@ResponseBody
	public String getStockStatus(ChartVo chartVo) {
		String str = "";
		
		try {
			str = chartService.getStockStatus(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "addCmplStockHistory")
	@ResponseBody
	public String addCmplStockHistory(String val) {
		String str = "";
		try {
			str = chartService.addCmplStockHistory(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "addPrdCmplCnt")
	@ResponseBody
	public String addPrdCmplCnt(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.addPrdCmpl(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "addPrdData")
	@ResponseBody
	public String addPrdData(String val) {
		String str = "";
		try {
			str = chartService.addPrdData(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getComList")
	@ResponseBody
	public String getComList(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getComList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getComName")
	@ResponseBody
	public String getComName(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getComName(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getOprNoList")
	@ResponseBody
	public String getOprNoList(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getOprNoList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getOprNo4Routing")
	@ResponseBody
	public String getOprNo4Routing(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getOprNo4Routing(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getOprNo")
	@ResponseBody
	public String getOprNo(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getOprNo(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "showPopup")
	@ResponseBody
	public String showPopup(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.showPopup(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

/*	@RequestMapping(value = "getPrdData")
	@ResponseBody
	public String getPrdData(ChartVo chartVo) {
		String str = "";
		logger.info("=========================================");
		logger.info("Debug Code : " + chartVo.toString());
		logger.info("Debug Code : " + chartVo.getEDate());
		logger.info("=========================================");
		try {
			str = chartService.getPrdData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
*/
	@RequestMapping(value = "setCalcTy")
	@ResponseBody
	public String setCalcTy(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.setCalcTy(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getCheckList")
	@ResponseBody
	public String getCheckList(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getCheckList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "addCheckStandardList")
	@ResponseBody
	public String addCheckStandardList(String val) {
		String str = "";
		try {
			str = chartService.addCheckStandardList(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getPrcNSpec")
	@ResponseBody
	public ChartVo getPrcNSpec(ChartVo chartVo) {
		try {
			chartVo = chartService.getPrcNSpec(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return chartVo;
	}

	@RequestMapping(value = "delChkStandard")
	@ResponseBody
	public String delChkStandard(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.delChkStandard(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getJSGroupCode")
	@ResponseBody
	public String getJSGroupCode(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getJSGroupCode(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getChkStandardList")
	@ResponseBody
	public String getChkStandrdList(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getChkStandardList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "addCheckStandard")
	@ResponseBody
	public String addCheckStandard(String val) {
		String str = "";
		try {
			str = chartService.addCheckStandard(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "saveRow")
	@ResponseBody
	public String saveRow(String val) {
		String str = "";
		try {
			str = chartService.saveRow(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "faultUpdate")
	@ResponseBody
	public String faultUpdate(String val) {
		String str = "";
		try {
			str = chartService.faultUpdate(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "setPrgSet")
	@ResponseBody
	public String setPrgSet(String val) {
		String str = "";
		try {
			str = chartService.setPrgSet(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getProgInfo")
	@ResponseBody
	public String getProgInfo(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getProgInfo(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "setSpdLoad")
	@ResponseBody
	public String setSpdLoad(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.setSpdLoad(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getCalcTy")
	@ResponseBody
	public ChartVo getCalcTy(ChartVo chartVo) {
		try {
			chartVo = chartService.getCalcTy(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return chartVo;
	}

	@RequestMapping(value = "getSpdLoadInfo")
	@ResponseBody
	public ChartVo getSpdLoadInfo(ChartVo chartVo) {
		try {
			chartVo = chartService.getSpdLoadInfo(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return chartVo;
	}

	@RequestMapping(value = "getToolInfo")
	@ResponseBody
	public String getToolInfo(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getToolInfo(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "saveUpdatedStock")
	@ResponseBody
	public String saveUpdatedStock(String val) {
		String str = "";
		try {
			str = chartService.saveUpdatedStock(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getOprNameList")
	@ResponseBody
	public String getOprNameList(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getOprNameList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getOprNmList")
	@ResponseBody
	public String getOprNmList(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getOprNmList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getLotInfo")
	@ResponseBody
	public String getLotInfo(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getLotInfo(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "testChart")
	public String test2() {
		return "chart/getTimeDataChart";
	};

	@RequestMapping(value = "getShipLotNo")
	@ResponseBody
	public String getShipLotNo(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getShipLotNo(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getOutLotTracer")
	@ResponseBody
	public String getOutLotTracer(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getOutLotTracer(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};

	@RequestMapping(value="login")
	@ResponseBody 
	public String loginCnt(ChartVo chartVo){
		String str = "failed"; 
		try {
			str = chartService.login(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block 
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value = "chkLogin")
	@ResponseBody
	public String chkLogin(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.chkLogin(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};

	@RequestMapping(value = "getLotNo")
	@ResponseBody
	public String getLotNo(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getLotNo(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	/*
	 * 2017-11-16
	 * Deprecated 예약.
	 * */
	@RequestMapping(value = "getLotNoByPrdNo")
	@ResponseBody
	@Deprecated
	public String getLotNoByPrdNo(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getLotNoByPrdNo(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getDvcListByPrdNo")
	@ResponseBody
	public String getDvcListByPrdNo(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getDvcListByPrdNo(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getCheckType")
	@ResponseBody
	public String getCheckType(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getCheckType(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getNewMatInfo")
	@ResponseBody
	public ChartVo getNewMatInfo(ChartVo chartVo) {
		try {
			chartVo = chartService.getNewMatInfo(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return chartVo;
	}

	@RequestMapping(value = "addNoOperation")
	public String addNoOperation(ChartVo chartVo) {
		return "chart/addNoOperation";
	};

	

	@RequestMapping(value = "getNonOpData")
	@ResponseBody
	public ChartVo getNonOpData(ChartVo chartVo) {
		try {
			chartVo = chartService.getNonOpData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return chartVo;
	};

	@RequestMapping(value = "updateNonOp")
	@ResponseBody
	public String updateNonOp(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.updateNonOp(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "addNonOp")
	@ResponseBody
	public String addNonOp(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.addNonOp(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "okDelNonOp")
	@ResponseBody
	public String okDelNonOp(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.okDelNonOp(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getOprNm")
	@ResponseBody
	public String getOprNm(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getOprNm(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getNonOpTy")
	@ResponseBody
	public String getNonOpTy(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getNonOpTy(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getStockInfo")
	@ResponseBody
	public ChartVo getStockInfo(ChartVo chartVo) {
		try {
			chartVo = chartService.getStockInfo(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return chartVo;
	};

	@RequestMapping(value = "getNonOpInfo")
	@ResponseBody
	public String getNonOpInfo(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getNonOpInfo(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "updateStock")
	@ResponseBody
	public String updateStock(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.updateStock(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "okDelStock")
	@ResponseBody
	public String okDelStock(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.okDelStock(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "addStock")
	@ResponseBody
	public String addStock(String val) {
		String str = "";
		try {
			str = chartService.addStock(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getMatInfo")
	@ResponseBody
	public String getMatInfo(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getMatInfo(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	

	@RequestMapping(value = "edit_svg")
	public String edit_svg() {
		return "chart/edit_svg";
	};

	@RequestMapping(value = "updateBGImg")
	@ResponseBody
	public String updateBGImg(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.updateBGImg(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "addMachine")
	@ResponseBody
	public String addMachine(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.addMachine(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getLeadTime")
	@ResponseBody
	public String getLeadTime(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getLeadTime(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "updateCd")
	@ResponseBody
	public String updateCd(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.updateCd(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "addTarget")
	public String addTarget() {
		return "chart/addTarget";
	};

	@RequestMapping(value = "addTarget_print")
	public String addTarget_print(String shopId, String date, Model model) {
		model.addAttribute("shopId", shopId);
		model.addAttribute("date", date);
		return "chart/addTarget_print";
	}

	@RequestMapping(value = "okDel")
	@ResponseBody
	public String okDel(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.okDel(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getCnt")
	@ResponseBody
	public String getCnt(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getCnt(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getChecker")
	@ResponseBody
	public String getChecker(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getChecker(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getFaultList")
	@ResponseBody
	public String getFaultList(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getFaultList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "addTmpFaulty")
	@ResponseBody
	public String addFaulty(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.addFaulty(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getDevieList")
	@ResponseBody
	public String getDevieList(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getDevieList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getCheckTyList")
	@ResponseBody
	public String getCheckTyList(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getCheckTyList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getFaultyList")
	@ResponseBody
	public String getFaultyList(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getFaultyList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "leadTime")
	public String leadTime() {
		return "chart/leadTime";
	}

	@RequestMapping(value = "banner")
	public String banner() {
		return "chart/banner";
	}

	@RequestMapping(value = "faulty")
	public String customerFaulty() {
		return "chart/faulty";
	}

	@RequestMapping(value = "faulty_kpi")
	public String customerFaulty_kpi() {
		return "chart/faulty_kpi";
	}

	@RequestMapping(value = "addFaulty")
	public String processFaulty(HttpServletRequest request) {
		request.setAttribute("addFaulty", request.getParameter("addFaulty"));
		request.setAttribute("prdNo", request.getParameter("prdNo"));
		request.setAttribute("cnt", request.getParameter("cnt"));
		request.setAttribute("workerCd", request.getParameter("worker"));
		return "chart/addFaulty";
	}

	@RequestMapping(value = "addFaultyHistory")
	public String addFaultyHistory() {
		return "chart/addFaultyHistory";
	}

	@RequestMapping(value = "reOpCycle")
	public String reOpCycle() {
		return "chart/reOpCycle";
	}
	@RequestMapping(value = "reOpCycle_TEST")
	public String reOpCycle_TEST() {
		return "chart/reOpCycle_TEST";
	}
	
	@RequestMapping(value = "addFaultyHistoryForKpi")
	public String addFaultyHistoryForKpi() {
		return "chart/addFaultyHistoryForKpi";
	}
	
	@RequestMapping(value = "stockStatusForKpi")
	public String stockStatusForKpi() {
		return "chart/stockStatusForKpi";
	}
	
	@RequestMapping(value = "alarmReportForKpi")
	public String alarmReportForKpi() {
		return "chart/alarmReportForKpi";
	}

	@RequestMapping(value = "getAdtList")
	@ResponseBody
	public String getAdtList(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getAdtList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};

	@RequestMapping(value = "getStatusExpression")
	@ResponseBody
	public String getStatusExpression(String dvcId) {
		String str = "";
		try {
			str = chartService.getStatusExpression(dvcId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			str = "fail";
		}
		return str;
	};
	
	@RequestMapping(value = "getMachineInfo")
	@ResponseBody
	public String getMachineInfo(SVGVo svgVo) {
		String str = "";
		try {
			str = chartService.getMachineInfo(svgVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};

	@RequestMapping(value = "totalMachinePrdStatus")
	public String totalMachinePrdStatus() {
		return "chart/totalMachinePrdStatus";
	};

	@RequestMapping(value = "adapterManager")
	public String adapterManager() {
		return "chart/adapterManager";
	};

	@RequestMapping(value = "getTemplate")
	@ResponseBody
	public String getTemplate(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getTemplate(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};

	@RequestMapping(value = "getCurrentProg")
	@ResponseBody
	public ChartVo getCurrentProg(ChartVo chartVo) {
		try {
			chartVo = chartService.getCurrentProg(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return chartVo;
	};

	@RequestMapping(value = "programManager_tool")
	public String programManager_kpi() {
		return "chart/programManager_tool";
	};

	@RequestMapping(value = "programManager")
	public String programManager() {
		return "chart/programManager";
	};

	@RequestMapping(value = "getSLCR")
	@ResponseBody
	public String getSLCR(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getSLCR(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getPrgInfo")
	@ResponseBody
	public String getPrgInfo(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getPrgInfo(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getProgCd")
	@ResponseBody
	public String getProgCd(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getProgCd(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getPrgCdList")
	@ResponseBody
	public String getPrgCdList(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getPrgCdList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};

	@RequestMapping(value = "getPerformanceAgainstGoal")
	@ResponseBody
	public String getPerformanceAgainstGoal(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getPerformanceAgainstGoal(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getGroup")
	@ResponseBody
	public String getGroup(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getGroup(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getBanner")
	@ResponseBody
	public ChartVo getBanner(ChartVo chartVo) {
		try {
			chartVo = chartService.getBanner(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return chartVo;
	}

	@RequestMapping(value = "performanceAgainstGoal")
	public String performanceAgainstGoal(HttpServletRequest request) {
		request.setAttribute("sDate", request.getParameter("sDate"));
		request.setAttribute("eDate", request.getParameter("eDate"));
		return "chart/performanceAgainstGoal";
	};

	@RequestMapping(value = "stockStatus")
	public String stockStatus() {
		return "chart/stockStatus";
	};

	@RequestMapping(value = "addBanner")
	@ResponseBody
	public void addBanner(ChartVo chartVo) {
		try {
			chartService.addBanner(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@RequestMapping(value = "cancelBanner")
	@ResponseBody
	public void cancelBanner(ChartVo chartVo) {
		try {
			chartService.cancelBanner(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "resetTool")
	@ResponseBody
	public void resetTool(ChartVo chartVo) {
		try {
			chartService.resetTool(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	};

	@RequestMapping(value = "getFacilitiesStatus")
	@ResponseBody
	public String getFacilitiesStatus(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getFacilitiesStatus(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "prdStatus_MCT")
	public String neonsign_MCT() {
		return "chart/neonSign_MCT";
	}

	@RequestMapping(value = "prdStatus_CNC")
	public String neonsign_CNC() {
		return "chart/neonSign_CNC";
	}

	@RequestMapping(value = "performanceAgainstGoal_chart")
	public String performanceAgainstGoal_chart(HttpServletRequest request) {
		request.setAttribute("sDate", request.getParameter("sDate"));
		request.setAttribute("eDate", request.getParameter("eDate"));

		return "chart/performanceAgainstGoal_chart";
	}

	@RequestMapping(value = "performanceAgainstGoal_chart_kpi")
	public String performanceAgainstGoal_chart_kpi() {
		return "chart/performanceAgainstGoal_chart_kpi";
	}

	@RequestMapping(value = "tableChart")
	public String tableChart() {
		return "chart/tableChart";
	};

	@RequestMapping(value = "traceManager")
	public String traceManager() {
		return "chart/traceManager";
	};

	@RequestMapping(value = "lotTracer")
	public String lotTracer() {
		return "chart/lotTracer";
	};

	@Deprecated
	@RequestMapping(value = "getLotTracer1")
	@ResponseBody
	public String getLotTracer(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getLotTracer(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getTraceHist")
	@ResponseBody
	public String getTraceHist(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getTraceHist(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getData")
	@ResponseBody
	public ChartVo getData(ChartVo chartVo) {
		try {
			chartVo = chartService.getData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return chartVo;
	}

	@RequestMapping(value = "getAllDvc")
	@ResponseBody
	public String getAllDvc(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getAllDvc(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};

	@RequestMapping(value = "getDvcId")
	@ResponseBody
	public String getDvcId(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getDvcId(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};

	@RequestMapping(value = "getReOpList")
	@ResponseBody
	public String getReOpList(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getReOpList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getRepairList")
	@ResponseBody
	public String getRepairList(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getRepairList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};

	@RequestMapping(value = "	getDetailBlockData")
	@ResponseBody
	public String getDetailBlockData(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getDetailBlockData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getDvc")
	@ResponseBody
	public String getDvc(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getDvc(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};

	@RequestMapping(value = "getRoutingInfo")
	@ResponseBody
	public String getRoutingInfo(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getRoutingInfo(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getDvcNameList")
	@ResponseBody
	public String getDvcNameList(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getDvcNameList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};

	@RequestMapping(value = "getDvcIdForSign")
	@ResponseBody
	public String getDvcIdForSign(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getDvcIdForSign(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};

	@RequestMapping(value = "getTimeData")
	@ResponseBody
	public String getTimeData(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getTimeData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};

	@RequestMapping(value = "getStartTime")
	@ResponseBody
	public String getStartTime(ChartVo chartVo) {
		logger.info(chartVo.getShopId().toString());
		String str = "";
		try {
			str = chartService.getStartTime(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};

	@RequestMapping(value = "getJigList")
	@ResponseBody
	public String getJicList(ChartVo chartVo) {
		String str = null;
		try {
			str = chartService.getJicList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};

	@RequestMapping(value = "rotateChart")
	public String rotateChart(ChartVo chartVo, HttpServletRequest request) {
		request.setAttribute("fromDashBoard", request.getParameter("fromDashBoard"));
		return "chart/detailChart_block_rotate";
	};

	@RequestMapping(value = "getToolList")
	@ResponseBody
	public String getToolList(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getToolList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	
	@RequestMapping(value = "toolManager")
	public String toolManager(ChartVo chartVo) {
		return "chart/toolManager";
	};

	@RequestMapping(value = "lampExpression")
	public String lampExpression(ChartVo chartVo) {
		return "chart/lampExpression";
	};
	
	@RequestMapping(value = "layout_Setting")
	public String layout_Setting() {
		return "chart/layout-Setting";
	};

	@RequestMapping(value = "setUtc")
	@ResponseBody
	public String setUtc(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.setUtc(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			str = "fail";
		}
		return str;
	};

	@RequestMapping(value = "getDivision")
	@ResponseBody
	public String getDivision(String shopId) {
		
		String result="";
		
		try {
			result = chartService.getDivision(shopId);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	@RequestMapping(value = "getUtc")
	@ResponseBody
	public String getUtc(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getUtc(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			str = "fail";
		}
		return str;
	};
	
	@RequestMapping(value = "ETC_Setting")
	public String ETC_Setting() {
		return "chart/ETC-Setting";
	};
	
	@RequestMapping(value = "getComStartTime")
	@ResponseBody
	public String getComStartTime(String shopId) {
		String str = "";
		try {
			str = chartService.getComStartTime(shopId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getLampExpression")
	@ResponseBody
	public String getLampExpression() {
		String str = "";
		try {
			str = chartService.getLampExpression();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value = "setLampExpression")
	@ResponseBody
	public String setLampExpression(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.setLampExpression(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			str = "fail";
		}
		return str;
	};
	
	@RequestMapping(value = "getAlarmList")
	@ResponseBody
	public String getAlarmList(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getAlarmList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};

	@RequestMapping(value = "dailyChart")
	public String dailyChart(ChartVo chartVo, HttpServletRequest request) {
		request.setAttribute("dvcName", chartVo.getName());
		request.setAttribute("sDate", chartVo.getSDate());
		request.setAttribute("eDate", chartVo.getEDate());

		return "/chart/wcGraph";
	};

	@RequestMapping(value = "getJigList4Report")
	@ResponseBody
	public String getJigList4Report(ChartVo chartVo) {
		String result = "";
		try {
			result = chartService.getJigList4Report(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};

	@RequestMapping(value = "getDvcList")
	@ResponseBody
	public String getDvcList(String shopId) {
		String result = "";
		try {
			result = chartService.getDvcList(shopId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};

	@RequestMapping(value = "getWcData")
	@ResponseBody
	public String getWcData(ChartVo chartVo) {
		String result = "";
		try {
			result = chartService.getWcData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};

	@RequestMapping(value = "getWcDataByDvc")
	@ResponseBody
	public String getWcDataByDvc(ChartVo chartVo) {
		String result = "";
		try {
			result = chartService.getWcDataByDvc(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};

	@RequestMapping(value = "getWorkerInfoo")
	@ResponseBody
	public ChartVo getWorkerInfoo(ChartVo chartVo) {
		try {
			chartVo = chartService.getWorkerInfo(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return chartVo;
	};

	@RequestMapping(value = "addWorker")
	@ResponseBody
	public String addWorker(ChartVo chartVo) {
		String result = "";
		try {
			result = chartService.addWorker(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};

	@RequestMapping(value = "getNextWorkerId")
	@ResponseBody
	public String getNextWorkerId(ChartVo chartVo) {
		String result = "";
		try {
			result = chartService.getNextWorkerId(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};

	@RequestMapping(value = "getTableData")
	@ResponseBody
	public String getTableData(ChartVo chartVo) {
		String result = "";
		try {
			result = chartService.getTableData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};

	@RequestMapping(value = "setPrdCmplData")
	@ResponseBody
	public String setPrdCmplData(ChartVo chartVo) {
		String str = null;
		try {
			str = chartService.setPrdCmplData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};

	/*
	 * 2017.09.18 퇴근보고 일생산 계획 등록에서 가져오기 Start 생성자 : John
	 */
	@RequestMapping(value = "getTodayWorkByName")
	@ResponseBody
	public String getTodayWorkByName(ChartVo chartVo) {
		String str = null;
		try {
			str = chartService.getTodayWorkByName(chartVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getWcList")
	@ResponseBody
	public String getWcList(ChartVo chartVo) {
		String str = null;
		try {
			str = chartService.getWcList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};

	@RequestMapping(value = "highChartTest")
	public String highChartTest() {
		return "chart/highChartTest";
	};

	@RequestMapping(value = "getAlarmData")
	@ResponseBody
	public String getAlarmData(ChartVo chartVo) {
		String result = "";
		try {
			result = chartService.getAlarmData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};

	@RequestMapping(value = "performanceReport")
	public String performanceReport() {
		return "chart/performanceReport";
	};

	@RequestMapping(value = "jigGraph")
	public String jigGraph(HttpServletRequest request, ChartVo chartVo) {
		request.setAttribute("group", chartVo.getGroup());
		return "chart/jigGraph";
	};

	@RequestMapping(value = "jigGraph_kpi")
	public String jigGraph_kpi(HttpServletRequest request, ChartVo chartVo) {
		request.setAttribute("group", chartVo.getGroup());
		return "chart/jigGraph_kpi";
	};

	@RequestMapping(value = "wcGraph")
	public String wcGraph() {
		return "chart/wcGraph";
	};

	@RequestMapping(value = "alarmReport")
	public String alarmReport() {
		return "chart/alarmReport";
	};

	@RequestMapping(value = "DIMM")
	public String DIMM() {
		return "chart/DIMM";
	};

	@RequestMapping(value = "dimf")
	public String dkimf() {
		return "chart/DIMF";
	};

	@RequestMapping(value = "edit")
	public String edit() {
		return "chart/dashBoard_edit";
	};

	@RequestMapping(value = "main_sb")
	public String main_sb() {
		return "chart/dashBoard_sb";
	};

	@RequestMapping(value = "addPrdCmpl")
	public String addPrdCmpl() {
		return "chart/addPrdCmpl";
	}

	@RequestMapping(value = "addPrdCmplHistory")
	public String addPrdCmplHistory() {
		return "chart/addPrdCmplHistory";
	};

	@RequestMapping(value = "index")
	public String main() {
		return "chart/main";
	};

	@RequestMapping(value = "checkPrdct")
	public String checkPrdct(HttpServletRequest request) {
		request.setAttribute("workerCd", request.getParameter("worker"));

		return "chart/checkPrdct";
	};

	@RequestMapping(value = "checkPrdctStandard")
	public String checkPrdctStandard() {

		return "chart/checkPrdctStandard";
	};

	@RequestMapping(value = "main")
	public String dashBoard() {

		return "chart/dashBoard";
	};

	@RequestMapping(value = "rotation")
	public String dashBoard_rotation() {

		return "chart/dashBoard_rotation";
	};

	@RequestMapping(value = "main_test")
	public String main_test() {

		return "chart/dashBoard_test";
	};

	@RequestMapping(value = "FC")
	public String FC() {

		return "chart/dashBoard_fc";
	};

	@RequestMapping(value = "incomeStock")
	public String incomeStock() {
		return "chart/incomeStock";
	};

	@RequestMapping(value = "workerMaanger")
	public String workerMaanger() {
		return "chart/workerManager";
	};

	@RequestMapping(value = "incomeStockHistory")
	public String incomeStockHistory() {
		return "chart/incomeStockHistory";
	};

	@RequestMapping(value = "releaseStock")
	public String release() {
		return "chart/release";
	};

	@RequestMapping(value = "releaseStockHistory")
	public String releaseStockHistory() {
		return "chart/releaseHistory";
	}

	@RequestMapping(value = "shipment")
	public String shipment() {
		return "chart/shipment";
	};

	@RequestMapping(value = "shipmentHistory")
	public String shipmentHistory() {
		return "chart/shipmentHistory";
	};

	@RequestMapping(value = "main_slide")
	public String main_slide() {
		return "chart/dashBoard_slide";
	};

	@RequestMapping(value = "main2_slide")
	public String main2_slide() {
		return "chart/dashBoard2_slide";
	};

	@RequestMapping(value = "main3_slide")
	public String main3_slide() {
		return "chart/dashBoard3_slide";
	};

	@RequestMapping(value = "main4_slide")
	public String main4_slide() {
		return "chart/dashBoard4_slide";
	};

	@RequestMapping(value = "main_en")
	public String main_en() {
		return "chart/dashBoard_en";
	};

	@RequestMapping(value = "reSizing")
	public String reSizing() {
		return "chart/dashBoard_resizing";
	};

	@RequestMapping(value = "multiVision")
	public String multiVision() {
		return "chart/multiVision";
	};

	// @RequestMapping(value="index")
	// public String index(){
	// return "chart/index";
	// };

	@RequestMapping(value = "IE")
	public String IE() {
		return "chart/dashBoard_IE";
	};

	@RequestMapping(value = "main2")
	public String main2() {
		return "chart/dashBoard2";
	};

	@RequestMapping(value = "main3")
	public String main3() {
		return "chart/dashBoard3";
	};

	@RequestMapping(value = "main4")
	public String main4() {
		return "chart/dashBoard4";
	};

	@RequestMapping(value = "mobile")
	public String mobile() {
		return "chart/mobile";
	};

	@RequestMapping(value = "mobile2")
	public String mobile2() {
		return "chart/mobile2";
	};

	@RequestMapping(value = "highChart1")
	public String highChart1() {
		return "chart/highChart1";
	};

	@RequestMapping(value = "highChart2")
	public String highChart2() {
		return "chart/highChart2";
	}

	@RequestMapping(value = "highChart3")
	public String highChart3() {
		return "chart/highChart3";
	};

	@RequestMapping(value = "highChart4")
	public String highChart4() {
		return "chart/highChart4";
	};

	@RequestMapping(value = "video")
	public String video() {
		return "test/video";
	}

	@RequestMapping(value = "video2")
	public String video2() {
		return "test/video2";
	};

	@RequestMapping(value = "getStatusData")
	@ResponseBody
	public String getStatusData(ChartVo chartVo) {
		String result = "";
		try {
			Calendar cal = Calendar.getInstance();// 오늘 날짜를 기준으루..
			// cal.add ( cal.DATE, 1); //2개월 전....
			String year = String.valueOf(cal.get(cal.YEAR));
			String month = addZero(String.valueOf(cal.get(cal.MONTH) + 1));
			String date = addZero(String.valueOf(cal.get(cal.DATE)));

			String hour = String.valueOf(cal.get(cal.HOUR_OF_DAY));
			String minute = String.valueOf(cal.get(cal.MINUTE));
			String second = String.valueOf(cal.get(cal.SECOND));
			chartVo.setDate(year + "-" + month + "-" + date);

			if (Integer.parseInt(hour) >= 20) {
				cal.add(cal.DATE, 1);
				year = String.valueOf(cal.get(cal.YEAR));
				month = addZero(String.valueOf(cal.get(cal.MONTH) + 1));
				date = addZero(String.valueOf(cal.get(cal.DATE)));

				chartVo.setDate(year + "-" + month + "-" + date);
			}
			;
			// chartVo.setDate(CommonFunction.mil2WorkDate(System.currentTimeMillis()));
			result = chartService.getStatusData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// result = chartVo.getCallback() + "(" + result + ")";
		return result;
	};

	@RequestMapping(value = "test")
	@ResponseBody
	public void test() {
		ChartVo chartVo = new ChartVo();
		chartVo.setDate("2015-05-09");
		try {
			chartService.test(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@RequestMapping(value = "getCurrentDvcData")
	@ResponseBody
	public ChartVo getCurrentDvcData(ChartVo chartVo) {
		try {
			Calendar cal = Calendar.getInstance();// 오늘 날짜를 기준으루..
			// cal.add ( cal.DATE, 1); //2개월 전....

			String year = String.valueOf(cal.get(cal.YEAR));
			String month = addZero(String.valueOf(cal.get(cal.MONTH) + 1));
			String date = addZero(String.valueOf(cal.get(cal.DATE)));

			String hour = String.valueOf(cal.get(cal.HOUR_OF_DAY));
			String minute = String.valueOf(cal.get(cal.MINUTE));
			String second = String.valueOf(cal.get(cal.SECOND));
			chartVo.setDate(year + "-" + month + "-" + date);

			if (Integer.parseInt(hour) >= 20) {
				cal.add(cal.DATE, 1);
				year = String.valueOf(cal.get(cal.YEAR));
				month = addZero(String.valueOf(cal.get(cal.MONTH) + 1));
				date = addZero(String.valueOf(cal.get(cal.DATE)));

				chartVo.setDate(year + "-" + month + "-" + date);
			}
			;

			chartVo = chartService.getCurrentDvcData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		;
		return chartVo;
	};

	// @Scheduled(fixedDelay = 5000)
	public void addData() {
		try {
			chartService.addData();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	};

	@RequestMapping(value = "getDvcName")
	@ResponseBody
	public String getDvcName(ChartVo chartVo) {
		String dvcName = null;
		try {
			dvcName = chartService.getDvcName(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dvcName;
	};

	@RequestMapping(value = "getAllDvcStatus")
	@ResponseBody
	public String getAllDvcStatus(ChartVo chartVo) {
		String result = null;
		try {
			result = chartService.getAllDvcStatus(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};

	@RequestMapping(value = "getAllDvcId")
	@ResponseBody
	public String getAllDvcId(ChartVo chartVo) {
		String result = null;
		try {
			result = chartService.getAllDvcId(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};

	@RequestMapping(value = "getBarChartDvcId")
	@ResponseBody
	public String getBarChartDvcId2(ChartVo chartVo) {
		String result = null;
		try {
			result = chartService.getBarChartDvcId(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};

	public String addZero(String str) {
		String rtn = str;
		if (rtn.length() == 1) {
			rtn = "0" + str;
		}
		;
		return rtn;
	};

	@RequestMapping(value = "getTime")
	@ResponseBody
	public String getTime() {
		String rtn = "";

		Calendar cal = Calendar.getInstance();// 오늘 날짜를 기준으루..
		// cal.add ( cal.DATE, 1); //2개월 전....

		String year = String.valueOf(cal.get(cal.YEAR));
		String month = String.valueOf(cal.get(cal.MONTH) + 1);
		String day = String.valueOf(cal.get(cal.DATE));

		String hour = String.valueOf(cal.get(cal.HOUR_OF_DAY));
		String minute = String.valueOf(cal.get(cal.MINUTE));
		String second = String.valueOf(cal.get(cal.SECOND));
		rtn = year + ". " + month + ". " + day + ".-" + hour + ":" + minute + ":" + second;
		return rtn;
	};

	@RequestMapping(value = "dashBoard_pad")
	public String pad() {
		return "chart/dashBoard_pad";
	};

	@RequestMapping(value = "dashBoard_left")
	public String pad_left() {
		return "chart/dashBoard_left";
	};

	@RequestMapping(value = "dashBoard_right")
	public String pad_right() {
		return "chart/dashBoard_right";
	};

	@RequestMapping(value = "dashBoard_center")
	public String center() {
		return "chart/dashBoard_center";
	};

	@RequestMapping(value = "polling1")
	@ResponseBody
	public String polling1() throws Exception {
		return chartService.polling1();
	};

	@RequestMapping(value = "setVideo")
	@ResponseBody
	public String setVideo(String id) throws Exception {
		return chartService.setVideo(id);
	};

	@RequestMapping(value = "initVideoPolling")
	@ResponseBody
	public void initVideoPolling() throws Exception {
		chartService.initVideoPolling();
	}

	@RequestMapping(value = "initPiePolling")
	@ResponseBody
	public void initPiePolling() throws Exception {
		chartService.initPiePolling();
	};

	@RequestMapping(value = "piePolling1")
	@ResponseBody
	public String piePolling1() throws Exception {
		return chartService.piePolling1();
	};

	@RequestMapping(value = "piePolling2")
	@ResponseBody
	public String piePolling2() throws Exception {
		return chartService.piePolling2();
	};

	@RequestMapping(value = "setChart1")
	@ResponseBody
	public String setChart1(String id) throws Exception {
		return chartService.setChart1(id);
	};

	@RequestMapping(value = "setChart2")
	@ResponseBody
	public String setChart2(String id) throws Exception {
		return chartService.setChart2(id);
	};

	@RequestMapping(value = "delVideoMachine")
	@ResponseBody
	public void delVideoMachine(String id) throws Exception {
		chartService.delVideoMachine(id);
	};

	@RequestMapping(value = "delChartMachine")
	@ResponseBody
	public void delChartMachine(String id) throws Exception {
		chartService.delChartMachine(id);
	};

	@RequestMapping(value = "getAdapterId")
	@ResponseBody
	public String getAdapterId(ChartVo chartVo) {
		String result = null;
		try {
			Calendar cal = Calendar.getInstance();// 오늘 날짜를 기준으루..
			// cal.add ( cal.DATE, 1); //2개월 전....

			String year = String.valueOf(cal.get(cal.YEAR));
			String month = addZero(String.valueOf(cal.get(cal.MONTH) + 1));
			String date = addZero(String.valueOf(cal.get(cal.DATE)));

			String hour = String.valueOf(cal.get(cal.HOUR_OF_DAY));
			String minute = String.valueOf(cal.get(cal.MINUTE));
			String second = String.valueOf(cal.get(cal.SECOND));
			chartVo.setDate(year + "-" + month + "-" + date);

			if (Integer.parseInt(hour) >= 20) {
				cal.add(cal.DATE, 1);
				year = String.valueOf(cal.get(cal.YEAR));
				month = addZero(String.valueOf(cal.get(cal.MONTH) + 1));
				date = addZero(String.valueOf(cal.get(cal.DATE)));

				chartVo.setDate(year + "-" + month + "-" + date);
			}
			;

			// result = chartService.getAllDvcId(chartVo);
			result = chartService.getAdapterId(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};

	@RequestMapping(value = "showDetailEvent")
	@ResponseBody
	public String showDetailEvent(ChartVo chartVo) throws Exception {
		return chartService.showDetailEvent(chartVo);
	};

	@RequestMapping(value = "getDetailStatus")
	@ResponseBody
	public String getDetailStatus(ChartVo chartVo) throws Exception {
		String result = null;
		try {
			Calendar cal = Calendar.getInstance();// 오늘 날짜를 기준으루..
			// cal.add ( cal.DATE, 1); //2개월 전....

			String year = String.valueOf(cal.get(cal.YEAR));
			String month = addZero(String.valueOf(cal.get(cal.MONTH) + 1));
			String date = addZero(String.valueOf(cal.get(cal.DATE)));

			String hour = String.valueOf(chartVo.getStartDateTime());
			String minute = String.valueOf(cal.get(cal.MINUTE));
			String second = String.valueOf(cal.get(cal.SECOND));

			chartVo.setDate(year + "-" + month + "-" + date);
			chartVo.setStartDateTime(year + "-" + month + "-" + date + " " + hour + ":00");
			chartVo.setEndDateTime(year + "-" + month + "-" + date + " " + hour + ":59");

			if (Integer.parseInt(hour) >= 20 && Integer.parseInt(hour) != 24) {
				cal.add(cal.DATE, -1);
				year = String.valueOf(cal.get(cal.YEAR));
				month = addZero(String.valueOf(cal.get(cal.MONTH) + 1));
				date = addZero(String.valueOf(cal.get(cal.DATE)));

				chartVo.setStartDateTime(year + "-" + month + "-" + date + " " + hour + ":00");
				chartVo.setEndDateTime(year + "-" + month + "-" + date + " " + hour + ":59");
			} else if (Integer.parseInt(hour) == 24) {
				year = String.valueOf(cal.get(cal.YEAR));
				month = addZero(String.valueOf(cal.get(cal.MONTH) + 1));
				date = addZero(String.valueOf(cal.get(cal.DATE)));

				chartVo.setStartDateTime(year + "-" + month + "-" + date + " " + "00:00");
				chartVo.setEndDateTime(year + "-" + month + "-" + date + " " + "00:59");
			}
			;

			// result = chartService.getAllDvcId(chartVo);
			result = chartService.getDetailStatus(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};

	// need param
	// -- deprecated. : inputVo.date = workdate
	// inputVo.dvcId = dvcId
	// inputVo.targetDateTime = yyyy-MM-dd HH:mm:ss
	@RequestMapping(value = "getTimeChart")
	@ResponseBody
	public List<BarChartVo> getTimeChart(ChartVo chartVo) {
		String workDate = CommonFunction.dateTime2WorkDate(chartVo.getTargetDateTime());
		chartVo.setDate(workDate);

		List<BarChartVo> chart = new ArrayList<BarChartVo>();
		List<TimeChartVo> listTime = chartService.getTimeChartData(chartVo);

		// rtn Data is empty.
		if (listTime == null) {
			return null;
		}

		Iterator<TimeChartVo> ite = listTime.iterator();
		while (ite.hasNext()) {
			TimeChartVo srcVo = ite.next();
			BarChartVo tmpVo = new BarChartVo();
			List<DataVo> data = new ArrayList<DataVo>();
			DataVo tmpDataVo = new DataVo();
			tmpDataVo.setStartTime(srcVo.getStartTime());
			tmpDataVo.setEndTime(srcVo.getEndTime());
			tmpDataVo.setY(srcVo.getY());
			data.add(tmpDataVo);
			tmpVo.setData(data);
			tmpVo.setColor(srcVo.getColor());
			chart.add(tmpVo);
		}
		return chart;
	};

	@RequestMapping(value = "getOtherChartsData")
	@ResponseBody
	public ChartVo getOtherChartsData(ChartVo chartVo) throws Exception {
		return chartService.getOtherChartsData(chartVo);
	};

	@RequestMapping(value = "testTimeChart")
	@ResponseBody
	public List<ChartVo> testTimeChart(ChartVo chartVo) throws Exception {
		return chartService.testTimeChartData(chartVo);
	};

	@RequestMapping(value = "singleChartStatus")
	public String singleChartStatus(HttpServletRequest request, HttpServletResponse response) {
		request.setAttribute("fromDashBoard", request.getParameter("fromDashBoard"));
		return "chart/singleChartStatus";
		// return "chart/detailChart_block";
	};

	@RequestMapping(value = "singleChartStatus_rotation")
	public String singleChartStatus_rotation(HttpServletRequest request, HttpServletResponse response) {
		request.setAttribute("fromDashBoard", request.getParameter("fromDashBoard"));
		return "chart/singleChartStatus_rotation";
		// return "chart/detailChart_block";
	};

	@RequestMapping(value = "singleChartStatus2")
	public String singleChartStatus2() {
		return "chart/singleChartStatus";
	};

	@RequestMapping(value = "singleChartStatus_MCT")
	public String singleChartStatus_MCT() {
		return "chart/singleChartStatus_MCT";
	};

	@RequestMapping(value = "singleChartStatus_CNC")
	public String singleChartStatus_CNC() {
		return "chart/singleChartStatus_CNC";
	};

	@RequestMapping(value = "prdStatus")
	public String neonsign(HttpServletRequest request) {
		String line = request.getParameter("line");

		request.setAttribute("_line", line);
		return "chart/neonSign";
	}

	@RequestMapping(value = "setLimit")
	@ResponseBody
	public void setLimit(ChartVo chartVo) {
		try {
			chartService.setLimit(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	};

	@RequestMapping(value = "delMachine")
	@ResponseBody
	public String delMachine(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.delMachine(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getBGImg")
	@ResponseBody
	public String getBGImg(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getBGImg(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "setMachinePos_edit")
	@ResponseBody
	public void setMachinePos(ChartVo chartVo) {
		try {
			chartService.setMachinePos_edit(chartVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	};

	@RequestMapping(value = "getMachine")
	@ResponseBody
	public String getMachine(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getMachine(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getRcvInfo")
	@ResponseBody
	public String getRcvInfo(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getRcvInfo(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getMachineName")
	@ResponseBody
	public String getMachineName(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getMachineName(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getWorkerWatingTime")
	@ResponseBody
	public String getWorkerWatingTime(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getWorkerWatingTime(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getWorkerStatus")
	public String getWorkerStatus() {

		return "chart/getWorkerStatus";
	}

	@RequestMapping(value = "getMaintenanceReport")
	@ResponseBody
	public String getMaintenanceReport(ChartVo chartVo) {
		String str = "";

		try {
			str = chartService.getMaintenanceReport(chartVo);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return str;
	}

	@RequestMapping(value = "setMaintenanceReport")
	@ResponseBody
	public String setMaintenanceReport(ChartVo chartVo) {
		String str = "";
		try {
			chartService.setMaintenanceReport(chartVo);
			str = "success";
		} catch (Exception e) {
			e.printStackTrace();
		}

		return str;
	}

	@RequestMapping(value = "getMaintenanceReportList")
	@ResponseBody
	public String getMaintenanceReportList(@RequestParam(value = "empCd", required = false) int empCd) {
		String str = "";
		try {
			str = chartService.getMaintenanceReportList(empCd);
		} catch (Exception e) {
			e.getStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getMaintenanceReportListPage")
	public String getMaintenanceReportListPage() {

		return "chart/getMaintenanceReportListPage";
	}

	@RequestMapping(value = "getWorkerPerformance")
	public String getWorkerPerformance() {

		return "chart/getWorkerPerformance";
	}

	@RequestMapping(value = "getMaintenanceReportListPageList")
	@ResponseBody
	public String getMaintenanceReportListPageList(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getMaintenanceReportListdvcId(chartVo);
		} catch (Exception e) {
			e.getStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "delMaintenanceReport")
	@ResponseBody
	public String delMaintenanceReport(ChartVo chartVo) {
		String str = "";
		try {
			chartService.delMaintenanceReport(chartVo);
			str = "success";
		} catch (Exception e) {
			e.getStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "cancelInvoice")
	@ResponseBody
	public String cancelInvoice(String val) {
		String str = "";
		try {
			chartService.cancelInvoice(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getAttendant_List")
	@ResponseBody
	public String getAttendant_List(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getAttendant_List(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "upDateWorkTimeHist")
	@ResponseBody
	public String upDateWorkTimeHist(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.upDateWorkTimeHist(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "addWorkTimeHist_Correct")
	@ResponseBody
	public String addWorkTimeHist_Correct(ChartVo chartVo) {
		String str = "";
		logger.info(chartVo.getWorker());
		logger.info(chartVo.getStartTime());
		try {
			str = chartService.addWorkTimeHist_Correct(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "addNonOpHst_One")
	@ResponseBody
	public String addNonOpHst_One(String val) {
		String str = "";
		try {
			str = chartService.addNonOpHst_One(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getmstmatno")
	@ResponseBody
	public String getmstmatno(String RWMATNO) {
		String str = "";
		try {
			str = chartService.getmstmatno(RWMATNO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getbertmstmatno")
	@ResponseBody
	public String getbertmstmatno(String RWMATNO) {
		String str = "";
		try {
			str = chartService.getbertmstmatno(RWMATNO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getmstitmprd")
	@ResponseBody
	public String getmstitmprd(String RWMATNO) {
		String str = "";
		try {
			str = chartService.getmstitmprd(RWMATNO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value = "printTestPage")
	public String printTestPage() {
		return "chart/printTestPage";
	}

	/*
	 * String Encoding 확인 코드 작성자 : John(조부관)
	 */
	public void checkStringEncoding(String originalString) {
		String[] charSet = { "utf-8", "euc-kr", "ksc5601", "iso-8859-1", "x-windows-949" };

		for (int i = 0; i < charSet.length; i++) {
			for (int j = 0; j < charSet.length; j++) {
				try {
					System.out.println("[" + charSet[i] + "," + charSet[j] + "] = "
							+ new String(originalString.getBytes(charSet[i]), charSet[j]));
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@RequestMapping(value = "barcodebulchul")
	@ResponseBody
	public String barcodebulchul(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.barcodebulchul(chartVo);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return str;
	}
	
	@RequestMapping(value = "getNonopHistory")
	@ResponseBody
	public String getNonopHistory(String idx) {
		String str = "";
		try {
			str = chartService.getNonopHistory(idx);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return str;
	}
	
	@RequestMapping(value = "insertWaitTime")
	@ResponseBody
	public String insertWaitTime(String val, String date) {
		String str = "";
		try {
			str = chartService.insertWaitTime(val, date);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return str;
	}
	
	@ResponseBody
	@RequestMapping(value="tbDvcTgCnt")
	public String tbDvcTgCnt(ChartVo chartVo) {
		String str="";
//		str = orderService.tbDvcTgCnt(chartVo);
		System.out.println("dlalal");
		return str;
	}
	
	@RequestMapping(value = "getAlarmAction")
	@ResponseBody
	public String getAlarmAction(String val) {
		String result = "";
		try {
			System.out.println("val ::"+val);
			result = chartService.getAlarmAction(val);
//			result = chartService.getAlarmData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};
	
	@ResponseBody
	@RequestMapping(value="getReCount")
	public String getReCount(ChartVo chartVo) {
		String str="";
		try {
			str=chartService.getReCount(chartVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return str;
	}

	@ResponseBody
	@RequestMapping(value="alarmExcept")
	public String alarmExcept(String val) {
		String str="";
		try {
			str=chartService.alarmExcept(val);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return str;
	}
	@ResponseBody
	@RequestMapping(value="alarmreturn")
	public String alarmreturn(ChartVo chartVo) {
		String str="";
		try {
			str=chartService.alarmreturn(chartVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return str;
	}
	
	@ResponseBody
	@RequestMapping(value="getOuterStock")
	public String getOuterStock(ChartVo chartVo) {
		String str="";
		
		try {
			str = chartService.getOuterStock(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return str;
	}
	
	@ResponseBody
	@RequestMapping(value="MaintenanceSave")
	public String MaintenanceSave(String val) {
		String str="";
		
		try {
			str = chartService.MaintenanceSave(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return str;
	}
	
	@RequestMapping(value = "selectMenu")
	@ResponseBody
	public String selectMenu() {
		String str = "";
		try {
			str = chartService.selectMenu();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getworkingList")
	@ResponseBody
	public String getworkingList() {
		String str = "";
		try {
			str = chartService.getworkingList();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value = "exportManagement")
	public String exportManagement() {
		return "chart/exportManagement";
	};
	
	@RequestMapping(value = "importManagement")
	public String importManagement() {
		return "chart/importManagement";
	};

	@RequestMapping(value = "shipment2")
	public String shipment2() {
		return "chart/shipment2";
	};

	@RequestMapping(value = "StockHistory")
	public String StockHistory() {
		return "chart/StockHistory";
	};

	@RequestMapping(value = "stockStatus2")
	public String stockStatus2() {
		return "chart/stockStatus2";
	};

	@RequestMapping(value = "stockStatusDay2")
	public String stockStatusDay2() {
		return "chart/stockStatusDay2";
	};
	
	@RequestMapping(value = "getTransList")
	@ResponseBody
	public String getTransList(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getTransList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value = "getExportList")
	@ResponseBody
	public String getExportList(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getExportList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value = "getImportList")
	@ResponseBody
	public String getImportList(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getImportList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	@RequestMapping(value = "getShipList")
	@ResponseBody
	public String getShipList(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getShipList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	@RequestMapping(value = "getStockList")
	@ResponseBody
	public String getStockList(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getStockList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value = "getItem")
	@ResponseBody
	public String getItem(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getItem(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value = "stockTotalCntCheck")
	@ResponseBody
	public String stockTotalCntCheck(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.stockTotalCntCheck(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value = "getInhistory")
	@ResponseBody
	public String getInhistory(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getInhistory(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value = "toolLifeManager")
	public String toolLifeManager() {
		return "chart/toolLifeManager";
	};
	
	@RequestMapping(value = "getToolLifeList")
	@ResponseBody
	public String getToolLifeList(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getToolLifeList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "toolResetSave")
	@ResponseBody
	public String toolResetSave(ChartVo chartVo,String val) {
		String str = "";
		try {
			str = chartService.toolResetSave(chartVo,val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value = "toolLimitSave")
	@ResponseBody
	public String toolLimitSave(String val) {
		String str = "";
		try {
			str = chartService.toolLimitSave(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value = "checkPrdctStandardSave")
	@ResponseBody
	public String checkPrdctStandardSave(String val) {
		String str = "";
		try {
			str = chartService.checkPrdctStandardSave(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value = "getSelectList")
	@ResponseBody
	public String getSelectList(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getSelectList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "processCnt")
	@ResponseBody
	public String processCnt(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.processCnt(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value = "stockTattoo")
	public String stockTattoo() {
		return "chart/stockTattoo";
	};
	
	@RequestMapping(value = "getTattooList")
	@ResponseBody
	public String getTattooList(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getTattooList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value = "saveTattoo")
	@ResponseBody
	public String saveTattoo(String val) {
		String str = "";
		try {
			str = chartService.saveTattoo(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value = "getMachineInformation")
	@ResponseBody
	public String getMachineInformation(ChartVo chartVo) {
		String result = "";
		try {
			result = chartService.getMachineInformation(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};
	
	@RequestMapping(value = "dayFaulty")
	@ResponseBody
	public String dayFaulty(ChartVo chartVo) {
		String result = "";
		try {
			result = chartService.dayFaulty(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};
	
	@RequestMapping(value = "dashBoardMove")
	public String dashBoardMove() {
		return "chart/dashBoard_move";
	};

	@RequestMapping(value = "saveDvcMove")
	@ResponseBody
	public String saveDvcMove(String val) {
		String str = "";
		try {
			str = chartService.saveDvcMove(val);
			System.out.println("--------------------");
			System.out.println(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value = "instrumentManager")
	public String instrumentManager() {
		return "chart/instrumentManager";
	};
	
	@RequestMapping(value = "getInstrument")
	@ResponseBody
	public String getInstrument(ChartVo chartVo) {
		String str="";
		try {
			str = chartService.getInstrument(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	@RequestMapping(value = "saveInstrument")
	@ResponseBody
	public String saveInstrument(ChartVo chartVo) {
		String str="";
		try {
			str = chartService.saveInstrument(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value = "imgUpload")
	@ResponseBody
	public String imgUpload(MultipartHttpServletRequest mtfRequest,ChartVo chartVo) throws IOException {
		String str="";
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		Date date = new Date();
		String currentTime=dateFormat.format(date);
		
		String src = mtfRequest.getParameter("src");
		
		System.out.println("src value : " + src);
		MultipartFile mf = mtfRequest.getFile("file");
		
		File dir = new File(System.getProperty("catalina.base") + "/webapps/barcode/a");

		if (!dir.exists()) {
			dir.mkdirs();
		};
		
//		String path = "C:\\Users\\UNOMIC01\\Documents\\workspace-spring-tool-suite-4-4.0.0.RELEASE\\MINTON\\src\\main\\webapp\\resources\\image\\";
		String path = System.getProperty("catalina.base") + "/webapps/barcode/a/";
		
		String originFileName = mf.getOriginalFilename(); // 원본 파일 명
		System.out.println("originFileName : " + originFileName); 
		long fileSize = mf.getSize(); // 파일 사이즈
		
		String ext = originFileName.substring(originFileName.lastIndexOf("."), originFileName.length());
	
		String safeFile = path + currentTime + ext ;
		
		System.out.println("safeFile ::" + safeFile);
		System.out.println("currentTime ::" + currentTime);
		System.out.println(ext);
		chartVo.setImg(currentTime+ext);	//	저장되는 파일이름
		chartVo.setCode(mtfRequest.getParameter("code"));	//관리번호
		try {
			System.out.println("시도는함");
			// 관리대장 이미지 수정및추가
		    mf.transferTo(new File(safeFile));
			if(mtfRequest.getParameter("imgTy").equals("basic")) {
			    str = chartService.saveImgPath(chartVo);
			}else if(mtfRequest.getParameter("imgTy").equals("detail")) {
				chartVo.setIdx(mtfRequest.getParameter("idx"));
			    str = chartService.saveImgPathDetail(chartVo);
			}
		    System.out.println("성공함");
		} catch (IllegalStateException e) {
		    // TODO Auto-generated catch block
			str = "fail";
		    System.out.println("error1 :"+e);
		    e.printStackTrace();
		} catch (IOException e) {
		    // TODO Auto-generated catch block
			str = "fail";
		    System.out.println("error2 :"+e);
		    e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			str = "fail";
			System.out.println("error3 :"+e);
			e.printStackTrace();
		}
         
        return str;

	}
	
	@RequestMapping(value = "saveImgPath")
	@ResponseBody
	public String saveImgPath(ChartVo chartVo) throws IOException {
		String str="";
		try {
			chartService.saveImgPath(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
				
	}
	
	@RequestMapping(value = "detailInstrument")
	@ResponseBody
	public String detailInstrument(ChartVo chartVo) throws IOException {
		String str="";
		try {
			str = chartService.detailInstrument(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
				
	}
	
	@RequestMapping(value = "detailInstrumentSave")
	@ResponseBody
	public String detailInstrumentSave(String val) throws IOException {
		String str="";
		try {
			str = chartService.detailInstrumentSave(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
		
	}
	
	@RequestMapping(value = "getSelectItemList")
	@ResponseBody
	public String getSelectItemList(ChartVo chartVo) {
		String str="";
		try {
			str = chartService.getSelectItemList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value = "saveSelectItem")
	@ResponseBody
	public String saveSelectItem(String val) {
		String str="";
		try {
			str = chartService.saveSelectItem(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getRecordItemList")
	@ResponseBody
	public String getRecordItemList(ChartVo chartVo) {
		String str="";
		try {
			str = chartService.getRecordItemList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	@RequestMapping(value = "deleteManagementItem")
	@ResponseBody
	public String deleteManagementItem(ChartVo chartVo) {
		String str="";
		try {
			str = chartService.deleteManagementItem(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	@RequestMapping(value = "deleteRecordItem")
	@ResponseBody
	public String deleteRecordItem(ChartVo chartVo) {
		String str="";
		try {
			str = chartService.deleteRecordItem(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
};
