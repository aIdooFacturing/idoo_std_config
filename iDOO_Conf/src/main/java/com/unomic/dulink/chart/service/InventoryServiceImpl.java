package com.unomic.dulink.chart.service;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.unomic.dulink.chart.domain.ChartVo;
import com.unomic.dulink.chart.domain.InventoryVo;

@Service
@Repository
public class InventoryServiceImpl extends SqlSessionDaoSupport implements InventoryService {
	private final static String namespace = "com.unomic.dulink.inventory.";

	Map statusMap = new HashMap();
	int chartOrder = 0;
	@Inject
	SqlSession sqlSessionTemplate;

	@Override
	public String BertStckMaster(InventoryVo inventoryVo) throws Exception {
		SqlSession sql = getSqlSession();

		List<InventoryVo> dataList = sql.selectList(namespace + "BertStckMaster", inventoryVo);

		List list = new ArrayList<InventoryVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "utf-8"));
			map.put("item", dataList.get(i).getItem());

			map.put("id", dataList.get(i).getId());
			map.put("rcvCnt", dataList.get(i).getRcvCnt());
			map.put("iniohdCnt", dataList.get(i).getIniohdCnt());
			map.put("issCnt", dataList.get(i).getIssCnt());
			map.put("notiCnt", dataList.get(i).getNotiCnt());
			map.put("ohdCnt", dataList.get(i).getOhdCnt());
			map.put("atohdCnt", dataList.get(i).getOhdCnt());
			map.put("copy", dataList.get(i).getCopy());

			map.put("idR", dataList.get(i).getIdR());
			map.put("rcvCntR", dataList.get(i).getRcvCntR());
			map.put("iniohdCntR", dataList.get(i).getIniohdCntR());
			map.put("issCntR", dataList.get(i).getIssCntR());
			map.put("notiCntR", dataList.get(i).getNotiCntR());
			map.put("ohdCntR", dataList.get(i).getOhdCntR());
			map.put("atohdCntR", dataList.get(i).getOhdCntR());
			map.put("copyR", dataList.get(i).getCopyR());

			map.put("idM", dataList.get(i).getIdM());
			map.put("rcvCntM", dataList.get(i).getRcvCntM());
			map.put("iniohdCntM", dataList.get(i).getIniohdCntM());
			map.put("issCntM", dataList.get(i).getIssCntM());
			map.put("notiCntM", dataList.get(i).getNotiCntM());
			map.put("ohdCntM", dataList.get(i).getOhdCntM());
			map.put("atohdCntM", dataList.get(i).getOhdCntM());
			map.put("copyM", dataList.get(i).getCopyM());

			map.put("idC", dataList.get(i).getIdC());
			map.put("rcvCntC", dataList.get(i).getRcvCntC());
			map.put("iniohdCntC", dataList.get(i).getIniohdCntC());
			map.put("issCntC", dataList.get(i).getIssCntC());
			map.put("notiCntC", dataList.get(i).getNotiCntC());
			map.put("ohdCntC", dataList.get(i).getOhdCntC());
			map.put("atohdCntC", dataList.get(i).getOhdCntC());
			map.put("copyC", dataList.get(i).getCopyC());

			map.put("idP", dataList.get(i).getIdP());
			map.put("rcvCntP", dataList.get(i).getRcvCntP());
			map.put("iniohdCntP", dataList.get(i).getIniohdCntP());
			map.put("issCntP", dataList.get(i).getIssCntP());
			map.put("notiCntP", dataList.get(i).getNotiCntP());
			map.put("ohdCntP", dataList.get(i).getOhdCntP());
			map.put("atohdCntP", dataList.get(i).getOhdCntP());
			map.put("copyP", dataList.get(i).getCopyP());

			list.add(map);
		}
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String BertStckMasterSave(String val, String date) throws Exception {
		// TODO Auto-generated method stub
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<InventoryVo> list = new ArrayList<InventoryVo>();
		String str = "";

		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			InventoryVo inventoryVo = new InventoryVo();
			inventoryVo.setPrdNo(tempObj.get("prdNo").toString());
			inventoryVo.setItem(tempObj.get("item").toString());
			inventoryVo.setDate(date);
			inventoryVo.setIniohdCnt(Integer.parseInt(tempObj.get("iniohdCnt").toString()));
			inventoryVo.setRcvCnt(Integer.parseInt(tempObj.get("rcvCnt").toString()));
			inventoryVo.setNotiCnt(Integer.parseInt(tempObj.get("notiCnt").toString()));
			inventoryVo.setIssCnt(Integer.parseInt(tempObj.get("issCnt").toString()));
			inventoryVo.setOhdCnt(Integer.parseInt(tempObj.get("ohdCnt").toString()));

			inventoryVo.setIniohdCntL(Integer.parseInt(tempObj.get("iniohdCntL").toString()));
			inventoryVo.setRcvCntL(Integer.parseInt(tempObj.get("rcvCntL").toString()));
			inventoryVo.setNotiCntL(Integer.parseInt(tempObj.get("notiCntL").toString()));
			inventoryVo.setIssCntL(Integer.parseInt(tempObj.get("issCntL").toString()));
			inventoryVo.setOhdCntL(Integer.parseInt(tempObj.get("ohdCntL").toString()));
			
			inventoryVo.setIniohdCntR(Integer.parseInt(tempObj.get("iniohdCntR").toString()));
			inventoryVo.setRcvCntR(Integer.parseInt(tempObj.get("rcvCntR").toString()));
			inventoryVo.setNotiCntR(Integer.parseInt(tempObj.get("notiCntR").toString()));
			inventoryVo.setIssCntR(Integer.parseInt(tempObj.get("issCntR").toString()));
			inventoryVo.setOhdCntR(Integer.parseInt(tempObj.get("ohdCntR").toString()));
			
			inventoryVo.setIniohdCntM(Integer.parseInt(tempObj.get("iniohdCntM").toString()));
			inventoryVo.setRcvCntM(Integer.parseInt(tempObj.get("rcvCntM").toString()));
			inventoryVo.setNotiCntM(Integer.parseInt(tempObj.get("notiCntM").toString()));
			inventoryVo.setIssCntM(Integer.parseInt(tempObj.get("issCntM").toString()));
			inventoryVo.setOhdCntM(Integer.parseInt(tempObj.get("ohdCntM").toString()));
			
			inventoryVo.setIniohdCntC(Integer.parseInt(tempObj.get("iniohdCntC").toString()));
			inventoryVo.setRcvCntC(Integer.parseInt(tempObj.get("rcvCntC").toString()));
			inventoryVo.setNotiCntC(Integer.parseInt(tempObj.get("notiCntC").toString()));
			inventoryVo.setIssCntC(Integer.parseInt(tempObj.get("issCntC").toString()));
			inventoryVo.setOhdCntC(Integer.parseInt(tempObj.get("ohdCntC").toString()));
			
			inventoryVo.setIniohdCntP(Integer.parseInt(tempObj.get("iniohdCntP").toString()));
			inventoryVo.setRcvCntP(Integer.parseInt(tempObj.get("rcvCntP").toString()));
			inventoryVo.setNotiCntP(Integer.parseInt(tempObj.get("notiCntP").toString()));
			inventoryVo.setIssCntP(Integer.parseInt(tempObj.get("issCntP").toString()));
			inventoryVo.setOhdCntP(Integer.parseInt(tempObj.get("ohdCntP").toString()));
			
			list.add(inventoryVo);
		}

		try {
			int size = (int) sqlSessionTemplate.selectOne(namespace + "checkStockData", list.get(0).getDate());
			if(size>0) {
				sqlSessionTemplate.delete(namespace+"deleteStockData", list.get(0).getDate());
			}
			sqlSessionTemplate.insert(namespace + "BertStckMasterSave", list);
			str="success";
		} catch (Exception e) {
			str="failed";
			logger.error(e.getMessage());
			e.printStackTrace();
			// TODO: handle exception
		}

		return str;
	}

	@Override
	@Transactional(rollbackFor=Exception.class)
	public String finishStckMaster(String val, String date) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("Date is : " + date);
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<InventoryVo> list = new ArrayList<InventoryVo>();
		String str = "";

		for (int i = 0; i < jsonArray.size(); i++) {
			System.out.println("Error Occur Check #1" );
			JSONObject tempObj = (JSONObject) jsonArray.get(i);
			InventoryVo stckVo = new InventoryVo();
			stckVo.setPrdNo(tempObj.get("prdNo").toString());
			stckVo.setItem(tempObj.get("item").toString());
			stckVo.setDate(date);
			stckVo.setIniohdCnt(Integer.parseInt(tempObj.get("iniohdCnt").toString()));
			stckVo.setRcvCnt(Integer.parseInt(tempObj.get("rcvCnt").toString()));
			stckVo.setNotiCnt(Integer.parseInt(tempObj.get("notiCnt").toString()));
			stckVo.setIssCnt(Integer.parseInt(tempObj.get("issCnt").toString()));
			stckVo.setOhdCnt(Integer.parseInt(tempObj.get("ohdCnt").toString()));
			
			stckVo.setIniohdCntR(Integer.parseInt(tempObj.get("iniohdCntR").toString()));
			stckVo.setRcvCntR(Integer.parseInt(tempObj.get("rcvCntR").toString()));
			stckVo.setNotiCntR(Integer.parseInt(tempObj.get("notiCntR").toString()));
			stckVo.setIssCntR(Integer.parseInt(tempObj.get("issCntR").toString()));
			stckVo.setOhdCntR(Integer.parseInt(tempObj.get("ohdCntR").toString()));
			
			stckVo.setIniohdCntM(Integer.parseInt(tempObj.get("iniohdCntM").toString()));
			stckVo.setRcvCntM(Integer.parseInt(tempObj.get("rcvCntM").toString()));
			stckVo.setNotiCntM(Integer.parseInt(tempObj.get("notiCntM").toString()));
			stckVo.setIssCntM(Integer.parseInt(tempObj.get("issCntM").toString()));
			stckVo.setOhdCntM(Integer.parseInt(tempObj.get("ohdCntM").toString()));
			
			stckVo.setIniohdCntC(Integer.parseInt(tempObj.get("iniohdCntC").toString()));
			stckVo.setRcvCntC(Integer.parseInt(tempObj.get("rcvCntC").toString()));
			stckVo.setNotiCntC(Integer.parseInt(tempObj.get("notiCntC").toString()));
			stckVo.setIssCntC(Integer.parseInt(tempObj.get("issCntC").toString()));
			stckVo.setOhdCntC(Integer.parseInt(tempObj.get("ohdCntC").toString()));
			
			stckVo.setIniohdCntP(Integer.parseInt(tempObj.get("iniohdCntP").toString()));
			stckVo.setRcvCntP(Integer.parseInt(tempObj.get("rcvCntP").toString()));
			stckVo.setNotiCntP(Integer.parseInt(tempObj.get("notiCntP").toString()));
			stckVo.setIssCntP(Integer.parseInt(tempObj.get("issCntP").toString()));
			stckVo.setOhdCntP(Integer.parseInt(tempObj.get("ohdCntP").toString()));
			
			stckVo.setOhdCnt(stckVo.getIniohdCnt()+stckVo.getRcvCnt()-stckVo.getNotiCnt()-stckVo.getIssCnt());
			stckVo.setOhdCntR(stckVo.getIniohdCntR()+stckVo.getRcvCntR());
			stckVo.setOhdCntM(stckVo.getIniohdCntM()+stckVo.getRcvCntM()-stckVo.getNotiCntM()-stckVo.getIssCntM());
			stckVo.setOhdCntC(stckVo.getIniohdCntC()+stckVo.getRcvCntC()-stckVo.getNotiCntC()-stckVo.getIssCntC());
			stckVo.setOhdCntP(stckVo.getIniohdCntP()+stckVo.getRcvCntP()-stckVo.getNotiCntP()-stckVo.getIssCntP());
			System.out.println("Error Occur Check #2");
			list.add(stckVo);
			
		}
		logger.info("리스트의 크기는 : " + list.size());
		try {
			int size = (int) sqlSessionTemplate.selectOne(namespace + "checkStockData", list.get(0).getDate());
			if(size>0) {
				sqlSessionTemplate.delete(namespace+"deleteStockData", list.get(0).getDate());
			}
			sqlSessionTemplate.insert(namespace + "finishStckMaster", list);
			str="success";
		} catch (Exception e) {
			str="failed";
			logger.error(e.getMessage());
			e.printStackTrace();
			// TODO: handle exception
		}

		return str;
	}

	@Override
	@Transactional
	public String OprNmList(InventoryVo vo) throws Exception {
		
		List<InventoryVo> dataList = sqlSessionTemplate.selectList(namespace + "OprNmList", vo);

		List list = new ArrayList<InventoryVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("dvcName", URLEncoder.encode(dataList.get(i).getDvcName(), "utf-8"));
			
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("oprNo", dataList.get(i).getOprNm());
			list.add(map);
		}
	
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String moveStock(String val) throws Exception {
		// TODO Auto-generated method stub
				JSONParser jsonParser = new JSONParser();
				JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
				JSONArray jsonArray = (JSONArray) jsonObj.get("val");

				SqlSession sql = getSqlSession();

				List<InventoryVo> list = new ArrayList<InventoryVo>();
				String str = "";

				for (int i = 0; i < jsonArray.size(); i++) {
					JSONObject tempObj = (JSONObject) jsonArray.get(i);

					InventoryVo inventoryVo = new InventoryVo();
					inventoryVo.setPrdNo(tempObj.get("prdNo").toString());
					inventoryVo.setDvcName(tempObj.get("dvcName").toString());
					inventoryVo.setBeforeProj(tempObj.get("afterProj").toString());
					inventoryVo.setNewLotNo(tempObj.get("newLotNo").toString());
					inventoryVo.setCnt(Integer.parseInt(tempObj.get("cnt").toString()));
					list.add(inventoryVo);
				}

				try {
					sql.insert(namespace + "moveStock", list);
					str="success";
				} catch (Exception e) {
					str="failed";
					logger.error(e.getMessage());
					e.printStackTrace();
					// TODO: handle exception
				}

				return str;
	}

	@Override
	@Transactional(rollbackFor=Exception.class)
	public String moveStockWithTrans(String val) throws Exception {
		
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		SqlSession sql = getSqlSession();

		List<InventoryVo> list = new ArrayList<InventoryVo>();
		List<InventoryVo> outerInsert = new ArrayList<InventoryVo>();//외주추가리스트
		List<InventoryVo> outerUpdate = new ArrayList<InventoryVo>();//외주변경리스트
		String str = "";

		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);
			InventoryVo inventoryVo = new InventoryVo();
			inventoryVo.setPrdNo(tempObj.get("prdNo").toString());
			inventoryVo.setDvcName(tempObj.get("dvcName").toString());
			inventoryVo.setOprNm(tempObj.get("oprNm").toString());
			inventoryVo.setAfterProj(tempObj.get("afterProj").toString());	// 이동할 공정
			inventoryVo.setCnt(Integer.parseInt(tempObj.get("cnt").toString()));
			inventoryVo.setLotNo(tempObj.get("lotNo").toString());
			inventoryVo.setGroupLotNo(tempObj.get("groupLotNo").toString());
			inventoryVo.setDatetime(tempObj.get("datetime").toString()); 
			inventoryVo.setId(tempObj.get("idx").toString()); 
			inventoryVo.setDeliveryNo(tempObj.get("deliveryNo").toString());
			
			InventoryVo addVo = new InventoryVo();
			addVo = (InventoryVo) sql.selectOne(namespace + "checkStock1", inventoryVo);
			inventoryVo.setPrce(addVo.getPrce());
			
			
			list.add(inventoryVo);

		}
		int size = list.size();
		logger.info("===================================" + size + "==========================================");
		List<String> lotNoList = makeRandomLotNo(size);
		
		// 새로운 로트번호 랜덤 생선
		for(int i=0; i<size ;i++) {
			logger.info("===================================" + lotNoList.get(i) + "==========================================");
			if(list.get(i).getOprNm().equals("0000")) {
				list.get(i).setNewLotNo("L"+lotNoList.get(i));
			}else if(list.get(i).getOprNm().equals("0005")) {
				list.get(i).setNewLotNo("R"+lotNoList.get(i));
			}
			else if(list.get(i).getOprNm().equals("0010")) {
				list.get(i).setNewLotNo("M"+lotNoList.get(i));
			}
			else if(list.get(i).getOprNm().equals("0020")) {
				list.get(i).setNewLotNo("C"+lotNoList.get(i));
			}
			else if(list.get(i).getOprNm().equals("0030")) {
				list.get(i).setNewLotNo("P"+lotNoList.get(i));
			}
		}
		
		try {
			sql.update(namespace + "insertStock11", list);	// 트렌스 재고 변경
			sql.insert(namespace + "insertStock1", list);	// 트렌스 추가
//			System.out.println("업데이트 구문");
			sql.insert(namespace + "updateStock11", list);	// 총재고 기존재고 -
			sql.insert(namespace + "updateStock1", list);	// 총재고 이동재고 +
			sql.insert(namespace + "updateProcessStock1", list);	// 공정 테이블이동재고 +
			str="success";
		} catch (Exception e) {
			str="failed";
			logger.error(e.getMessage());
			e.printStackTrace();
			// TODO: handle exception
		}

		return str;
		
	}
	@Override
	@Transactional(rollbackFor=Exception.class)
	public String stockMoveTrans(String val) throws Exception {
		
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");
		
		SqlSession sql = getSqlSession();
		
		List<InventoryVo> list = new ArrayList<InventoryVo>();
		List<InventoryVo> prolist = new ArrayList<InventoryVo>();	//공정이동할 리스트 -
		List<InventoryVo> propluslist = new ArrayList<InventoryVo>();	//공정이동할 리스트 +
		List<InventoryVo> shiplist = new ArrayList<InventoryVo>();

		String str = "";
		
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);
			InventoryVo inventoryVo = new InventoryVo();
			inventoryVo.setPrdNo(tempObj.get("prdNo").toString());
			inventoryVo.setBeforeProj(tempObj.get("proj").toString());			
			inventoryVo.setLotNo(tempObj.get("lotNo").toString());
//			inventoryVo.setBarcode(tempObj.get("barcode").toString());
//			inventoryVo.setAfterProj(tempObj.get("0005").toString());	
			inventoryVo.setAfterProj(tempObj.get("afterProj").toString());	// 이동할 공정
			inventoryVo.setPlan(tempObj.get("trans").toString());
			inventoryVo.setItem(tempObj.get("item").toString());	
			inventoryVo.setPrce(Integer.parseInt(tempObj.get("prce").toString()));
			inventoryVo.setSendCnt(Integer.parseInt(tempObj.get("sendCnt").toString()));
//			inventoryVo.setSendCnt(Integer.parseInt(tempObj.get("sendCnt").toString()));
			
			if(tempObj.get("proj").equals("0005")) {
				System.out.println("난공정 - 이다");
				prolist.add(inventoryVo);
				
//				inventoryVo.setAfterProj("0005");
			}
			if( tempObj.get("afterProj").equals("0010") || tempObj.get("afterProj").equals("0020") || tempObj.get("afterProj").equals("0030")) {
				System.out.println("난공정 + 이다");
				propluslist.add(inventoryVo);
			}
			
			if(tempObj.get("afterProj").toString().equals("0090")) {	//출하 취소 담기
				shiplist.add(inventoryVo);
			}else {	
				list.add(inventoryVo);
			}
			
		}
		try {
			if(list.size()>0) {
				sql.insert(namespace + "stockInsertTrans" ,list);	//  트랜스 추가
				sql.insert(namespace + "stockUpdateStock" ,list);  //	총재고에서 현재창고(공정창고) 재고 마이너스
				sql.insert(namespace + "stockInsertStock" ,list);	//  총재고 이동창고 추가
				
				sql.delete(namespace + "stockDeleteStock" );
			}
			if(shiplist.size()>0) {
				sql.insert(namespace + "stockShipInsertTrans" ,shiplist);	// 출하 트랜스 추가
				sql.insert(namespace + "stockUpdateStock" ,shiplist);  //	총재고에서 현재창고(공정창고) 재고 마이너스
				sql.insert(namespace + "stockInsertStock" ,shiplist);	//  총재고 이동창고 추가
			}
			if(prolist.size()>0) {
				sql.insert(namespace + "stockUpdateProcess" ,prolist);	//	공정창고 수량 -시키기 ->현재창고가 0005(자재창고일때만)
			}
			if(propluslist.size()>0) {
				sql.insert(namespace + "stockPlusProcess" ,propluslist);	//	공정창고 수량 + 시키기 ->이동창고가 0010,0020,0030 (일때만)
			}
//			sql.insert(namespace + "importUpdateProcess" ,list);	//공정창고 수량 -시키기
			
			str="success";
		} catch (Exception e) {
			str="failed";
			logger.error(e.getMessage());
			e.printStackTrace();
			// TODO: handle exception
		}
		
		return str;
		
	}
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public String exportMoveTrans(String val) throws Exception {
		
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		SqlSession sql = getSqlSession();

		List<InventoryVo> list = new ArrayList<InventoryVo>();
		List<InventoryVo> outerInsert = new ArrayList<InventoryVo>();//외주추가리스트
		List<InventoryVo> outerUpdate = new ArrayList<InventoryVo>();//외주변경리스트
		String str = "";

		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);
			InventoryVo inventoryVo = new InventoryVo();
			
//타각			inventoryVo.setIdx(Integer.parseInt(tempObj.get("idx").toString()));
			
			inventoryVo.setPrdNo(tempObj.get("prdNo").toString());
			inventoryVo.setVndNo(tempObj.get("vndNo").toString());
			inventoryVo.setBeforeProj(tempObj.get("beforeProj").toString());
//			inventoryVo.setAfterProj(tempObj.get("0060").toString());	// 이동할 공정
			inventoryVo.setAfterProj(tempObj.get("afterProj").toString());	// 이동할 공정
			inventoryVo.setPlan("반출");
			inventoryVo.setItem(tempObj.get("item").toString());	// 이동할 공정
			inventoryVo.setCnt(Integer.parseInt(tempObj.get("cnt").toString()));
			inventoryVo.setDeliveryNo(tempObj.get("deliveryNo").toString());
//			inventoryVo.setSendCnt(Integer.parseInt(tempObj.get("cnt").toString()));
			inventoryVo.setSendCnt(Integer.parseInt(tempObj.get("sendCnt").toString()));
			inventoryVo.setDate(tempObj.get("date").toString());
			inventoryVo.setNewLotNo(tempObj.get("barcode").toString());
			inventoryVo.setRemarks(tempObj.get("remarks").toString());
			//반출 비고 입력 작업진행중.. 여기까지완료? url 경로 주석처리해놓음 현재
			
			String before=(String) sql.selectOne(namespace + "beforeChk" ,inventoryVo);	//  총재고 이동창고 추가
			if(before==null) {
				inventoryVo.setBeforeProj("0005");
			}else if(before!=null) {
				inventoryVo.setBeforeProj(before);
			}
			list.add(inventoryVo);

		}
		
		try { 
			
//타각관리			sql.update(namespace + "exportUpdateTrans" ,list);	//	타각 재고 - 시키기
			sql.insert(namespace + "exportInsertTrans" ,list);	//  트랜스 추가
			sql.insert(namespace + "exportUpdateStock" ,list);  //	현재창고 재고 마이너스	총재고
			sql.insert(namespace + "exportInsertStock" ,list);	//  총재고 이동창고 추가	총재고
			
			sql.insert(namespace + "exportUpdateProcess" ,list);//	공정창고 현재창고 재고 마이너스	공정창고
			sql.insert(namespace + "exportUpdateNextProcess" ,list);//	공정창고 현재창고 재고 마이너스	공정창고

/*			sql.update(namespace + "insertStock11", list);	// 트렌스 재고 변경
			sql.insert(namespace + "insertStock1", list);	// 트렌스 추가
			System.out.println("업데이트 구문");
			sql.update(namespace + "updateStock11", list);	// 기존재고 -
			sql.insert(namespace + "updateStock1", list);	// 이동재고 +
*/			
			str="success";
		} catch (Exception e) {
			str="failed";
			logger.error(e.getMessage());
			e.printStackTrace();
			// TODO: handle exception
		}

		return str;
		
	}
	
	//반입관리 바코드로 처리할경우
	@Override
	@Transactional(rollbackFor=Exception.class)
	public String BarcodeImportMoveTrans(InventoryVo inventoryVo) throws Exception {
		
		SqlSession sql = getSqlSession();
		
		String str = "";
		
		
		try {
			
/*			System.out.println("zzzz");
			System.out.println(inventoryVo.getSendCnt());
			System.out.println(inventoryVo.getBarcode());
			System.out.println(inventoryVo.getPrdNo());
			
			System.out.println("이전공정 :" + inventoryVo.getBeforeProj() + "다음공정 : " + inventoryVo.getAfterProj());
			System.out.println("품번 :" + inventoryVo.getPrdNo() + "차종 :"+inventoryVo.getItem() + "불량수량 :" + inventoryVo.getFault());
		*/	
			sql.insert(namespace + "BarcodeimportInsertTrans" ,inventoryVo);	//  트랜스 추가 
			sql.insert(namespace + "BarcodeimportUpdateStock" ,inventoryVo);  //	총재고에서 현재창고 재고 마이너스
			sql.insert(namespace + "BarcodeimportInsertStock" ,inventoryVo);	//  총재고 이동창고 추가
			sql.insert(namespace + "BarcodeimportUpdateProcess" ,inventoryVo);	//공정창고 수량 -

			str="success";
		} catch (Exception e) {
			str="failed";
			logger.error(e.getMessage());
			e.printStackTrace();
			// TODO: handle exception
		}
		
		return str;
		
	}
	
	//반입관리 체크항목 선택후 저장 버튼 클릭시
	@Override
	@Transactional(rollbackFor=Exception.class)
	public String importMoveTrans(String val) throws Exception {
		
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");
		
		SqlSession sql = getSqlSession();
		
		List<InventoryVo> list = new ArrayList<InventoryVo>();
		String str = "";
		
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);
			InventoryVo inventoryVo = new InventoryVo();
			inventoryVo.setPrdNo(tempObj.get("prdNo").toString());
			inventoryVo.setVndNo(tempObj.get("vndNo").toString());
			inventoryVo.setBeforeProj(tempObj.get("proj").toString());
			inventoryVo.setBarcode(tempObj.get("barcode").toString());
//			inventoryVo.setAfterProj(tempObj.get("0005").toString());	
			if(inventoryVo.getBeforeProj().equals(tempObj.get("afterProj").toString())) {
				inventoryVo.setAfterProj("0090");	// 이동할 공정
			}else {
				inventoryVo.setAfterProj(tempObj.get("afterProj").toString());	// 이동할 공정
			}
			inventoryVo.setPlan("반입");
			inventoryVo.setItem(tempObj.get("item").toString());	
			inventoryVo.setCnt(Integer.parseInt(tempObj.get("cnt").toString()));
			inventoryVo.setSendCnt(Integer.parseInt(tempObj.get("sendCnt").toString()));
			inventoryVo.setIdx(Integer.parseInt(tempObj.get("idx").toString()));
			inventoryVo.setFault(Integer.parseInt(tempObj.get("fault").toString()));
			inventoryVo.setDeliveryNo(tempObj.get("deliveryNo").toString());	
			
			list.add(inventoryVo);
			
/*			System.out.println("이동전 공정 ::" + inventoryVo.getBeforeProj());
			System.out.println("이동할 공정 ::" + inventoryVo.getAfterProj());
			System.out.println("총수량 ::" + inventoryVo.getCnt() + "불량 수량 ::" + inventoryVo.getFault());
			System.out.println("이동 수량 ::" + inventoryVo.getSendCnt() + "불량 수량 ::" + inventoryVo.getFault());*/
		}
		
		try {
			
			
			sql.insert(namespace + "importInsertTrans" ,list);	//  트랜스 추가
			sql.insert(namespace + "importUpdateStock" ,list);  //	총재고에서 현재창고 재고 마이너스
			sql.insert(namespace + "importInsertStock" ,list);	//  총재고 이동창고 추가
			sql.insert(namespace + "importUpdateProcess" ,list);	//공정창고 수량 -
			/*sql.update(namespace + "importUpdateTrans" ,list);  //	트랜스에서 현재창고 재고 마이너스
			
*/			// <== 181030 최종수정
			
			/*			sql.update(namespace + "insertStock11", list);	// 트렌스 재고 변경
			sql.insert(namespace + "insertStock1", list);	// 트렌스 추가
			System.out.println("업데이트 구문");
			sql.update(namespace + "updateStock11", list);	// 기존재고 -
			sql.insert(namespace + "updateStock1", list);	// 이동재고 +
			 */			
			str="success";
		} catch (Exception e) {
			str="failed";
			logger.error(e.getMessage());
			e.printStackTrace();
			// TODO: handle exception
		}
		
		return str;
		
	}
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public String shipMoveTrans(String val) throws Exception {
		
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");
		
		SqlSession sql = getSqlSession();
		
		List<InventoryVo> list = new ArrayList<InventoryVo>();
		String str = "";
		
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);
			InventoryVo inventoryVo = new InventoryVo();
			inventoryVo.setPrdNo(tempObj.get("prdNo").toString());
			inventoryVo.setVndNo(tempObj.get("vndNo").toString());
			inventoryVo.setBeforeProj("0005");			
			inventoryVo.setLotNo(tempObj.get("shipLot").toString());
			
//			inventoryVo.setBarcode(tempObj.get("barcode").toString());
//			inventoryVo.setAfterProj(tempObj.get("0005").toString());	
			inventoryVo.setAfterProj(tempObj.get("proj").toString());	// 이동할 공정
			inventoryVo.setPlan("완성창고");
			inventoryVo.setItem(tempObj.get("item").toString());	
			inventoryVo.setPrce(Integer.parseInt(tempObj.get("prce").toString()));
			inventoryVo.setCnt(Integer.parseInt(tempObj.get("cnt").toString()));
//			inventoryVo.setSendCnt(Integer.parseInt(tempObj.get("sendCnt").toString()));
			list.add(inventoryVo);
			
		}
		
		try {
			sql.insert(namespace + "shipInsertTrans" ,list);	//  트랜스 추가
			sql.insert(namespace + "shipsuccessInsert" ,list);	//	완성창고 테이블에 추가
			sql.insert(namespace + "shipUpdateStock" ,list);  //	총재고에서 현재창고(공정창고) 재고 마이너스
			sql.insert(namespace + "shipUpdateProcess" ,list);	//공정창고 수량 -시키기
//			sql.insert(namespace + "shipInsertStock" ,list);	//  총재고 이동창고 추가 
			
			
			
			/*			sql.update(namespace + "insertStock11", list);	// 트렌스 재고 변경
			sql.insert(namespace + "insertStock1", list);	// 트렌스 추가
			System.out.println("업데이트 구문");
			sql.update(namespace + "updateStock11", list);	// 기존재고 -
			sql.insert(namespace + "updateStock1", list);	// 이동재고 +
			 */			
			str="success";
		} catch (Exception e) {
			str="failed";
			logger.error(e.getMessage());
			e.printStackTrace();
			// TODO: handle exception
		}
		
		return str;
		
	}
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public String finishMoveTrans(String val) throws Exception {
		
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");
		
		SqlSession sql = getSqlSession();
		
		List<InventoryVo> list = new ArrayList<InventoryVo>();
		String str = "";
		
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);
			InventoryVo inventoryVo = new InventoryVo();
			inventoryVo.setPrdNo(tempObj.get("prdNo").toString());
			inventoryVo.setVndNo("0");
			inventoryVo.setBeforeProj(tempObj.get("proj").toString());			
			inventoryVo.setLotNo(tempObj.get("shipLot").toString());
			
//			inventoryVo.setBarcode(tempObj.get("barcode").toString());
//			inventoryVo.setAfterProj(tempObj.get("0005").toString());	
			inventoryVo.setAfterProj("9999");	// 이동할 공정
			inventoryVo.setPlan("출하");
			inventoryVo.setItem(tempObj.get("item").toString());	
			inventoryVo.setPrce(Integer.parseInt(tempObj.get("prce").toString()));
//			inventoryVo.setCnt(Integer.parseInt(tempObj.get("cnt").toString()));
			inventoryVo.setSendCnt(Integer.parseInt(tempObj.get("sendCnt").toString()));
//			inventoryVo.setIdx(Integer.parseInt(tempObj.get("idx").toString()));
//			inventoryVo.setSendCnt(Integer.parseInt(tempObj.get("sendCnt").toString()));
			list.add(inventoryVo);
			
		}
		
		try {
			sql.insert(namespace + "finishInsertTrans" ,list);	//  트랜스 추가
			sql.insert(namespace + "finishSuccessMinus" ,list);	//	완성테이블 에서 수량빼기
//			sql.update(namespace + "finishUpdateTrans" ,list);  //	트랜스에서 현재창고 재고 마이너스
//			sql.update(namespace + "finishUpdateStock" ,list);  //	총재고에서 현재창고(공정창고) 재고 마이너스

//			sql.insert(namespace + "finishInsertStock" ,list);	//  총재고 이동창고 추가 마무리 필요없음
//			sql.insert(namespace + "finishUpdateProcess" ,list);	//공정창고 수량 -시키기 완성창고로 갈때 빼므로 안해도될듯
			
			/*			sql.update(namespace + "insertStock11", list);	// 트렌스 재고 변경
			sql.insert(namespace + "insertStock1", list);	// 트렌스 추가
			System.out.println("업데이트 구문");
			sql.update(namespace + "updateStock11", list);	// 기존재고 -
			sql.insert(namespace + "updateStock1", list);	// 이동재고 +
			 */			
			str="success";
		} catch (Exception e) {
			str="failed";
			logger.error(e.getMessage());
			e.printStackTrace();
			// TODO: handle exception
		}
		
		return str;
		
	}
	
	/*
	 * 로트번호 생성을 위한 랜덤 문자열 생성 함수.
	 * */
	public List<String> makeRandomLotNo(int size) {
		Random rnd = new Random();
		
		List<String> lotList = new ArrayList<String>();
		for(int j=0;j<size;j++) {
			StringBuffer temp = new StringBuffer();
			for (int i = 0; i < 2; i++) {
			    temp.append((char) ((int) (rnd.nextInt(26)) + 65));
			}
			for (int i = 0; i < 2; i++) {
				temp.append((rnd.nextInt(10)));
			}
			lotList.add(temp.toString());
		}
		
		
		return lotList;
	}

	@Override
	public String getStockStatusDay(InventoryVo inventoryVo) throws Exception {
		SqlSession sql = getSqlSession();

		System.out.println("입력날짜;; "+ inventoryVo.getDate());
		List<InventoryVo> dataList = sql.selectList(namespace + "getStockStatusDay", inventoryVo);

		List list = new ArrayList<InventoryVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "utf-8"));
			map.put("item", dataList.get(i).getItem());
			map.put("chk", dataList.get(i).getChk());
			map.put("minOpr", dataList.get(i).getMinOpr());
			
			map.put("id", dataList.get(i).getId());
			map.put("rcvCnt", dataList.get(i).getRcvCnt());
			map.put("iniohdCnt", dataList.get(i).getIniohdCnt());
			map.put("issCnt", dataList.get(i).getIssCnt());
			map.put("notiCnt", dataList.get(i).getNotiCnt());
			map.put("atohdCnt", dataList.get(i).getOhdCnt());
			map.put("copy", dataList.get(i).getCopy());

			map.put("idL", dataList.get(i).getIdL());
			map.put("rcvCntL", dataList.get(i).getRcvCntL());
			map.put("iniohdCntL", dataList.get(i).getIniohdCntL());
			map.put("issCntL", dataList.get(i).getIssCntL());
			map.put("notiCntL", dataList.get(i).getNotiCntL());
			map.put("atohdCntL", dataList.get(i).getOhdCntL());
			map.put("copyL", dataList.get(i).getCopyL());

			map.put("idR", dataList.get(i).getIdR());
			map.put("rcvCntR", dataList.get(i).getRcvCntR());
			map.put("iniohdCntR", dataList.get(i).getIniohdCntR());
			map.put("issCntR", dataList.get(i).getIssCntR());
			map.put("notiCntR", dataList.get(i).getNotiCntR());
			map.put("atohdCntR", dataList.get(i).getOhdCntR());
			map.put("copyR", dataList.get(i).getCopyR());

			map.put("idM", dataList.get(i).getIdM());
			map.put("rcvCntM", dataList.get(i).getRcvCntM());
			map.put("iniohdCntM", dataList.get(i).getIniohdCntM());
			map.put("issCntM", dataList.get(i).getIssCntM());
			map.put("notiCntM", dataList.get(i).getNotiCntM());
			map.put("atohdCntM", dataList.get(i).getOhdCntM());
			map.put("copyM", dataList.get(i).getCopyM());

			map.put("idC", dataList.get(i).getIdC());
			map.put("rcvCntC", dataList.get(i).getRcvCntC());
			map.put("iniohdCntC", dataList.get(i).getIniohdCntC());
			map.put("issCntC", dataList.get(i).getIssCntC());
			map.put("notiCntC", dataList.get(i).getNotiCntC());
			map.put("atohdCntC", dataList.get(i).getOhdCntC());
			map.put("copyC", dataList.get(i).getCopyC());

			map.put("idP", dataList.get(i).getIdP());
			map.put("rcvCntP", dataList.get(i).getRcvCntP());
			map.put("iniohdCntP", dataList.get(i).getIniohdCntP());
			map.put("issCntP", dataList.get(i).getIssCntP());
			map.put("notiCntP", dataList.get(i).getNotiCntP());
			map.put("atohdCntP", dataList.get(i).getOhdCntP());
			map.put("copyP", dataList.get(i).getCopyP());

			list.add(map);
		}
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getfinishedStockStatus(InventoryVo inventoryVo) throws Exception {
		SqlSession sql = getSqlSession();

		List<InventoryVo> dataList = sql.selectList(namespace + "getfinishedStockStatus", inventoryVo);

		List list = new ArrayList<InventoryVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "utf-8"));
			map.put("ex", dataList.get(i).getEx());
			map.put("item", dataList.get(i).getItem());
			map.put("date", dataList.get(i).getDate());
			map.put("chk", dataList.get(i).getChk());
			map.put("minOpr", dataList.get(i).getMinOpr());
			map.put("maxOpr", dataList.get(i).getMaxOpr());

			map.put("sumIn", dataList.get(i).getSumIn());
			map.put("sumInL", dataList.get(i).getSumInL());
			map.put("sumInR", dataList.get(i).getSumInR());
			map.put("sumInM", dataList.get(i).getSumInM());
			map.put("sumInC", dataList.get(i).getSumInC());
			map.put("sumInF", dataList.get(i).getSumInF());
			map.put("sumInP", dataList.get(i).getSumInP());

			map.put("sumOutP", dataList.get(i).getSumOutP());
			
			
			
			map.put("rcvCnt", dataList.get(i).getRcvCnt());
			map.put("iniohdCnt", dataList.get(i).getIniohdCnt());
			map.put("issCnt", dataList.get(i).getIssCnt());
			map.put("notiCnt", dataList.get(i).getNotiCnt());
			map.put("ohdCnt", dataList.get(i).getOhdCnt());

			map.put("rcvCntL", dataList.get(i).getRcvCntL());
			map.put("iniohdCntL", dataList.get(i).getIniohdCntL());
			map.put("issCntL", dataList.get(i).getIssCntL());
			map.put("notiCntL", dataList.get(i).getNotiCntL());
			map.put("atohdCntL", dataList.get(i).getOhdCntL());
			map.put("copyL", dataList.get(i).getCopyL());
			
			map.put("rcvCntR", dataList.get(i).getRcvCntR());
			map.put("iniohdCntR", dataList.get(i).getIniohdCntR());
			map.put("issCntR", dataList.get(i).getIssCntR());
			map.put("notiCntR", dataList.get(i).getNotiCntR());
			map.put("ohdCntR", dataList.get(i).getOhdCntR());

			map.put("rcvCntM", dataList.get(i).getRcvCntM());
			map.put("iniohdCntM", dataList.get(i).getIniohdCntM());
			map.put("issCntM", dataList.get(i).getIssCntM());
			map.put("notiCntM", dataList.get(i).getNotiCntM());
			map.put("ohdCntM", dataList.get(i).getOhdCntM());

			map.put("rcvCntC", dataList.get(i).getRcvCntC());
			map.put("iniohdCntC", dataList.get(i).getIniohdCntC());
			map.put("issCntC", dataList.get(i).getIssCntC());
			map.put("notiCntC", dataList.get(i).getNotiCntC());
			map.put("ohdCntC", dataList.get(i).getOhdCntC());

			map.put("rcvCntF", dataList.get(i).getRcvCntF());
			map.put("iniohdCntF", dataList.get(i).getIniohdCntF());
			map.put("issCntF", dataList.get(i).getIssCntF());
			map.put("notiCntF", dataList.get(i).getNotiCntF());
			map.put("ohdCntF", dataList.get(i).getOhdCntF());

			map.put("rcvCntP", dataList.get(i).getRcvCntP());
			map.put("iniohdCntP", dataList.get(i).getIniohdCntP());
			map.put("issCntP", dataList.get(i).getIssCntP());
			map.put("notiCntP", dataList.get(i).getNotiCntP());
			map.put("ohdCntP", dataList.get(i).getOhdCntP());

			list.add(map);
		}
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}
	
	@Override
	public String getTermStockStatus(InventoryVo inventoryVo) throws Exception {
		SqlSession sql = getSqlSession();
		
		List<InventoryVo> dataList = sql.selectList(namespace + "getTermStockStatus", inventoryVo);
		
		List list = new ArrayList<InventoryVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "utf-8"));
			map.put("ex", dataList.get(i).getEx());
			map.put("item", dataList.get(i).getItem());
			map.put("date", dataList.get(i).getDate());
			map.put("chk", dataList.get(i).getChk());
			map.put("minOpr", dataList.get(i).getMinOpr());
			map.put("maxOpr", dataList.get(i).getMaxOpr());
			
			map.put("sumIn", dataList.get(i).getSumIn());
			map.put("sumInL", dataList.get(i).getSumInL());
			map.put("sumInR", dataList.get(i).getSumInR());
			map.put("sumInM", dataList.get(i).getSumInM());
			map.put("sumInC", dataList.get(i).getSumInC());
			map.put("sumInF", dataList.get(i).getSumInF());
			map.put("sumInP", dataList.get(i).getSumInP());
			
			map.put("sumOutP", dataList.get(i).getSumOutP());
			
			
			
			map.put("rcvCnt", dataList.get(i).getRcvCnt());
			map.put("iniohdCnt", dataList.get(i).getIniohdCnt());
			map.put("issCnt", dataList.get(i).getIssCnt());
			map.put("notiCnt", dataList.get(i).getNotiCnt());
			map.put("ohdCnt", dataList.get(i).getOhdCnt());
			
			map.put("rcvCntL", dataList.get(i).getRcvCntL());
			map.put("iniohdCntL", dataList.get(i).getIniohdCntL());
			map.put("issCntL", dataList.get(i).getIssCntL());
			map.put("notiCntL", dataList.get(i).getNotiCntL());
			map.put("atohdCntL", dataList.get(i).getOhdCntL());
			map.put("copyL", dataList.get(i).getCopyL());
			
			map.put("rcvCntR", dataList.get(i).getRcvCntR());
			map.put("iniohdCntR", dataList.get(i).getIniohdCntR());
			map.put("issCntR", dataList.get(i).getIssCntR());
			map.put("notiCntR", dataList.get(i).getNotiCntR());
			map.put("ohdCntR", dataList.get(i).getOhdCntR());
			
			map.put("rcvCntM", dataList.get(i).getRcvCntM());
			map.put("iniohdCntM", dataList.get(i).getIniohdCntM());
			map.put("issCntM", dataList.get(i).getIssCntM());
			map.put("notiCntM", dataList.get(i).getNotiCntM());
			map.put("ohdCntM", dataList.get(i).getOhdCntM());
			
			map.put("rcvCntC", dataList.get(i).getRcvCntC());
			map.put("iniohdCntC", dataList.get(i).getIniohdCntC());
			map.put("issCntC", dataList.get(i).getIssCntC());
			map.put("notiCntC", dataList.get(i).getNotiCntC());
			map.put("ohdCntC", dataList.get(i).getOhdCntC());
			
			map.put("rcvCntF", dataList.get(i).getRcvCntF());
			map.put("iniohdCntF", dataList.get(i).getIniohdCntF());
			map.put("issCntF", dataList.get(i).getIssCntF());
			map.put("notiCntF", dataList.get(i).getNotiCntF());
			map.put("ohdCntF", dataList.get(i).getOhdCntF());
			
			map.put("rcvCntP", dataList.get(i).getRcvCntP());
			map.put("iniohdCntP", dataList.get(i).getIniohdCntP());
			map.put("issCntP", dataList.get(i).getIssCntP());
			map.put("notiCntP", dataList.get(i).getNotiCntP());
			map.put("ohdCntP", dataList.get(i).getOhdCntP());
			
			list.add(map);
		}
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getLotTracer(InventoryVo inventoryVo) throws Exception {
		SqlSession sql = getSqlSession();
		// inventoryVo.setProj(sql.selectOne(namespace + "getLotProj", inventoryVo.getLotNo()).toString());
		// System.out.println("PROJ is : " + inventoryVo.getProj());
		List<InventoryVo> dataList = sql.selectList(namespace + "getLotTracer", inventoryVo);
		
		List list = new ArrayList<InventoryVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "utf-8"));
			map.put("item", dataList.get(i).getItem());
			
			map.put("wLotNo", dataList.get(i).getWLotNo());
			map.put("rLotNo", dataList.get(i).getRLotNo());
			map.put("mLotNo", dataList.get(i).getMLotNo());
			map.put("cLotNo", dataList.get(i).getCLotNo());
			map.put("pLotNo", dataList.get(i).getPLotNo());
			
			map.put("wLotCnt", dataList.get(i).getWLotCnt());
			map.put("rLotCnt", dataList.get(i).getRLotCnt());
			map.put("mLotCnt", dataList.get(i).getMLotCnt());
			map.put("cLotCnt", dataList.get(i).getCLotCnt());
			map.put("pLotCnt", dataList.get(i).getPLotCnt());
			
			map.put("wLotStock", dataList.get(i).getWLotStock());
			map.put("rLotStock", dataList.get(i).getRLotStock());
			map.put("mLotStock", dataList.get(i).getMLotStock());
			map.put("cLotStock", dataList.get(i).getCLotStock());
			map.put("pLotStock", dataList.get(i).getPLotStock());
			
			map.put("wRegDate", dataList.get(i).getWRegDate());
			map.put("rRegDate", dataList.get(i).getRRegDate());
			map.put("mRegDate", dataList.get(i).getMRegDate());
			map.put("cRegDate", dataList.get(i).getCRegDate());
			map.put("pRegDate", dataList.get(i).getPRegDate());

			list.add(map);
		}
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public String stockProcessSave(String val, String date) throws Exception {
		// TODO Auto-generated method stub
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<InventoryVo> list = new ArrayList<InventoryVo>();
		String str = "";

		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			InventoryVo inventoryVo = new InventoryVo();
			inventoryVo.setItem(tempObj.get("item").toString());
			inventoryVo.setProj(tempObj.get("proj").toString());
			inventoryVo.setDate(tempObj.get("date").toString());
			inventoryVo.setIniohdCnt(Integer.parseInt(tempObj.get("iniohdCnt").toString()));
			inventoryVo.setRcvCnt(Integer.parseInt(tempObj.get("rcvCnt").toString()));
			inventoryVo.setNotiCnt(Integer.parseInt(tempObj.get("notiCnt").toString()));
			inventoryVo.setIssCnt(Integer.parseInt(tempObj.get("issCnt").toString()));
			inventoryVo.setOhdCnt(Integer.parseInt(tempObj.get("ohdCnt").toString()));
			
			list.add(inventoryVo);
		}

		try {
			/*int size = (int) sqlSessionTemplate.selectOne(namespace + "checkStockData", list.get(0).getDate());
			if(size>0) {
				sqlSessionTemplate.delete(namespace+"deleteStockData", list.get(0).getDate());
			}*/
			sqlSessionTemplate.insert(namespace + "stockProcessSave", list);
			str="success";
		} catch (Exception e) {
			str="failed";
			logger.error(e.getMessage());
			e.printStackTrace();
			// TODO: handle exception
		}

		return str;
	}
	
	@Override
	public String getStockUpList(InventoryVo inventoryVo) throws Exception {
		SqlSession sql = getSqlSession();
		// inventoryVo.setProj(sql.selectOne(namespace + "getLotProj", inventoryVo.getLotNo()).toString());
		// System.out.println("PROJ is : " + inventoryVo.getProj());
		List<InventoryVo> dataList = sql.selectList(namespace + "getStockUpList", inventoryVo);
		
		List list = new ArrayList<InventoryVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "utf-8"));
			
			map.put("iniohdCnt", dataList.get(i).getIniohdCntL());
			map.put("rcvCnt", dataList.get(i).getRcvCntL());
			map.put("issCnt", dataList.get(i).getIssCntL());
			map.put("notiCnt", dataList.get(i).getNotiCntL());
			map.put("ohdCnt", dataList.get(i).getOhdCntL());

			map.put("iniohdCntP", dataList.get(i).getIniohdCntP());
			map.put("rcvCntP", dataList.get(i).getRcvCntP());
			map.put("issCntP", dataList.get(i).getIssCntP());
			map.put("notiCntP", dataList.get(i).getNotiCntP());
			map.put("ohdCntP", dataList.get(i).getOhdCntP());

			map.put("date", dataList.get(i).getDate());
			
			list.add(map);
		}
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}
	
	@Override
	public String getStockProUpList(InventoryVo inventoryVo) throws Exception {
		SqlSession sql = getSqlSession();
		// inventoryVo.setProj(sql.selectOne(namespace + "getLotProj", inventoryVo.getLotNo()).toString());
		// System.out.println("PROJ is : " + inventoryVo.getProj());
		List<InventoryVo> dataList = sql.selectList(namespace + "getStockProUpList", inventoryVo);
		
		List list = new ArrayList<InventoryVo>();
		for (int i = 0; i < dataList.size(); i++) { 
			Map map = new HashMap();
			map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "utf-8"));
			map.put("item", URLEncoder.encode(dataList.get(i).getItem(), "utf-8"));
			
			map.put("iniohdCnt", dataList.get(i).getIniohdCntL());
			map.put("rcvCnt", dataList.get(i).getRcvCntL());
			map.put("issCnt", dataList.get(i).getIssCntL());
			map.put("notiCnt", dataList.get(i).getNotiCntL());
			map.put("ohdCnt", dataList.get(i).getOhdCntL());
			
			map.put("iniohdCntR", dataList.get(i).getIniohdCntR());
			map.put("rcvCntR", dataList.get(i).getRcvCntR());
			map.put("issCntR", dataList.get(i).getIssCntR());
			map.put("notiCntR", dataList.get(i).getNotiCntR());
			map.put("ohdCntR", dataList.get(i).getOhdCntR());

			map.put("iniohdCntM", dataList.get(i).getIniohdCntM());
			map.put("rcvCntM", dataList.get(i).getRcvCntM());
			map.put("issCntM", dataList.get(i).getIssCntM());
			map.put("notiCntM", dataList.get(i).getNotiCntM());
			map.put("ohdCntM", dataList.get(i).getOhdCntM());
			
			map.put("iniohdCntC", dataList.get(i).getIniohdCntC());
			map.put("rcvCntC", dataList.get(i).getRcvCntC());
			map.put("issCntC", dataList.get(i).getIssCntC());
			map.put("notiCntC", dataList.get(i).getNotiCntC());
			map.put("ohdCntC", dataList.get(i).getOhdCntC());
			
			map.put("iniohdCntF", dataList.get(i).getIniohdCntF());
			map.put("rcvCntF", dataList.get(i).getRcvCntF());
			map.put("issCntF", dataList.get(i).getIssCntF());
			map.put("notiCntF", dataList.get(i).getNotiCntF());
			map.put("ohdCntF", dataList.get(i).getOhdCntF());
			
			map.put("date", dataList.get(i).getDate());
			
			list.add(map);
		}
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}
	
	@Override
	public String stockLastUpSave(String val) throws Exception {
		// TODO Auto-generated method stub
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<InventoryVo> list = new ArrayList<InventoryVo>();
		String str = "";

		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			InventoryVo inventoryVo = new InventoryVo();
			inventoryVo.setPrdNo(tempObj.get("prdNo").toString());
			inventoryVo.setProj(tempObj.get("proj").toString());
//			inventoryVo.setItem(tempObj.get("item").toString());

			inventoryVo.setIniohdCnt(Integer.parseInt(tempObj.get("iniohdCnt").toString()));
			inventoryVo.setRcvCnt(Integer.parseInt(tempObj.get("rcvCnt").toString()));
			inventoryVo.setIssCnt(Integer.parseInt(tempObj.get("issCnt").toString()));
			inventoryVo.setNotiCnt(Integer.parseInt(tempObj.get("notiCnt").toString()));
			inventoryVo.setOhdCnt(Integer.parseInt(tempObj.get("ohdCnt").toString()));
			inventoryVo.setDate(tempObj.get("date").toString());

			
			list.add(inventoryVo);
		}

		try {

			sqlSessionTemplate.insert(namespace + "stockLastUpSave", list);
			
			str="success";
		} catch (Exception e) {
			str="failed";
			logger.error(e.getMessage());
			e.printStackTrace();
			// TODO: handle exception
		}

		return str;
	}
	
	@Override
	public String getUpdateOhdCnt(InventoryVo inventoryVo) throws Exception {
		SqlSession sql = getSqlSession();
		// inventoryVo.setProj(sql.selectOne(namespace + "getLotProj", inventoryVo.getLotNo()).toString());
		// System.out.println("PROJ is : " + inventoryVo.getProj());
		
		//소재 창고 수량 불러오기
		List<InventoryVo> dataList = sql.selectList(namespace + "getUpdateOhdCnt", inventoryVo);
		
		//공정 창고 수량 불러오기
		List<InventoryVo> dataListPro = sql.selectList(namespace + "getUpdatePro", inventoryVo);
		
		//완성품번 리스트 불러오기
		List<InventoryVo> dataListRt = sql.selectList(namespace + "getUpdateRt", inventoryVo);
		
		List list = new ArrayList<InventoryVo>();
		List listPro = new ArrayList<InventoryVo>();
		List listRt = new ArrayList<InventoryVo>();
		
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "utf-8"));
			
			map.put("vndNm", URLEncoder.encode(dataList.get(i).getVndNm(), "utf-8"));
			
			map.put("idx", dataList.get(i).getIdx());
			map.put("barcode", dataList.get(i).getBarcode());
			map.put("iniohdCnt", dataList.get(i).getCnt());
			map.put("ohdCnt", dataList.get(i).getStock());
						
			map.put("iniohdCnt", dataList.get(i).getIniohdCnt());
			map.put("rcvCnt", dataList.get(i).getRcvCnt());
			map.put("issCnt", dataList.get(i).getIssCnt());
			map.put("notiCnt", dataList.get(i).getNotiCnt());
			map.put("ohdCnt", dataList.get(i).getOhdCnt());

			map.put("date", dataList.get(i).getDate());
			
			list.add(map);
		}
		for (int i = 0; i < dataListPro.size(); i++) {
			Map map = new HashMap();
			map.put("prdNo", URLEncoder.encode(dataListPro.get(i).getPrdNo(), "utf-8"));
			map.put("item", URLEncoder.encode(dataListPro.get(i).getItem(), "utf-8"));
			
			map.put("ohdCnt", dataListPro.get(i).getOhdCnt());
			map.put("rcvCnt", dataListPro.get(i).getRcvCnt());
			map.put("issCnt", dataListPro.get(i).getIssCnt());
			map.put("notiCnt", dataListPro.get(i).getNotiCnt());
			map.put("iniohdCnt", dataListPro.get(i).getIniohdCnt());
			map.put("ohdCntR", dataListPro.get(i).getOhdCntR());
			map.put("ohdCntM", dataListPro.get(i).getOhdCntM());
			map.put("ohdCntC", dataListPro.get(i).getOhdCntC());
			map.put("ohdCntP", dataListPro.get(i).getOhdCntP());
			
			map.put("date", dataListPro.get(i).getDate());
			map.put("eDate", dataListPro.get(i).getEDate());
			
			listPro.add(map);
		}
		
		for (int i = 0; i < dataListRt.size(); i++) {
			Map map = new HashMap();
			map.put("vndNo", dataListRt.get(i).getVndNo());
			map.put("prdNo", URLEncoder.encode(dataListRt.get(i).getPrdNo(), "utf-8"));
			map.put("proj", dataListRt.get(i).getProj());
			map.put("shipLot", dataListRt.get(i).getShipLot());
			

			map.put("iniohdCnt", dataListRt.get(i).getIniohdCnt());
			map.put("rcvCnt", dataListRt.get(i).getRcvCnt());
			map.put("issCnt", dataListRt.get(i).getIssCnt());
			map.put("notiCnt", dataListRt.get(i).getNotiCnt());
			map.put("ohdCnt", dataListRt.get(i).getOhdCnt());
			
			map.put("date", dataListRt.get(i).getDate());
			
			listRt.add(map);
		}
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		dataMap.put("dataListRt", listRt);
		dataMap.put("dataListPro", listPro);
		
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}
	
	@Override
	public String stockInohdCntSave(InventoryVo inventoryVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str;
		try {
			
			// 재고 변경하기
			sql.update(namespace + "stockInohdCntSave" , inventoryVo);
			//변경 트렌스 남기기
			sql.insert(namespace + "insertInohdCntTrans" , inventoryVo);

			str="success";
		} catch (Exception e) {
			str="fail";
			// TODO: handle exception
		}
		return str;
	}
	
	@Override
	public String stockSuccessInohdCntSave(InventoryVo inventoryVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str;
		try {
			
			//재고 변경하기
			sql.update(namespace + "stockSuccessInohdCntSave" , inventoryVo);
			//변경 트렌스 남기기
			sql.insert(namespace + "insertSuccessInohdCntTrans" , inventoryVo);
			
			str="success";
		} catch (Exception e) {
			str="fail";
			// TODO: handle exception
		}
		return str;
	}
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public String stockdeadLineSave(String total, String pro) throws Exception {
		// TODO Auto-generated method stub
		
		// TOTAL 값 가져오기
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(total);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");
		
		// PROCESS 값 가져오기
		JSONParser jsonParserPro = new JSONParser();
		JSONObject jsonObjPro = (JSONObject) jsonParser.parse(pro);
		JSONArray jsonArrayPro = (JSONArray) jsonObjPro.get("val");
		
		
		// TOTAL list 담기
		List<InventoryVo> list = new ArrayList<InventoryVo>();
		
		// PROCESS list 담기
		List<InventoryVo> listPro = new ArrayList<InventoryVo>();

		String str = "";

		System.out.println("===============TOTAL==============");
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			InventoryVo inventoryVo = new InventoryVo();
			System.out.println(tempObj.get("prdNo").toString() + " : " + "[" + tempObj.get("date").toString() + "]" + tempObj.get("ohdCnt").toString());
			
			inventoryVo.setPrdNo(tempObj.get("prdNo").toString());
			inventoryVo.setProj(tempObj.get("proj").toString());
			inventoryVo.setDate(tempObj.get("date").toString());

			inventoryVo.setBiniohdCnt(Integer.parseInt(tempObj.get("biniohdCnt").toString()));
			inventoryVo.setIniohdCnt(Integer.parseInt(tempObj.get("iniohdCnt").toString()));
			inventoryVo.setBohdCnt(Integer.parseInt(tempObj.get("bohdCnt").toString()));
			inventoryVo.setOhdCnt(Integer.parseInt(tempObj.get("ohdCnt").toString()));
			inventoryVo.setBigo(tempObj.get("bigo").toString());
			
			list.add(inventoryVo);
/*			inventoryVo.setItem(tempObj.get("item").toString());
			inventoryVo.setProj(tempObj.get("proj").toString());
			inventoryVo.setDate(tempObj.get("date").toString());
			inventoryVo.setIniohdCnt(Integer.parseInt(tempObj.get("iniohdCnt").toString()));
			inventoryVo.setRcvCnt(Integer.parseInt(tempObj.get("rcvCnt").toString()));
			inventoryVo.setNotiCnt(Integer.parseInt(tempObj.get("notiCnt").toString()));
			inventoryVo.setIssCnt(Integer.parseInt(tempObj.get("issCnt").toString()));
			inventoryVo.setOhdCnt(Integer.parseInt(tempObj.get("ohdCnt").toString()));
			
			list.add(inventoryVo);*/
		}
		


		System.out.println("===============PRO==============");

		for (int i = 0; i < jsonArrayPro.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArrayPro.get(i);

			InventoryVo inventoryVo = new InventoryVo();
			System.out.println(tempObj.get("prdNo").toString() + " : " + "[" + tempObj.get("date").toString() + "]" + tempObj.get("ohdCnt").toString());
			
			inventoryVo.setPrdNo(tempObj.get("prdNo").toString());
			inventoryVo.setProj(tempObj.get("proj").toString());
			inventoryVo.setDate(tempObj.get("date").toString());

//			inventoryVo.setBiniohdCnt(Integer.parseInt(tempObj.get("biniohdCnt").toString()));
//			inventoryVo.setIniohdCnt(Integer.parseInt(tempObj.get("iniohdCnt").toString()));
			inventoryVo.setBohdCnt(Integer.parseInt(tempObj.get("bohdCnt").toString()));
			inventoryVo.setOhdCnt(Integer.parseInt(tempObj.get("ohdCnt").toString()));
			inventoryVo.setBigo(tempObj.get("bigo").toString());
			
			listPro.add(inventoryVo);

		}
		try {
			//변경된 사항이 있어 list에 데이터가 있을경우만
			if(list.size()>0) {
				//pro 총 공정 테이블 이력 남기기 => TB_STOCK_TOTAL
				sqlSessionTemplate.insert(namespace + "totalProMoveHistory", list);
				//pro 총 공정 테이블 수량 변경하기 => TB_STOCK_TOTAL
				sqlSessionTemplate.update(namespace + "totalProMoveUpt", list);
			}
			//변경된 사항이 있어 listPro에 데이터가 있을경우만
			if(listPro.size()>0) {
				//pro 재고마감 테이블 이력 남기기 => TB_STOCK_PROCESS
				sqlSessionTemplate.insert(namespace + "ProMoveHistory", listPro);
				//pro 재고마감 테이블 수량 변경하기 => TB_STOCK_PROCESS
				sqlSessionTemplate.update(namespace + "proMoveUpt", listPro);
			}
			
			str="success";
		} catch (Exception e) {
			str="failed";
			logger.error(e.getMessage());
			e.printStackTrace();
			// TODO: handle exception
		}

		return str;
	}

	@Override
	public String getServerTime() throws Exception {
		
		String str ="";
		try {
			str = (String) sqlSessionTemplate.selectOne(namespace + "getServerTime");
		} catch (Exception e) {
			str = "fail";
			// TODO: handle exception
		}
		// TODO Auto-generated method stub
		return str;
	}
	
}
