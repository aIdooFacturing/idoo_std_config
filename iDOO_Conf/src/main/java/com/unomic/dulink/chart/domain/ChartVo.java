
package com.unomic.dulink.chart.domain;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ChartVo {
	String nonOpTimeD;
	String workerId;
	String password;
	String shipId;
	String attendanceTy;
	int clmNo;
	String grpCd;
	
	String redExpression;
	String redBlinkExpression;
	String yellowExpression;
	String yellowBlinkExpression;
	String greenExpression;
	String greenBlinkExpression;
	
	String statement;
	String attendanceDiv;
	String attendance;
	String nonOpTimeN;
	String token;
	String utc;
	
	String empCd;
	String empN;
	String expression;
	String empD;
	String empCdN;
	String empCdD;

	String MANDT;
	String PLNT;
	String MATNO;
	String MATNM;
	String RWMATNO;
	String VNDNM;
	String SPLNM;
	String PRCE;
	// String SPEC;
	String ITVDV;
	String before;

	String delivererName;
	String totalPrc;
	String wcCd;
	String delivererId;
	String machCd;
	String mnPrgNo;
	String cmpCd;
	String cylTmSe;
	String outLot;
	String workTime;
	String deliveryDate;
	String deliveryNo;
	String dvcName;
	String email;
	String group1;
	String group2;
	String group3;
	String reason;
	String preOpr;
	Integer partCyl;
	String nextOpr;
	String tgCyl;
	int workTy;
	String dvcCntCyl;
	String okRatio;
	int preSend;
	String fResult;
	String vndNo;
	String resetDt;

	String attrCd;
	String baseCnt;
	String inputCnt;
	int workIdx;
	String oprCd;
	String outCnt;
	String pwd;
	String fromOprNm;

	String goalRatio;
	String workTm;
	int stockCnt;
	String fromDashboard;
	String tgCntD;
	String tgCntN;
	String tgRunTimeD;
	String tgRunTimeN;
	String cntPerCylD;
	String cntPerCylN;

	int nd;
	String attrNameKo;
	String prdCntLmt;
	int calcTy;
	String result;
	String result2;
	String result3;
	String result4;
	String afterMeasurer;
	String checkCycle;
	String zero;
	String zTo50;
	String fTo100;
	String situationTy;
	String listId;
	String situation;
	String over100;
	String unit;
	String attrTy;
	String dp;
	String group;
	String cheker;
	String prdctPerHour;
	int updatedRcvCnt;
	String rw;
	int updatedNotiCnt;
	String cycleTmM;
	String cycleTmS;
	String cvt;
	String uph;
	String workTmMD;
	String workTmMN;
	String capa;
	String prd;
	String opTime;
	String codeGroup;
	String codeGroupName;
	String order;
	String rnTmCrtSec;
	String worker;
	String code;
	String codeName;
	String oprNo;
	String workerD;
	String workerN;
	String nonOpTime;
	String oprNm;
	String workerOpRatio;
	String nonOpTy;
	String nonOpTyTxt;
	String time;
	String workerOpTime;
	String msg;
	String tId;
	String prdCd;
	String opPrdCnt;
	String prdctPerCyl;
	String cylCnt;
	String cstmPrdCnt;
	String cnt;
	String prdOdr;
	String prdPrc;
	String prc;
	String checker;
	String part;
	String seq;
	String situTy;
	String situ;
	String cause;
	String gchTy;
	String gch;
	String action;
	String chkType;
	String chkTy;

	int index;
	String prdCnt;
	String processFaultyCnt;
	String customerFaultyCnt;
	String processFaultyRate;
	String customerFaultyRate;
	String faultCnt;
	int faultCntRatio;
	String setUpTime;
	String totalTime;
	String total;
	String prdNo;
	int sec;
	String cntPerCyl;
	String cylTm;
	String cylAvgTm;
	String cylSdTm;
	String totalSpdLoad;
	String totalAvgSpdLoad;
	String totalSdSpdLoad;

	int x;
	int y;
	double width;
	double height;
	String viewBox;
	String transform;
	String d;
	int dvcX;
	int dvcY;
	String comName;

	int dvcWidth;
	int dvcHeight;
	String isSpd;
	String prgCd;
	String prgName;
	String chkSpdLoad;
	String ratio;
	String prdCntCrt;
	String offset;
	String cycleRemain;
	String slAv;
	String slMx;
	String slMn;
	String slCr;
	String rgb;
	String cycleUsedRate;
	String toolNm;
	String lotNo;
	String shipLotNo;
	int sendCnt;
	int lotCnt;
	int smplCnt;
	int preSendCnt;
	int notiCnt;
	int rcvCnt;
	String inputDate;
	String line;
	String maxRow;
	String spec;
	String item;
	String target;
	String low;
	String up;
	String value;
	int workerCnt;
	String attrNameOthr;
	String portNo;
	String lot1;
	String lot2;
	String lot3;
	String lot4;
	String lot5;
	int cnt1;
	int cnt2;
	int cnt3;
	int cnt4;
	int cnt5;
	int totalCnt;
	String measurer;
	int cmplCnt;
	String jsGroupCd;
	String runTimeLimit;
	String runTimeCurrent;
	String runTimeRemain;
	String runTimeUsedRate;
	String offsetDLimit;
	String offsetDInit;
	String offsetDCurrent;
	String offsetHLimit;
	String offsetHInit;
	String offsetHCurrent;
	String REGDT;
	String offsetD;
	String offsetH;
	String END_TIME;
	String START_TIME;
	String OP150INSP;
	String OP100110INSP;
	String OP8090INSP;
	String OP150;
	String OP110;
	String OP100;
	String OP90;
	String OP80;
	String SN;
	String stDate;
	String fnDate;
	String eName;
	String rpCause;
	String adtId;
	String lastAlarmCode;
	String lastTgPrdctNum;
	String lastFnPrdctNum;
	String cntCyl;
	String remainCnt;
	String NC;

	String LastAvrCycleTime;
	String lastAlarmMsg;
	String ncAlarmNum1;
	String ncAlarmMsg1;
	String ncAlarmNum2;
	String ncAlarmMsg2;
	String avrCycleTimeSec;
	String tgPrdctCnt;
	String crtRunningTimeSec;
	String ncAlarmNum3;
	String ncAlarmMsg3;
	String ncAlarmNum4;
	String ncAlarmMsg4;
	String ncAlarmNum5;
	String ncAlarmMsg5;
	String pmcAlarmNum1;
	String pmcAlarmMsg1;
	String pmcAlarmNum2;
	String pmcAlarmMsg2;
	String pmcAlarmNum3;
	String pmcAlarmMsg3;
	String pmcAlarmNum4;
	String pmcAlarmMsg4;
	String pmcAlarmNum5;
	String pmcAlarmMsg5;
	Integer shopId;
	String dvcId;
	String val;
	String delayTimeSec;
	String startDateTime;
	String alarmCode;
	String cd;
	String alarmMsg;
	String targetTime;
	String sDate;
	String status;
	String eDate;
	String id;
	String tgDate;
	String tgRunTime;
	String runTime;
	String limitPrdCnt;
	String limitRunTime;

	String tgCnt;
	String GRNM;
	String workDate;
	String isOld;
	String ty;
	String endDateTime;
	String chartStatus;
	String spdLoad;
	String spdOverride;
	String spdActualSpeed;
	String rapidOverride;
	String actualFeed;
	String feedOverride;
	String mode;
	String programHeader;
	String programName;
	String alarm;
	String date;
	String PRDPRGM;
	String CNTMCD;

	String callback;
	String name;
	String lastUpdateTime;
	String lastModal;
	String operationTime;
	String WC;
	String nameAbb;
	String WCCD;

	String jig;
	String isAuto;
	int chartOrder;
	String type;
	int spdTime;
	String EXCD;
	int isvalue;
	String lastProgramName;
	String lastProgramHeader;
	String startTime;
	String endTime;
	String targetRatio;
	int cuttingTime;
	int inCycleTime;
	int waitTime;
	int alarmTime;
	int noConnectionTime;
	double opRatio;
	String cuttingTimeRatio;
	double cuttingRatio;

	String targetDateTime;
	String routing;
	List<String> nameMap = null;

	String beforeId;
	int TimeCHECK;

	String report;
	String solver;
	String solverName;
	String device;
	String regDate;

	String idx;
	int count;

	String stockid;
	String totaldelivery;
	String chklotno;
	int CNCCNT;
	int HOUSECNT;
	int RCVSER;
	int issCnt;
	int ohdCnt;
	int iniohdCnt;
	String barcode;
	String plusDay;
	String overTime;
	String attandTime;
	String rank;
	
	List<String> prdNoList;
	List<String> dayList;
	List<Integer> empCdList;
	int average;
	int lotStock;
	
	int checkNum;
	String totalWaitTime;
	int countDvc;
	int lastRank;
	int cntWorker;
	int dvcCnt;
	
	int prdNoCnt;
	int exist;
	int rawIdx;
	
	String checkedStandard;
	String approve;
	String lv;
	
	int minPartCyl;
	int maxPartCyl;
	int cpc;
	
	int beforeWorkIdx;
	
	int workerStandard;
	int deviceStandard;
	
	int targetRuntime;
	
	int avgCyleTime;
	
	int multiPrdNo;
	
	String startTimeD;
	String startTimeN;
	String endTimeD;
	String endTimeN;
	int startCnt;
	int endCnt;
	
	String startWorkTime;
	String endWorkTime;
	String category;
	String monthDate;
	String operatingRate;
	int ppm;
	
	String groupLotNo;
	
	int targetCnt;
	
	String actionEmpCd;
	String actionDate;
	String proj;
	String plan;
	String beforeProj;
	String afterProj;

	String newLotNo;
	
	int lmtT;
	int RunT;
	int lmtC;
	
	int ToolCnt;
	float found1;
	float found2;
	float beforeC;
	float afterC;
	int workFault;
	int leaderFault;
	int Fault1;
	int Fault2;
	String leader;
	String ITEMNO;
	
	String countCapa;
	String remarks;
	String searchText;
	
	String dDay;
	String mDay;
	
	String division;
	String standard;
	int interval;
	int RInterval;
	String lastChk;
	String RlastChk;
	String model;
	String modelName;
	String department;
	String equipment;
	String applyDate;
	String img;
	String purpose;
	String useLine;
	String chkAdd;
	
	String cSize;
	String passed;
	String bigo;
	
	String cs1;
	String cs2;
	String cs3;
	String cs4;
	String cs5;
	
	String chk1;
	String chk2;
	String chk3;
	String chk4;
	String chk5;
	
	int chk;
}
