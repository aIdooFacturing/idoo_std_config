package com.unomic.dulink.chart.service;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.unomic.dulink.chart.domain.ChartVo;

@Repository
@Service
public class CommonServiceImpl implements CommonService {
	
	private final static String namespace = "com.unomic.dulink.chart.";
	private final static String namespace_common = "com.unomic.dulink.common.";

	@Inject
	SqlSession sqlSessionTemplate;

	@Override
	public String getWorkerList(ChartVo chartVo) throws Exception {
		List<ChartVo> dataList = sqlSessionTemplate.selectList(namespace + "getWorkerList", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();

			map.put("id", dataList.get(i).getId());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);

		return str;
	}
	
	@Override
	public String getAllWorkerList(ChartVo chartVo) throws Exception {
		List<ChartVo> dataList = sqlSessionTemplate.selectList(namespace + "getAllWorkerList", chartVo);
		List<ChartVo> dataList1 = sqlSessionTemplate.selectList(namespace + "totalWorker", chartVo);

		List list = new ArrayList<ChartVo>();
		List list1 = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("id", dataList.get(i).getId());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("part", URLEncoder.encode(dataList.get(i).getPart(), "utf-8"));
			map.put("email", dataList.get(i).getEmail());
			map.put("LV", dataList.get(i).getLv());

			list.add(map);
		};
		for (int i = 0; i < dataList1.size(); i++) {
			Map map = new HashMap();
			map.put("id", dataList1.get(i).getId());
			map.put("name", URLEncoder.encode(dataList1.get(i).getName(), "utf-8"));
			map.put("part", URLEncoder.encode(dataList1.get(i).getPart(), "utf-8"));
			map.put("email", dataList1.get(i).getEmail());
			map.put("LV", dataList1.get(i).getLv());

			list1.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		dataMap.put("dataList1", list1);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);

		return str;
	}
	
	@Override
	public String getPrdNoList(ChartVo chartVo) throws Exception {
		List<ChartVo> dataList = sqlSessionTemplate.selectList(namespace + "getPrdNoList", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "utf-8"));
			map.put("matNo", URLEncoder.encode(dataList.get(i).getRWMATNO(), "utf-8"));
			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String dvcList() throws Exception {
		List<ChartVo> dataList = sqlSessionTemplate.selectList(namespace_common + "dvcList");

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("dvcName", dataList.get(i).getName());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getPrdNoListByRtng(ChartVo chartVo) throws Exception {
		List<ChartVo> dataList = sqlSessionTemplate.selectList(namespace_common + "getPrdNoListByRtng");

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("prdNo", dataList.get(i).getPrdNo());
			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getDeviceCnt(ChartVo chartVo) throws Exception {
		List<ChartVo> dataList = sqlSessionTemplate.selectList(namespace_common + "getDeviceCnt", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("tgCnt", dataList.get(i).getTgCnt());
			map.put("cntCyl", dataList.get(i).getCntCyl());
			map.put("partCyl", dataList.get(i).getPartCyl());
			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

}
