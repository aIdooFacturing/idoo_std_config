package com.unomic.dulink.chart.service;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unomic.dulink.chart.controller.OrderController;
import com.unomic.dulink.chart.domain.AverageVo;
import com.unomic.dulink.chart.domain.ChartVo;

@Service
@Repository
public class OrderServiceImpl implements OrderService {

	private final static String namespace_chart = "com.unomic.dulink.chart.";
	private final static String namespace = "com.unomic.dulink.order.";

	private static final Logger logger = LoggerFactory.getLogger(OrderServiceImpl.class);

	@Inject
	SqlSession sqlSessionTemplate;

	@Override
	public String getPrdNoList(ChartVo chartVo) throws Exception {
		List<ChartVo> dataList = sqlSessionTemplate.selectList(namespace_chart + "getPrdNoList", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "utf-8"));

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getPrdData(ChartVo chartVo) throws Exception {

		List<ChartVo> dataList_waitTimeList = new ArrayList();
		if (chartVo.getWaitTime() == 0) {

		} else {
			dataList_waitTimeList = sqlSessionTemplate.selectList(namespace + "waitTimeList", chartVo);
		}

		List<ChartVo> dataList = sqlSessionTemplate.selectList(namespace + "getPrdData", chartVo);
		List<AverageVo> averageList = new ArrayList();

		int dataList_length = dataList.size();
		String tempDate = null, empCd = null;
		int sum = 0, average = 0, count = 0;
		for (int i = 0; i < dataList_length; i++) {
			if (dataList.get(i).getEmpCd() == null) {
				dataList.get(i).setEmpCd("0");
			}
			if (tempDate == null && empCd == null) {
				if (averageList.size() > 0) {
					for (int j = 0; j < averageList.size(); j++) {
						if (averageList.get(j).getDate().equals(dataList.get(i).getDate())
								&& averageList.get(j).getEmpCd().equals(dataList.get(i).getEmpCd())) {

						} else {
							tempDate = dataList.get(i).getDate();
							empCd = dataList.get(i).getEmpCd();
						}
					}
				} else {
					tempDate = dataList.get(i).getDate();
					empCd = dataList.get(i).getEmpCd();
				}
			} else {
				for (int j = 0; j < dataList_length; j++) {
					if (dataList.get(j).getEmpCd() == null) {
						dataList.get(j).setEmpCd("0");
					}
					if (dataList.get(j).getDate().equals(tempDate)
							&& dataList.get(j).getEmpCd().equals(empCd)) {

						sum = sum + Integer.parseInt(dataList.get(j).getGoalRatio());
						count++;
					}
				}
				try {
					average = sum / count;
				} catch (ArithmeticException e) {
					average = 0;
				}
				AverageVo vo = new AverageVo();
				vo.setDate(tempDate);
				vo.setAverage(average);
				vo.setEmpCd(empCd);
				averageList.add(vo);
				logger.info("tempDate is : " + tempDate);
				logger.info("empCd is : " + empCd);
				sum = 0;
				count = 0;
				tempDate = null;
				empCd = null;
			}
		}

		for (int i = 0; i < averageList.size(); i++) {
			logger.info("===================");
			logger.info("Date is : " + averageList.get(i).getDate());
			logger.info("EmpCd is : " + averageList.get(i).getEmpCd());
			logger.info("average is " + averageList.get(i).getAverage());
			for (int j = 0; j < dataList_length; j++) {
				if (averageList.get(i).getEmpCd().equals(dataList.get(j).getEmpCd())
						&& averageList.get(i).getDate().equals(dataList.get(j).getDate())) {
					dataList.get(j).setAverage(averageList.get(i).getAverage());
				}
			}
		}

		if (chartVo.getWaitTime() == 0) {

		} else {
			ChartVo temp = null;
			List<ChartVo> tempList = new ArrayList<ChartVo>();
			int length = dataList_waitTimeList.size();
			logger.info("===================");
			for (int i = 0; i < length; i++) {
				if (dataList_waitTimeList.get(i).getChartStatus().equals("WAIT")) {
					if (temp == null) {
						temp = dataList_waitTimeList.get(i);
					} else {
						if (temp.getEDate().equals(dataList_waitTimeList.get(i).getSDate())
								&& temp.getDvcId().equals(dataList_waitTimeList.get(i).getDvcId())
								&& temp.getWorkDate().equals(dataList_waitTimeList.get(i).getWorkDate())) {
							int waitTime = temp.getWaitTime();
							temp = dataList_waitTimeList.get(i);
							temp.setWaitTime(waitTime + 2);
						} else {
							temp.setWaitTime(temp.getWaitTime() + 2);
							if (temp.getWaitTime() <= 6) {

							} else {
								tempList.add(temp);
								logger.info("temp waitTime : " + temp.getWaitTime());
								temp = null;
							}

						}
					}
				} else {
					if (temp == null) {
					} else {
						if (temp.getWaitTime() <= 6) {

						} else {
							tempList.add(temp);
							logger.info("temp waitTime : " + temp.getWaitTime());
							temp = null;
						}
					}
				}
			}

			int length_1 = tempList.size();
			for (int i = 0; i < dataList.size(); i++) {
				for (int j = 0; j < length_1; j++) {
					if (tempList.get(j).getDvcId().equals(dataList.get(i).getDvcId())
							&& tempList.get(j).getWorkDate().equals(dataList.get(i).getDate())) {
						dataList.get(i).setWaitTime(tempList.get(j).getWaitTime());
					}
				}
			}
		}

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();

			map.put("prdNo", dataList.get(i).getPrdNo());
			map.put("oprNm", dataList.get(i).getOprNm());
			map.put("oprCd", dataList.get(i).getOprCd());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("cylTmMi", dataList.get(i).getCycleTmM());
			map.put("cylTmSec", dataList.get(i).getCycleTmS());
			map.put("cvt", dataList.get(i).getCvt());
			map.put("uph", dataList.get(i).getUph());
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("worker", URLEncoder.encode(dataList.get(i).getWorker(), "utf-8"));
			map.put("workerCnt", dataList.get(i).getWorkerCnt());
			map.put("date", dataList.get(i).getDate());
			map.put("sDate", dataList.get(i).getSDate());
			map.put("eDate", dataList.get(i).getEDate());
			map.put("workTm", dataList.get(i).getWorkTm());
			map.put("capa", dataList.get(i).getCapa());
			map.put("target", dataList.get(i).getTarget());
			map.put("cnt", dataList.get(i).getCnt());
			map.put("partCyl", dataList.get(i).getPartCyl());
			map.put("faultCnt", dataList.get(i).getFaultCnt());
			map.put("okRatio", dataList.get(i).getOkRatio());
			map.put("nd", dataList.get(i).getNd());
			map.put("goalRatio", dataList.get(i).getGoalRatio());
			map.put("overTime", dataList.get(i).getOverTime());
			map.put("attandTime", dataList.get(i).getAttandTime());
			map.put("rank", dataList.get(i).getRank());
			map.put("waitTime", dataList.get(i).getWaitTime());
			map.put("average", dataList.get(i).getAverage());
			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);

		return str;
	}

	@Override
	public String getWorkerPerfomance(ChartVo chartVo) throws Exception {

		List<ChartVo> dataList = new ArrayList<ChartVo>();

		try {
			dataList = sqlSessionTemplate.selectList(namespace_chart + "getWorkerPerfomance", chartVo);
		} catch (Exception e) {
			e.printStackTrace();
		}

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {

			Map map = new HashMap();
			map.put("prdNo", dataList.get(i).getPrdNo());
			map.put("oprNm", dataList.get(i).getOprNm());
			map.put("oprCd", dataList.get(i).getOprCd());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("cylTmMi", dataList.get(i).getCycleTmM());
			map.put("cylTmSec", dataList.get(i).getCycleTmS());
			map.put("cvt", dataList.get(i).getCvt());
			map.put("uph", dataList.get(i).getUph());
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("worker", URLEncoder.encode(dataList.get(i).getWorker(), "utf-8"));
			map.put("workerCnt", dataList.get(i).getWorkerCnt());
			map.put("date", dataList.get(i).getDate());
			map.put("workTm", dataList.get(i).getWorkTm());
			map.put("capa", dataList.get(i).getCapa());
			map.put("target", dataList.get(i).getTarget());
			map.put("cnt", dataList.get(i).getCnt());
			map.put("partCyl", dataList.get(i).getPartCyl());
			map.put("faultCnt", dataList.get(i).getFaultCnt());
			map.put("okRatio", dataList.get(i).getOkRatio());
			map.put("nd", dataList.get(i).getNd());
			map.put("goalRatio", dataList.get(i).getGoalRatio());

			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);

		return str;
	}

	@Override
	public String getLatestDate() throws Exception {

		String str = (String) sqlSessionTemplate.selectOne(namespace_chart + "getLatestDate");
		return str;
	}

	@Override
	public String getTargetData(ChartVo chartVo) throws Exception {
		
		List<ChartVo> dataList = sqlSessionTemplate.selectList(namespace_chart + "getTargetData", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("routing", URLEncoder.encode(dataList.get(i).getRouting(), "UTF-8"));
			map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "UTF-8"));
			map.put("tgCntD", dataList.get(i).getTgCntD());
			map.put("tgCntN", dataList.get(i).getTgCntN());
			map.put("cntPerCyl", dataList.get(i).getCntPerCyl());
			map.put("tgRunTimeD", dataList.get(i).getTgRunTimeD());
			map.put("tgRunTimeN", dataList.get(i).getTgRunTimeN());
			map.put("empD", dataList.get(i).getEmpD());
			map.put("empN", dataList.get(i).getEmpN());
			map.put("empCdN", dataList.get(i).getEmpCdN());
			map.put("empCdD", dataList.get(i).getEmpCdD());
			map.put("capa", dataList.get(i).getCapa());
			map.put("uph", dataList.get(i).getUph());
			map.put("cntPerCyl", dataList.get(i).getCntPerCyl());
			map.put("multiPrdNo", dataList.get(i).getMultiPrdNo());
			
			map.put("startTimeN", dataList.get(i).getStartTimeN());
			map.put("startTimeD", dataList.get(i).getStartTimeD());
			map.put("endTimeN", dataList.get(i).getEndTimeN());
			map.put("endTimeD", dataList.get(i).getEndTimeD());
			map.put("group", dataList.get(i).getGroup());
			list.add(map);
		}
		;

		String str = "";
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getTargetData1(ChartVo chartVo) throws Exception {
		List<ChartVo> dataList = sqlSessionTemplate.selectList(namespace_chart + "getTargetData1", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			// map.put("tgCnt", dataList.get(i).getTgCnt());
			// map.put("tgRunTime", dataList.get(i).getTgRunTime());
			// map.put("cntPerCyl", dataList.get(i).getCntPerCyl());
			map.put("tgCntD", dataList.get(i).getTgCntD());
			map.put("tgCntN", dataList.get(i).getTgCntN());
			map.put("cntPerCylD", dataList.get(i).getCntPerCylD());
			map.put("cntPerCylN", dataList.get(i).getCntPerCylN());
			map.put("tgRunTimeD", dataList.get(i).getTgRunTimeD());
			map.put("tgRunTimeN", dataList.get(i).getTgRunTimeN());
			map.put("empD", URLEncoder.encode(dataList.get(i).getEmpD(), "UTF-8"));
			map.put("empN", URLEncoder.encode(dataList.get(i).getEmpN(), "UTF-8"));
			map.put("empCdN", dataList.get(i).getEmpCdN());
			map.put("empCdD", dataList.get(i).getEmpCdD());
			map.put("capa", dataList.get(i).getCapa());
			map.put("uph", dataList.get(i).getUph());

			list.add(map);
		}
		;

		String str = "";
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getTargetData2(ChartVo chartVo) throws Exception {
		List<ChartVo> dataList = sqlSessionTemplate.selectList(namespace_chart + "getTargetData2", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			// map.put("tgCnt", dataList.get(i).getTgCnt());
			// map.put("tgRunTime", dataList.get(i).getTgRunTime());
			// map.put("cntPerCyl", dataList.get(i).getCntPerCyl());
			map.put("tgCntD", dataList.get(i).getTgCntD());
			map.put("tgCntN", dataList.get(i).getTgCntN());
			map.put("cntPerCylD", dataList.get(i).getCntPerCylD());
			map.put("cntPerCylN", dataList.get(i).getCntPerCylN());
			map.put("tgRunTimeD", dataList.get(i).getTgRunTimeD());
			map.put("tgRunTimeN", dataList.get(i).getTgRunTimeN());
			map.put("empD", URLEncoder.encode(dataList.get(i).getEmpD(), "UTF-8"));
			map.put("empN", URLEncoder.encode(dataList.get(i).getEmpN(), "UTF-8"));
			map.put("empCdN", dataList.get(i).getEmpCdN());
			map.put("empCdD", dataList.get(i).getEmpCdD());
			map.put("capa", dataList.get(i).getCapa());
			map.put("uph", dataList.get(i).getUph());

			list.add(map);
		}
		;

		String str = "";
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	@Transactional
	public String addTargetCnt(String val,String addval,String capaval) throws Exception {
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");
		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			
			
			
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			ChartVo chartVo = new ChartVo();
			chartVo.setDvcId(tempObj.get("dvcId").toString());
			chartVo.setTgCyl(tempObj.get("tgCyl").toString());
			chartVo.setTgRunTime(tempObj.get("tgRunTime").toString());
			chartVo.setTgDate(tempObj.get("tgDate").toString());
			chartVo.setType(tempObj.get("type").toString());
			chartVo.setEmpCd(tempObj.get("empCd").toString());
			chartVo.setCntPerCyl(tempObj.get("cntPerCyl").toString());

			chartVo.setStartTime(tempObj.get("startTime").toString());
			chartVo.setEndTime(tempObj.get("endTime").toString());

			list.add(chartVo);
		}

		//추가작업자
		JSONParser jsonParserAdd = new JSONParser();
		JSONObject jsonObjAdd = (JSONObject) jsonParserAdd.parse(addval);
		JSONArray jsonArrayAdd = (JSONArray) jsonObjAdd.get("val");
		List<ChartVo> listAdd = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArrayAdd.size(); i++) {
			
			
			JSONObject tempObj = (JSONObject) jsonArrayAdd.get(i);

			ChartVo chartVo = new ChartVo();
			chartVo.setDvcId(tempObj.get("dvcId").toString());
			chartVo.setTgCyl(tempObj.get("tgCyl").toString());
			chartVo.setTgRunTime(tempObj.get("tgRunTime").toString());
			chartVo.setTgDate(tempObj.get("tgDate").toString());
			chartVo.setType(tempObj.get("type").toString());
			chartVo.setEmpCd(tempObj.get("empCd").toString());
			chartVo.setCntPerCyl(tempObj.get("cntPerCyl").toString());

			chartVo.setStartTime(tempObj.get("startTime").toString());
			chartVo.setEndTime(tempObj.get("endTime").toString());
			
			System.out.println("test : " +tempObj.get("count").toString());
			chartVo.setCount(Integer.parseInt(tempObj.get("count").toString()));

			listAdd.add(chartVo);
		}
		
		//capa 등록
		JSONParser jsonParserCapa = new JSONParser();
		JSONObject jsonObjCapa = (JSONObject) jsonParserCapa.parse(capaval);
		JSONArray jsonArrayCapa = (JSONArray) jsonObjCapa.get("val");
		List<ChartVo> listCapa = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArrayCapa.size(); i++) {
			
			
			
			JSONObject tempObj = (JSONObject) jsonArrayCapa.get(i);

			ChartVo chartVo = new ChartVo();
			chartVo.setDvcId(tempObj.get("dvcId").toString());
			chartVo.setCapa(tempObj.get("capa").toString());
			chartVo.setName(tempObj.get("name").toString());
			chartVo.setWorkIdx(Integer.parseInt(tempObj.get("type").toString()));
			listCapa.add(chartVo);
		}
		
		ChartVo chartVo = new ChartVo();
		chartVo.setDate(list.get(0).getTgDate());
		String str = "";

		try {
			int exist = (Integer) sqlSessionTemplate.selectOne(namespace_chart + "cntDuplData_nd",
					list.get(0).getTgDate());
			/*if (exist > 0) {
				//기존작업자
				sqlSessionTemplate.delete(namespace_chart + "delTargetCnt_nd", list.get(0).getTgDate());
				//추가작업자
				sqlSessionTemplate.delete(namespace_chart + "delTargetCnt_nd_add", list.get(0).getTgDate());
			}*/
			
			//기존작업자
			sqlSessionTemplate.insert(namespace_chart + "addTargetCnt", list);
			//추가작업자
			if(listAdd.size()>0) {
				System.out.println("chk listAdd ::" + listAdd);
				sqlSessionTemplate.insert(namespace_chart + "addTargetCnt_add", listAdd);
			}
//			System.out.println("services ::"+listAdd);
			
			//capa 변경
			sqlSessionTemplate.insert(namespace_chart + "capaAdd", listCapa);
			
			int delchk = (Integer) sqlSessionTemplate.selectOne(namespace_chart + "dataChk", chartVo);	//실시간 수량데이터 정보에 값을 가지고있는지 확인(10일이전까지만 가지고있음)
//			System.out.println("체크한 수량은 ::" + delchk);
			//작업자 변경시 삭제 후 수량변경
			if(delchk>0) {
				sqlSessionTemplate.delete(namespace_chart + "deleteWorkerCnt", chartVo);
				sqlSessionTemplate.insert(namespace_chart + "changeWorkerCnt", chartVo);
				
				//endCnt 변경하기
				sqlSessionTemplate.update(namespace_chart + "changeEndCnt", chartVo);	//기존작업자
				sqlSessionTemplate.update(namespace_chart + "changeEndCntAdd", chartVo);//추가작업자
				
			}
			
			//이미 실적보고 후 작업자가 변경되었을때 변경된 작업자수량을 삭제시키는 로직(HST_PRD_ADD 작업실적 보고된 테이블)
			sqlSessionTemplate.delete(namespace_chart + "updateHSTPRDADD",chartVo);
			
			str = "success";
		} catch (Exception e) {
			e.printStackTrace();
			str = "fail";
		}

		return str;
	}
	
	@Override
	public String insertTgndExtra(String val) throws Exception {
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");
		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			ChartVo chartVo = new ChartVo();
			chartVo.setDvcId(tempObj.get("dvcId").toString());
			chartVo.setPrdNo(tempObj.get("prdNo").toString());
			chartVo.setRouting(tempObj.get("routing").toString());
			chartVo.setTgCntD(tempObj.get("tgCntD").toString());
			chartVo.setTgCntN(tempObj.get("tgCntN").toString());
			chartVo.setTgRunTimeD(tempObj.get("tgRunTimeD").toString());
			chartVo.setTgRunTimeN(tempObj.get("tgRunTimeN").toString());
			chartVo.setCapa(tempObj.get("capa").toString());
			chartVo.setUph(tempObj.get("uph").toString());
			chartVo.setEmpN(tempObj.get("empN").toString());
			chartVo.setEmpD(tempObj.get("empD").toString());
			chartVo.setTgDate(tempObj.get("tgDate").toString());
			chartVo.setMultiPrdNo(Integer.parseInt(tempObj.get("multiPrdNo").toString()));
			chartVo.setCntPerCyl(tempObj.get("cntPerCyl").toString());

			list.add(chartVo);
		}

		String str = "";

		try {
			sqlSessionTemplate.delete(namespace_chart + "delTgndExtra", list.get(0).getTgDate());
			sqlSessionTemplate.insert(namespace_chart + "insertTgndExtra", list);
			str = "success";
		} catch (Exception e) {
			e.printStackTrace();
			str = "fail";
		}

		return str;
	}

	@Override
	public String waitTimeList(ChartVo chartVo) throws Exception {
		List<ChartVo> dataList = sqlSessionTemplate.selectList(namespace + "waitTimeList", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("chartStatus", dataList.get(i).getChartStatus());
			map.put("sDate", dataList.get(i).getSDate());
			map.put("eDate", dataList.get(i).getEDate());
			map.put("workDate", dataList.get(i).getWorkDate());

			list.add(map);
		}
		;

		String str = "";
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	@Transactional(readOnly=true)
	public String getDvcIdByPrdNo(ChartVo chartVo) throws Exception {
		logger.debug("====================================");
		logger.debug(chartVo.getPrdNo());
		logger.debug("====================================");
		List<ChartVo> dataList = sqlSessionTemplate.selectList(namespace + "getDvcIdByPrdNo", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("name", dataList.get(i).getName());
			map.put("dvcId", dataList.get(i).getDvcId());
			list.add(map);
		};

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}
	
	@Override
	public String dvcWorkerlist(int empCd) throws Exception {
		// TODO Auto-generated method stub
		
		List list = new ArrayList<ChartVo>();
		List<ChartVo> dataList = sqlSessionTemplate.selectList(namespace +"dvcWorkerlist",empCd);
		for (int i=0; i<dataList.size(); i++) {
			Map map = new HashMap();
			map.put("empCd", dataList.get(i).getEmpCd());
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("idx", dataList.get(i).getIdx());
			list.add(map);
		}
		
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String saveWorkerList(String val) throws Exception {
		// TODO Auto-generated method stub
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<ChartVo> list = new ArrayList<ChartVo>();
		List<ChartVo> updateList = new ArrayList<ChartVo>();
		String str = "";
		
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);
			ChartVo chartVo = new ChartVo();
			if(tempObj.get("idx").toString().equals("")) {
				chartVo.setDvcId(tempObj.get("dvcId").toString());
				chartVo.setEmpCd(tempObj.get("empCd").toString());
				list.add(chartVo);
			}else {
				chartVo.setDvcId(tempObj.get("dvcId").toString());
				chartVo.setEmpCd(tempObj.get("empCd").toString());
				chartVo.setIdx(tempObj.get("idx").toString());
				updateList.add(chartVo);
			}
		}
		if(list.size()>0) {
			sqlSessionTemplate.insert(namespace + "insertWorkerList", list);
		}
		if(updateList.size()>0) {
			sqlSessionTemplate.update(namespace + "saveWorkerList", updateList);
		}
//		sqlSessionTemplate.insert(namespace_chart + "saveWorkerList", list);

		str="success";
		return str;
	}

	@Override
	public String delWorkerList(String idx) throws Exception {
		String str="";
		sqlSessionTemplate.delete(namespace + "delWorkerList", idx);
		// TODO Auto-generated method stub
		
		str="success";
		return str;
	}

	@Override
	public String dvcToWorkerList(ChartVo chartVo) throws Exception {
		List<ChartVo> dataList = sqlSessionTemplate.selectList(namespace + "dvcToWorkerList", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();

			map.put("id", dataList.get(i).getId());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);

		return str;
	}

	@Override
	public String test(String val) throws Exception {
		// TODO Auto-generated method stub
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<ChartVo> list = new ArrayList<ChartVo>();
		List<ChartVo> updateList = new ArrayList<ChartVo>();
		String str = "";
		
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);
			ChartVo chartVo = new ChartVo();
			
			chartVo.setDvcId(tempObj.get("dvcId").toString());
			chartVo.setEmpCd(tempObj.get("empCd").toString());
			list.add(chartVo);
		}
		
		return null;
	}

	@Override
	public String workerDvclist(int dvcId) throws Exception {
		// TODO Auto-generated method stub
		
		List list = new ArrayList<ChartVo>();
		List<ChartVo> dataList = sqlSessionTemplate.selectList(namespace +"workerDvclist",dvcId);
		for (int i=0; i<dataList.size(); i++) {
			Map map = new HashMap();
			map.put("empCd", dataList.get(i).getEmpCd());
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("idx", dataList.get(i).getIdx());
			list.add(map);
		}
		
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String checkIdPw(ChartVo chartVo) throws Exception {
		
		List list = new ArrayList<ChartVo>();
		List<ChartVo> dataList = sqlSessionTemplate.selectList(namespace +"checkIdPw",chartVo);
		
		for (int i=0; i<dataList.size(); i++) {
			Map map = new HashMap();
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("empCd", dataList.get(i).getEmpCd());
			map.put("lv", dataList.get(i).getLv());
			list.add(map);
		}
		
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String checkPartCnt(ChartVo chartVo) throws Exception {
		
		List list = new ArrayList<ChartVo>();
		
		List<ChartVo> dataList = sqlSessionTemplate.selectList(namespace +"checkPartCnt",chartVo);
		
		for (int i=0; i<dataList.size(); i++) {
			Map map = new HashMap();
			
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("minPartCyl", dataList.get(i).getMinPartCyl());
			map.put("maxPartCyl", dataList.get(i).getMaxPartCyl());
			map.put("cpc", dataList.get(i).getCpc());
			map.put("dvcId", dataList.get(i).getDvcId());
			list.add(map);
		}
		
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getPartCnt(ChartVo chartVo) throws Exception {
		List list = new ArrayList<ChartVo>();
		
		List<ChartVo> dataList = sqlSessionTemplate.selectList(namespace +"getPartCnt",chartVo);
		
		for (int i=0; i<dataList.size(); i++) {
			Map map = new HashMap();
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("partCyl", dataList.get(i).getPartCyl());
			map.put("date", dataList.get(i).getDate());
			list.add(map);
		}
		
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String deleteJobSplit(ChartVo chartVo) throws Exception {
		// TODO Auto-generated method stub
		sqlSessionTemplate.delete(namespace_chart+"deleteJobSplit",chartVo);
		
		System.out.println("service ::"+chartVo.getGroup());
		System.out.println("service ::"+chartVo.getDvcId());
		System.out.println("service ::"+chartVo.getTgDate());
		return null;
	}

}
