package com.unomic.dulink.chart.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.unomic.dulink.chart.domain.FileDTO;
import com.unomic.dulink.chart.domain.UploadedFile;
import com.unomic.dulink.chart.service.ChartService;
/**
 * Handles requests for the application home page.
 */
/**
 * Handles requests for the application home page.
 */
@RequestMapping(value = "/file")
@Controller
public class FileUpLoadController {

	private static final Logger logger = LoggerFactory.getLogger(FileUpLoadController.class);
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@Autowired
	private ChartService chartService; 
	
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	@ResponseBody
    public String fileSubmit(FileDTO dto) {
		String str = "";
        MultipartFile uploadfile = dto.getUploadfile();
        
        if (uploadfile != null) {
            String fileName = uploadfile.getOriginalFilename();
            dto.setFileName(fileName);
            try {
                // 1. FileOutputStream 사용
                // byte[] fileData = file.getBytes();
                // FileOutputStream output = new FileOutputStream("C:/images/" + fileName);
                // output.write(fileData);
                 
                // 2. File 사용
            	String name = String.valueOf(System.currentTimeMillis());
            	String last_name = fileName.substring(fileName.lastIndexOf(".")); 
            			 
            	str = name + last_name;
            	File file = new File("/usr/local/apache-tomcat-7.0.72/webapps/img/" + str);
            	//File file = new File("/Users/jeongwan/Desktop/" + str);
                uploadfile.transferTo(file);
            } catch (IOException e) {
                e.printStackTrace();
            } // try - catch
        } // if
        // 데이터 베이스 처리를 현재 위치에서 처리
        return str;
    }
};


