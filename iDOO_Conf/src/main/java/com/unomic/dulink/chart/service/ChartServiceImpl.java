package com.unomic.dulink.chart.service;

import java.beans.Encoder;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;
import javax.sound.sampled.AudioFormat.Encoding;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.SqlSession;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;

import com.unomic.dulink.chart.domain.ChartVo;
import com.unomic.dulink.chart.domain.DivisionVo;
import com.unomic.dulink.chart.domain.TimeChartVo;
import com.unomic.dulink.common.domain.CommonCode;
import com.unomic.dulink.common.domain.CommonFunction;
import com.unomic.dulink.svg.domain.SVGVo;

@Service
@Repository
public class ChartServiceImpl extends SqlSessionDaoSupport implements ChartService {
	private final static String namespace = "com.unomic.dulink.chart.";

	@Inject
	SqlSession sqlSessionTemplate;

	Map statusMap = new HashMap();
	int chartOrder = 0;

	private ArrayList list;

	@Override
	public String createUser(String models) throws Exception {
		
		
		SqlSession sql = getSqlSession();
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(models);
		ChartVo chartVo = new ChartVo();

		chartVo.setId(jsonObj.get("name").toString());
		chartVo.setPassword(jsonObj.get("password").toString());
		chartVo.setLv(jsonObj.get("level").toString());
		chartVo.setShopId(Integer.parseInt(jsonObj.get("shopId").toString()));
		
		try {
			String idx = (String) sql.selectOne(namespace + "createUser", chartVo);
			chartVo.setIdx(idx);
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}

		List list = new ArrayList();
		Map map = new HashMap();
		map.put("name", chartVo.getId());
		map.put("password", chartVo.getPassword());
		map.put("level", chartVo.getLv());
		map.put("shopId", chartVo.getShopId());
		map.put("idx", chartVo.getIdx());
		

		list.add(map);
		String str = "";

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(list);
		return str;
	}
	
	@Override
	public void deleteUser(String models) throws Exception {
		SqlSession sql = getSqlSession();
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(models);
		ChartVo chartVo = new ChartVo();
		chartVo.setIdx(jsonObj.get("IDX").toString());
		chartVo.setName(jsonObj.get("name").toString());
		chartVo.setPassword(jsonObj.get("password").toString());
		chartVo.setLv(jsonObj.get("level").toString());

		try {
			sql.delete(namespace + "deleteUser", chartVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public String updateUser(String models) throws Exception {
		SqlSession sql = getSqlSession();
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(models);
		ChartVo chartVo = new ChartVo();
		
		chartVo.setId(jsonObj.get("name").toString());
		chartVo.setPassword(jsonObj.get("password").toString());
		chartVo.setLv(jsonObj.get("level").toString());
		chartVo.setIdx(jsonObj.get("IDX").toString());
		chartVo.setShopId(Integer.parseInt(jsonObj.get("shopId").toString()));

		try {
			sql.update(namespace + "updateUser", chartVo);
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}

		return models;
	}
	
	@Override
	public String getUserList(String shopId) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getUserList", shopId);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("IDX", dataList.get(i).getIdx());
			map.put("name", dataList.get(i).getId());
			map.put("password", dataList.get(i).getPassword());
			map.put("level", dataList.get(i).getLv());
			map.put("shopId", dataList.get(i).getShopId());
			list.add(map);
		};

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(list);
		return str;
	}
	
	public boolean chkOrderStatus(String status) {
		boolean result = false;
		// if(chartOrder<=Integer.parseInt((String) statusMap.get(status))){
		// chartOrder = Integer.parseInt((String) statusMap.get(status));
		// result = true;
		// };
		return result;
	};

	@Override
	public String login(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		ChartVo accountInfo = new ChartVo();
		try {
			accountInfo = (ChartVo) sql.selectOne(namespace + "login", chartVo);
			if (accountInfo.getId() != null) {
				Map map = new HashMap();

				map.put("id", accountInfo.getId());
				map.put("level", "1");
				map.put("name", URLEncoder.encode(accountInfo.getName(), "UTF-8"));
				map.put("message", "success");

				ObjectMapper om = new ObjectMapper();
				str = om.defaultPrettyPrintingWriter().writeValueAsString(map);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Map map = new HashMap();
			map.put("message", "failed");
			ObjectMapper om = new ObjectMapper();
			str = om.defaultPrettyPrintingWriter().writeValueAsString(map);

		}

		return str;
	}

	@Override
	public String getMachineInfo(SVGVo svgVo) throws Exception {
		SqlSession sql = getSqlSession();

		List<SVGVo> machineList = sql.selectList(namespace + "getMachineInfo", svgVo);
		List list = new ArrayList();

		for (int i = 0; i < machineList.size(); i++) {
			Map map = new HashMap();
			map.put("id", ((SVGVo) machineList.get(i)).getId());
			map.put("dvcId", ((SVGVo) machineList.get(i)).getDvcId());

			String name = "";
			if (machineList.get(i).getName() != null) {
				name = URLEncoder.encode(machineList.get(i).getName(), "UTF-8");
				name = name.replaceAll("\\+", "%20");
			}
			;

			map.put("name", name);
			map.put("x", machineList.get(i).getX());
			map.put("y", machineList.get(i).getY());
			map.put("w", machineList.get(i).getW());
			map.put("h", machineList.get(i).getH());
			map.put("pic", machineList.get(i).getPic());
			map.put("notUse", machineList.get(i).getNotUse());
			map.put("fontSize", machineList.get(i).getFontSize());
			map.put("workDate", machineList.get(i).getWorkDate());
			map.put("lastChartStatus", machineList.get(i).getLastChartStatus());
			map.put("operationTime", machineList.get(i).getOperationTime());
			map.put("isChg", machineList.get(i).getIsChg());
			map.put("tgCyl", machineList.get(i).getTgCyl());
			map.put("cntCyl", machineList.get(i).getCntCyl());
			map.put("worker", URLEncoder.encode(machineList.get(i).getWorker(), "utf-8"));

			list.add(map);
		}
		;

		String str = "";

		Map resultMap = new HashMap();
		resultMap.put("machineList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);

		return str;
	}

	@Override
	public String setCirclePos(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		try {
			sql.update(namespace + "setCirclePos", chartVo);
			str = "success";
		} catch (Exception e) {
			e.printStackTrace();
			str = "fail";
		}

		return str;
	}

	public String addZero(String str) {
		if (str.length() == 1) {
			return "0" + str;
		} else {
			return str;
		}
	};

	@Override
	public String getStatusData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		return (String) sql.selectOne(namespace + "getStatusData", chartVo);
	};

	@Override
	public ChartVo getCurrentDvcData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		chartVo = (ChartVo) sql.selectOne(namespace + "getCurrentDvcData", chartVo);
		// chartVo.setEndDateTime((String)sql.selectOne(namespace + "getLastUpdateTime",
		// chartVo));

		if (chartVo == null) {
			chartVo = new ChartVo();
			chartVo.setChartStatus("null");
		}
		;

		return chartVo;
	};

	@Override
	public void addData() throws Exception {
		SqlSession sql = getSqlSession();
		ChartVo chartVo = new ChartVo();

		for (int i = 0; i <= 1; i++) {
			String status = "";
			int random = (int) (Math.random() * 10 + 1) + (int) (Math.random() * 10 + 1)
					+ (int) (Math.random() * 10 + 1) + (int) (Math.random() * 10 + 1);
			if (random == 40) {
				status = "Alarm";
			} else if (random >= 38) {
				status = "Wait";
			} else {
				status = "In-Cycle";
			}
			;

			Date d = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String date = sdf.format(d);

			SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String time = sdf2.format(d);

			String spdLoad = String
					.valueOf(Math
							.floor((Math.random() * 10 + 1) + (Math.random() * 10 + 1) + (Math.random() * 10 + 1) + 60))
					.substring(0, 2);
			if (spdLoad.equals("90"))
				spdLoad = "0";
			String feedOverride = String
					.valueOf(Math
							.floor((Math.random() * 10 + 1) + (Math.random() * 10 + 1) + (Math.random() * 10 + 1) + 60))
					.substring(0, 2);
			if (feedOverride.equals("90"))
				spdLoad = "0";

			int dvcId = 0, adtId = 0;
			if (i == 0) {
				dvcId = 1;
				adtId = 13;
			} else if (i == 1) {
				dvcId = 5;
				adtId = 14;
			}

			chartVo.setDvcId(Integer.toString(dvcId));
			chartVo.setLastUpdateTime(time);
			chartVo.setStartDateTime(time);
			chartVo.setEndDateTime(time);
			chartVo.setAdtId(Integer.toString(adtId));
			chartVo.setSpdLoad(spdLoad);
			chartVo.setChartStatus(status);
			chartVo.setFeedOverride(feedOverride);
			chartVo.setDate(date);

			String chartStatus = (String) sql.selectOne(namespace + "getLastChartStatus", chartVo);

			if (chartStatus.equals(status)) {
				sql.update(namespace + "updateDeviceStatus", chartVo);
			} else {
				sql.update(namespace + "updateDeviceStatus", chartVo);
				chartVo.setStartDateTime((String) sql.selectOne(namespace + "getStartDateTime", chartVo));
				sql.update(namespace + "setChartEndTime", chartVo);
				chartVo.setStartDateTime(time);
				sql.insert(namespace + "addChartStatus", chartVo);
			}
		}
		;
	}

	@Override
	public String getDvcName(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String dvcName = (String) sql.selectOne(namespace + "getDvcName", chartVo);
		return dvcName;
	}

	@Override
	public String getAllDvcStatus(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> chartData = sql.selectList(namespace + "getAllDvcStatus", chartVo);
		List list = new ArrayList();
		for (int i = 0; i < chartData.size(); i++) {
			Map map = new HashMap();
			map.put("adt_id", chartData.get(i).getAdtId());
			map.put("startDateTime", chartData.get(i).getStartDateTime());
			map.put("endDateTime", chartData.get(i).getEndDateTime());
			map.put("chartStatus", chartData.get(i).getChartStatus());
			map.put("alarm", chartData.get(i).getAlarm());
			map.put("spdLoad", chartData.get(i).getSpdLoad());
			map.put("spdActualSpeed", chartData.get(i).getSpdActualSpeed());
			map.put("feedOverride", chartData.get(i).getFeedOverride());
			map.put("actualFeed", chartData.get(i).getActualFeed());
			map.put("date", chartData.get(i).getDate());
			map.put("name", chartData.get(i).getName());
			map.put("dvcId", chartData.get(i).getDvcId());

			list.add(map);
		}
		;

		String str = "";

		Map resultMap = new HashMap();
		resultMap.put("chartStatus", list);
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);
		return str;
	}

	@Override
	public String getAllDvcId(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dvcIdList = sql.selectList(namespace + "getAllDvcId", chartVo);

		List list = new ArrayList();

		for (int i = 0; i < dvcIdList.size(); i++) {
			Map map = new HashMap();
			map.put("dvcId", dvcIdList.get(i).getDvcId());
			map.put("name", dvcIdList.get(i).getName());
			map.put("chartStatus", dvcIdList.get(i).getChartStatus());
			map.put("operationTime", dvcIdList.get(i).getOperationTime());

			String jig = "";
			if (dvcIdList.get(i).getJig() != null) {
				jig = URLEncoder.encode(dvcIdList.get(i).getJig(), "UTF-8");
				jig = jig.replaceAll("\\+", "%20");
			}
			;

			String wc = "";
			if (dvcIdList.get(i).getWC() != null) {
				wc = URLEncoder.encode(dvcIdList.get(i).getWC(), "UTF-8");
				wc = wc.replaceAll("\\+", "%20");
			}
			;

			map.put("jig", jig);
			map.put("WC", wc);

			list.add(map);
		}
		;

		Map resultMap = new HashMap();
		resultMap.put("dvcId", list);
		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);

		return str;
	}

	@Override
	public String polling1() throws Exception {
		SqlSession sql = getSqlSession();
		return (String) sql.selectOne(namespace + "getVideoMachine");
	};

	@Override
	public String setVideo(String id) throws Exception {
		SqlSession sql = getSqlSession();
		String rtn = "";

		try {
			sql.update(namespace + "setVideo", id);
			rtn = "success";
		} catch (Exception e) {
			rtn = "fail";
		}
		;
		return rtn;
	};

	@Override
	public void initVideoPolling() throws Exception {
		SqlSession sql = getSqlSession();
		sql.update(namespace + "initVideoPolling");
	};

	@Override
	public void initPiePolling() throws Exception {
		SqlSession sql = getSqlSession();
		sql.update(namespace + "initPiePolling");
	}

	@Override
	public String piePolling1() throws Exception {
		SqlSession sql = getSqlSession();
		return (String) sql.selectOne(namespace + "piePolling1");
	};

	@Override
	public String piePolling2() throws Exception {
		SqlSession sql = getSqlSession();
		return (String) sql.selectOne(namespace + "piePolling2");
	};

	@Override
	public String setChart1(String id) throws Exception {
		SqlSession sql = getSqlSession();
		String rtn = "";

		try {
			sql.update(namespace + "setChart1", id);
			rtn = "success";
		} catch (Exception e) {
			rtn = "fail";
		}
		return rtn;
	};

	@Override
	public String setChart2(String id) throws Exception {
		SqlSession sql = getSqlSession();
		String rtn = "";

		try {
			sql.update(namespace + "setChart2", id);
			rtn = "success";
		} catch (Exception e) {
			rtn = "fail";
		}
		return rtn;
	}

	@Override
	public void delVideoMachine(String id) throws Exception {
		SqlSession sql = getSqlSession();
		sql.update(namespace + "delVideoMachine", id);
	}

	@Override
	public void delChartMachine(String id) throws Exception {
		SqlSession sql = getSqlSession();
		String machine = (String) sql.selectOne(namespace + "chkChartMachine1", id);

		if (machine == null) {
			sql.update(namespace + "delChartMachine2");
		} else {
			sql.update(namespace + "delChartMachine1");
		}
		;
	}

	@Override
	public void test(ChartVo chartVo) throws Exception {

		SqlSession sql = getSqlSession();
		List<ChartVo> chartData = sql.selectList(namespace + "getAllDvcStatus", chartVo);
	}

	@Override
	public String getAdapterId(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dvcIdList = sql.selectList(namespace + "getAdapterId", chartVo);

		List list = new ArrayList();

		for (int i = 0; i < dvcIdList.size(); i++) {
			Map map = new HashMap();
			map.put("dvcId", dvcIdList.get(i).getDvcId());
			map.put("name", dvcIdList.get(i).getName());
			map.put("chartStatus", dvcIdList.get(i).getChartStatus());
			map.put("operationTime", dvcIdList.get(i).getOperationTime());
			map.put("adtId", dvcIdList.get(i).getAdtId());

			list.add(map);
		}
		;

		Map resultMap = new HashMap();
		resultMap.put("dvcId", list);
		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);

		return str;
	}

	@Override
	public String getBarChartDvcId(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dvcIdList = sql.selectList(namespace + "getBarChartDvcId", chartVo);

		List list = new ArrayList();

		for (int i = 0; i < dvcIdList.size(); i++) {
			Map map = new HashMap();
			map.put("dvcId", dvcIdList.get(i).getDvcId());
			map.put("name", dvcIdList.get(i).getName());
			map.put("chartStatus", dvcIdList.get(i).getChartStatus());
			map.put("operationTime", dvcIdList.get(i).getOperationTime());

			list.add(map);
		}
		;

		Map resultMap = new HashMap();
		resultMap.put("dvcId", list);
		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);

		return str;
	}

	@Override
	public String showDetailEvent(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> chartData = sql.selectList(namespace + "showDetailEvent", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < chartData.size(); i++) {
			Map map = new HashMap();
			map.put("startDateTime", chartData.get(i).getStartDateTime());
			map.put("endDateTime", chartData.get(i).getEndDateTime());
			map.put("chartStatus", chartData.get(i).getChartStatus());
			map.put("alarm", chartData.get(i).getAlarm());
			map.put("spdLoad", chartData.get(i).getSpdLoad());
			map.put("feedOverride", chartData.get(i).getFeedOverride());

			list.add(map);
		}
		;

		String str = "";
		Map resultMap = new HashMap();
		resultMap.put("chartData", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);

		return str;
	}

	@Override
	public String getDetailStatus(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();

		List<ChartVo> chartData = sql.selectList(namespace + "getDetailChart", chartVo);

		List statusList = new ArrayList();
		for (int i = 0; i < chartData.size(); i++) {
			Map map = new HashMap();
			map.put("adt_id", chartData.get(i).getAdtId());
			map.put("startDateTime", chartData.get(i).getStartDateTime());
			map.put("endDateTime", chartData.get(i).getEndDateTime());
			map.put("chartStatus", chartData.get(i).getChartStatus());

			statusList.add(map);
		}
		;

		Map map = new HashMap();
		map.put("chartStatus", statusList);

		String str = "";
		ObjectMapper om = new ObjectMapper();

		str = om.defaultPrettyPrintingWriter().writeValueAsString(map);
		return str;
	}

	@Override
	public ChartVo getOtherChartsData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		chartVo = (ChartVo) sql.selectOne(namespace + "getOtherChartsData", chartVo);
		return chartVo;
	};

	// 로직 구성. 1부터 144까지 빈칸 만들기.
	// default no-connection
	// 현재시간 10분 전까지 계산.
	// 같은 시간 있으면 넣기.
	// 들어간 날짜로 조회하는 루틴 필요.
	@Override
	public List<TimeChartVo> getTimeChartData(ChartVo inputVo) {
		SqlSession sql = getSqlSession();
		List<ChartVo> listChartData = sql.selectList(namespace + "getTimeChartData", inputVo);

		if (listChartData.size() < 1) {
			return null;
		}

		List<TimeChartVo> tmpTimeChart = new ArrayList();
		int CHART_SIZE = 144;

		String stTime = CommonFunction.getChartStartTime(inputVo.getTargetDateTime());
		Long unixTime = CommonFunction.dateTime2Mil(stTime);
		String stDate = stTime;

		// 10분 추가.
		String edDate = CommonFunction.unixTime2Datetime(CommonFunction.dateTime2Mil(stDate) + 600000L);
		for (int i = 0; i < CHART_SIZE; i++) {

			TimeChartVo tmpVo = new TimeChartVo();
			tmpVo.setStartTime(stDate);
			tmpVo.setEndTime(edDate);
			tmpVo.setY(CommonCode.MSG_CHART_DIST);

			Iterator<ChartVo> ite = listChartData.iterator();
			tmpVo.setColor(CommonCode.MSG_COLOR_NO_CONNECTION);
			while (ite.hasNext()) {
				ChartVo tmpChartVo = ite.next();
				if (CommonFunction.cutRight(tmpChartVo.getStartDateTime(), 2).equals(tmpVo.getStartTime())) {
					if (tmpChartVo.getChartStatus().equals(CommonCode.MSG_ALARM)) {
						tmpVo.setColor(CommonCode.MSG_COLOR_ALARM);
					} else if (tmpChartVo.getChartStatus().equals(CommonCode.MSG_WAIT)) {
						tmpVo.setColor(CommonCode.MSG_COLOR_WAIT);
					} else if (tmpChartVo.getChartStatus().equals(CommonCode.MSG_IN_CYCLE)) {
						tmpVo.setColor(CommonCode.MSG_COLOR_IN_CYCLE);
					}
				}

				// 가져온 마지막 데이터일때... loop 탈출.
				if (!ite.hasNext()
						&& CommonFunction.cutRight(tmpChartVo.getStartDateTime(), 2).equals(tmpVo.getStartTime())) {
					tmpTimeChart.add(tmpVo);

					return tmpTimeChart;

				}
			}
			stDate = edDate;
			edDate = CommonFunction.unixTime2Datetime(CommonFunction.dateTime2Mil(stDate) + 600000L);
			tmpTimeChart.add(tmpVo);
		}
		return tmpTimeChart;
	};

	@Override
	public List<ChartVo> testTimeChartData(ChartVo inputVo) {
		SqlSession sql = getSqlSession();
		List<ChartVo> listChartData = sql.selectList(namespace + "getTimeChartData", inputVo);

		return listChartData;
	}

	@Override
	public String getJicList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> jigList = sql.selectList(namespace + "getJicList", chartVo);

		List jigs = new ArrayList();
		for (int i = 0; i < jigList.size(); i++) {
			Map map = new HashMap();
			map.put("jig", URLEncoder.encode(jigList.get(i).getJig(), "utf-8"));

			jigs.add(map);
		}
		;

		Map jigMap = new HashMap();
		jigMap.put("jigList", jigs);

		String str = "";
		ObjectMapper ob = new ObjectMapper();
		str = ob.defaultPrettyPrintingWriter().writeValueAsString(jigMap);
		return str;
	};

	@Override
	public String getWcList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();

		List<ChartVo> jigList = sql.selectList(namespace + "getWcList", chartVo);

		List jigs = new ArrayList();
		for (int i = 0; i < jigList.size(); i++) {
			Map map = new HashMap();
			map.put("WC", URLEncoder.encode(jigList.get(i).getWC(), "utf-8"));

			jigs.add(map);
		}
		;

		Map jigMap = new HashMap();
		jigMap.put("wcList", jigs);

		String str = "";
		ObjectMapper ob = new ObjectMapper();
		str = ob.defaultPrettyPrintingWriter().writeValueAsString(jigMap);
		return str;
	}

	@Override
	public String getTableData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getDataList", chartVo);

		List list = new ArrayList();

		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
//			URLDecoder.decode(
			map.put("name", URLDecoder.decode(dataList.get(i).getName()));
			map.put("target_time", dataList.get(i).getTargetDateTime());
			map.put("inCycle_time", dataList.get(i).getInCycleTime());
			map.put("wait_time", dataList.get(i).getWaitTime());
			map.put("alarm_time", dataList.get(i).getAlarmTime());
			map.put("noConnTime", dataList.get(i).getNoConnectionTime());
			// map.put("line", dataList.get(i).getLine());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("tableData", list);

		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getWcData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> wcList = sql.selectList(namespace + "getWcData", chartVo);

		List list = new ArrayList();

		for (int i = 0; i < wcList.size(); i++) {
			Map map = new HashMap();
			map.put("name", URLEncoder.encode(wcList.get(i).getName(), "utf-8"));
			map.put("target_time", wcList.get(i).getTargetDateTime());
			map.put("inCycle_time", wcList.get(i).getInCycleTime());
			map.put("wait_time", wcList.get(i).getWaitTime());
			map.put("alarm_time", wcList.get(i).getAlarmTime());
			map.put("noConnTime", wcList.get(i).getNoConnectionTime());
			map.put("workDate", wcList.get(i).getWorkDate());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("wcList", list);

		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getWcDataByDvc(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> wcList = sql.selectList(namespace + "getWcDataByDvc", chartVo);

		List list = new ArrayList();

		for (int i = 0; i < wcList.size(); i++) {
			Map map = new HashMap();

			map.put("target_time", wcList.get(i).getTargetDateTime());
			map.put("inCycle_time", wcList.get(i).getInCycleTime());
			map.put("wait_time", wcList.get(i).getWaitTime());
			map.put("alarm_time", wcList.get(i).getAlarmTime());
			map.put("noConnTime", wcList.get(i).getNoConnectionTime());
			map.put("workDate", wcList.get(i).getWorkDate());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("wcList", list);

		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}
	
	@Override
	public String getComStartTime(String shopId) throws Exception {
		SqlSession sql = getSqlSession();
		String str = (String) sql.selectOne(namespace + "getComStartTime", shopId);
		return str;
	}
	
	@Override
	public String getStatusExpression(String dvcId) throws Exception {
		SqlSession sql = getSqlSession();
		
		List <ChartVo> stateList = sql.selectList(namespace + "getStatusExpression",dvcId);
		List list=new ArrayList();
		
		for(int i = 0; i < stateList.size(); i++){
			Map map = new HashMap();
			map.put("type", ((ChartVo) stateList.get(i)).getType()); 
			map.put("status", ((ChartVo) stateList.get(i)).getStatus());
			map.put("statement", ((ChartVo) stateList.get(i)).getStatement());
			list.add(map);
		};
		 
		String str="";
		
		Map resultMap=new HashMap();
		resultMap.put("statementList", list);
		
		ObjectMapper om = new ObjectMapper();
		str=om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);
		
		return str;
	}
	
	@Override
	public String setUtc(ChartVo chartVo) throws Exception {
		String result="fail";
		
		SqlSession sql = getSqlSession();
		try {
			sql.update(namespace+"setUtc", chartVo);
			result="success";
		}catch(Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	@Override
	public String getDivision(String shopId) throws Exception {
		SqlSession sql = getSqlSession();
		List<DivisionVo> dataList = sql.selectList(namespace + "getDivision", shopId);

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", dataList);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataList);
		return str;
	}

	
	@Override
	public String getUtc(ChartVo chartVo) throws Exception {
		String result="fail";
		
		SqlSession sql = getSqlSession();
		try {
			result = (String) sql.selectOne(namespace+"getUtc", chartVo);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	
	@Override
	public String getLampExpression() throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> lampExpressionList = sql.selectList(namespace + "getLampExpression");

		List list = new ArrayList();

		for (int i = 0; i < lampExpressionList.size(); i++) {
			Map map = new HashMap();
			map.put("dvcId", lampExpressionList.get(i).getDvcId());
			map.put("name", lampExpressionList.get(i).getName());
			map.put("redExpression", lampExpressionList.get(i).getRedExpression());
			map.put("redBlinkExpression", lampExpressionList.get(i).getRedBlinkExpression());
			map.put("yellowExpression", lampExpressionList.get(i).getYellowExpression());
			map.put("yellowBlinkExpression", lampExpressionList.get(i).getYellowBlinkExpression());
			map.put("greenExpression", lampExpressionList.get(i).getGreenExpression());
			map.put("greenBlinkExpression", lampExpressionList.get(i).getGreenBlinkExpression());

			list.add(map);
		}
		;

		Map resultMap = new HashMap();
		resultMap.put("dvcId", list);
		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);

		return str;
	}
	
	@Override
	public String setLampExpression(ChartVo chartVo) throws Exception {
		String status = "";

		SqlSession sql = getSqlSession();
		try {
			
			if(chartVo.getExpression()=="") {
				chartVo.setExpression(null);
			}
			
			int check = (int) sql.selectOne(namespace + "getLampExpressionCount", chartVo);
			if (check >= 1) {
				sql.update(namespace + "setLampExpression", chartVo);
			} else {
				sql.update(namespace + "setLampExpression", chartVo);
			}

			status = "success";
		} catch (Exception e) {
			e.printStackTrace();
			status = "fail";
		}

		// TODO 자동 생성된 메소드 스텁
		return status;
	}

	@Override
	public String getDvcList(String shopId) throws Exception {
		String result="fail";
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getDvcList", shopId);

		List list = new ArrayList();
		
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("name", dataList.get(i).getName());
			map.put("dvcId", dataList.get(i).getDvcId());
			list.add(map);
		};

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(list);
		return str;
		
	}

	@Override
	public String getAlarmData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();

		List<ChartVo> alarmList = sql.selectList(namespace + "getAlarmData", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < alarmList.size(); i++) {
			Map map = new HashMap();
			map.put("line", URLEncoder.encode(alarmList.get(i).getLine(), "utf-8"));
			map.put("name", URLEncoder.encode(alarmList.get(i).getName(), "utf-8"));
			map.put("startDateTime", alarmList.get(i).getStartDateTime());
			map.put("dvcId", alarmList.get(i).getDvcId());
			map.put("endDateTime", alarmList.get(i).getEndDateTime());
			map.put("delayTimeSec", alarmList.get(i).getDelayTimeSec());
			map.put("ncAlarmNum1", alarmList.get(i).getNcAlarmNum1());
			map.put("ncAlarmMsg1", URLEncoder.encode(alarmList.get(i).getNcAlarmMsg1(), "utf-8"));
			map.put("ncAlarmNum2", alarmList.get(i).getNcAlarmNum2());
			map.put("ncAlarmMsg2", URLEncoder.encode(alarmList.get(i).getNcAlarmMsg2(), "utf-8"));
			map.put("ncAlarmNum3", alarmList.get(i).getNcAlarmNum3());
			map.put("ncAlarmMsg3", URLEncoder.encode(alarmList.get(i).getNcAlarmMsg3(), "utf-8"));
			
			map.put("cause", alarmList.get(i).getCause());
			map.put("result", alarmList.get(i).getResult());
			map.put("empCd", alarmList.get(i).getEmpCd());
			map.put("date", alarmList.get(i).getDate());
			list.add(map);
		}
		;

		Map alarmMap = new HashMap();
		alarmMap.put("alarmList", list);

		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(alarmMap);

		return str;
	}

	@Override
	public String getJigList4Report(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dvcList = sql.selectList(namespace + "getJigList4Report", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dvcList.size(); i++) {
			Map map = new HashMap();
			map.put("name", URLEncoder.encode(dvcList.get(i).getName(), "utf-8"));
			list.add(map);
		}
		;

		Map dvcMap = new HashMap();
		dvcMap.put("dvcList", list);

		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dvcMap);

		return str;
	}

	@Override
	public String getAlarmList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getAlarmList", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("startDateTime", dataList.get(i).getStartDateTime());
			map.put("endDateTime", dataList.get(i).getEndDateTime());
			map.put("ncAlarmNum1", dataList.get(i).getNcAlarmNum1());
			map.put("ncAlarmMsg1", URLEncoder.encode(dataList.get(i).getNcAlarmMsg1(), "utf-8"));
			map.put("ncAlarmNum2", dataList.get(i).getNcAlarmNum2());
			map.put("ncAlarmMsg2", URLEncoder.encode(dataList.get(i).getNcAlarmMsg2(), "utf-8"));
			map.put("ncAlarmNum3", dataList.get(i).getNcAlarmNum3());
			map.put("ncAlarmMsg3", URLEncoder.encode(dataList.get(i).getNcAlarmMsg3(), "utf-8"));

			list.add(map);
		}
		;

		String str = "";
		Map dataMap = new HashMap();
		dataMap.put("alarmList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getStartTime(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String rtn = (String) sql.selectOne(namespace + "getStartTime", chartVo);
		return rtn;
	}

	@Override
	public String getMachineName(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = (String) sql.selectOne(namespace + "getMachineName", chartVo);

		return URLEncoder.encode(str);
	};

	@Override
	public String getTargetData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getTargetData", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("routing", URLEncoder.encode(dataList.get(i).getRouting(), "UTF-8"));
			map.put("prdNo", dataList.get(i).getPrdNo());
			// map.put("tgCnt", dataList.get(i).getTgCnt());
			// map.put("tgRunTime", dataList.get(i).getTgRunTime());
			// map.put("cntPerCyl", dataList.get(i).getCntPerCyl());
			map.put("tgCntD", dataList.get(i).getTgCntD());
			map.put("tgCntN", dataList.get(i).getTgCntN());
			map.put("cntPerCylD", dataList.get(i).getCntPerCylD());
			map.put("cntPerCylN", dataList.get(i).getCntPerCylN());
			map.put("tgRunTimeD", dataList.get(i).getTgRunTimeD());
			map.put("tgRunTimeN", dataList.get(i).getTgRunTimeN());
			map.put("empD", URLEncoder.encode(dataList.get(i).getEmpD(), "UTF-8"));
			map.put("empN", URLEncoder.encode(dataList.get(i).getEmpN(), "UTF-8"));
			map.put("empCdN", dataList.get(i).getEmpCdN());
			map.put("empCdD", dataList.get(i).getEmpCdD());
			map.put("capa", dataList.get(i).getCapa());
			map.put("uph", dataList.get(i).getUph());

			list.add(map);
		}
		;

		String str = "";
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getTimeData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> statusList = sql.selectList(namespace + "getTimeData", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < statusList.size(); i++) {
			Map map = new HashMap();
			map.put("status", statusList.get(i).getStatus());
			map.put("startDateTime", statusList.get(i).getStartDateTime());
			map.put("spdLoad", statusList.get(i).getSpdLoad());
			map.put("spdOverride", statusList.get(i).getFeedOverride());

			list.add(map);
		}
		;

		Map statusMap = new HashMap();
		statusMap.put("statusList", list);

		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(statusMap);
		return str;
	}

	@Override
	public String getDetailBlockData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getDetailBlockData", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("lastAlarmCode", dataList.get(i).getLastAlarmCode());
			map.put("lastAlarmMsg", dataList.get(i).getLastAlarmMsg());
			map.put("lastTgPrdctNum", dataList.get(i).getLastTgPrdctNum());
			map.put("lastFnPrdctNum", dataList.get(i).getLastFnPrdctNum());
			map.put("LastAvrCycleTime", dataList.get(i).getLastAvrCycleTime());
			map.put("tgCnt", dataList.get(i).getTgCnt());
			map.put("status", dataList.get(i).getStatus());
			map.put("prdctPerHour", dataList.get(i).getPrdctPerHour());

			map.put("prdctPerCyl", dataList.get(i).getPrdctPerCyl());
			map.put("remainCnt", dataList.get(i).getRemainCnt());
			map.put("feedOverride", dataList.get(i).getFeedOverride());
			map.put("spdLoad", dataList.get(i).getSpdLoad());
			map.put("ncAlarmNum1", dataList.get(i).getNcAlarmNum1());
			map.put("ncAlarmMsg1", URLEncoder.encode(dataList.get(i).getNcAlarmMsg1(), "utf-8"));
			map.put("ncAlarmNum2", dataList.get(i).getNcAlarmNum2());
			map.put("ncAlarmMsg2", URLEncoder.encode(dataList.get(i).getNcAlarmMsg2(), "utf-8"));
			map.put("ncAlarmNum3", dataList.get(i).getNcAlarmNum3());
			map.put("ncAlarmMsg3", URLEncoder.encode(dataList.get(i).getNcAlarmMsg3(), "utf-8"));

			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}

	@Override
	public String getRepairList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getRepairList", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("stDate", dataList.get(i).getStDate());
			map.put("fnDate", dataList.get(i).getFnDate());
			map.put("rpCause", URLEncoder.encode(dataList.get(i).getRpCause()));
			map.put("eName", URLEncoder.encode(dataList.get(i).getEName()));

			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}

	@Override
	public String getDvcNameList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getDvcNameList", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("id", dataList.get(i).getId());
			map.put("status", dataList.get(i).getStatus());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("lastAlarmCode", dataList.get(i).getLastAlarmCode());
			map.put("lastAlarmMsg", dataList.get(i).getLastAlarmMsg());

			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}

	@Override
	public String getDvcIdForSign(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getDvcIdForSign", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("id", dataList.get(i).getDvcId());
			map.put("status", dataList.get(i).getStatus());
			map.put("name", URLEncoder.encode(dataList.get(i).getName()));
			map.put("lastAlarmCode", dataList.get(i).getLastAlarmCode());
			map.put("lastAlarmMsg", dataList.get(i).getLastAlarmMsg());

			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}

	@Override
	public String getDvcId(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getDvcId", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("status", dataList.get(i).getStatus());

			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}

	@Override
	public ChartVo getData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		chartVo = (ChartVo) sql.selectOne(namespace + "getData", chartVo);

		return chartVo;
	}

	@Override
	public String getFacilitiesStatus(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getFacilitiesStatus", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("cd", dataList.get(i).getCd());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "UTF-8"));
			map.put("nameAbb", dataList.get(i).getNameAbb());
			map.put("WCCD", URLEncoder.encode(dataList.get(i).getWcCd(), "UTF-8"));
			map.put("jig", URLEncoder.encode(dataList.get(i).getJig(), "UTF-8"));
			map.put("isAuto", URLEncoder.encode(dataList.get(i).getIsAuto(), "UTF-8"));
			map.put("isvalue", dataList.get(i).getIsvalue());
			map.put("type", URLEncoder.encode(dataList.get(i).getType(), "UTF-8"));
			map.put("EXCD", dataList.get(i).getEXCD());
			map.put("NC", dataList.get(i).getNC());
			map.put("PRDPRGM", dataList.get(i).getPRDPRGM());
			map.put("CNTMCD", dataList.get(i).getCNTMCD());
			map.put("tgCnt", dataList.get(i).getTgCnt());
			map.put("tgRunTime", dataList.get(i).getTgRunTime());
			map.put("empD", URLEncoder.encode(dataList.get(i).getEmpD(), "UTF-8"));
			map.put("empN", URLEncoder.encode(dataList.get(i).getEmpN(), "UTF-8"));
			map.put("empCdN", dataList.get(i).getEmpCdN());
			map.put("empCdD", dataList.get(i).getEmpCdD());
			map.put("cntPerCyl", dataList.get(i).getCntPerCyl());

			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}

	@Override
	public String getTraceHist(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();

		List<ChartVo> dataList = sql.selectList(namespace + "getTraceHist", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("SN", dataList.get(i).getSN());
			map.put("OP80", dataList.get(i).getOP80());
			map.put("OP90", dataList.get(i).getOP90());
			map.put("OP100", dataList.get(i).getOP100());
			map.put("OP110", dataList.get(i).getOP110());
			map.put("OP150", dataList.get(i).getOP150());
			map.put("OP8090INSP", dataList.get(i).getOP8090INSP());
			map.put("OP100110INSP", dataList.get(i).getOP100110INSP());
			map.put("OP150INSP", dataList.get(i).getOP150INSP());
			map.put("START_TIME", dataList.get(i).getStartTime());
			map.put("END_TIME", dataList.get(i).getEndTime());
			map.put("REGDT", dataList.get(i).getREGDT());

			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}

	@Override
	public String getToolList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();

		List<ChartVo> dataList = sql.selectList(namespace + "getToolList", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("toolNm", dataList.get(i).getToolNm());
			map.put("spec", dataList.get(i).getSpec());
			map.put("portNo", dataList.get(i).getPortNo());
			map.put("prdCntLmt", dataList.get(i).getPrdCntLmt());
			map.put("prdCntCrt", dataList.get(i).getPrdCntCrt());
			map.put("cycleRemain", dataList.get(i).getCycleRemain());
			map.put("cycleUsedRate", dataList.get(i).getCycleUsedRate());
			map.put("runTimeLimit", dataList.get(i).getRunTimeLimit());
			map.put("runTimeCurrent", dataList.get(i).getRunTimeCurrent());
			map.put("runTimeRemain", dataList.get(i).getRunTimeRemain());
			map.put("runTimeUsedRate", dataList.get(i).getRunTimeUsedRate());
			map.put("offsetDLimit", dataList.get(i).getOffsetDLimit());
			map.put("offsetDInit", dataList.get(i).getOffsetDInit());
			map.put("offsetDCurrent", dataList.get(i).getOffsetDCurrent());
			map.put("offsetHLimit", dataList.get(i).getOffsetHLimit());
			map.put("offsetHInit", dataList.get(i).getOffsetHInit());
			map.put("offsetHCurrent", dataList.get(i).getOffsetHCurrent());
			map.put("date", dataList.get(i).getDate());
			map.put("resetDt", dataList.get(i).getResetDt());

			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}

	@Override
	public void resetTool(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();

		// reset date
		sql.update(namespace + "resetTool", chartVo);

		sql.update(namespace + "setHstNull", chartVo);
		// sql.update(namespace + "setMstNull", chartVo);

	}

	@Override
	public void setLimit(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		sql.update(namespace + "setLimit", chartVo);
	}

	@Override
	public void addBanner(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		sql.update(namespace + "addBanner", chartVo);
	}
	
	@Override
	public void cancelBanner(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		sql.update(namespace + "cancelBanner", chartVo);
	}

	@Override
	public ChartVo getBanner(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		chartVo = (ChartVo) sql.selectOne(namespace + "getBanner", chartVo);
		return chartVo;
	}

	@Override
	public String getGroup(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();

		List<ChartVo> dataList = sql.selectList(namespace + "getGroup", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("group", dataList.get(i).getGroup());
			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}

	@Override
	public String getPerformanceAgainstGoal(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getPerformanceAgainstGoal", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("group", dataList.get(i).getGroup());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(),"utf-8"));
			map.put("opTime", dataList.get(i).getOpTime());
			map.put("cntCyl", dataList.get(i).getCntCyl());
			map.put("partCyl", dataList.get(i).getPartCyl());
			map.put("dvcCntCyl", dataList.get(i).getDvcCntCyl());
			map.put("tgCyl", dataList.get(i).getTgCnt());
			map.put("faultCnt", dataList.get(i).getFaultCnt());
			map.put("targetRatio", dataList.get(i).getTargetRatio());
			map.put("target_time", dataList.get(i).getTargetDateTime());
			map.put("inCycle_time", dataList.get(i).getInCycleTime());
			map.put("wait_time", dataList.get(i).getWaitTime());
			map.put("alarm_time", dataList.get(i).getAlarmTime());
			map.put("noConnTime", dataList.get(i).getNoConnectionTime());
			map.put("lastAvrCycleTime", dataList.get(i).getLastAvrCycleTime());
			map.put("opRatio", dataList.get(i).getOpRatio());

			map.put("capa", dataList.get(i).getCapa());
			map.put("cycleTmM", dataList.get(i).getCycleTmM());
			map.put("cycleTmS", dataList.get(i).getCycleTmS());
			map.put("cvt", dataList.get(i).getCvt());
			map.put("uph", dataList.get(i).getUph());
			map.put("workerD", URLEncoder.encode(dataList.get(i).getWorkerD(), "utf-8"));
			map.put("workerN", URLEncoder.encode(dataList.get(i).getWorkerN(), "utf-8"));
			map.put("type", dataList.get(i).getType());
			map.put("workTmMD", dataList.get(i).getWorkTmMD());
			map.put("workTmMN", dataList.get(i).getWorkTmMN());

			list.add(map);
		}
		;
		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}

	@Override
	public String getAllDvc(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();

		List<ChartVo> dataList = sql.selectList(namespace + "getAllDvc", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();

			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "UTF-8"));
			map.put("dvcId", dataList.get(i).getDvcId());

			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}

	@Override
	public String getPrgCdList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getPrgCdList", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("prgCd", URLEncoder.encode(dataList.get(i).getPrgCd()));

			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}

	@Override
	public String getProgCd(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getProgCd", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("line", dataList.get(i).getLine());
			map.put("cd", dataList.get(i).getCd());

			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}

	@Override
	public String getSLCR(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getSLCR", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("slCr", dataList.get(i).getSlCr());

			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}

	@Override
	public String getPrgInfo(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getPrgInfo", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("slAv", dataList.get(i).getSlAv());
			map.put("slMx", dataList.get(i).getSlMx());
			map.put("slMn", dataList.get(i).getSlMn());
			map.put("slCr", dataList.get(i).getSlCr());
			map.put("sec", dataList.get(i).getSec());

			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}

	@Override
	public ChartVo getCurrentProg(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		chartVo = (ChartVo) sql.selectOne(namespace + "getCurrentProg", chartVo);

		return chartVo;
	}

	@Override
	public String getTemplate(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getTemplate", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("id", dataList.get(i).getId());
			map.put("name", dataList.get(i).getName());
			map.put("x", dataList.get(i).getX());
			map.put("y", dataList.get(i).getY());
			map.put("width", dataList.get(i).getWidth());
			map.put("height", dataList.get(i).getHeight());
			map.put("viewBox", dataList.get(i).getViewBox());
			map.put("transform", dataList.get(i).getTransform());
			map.put("d", dataList.get(i).getD());
			map.put("status", dataList.get(i).getStatus());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getAdtList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getAdtList", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("id", dataList.get(i).getId());
			map.put("name", dataList.get(i).getName());
			map.put("x", dataList.get(i).getX());
			map.put("y", dataList.get(i).getY());
			map.put("width", dataList.get(i).getWidth());
			map.put("height", dataList.get(i).getHeight());
			map.put("viewBox", dataList.get(i).getViewBox());
			map.put("transform", dataList.get(i).getTransform());
			map.put("d", dataList.get(i).getD());
			map.put("status", dataList.get(i).getStatus());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getLeadTime(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getLeadTime", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("prdPrc", URLEncoder.encode(dataList.get(i).getPrdPrc(), "utf-8"));
			map.put("inCycleTime", dataList.get(i).getInCycleTime());
			map.put("cuttingTime", dataList.get(i).getCuttingTime());
			map.put("noConnectionTime", dataList.get(i).getNoConnectionTime());
			map.put("waitTime", dataList.get(i).getWaitTime());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getFaultyList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getFaultyList", chartVo); //그래프 , 차트 ,귀책구분,원인구분
		/*List<ChartVo> nameFaultyList = sql.selectList(namespace + "nameFaultyList", chartVo); //장비별
		List<ChartVo> oprNoFaultyList = sql.selectList(namespace + "oprNoFaultyList", chartVo); //공절별
		List<ChartVo> prdNoFaultyList = sql.selectList(namespace + "prdNoFaultyList", chartVo); //제품별
*/		List<ChartVo> resultFaultyList = sql.selectList(namespace + "resultFaultyList", chartVo); //조치별
		List<ChartVo> causeFaultyList = sql.selectList(namespace + "causeFaultyList", chartVo); //원인별
		List<ChartVo> statusFaultyList = sql.selectList(namespace + "statusFaultyList", chartVo); //현상별
/**/
		List list = new ArrayList<ChartVo>();
/*		List nameList = new ArrayList<ChartVo>();
		List oprNoList = new ArrayList<ChartVo>();
		List prdNoList = new ArrayList<ChartVo>();*/
		List resultList = new ArrayList<ChartVo>();
		List causeList = new ArrayList<ChartVo>();
		List statusList = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("sDate", dataList.get(i).getSDate());
			map.put("totalCnt", dataList.get(i).getTotalCnt()); //총 생산수량
			map.put("cnt", dataList.get(i).getCnt()); //총 합격수량
			map.put("faultCnt", dataList.get(i).getFaultCnt()); //총 불량수량
			map.put("processFaultyCnt", dataList.get(i).getProcessFaultyCnt()); //업체 불량수량
			map.put("customerFaultyCnt", dataList.get(i).getCustomerFaultyCnt()); //작업자 불량수량
			map.put("countDvc", dataList.get(i).getCountDvc()); //소재 불량수량
			map.put("count", dataList.get(i).getCount()); //가공 불량수량
			map.put("ratio", dataList.get(i).getRatio()); //불량율			
//			map.put("ratio", (Integer.parseInt(dataList.get(i).getFaultCnt())/dataList.get(i).getTotalCnt())*100); //불량율
/*			map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "utf-8"));
			map.put("prdCnt", dataList.get(i).getPrdCnt());
			map.put("prd", URLEncoder.encode(dataList.get(i).getPrd(), "utf-8"));
			// map.put("opPrdCnt",
			// URLEncoder.encode(dataList.get(i).getOpPrdCnt(),"utf-8"));
			// map.put("cstmPrdCnt",
			// URLEncoder.encode(dataList.get(i).getCstmPrdCnt(),"utf-8"));
			map.put("processFaultyCnt", dataList.get(i).getProcessFaultyCnt());
			map.put("customerFaultyCnt", dataList.get(i).getCustomerFaultyCnt());
			// map.put("processFaultyRate", dataList.get(i).getProcessFaultyRate());
			// map.put("customerFaultyRate", dataList.get(i).getCustomerFaultyRate());
*/
			list.add(map);
		}
		
		/*//장비별
		for (int i = 0; i < nameFaultyList.size(); i++) {
			Map map = new HashMap();
			map.put("name", nameFaultyList.get(i).getName()); // 제품명
			map.put("cnt", nameFaultyList.get(i).getCnt()); //총 합격수량
			nameList.add(map);
		}
		//공정별
		for (int i = 0; i < oprNoFaultyList.size(); i++) {
			Map map = new HashMap();
			map.put("oprNo", oprNoFaultyList.get(i).getOprNo()); // 제품명
			map.put("cnt", oprNoFaultyList.get(i).getCnt()); //총 합격수량
			oprNoList.add(map);
		}
		//제품별
		for (int i = 0; i < prdNoFaultyList.size(); i++) {
			Map map = new HashMap();
			map.put("category", prdNoFaultyList.get(i).getCategory()); // 제품명
			map.put("value", prdNoFaultyList.get(i).getValue()); //총 합격수량
			prdNoList.add(map);
		}*/
		
		//조치별
		for (int i = 0; i < resultFaultyList.size(); i++) {
			Map map = new HashMap();
			map.put("category", URLEncoder.encode(resultFaultyList.get(i).getCategory(), "utf-8")); // 제품명
			map.put("value", Integer.parseInt(resultFaultyList.get(i).getValue())); //총 합격수량
			resultList.add(map);
		}

		//원인별
		for (int i = 0; i < causeFaultyList.size(); i++) {
			Map map = new HashMap();
			map.put("category", URLEncoder.encode(causeFaultyList.get(i).getCategory(), "utf-8")); // 제품명
			map.put("value", Integer.parseInt(causeFaultyList.get(i).getValue())); //총 합격수량
			causeList.add(map);
		}

		//현상별
		for (int i = 0; i < statusFaultyList.size(); i++) {
			Map map = new HashMap();
			map.put("category", URLEncoder.encode(statusFaultyList.get(i).getCategory(), "utf-8")); // 제품명
			map.put("value", Integer.parseInt(statusFaultyList.get(i).getValue())); //총 합격수량
			statusList.add(map);
		}

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		/*dataMap.put("nameFaultyList", nameList);
		dataMap.put("oprNoFaultyList", oprNoList);
		dataMap.put("prdNoFaultyList", prdNoList);*/
		dataMap.put("causeFaultyList", causeList);//원인별
		dataMap.put("resultFaultyList", resultList);//조치별
		dataMap.put("statusFaultyList", statusList);//현상별
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getCheckTyList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getCheckTyList", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("id", dataList.get(i).getId());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getDevieList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getDevieList", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("id", dataList.get(i).getId());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String addFaulty(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";

		sql.insert(namespace + "addFaulty", chartVo);
		str = chartVo.getId();

		return str;
	}

	@Override
	public String updateCd(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		try {
			sql.update(namespace + "updateCd", chartVo);
			str = "success";
		} catch (Exception e) {
			e.printStackTrace();
			str = "fail";
		}
		;
		return str;
	}

	@Override
	public String getFaultList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getFaultList", chartVo);
		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("id", dataList.get(i).getId());
			map.put("cnt", dataList.get(i).getCnt());
			map.put("chkTy", URLEncoder.encode(dataList.get(i).getChkTy(), "utf-8"));
			map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "utf-8"));
			map.put("prdPrc", URLEncoder.encode(dataList.get(i).getPrdPrc(), "utf-8"));
			map.put("dvcName", URLEncoder.encode(dataList.get(i).getDvcName(), "utf-8"));
			map.put("sDate", dataList.get(i).getSDate());
			map.put("checker", URLEncoder.encode(dataList.get(i).getChecker(), "utf-8"));
			map.put("part", URLEncoder.encode(dataList.get(i).getPart(), "utf-8"));
			map.put("situTy", URLEncoder.encode(dataList.get(i).getSituTy(), "utf-8"));
			map.put("situ", URLEncoder.encode(dataList.get(i).getSitu(), "utf-8"));
			map.put("cause", URLEncoder.encode(dataList.get(i).getCause(), "utf-8"));
			map.put("gchTy", URLEncoder.encode(dataList.get(i).getGchTy(), "utf-8"));
			map.put("gch", URLEncoder.encode(dataList.get(i).getGch(), "utf-8"));
			map.put("action", URLEncoder.encode(dataList.get(i).getAction(), "utf-8"));

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getChecker(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = URLEncoder.encode((String) sql.selectOne(namespace + "getChecker", chartVo));
		return str;
	}

	@Override
	public String getCnt(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = (String) sql.selectOne(namespace + "getCnt", chartVo);
		return str;
	}

	@Override
	public String okDel(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		sql.delete(namespace + "okDel", chartVo);
		return null;
	}

	@Override
	public String addMachine(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		try {
			sql.insert(namespace + "addMachine", chartVo);
			str = "success";
		} catch (Exception e) {
			e.printStackTrace();
			str = e.toString();
		}
		return str;
	}

	@Override
	public String getMachine(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getMachine", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("id", dataList.get(i).getId());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("viewBox", dataList.get(i).getViewBox());
			map.put("transform", dataList.get(i).getTransform());
			map.put("d", dataList.get(i).getD());
			map.put("x", dataList.get(i).getX());
			map.put("y", dataList.get(i).getY());
			map.put("w", dataList.get(i).getWidth());
			map.put("h", dataList.get(i).getHeight());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public void setMachinePos_edit(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		sql.update(namespace + "setMachinePos_edit", chartVo);
	}

	@Override
	public String delMachine(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		try {
			sql.delete(namespace + "delMachine", chartVo);
			str = "success";
		} catch (Exception e) {
			e.printStackTrace();
			str = "fail";
		}
		return str;
	}

	@Override
	public String getBGImg(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = URLEncoder.encode((String) sql.selectOne(namespace + "getBGImg", chartVo), "utf-8");

		return str;
	}

	@Override
	public String updateBGImg(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		try {
			sql.update(namespace + "updateBGImg", chartVo);
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}
		return str;
	}

	@Override
	public String getPrdNo(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getPrdNo", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("prdNo", dataList.get(i).getPrdNo());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getMatInfo(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getMatInfo", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "utf-8"));
			map.put("ITEMNO", URLEncoder.encode(dataList.get(i).getITEMNO(), "utf-8"));
			map.put("RWMATNO", URLEncoder.encode(dataList.get(i).getRWMATNO(), "utf-8"));
			map.put("prce", Integer.parseInt(dataList.get(i).getPRCE()));
			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getRcvInfo(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getRcvInfo", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("id", dataList.get(i).getId());
			map.put("comName", URLEncoder.encode(dataList.get(i).getComName(), "utf-8"));
			map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "utf-8"));
			map.put("spec", dataList.get(i).getSpec());
			map.put("lotNo", dataList.get(i).getLotNo());
			map.put("lotCnt", dataList.get(i).getLotCnt());
			map.put("smplCnt", dataList.get(i).getSmplCnt());
			map.put("notiCnt", dataList.get(i).getNotiCnt());
			map.put("rcvCnt", dataList.get(i).getRcvCnt());
			map.put("inputDate", dataList.get(i).getInputDate());
			map.put("deliveryNo", dataList.get(i).getDeliveryNo());
			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public ChartVo getNewMatInfo(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		chartVo = (ChartVo) sql.selectOne(namespace + "getNewMatInfo", chartVo);
		return chartVo;
	}

/*	// 2017 09 26 조동현 Delbert 수정
	@Override
	@Transactional(rollbackFor = Exception.class)
	public String addStock(String val) throws Exception {
		SqlSession sql = getSqlSession();
		
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");
		
		List<ChartVo> list = new ArrayList<ChartVo>();
		String str = "";
		
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);
			ChartVo chartVo = new ChartVo();
			chartVo.setId(tempObj.get("id").toString());
			chartVo.setPrdNo(tempObj.get("prdNo").toString());
			chartVo.setDate(tempObj.get("date").toString());
			chartVo.setLotNo(tempObj.get("lotNo").toString());
			chartVo.setVndNo(tempObj.get("vndNo").toString());
			chartVo.setDeliveryNo(tempObj.get("deliveryNo").toString());
			chartVo.setLotCnt(Integer.parseInt(tempObj.get("lotCnt").toString()));
			chartVo.setSmplCnt(Integer.parseInt(tempObj.get("smplCnt").toString()));
			chartVo.setNotiCnt(Integer.parseInt(tempObj.get("notiCnt").toString()));
			chartVo.setRcvCnt(Integer.parseInt(tempObj.get("rcvCnt").toString()));
			chartVo.setBarcode(tempObj.get("barcode").toString());
			list.add(chartVo);
			// 입고처리된것이 있으면 1나와서 업데이트처리 없으면 추가?
			// int checklist = (int) sql.selectOne(namespace + "checkdelivery",chartVo);
			
			int checkStock = (int) sql.selectOne(namespace + "checkOuterStock", chartVo); // 도금,외주 작업 확인
			
			System.out.println("lotNo ::"+tempObj.get("lotNo").toString());
			System.out.println("barcode ::"+tempObj.get("barcode").toString());
			System.out.println("품번 이름은? :: "+tempObj.get("prdNo").toString()+" , "+tempObj.get("prdNo").toString().indexOf("_RW"));
			System.out.println("비교 :"+ ((tempObj.get("prdNo").toString().indexOf("_RW"))==-1));
			
			if((tempObj.get("prdNo").toString().indexOf("_RW"))==-1){
				if (checkStock == 0) {// 재고 없으면 재고 새로 추가
					System.out.println("재고없음");
					sql.insert(namespace + "insertOuStock", chartVo);
				} else {// 재고 있으면 기존 재고에 +
					System.out.println("재고있음 ");
					sql.update(namespace + "OuterStock", chartVo);
				}
			}
			
			
//			int checkStock = (int) sql.selectOne(namespace + "checkStock", chartVo); // 재고확인
			
//			if (checkStock == 0) {// 재고 없으면 재고 새로 추가
//				sql.insert(namespace + "insertStock", chartVo);
//			} else {// 재고 있으면 기존 재고에 +
//				sql.update(namespace + "modifyStock", chartVo);
//			}
//			
			
			try {
				sql.insert(namespace + "addStock", chartVo); // 입고처리현황에 추가
				sql.insert(namespace + "addStockTable", chartVo); // 재고 테이블에 추간
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		try {
			sql.update(namespace + "chkupdateStock", list); // 입고 처리 완료시 chk=1로 변경후 화면에서 안보이게하기
			str = "success";
		} catch (Exception e) {
			e.printStackTrace();
			str = "fail";
		}
		
		return str;
	}
*/	
	// 2018 05 08 조동현 Delbert 수정
	@Override
	@Transactional(rollbackFor = Exception.class)
	public String addStock(String val) throws Exception {
		SqlSession sql = getSqlSession();

		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<ChartVo> list = new ArrayList<ChartVo>();
		List<ChartVo> listRW = new ArrayList<ChartVo>();	//외주 입고처리시 담기
		List<ChartVo> lastlist = new ArrayList<ChartVo>();	// 외주 마지막공정시 담기
		String str = "";

		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);
			ChartVo chartVo = new ChartVo();
			chartVo.setId(tempObj.get("id").toString());
			chartVo.setPrdNo(tempObj.get("prdNo").toString());
			chartVo.setDate(tempObj.get("date").toString());
			chartVo.setLotNo(tempObj.get("lotNo").toString());
			chartVo.setVndNo(tempObj.get("vndNo").toString());
			chartVo.setDeliveryNo(tempObj.get("deliveryNo").toString());
			chartVo.setLotCnt(Integer.parseInt(tempObj.get("lotCnt").toString()));
			chartVo.setSmplCnt(Integer.parseInt(tempObj.get("smplCnt").toString()));
			chartVo.setNotiCnt(Integer.parseInt(tempObj.get("notiCnt").toString()));
			chartVo.setRcvCnt(Integer.parseInt(tempObj.get("rcvCnt").toString()));
			chartVo.setBarcode(tempObj.get("barcode").toString());
			chartVo.setProj("0000");
			chartVo.setPlan("입고");

			ChartVo addVo = new ChartVo();			
			addVo = (ChartVo) sql.selectOne(namespace + "checkStock1", chartVo);
			
			chartVo.setPRCE(addVo.getPRCE());
			
			list.add(chartVo);
			
			// 입고처리된것이 있으면 1나와서 업데이트처리 없으면 추가?
			// int checklist = (int) sql.selectOne(namespace + "checkdelivery",chartVo);


			if((tempObj.get("prdNo").toString().indexOf("_RW"))==-1){ //완성품 입고시
//			 	chartVo.setProj("0005");
//				chartVo.setBeforeProj("0050");
				
				chartVo.setItem(tempObj.get("prdNo").toString());				
				String checkStock = (String) sql.selectOne(namespace + "checkOuterStock", chartVo); // 공정번호 확인
				chartVo.setProj("0005");
				chartVo.setBeforeProj(checkStock);

				String maxcheck = (String) sql.selectOne(namespace + "maxCheckOp" ,chartVo); // 마지막 공정 가지고 오기
				
				listRW.add(chartVo);
				

				chartVo.setPlan("입고(외주)");
				
				chartVo.setPrdNo(tempObj.get("prdNo").toString());

				if(checkStock.equals(maxcheck)) {
					lastlist.add(chartVo);
//					System.out.println("나는 완성품이다 ( 마지막 )");
				}else {
//					System.out.println("나는 완성품이다 ( 공정 남음 )");
				}
				//System.out.println("금액 : "+chartVo.getPRCE());
				//System.out.println(" 소재 :"+chartVo.getPrdNo());
				//System.out.println("회사명 :"+chartVo.getVndNo());
				//System.out.println("로트번호 :"+chartVo.getLotNo());
				
				////String checkStock1 = (String) sql.selectOne(namespace + "checkStock1", chartVo); // 소재로 재고를 관리하기 위하여
//				chartVo.setPrdNo(checkStock1);
				list.add(chartVo);

			}
			
			
//			int checkStock = (int) sql.selectOne(namespace + "checkStock", chartVo); // 재고확인

//			if (checkStock == 0) {// 재고 없으면 재고 새로 추가
//				sql.insert(namespace + "insertStock", chartVo);
//			} else {// 재고 있으면 기존 재고에 +
//				sql.update(namespace + "modifyStock", chartVo);
//			}
//			
			try {
				sql.insert(namespace + "addStock1", chartVo); // 트렌스 추가
				sql.insert(namespace + "addStockTable1", chartVo); // 총재고 추가
				if(chartVo.getBeforeProj()==null && chartVo.getProj().equals("0005")) {// null이면 재고 테이블에 소재 0005로 추가
								sql.insert(namespace + "addStockProcessRW", chartVo); // 총재고 추가	
				}else if(chartVo.getBeforeProj()!=null && chartVo.getProj().equals("0005")) {//null 이 아니면 그 proj번호로 소재 추가
								sql.insert(namespace + "addStockProcessLHRH", chartVo); // 총재고 추가					
				}
				

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		try {
			if(listRW.size()>0) {//완성외주이면
				//외주 마이너스
					sql.insert(namespace + "StockMinusCompany", listRW);
			}
			if(lastlist.size()>0) {//마지막 공정일시
					sql.insert(namespace + "lasttransadd", lastlist);		// 완성창고 트렌스 추가
					sql.insert(namespace + "successStockAdd", lastlist);	//완성창고 수량 +
					sql.insert(namespace + "totalStockReCount" , lastlist);	//총재고 - 공정에 입고됐었으므로 공정 -
					sql.insert(namespace + "proccessReCount" , lastlist);	//공정재고 -
				
/*				System.out.println("마지막공정이다" + lastlist.get(0).getPrdNo());
				System.out.println("마지막공정이다" + lastlist.get(0).getItem());
				System.out.println("마지막공정이다" + lastlist.get(0).getBeforeProj());
				System.out.println("마지막공정이다" + lastlist.get(0).getLotNo());
				System.out.println("마지막공정이다" + lastlist.get(0).getRcvCnt());*/
			} 
			 sql.update(namespace + "chkupdateStock", list); // 입고 처리 완료시 chk=1로 변경후 화면에서 안보이게하기
			str = "success";
		} catch (Exception e) {
			e.printStackTrace();
			str = "fail";
		}

		return str;
	}
/*	@Override
	@Transactional(rollbackFor = Exception.class)
	public String addStock(String val) throws Exception {
		SqlSession sql = getSqlSession();
		
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");
		
		List<ChartVo> list = new ArrayList<ChartVo>();
		List<ChartVo> listRW = new ArrayList<ChartVo>();	//외주 입고처리시 담기
		String str = "";
		
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);
			ChartVo chartVo = new ChartVo();
			chartVo.setId(tempObj.get("id").toString());
			chartVo.setPrdNo(tempObj.get("prdNo").toString());
			chartVo.setDate(tempObj.get("date").toString());
			chartVo.setLotNo(tempObj.get("lotNo").toString());
			chartVo.setVndNo(tempObj.get("vndNo").toString());
			chartVo.setDeliveryNo(tempObj.get("deliveryNo").toString());
			chartVo.setLotCnt(Integer.parseInt(tempObj.get("lotCnt").toString()));
			chartVo.setSmplCnt(Integer.parseInt(tempObj.get("smplCnt").toString()));
			chartVo.setNotiCnt(Integer.parseInt(tempObj.get("notiCnt").toString()));
			chartVo.setRcvCnt(Integer.parseInt(tempObj.get("rcvCnt").toString()));
			chartVo.setBarcode(tempObj.get("barcode").toString());
			chartVo.setProj("0000");
			chartVo.setPlan("입고");
			ChartVo addVo = new ChartVo();			
			addVo = (ChartVo) sql.selectOne(namespace + "checkStock1", chartVo);
			
			chartVo.setPRCE(addVo.getPRCE());
			
			
			
			
			if((tempObj.get("prdNo").toString().indexOf("_RW"))==-1){ //완성품 입고시
				chartVo.setItem(tempObj.get("prdNo").toString());				
				String checkStock = (String) sql.selectOne(namespace + "checkOuterStock", chartVo); // 현재 공정번호 확인
				chartVo.setProj("0005");
				chartVo.setBeforeProj(checkStock);
				String maxcheck = (String) sql.selectOne(namespace + "maxCheckOp" ,chartVo); // 마지막 공정 가지고 오기
				
				listRW.add(chartVo);
				chartVo.setPlan("입고(외주)");
				
				chartVo.setPrdNo(tempObj.get("prdNo").toString());
				if(checkStock.equals(maxcheck)) {
					System.out.println("나는 완성품이다 ( 마지막 )");
				}else {
					System.out.println("나는 완성품이다 ( 공정 남음 )");
				}
				
				list.add(chartVo);
				
			}else {	//소재일 경우
				System.out.println("나는 소재이다");
				list.add(chartVo);
			}
			
			
//			int checkStock = (int) sql.selectOne(namespace + "checkStock", chartVo); // 재고확인
			
//			if (checkStock == 0) {// 재고 없으면 재고 새로 추가
//				sql.insert(namespace + "insertStock", chartVo);
//			} else {// 재고 있으면 기존 재고에 +
//				sql.update(namespace + "modifyStock", chartVo);
//			}
//			
			try {
				//1sql.insert(namespace + "addStock1", chartVo); // 트렌스 추가
				//2sql.insert(namespace + "addStockTable1", chartVo); // 총재고 추가
				if(chartVo.getBeforeProj()==null && chartVo.getProj().equals("0005")) {// null이면 재고 테이블에 소재 0005로 추가
					//3sql.insert(namespace + "addStockProcessRW", chartVo); // 총재고 추가	
				}else if(chartVo.getBeforeProj()!=null && chartVo.getProj().equals("0005")) {//null 이 아니면 그 proj번호로 소재 추가
					//4sql.insert(namespace + "addStockProcessLHRH", chartVo); // 총재고 추가					
				}
				
				if(listRW.size()>0) {//완성외주이면
					//외주 마이너스
					//5sql.insert(namespace + "StockMinusCompany", listRW);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		try {
			//6sql.update(namespace + "chkupdateStock", list); // 입고 처리 완료시 chk=1로 변경후 화면에서 안보이게하기
			str = "success";
		} catch (Exception e) {
			e.printStackTrace();
			str = "fail";
		}
		
		return str;
	}*/

	@Override
	public String okDelStock(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		try {
			chartVo = (ChartVo) sql.selectOne(namespace + "getStockPrdNo", chartVo);
			sql.delete(namespace + "okDelStock", chartVo);

			sql.update(namespace + "modifyStock", chartVo);
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}
		return str;
	}

	@Override
	public ChartVo getStockInfo(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		chartVo = (ChartVo) sql.selectOne(namespace + "getStockInfo", chartVo);
		return chartVo;
	}

	@Override
	public String updateStock(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		try {
			sql.delete(namespace + "updateStock", chartVo);
			// if(chartVo.getUpdatedRcvCnt()!=0){
			// chartVo.setRcvCnt(chartVo.getUpdatedRcvCnt());
			// }
			//
			// chartVo.setNotiCnt(chartVo.getUpdatedNotiCnt());

			chartVo.setRcvCnt(chartVo.getUpdatedRcvCnt());
			chartVo.setNotiCnt(chartVo.getUpdatedNotiCnt());

			sql.update(namespace + "modifyStock", chartVo);
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}
		return str;
	}

	@Override
	public String getNonOpInfo(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getNonOpInfo", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("id", dataList.get(i).getId());
			// map.put("prdNo", dataList.get(i).getPrdNo());
			// map.put("oprNm", URLEncoder.encode(dataList.get(i).getOprNm(),"utf-8"));
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("date", dataList.get(i).getDate());
			map.put("ty", dataList.get(i).getTy());
			map.put("cuttingTime", dataList.get(i).getCuttingTime());
			map.put("inCycleTime", dataList.get(i).getInCycleTime());
			map.put("waitTime", dataList.get(i).getWaitTime());
			map.put("alarmTime", dataList.get(i).getAlarmTime());
			map.put("noConnectionTime", dataList.get(i).getNoConnectionTime());
			map.put("opRatio", dataList.get(i).getOpRatio());
			map.put("cuttingTimeRatio", dataList.get(i).getCuttingTimeRatio());
			map.put("worker", URLEncoder.encode(dataList.get(i).getWorker(), "utf-8"));
			map.put("totalTime", dataList.get(i).getTotalTime());
			map.put("nonOpTime", dataList.get(i).getNonOpTime());
			map.put("workerOpTime", dataList.get(i).getWorkerOpTime());
			map.put("workerOpRatio", dataList.get(i).getWorkerOpRatio());
			map.put("nonOpTy", dataList.get(i).getNonOpTy());
			map.put("nonOpTyTxt", URLEncoder.encode(dataList.get(i).getNonOpTyTxt(), "utf-8"));
			map.put("startTime", dataList.get(i).getStartTime());
			map.put("endTime", dataList.get(i).getEndTime());
			map.put("time", dataList.get(i).getTime());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public ChartVo getNonOpData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		chartVo = (ChartVo) sql.selectOne(namespace + "getNonOpData", chartVo);
		return chartVo;
	}

	@Override
	public String getOprNm(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getOprNm", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("oprNm", URLEncoder.encode(dataList.get(i).getOprNm(), "utf-8"));

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getNonOpTy(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getNonOpTy", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("id", dataList.get(i).getId());
			map.put("nonOpTy", URLEncoder.encode(dataList.get(i).getNonOpTy(), "utf-8"));

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String updateNonOp(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		try {

			sql.update(namespace + "updateNonOp", chartVo);
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}
		return str;
	}

	@Override
	public String okDelNonOp(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		try {

			sql.update(namespace + "okDelNonOp", chartVo);
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}
		return str;
	}

	@Override
	public String addNonOp(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		try {

			sql.insert(namespace + "addNonOp", chartVo);
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}
		return str;
	}

	@Override
	public String getDvcListByPrdNo(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getDvcListByPrdNo", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getCheckType(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getCheckType", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("id", dataList.get(i).getId());
			map.put("codeName", URLEncoder.encode(dataList.get(i).getCodeName(), "utf-8"));

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	/*
	 * 2017-11-16 Deprecated 예약.
	 */
	@Override
	@Deprecated
	public String getLotNoByPrdNo(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getLotNoByPrdNo", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("id", dataList.get(i).getId());
			map.put("lotNo", URLEncoder.encode(dataList.get(i).getLotNo(), "utf-8"));

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getToolInfo(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getToolInfo", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("portNo", dataList.get(i).getPortNo());
			map.put("prdCnt", dataList.get(i).getPrdCnt());
			map.put("runTime", dataList.get(i).getRunTime());
			map.put("limitPrdCnt", dataList.get(i).getLimitPrdCnt());
			map.put("limitRunTime", dataList.get(i).getLimitRunTime());
			map.put("offsetD", dataList.get(i).getOffsetD());
			map.put("offsetH", dataList.get(i).getOffsetH());

			map.put("programName", dataList.get(i).getProgramName());
			map.put("mode", dataList.get(i).getMode());
			map.put("spdLoad", dataList.get(i).getSpdLoad());
			map.put("feedOverride", dataList.get(i).getFeedOverride());

			map.put("sec", dataList.get(i).getSec());
			map.put("avrCycleTimeSec", dataList.get(i).getAvrCycleTimeSec());
			map.put("prdNo", dataList.get(i).getPrdNo());

			list.add(map);

		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getLotInfo(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getLotInfo", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();

			map.put("id", dataList.get(i).getId());
			map.put("lotNo", dataList.get(i).getLotNo());
			map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "utf-8"));
			map.put("spec", dataList.get(i).getSpec());
			map.put("rcvCnt", dataList.get(i).getRcvCnt());
			map.put("sendCnt", dataList.get(i).getSendCnt());
			map.put("stockCnt", dataList.get(i).getStockCnt());
			map.put("oprNm", dataList.get(i).getOprNm());
			map.put("group", dataList.get(i).getGroup());
			map.put("ITMPRD", dataList.get(i).getItem());
			map.put("barcode", dataList.get(i).getBarcode());
			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	public String saveUpdatedStock(String val) throws Exception {
		SqlSession sql = getSqlSession();

		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);
			ChartVo chartVo = new ChartVo();
			chartVo.setId(tempObj.get("id").toString());
			chartVo.setPrdNo(tempObj.get("prdNo").toString());
			chartVo.setSendCnt(Integer.parseInt(tempObj.get("sendCnt").toString()));
			chartVo.setPreSendCnt(Integer.parseInt(tempObj.get("preSendCnt").toString()));
			chartVo.setDate(tempObj.get("date").toString());
			chartVo.setLotNo(tempObj.get("lotNo").toString());
			chartVo.setOprNm(tempObj.get("oprNm").toString());
			chartVo.setInputCnt(tempObj.get("inputCnt").toString());
			chartVo.setOprNo(tempObj.get("oprNo").toString());

			list.add(chartVo);
			sql.insert(namespace + "addReleaseStock", chartVo); // 불출현황조회 추가
			int chk = (int) sql.selectOne(namespace + "bertchkhstship", chartVo);
			if (chk >= 1) {
				sql.update(namespace + "shipmentupdbert", chartVo); // 출하관리로 이동
			} else {
				sql.insert(namespace + "shipmentaddbert", chartVo); // 출하관리로 이동
			}
			sql.update(namespace + "bertstckcnt", chartVo); // 기존창고 에서 불출수량+재고수량-
			int chk1 = (int) sql.selectOne(namespace + "bertchkstck", chartVo);
			if (chk1 >= 1) {
				sql.update(namespace + "bertstck", chartVo); // 옮긴 창고에서 입고수량+재고수량+
			} else {
				sql.insert(namespace + "bertaddstck", chartVo);
			}

		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", list);

		String str = "";

		try {
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}

		return str;
	}

	@Override
	public String getOprNmList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getOprNmList", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();

			map.put("val", dataList.get(i).getVal());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);

		return str;
	}

	@Override
	public ChartVo getSpdLoadInfo(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		chartVo = (ChartVo) sql.selectOne(namespace + "getSpdLoadInfo", chartVo);
		if (chartVo == null) {
			chartVo = new ChartVo();
			chartVo.setZTo50("0");
			chartVo.setFTo100("0");
			chartVo.setOver100("0");
		}
		return chartVo;
	}

	@Override
	public String setSpdLoad(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		try {
			sql.delete(namespace + "delSpdLoadSet", chartVo);
			sql.insert(namespace + "addSpdLoadSet", chartVo);
			str = "success";
		} catch (Exception e) {
			e.printStackTrace();
			str = "fail";
		}

		return str;
	}

	@Override
	public String getProgInfo(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getProgInfo", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();

			map.put("prgCd", dataList.get(i).getPrgCd());
			map.put("ratio", dataList.get(i).getRatio());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);

		return str;
	}

	@Override
	public String setPrgSet(String val) throws Exception {
		SqlSession sql = getSqlSession();

		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			ChartVo chartVo = new ChartVo();
			chartVo.setPrgCd(tempObj.get("prgCd").toString());
			chartVo.setRatio(tempObj.get("ratio").toString());
			chartVo.setDvcId(tempObj.get("dvcId").toString());

			list.add(chartVo);
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", list);

		String str = "";

		try {
			sql.delete(namespace + "delPrgSet", list);
			sql.delete(namespace + "addPrgSet", list);

			str = "success";
		} catch (Exception e) {
			str = "fail";
		}

		return str;
	}

	@Override
	public String setCalcTy(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		try {
			sql.delete(namespace + "delCalcTy", chartVo);
			sql.delete(namespace + "addCalcTy", chartVo);

			str = "success";
		} catch (Exception e) {
			str = "fail";
			e.printStackTrace();
		}
		return str;
	}

	@Override
	public ChartVo getCalcTy(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		chartVo = (ChartVo) sql.selectOne(namespace + "getCalcTy", chartVo);
		if (chartVo == null) {
			chartVo = new ChartVo();
			chartVo.setIsSpd("0");
			chartVo.setCalcTy(0);
		}
		return chartVo;
	}

	@Override
	public String saveRow(String val) throws Exception {
		SqlSession sql = getSqlSession();

		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<ChartVo> list = new ArrayList<ChartVo>();	

		List<ChartVo> totallist = new ArrayList<ChartVo>();	//총재고 뺄것
		List<ChartVo> prolist = new ArrayList<ChartVo>();	//공정재고 뺄리스트
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			ChartVo chartVo = new ChartVo();
			chartVo.setChkTy(tempObj.get("chkTy").toString());
			chartVo.setPrdNo(tempObj.get("prdNo").toString());
			chartVo.setPrdPrc(tempObj.get("prdPrc").toString());
			chartVo.setDvcId(tempObj.get("dvcId").toString());
			chartVo.setDate(tempObj.get("date").toString());
			chartVo.setChecker(tempObj.get("checker").toString());
			chartVo.setPart(tempObj.get("part").toString());
			chartVo.setSituationTy(tempObj.get("situationTy").toString());
			chartVo.setSituation(tempObj.get("situation").toString());
			chartVo.setCause(tempObj.get("cause").toString());
			chartVo.setGchTy(tempObj.get("gchTY").toString());
			chartVo.setGch(tempObj.get("gch").toString());
			chartVo.setCnt(tempObj.get("cnt").toString());	// 총 불량수량
			chartVo.setSendCnt(Integer.parseInt(tempObj.get("sendCnt").toString()));	//불량창고로 보내는 수량
			chartVo.setAction(tempObj.get("action").toString());
			chartVo.setProj(tempObj.get("proj").toString());
			list.add(chartVo);
			
			//고객사,필드 일땐 총재고 - 안하기 위해서
			if(tempObj.get("proj").toString().equals("0")) {
			}else {
				totallist.add(chartVo);
			}
			//공정이동 0005, 0010, 0020, 0030 일때만 이동하기 위해서
			if(tempObj.get("proj").toString().equals("0005") || tempObj.get("proj").toString().equals("0010") || tempObj.get("proj").toString().equals("0020") || tempObj.get("proj").toString().equals("0030")) {
				prolist.add(chartVo);
			}

		}

		String str = "";

//		sql.delete(namespace + "delPrcDfct", list);
		sql.insert(namespace + "addPrdFaulty", list);	//불량등록

		sql.insert(namespace + "transaddFault", list);	//불량 트렌스 추가
		sql.insert(namespace + "stockaddFault", list);	//총재고 +(불량창고)
		
		if(totallist.size()>0) {
			sql.insert(namespace + "stockminusFault", totallist);	//총재고 -(현재고)
		}
		if(prolist.size()>0) {
			sql.insert(namespace + "processaddFault", list);	//공정재고 - 이동
		}

		try {
			// sql.delete(namespace + "delPrcDfct", list);
			// sql.insert(namespace + "addReleaseStock", list);

			str = "success";
		} catch (Exception e) {
			str = "fail";
		}

		return str;
	}

	@Override
	public String getJSGroupCode(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getJSGroupCode", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();

			map.put("id", dataList.get(i).getId());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);

		return str;
	}

	@Override
	public String addCheckStandard(String val) throws Exception {
		SqlSession sql = getSqlSession();

		JSONParser jsonParser = new JSONParser();
		System.out.println("val ::"+val);
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			ChartVo chartVo = new ChartVo();
			chartVo.setId(tempObj.get("id").toString());
			chartVo.setPrdNo(tempObj.get("prdNo").toString());
			chartVo.setDvcId(tempObj.get("dvcId").toString());
			chartVo.setChkTy(tempObj.get("chkTy").toString());
			chartVo.setAttrTy(tempObj.get("attrTy").toString());
			chartVo.setAttrCd(tempObj.get("attrCd").toString());
			chartVo.setAttrNameKo(tempObj.get("attrNameKo").toString());
			chartVo.setDp(tempObj.get("dp").toString());
			chartVo.setUnit(tempObj.get("unit").toString());
			chartVo.setSpec(tempObj.get("spec").toString());
			chartVo.setTarget(tempObj.get("target").toString());
			chartVo.setLow(tempObj.get("low").toString());
			chartVo.setUp(tempObj.get("up").toString());
			chartVo.setMeasurer(tempObj.get("measurer").toString());
			chartVo.setJsGroupCd(tempObj.get("jsGroupCd").toString());
			chartVo.setAttrNameOthr(tempObj.get("attrNameOthr").toString());

			if (chartVo.getId().equals("0")) {
				list.add(chartVo);
			} else {
				sql.update(namespace + "updateCheckStandard", chartVo);
			}
		}

		String str = "";

		try {
			if (list.size() > 0)
				sql.insert(namespace + "addChkStandard", list);
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}

		return str;
	}

	@Override
	public String getChkStandardList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getChkStandardList", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();

			map.put("id", dataList.get(i).getId());
			map.put("prdNo", dataList.get(i).getPrdNo());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("chkTy", dataList.get(i).getChkTy());
			map.put("attrTy", dataList.get(i).getAttrTy());
			map.put("attrCd", dataList.get(i).getAttrCd());
			map.put("attrNameKo", URLEncoder.encode(dataList.get(i).getAttrNameKo(), "utf-8"));
			map.put("dp", dataList.get(i).getDp());
			map.put("unit", dataList.get(i).getUnit());
			map.put("spec", URLEncoder.encode(dataList.get(i).getSpec(), "utf-8"));
			map.put("target", dataList.get(i).getTarget());
			map.put("low", dataList.get(i).getLow());
			map.put("up", dataList.get(i).getUp());
			map.put("measurer", URLEncoder.encode(dataList.get(i).getMeasurer(), "utf-8"));
			map.put("jsGroupCd", dataList.get(i).getJsGroupCd());
			map.put("attrNameOthr", dataList.get(i).getAttrNameOthr());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);

		return str;
	}

	@Override
	public String delChkStandard(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		try {
			sql.delete(namespace + "delChkStandard", chartVo);
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}
		return str;
	}

	@Override
	public String getCheckList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getCheckList", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();

			map.put("id", dataList.get(i).getId());
			map.put("clmNo", dataList.get(i).getClmNo());
			map.put("listId", dataList.get(i).getListId());
			map.put("prdNo", dataList.get(i).getPrdNo());
			map.put("name", URLEncoder.encode(dataList.get(i).getName()));
			map.put("chkTy", URLEncoder.encode(dataList.get(i).getChkTy(), "utf-8"));
			map.put("attrTy", dataList.get(i).getAttrTy());
			map.put("attrNameKo", URLEncoder.encode(dataList.get(i).getAttrNameKo(), "utf-8"));
			map.put("spec", URLEncoder.encode(dataList.get(i).getSpec(), "utf-8"));
			map.put("target", dataList.get(i).getTarget());
			map.put("low", dataList.get(i).getLow());
			map.put("up", dataList.get(i).getUp());
			map.put("measurer", URLEncoder.encode(dataList.get(i).getMeasurer(), "utf-8"));
			map.put("workTy", dataList.get(i).getWorkTy());
			map.put("fResult", dataList.get(i).getFResult());
			map.put("result", URLEncoder.encode(dataList.get(i).getResult(), "utf-8"));
			map.put("result2", URLEncoder.encode(dataList.get(i).getResult2(), "utf-8"));
			map.put("date", dataList.get(i).getDate());
			map.put("checkCycle", dataList.get(i).getCheckCycle());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);

		return str;
	}

	@Override
	public String addCheckStandardList(String val) throws Exception {
		SqlSession sql = getSqlSession();

		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			ChartVo chartVo = new ChartVo();
			chartVo.setClmNo(Integer.parseInt(tempObj.get("clmNo").toString()));
			chartVo.setId(tempObj.get("id").toString());
			chartVo.setListId(tempObj.get("id").toString());
			chartVo.setResult(tempObj.get("result").toString());
			chartVo.setResult2(tempObj.get("result2").toString());
			chartVo.setFResult(tempObj.get("fResult").toString());
			chartVo.setTy(tempObj.get("workTy").toString());
			chartVo.setDate(tempObj.get("date").toString());
			chartVo.setChecker(tempObj.get("checker").toString());
			chartVo.setCheckCycle(tempObj.get("chkCycle").toString());
			chartVo.setDvcId(tempObj.get("dvcId").toString());
			chartVo.setAttrNameKo(tempObj.get("attrNameKo").toString());

			list.add(chartVo);
			
		}

		String str = "";

		try {
			if (list.size() > 0)
				sql.insert(namespace + "addChkStandardList", list);
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}

		return str;
	}

	@Override
	public String getPrdData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getPrdData", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();

			map.put("prdNo", dataList.get(i).getPrdNo());
			map.put("oprNm", dataList.get(i).getOprNm());
			map.put("oprCd", dataList.get(i).getOprCd());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("cylTmMi", dataList.get(i).getCycleTmM());
			map.put("cylTmSec", dataList.get(i).getCycleTmS());
			map.put("cvt", dataList.get(i).getCvt());
			map.put("uph", dataList.get(i).getUph());
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("worker", URLEncoder.encode(dataList.get(i).getWorker(), "utf-8"));
			map.put("workerCnt", dataList.get(i).getWorkerCnt());
			map.put("date", dataList.get(i).getDate().toString());
			map.put("sDate", dataList.get(i).getSDate());
			map.put("eDate", dataList.get(i).getEDate());
			map.put("workTm", dataList.get(i).getWorkTm());
			map.put("capa", dataList.get(i).getCapa());
			map.put("target", dataList.get(i).getTarget());
			map.put("cnt", dataList.get(i).getCnt());
			map.put("partCyl", dataList.get(i).getPartCyl());
			map.put("faultCnt", dataList.get(i).getFaultCnt());
			map.put("okRatio", dataList.get(i).getOkRatio());
			map.put("nd", dataList.get(i).getNd());
			map.put("goalRatio", dataList.get(i).getGoalRatio());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);

		return str;
	}

	@Override
	public String showPopup(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "showPopup", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();

			map.put("lotNo", dataList.get(i).getLotNo());
			map.put("lotCnt", dataList.get(i).getLotCnt());
			map.put("baseCnt", dataList.get(i).getBaseCnt());
			map.put("inputCnt", dataList.get(i).getInputCnt());
			map.put("prdCnt", dataList.get(i).getPrdCnt());
			map.put("outCnt", dataList.get(i).getOutCnt());
			map.put("oprNm", URLEncoder.encode(dataList.get(i).getOprNm(), "utf-8"));
			map.put("id", dataList.get(i).getId());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);

		return str;
	}

	@Override
	public String getOprNo(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getOprNo", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();

			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("oprNo", dataList.get(i).getOprNo());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);

		return str;
	}

	@Override
	public String addPrdData(String val) throws Exception {
		SqlSession sql = getSqlSession();

		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			ChartVo chartVo = new ChartVo();
			chartVo.setId(tempObj.get("id").toString());
			chartVo.setPrdNo(tempObj.get("prdNo").toString());
			chartVo.setLotNo(tempObj.get("lotNo").toString());
			chartVo.setOprNo(tempObj.get("oprNo").toString());
			chartVo.setDvcId(tempObj.get("dvcId").toString());
			chartVo.setNd(Integer.parseInt(tempObj.get("nd").toString()));
			chartVo.setBaseCnt(tempObj.get("baseCnt").toString());
			chartVo.setInputCnt(tempObj.get("inputCnt").toString());
			chartVo.setPrdCnt(tempObj.get("prdCnt").toString());
			chartVo.setOutCnt(tempObj.get("outCnt").toString());
			chartVo.setDate(tempObj.get("date").toString());
			chartVo.setWorker(tempObj.get("worker").toString());
			chartVo.setFromOprNm(tempObj.get("fromOprNm").toString());

			// if(chartVo.getListId().equals("0")){
			// list.add(chartVo);
			// }else{
			// sql.update(namespace + "updateCheckStandardList", chartVo);
			// }

			if (chartVo.getOutCnt().equals("0")) {

			} else {
				list.add(chartVo);
			}
		}

		String str = "";

		try {
			if (list.size() > 0)
				sql.insert(namespace + "addPrdData", list);
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}

		return str;
	}

	@Override
	public String getComName(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = URLEncoder.encode((String) sql.selectOne(namespace + "getComName", chartVo), "utf-8");
		return str;
	}

	@Override
	public String getNextWorkerId(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String rtn = (String) sql.selectOne(namespace + "getNextWorkerId", chartVo);
		return rtn;
	}

	@Override
	public String addWorker(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		try {

			int exist = (Integer) sql.selectOne(namespace + "chkWorker", chartVo);
			if (exist > 0) {
				sql.update(namespace + "updateWorker", chartVo);
			} else {
				sql.insert(namespace + "addWorker", chartVo);
			}
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}
		return str;
	}

	@Override
	public ChartVo getWorkerInfo(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		chartVo = (ChartVo) sql.selectOne(namespace + "getWorkerInfo", chartVo);
		return chartVo;
	}

	@Override
	public String getComList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		//업체 리스트
		List<ChartVo> dataList = sql.selectList(namespace + "getComList", chartVo);

		//라우팅별 업체 목록리스트 (반출 반입할때 쓰임)
		List<ChartVo> dataList1 = sql.selectList(namespace + "getExportComList", chartVo);

		List list = new ArrayList<ChartVo>();

		List list1 = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();

			map.put("id", dataList.get(i).getId());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));

			list.add(map);
		}
		;
		
		for (int i = 0; i < dataList1.size(); i++) {
			Map map = new HashMap();
			
			map.put("prdNo", dataList1.get(i).getPrdNo());
			map.put("afterProj", dataList1.get(i).getAfterProj());
			map.put("id", dataList1.get(i).getId());
			map.put("name", URLEncoder.encode(dataList1.get(i).getName(), "utf-8"));
			
			list1.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		dataMap.put("dataList1", list1);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);

		return str;
	}

	@Override
	public String getLotInfoHistory(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getLotInfoHistory", chartVo);
		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();

			map.put("lotNo", dataList.get(i).getLotNo());
			map.put("id", dataList.get(i).getId());
			map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "utf-8"));
			map.put("oprNm", dataList.get(i).getOprNm());
			map.put("spec", dataList.get(i).getSpec());
			map.put("sendCnt", dataList.get(i).getSendCnt());
			map.put("date", dataList.get(i).getDate());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String searchWorker(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "searchWorker", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();

			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("id", dataList.get(i).getId());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getReleaseInfo(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getReleaseInfoAlter", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();

			map.put("idx", dataList.get(i).getIdx());
			map.put("prdNo", dataList.get(i).getPrdNo());
			map.put("spec", dataList.get(i).getSpec());
//			map.put("lotNo", dataList.get(i).getLotNo());
			map.put("lotNo", dataList.get(i).getGroupLotNo());
			map.put("lotCnt", dataList.get(i).getLotCnt());
			map.put("lotStock", dataList.get(i).getLotStock());
			map.put("regdate", dataList.get(i).getRegDate());
			list.add(map);
		}

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String addPrdCmpl(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";

		try {
			int exist = (Integer) sql.selectOne(namespace + "chkCmpl", chartVo);

			if (exist > 0) {
				sql.update(namespace + "updateCmpl", chartVo);
			} else {
				sql.insert(namespace + "addPrdCmpl", chartVo);
			}

			chartVo.setOprNm((String) sql.selectOne(namespace + "getOpr", chartVo));

			if (chartVo.getOprNm().equals("0010")) {
				chartVo.setPreOpr("0000");
			} else if (chartVo.getOprNm().equals("0020")) {
				chartVo.setPreOpr("0010");
			} else if (chartVo.getOprNm().equals("0030")) {
				chartVo.setPreOpr("0020");
			} else if (chartVo.getOprNm().equals("1000")) {
				chartVo.setPreOpr("0030");
			}

			if (!chartVo.getOprNm().equals("0010")) {
				// 공정 재고 +
				int stockExist = (Integer) sql.selectOne(namespace + "chkCurrentCmplStock", chartVo);
				if (stockExist > 0) {
					sql.update(namespace + "updateCurrnetCmplStock", chartVo);
				} else {
					sql.insert(namespace + "addCurrnetCmplStock", chartVo);
				}

				// 이전 공정 -
				int stockeNextExist = (Integer) sql.selectOne(namespace + "chkNextCmplStock", chartVo);
				if (stockeNextExist > 0) {
					sql.update(namespace + "minusNextCmplStock", chartVo);
				} else {
					sql.insert(namespace + "addNextCmplStock", chartVo);
				}
			}

			// mat_out_history
			// sql.insert(namespace + "addMatOut",chartVo);

			// if(chartVo.getOprNm().equals("0040")){
			// //출하 history
			//
			// for(int i = 1; i <=5 ; i++){
			// String lotNo = null;
			// int lotCnt = 0;
			// if(i==1){
			// lotNo = chartVo.getLot1();
			// lotCnt = chartVo.getCnt1();
			// }else if(i==2){
			// lotNo = chartVo.getLot2();
			// lotCnt = chartVo.getCnt2();
			// }else if(i==3){
			// lotNo = chartVo.getLot3();
			// lotCnt = chartVo.getCnt3();
			// }else if(i==4){
			// lotNo = chartVo.getLot4();
			// lotCnt = chartVo.getCnt4();
			// }else if(i==5){
			// lotNo = chartVo.getLot5();
			// lotCnt = chartVo.getCnt5();
			// }
			//
			// chartVo.setLotNo(lotNo);
			// chartVo.setLotCnt(lotCnt);
			//
			// int shipExist = (Integer) sql.selectOne(namespace + "chkShip", chartVo);
			//
			// if(shipExist>0){
			// sql.update(namespace + "updateShipCnt", chartVo);
			// }else{
			// sql.insert(namespace + "addShipCnt", chartVo);
			// }
			// }
			// }
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}
		return str;
	}

	@Override
	public String addCmplStockHistory(String val) throws Exception {
		SqlSession sql = getSqlSession();
		String oprNmstatus = "";

		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			ChartVo chartVo = new ChartVo();
			chartVo.setId(tempObj.get("id").toString());
			chartVo.setPrdNo(tempObj.get("prdNo").toString());
			chartVo.setItem(tempObj.get("item").toString());
			chartVo.setSendCnt(Integer.parseInt(tempObj.get("sendCnt").toString()));
			chartVo.setLotNo(tempObj.get("lotNo").toString());
			chartVo.setDate(tempObj.get("date").toString());
			chartVo.setOutLot(tempObj.get("outLot").toString());
//			chartVo.setInputCnt(tempObj.get("inputCnt").toString());
			// chartVo.setPreSend(Integer.parseInt(tempObj.get("preSend").toString()));

			list.add(chartVo);

			// ChartVo chkprocess=(ChartVo) sql.selectOne(namespace + "bertchkprocess",
			// chartVo); //R , MCT ,CNC 구분위해 cnt가져오기
			sql.update(namespace + "setLatestData", chartVo); // 로트번호와 같은것의 latest를 0으로 변경?왜
			// List<ChartVo> dvcid = sql.selectList(namespace + "getmstitmprd", RWMATNO);
			// List<ChartVo> getinfo=sql.selectList(namespace + "getRcvCnt", chartVo); //재고,
			// 입고, 불출, 완성수량을 빼옴
			
			sql.update(namespace + "shipStockUpdate", chartVo);	//출하 수량 재고 빼기
			
			sql.insert(namespace + "shipManager" ,chartVo);
			
			
			
			//3193 ~ 3206 주석 지우기 
			
			/*ChartVo getinfo = (ChartVo) sql.selectOne(namespace + "getRcvCnt", chartVo);
			chartVo.setPrdCnt(getinfo.getPrdCnt());
			chartVo.setCmplCnt(getinfo.getCmplCnt());
			chartVo.setOutCnt(getinfo.getOutCnt());
			chartVo.setCnt(getinfo.getCnt());

			System.out.println("여기까지 보여주기 : " + chartVo);
			sql.insert(namespace + "addCmplStockHistory", chartVo); // 다시 추가함

			// 임의로
			oprNmstatus = "1000";
			chartVo.setOprNm(oprNmstatus);
			sql.update(namespace + "minusComplStock", chartVo); // 재고 수량 수정
			
*/			// �빊�뮉釉� 占쎌뿳占쎈뮞占쎈꽅�뵳占�

			/*
			 * sql.update(namespace + "setLatestData", chartVo); //로트번호와 같은것의 latest를 0으로
			 * 변경?왜 chartVo.setCnt((String) sql.selectOne(namespace + "getCmplCnt",
			 * chartVo)); //완성수량을 빼옴 chartVo.setRcvCnt((Integer) sql.selectOne(namespace +
			 * "getRcvCnt", chartVo)); //입고수량을 빼옴
			 * 
			 * // sql.insert(namespace + "addCmplStockHistory", chartVo); //다시 추가함
			 * sql.update(namespace + "minusComplStock", chartVo); //재고 수량 수정
			 */ }

		String str = "";

		try {
			// if(list.size()>0) sql.insert(namespace + "addPrdData", list); //출하현황조회 추가
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}

		return str;
	}

	@Override
	public String getStockStatus(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getStockStatus", chartVo);
		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "utf-8"));
			map.put("rcvCnt", dataList.get(i).getRcvCnt());
			map.put("iniohdCnt", dataList.get(i).getIniohdCnt());
			map.put("issCnt", dataList.get(i).getIssCnt());
			map.put("notiCnt", dataList.get(i).getNotiCnt());
			map.put("ohdCnt", dataList.get(i).getOhdCnt());
			map.put("cnt", dataList.get(i).getCnt());
			map.put("spec", dataList.get(i).getSpec());
			map.put("oprNm", URLEncoder.encode(dataList.get(i).getOprNm(),"utf-8"));

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getReleaseInfoHist(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getReleaseInfoHist", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "utf-8"));
			map.put("spec", dataList.get(i).getSpec());
			map.put("lotNo", dataList.get(i).getLotNo());
			map.put("outLot", dataList.get(i).getOutLot());
			map.put("date", dataList.get(i).getDate());
			map.put("outCnt", dataList.get(i).getOutCnt());
			map.put("sDate", dataList.get(i).getSDate());
			map.put("eDate", dataList.get(i).getEDate());
			map.put("prc", dataList.get(i).getPrc());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getLotTracer(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getLotTracer", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "utf-8"));
			map.put("oprNo", dataList.get(i).getOprNo());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("worker", URLEncoder.encode(dataList.get(i).getWorker(), "utf-8"));
			map.put("lotNo", dataList.get(i).getLotNo());
			map.put("shipId", dataList.get(i).getShipId());
			map.put("shipLotNo", dataList.get(i).getShipLotNo());
			map.put("date", dataList.get(i).getDate());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getLotNo(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getLotNo", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("lotNo", dataList.get(i).getLotNo());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	};

	@Override
	public String getShipLotNo(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getShipLotNo", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("lotNo", dataList.get(i).getLotNo());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String chkLogin(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		try {
			String exist = (String) sql.selectOne(namespace + "chkLogin", chartVo);

			if (exist != "" || exist != null) {
				str = exist;
			} else {
				str = "fail";
			}
		} catch (Exception e) {
			e.printStackTrace();
			str = "fail";
		}
		return str;
	}

	@Override
	public String getOutLotTracer(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getOutLotTracer", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "utf-8"));
			map.put("oprNo", dataList.get(i).getOprNo());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("worker", URLEncoder.encode(dataList.get(i).getWorker(), "utf-8"));
			map.put("lotNo", dataList.get(i).getLotNo());
			map.put("shipLotNo", dataList.get(i).getShipLotNo());
			map.put("date", dataList.get(i).getDate());
			map.put("shipId", dataList.get(i).getShipId());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getOprNoList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getOprNoList", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("oprNm", dataList.get(i).getOprNm());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String transferOprCnt(String val) throws Exception {
		SqlSession sql = getSqlSession();

		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			ChartVo chartVo = new ChartVo();
			chartVo.setPrdNo(tempObj.get("prdNo").toString());
			chartVo.setOprNm(tempObj.get("oprNm").toString());
			chartVo.setDate(tempObj.get("date").toString());
			chartVo.setPreOpr(tempObj.get("preOpr").toString());
			chartVo.setSendCnt(Integer.parseInt(tempObj.get("sendCnt").toString()));

			int StockeExist = (Integer) sql.selectOne(namespace + "chkPreOprCnt", chartVo);
			// 이전 공정 수량 -
			if (StockeExist > 0) {
				sql.update(namespace + "minusPreOprCnt", chartVo);
			} else {
				sql.insert(namespace + "addPreOprCnt", chartVo);
			}

			int exist = (Integer) sql.selectOne(namespace + "chkOprCnt", chartVo);
			// 공정 수량 ++
			if (exist > 0) {
				sql.update(namespace + "updateOprCnt", chartVo);
			} else {
				sql.insert(namespace + "addOprCnt", chartVo);
			}

			list.add(chartVo);
		}

		String str = "";

		try {

			if (list.size() > 0)
				sql.insert(namespace + "addOutCnt", list); // 불출현황조회 추가
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}

		return str;
	}

	@Override
	public String getStockInfoByTable(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getStockInfoByTable", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();

			map.put("id", dataList.get(i).getIdx());
			map.put("oprNm", dataList.get(i).getOprNm());
			map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "utf-8"));
			map.put("lotCnt", dataList.get(i).getLotCnt());
			map.put("lotNo", dataList.get(i).getLotNo());
			map.put("lotStock", dataList.get(i).getLotStock());
			map.put("regdate", dataList.get(i).getRegDate());
			map.put("nowDvcName", dataList.get(i).getDvcId());
			map.put("groupLotNo", dataList.get(i).getGroupLotNo());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}
	
	@Override
	public String getStockInfoByTableLine(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getStockInfoByTableLine1", chartVo);
		
		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			
			map.put("id", dataList.get(i).getIdx());
			map.put("oprNm", dataList.get(i).getOprNm());
			map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "utf-8"));
			map.put("lotCnt", dataList.get(i).getLotCnt());
			map.put("lotNo", dataList.get(i).getLotNo());
			map.put("lotStock", dataList.get(i).getLotStock());
			map.put("deliveryNo",dataList.get(i).getDeliveryNo());
			map.put("regdate", dataList.get(i).getRegDate());
			map.put("nowDvcName", dataList.get(i).getDvcId());
			map.put("groupLotNo", dataList.get(i).getGroupLotNo());
			
			list.add(map);
		}
		;
		
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getReOpList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getReOpList", chartVo);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();

			map.put("total", dataList.get(i).getPartCyl());
			map.put("dvcName", URLEncoder.encode(dataList.get(i).getDvcName(), "utf-8"));
			map.put("date", dataList.get(i).getDate());
			map.put("prgName", dataList.get(i).getPrgName());
			map.put("chkSpdLoad", dataList.get(i).getChkSpdLoad());
			map.put("cylTm", dataList.get(i).getCylTm());
			map.put("cylAvgTm", dataList.get(i).getCylAvgTm());
			map.put("cylSdTm", dataList.get(i).getCylSdTm());
			map.put("totalSpdLoad", dataList.get(i).getTotalSpdLoad());
			map.put("totalAvgSpdLoad", dataList.get(i).getTotalAvgSpdLoad());
			map.put("totalSdSpdLoad", dataList.get(i).getTotalSdSpdLoad());
			map.put("smplCnt", dataList.get(i).getSmplCnt());
			map.put("sDate", dataList.get(i).getSDate());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getDeliverer(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> chartData = sql.selectList(namespace + "getDeliverer", chartVo);
		List list = new ArrayList();
		for (int i = 0; i < chartData.size(); i++) {
			Map map = new HashMap();
			map.put("id", chartData.get(i).getId());
			map.put("name", URLEncoder.encode(chartData.get(i).getName(), "utf-8"));

			list.add(map);
		}
		;

		String str = "";

		Map resultMap = new HashMap();
		resultMap.put("chartStatus", list);
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);
		return str;
	}

	@Override
	public String getDeliveryHistory(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> chartData = sql.selectList(namespace + "getDeliveryHistory", chartVo);
		List list = new ArrayList();
		for (int i = 0; i < chartData.size(); i++) {
			Map map = new HashMap();
			map.put("id", chartData.get(i).getId());
			map.put("delivererId", chartData.get(i).getDelivererId());
			map.put("lotNo", chartData.get(i).getLotNo());
			map.put("prdNo", chartData.get(i).getPrdNo());
			map.put("spec", chartData.get(i).getSpec());
			map.put("unit", chartData.get(i).getUnit());
			map.put("prc", chartData.get(i).getPrc());
			map.put("totalPrc", chartData.get(i).getTotalPrc());
			map.put("cnt", chartData.get(i).getCnt());
			map.put("deliveryDate", chartData.get(i).getDeliveryDate());
			map.put("deliveryNo", chartData.get(i).getDeliveryNo());

			list.add(map);
		}
		;

		String str = "";

		Map resultMap = new HashMap();
		resultMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);
		return str;
	}

	@Override
	public String getDeliveryNo(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		str = (String) sql.selectOne(namespace + "getDeliveryNo", chartVo);
		if (str.equals("1")) {
			str = chartVo.getSDate().replaceAll("-", "").substring(2) + "00001";
		}

		return str;
	}

	@Override
	public String getDeliverInfo(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> chartData = sql.selectList(namespace + "getDeliverInfo", chartVo);
		List list = new ArrayList();
		for (int i = 0; i < chartData.size(); i++) {
			Map map = new HashMap();
			map.put("id", chartData.get(i).getId());
			map.put("name", URLEncoder.encode(chartData.get(i).getName(), "utf-8"));
			map.put("prdNo", chartData.get(i).getPrdNo());
			map.put("spec", chartData.get(i).getSpec());
			map.put("lotNo", chartData.get(i).getLotNo());
			map.put("spec", chartData.get(i).getSpec());
			map.put("cnt", chartData.get(i).getCnt());
			map.put("stockid", chartData.get(i).getStockid());
			map.put("totaldelivery", chartData.get(i).getTotaldelivery());
			map.put("barcode", chartData.get(i).getBarcode());
			map.put("date", chartData.get(i).getDate());

			list.add(map);
		}
		;

		String str = "";

		Map resultMap = new HashMap();
		resultMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);
		return str;
	};

	@Override
	public String delDeliverHistory(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		try {
			sql.delete(namespace + "delDeliverHistory", chartVo);
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}
		return str;
	}

	@Override
	public String addDeliveryDate(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		try {
			sql.update(namespace + "addDeliveryDate", chartVo);
			str = "success";
		} catch (Exception e) {

			str = "fail";
		}
		return str;
	}

	@Override
	public String addDeliveryHst(String val) throws Exception {
		SqlSession sql = getSqlSession();

		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			ChartVo chartVo = new ChartVo();
			chartVo.setDelivererId(tempObj.get("delivererId").toString());
			chartVo.setLotNo(tempObj.get("lotNo").toString());
			chartVo.setPrdNo(tempObj.get("prdNo").toString());
			chartVo.setSpec(tempObj.get("spec").toString());
			chartVo.setUnit(tempObj.get("unit").toString());
			chartVo.setCnt(tempObj.get("cnt").toString());
			chartVo.setTotalPrc(tempObj.get("totalPrc").toString());
			chartVo.setDeliveryDate(tempObj.get("deliveryDate").toString());
			// chartVo.setDeliveryNo(tempObj.get("deliveryNo").toString());

			list.add(chartVo);
		}

		String str = "";

		try {
			sql.insert(namespace + "addDeliveryHst", list);
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}

		return str;
	};

	@Override
	public String updateHistory(String val) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";

		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			ChartVo chartVo = new ChartVo();
			chartVo.setId(tempObj.get("id").toString());

			list.add(chartVo);
		}

		try {
			sql.update(namespace + "updateHistory", list);
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}
		return str;
	}

	@Override
	public String login_worker(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		try {
			int exist = (int) sql.selectOne(namespace + "login_worker", chartVo);
			if (exist == 0) {
				str = "fail";
			} else {
				str = "success";
			}
		} catch (Exception e) {
			str = "fail";
		}
		return str;
	}

	@Override
	public String getNonOpHist(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> chartData = sql.selectList(namespace + "getNonOpHist", chartVo);
		List list = new ArrayList();
		for (int i = 0; i < chartData.size(); i++) {
			Map map = new HashMap();
			map.put("name", URLEncoder.encode(chartData.get(i).getName(), "utf-8"));
			map.put("reason", URLEncoder.encode(chartData.get(i).getReason(), "utf-8"));
			map.put("worker", URLEncoder.encode(chartData.get(i).getWorker(), "utf-8"));
			map.put("sDate", chartData.get(i).getSDate());
			map.put("eDate", chartData.get(i).getEDate());
			map.put("time", chartData.get(i).getTime());
			map.put("chartStatus", chartData.get(i).getChartStatus());
			list.add(map);
		}
		;

		String str = "";

		Map resultMap = new HashMap();
		resultMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);
		return str;
	}

	@Override
	public String addNonOpHst(String val) throws Exception {
		SqlSession sql = getSqlSession();

		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			ChartVo chartVo = new ChartVo();
			// chartVo.setId(tempObj.get("id").toString());
			chartVo.setWorker(tempObj.get("worker").toString());
			chartVo.setDvcId(tempObj.get("dvcId").toString());
			chartVo.setDate(tempObj.get("date").toString());
			chartVo.setNonOpTy(tempObj.get("nonOpTy").toString());
			chartVo.setSDate(tempObj.get("sDate").toString());
			chartVo.setEDate(tempObj.get("eDate").toString());
			chartVo.setNonOpTime(tempObj.get("nonOpTime").toString());

			list.add(chartVo);
		}

		String str = "";

		try {
			sql.insert(namespace + "addNonOpHst", list);
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}

		return str;
	}

	@Override
	public String getCdGroup1(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> chartData = sql.selectList(namespace + "getCdGroup1", chartVo);
		List list = new ArrayList();
		for (int i = 0; i < chartData.size(); i++) {
			Map map = new HashMap();
			map.put("attendanceTy", chartData.get(i).getId());
			map.put("attendanceTyName", URLEncoder.encode(chartData.get(i).getName(), "utf-8"));
			list.add(map);
		}
		;

		String str = "";

		Map resultMap = new HashMap();
		resultMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);
		return str;
	}

	@Override
	public String getCdGroup2(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> chartData = sql.selectList(namespace + "getCdGroup2", chartVo);
		List list = new ArrayList();
		for (int i = 0; i < chartData.size(); i++) {
			Map map = new HashMap();
			map.put("attendanceDiv", chartData.get(i).getId());
			map.put("attendanceDivName", URLEncoder.encode(chartData.get(i).getName(), "utf-8"));
			list.add(map);
		}
		;

		String str = "";

		Map resultMap = new HashMap();
		resultMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);
		return str;
	}

	@Override
	public String getCdGroup3(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> chartData = sql.selectList(namespace + "getCdGroup3", chartVo);
		List list = new ArrayList();
		for (int i = 0; i < chartData.size(); i++) {
			Map map = new HashMap();
			map.put("attendance", chartData.get(i).getId());
			map.put("attendanceName", URLEncoder.encode(chartData.get(i).getName(), "utf-8"));
			list.add(map);
		}
		;

		String str = "";

		Map resultMap = new HashMap();
		resultMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);
		return str;
	}

	@Override
	public String getAttendanceHist(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> chartData = sql.selectList(namespace + "getAttendanceHist", chartVo);
		List list = new ArrayList();
		for (int i = 0; i < chartData.size(); i++) {
			Map map = new HashMap();
			map.put("id", chartData.get(i).getId());
			map.put("worker", chartData.get(i).getWorker());
			map.put("name", URLEncoder.encode(chartData.get(i).getName(),"utf-8"));
			map.put("group1", chartData.get(i).getGroup1());
			map.put("attendanceTy", URLEncoder.encode(chartData.get(i).getAttendanceTy(),"utf-8"));
			map.put("group2", chartData.get(i).getGroup2());
			map.put("attendanceDiv", URLEncoder.encode(chartData.get(i).getAttendanceDiv(),"utf-8"));
			map.put("group3", chartData.get(i).getGroup3());
			map.put("attendance", URLEncoder.encode(chartData.get(i).getAttendance(),"utf-8"));
			map.put("date", chartData.get(i).getDate());
			map.put("sDate", chartData.get(i).getSDate());
			map.put("eDate", chartData.get(i).getEDate());
			map.put("approve", chartData.get(i).getApprove());
			map.put("reason", URLEncoder.encode(chartData.get(i).getReason(), "utf-8"));
			map.put("msg", URLEncoder.encode(chartData.get(i).getMsg(), "utf-8"));

			list.add(map);
		}
		;

		String str = "";

		Map resultMap = new HashMap();
		resultMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);
		return str;
	}

	@Override
	public String addAttendanceHst(String val) throws Exception {
		SqlSession sql = getSqlSession();

		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			ChartVo chartVo = new ChartVo();
			chartVo.setWorker(tempObj.get("worker").toString());
			chartVo.setGroup1(tempObj.get("attendanceTy").toString());
			chartVo.setGroup2(tempObj.get("attendanceDiv").toString());
			chartVo.setGroup3(tempObj.get("attendance").toString());
			chartVo.setReason(tempObj.get("reason").toString());
			chartVo.setDate(tempObj.get("date").toString());
			chartVo.setEDate(tempObj.get("sDate").toString());
			chartVo.setSDate(tempObj.get("eDate").toString());
			chartVo.setApprove(tempObj.get("approve").toString());

			list.add(chartVo);
		}

		String str = "";

		try {
			sql.insert(namespace + "addAttendanceHst", list);
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}

		return str;
	}

	@Override
	public String updateAttendance(String val) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";

		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			ChartVo chartVo = new ChartVo();
			chartVo.setId(tempObj.get("id").toString());
			chartVo.setReason(tempObj.get("reason").toString());
			chartVo.setSDate(tempObj.get("sDate").toString());
			chartVo.setEDate(tempObj.get("eDate").toString());
			chartVo.setDate(tempObj.get("date").toString());
			chartVo.setAttendanceTy(tempObj.get("attendanceTy").toString());
			chartVo.setAttendanceDiv(tempObj.get("attendanceDiv").toString());
			chartVo.setAttendance(tempObj.get("attendance").toString());
			chartVo.setApprove(tempObj.get("approve").toString());

			list.add(chartVo);
		}

		try {
			sql.update(namespace + "updateAttendance", list);
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}
		return str;
	}

	@Override
	public String addWorkTimeHist(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		try {

			int exist = (int) sql.selectOne(namespace + "chkWorkTimeHist", chartVo);

			if (exist != 0) {
				str = "duple";
			} else {
				sql.insert(namespace + "addWorkTimeHist", chartVo);

				str = "success";
			}
		} catch (Exception e) {
			str = "fail";
		}
		return str;
	}
	
	@Override
	public String addWorkEndTimeHist(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		try {
			
			int exist = (int) sql.selectOne(namespace + "chkWorkEndTimeHist", chartVo);
			
			int existWork = (int) sql.selectOne(namespace + "chkWorkTimeHist", chartVo);
			
			if (exist >= 1) {
				System.out.println("중복들어옴");
				str = "duple";
			}else if(existWork == 0) {
				System.out.println("출근안함");
				str = "No";
			}else {
				sql.update(namespace + "addWorkEndTimeHist", chartVo);
				
				str = "success";
			}
		} catch (Exception e) {
			str = "fail";
		}
		return str;
	}

	@Override
	public String getOprNameList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> chartData = sql.selectList(namespace + "getOprNameList", chartVo);
		List list = new ArrayList();
		for (int i = 0; i < chartData.size(); i++) {
			Map map = new HashMap();
			map.put("name", chartData.get(i).getName());

			list.add(map);
		}
		;

		String str = "";

		Map resultMap = new HashMap();
		resultMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);
		return str;
	}

	@Override
	public String setPrdCmplData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> chartData = sql.selectList(namespace + "setPrdCmplData", chartVo);
		List list = new ArrayList();
		for (int i = 0; i < chartData.size(); i++) {
			Map map = new HashMap();
			map.put("dvcId", chartData.get(i).getDvcId());
			map.put("workerCnt", chartData.get(i).getWorkerCnt());
			map.put("name", URLEncoder.encode(chartData.get(i).getName(), "utf-8"));
			map.put("tgCnt", chartData.get(i).getTgCnt());
			map.put("cntCyl", chartData.get(i).getCntCyl());
			map.put("partCyl", chartData.get(i).getPartCyl());
			map.put("lot1", chartData.get(i).getLot1());
			map.put("cnt1", chartData.get(i).getCnt1());
			map.put("lot2", chartData.get(i).getLot2());
			map.put("cnt2", chartData.get(i).getCnt2());
			map.put("lot3", chartData.get(i).getLot3());
			map.put("cnt3", chartData.get(i).getCnt3());
			map.put("lot4", chartData.get(i).getLot4());
			map.put("cnt4", chartData.get(i).getCnt4());
			map.put("lot5", chartData.get(i).getLot5());
			map.put("cnt5", chartData.get(i).getCnt5());

			list.add(map);
		}
		;

		String str = "";

		Map resultMap = new HashMap();
		resultMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);
		return str;
	};

	@Override
	public String getTodayWorkByName(ChartVo chartVo) throws Exception {

		SqlSession sql = getSqlSession();
		List<ChartVo> chartData = sql.selectList(namespace + "getTodayWorkByName", chartVo);
		List list = new ArrayList();

		for (int i = 0; i < chartData.size(); i++) {

			Map map = new HashMap();
			map.put("dvcId", chartData.get(i).getDvcId());
			map.put("workTy", chartData.get(i).getWorkIdx());
			map.put("prdNo", chartData.get(i).getPrdNo());
			map.put("name", URLEncoder.encode(chartData.get(i).getName(), "utf-8"));
			map.put("tgCnt", chartData.get(i).getTgCnt());
			map.put("cntCyl", chartData.get(i).getCntCyl());
			map.put("partCyl", chartData.get(i).getPartCyl());
			map.put("workerCnt", chartData.get(i).getWorkerCnt());
			map.put("lot1", chartData.get(i).getLot1());
			map.put("cnt1", chartData.get(i).getCnt1());
			map.put("lot2", chartData.get(i).getLot2());
			map.put("cnt2", chartData.get(i).getCnt2());
			map.put("lot3", chartData.get(i).getLot3());
			map.put("cnt3", chartData.get(i).getCnt3());
			map.put("lot4", chartData.get(i).getLot4());
			map.put("cnt4", chartData.get(i).getCnt4());
			map.put("lot5", chartData.get(i).getLot5());
			map.put("cnt5", chartData.get(i).getCnt5());
			map.put("empCd", chartData.get(i).getEmpCd());

			list.add(map);
		}
		;

		String str = "";

		Map resultMap = new HashMap();
		resultMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);

		return str;
	}

	@Override
	public String delAttendanceHistory(String id) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		try {
			sql.delete(namespace + "delAttendanceHistory", id);
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}
		return str;
	}

	@Override
	public String delNonOpHistory(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		try {
			sql.delete(namespace + "delNonOpHistory", chartVo);
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}
		return str;
	}

	@Override
	public String addPrdComplHist(String val) throws Exception {
		SqlSession sql = getSqlSession();

		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);
			ChartVo chartVo = new ChartVo();
			ChartVo stckinfo = new ChartVo();
			chartVo.setWorker(tempObj.get("worker").toString());
			chartVo.setPrdNo(tempObj.get("prdNo").toString());
			chartVo.setDvcId(tempObj.get("dvcId").toString());
			chartVo.setDate(tempObj.get("date").toString());
			chartVo.setTy(tempObj.get("ty").toString());
			try {
				chartVo.setWorkerCnt(Integer.parseInt(tempObj.get("workerCnt").toString()));
				System.out.println("※작업자 수량 :: "+chartVo.getWorkerCnt());
				chartVo.setLot1(tempObj.get("lot1").toString());
				chartVo.setLot2(tempObj.get("lot2").toString());
				chartVo.setLot3(tempObj.get("lot3").toString());
				chartVo.setLot4(tempObj.get("lot4").toString());
				chartVo.setLot5(tempObj.get("lot5").toString());
			} catch (Exception e) {
				e.printStackTrace();
			}

			/*
			 * stckinfo = (ChartVo) sql.selectOne(namespace + "stckdvc", chartVo);
			 * chartVo.setOprNo(stckinfo.getOprNo()); chartVo.setIndex(stckinfo.getIndex());
			 * chartVo.setOprNm(stckinfo.getOprNm());
			 */
			list.add(chartVo);

			/*
			 * sql.update(namespace + "workstckminus", chartVo);
			 * 
			 * if (chartVo.getOprNo().equals("0010")) { sql.update(namespace +
			 * "Rworkstckminus", chartVo); // R일때 chartVo.setOprNo("0020");
			 * sql.update(namespace + "workstckplus", chartVo); } else if
			 * (chartVo.getOprNo().equals("0020")) { chartVo.setOprNo("0030");
			 * sql.update(namespace + "workstckplus", chartVo); } else if
			 * (chartVo.getOprNo().equals("0030")) { chartVo.setOprNo("1000");
			 * sql.update(namespace + "workstckplus", chartVo); }
			 */
		}

		String str = "";

		try {
			sql.delete(namespace + "delComplHist", list);
			sql.insert(namespace + "addPrdComplHist", list);
			str = "success";
		} catch (Exception e) {
			e.printStackTrace();
			str = "fail";
		}

		return str;
	}

	@Override
	public String updateAttendance(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		try {
			sql.update(namespace + "updateAttendance", chartVo);
			str = "success";
		} catch (Exception e) {
			e.printStackTrace();
			str = "fail";
		}
		return str;
	}

	@Override
	public String updateNonOpHist(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		try {
			sql.update(namespace + "updateNonOpHist", chartVo);
			str = "success";
		} catch (Exception e) {
			e.printStackTrace();
			str = "fail";
		}
		return str;
	}

	@Override
	public String getAllDeliverer(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> chartData = sql.selectList(namespace + "getAllDeliverer", chartVo);
		List list = new ArrayList();
		for (int i = 0; i < chartData.size(); i++) {
			Map map = new HashMap();
			map.put("delivererId", chartData.get(i).getDelivererId());
			map.put("delivererName", URLEncoder.encode(chartData.get(i).getDelivererName(), "utf-8"));

			list.add(map);
		}
		;

		String str = "";

		Map resultMap = new HashMap();
		resultMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);
		return str;
	}

	@Override
	public ChartVo getPrcNSpec(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		chartVo = (ChartVo) sql.selectOne(namespace + "getPrcNSpec", chartVo);
		return chartVo;
	}

	@Override
	public String updateMachnieWorker(String val) throws Exception {
		SqlSession sql = getSqlSession();

		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			ChartVo chartVo = new ChartVo();
			chartVo.setDvcId(tempObj.get("dvcId").toString());
			chartVo.setWorkerD(tempObj.get("workerD").toString());
			chartVo.setWorkerN(tempObj.get("workerN").toString());

			list.add(chartVo);

		}

		String str = "";

		try {
			sql.update(namespace + "updateMachnieWorker", list);

			str = "success";
		} catch (Exception e) {
			str = "fail";
		}

		return str;
	}

	@Override
	public String getOprNo4Routing(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> chartData = sql.selectList(namespace + "getOprNo4Routing", chartVo);
		List list = new ArrayList();
		for (int i = 0; i < chartData.size(); i++) {
			Map map = new HashMap();
			map.put("oprNo", chartData.get(i).getOprNo());

			list.add(map);
		}
		;

		String str = "";

		Map resultMap = new HashMap();
		resultMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);
		return str;
	}

	@Override
	public String getDvc(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> chartData = sql.selectList(namespace + "getDvc", chartVo);
		List list = new ArrayList();
		for (int i = 0; i < chartData.size(); i++) {
			Map map = new HashMap();
			map.put("dvcId", chartData.get(i).getDvcId());
//			map.put("dvcName", chartData.get(i).getDvcName());
			map.put("dvcName", URLEncoder.encode(chartData.get(i).getDvcName(), "UTF-8"));
			list.add(map);
		}
		;

		String str = "";

		Map resultMap = new HashMap();
		resultMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);
		return str;
	}

	@Override
	public String getRoutingInfo(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> chartData = sql.selectList(namespace + "getRoutingInfo", chartVo);
		List list = new ArrayList();
		for (int i = 0; i < chartData.size(); i++) {
			Map map = new HashMap();
			map.put("dvcId", chartData.get(i).getDvcId());
			map.put("wcCd", chartData.get(i).getWcCd());
			map.put("machCd", chartData.get(i).getMachCd());
			map.put("mnPrgNo", chartData.get(i).getMnPrgNo());
			map.put("cmpCd", chartData.get(i).getCmpCd());
			map.put("cylTmSe", chartData.get(i).getCylTmSe());
			map.put("CVT", chartData.get(i).getCvt());
			map.put("oprNo", chartData.get(i).getOprNo());
			map.put("grpCd", chartData.get(i).getGrpCd());
			map.put("seq", chartData.get(i).getSeq());

			list.add(map);
		}
		;

		String str = "";

		Map resultMap = new HashMap();
		resultMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);
		return str;
	}

	@Override
	public String updatRouting(String val) throws Exception {
		SqlSession sql = getSqlSession();

		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			ChartVo chartVo = new ChartVo();
			chartVo.setDvcId(tempObj.get("dvcId").toString());
			chartVo.setOprNo(tempObj.get("oprNo").toString());
			chartVo.setWcCd(tempObj.get("wcCd").toString());
			chartVo.setMnPrgNo(tempObj.get("mnPrgNo").toString());
			chartVo.setCmpCd(tempObj.get("cmpCd").toString());
			chartVo.setGrpCd(tempObj.get("grpCd").toString());
			chartVo.setSeq(tempObj.get("seq").toString());
			chartVo.setPrdNo(tempObj.get("prdNo").toString());
			chartVo.setBeforeId(tempObj.get("beforeId").toString());

			list.add(chartVo);

		}

		String str = "";

		try {
			sql.update(namespace + "updatRouting", list);

			str = "success";
		} catch (Exception e) {
			e.printStackTrace();
			str = "fail";
		}

		return str;
	}

	@Override
	public String addRouting(String val) throws Exception {
		SqlSession sql = getSqlSession();

		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			ChartVo chartVo = new ChartVo();
			chartVo.setDvcId(tempObj.get("dvcId").toString());
			chartVo.setOprNo(tempObj.get("oprNo").toString());
			chartVo.setWcCd(tempObj.get("wcCd").toString());
			chartVo.setMnPrgNo(tempObj.get("mnPrgNo").toString());
			chartVo.setCmpCd(tempObj.get("cmpCd").toString());
			chartVo.setGrpCd(tempObj.get("grpCd").toString());
			chartVo.setSeq(tempObj.get("seq").toString());
			chartVo.setPrdNo(tempObj.get("prdNo").toString());

			list.add(chartVo);

		}

		String str = "";

		try {
			sql.insert(namespace + "addRouting", list);

			str = "success";
		} catch (Exception e) {
			e.printStackTrace();
			str = "fail";
		}

		return str;
	}

	@Override
	public String delRouting(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		try {
			sql.update(namespace + "delRouting", chartVo);
			str = "success";
		} catch (Exception e) {
			e.printStackTrace();
			str = "fail";
		}
		;
		return str;
	}

	@Override
	public String traceManagerUpdate(String val) throws Exception {
		String str = "";

		SqlSession sql = getSqlSession();
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");
		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);
			// (Integer.parseInt(tempObj.get("dvcId").toString()));

			ChartVo chartVo = new ChartVo();
			chartVo.setDvcId(tempObj.get("dvcId").toString());
			chartVo.setCd(tempObj.get("cd").toString());
			chartVo.setName(tempObj.get("name").toString());
			chartVo.setNameAbb(tempObj.get("nameAbb").toString());
			chartVo.setWcCd(tempObj.get("WCCD").toString());
			chartVo.setJig(tempObj.get("jig").toString());
			chartVo.setIsAuto(tempObj.get("isAuto").toString());
			chartVo.setType(tempObj.get("type").toString());
			chartVo.setEXCD(tempObj.get("EXCD").toString());
			chartVo.setNC(tempObj.get("NC").toString());
			chartVo.setCNTMCD(tempObj.get("CNTMCD").toString());
			chartVo.setEmpN(tempObj.get("empN").toString());
			chartVo.setEmpD(tempObj.get("empD").toString());
			//chartVo.setCntPerCyl(tempObj.get("cntPerCyl").toString());

			list.add(chartVo);
		}
		try {
			sql.update(namespace + "traceManagerUpdate", list);
			//sql.update(namespace + "updateCntPerCyl", list);
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}

		return str;
	}

	@Override
	public void traceManagerDelete(String delname) throws Exception {
		SqlSession sql = getSqlSession();
		sql.delete(namespace + "traceManagerDelete", delname); 

	}

	@Override
	public String traceManagerCount(ChartVo chartVo, String length) throws Exception {
		SqlSession sql = getSqlSession();
		String relength = (String) sql.selectOne(namespace + "traceManagerCount", chartVo);
		return relength;
	}

	@Override
	public String traceManagerInsert(ChartVo chartVo, int length, String val, int maxlength) throws Exception {
		String str = "";
		SqlSession sql = getSqlSession();
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");
		List<ChartVo> list = new ArrayList<ChartVo>();

		for (int i = jsonArray.size() - (maxlength - length); i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);
			// (Integer.parseInt(tempObj.get("dvcId").toString()));

			ChartVo chVo = new ChartVo();
			chVo.setDvcId(tempObj.get("dvcId").toString());
			chVo.setCd(tempObj.get("cd").toString());
			chVo.setName(tempObj.get("name").toString());
			chVo.setNameAbb(tempObj.get("nameAbb").toString());
			chVo.setWcCd(tempObj.get("WCCD").toString());
			chVo.setJig(tempObj.get("jig").toString());
			chVo.setIsAuto(tempObj.get("isAuto").toString());
			chVo.setType(tempObj.get("type").toString());
			chVo.setEXCD(tempObj.get("EXCD").toString());
			chVo.setNC(tempObj.get("NC").toString());
			chVo.setCNTMCD(tempObj.get("CNTMCD").toString());
			chVo.setEmpN(tempObj.get("empN").toString());
			chVo.setEmpD(tempObj.get("empD").toString());
			chVo.setId(tempObj.get("ip").toString());
			list.add(chVo);
		}
		try {
			sql.insert(namespace + "traceManagerInsert", list);
			str = "success";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return str;
	}

	@Override
	public String getmstmat(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getmstmat", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("MANDT", dataList.get(i).getMANDT());
			map.put("PLNT", dataList.get(i).getPLNT());
			map.put("MATNO", dataList.get(i).getMATNO());
			map.put("MATNM", URLEncoder.encode(dataList.get(i).getMATNM(), "UTF-8"));
			map.put("ITEMNO", URLEncoder.encode(dataList.get(i).getITEMNO(), "UTF-8"));
			map.put("RWMATNO", dataList.get(i).getRWMATNO());
			map.put("VNDNM", dataList.get(i).getVNDNM());
			// map.put("SPL_NM", URLEncoder.encode(dataList.get(i).getIsAuto(),"UTF-8"));
			map.put("SPLNM", dataList.get(i).getSPLNM());
			map.put("PRCE", dataList.get(i).getPRCE());
			map.put("SPEC", dataList.get(i).getSpec());
			map.put("ITVDV", dataList.get(i).getITVDV());
			map.put("SEQ", dataList.get(i).getSeq());
			map.put("UNIT", dataList.get(i).getUnit());
			list.add(map);

		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);

		return str;
	}

	@Override
	public void mstmatDelete(String delname) throws Exception {
		SqlSession sql = getSqlSession();
		sql.delete(namespace + "mstmatDelete", delname);

	}

	@Override
	public String mstmatUpdate(String val) throws Exception {
		String str = "";

		SqlSession sql = getSqlSession();
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");
		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);
			// (Integer.parseInt(tempObj.get("dvcId").toString()));

			ChartVo chartVo = new ChartVo();
			chartVo.setMANDT(tempObj.get("MANDT").toString());
			chartVo.setPLNT(tempObj.get("PLNT").toString());
			chartVo.setMATNO(tempObj.get("MATNO").toString());
			chartVo.setMATNM(tempObj.get("MATNM").toString());
			chartVo.setRWMATNO(tempObj.get("RWMATNO").toString());
			chartVo.setVNDNM(tempObj.get("VNDNM").toString());
			chartVo.setSPLNM(tempObj.get("SPLNM").toString());
			chartVo.setPRCE(tempObj.get("PRCE").toString());
			chartVo.setSpec(tempObj.get("SPEC").toString());
			chartVo.setITVDV(tempObj.get("ITVDV").toString());
			chartVo.setSeq(tempObj.get("SEQ").toString());
			chartVo.setUnit(tempObj.get("UNIT").toString());
			chartVo.setBefore(tempObj.get("before").toString());
			chartVo.setITEMNO(tempObj.get("ITEMNO").toString());

			list.add(chartVo);
		}
		try {
			sql.update(namespace + "mstmatUpdate", list);
			str = "success";
		} catch (Exception e) {
			e.printStackTrace();
			str = "fail";
		}

		return str;
	}

	@Override
	public String mstmatInsert(ChartVo chartVo, int length, String val, int maxlength) throws Exception {
		String str = "";
		SqlSession sql = getSqlSession();
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");
		List<ChartVo> list = new ArrayList<ChartVo>();

		for (int i = jsonArray.size() - (maxlength - length); i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);
			// (Integer.parseInt(tempObj.get("dvcId").toString()));

			ChartVo chVo = new ChartVo();
			chVo.setMANDT(tempObj.get("MANDT").toString());
			chVo.setPLNT(tempObj.get("PLNT").toString());
			chVo.setMATNO(tempObj.get("MATNO").toString());
			chVo.setMATNM(tempObj.get("MATNM").toString());
			chVo.setRWMATNO(tempObj.get("RWMATNO").toString());
			chVo.setVNDNM(tempObj.get("VNDNM").toString());
			chVo.setSPLNM(tempObj.get("SPLNM").toString());
			chVo.setPRCE(tempObj.get("PRCE").toString());
			chVo.setSpec(tempObj.get("SPEC").toString());
			chVo.setITVDV(tempObj.get("ITVDV").toString());
			chVo.setSeq(tempObj.get("SEQ").toString());

			list.add(chVo);
		}
		try {
			sql.insert(namespace + "mstmatInsert", list);
			str = "success";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return str;
	}

	@Override
	public String mstmatCount(ChartVo chartVo, String length) throws Exception {
		SqlSession sql = getSqlSession();
		String relength = (String) sql.selectOne(namespace + "mstmatCount", chartVo);
		return relength;
	}

	@Override
	public String delWorker(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		try {
			sql.update(namespace + "delWorker", chartVo);
			str = "success";
		} catch (Exception e) {
			e.printStackTrace();
			str = "fail";
		}
		return str;
	}

	
	
	/*
	 * @author John Joe ( 조부관 )
	 * @version 0.1
	 * @see com.unomic.dulink.chart.service.ChartService#getWorkerWatingTime(com.unomic.dulink.chart.domain.ChartVo)
	 * @return JSON String 
	 * 2017-11-24 비가동 집계를 위한 함수
	 * 현재 비가동 집계를 위한 코드가 Dulink장비 / 웹소스에 포함되어있지않아
	 * TimeChart에 집계되는 데이터를 바탕으로 구현
	 * TimeChart에서 해당하는 일자의 데이터를 모두 불러와 
	 * 비가동이 6분이상 지속되는 부분(TimeChart가 2분단위로 기록이 되므로 5분이상의 비가동 집계를 위해서는 6분단위가 최소임)을
	 * 추려내어 TB_WAITTIME에 따로 저장 (하루 단위로 비가동 집계를 하더라도 몇만건의 데이터 조회가 필요하므로 조회결과를 따로 저장)
	 * 다음번 비가동 집계시 TB_WAITTIME테이블에 저장 여부 판단하여 불러옴.
	 * 
	 */
	@Override
	public String getWorkerWatingTime(ChartVo chartVo) throws Exception {

		SqlSession sql = getSqlSession();
		List<ChartVo> compareList = sql.selectList(namespace+"checkWaitDay", chartVo);
		int compareSize = compareList.size();
		List<ChartVo> dataList = new ArrayList<ChartVo>();
		
		SimpleDateFormat mSimpleDateFormat1 = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA );
		Date sDate = mSimpleDateFormat1.parse(chartVo.getSDate());
		String sDate1 = mSimpleDateFormat1.format(sDate);
		
		Date currentTime = new Date ();
		String mTime = mSimpleDateFormat1.format ( currentTime );
		
		boolean check = false;
		
		if(mTime.equals(sDate1)) {
		}else {
			if(compareSize>0) {
				check= true;
			}
		}
		
		if(!check) {
			dataList = sql.selectList(namespace + "getWorkerWatingTime", chartVo);
		}else {
			dataList = sql.selectList(namespace + "getWorkerWatingTime", chartVo);

			//dataList = sql.selectList(namespace + "getWaitTime", chartVo);
		}
		
		int size = dataList.size();

		List list = new ArrayList();
		for (int i = 0; i < size; i++) {
			Map map = new HashMap();
			if(check) {
				map.put("checkNum", 1);
				map.put("exist", dataList.get(i).getExist());
			}
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("seq", dataList.get(i).getSeq());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "UTF-8"));
			map.put("sDate", dataList.get(i).getSDate());
			map.put("eDate", dataList.get(i).getEDate());
			map.put("oprNo", URLEncoder.encode(dataList.get(i).getOprNo(), "UTF-8"));
			map.put("chartStatus", dataList.get(i).getChartStatus());
			map.put("workIdx", dataList.get(i).getWorkIdx());
			map.put("checkNum", dataList.get(i).getCheckNum());
			map.put("waitTime", dataList.get(i).getWaitTime());
			map.put("totalWaitTime", dataList.get(i).getTotalWaitTime());
			map.put("empCd", dataList.get(i).getEmpCd());
			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);

		return str;
	}

	@Override
	public String getMaintenanceReport(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();

		List<ChartVo> dataList = sql.selectList(namespace + "getMaintenanceReport", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();

			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "UTF-8"));
			map.put("workIdx", dataList.get(i).getWorkIdx());
			map.put("date", dataList.get(i).getDate());
			map.put("lastAlarmMsg", dataList.get(i).getLastAlarmMsg());

			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);

		return str;
	}

	@Override
	public void setMaintenanceReport(ChartVo chartVo) throws Exception {

		SqlSession sql = getSqlSession();
		try {
			sql.insert(namespace + "setMaintenanceReport", chartVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public String getMaintenanceReportList(int empCd) throws Exception {
		SqlSession sql = getSqlSession();

		List<ChartVo> dataList = sql.selectList(namespace + "getMaintenanceReportList", empCd);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("idx", dataList.get(i).getIdx());
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("device", dataList.get(i).getDevice());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "UTF-8"));
			map.put("solver_Name", URLEncoder.encode(dataList.get(i).getSolverName(), "UTF-8"));
			map.put("regDate", dataList.get(i).getRegDate());
			map.put("lastAlarmMsg", dataList.get(i).getLastAlarmMsg());
			map.put("alarmCode", dataList.get(i).getAlarmCode());
			map.put("solver", dataList.get(i).getSolver());
			map.put("empCd", dataList.get(i).getEmpCd());
			map.put("report", URLEncoder.encode(dataList.get(i).getReport(), "UTF-8"));

			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);

		return str;
	}

	@Override
	public String getMaintenanceReportListdvcId(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();

		List<ChartVo> dataList = new ArrayList<ChartVo>();

		try {
			dataList = sql.selectList(namespace + "getMaintenanceReportList", chartVo);
		} catch (Exception e) {
			e.printStackTrace();
		}

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("idx", dataList.get(i).getIdx());
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("device", dataList.get(i).getDevice());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "UTF-8"));
			map.put("solver_Name", URLEncoder.encode(dataList.get(i).getSolverName(), "UTF-8"));
			map.put("regDate", dataList.get(i).getRegDate());
			map.put("lastAlarmMsg", dataList.get(i).getLastAlarmMsg());
			map.put("alarmCode", dataList.get(i).getAlarmCode());
			map.put("solver", dataList.get(i).getSolver());
			map.put("empCd", dataList.get(i).getEmpCd());
			map.put("report", URLEncoder.encode(dataList.get(i).getReport(), "UTF-8"));

			map.put("cause", dataList.get(i).getCause());
			map.put("result", dataList.get(i).getResult());
			map.put("actionEmpCd", dataList.get(i).getActionEmpCd());
			map.put("actionDate", dataList.get(i).getActionDate());

			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);

		return str;
	}

	@Override
	public void delMaintenanceReport(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		try {
			sql.delete(namespace + "delMaintenanceReport", chartVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// delbert 납품서발행취소
	@Override
	public String cancelInvoice(String val) throws Exception {
		String str = "";

		SqlSession sql = getSqlSession();
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			ChartVo chartVo = new ChartVo();
			chartVo.setId(tempObj.get("id").toString());

			sql.update(namespace + "cancelInvoice", chartVo);
		}

		/*
		 * for(int i=0; i<val.length(); i++) { }
		 */
		return str;
	}

	@Override
	public String getAttendant_List(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();

		List<ChartVo> dataList = new ArrayList<ChartVo>();

		try {
			dataList = sql.selectList(namespace + "getAttendant_List", chartVo);
		} catch (Exception e) {
			e.printStackTrace();
		}

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {

			Map map = new HashMap();

			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("group1", URLEncoder.encode(dataList.get(i).getGroup1(), "utf-8"));
			map.put("group2", URLEncoder.encode(dataList.get(i).getGroup2(), "utf-8"));
			map.put("group3", URLEncoder.encode(dataList.get(i).getGroup3(), "utf-8"));
			map.put("date", dataList.get(i).getDate());

			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);

		return str;
	}

	@Override
	@Transactional
	public String upDateWorkTimeHist(ChartVo chartVo) throws Exception {
		String str = "";
		try {
			sqlSessionTemplate.update(namespace + "upDateWorkTimeHist", chartVo);
			str = "success";
		} catch (Exception e) {
			e.printStackTrace();
			str = "fail";
		}

		return str;
	}

	@Override
	public String addWorkTimeHist_Correct(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		try {
			sql.insert(namespace + "addWorkTimeHist_Correct", chartVo);
			str = "success";
		} catch (Exception e) {
			e.printStackTrace();
			str = "fail";
		}

		return str;
	}

	@Override
	public String addNonOpHst_One(String val) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");
		List<ChartVo> list = new ArrayList<ChartVo>();
		
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);
			ChartVo chartVo = new ChartVo();
			chartVo.setEmpCd(tempObj.get("empCd").toString());
			chartVo.setDvcId(tempObj.get("dvcId").toString());
			chartVo.setTime(tempObj.get("time").toString());
			chartVo.setSDate(tempObj.get("sDate").toString());
			chartVo.setEDate(tempObj.get("eDate").toString());
			chartVo.setChartStatus(tempObj.get("chartStatus").toString());
			chartVo.setReason(tempObj.get("reason").toString());
//		   	chartVo.setRawIdx(Integer.parseInt(tempObj.get("seq").toString()));
//			chartVo.setIdx(tempObj.get("idx").toString());
			list.add(chartVo);
		}
		
		try {
			sql.delete(namespace + "delNonOpHistory", list);
			sql.update(namespace + "updNonOpHst", list);
			sql.insert(namespace + "addNonOpHst_One", list);
			str = "success";
		} catch (Exception e) {
			e.printStackTrace();
			str = "fail";
		}

		return str;
	}

	@Override
	public String getmstmatno(String RWMATNO) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getmstmatno", RWMATNO);

		List list = new ArrayList<ChartVo>();
		if (dataList.size() == 0) {
			List<ChartVo> dvcid = sql.selectList(namespace + "getmstitmprd", RWMATNO);
			for (int j = 0; j < dvcid.size(); j++) {
				Map map = new HashMap();
				map.put("dvcId", dvcid.get(j).getDvcId());
				map.put("oprNo", dvcid.get(j).getOprNo());
				list.add(map);
			}
		} else {
			for (int i = 0; i < dataList.size(); i++) {
				String prdNo = dataList.get(i).getMATNO();
				List<ChartVo> dvcid = sql.selectList(namespace + "getmstitmprd", prdNo);
				// map.put("MATNO", dataList.get(i).getMATNO());
				for (int j = 0; j < dvcid.size(); j++) {
					Map map = new HashMap();
					map.put("dvcId", dvcid.get(j).getDvcId());
					map.put("oprNo", dvcid.get(j).getOprNo());
					list.add(map);
				}
			}
			;
		}
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getbertmstmatno(String RWMATNO) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getbertmstmatno", RWMATNO);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("MATNO", dataList.get(i).getMATNO());
			map.put("RWMATNO", dataList.get(i).getRWMATNO());
			map.put("ITEMNO", dataList.get(i).getITEMNO());
			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getmstitmprd(String RWMATNO) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getmstitmprd", RWMATNO);

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("MATNO", dataList.get(i).getWcCd());
			map.put("dvcId", dataList.get(i).getDvcId());
			list.add(map);
		}
		;
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String barcodebulchul(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();

		ChartVo dataList = (ChartVo) sql.selectOne(namespace + "barcodebulchul", chartVo);

		chartVo.setId(dataList.getId());
		chartVo.setPrdNo(dataList.getPrdNo());
		chartVo.setLotNo(dataList.getLotNo());
		chartVo.setSendCnt(dataList.getSendCnt());
		chartVo.setInputCnt(Integer.toString(dataList.getSendCnt()));
		chartVo.setPreOpr("0000");
		chartVo.setOprNm("0010");
		chartVo.setOprNo("0010");

		if (dataList.getCount() == 0) {
			return "dd";
		} else {
			sql.insert(namespace + "barcodeaddReleaseStock", chartVo); // 불출현황조회 추가

			int chk = (int) sql.selectOne(namespace + "bertchkhstship", chartVo);
			if (chk >= 1) {
				sql.update(namespace + "shipmentupdbert", chartVo); // 출하관리로 이동
			} else {
				sql.insert(namespace + "shipmentaddbert", chartVo); // 출하관리로 이동
			}
			sql.update(namespace + "bertstckcnt", chartVo); // 기존창고 에서 불출수량+재고수량-
			int chk1 = (int) sql.selectOne(namespace + "bertchkstck", chartVo);
			if (chk1 >= 1) {
				sql.update(namespace + "bertstck", chartVo); // 옮긴 창고에서 입고수량+재고수량+
			} else {
				sql.insert(namespace + "bertaddstck", chartVo);
			}
		}
		return "ss";
	}

	@Override
	public String addDeliveryDate(String val) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List list = new ArrayList();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			ChartVo chartVo = new ChartVo();
			chartVo.setId(tempObj.get("id").toString());
			chartVo.setLotNo(tempObj.get("lotNo").toString());
			chartVo.setDeliveryNo(tempObj.get("deliverNo").toString());
			chartVo.setDeliveryDate(tempObj.get("deliveryDate").toString());
			int idx = (int) sql.selectOne(namespace + "deliverycheck", chartVo);
			idx = idx + 1;
			chartVo.setIndex(idx);
			chartVo.setBarcode(tempObj.get("lotNo").toString() + "_" + idx);
			sql.update(namespace + "addDeliveryDate", chartVo);

			Map map = new HashMap();
			map.put("id", tempObj.get("id").toString());
			map.put("barcode", URLEncoder.encode(chartVo.getBarcode(), "utf-8"));
			list.add(map);
		}

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);

		try {
			// 0
		} catch (Exception e) {
			// str = "fail";
		}
		return str;
	}

	@Override
	public String insertWaitTime(String val, String date) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");
		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);
			ChartVo chartVo = new ChartVo();
			chartVo.setEmpCd(tempObj.get("empCd").toString());
			chartVo.setDvcId(tempObj.get("dvcId").toString());
			chartVo.setWaitTime(Integer.parseInt(tempObj.get("time").toString()));
			chartVo.setSDate(tempObj.get("sDate").toString());
			chartVo.setEDate(tempObj.get("eDate").toString());
			chartVo.setChartStatus(tempObj.get("chartStatus").toString());
			chartVo.setWorkIdx(Integer.parseInt(tempObj.get("workIdx").toString()));
			list.add(chartVo);
		}
		try {
			sql.delete(namespace + "deleteWaitTime", date);
			if(list.size()==0) {
				ChartVo chartVo = new ChartVo();
				chartVo.setDvcId("0");
				chartVo.setEmpCd("0");
				chartVo.setWaitTime(0);
				chartVo.setSDate(date + " 01:00:00");
				chartVo.setEDate(date + " 01:00:00");
				chartVo.setWorkIdx(1);
				list.add(chartVo);
				sql.insert(namespace + "insertWaitTime", list);
			}else {
				sql.insert(namespace + "insertWaitTime", list);
			}
			str="success";
		}catch(Exception e) {
			e.printStackTrace();
			str="fail";
		}

		return str;
	}

	@Override
	public String getNonopHistory(String idx) throws Exception {
		SqlSession sql = getSqlSession();

		List<ChartVo> dataList = sql.selectList(namespace + "getNonopHistory", idx);
		int size = dataList.size();
		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < size; i++) {
			Map map = new HashMap();
			map.put("idx", dataList.get(i).getIdx());
			map.put("reason", dataList.get(i).getReason());
			map.put("startTime", dataList.get(i).getSDate());
			map.put("endTime", dataList.get(i).getEDate());
			list.add(map);
		}
		;
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
		
	}

	@Override
	public String approveAttendence(String id) throws Exception {
		String str="";
		SqlSession sql = getSqlSession();
		try {
			sql.update(namespace + "updateApprovement", id);
			str="success";
		}catch (Exception e) {
			str="fail";
			e.printStackTrace();
		}
		
		return str;
	}

	@Override
	public String takeAwayApprove(@PathVariable("id") String id, @PathVariable("msg") String msg) throws Exception {
		String str="";
		SqlSession sql = getSqlSession();
		ChartVo vo = new ChartVo();
		vo.setId(id);
		vo.setMsg(msg);
		try {
			sql.update(namespace + "takeAwayApprove", vo);
			str="success";
		}catch (Exception e) {
			str="fail";
			e.printStackTrace();
		}
		
		return str;
	}
	
	@Override
	public String cancelApprove(@PathVariable("id") String id) throws Exception {
		String str="";
		SqlSession sql = getSqlSession();
		ChartVo vo = new ChartVo();
		vo.setId(id);
		try {
			sql.update(namespace + "cancelApprove", vo);
			str="success";
		}catch (Exception e) {
			str="fail";
			e.printStackTrace();
		}
		
		return str;
	}

	@Override
	public void mobile_token_update(String token) throws Exception {
		SqlSession sql = getSqlSession();
		int exist = (Integer) sql.selectOne(namespace +"chkToken", token);
		if(exist == 0) {
			sql.insert(namespace + "mobile_token_update", token);
		}
	}

	@Override
	public void pushAlarmToMobile() throws Exception {
		SqlSession sql = getSqlSession();
		
		List<ChartVo> dataList = sql.selectList(namespace + "getToken");
		int size = dataList.size();
		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < size; i++) {
			pushToMobile(dataList.get(i).getToken());
		};
	}
	
	public void pushToMobile(String token) {
		JSONObject obj = new JSONObject();
		obj.put("to", token);
		
		//foreground
		JSONObject dataObj = new JSONObject();
		dataObj.put("message", "장비 알람");
		dataObj.put("title", "장비 알람");
		
		//background
		JSONObject notiObj = new JSONObject();
		notiObj.put("body", "장비 알람");
		notiObj.put("title", "장비 알람");
		notiObj.put("sound", "default");
		
		obj.put("data", dataObj);
		obj.put("notification", notiObj);
		
		try {
			URL u = new URL("https://fcm.googleapis.com/fcm/send");
			HttpURLConnection conn = (HttpURLConnection) u.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty( "Content-Type", "application/json" );
			conn.setRequestProperty( "Authorization", "key=AIzaSyAEbn7j_XkdCH8zqCn9p6uwxoEmRe1kJgc");
			OutputStreamWriter osw = new OutputStreamWriter(conn.getOutputStream());
			osw.write(obj.toString());
			osw.flush();
			
			BufferedReader br = null;
			br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
			
			String line = null;
			while((line = br.readLine()) != null) {
			}
			
			osw.close();
			br.close();
		}catch (Exception e){
			e.printStackTrace();
		}	
	}

	@Override
	public String getAlarmAction(String val) throws Exception {
		
		SqlSession sql = getSqlSession();

		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<ChartVo> insertList = new ArrayList<ChartVo>();
		List<ChartVo> updateList = new ArrayList<ChartVo>();
		String str = "";

		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);
			ChartVo chartVo = new ChartVo();
			chartVo.setDvcId(tempObj.get("dvcId").toString());
			chartVo.setStartTime(tempObj.get("startDateTime").toString());
			chartVo.setEndTime(tempObj.get("endDateTime").toString());
			chartVo.setNcAlarmNum1(tempObj.get("ncAlarmNum1").toString());
			chartVo.setCause(tempObj.get("cause").toString());
			chartVo.setResult(tempObj.get("result").toString());
			chartVo.setEmpCd(tempObj.get("empCd").toString());
			chartVo.setDate(tempObj.get("date").toString());
			
			int checkAlarm = (int) sql.selectOne(namespace + "checkAlarmAct", chartVo); // 등록할데이터확인

			if(checkAlarm==0){
				System.out.println("없음");
				insertList.add(chartVo);
			}else {
				System.out.println("있음");
				updateList.add(chartVo);
			}
			
			System.out.println("chk cnt ::"+checkAlarm);
		}

		try {
			if(insertList.size()!=0) {
				System.out.println("insertlist ::" + insertList.size());
				sql.insert(namespace + "addAlrAct", insertList);
			}
			if(updateList.size()!=0) {
				System.out.println("updatelist ::" + updateList.size());
				sql.update(namespace + "updAlrAct", updateList);
			}
//			sql.update(namespace + "chkupdateStock", list); // 입고 처리 완료시 chk=1로 변경후 화면에서 안보이게하기
			str = "success";
		} catch (Exception e) {
			e.printStackTrace();
			str = "fail";
		}	
		return str;
	}

	@Override
	public String getReCount(ChartVo chartVo) throws Exception {
		// TODO Auto-generated method stub
		SqlSession sql = getSqlSession();
//		sql.select(namespace + "getReCount", (ResultHandler) chartVo);
		System.out.println("sql 실행하자");
		sql.selectOne(namespace +"getReCount",chartVo);
		System.out.println("sql 실행됐다");
		return null;
	}

	@Override
	public String alarmExcept(String val) throws Exception {
		// TODO Auto-generated method stub
		SqlSession sql = getSqlSession();

		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<ChartVo> list = new ArrayList<ChartVo>();
		String str = "";

		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);
			ChartVo chartVo = new ChartVo();
			
			chartVo.setDvcId(tempObj.get("dvcId").toString());
			chartVo.setAlarm(tempObj.get("alarm").toString());
			
			list.add(chartVo);
		}

		try {
			
			sql.insert(namespace + "alarmExcept", list);
			str = "success";
		} catch (Exception e) {
			e.printStackTrace();
			str = "fail";
		}	

		return str;
	}
	@Override
	public String alarmreturn(ChartVo chartVo) throws Exception {
		// TODO Auto-generated method stub
		SqlSession sql = getSqlSession();
		String str = "";

		try {
			sql.delete(namespace + "alarmreturn", chartVo);
			str = "success";
		} catch (Exception e) {
			e.printStackTrace();
			str = "fail";
		}	
		
		return str;
	}

	@Override
	public String getOuterStock(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getOuterStock", chartVo);
		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "utf-8"));
			map.put("rcvCnt", dataList.get(i).getRcvCnt());
			map.put("iniohdCnt", dataList.get(i).getIniohdCnt());
			map.put("issCnt", dataList.get(i).getIssCnt());
			map.put("notiCnt", dataList.get(i).getNotiCnt());
			map.put("ohdCnt", dataList.get(i).getOhdCnt());
			map.put("cnt", dataList.get(i).getCnt());
			map.put("spec", dataList.get(i).getSpec());
			map.put("oprNm", URLEncoder.encode(dataList.get(i).getOprNm(),"utf-8"));
			map.put("regdate", dataList.get(i).getRegDate());
			map.put("lotNo", dataList.get(i).getLotNo());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}
	
	@Override
	public String MaintenanceSave(String val) throws Exception {

		String str = "";
		SqlSession sql = getSqlSession();

		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			ChartVo chartVo = new ChartVo();
			chartVo.setIdx(tempObj.get("idx").toString());
			chartVo.setCause(tempObj.get("cause").toString());
			chartVo.setResult(tempObj.get("result").toString());
			chartVo.setActionEmpCd(tempObj.get("empCd").toString());
			chartVo.setActionDate(tempObj.get("date").toString());

			list.add(chartVo);

		}
		
		
		try {
			System.out.println("삽입 ::"+list);
			sql.update(namespace + "MaintenanceSave", list);
			str = "success";
		} catch (Exception e) {
			e.printStackTrace();
			str = "fail";
		}

		return str;
	}
	
	@Override
	public String selectMenu() throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "selectMenu");

		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
				map.put("group", dataList.get(i).getGroup());
				map.put("value", dataList.get(i).getValue());
				map.put("name", URLEncoder.encode(dataList.get(i).getName(),"utf-8"));
				list.add(map);
		}

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}
	
	@Override
	public String getworkingList() throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getworkingList");
		
		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("group", dataList.get(i).getGroup());
			map.put("value", dataList.get(i).getValue());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(),"utf-8"));
			list.add(map);
		}
		
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}
	
	@Override
	public String getTransList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getTransList", chartVo);
		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "utf-8"));
			map.put("cnt", dataList.get(i).getOhdCnt());
			map.put("proj", dataList.get(i).getProj());
			map.put("date", dataList.get(i).getDate());
			
			list.add(map);
		}
		;
		
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}
	
	@Override
	public String getExportList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getExportList", chartVo);
		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "utf-8"));
			map.put("cnt", dataList.get(i).getOhdCnt());
			map.put("proj", dataList.get(i).getProj());
			map.put("date", dataList.get(i).getDate());
			map.put("ITEMNO", dataList.get(i).getITEMNO());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}
	
	@Override
	public String getImportList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getImportList", chartVo);
		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "utf-8"));
			map.put("item", URLEncoder.encode(dataList.get(i).getItem(), "utf-8"));
			map.put("vndNm", URLEncoder.encode(dataList.get(i).getVNDNM(), "utf-8"));
			map.put("idx", dataList.get(i).getIdx());
			map.put("vndNo", dataList.get(i).getVndNo());
			map.put("barcode", dataList.get(i).getBarcode());
			map.put("proj", dataList.get(i).getProj());
			map.put("cnt", dataList.get(i).getCnt());
			map.put("fault", dataList.get(i).getFaultCnt());
			map.put("stock", dataList.get(i).getStockCnt());
			map.put("date", dataList.get(i).getDate());
			map.put("deliveryNo", dataList.get(i).getDeliveryNo());
			map.put("remarks", URLEncoder.encode(dataList.get(i).getRemarks(), "utf-8"));
			map.put("afterProj", dataList.get(i).getAfterProj());
			map.put("ITEMNO", dataList.get(i).getITEMNO());
			list.add(map);
		}
		;
		
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}
	
	@Override
	public String getShipList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getShipList", chartVo);
		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "utf-8"));
			map.put("item", URLEncoder.encode(dataList.get(i).getItem(), "utf-8"));
//			map.put("idx", dataList.get(i).getIdx());
			map.put("shipLot", dataList.get(i).getShipLotNo());
			map.put("proj", dataList.get(i).getProj());
//			map.put("cnt", dataList.get(i).getCnt());
			map.put("stock", dataList.get(i).getStockCnt());
			map.put("date", dataList.get(i).getDate());
			
			list.add(map);
		}
		;
		
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}
	
	@Override
	public String getStockList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getStockList", chartVo);
		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "utf-8"));
			map.put("proj", URLEncoder.encode(dataList.get(i).getProj(), "utf-8"));
			map.put("idx", dataList.get(i).getIdx());
//			map.put("item", URLEncoder.encode(dataList.get(i).getItem(), "utf-8"));
//			map.put("idx", dataList.get(i).getIdx());
//			map.put("shipLot", dataList.get(i).getShipLotNo());
			map.put("iniohdCnt", dataList.get(i).getIniohdCnt());
			map.put("rcvCnt", dataList.get(i).getRcvCnt());
			map.put("issCnt", dataList.get(i).getIssCnt());
			map.put("cnt", dataList.get(i).getCnt());
//			map.put("stock", dataList.get(i).getStockCnt());
//			map.put("date", dataList.get(i).getDate());
			
			list.add(map);
		}
		;
		
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}
	
	@Override
	public String getItem(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getItem", chartVo);
		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("matNo", dataList.get(i).getMATNO());
			map.put("prce", dataList.get(i).getPRCE());
			map.put("prdNo", dataList.get(i).getRWMATNO());
			list.add(map);
		}
		;
		
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}
	
	@Override
	public String stockTotalCntCheck(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "stockTotalCntCheck", chartVo);
		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("cnt",Integer.parseInt(dataList.get(i).getCnt()));
			list.add(map);
		}
		;
		
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}
	
	@Override
	public String getInhistory(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getInhistory", chartVo);//입고
		List<ChartVo> dataListOut = sql.selectList(namespace + "getOuthistory", chartVo);//불출
		List<ChartVo> dataListOuter = sql.selectList(namespace + "getOuterhistory", chartVo);//반　출입
		List<ChartVo> dataListShip = sql.selectList(namespace + "getShiphistory", chartVo);//출하
		List<ChartVo> dataListMove = sql.selectList(namespace + "getMovehistory", chartVo);//재고이동
		List list = new ArrayList<ChartVo>();
		List listOut = new ArrayList<ChartVo>();
		List listOuter = new ArrayList<ChartVo>();
		List listShip = new ArrayList<ChartVo>();
		List listMove = new ArrayList<ChartVo>();
		//입고 현황
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("vndNm", URLEncoder.encode(dataList.get(i).getVNDNM(), "utf-8"));
			map.put("prdNo",dataList.get(i).getPrdNo());
			map.put("plan", URLEncoder.encode(dataList.get(i).getPlan(), "utf-8"));
			map.put("deliveryNo",dataList.get(i).getDeliveryNo());
			map.put("lotNo",dataList.get(i).getLotNo());
			map.put("barcode",dataList.get(i).getBarcode());
			map.put("cnt",Integer.parseInt(dataList.get(i).getCnt()));
			map.put("prce",Integer.parseInt(dataList.get(i).getPRCE()));
			map.put("date",dataList.get(i).getDate());
			list.add(map);
		};
		//불출 현황
		for (int i = 0; i < dataListOut.size(); i++) {
			Map map = new HashMap();
			map.put("prdNo",dataListOut.get(i).getPrdNo());
			map.put("plan", URLEncoder.encode(dataListOut.get(i).getPlan(), "utf-8"));
			map.put("barcode",dataListOut.get(i).getBarcode());
			map.put("lotNo",dataListOut.get(i).getLotNo());
			map.put("cnt",Integer.parseInt(dataListOut.get(i).getCnt()));
			map.put("prce",Integer.parseInt(dataListOut.get(i).getPRCE()));
			map.put("date",dataListOut.get(i).getDate());
			listOut.add(map);
		};
		//반　출입 현황
		for (int i = 0; i < dataListOuter.size(); i++) {
			Map map = new HashMap();
			map.put("vndNm", URLEncoder.encode(dataListOuter.get(i).getVNDNM(), "utf-8"));
			map.put("prdNo",dataListOuter.get(i).getPrdNo());
			map.put("deliveryNo", dataListOuter.get(i).getDeliveryNo());
			map.put("plan", URLEncoder.encode(dataListOuter.get(i).getPlan(), "utf-8"));
			map.put("proj",dataListOuter.get(i).getAfterProj());
			map.put("afterProj",dataListOuter.get(i).getAfterProj());
			map.put("cnt",Integer.parseInt(dataListOuter.get(i).getCnt()));
			map.put("date",dataListOuter.get(i).getDate());
			
			map.put("RWMATNO", dataListOuter.get(i).getRWMATNO());
			map.put("barcode", dataListOuter.get(i).getBarcode());
			map.put("ITEMNO", dataListOuter.get(i).getITEMNO());
			map.put("remarks", URLEncoder.encode(dataListOuter.get(i).getRemarks(), "utf-8"));
			listOuter.add(map);
		};
		//출하 현황
		for (int i = 0; i < dataListShip.size(); i++) {
			Map map = new HashMap();
			map.put("prdNo",dataListShip.get(i).getPrdNo());
			map.put("plan", URLEncoder.encode(dataListShip.get(i).getPlan(), "utf-8"));
			map.put("shipLot",dataListShip.get(i).getShipLotNo());
			map.put("cnt",Integer.parseInt(dataListShip.get(i).getCnt()));
			map.put("prce",Integer.parseInt(dataListShip.get(i).getPRCE()));
			map.put("date",dataListShip.get(i).getDate());
			listShip.add(map);
		};
		//재고이동 현황
		for (int i = 0; i < dataListMove.size(); i++) {
			Map map = new HashMap();
			map.put("prdNo",dataListMove.get(i).getPrdNo());
			map.put("plan", URLEncoder.encode(dataListMove.get(i).getPlan(), "utf-8"));
			map.put("shipLot",dataListMove.get(i).getShipLotNo());
			map.put("cnt",Integer.parseInt(dataListMove.get(i).getCnt()));
			map.put("prce",Integer.parseInt(dataListMove.get(i).getPRCE()));
			map.put("date",dataListMove.get(i).getDate());
			listMove.add(map);
		};
		
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);	// 입고현황
		dataMap.put("dataListOut", listOut);	// 입고현황
		dataMap.put("dataListOuter", listOuter);	// 입고현황
		dataMap.put("dataListShip", listShip);	// 입고현황
		dataMap.put("dataListMove", listMove);	// 입고현황
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}
	
	@Override
	public String getToolLifeList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();

		List<ChartVo> dataList = sql.selectList(namespace + "getToolLifeList", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("toolNm", dataList.get(i).getToolNm());
			map.put("spec", URLEncoder.encode(dataList.get(i).getSpec(), "utf-8"));
//			map.put("spec", dataList.get(i).getSpec());
			map.put("portNo", dataList.get(i).getPortNo());
			map.put("lmtC", Integer.parseInt(dataList.get(i).getPrdCntLmt()));
			map.put("cnt", Integer.parseInt(dataList.get(i).getCnt()));
			map.put("lmtT", Double.parseDouble(dataList.get(i).getRunTimeLimit()));
			map.put("time", Double.parseDouble(dataList.get(i).getRunTimeCurrent()));
			
/*			map.put("prdCntCrt", dataList.get(i).getPrdCntCrt());
			map.put("cycleRemain", dataList.get(i).getCycleRemain());
			map.put("cycleUsedRate", dataList.get(i).getCycleUsedRate());
			map.put("runTimeLimit", dataList.get(i).getRunTimeLimit());
			map.put("runTimeCurrent", dataList.get(i).getRunTimeCurrent());
			map.put("runTimeRemain", dataList.get(i).getRunTimeRemain());
			map.put("runTimeUsedRate", dataList.get(i).getRunTimeUsedRate());
			map.put("offsetDLimit", dataList.get(i).getOffsetDLimit());
			map.put("offsetDInit", dataList.get(i).getOffsetDInit());
			map.put("offsetDCurrent", dataList.get(i).getOffsetDCurrent());
			map.put("offsetHLimit", dataList.get(i).getOffsetHLimit());
			map.put("offsetHInit", dataList.get(i).getOffsetHInit());
			map.put("offsetHCurrent", dataList.get(i).getOffsetHCurrent());
			map.put("date", dataList.get(i).getDate());*/
			map.put("resetDt", dataList.get(i).getResetDt());
			
			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}
	@Override
	public String toolResetSave(ChartVo chartVo,String val) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";

		System.out.println(val);
		
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");
		
		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			ChartVo listVo = new ChartVo();
			listVo.setDate(tempObj.get("date").toString());
			listVo.setTime(tempObj.get("time").toString());
			listVo.setPrdNo(tempObj.get("prdNo").toString());
			listVo.setToolNm(tempObj.get("toolName").toString());
			listVo.setLmtC(Integer.parseInt(tempObj.get("lmtC").toString()));
			listVo.setToolCnt(Integer.parseInt(tempObj.get("ToolCnt").toString()));
			listVo.setCause(tempObj.get("cause").toString());
			listVo.setSpec(tempObj.get("spec").toString());
			listVo.setFound1(Float.parseFloat(tempObj.get("found1").toString()));
			listVo.setFound2(Float.parseFloat(tempObj.get("found2").toString()));
			listVo.setBeforeC(Float.parseFloat(tempObj.get("before").toString()));
			listVo.setAfterC(Float.parseFloat(tempObj.get("after").toString()));
			listVo.setWorkFault(Integer.parseInt(tempObj.get("workFault").toString()));
			listVo.setFault1(Integer.parseInt(tempObj.get("cnt1").toString()));
			listVo.setLeaderFault(Integer.parseInt(tempObj.get("leaderFault").toString()));
			listVo.setFault2(Integer.parseInt(tempObj.get("cnt2").toString()));
			listVo.setWorker(tempObj.get("worker").toString());
			listVo.setLeader(tempObj.get("leader").toString());
//			listVo.setName(tempObj.get("toolName").toString());
			
			list.add(listVo);
		}
		
		System.out.println("----list----");
		System.out.println(list);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", list);
		System.out.println(list);
		
		try {
			sql.insert(namespace + "tooladdlist", list);
			sql.update(namespace + "toolResetSave", chartVo);
			str = "success";
		} catch (Exception e) {
			e.printStackTrace();
			str = "fail";
		}

		return str;
	}
	
	@Override
	public String toolLimitSave(String val) throws Exception {
		SqlSession sql = getSqlSession();
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			ChartVo chartVo = new ChartVo();
//			chartVo.setPrgCd(tempObj.get("prgCd").toString());
//			chartVo.setRatio(tempObj.get("ratio").toString());
			chartVo.setDvcId(tempObj.get("dvcId").toString());
			chartVo.setPortNo(tempObj.get("portNo").toString());
			chartVo.setSpec(tempObj.get("spec").toString());
			chartVo.setCnt(tempObj.get("lmtC").toString());
			chartVo.setLimitRunTime(tempObj.get("lmtT").toString());
			
			list.add(chartVo);
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", list);

		String str = "";
		try {
			sql.update(namespace + "toolLimitSave", list);
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}

		return str;
	}
	
	@Override
	public String checkPrdctStandardSave(String val) throws Exception {
		SqlSession sql = getSqlSession();
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");
		
		List<ChartVo> list = new ArrayList<ChartVo>();
		List<ChartVo> updatelist = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);
			
			ChartVo chartVo = new ChartVo();
			chartVo.setId(tempObj.get("id").toString());
			chartVo.setPrdNo(tempObj.get("prdNo").toString());
			chartVo.setName(tempObj.get("name").toString());
			chartVo.setDvcId(tempObj.get("dvcId").toString());	// null
			chartVo.setChkTy(tempObj.get("chkTy").toString());
			chartVo.setAttrTy(tempObj.get("attrTy").toString());
			chartVo.setAttrCd(tempObj.get("attrCd").toString());
			chartVo.setAttrNameKo(tempObj.get("attrNameKo").toString());
			chartVo.setDp(tempObj.get("dp").toString());	//
			chartVo.setUnit(tempObj.get("unit").toString());
			chartVo.setSpec(tempObj.get("spec").toString());
			chartVo.setTarget(tempObj.get("target").toString());	//null
			chartVo.setLow(tempObj.get("low").toString());		//null
			chartVo.setUp(tempObj.get("up").toString());	// null
			chartVo.setMeasurer(tempObj.get("measurer").toString());
			chartVo.setJsGroupCd(tempObj.get("jsGroupCd").toString());
			chartVo.setAttrNameOthr(tempObj.get("attrNameOthr").toString());
			
/*			System.out.println( "테스트 attr:: " + tempObj.get("attrNameKo").toString());
			System.out.println( "encode attr:: " + URLEncoder.encode(tempObj.get("attrNameKo").toString()));
			System.out.println( "decode attr:: " + URLDecoder.decode(tempObj.get("attrNameKo").toString()));
			System.out.println("");*/
			if (chartVo.getId().equals("0")) {
				list.add(chartVo);
			} else {
				updatelist.add(chartVo);
//				sql.update(namespace + "updateCheckStandard", chartVo);
			}
//			list.add(chartVo);
		}
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", list);
		
		String str = "";
		try {
			if (list.size() > 0)
			sql.insert(namespace + "addChkStandard", list);
			//sql.update(namespace + "toolLimitSave", list);
			if (updatelist.size() >0)
			sql.update(namespace + "checkPrdctStandardSave", updatelist);

			str = "success";
		} catch (Exception e) {
			str = "fail";
		}
		
		return str;
	}
	
	@Override
	public String getSelectList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();

		List<ChartVo> dataList = sql.selectList(namespace + "getSelectList", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("id", dataList.get(i).getId());
			map.put("group", dataList.get(i).getGroup());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			
			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}

	@Override
	public String faultUpdate(String val) throws Exception {
		SqlSession sql = getSqlSession();

		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<ChartVo> list = new ArrayList<ChartVo>();	

		List<ChartVo> totallist = new ArrayList<ChartVo>();	//총재고 뺄것
		List<ChartVo> prolist = new ArrayList<ChartVo>();	//공정재고 뺄리스트
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			ChartVo chartVo = new ChartVo();
			chartVo.setId(tempObj.get("id").toString());
			chartVo.setChkTy(tempObj.get("chkTy").toString());
			chartVo.setPrdNo(tempObj.get("prdNo").toString());
			chartVo.setPrdPrc(tempObj.get("prdPrc").toString());
			chartVo.setDvcId(tempObj.get("dvcId").toString());
			chartVo.setDate(tempObj.get("date").toString());
			chartVo.setChecker(tempObj.get("checker").toString());
			chartVo.setPart(tempObj.get("part").toString());
			chartVo.setSituationTy(tempObj.get("situationTy").toString());
			chartVo.setSituation(tempObj.get("situation").toString());
			chartVo.setCause(tempObj.get("cause").toString());
			chartVo.setGchTy(tempObj.get("gchTY").toString());
			chartVo.setGch(tempObj.get("gch").toString());
			chartVo.setCnt(tempObj.get("cnt").toString());
			chartVo.setAction(tempObj.get("action").toString());
			chartVo.setProj(tempObj.get("proj").toString());
			list.add(chartVo);

		}

		System.out.println("list id ::" + list.get(0).getId() );
		System.out.println("list dvcId ::" + list.get(0).getDvcId() );
		System.out.println("list dvcId ::");
		String str = "";
		sql.update(namespace + "faultUpdate", list);


		try {
			// sql.delete(namespace + "delPrcDfct", list);
			// sql.insert(namespace + "addReleaseStock", list);

			str = "success";
		} catch (Exception e) {
			str = "fail";
		}

		return str;
	}
	
	@Override
	public String processCnt(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();

		System.out.println("들고가는것들;;;");
		System.out.println(chartVo.getVndNo());
		System.out.println(chartVo.getItem());
		
		List<ChartVo> dataList = sql.selectList(namespace + "processCnt", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("cnt", Integer.parseInt(dataList.get(i).getCnt()));
//			map.put("group", dataList.get(i).getGroup());
//			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			
			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}
	
	@Override
	public String getTattooList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getTattooList", chartVo);
		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("idx", dataList.get(i).getIdx());
			map.put("item", URLEncoder.encode(dataList.get(i).getItem(), "utf-8"));
			map.put("prdNo", URLEncoder.encode(dataList.get(i).getRWMATNO(), "utf-8"));
			map.put("shipLot", dataList.get(i).getShipLotNo());
			map.put("cnt", dataList.get(i).getCnt());
			map.put("stockCnt", dataList.get(i).getStockCnt());
			map.put("ITEMNO", dataList.get(i).getITEMNO());
			map.put("date", dataList.get(i).getDate());
			
			list.add(map);
		}
		;
		
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}
	
	@Override
	public String saveTattoo(String val) throws Exception {
		SqlSession sql = getSqlSession();
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<ChartVo> list = new ArrayList<ChartVo>();
		List<ChartVo> finishList = new ArrayList<ChartVo>();
		
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			ChartVo chartVo = new ChartVo();
//			chartVo.setPrgCd(tempObj.get("prgCd").toString());
//			chartVo.setRatio(tempObj.get("ratio").toString());
			chartVo.setItem(tempObj.get("item").toString());
			chartVo.setLotNo(tempObj.get("shipLot").toString());
			chartVo.setCnt(tempObj.get("cnt").toString());
//			chartVo.setCnt1(Integer.parseInt(tempObj.get("cnt").toString()));
			chartVo.setStockCnt(Integer.parseInt(tempObj.get("cnt").toString()));

			int chk=(int) sql.selectOne(namespace + "lastProjChk",chartVo);	//이상 외주가 있는지 확인 외주없으면 완성창고로 바로 등록
			if(chk<1) {
				finishList.add(chartVo);
			}
			chartVo.setCheckNum(chk);
			System.out.println("chk test ::" + chk);
			list.add(chartVo);
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", list);

		String str = "";
		try {
			if(finishList.size()>0) {	// 다음 외주가 없을 때
				sql.insert(namespace + "finishStock",finishList);		//완성창고 수량 +
				sql.insert(namespace + "totalStockMinus",finishList);	// 총재고 공정창고 수량 - 
				sql.insert(namespace + "processStockMinus",finishList);// 공정재고 마지막 공정 수량 - 
				sql.insert(namespace + "finishHistory",finishList);		// 완성창고 history 추가
					
					
			}

			sql.insert(namespace + "saveTattoo", list);				//타각 history
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}

		return str;
	}
	
	@Override
	public String getMachineInformation(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();

		List<ChartVo> dataList = sql.selectList(namespace + "getMachineInformation", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
//			map.put("dvcId", URLEncoder.encode(dataList.get(i).getLine(), "utf-8"));
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("tgCyl", dataList.get(i).getTgCyl());
			map.put("cntPerCyl", dataList.get(i).getCntPerCyl());
			map.put("cnt", dataList.get(i).getCnt());
			map.put("countCapa", dataList.get(i).getCountCapa());
			map.put("avgCyleTime", dataList.get(i).getAvgCyleTime());
			map.put("prdctPerHour", dataList.get(i).getPrdctPerHour());
			map.put("feedOverride", dataList.get(i).getFeedOverride());
			map.put("spdLoad", dataList.get(i).getSpdLoad());
			map.put("opRatio", dataList.get(i).getOpRatio());
			map.put("cuttingRatio", dataList.get(i).getCuttingRatio());

			list.add(map);
		}
		;

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);

		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);

		return str;
	}

	@Override
	public String dayFaulty(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();

		List<ChartVo> dataList = sql.selectList(namespace + "dayFaulty", chartVo);

		List<ChartVo> resultFaultyList = sql.selectList(namespace + "resultFaultyList", chartVo); //조치별
		List<ChartVo> causeFaultyList = sql.selectList(namespace + "causeFaultyList", chartVo); //원인별
		List<ChartVo> statusFaultyList = sql.selectList(namespace + "statusFaultyList", chartVo); //현상별
		
		
		List list = new ArrayList();
		List resultList = new ArrayList();
		List causeList = new ArrayList();
		List statusList = new ArrayList();
		
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
//			map.put("dvcId", URLEncoder.encode(dataList.get(i).getLine(), "utf-8"));
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("tgDate", dataList.get(i).getTgDate());
//			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("totalCnt", dataList.get(i).getTotalCnt()); //총 생산수량
			map.put("cnt", dataList.get(i).getCnt()); //총 합격수량
			map.put("fault", dataList.get(i).getFaultCnt()); //총 불량수량
			map.put("processFaultyCnt", dataList.get(i).getProcessFaultyCnt()); //업체 불량수량
			map.put("customerFaultyCnt", dataList.get(i).getCustomerFaultyCnt()); //작업자 불량수량
			map.put("countDvc", dataList.get(i).getCountDvc()); //소재 불량수량 
			map.put("count", dataList.get(i).getCount()); //가공 불량수량
			map.put("ratio", dataList.get(i).getRatio()); //불량율			
			System.out.println("dvcId : " + dataList.get(i).getTgDate() + ", fault : " + dataList.get(i).getFaultCnt() + ", countDvc: " + dataList.get(i).getCountDvc());
			list.add(map);
		};
		
		//조치별
		for (int i = 0; i < resultFaultyList.size(); i++) {
			Map map = new HashMap();
			map.put("category", URLEncoder.encode(resultFaultyList.get(i).getCategory(), "utf-8")); // 제품명
			map.put("value", Integer.parseInt(resultFaultyList.get(i).getValue())); //총 합격수량
			resultList.add(map);
		}

		//원인별
		for (int i = 0; i < causeFaultyList.size(); i++) {
			Map map = new HashMap();
			map.put("category", URLEncoder.encode(causeFaultyList.get(i).getCategory(), "utf-8")); // 제품명
			map.put("value", Integer.parseInt(causeFaultyList.get(i).getValue())); //총 합격수량
			causeList.add(map);
		}

		//현상별
		for (int i = 0; i < statusFaultyList.size(); i++) {
			Map map = new HashMap();
			map.put("category", URLEncoder.encode(statusFaultyList.get(i).getCategory(), "utf-8")); // 제품명
			map.put("value", Integer.parseInt(statusFaultyList.get(i).getValue())); //총 합격수량
			statusList.add(map);
		}
		
		

		Map dataMap = new HashMap();
		dataMap.put("dataList", list);// TODO Auto-generated method stub
		
		dataMap.put("causeFaultyList", causeList);//원인별
		dataMap.put("resultFaultyList", resultList);//조치별
		dataMap.put("statusFaultyList", statusList);//현상별
		
		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		
		return str;
	}

	@Override
	public String saveDvcMove(String val) throws Exception {
		String str = "";

		SqlSession sql = getSqlSession();
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");
		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);
			// (Integer.parseInt(tempObj.get("dvcId").toString()));

			ChartVo chartVo = new ChartVo();
			chartVo.setDvcId(tempObj.get("dvcId").toString());
			chartVo.setName(tempObj.get("name").toString());
			chartVo.setX(Integer.parseInt(tempObj.get("x").toString()));
			chartVo.setY(Integer.parseInt(tempObj.get("y").toString()));
			list.add(chartVo);
		}
		
		try {
			System.out.println("----list-----");
			System.out.println(list);
			
			sql.update(namespace + "saveDvcMove", list);
			//sql.update(namespace + "saveDvcMove", list);
			//sql.update(namespace + "updateCntPerCyl", list);
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}
		
		// TODO Auto-generated method stub
		return str;
	}

	@Override
	public String getInstrument(ChartVo chartVo) throws Exception {
		// TODO Auto-generated method stub
		
		SqlSession sql = getSqlSession();
		
		
		List<ChartVo> dataList = sql.selectList(namespace + "getInstrument", chartVo);
		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("idx", dataList.get(i).getIdx());
			map.put("code", dataList.get(i).getCode());
			map.put("division", URLEncoder.encode(dataList.get(i).getDivision(), "utf-8"));
			map.put("type", URLEncoder.encode(dataList.get(i).getType(), "utf-8"));
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("CodeName", URLEncoder.encode(dataList.get(i).getCodeName(), "utf-8"));
			map.put("standard", URLEncoder.encode(dataList.get(i).getStandard(), "utf-8"));
			map.put("interval", dataList.get(i).getInterval());
			map.put("lastChk", dataList.get(i).getLastChk());
			map.put("sDate", dataList.get(i).getSDate());
			map.put("RInterval", dataList.get(i).getRInterval());
			map.put("RlastChk", dataList.get(i).getRlastChk());
			map.put("purpose", URLEncoder.encode(dataList.get(i).getPurpose(), "utf-8"));
			map.put("useLine", URLEncoder.encode(dataList.get(i).getUseLine(), "utf-8"));
			map.put("VNDNM", URLEncoder.encode(dataList.get(i).getVNDNM(), "utf-8"));
			map.put("model", URLEncoder.encode(dataList.get(i).getModel(), "utf-8"));
			map.put("comName", URLEncoder.encode(dataList.get(i).getComName(), "utf-8"));
			map.put("date", dataList.get(i).getDate());
			map.put("empCd", URLEncoder.encode(dataList.get(i).getEmpCd(), "utf-8"));
			map.put("status", URLEncoder.encode(dataList.get(i).getStatus(), "utf-8"));
			map.put("department", URLEncoder.encode(dataList.get(i).getDepartment(), "utf-8"));
			map.put("img", dataList.get(i).getImg());
			
			list.add(map);
		}
		;
		
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String saveInstrument(ChartVo chartVo) throws Exception {
		// TODO Auto-generated method stub
		String str = "";
		int DupleChk = 0;
		
		SqlSession sql = getSqlSession();
		
		//관리코드 기본키 듀플 체크하기
		DupleChk = (int)sql.selectOne(namespace + "DupleInstrument", chartVo);
		
		System.out.println("중복체크 :" + DupleChk + " 신규 ? :" + chartVo.getChkAdd());
		if(chartVo.getChkAdd().equals("add") && DupleChk >= 1) {
			return "duple";
		}
		// 저장시키기
		sql.insert(namespace + "saveInstrument", chartVo);
		str = "success";
		return str;
	}

	@Override
	public String saveImgPath(ChartVo chartVo) throws Exception {
		// TODO Auto-generated method stub
		String str="";
		SqlSession sql = getSqlSession();
		sql.update(namespace + "saveImgPath",chartVo);
		str = "success";
		return str;
	}

	@Override
	public String detailInstrument(ChartVo chartVo) throws Exception {
		// TODO Auto-generated method stub
		
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "detailInstrument", chartVo);
		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("idx", dataList.get(i).getIdx());
			map.put("code", dataList.get(i).getCode());
			map.put("vndNm", URLEncoder.encode(dataList.get(i).getVNDNM(), "utf-8"));
			map.put("prc", dataList.get(i).getPrc());
			map.put("cSize", dataList.get(i).getCSize());
			map.put("passed", URLEncoder.encode(dataList.get(i).getPassed(), "utf-8"));
			map.put("bigo", URLEncoder.encode(dataList.get(i).getBigo(), "utf-8"));
			map.put("img", dataList.get(i).getImg());
			map.put("date", dataList.get(i).getDate());
			map.put("regdt", dataList.get(i).getREGDT());
			
			list.add(map);
		}
		;
		
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		
		return str;
	}

	@Override
	public String detailInstrumentSave(String val) throws Exception {
		String str = "";

		SqlSession sql = getSqlSession();
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("valIn");	// insert data
		JSONArray jsonArrayUp = (JSONArray) jsonObj.get("valUp");// update data

		System.out.println("--추가--");
		System.out.println(jsonArray);
		System.out.println("--변경--");
		System.out.println(jsonArrayUp);
		List<ChartVo> list = new ArrayList<ChartVo>();
		List<ChartVo> Uplist = new ArrayList<ChartVo>();
		//추가시키기
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);
			// (Integer.parseInt(tempObj.get("dvcId").toString()));

			ChartVo chartVo = new ChartVo();
			chartVo.setCode(tempObj.get("code").toString());
			chartVo.setVNDNM(tempObj.get("vndNm").toString());
			chartVo.setDate(tempObj.get("date").toString());
			chartVo.setPrc(tempObj.get("prc").toString());
			chartVo.setCSize(tempObj.get("cSize").toString());
			chartVo.setPassed(tempObj.get("passed").toString());
			chartVo.setBigo(tempObj.get("bigo").toString());
			chartVo.setImg(tempObj.get("img").toString());
			list.add(chartVo);
		}
		//수정시키기
			for (int i = 0; i < jsonArrayUp.size(); i++) {
				JSONObject tempObj = (JSONObject) jsonArrayUp.get(i);
				// (Integer.parseInt(tempObj.get("dvcId").toString()));

				ChartVo chartVo = new ChartVo();
				chartVo.setIdx(tempObj.get("idx").toString());
				chartVo.setCode(tempObj.get("code").toString());
				chartVo.setVNDNM(tempObj.get("vndNm").toString());
				chartVo.setDate(tempObj.get("date").toString());
				chartVo.setPrc(tempObj.get("prc").toString());
				chartVo.setCSize(tempObj.get("cSize").toString());
				chartVo.setPassed(tempObj.get("passed").toString());
				chartVo.setBigo(tempObj.get("bigo").toString());
				chartVo.setImg(tempObj.get("img").toString());
				Uplist.add(chartVo);
			}
		try {
			
			if(list.size()>=1) {
				sql.insert(namespace + "instrumentAdd", list);
			}
			if(Uplist.size()>=1) {
				sql.update(namespace + "instrumentUp", Uplist);
			}
			//sql.update(namespace + "saveDvcMove", list);
			//sql.update(namespace + "updateCntPerCyl", list);
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}
		
		// TODO Auto-generated method stub
		return str;
	}

	@Override
	public String getSelectItemList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		
		//해당 분류명 가져오기
		List<ChartVo> dataList = sql.selectList(namespace + "getSelectItemList", chartVo);
		List list = new ArrayList<ChartVo>();
		//선택 카테고리 가져오기
		List<ChartVo> dataList1 = sql.selectList(namespace + "getItemList", chartVo);
		List list1 = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();

			map.put("group", dataList.get(i).getGroup());
			map.put("cd", dataList.get(i).getCd());
			map.put("id", dataList.get(i).getId());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("nameAbb", URLEncoder.encode(dataList.get(i).getNameAbb(), "utf-8"));
			
			list.add(map);
		};
		
		for (int i = 0; i < dataList1.size(); i++) {
			Map map = new HashMap();

			map.put("group", dataList1.get(i).getGroup());
			map.put("cd", dataList1.get(i).getCd());
			map.put("id", dataList1.get(i).getId());
			map.put("name", URLEncoder.encode(dataList1.get(i).getName(), "utf-8"));
			map.put("nameAbb", URLEncoder.encode(dataList1.get(i).getNameAbb(), "utf-8"));
			
			list1.add(map);
		};
		
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		dataMap.put("dataList1", list1);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		
		return str;
	}

	@Override
	public String saveSelectItem(String val) throws Exception {
		SqlSession sql = getSqlSession();
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");
		
		List<ChartVo> list = new ArrayList<ChartVo>();

		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);
			
			ChartVo chartVo = new ChartVo();
			chartVo.setName(tempObj.get("name").toString());
			chartVo.setNameAbb(tempObj.get("nameAbb").toString());
			chartVo.setCd(tempObj.get("cd").toString());
			chartVo.setGroup(tempObj.get("group").toString());
			list.add(chartVo);
			System.out.println("group :" + chartVo.getGroup() + " , cd :" + chartVo.getCd() + " :::" + chartVo.getName());
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", list);

		String str = "";
		try {
			sql.insert(namespace + "saveSelectItem", list);
			str = "success";
		} catch (Exception e) {
			str = "fail";
		}
		
		// TODO Auto-generated method stub
		return str;
	}

	@Override
	public String getRecordItemList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		
		//모든 교정이력 가지고오기
		List<ChartVo> dataList = sql.selectList(namespace + "getRecordItemList", chartVo);
		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("idx", dataList.get(i).getIdx());
			map.put("code", dataList.get(i).getCode());
			map.put("vndNm", URLEncoder.encode(dataList.get(i).getVNDNM(), "utf-8"));
			map.put("prc", dataList.get(i).getPrc());
			map.put("cSize", dataList.get(i).getCSize());
			map.put("passed", URLEncoder.encode(dataList.get(i).getPassed(), "utf-8"));
			map.put("bigo", URLEncoder.encode(dataList.get(i).getBigo(), "utf-8"));
			map.put("img", dataList.get(i).getImg());
			map.put("date", dataList.get(i).getDate());
			map.put("regdt", dataList.get(i).getREGDT());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("type", URLEncoder.encode(dataList.get(i).getType(), "utf-8"));
			map.put("model", URLEncoder.encode(dataList.get(i).getModel(), "utf-8"));
			
			list.add(map);
		};
		
		
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		
		return str;
	}

	@Override
	public String deleteManagementItem(ChartVo chartVo) throws Exception {
		// TODO Auto-generated method stub
		String str="";
		SqlSession sql = getSqlSession();
		System.out.println("삭제할 데이터 :" + chartVo.getCode());
		
		//기록 남기기 위해 삭제할 데이터를 먼저 INSERT 시키기
		sql.insert(namespace + "deleteInsertInstrument",chartVo);	//관리대장 기록
		sql.insert(namespace + "deleteInsertInstrumentDetail",chartVo);	// 교정이력 기록
		//관리대장 삭제하기
		sql.delete(namespace + "deleteManagementItem",chartVo);
		//교정이력 모두 삭제하기
		sql.delete(namespace + "deleteRecordAll",chartVo);
		str = "success";
		return str;
	}

	@Override
	public String deleteRecordItem(ChartVo chartVo) throws Exception {
		// TODO Auto-generated method stub
		String str="";
		SqlSession sql = getSqlSession();
		System.out.println("삭제할 데이터 :" + chartVo.getIdx());
		
		//기록 남기기 위해 삭제할 데이터를 먼저 INSERT 시키기
		sql.insert(namespace + "deleteInsertRecordItem",chartVo);	// 교정이력 기록
		//선택항목 교정이력 삭제하기
		sql.delete(namespace + "deleteRecordItem",chartVo);
		
		str = "success";
		return str;
	}

	@Override
	public String saveImgPathDetail(ChartVo chartVo) throws Exception {
		// TODO Auto-generated method stub
		String str="";
		SqlSession sql = getSqlSession();
		sql.update(namespace + "saveImgPathDetail",chartVo);
		str = "success";
		return str;
	}

	@Override
	public String getInlist(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		
		List<ChartVo> dataList = sql.selectList(namespace + "getInlist", chartVo);
		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("vndNo", dataList.get(i).getVndNo());
			map.put("vndNm", URLEncoder.encode(dataList.get(i).getVNDNM(), "utf-8"));
			map.put("prdNo", dataList.get(i).getPrdNo());
			map.put("deliveryNo", dataList.get(i).getDeliveryNo());
			map.put("lotNo", dataList.get(i).getLotNo());
			map.put("cnt", dataList.get(i).getCnt());
			map.put("date", dataList.get(i).getDate());
			
			list.add(map);
		};
		
		System.out.println("================zz===========");
		System.out.println(dataList);
		
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		
		return str;
	}

	@Override
	public String getOutlist(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		
		List<ChartVo> dataList = sql.selectList(namespace + "getOutlist", chartVo);
		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("prdNo", dataList.get(i).getPrdNo());
			map.put("lotNo", dataList.get(i).getLotNo());
			map.put("cnt", dataList.get(i).getCnt());
			map.put("date", dataList.get(i).getDate());
			
			list.add(map);
		};
		
		
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		
		return str;
	}

	@Override
	public String getRelist(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		
		List<ChartVo> dataList = sql.selectList(namespace + "getRelist", chartVo);
		List list = new ArrayList<ChartVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("vndNm", URLEncoder.encode(dataList.get(i).getVNDNM(), "utf-8"));
			map.put("prdNo", dataList.get(i).getPrdNo());
			map.put("deliveryNo", dataList.get(i).getDeliveryNo());
			map.put("lotNo", dataList.get(i).getBarcode());
			map.put("cnt", dataList.get(i).getCnt());
			map.put("date", dataList.get(i).getDate());
			
			list.add(map);
		};
		
		
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		
		return str;
	}
}

