package com.unomic.dulink.svg.service;

import java.net.URLEncoder;
import java.security.spec.EncodedKeySpec;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.codehaus.jackson.map.ObjectMapper;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.unomic.dulink.svg.domain.SVGVo;


@Service
@Repository
public class SVGServiceImpl extends SqlSessionDaoSupport implements SVGService{
	private final static String namespace= "com.unomic.smarti.svg.";
	
	@Override
	public String getMachineInfo(SVGVo svgVo) throws Exception {
SqlSession sql = getSqlSession();
		
		List <SVGVo> machineList = sql.selectList(namespace + "getMachineInfo", svgVo);
		List list=new ArrayList();
		
		for(int i = 0; i < machineList.size(); i++){
			Map map = new HashMap();
			map.put("id", ((SVGVo) machineList.get(i)).getId()); 
			map.put("dvcId", ((SVGVo) machineList.get(i)).getDvcId());
			
			String name = "";
			if(machineList.get(i).getName()!=null){
				name = URLEncoder.encode(machineList.get(i).getName(), "UTF-8");
				name = name.replaceAll("\\+", "%20");
			}; 
			
			map.put("name", name);
			map.put("x", machineList.get(i).getX());
			map.put("ieX", machineList.get(i).getIeX());
			map.put("ieY", machineList.get(i).getIeY());
			map.put("notUse", machineList.get(i).getNotUse());
			map.put("y", machineList.get(i).getY());
			map.put("w", machineList.get(i).getW());
			map.put("h", machineList.get(i).getH());
			map.put("fontSize", machineList.get(i).getFontSize());
			map.put("pic", machineList.get(i).getPic());
			map.put("lastChartStatus", machineList.get(i).getLastChartStatus());
			map.put("operationTime", machineList.get(i).getOperationTime());
			map.put("width", machineList.get(i).getWidth());
			map.put("height", machineList.get(i).getHeight());
			map.put("viewBox", machineList.get(i).getViewBox());
			map.put("d", machineList.get(i).getD());
			map.put("transform", machineList.get(i).getTransform());
			map.put("isChg", machineList.get(i).getIsChg());
			map.put("isChg", machineList.get(i).getIsChg());
			map.put("chgTy", machineList.get(i).getChgTy());
			map.put("type", machineList.get(i).getType());
			
			map.put("red", machineList.get(i).getRed());
			map.put("redBlink", machineList.get(i).getRedBlink());
			map.put("green", machineList.get(i).getGreen());
			map.put("greenBlink", machineList.get(i).getGreenBlink());
			map.put("yellow", machineList.get(i).getYellow());
			map.put("yellowBlink", machineList.get(i).getYellowBlink());
			
			list.add(map);
		};
		 
		String str="";
		
		Map resultMap=new HashMap();
		resultMap.put("machineList", list);
		
		ObjectMapper om = new ObjectMapper();
		str=om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);
		
		return str;
	}

	@Override
	public void setMachinePos(SVGVo svgVo) throws Exception {
		SqlSession sql = getSqlSession();
		sql.update(namespace + "setMachinePos", svgVo);
	}

	@Override
	public SVGVo getMarker() throws Exception {
		SqlSession sql = getSqlSession();
		
		SVGVo svgVo = new SVGVo();
		svgVo = (SVGVo) sql.selectOne(namespace + "getMarker"); 
		return svgVo;
	}

	@Override
	public String getMachineInfo2(SVGVo svgVo) throws Exception {
		SqlSession sql = getSqlSession();
		
		List <SVGVo> machineList = sql.selectList(namespace + "getMachineInfo2", svgVo);
		List list=new ArrayList();
		
		for(int i = 0; i < machineList.size(); i++){
			Map map = new HashMap();
			map.put("id", ((SVGVo) machineList.get(i)).getId());
			map.put("dvcId", ((SVGVo) machineList.get(i)).getDvcId());
			
			String name = "";
			if(machineList.get(i).getName()!=null){
				name = URLEncoder.encode(machineList.get(i).getName(), "UTF-8");
				name = name.replaceAll("\\+", "%20");
			};
			
			map.put("name", name);
			map.put("x", machineList.get(i).getX());
			map.put("y", machineList.get(i).getY());
			map.put("w", machineList.get(i).getW());
			map.put("h", machineList.get(i).getH());
			map.put("pic", machineList.get(i).getPic());
			map.put("notUse", machineList.get(i).getNotUse());
			map.put("fontSize", machineList.get(i).getFontSize());
			map.put("lastChartStatus", machineList.get(i).getLastChartStatus());
			map.put("operationTime", machineList.get(i).getOperationTime());
			map.put("isChg", machineList.get(i).getIsChg());
			
			list.add(map);
		};
		
		String str="";
		
		Map resultMap=new HashMap();
		resultMap.put("machineList", list);
		
		ObjectMapper om = new ObjectMapper();
		str=om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);
		
		return str;
	}

	@Override
	public void setMachinePos2(SVGVo svgVo) throws Exception {
		SqlSession sql = getSqlSession();
		sql.update(namespace + "setMachinePos2", svgVo);
		
	}

	@Override
	public SVGVo getMarker2() throws Exception {
		SqlSession sql = getSqlSession();
		
		SVGVo svgVo = new SVGVo();
		svgVo = (SVGVo) sql.selectOne(namespace + "getMarker2"); 
		return svgVo;
	};
	
};

