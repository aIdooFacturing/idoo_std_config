package com.unomic.dulink.svg.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SVGVo{
	int id;
	String worker;
	String name;
	String opRatio;
	String chgTy;
	String type;
	String red;
	String redBlink;
	String green;
	String greenBlink;
	String yellow;
	String yellowBlink;
	int x;
	int y;
	int w;
	int h;
	float width;
	int nd;
	String workerN;
	String workerD;
	String workDate;
	String ieX;
	String ieY;
	int tgCyl;
	int cntCyl;
	float height;
	String viewBox;
	String d;
	String transform;
	String isChg;
	String pic;
	int notUse;
	String status;
	String lastChartStatus;
	double spd_load;
	double feed_override;
	String alarm;
	String endDateTime;
	String startDateTime;
	Integer shopId;
	int adt_id;
	int m_id;
	int dvcId;
	int adapter_id;
	String operationTime;
	int fontSize;
};
