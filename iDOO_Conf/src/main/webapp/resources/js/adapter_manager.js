$(function(){
	setEl();
	getAdtList();
});

var shopId = 1;
function getAdtList(){
	var url = ctxPath + "/chart/getAdtList.do";
	var param = "shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			var json = data.dataList;
			
			var tr = "<tr>" + 
						"<td class='table_title center-align'>Serial No.</td>" + 
						"<td class='table_title center-align'>Machine No.</td>" + 
						"<td class='table_title center-align'>Firm Ver.</td>" + 
						"<td class='table_title center-align'>IP</td>" + 
						"<td class='table_title center-align'>Mac Addr</td>" + 
						"<td class='table_title center-align'>Reboot</td>" + 
						"<td class='table_title center-align'>S/W Update</td>" + 
						"<td class='table_title center-align'>Firm Update</td>" + 
						"<td class='table_title center-align'>Reg Date</td>" + 
					"</tr>";
			
			$(json).each(function(idx, data){
				
			});
		}
	});
};

function setEl(){
	$("#search_table").css({
		"width" : getElSize(900)
	});
	
	$(".unos_table td").css({
		"padding" : getElSize(20),
		"font-size" : getElSize(40)
	});
	
	$(".unos_input").css({
		"font-size" : getElSize(50)
	});
	
	$("#adapter_list").css({
		"height" : getElSize(700)
	});
	
	$(".unos_select").css({
		"font-size": getElSize(40)
	});
	
	$(".unos_select > span").css({
		"border-bottom": getElSize(5) + "px solid #D70000"
	});
	
	$(".unos_btn").css({
		"height" : getElSize(60),
		"font-size" : getElSize(40)
	});
	
	$(".unos_btn").mousedown(function(){
		$(this).css({
			"opacity" : 0.7
		});
	});
	
	$(".unos_btn").mouseup(function(){
		$(this).css({
			"opacity" : 1
		});
	});
	
	$("#remark").attr({
		"cols" : getElSize(300),
		"rows" : getElSize(10)
	}).css("font-size", getElSize(40));
	
	$(".table_label").css({
		"width" : getElSize(500)
	})
};