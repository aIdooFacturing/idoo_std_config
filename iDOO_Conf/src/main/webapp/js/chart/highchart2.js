$(function() {
	drawPieChart("pieChart1");
	drawPieChart("pieChart2");
});

function drawPieChart(id) {
	// Build the chart
	$('#' + id)
			.highcharts(
					{
						chart : {
							plotBackgroundColor : null,
							plotBorderWidth : null,
							plotShadow : false,
							backgroundColor : 'rgba(255, 255, 255, 0)',
							type: 'pie',
				            options3d: {
				                enabled: true,
				                alpha: 45
				            }
						},
						credits : false,
						title : {
							text : false
						},
						tooltip : {
							pointFormat : '{series.name}: <b>{point.percentage:.1f}%</b>'
						},
						plotOptions : {
							pie : {
								 innerSize: 130,
					                depth: 45,
								 size:'100%',
								allowPointSelect : true,
								cursor : 'pointer',
								dataLabels : {
									enabled : false,
									format : '<b>{point.name}</b>: {point.percentage:.1f} %',
									style : {
										color : (Highcharts.theme && Highcharts.theme.contrastTextColor)
												|| 'black'
									}
								}
							}
						},
						exporting: false,
						series : [ {
							type : 'pie',
							name : 'Browser share',
							data : [ [ 'Firefox', 45.0 ], 
							         [ 'IE', 26.8 ],
							         [ 'Safari', 8.5 ],
							         [ 'Opera', 6.2 ],
									[ 'Others', 0.7 ] ]
						} ]
					});
}