var NS="http://www.w3.org/2000/svg";
var draw;
var background_img;
var marker;
//var imgPath = "../Device_Status/resources/upload/";
var imgPath = "../iDOO_STD_Setting/resources/upload/";

var borderColor = "";
var shopId = 1;
var totalOperationRatio = 0;

$(function() {

	$("#title_main").css({
		left : window.innerWidth/2 - ($("#title_main").width()/2)
	});
	
	draw = SVG("svg");
	
	var timeStamp = new Date().getMilliseconds();
	
	getMachineInfo();
});

function redrawPieChart(){
	var url = ctxPath + "/chart/getMachineInfo.do";
	var param = "shopId=" + shopId;
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function (data){
			var json = data.machineList;
			inCycleMachine = 0;
			waitMachine = 0;
			alarmMachine = 0;
			powerOffMachine = 0;
			
			$(json).each(function(key, data){
				
				if(data.lastChartStatus=="IN-CYCLE" || data.lastChartStatus=="CUT"){
					replaceAll(replaceAll(decodeURI(data.name), "%23","#"),"%2F","/")
					inCycleMachine++;
				}else if(data.lastChartStatus=="WAIT"){
					waitMachine++;
				}else if(data.lastChartStatus=="ALARM"){
					alarmMachine++;
				}else if(data.lastChartStatus=="NO-CONNECTION"){
					powerOffMachine++;
				};
				
				if(data.type=='MTC'){
					if(data.green==1){
						data.lampStatus="IN-CYCLE";
					}else if(data.greenBlink==1){
						data.lampStatus="IN-CYCLE-BLINK";
					}else if(data.red==1){
						data.lampStatus="ALARM-NOBLINK";
					}else if(data.redBlink==1){
						data.lampStatus="ALARM";
					}else if(data.yellow==1){
						data.lampStatus="WAIT-NOBLINK";
					}else if(data.yellowBlink==1){
						data.lampStatus="WAIT";
					}else{
						data.lampStatus="NO-CONNECTION";
					}
				}else{
					data.lampStatus=data.lastChartStatus
				}
				
			});
			
			if(waitMachine==0 && alarmMachine==0){
				borderColor = "green";
			}else if(alarmMachine==0){
				borderColor = "yellow";
			}else if(alarmMachine!=0){
				borderColor = "red";
			};
			
		}
	});
};

function printMachineName(arrayIdx, x, y, w, h, name, color, fontSize, dvcId){
	
	var wMargin = 0;
	var hMargin = 0;
	
	var name = "<span id='n" + dvcId + "' class='name'>" + name + "</span>";
	$("body").append(name)
	$("#n" +dvcId).css({
		"position" : "absolute",
		"color" : color,
		"fontSize" : fontSize,
		"z-index" : 9
	});
	
	$("#n" +dvcId).css({
//		"top" : $("#svg_td").offset().top + y + (h/2) - ($("#n" + dvcId).height()/2),
//		"left" : $("#svg_td").offset().left + x + (w/2) - ($("#n" + dvcId).width()/2),
		
		"top" : $("#svg_td").offset().top + y + (h/2) - ($("#n" + dvcId).height()/2) + getElSize(120),
		"left" : $("#svg_td").offset().left + x + (w/2) - ($("#n" + dvcId).width()/2) + getElSize(200),
	})
};

function nl2br(value) { 
	  return value.replace(/<br>/g, "\n");
};

var machineProp = new Array();
var machineList = new Array();
var machineName = new Array();
var machineStatus = new Array();
var newMachineStatus = new Array();
var machineArray2 = new Array();
var first = true;

function getMachineInfo(){
	var url = ctxPath + "/chart/getMachineInfo.do";
	var param = "shopId=" + shopId;
	console.log("getMachineInfo")
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function (data){
			console.log(data)
			$(".name").remove();
			$('#svg').loading('stop');
			var json = data.machineList;
			
			newMachineStatus = new Array();
			machineProp = new Array();
			machineStatus = new Array();
			inCycleMachine = 0;
			waitMachine = 0;
			alarmMachine = 0;
			powerOffMachine = 0;

			operationTime = 0;
			
			$(".wifi").remove()
			$(json).each(function(key, data){
				
				if(data.lastChartStatus=="IN-CYCLE" || data.lastChartStatus=="CUT"){
					replaceAll(replaceAll(decodeURI(data.name), "%23","#"),"%2F","/")
					inCycleMachine++;
				}else if(data.lastChartStatus=="WAIT"){
					waitMachine++;
				}else if(data.lastChartStatus=="ALARM"){
					alarmMachine++;
				}else if(data.lastChartStatus=="NO-CONNECTION"){
					powerOffMachine++;
				};
				
				if(data.type=='MTC'){
					if(data.green==1){
						data.lampStatus="IN-CYCLE";
					}else if(data.greenBlink==1){
						data.lampStatus="IN-CYCLE-BLINK";
					}else if(data.red==1){
						data.lampStatus="ALARM-NOBLINK";
					}else if(data.redBlink==1){
						data.lampStatus="ALARM";
					}else if(data.yellow==1){
						data.lampStatus="WAIT-NOBLINK";
					}else if(data.yellowBlink==1){
						data.lampStatus="WAIT";
					}else{
						data.lampStatus="NO-CONNECTION";
					}
				}else{
					data.lampStatus=data.lastChartStatus
				}
				
				var text_color;
				if(data.lampStatus=="WAIT" || data.lampStatus=="WAIT-NOBLINK" || data.lampStatus=="NO-CONNECTION" || data.lampStatus=="ALARM" || data.lampStatus=="ALARM-NOBLINK"){
					text_color = "#000000";
				}else{
					text_color = "#ffffff";
				};
				
				var array = new Array();
				var machineColor = new Array();
				
				array.push(data.id);
				array.push(replaceAll(replaceAll(decodeURI(data.name), "%23","#"),"%2F","/"));
				array.push(getElSize(data.x));
				array.push(getElSize(data.y));
				array.push(getElSize(data.w));
				array.push(getElSize(data.h));
				array.push(data.pic);
				array.push(data.lampStatus);
				array.push(data.dvcId);
				array.push(getElSize(data.fontSize));
				array.push(data.showing)
				
				
				
				machineStatus.push(array)
				operationTime += Number(data.operationTime);
				
				//idx, x, y, w, h, name
				printMachineName(null, getElSize(data.x), getElSize(data.y), getElSize(data.w), getElSize(data.h), replaceAll(replaceAll(decodeURI(data.name), "%23","#"),"%2F","/"), text_color, getElSize(data.fontSize), data.dvcId);
				
				var img_width = getElSize(30);
				if(String(data.type).indexOf("IO") == -1 && data.type != null){
					var wifi = document.createElement("div");
					wifi.setAttribute("class", "wifi");
					wifi.style.cssText = "position : absolute" + 
					"; border: 1px solid black "+
					"; width : " + getElSize(50) + "px" +
					"; height : " + getElSize(50) + "px" +
					"; border-radius :50%" + 
					"; top: " + ($("#n" + data.dvcId).offset().top - img_width) + "px" + 
					"; z-index : " + 9;
					
					if(data.dvcId==66 || data.dvcId==67 || data.dvcId==68){
						wifi.style.cssText +="; left : " + ($("#n" + data.dvcId).offset().left - getElSize(80)) + "px"; 
					}else if(data.dvcId==16){
						wifi.style.cssText +="; left : " + ($("#n" + data.dvcId).offset().left - getElSize(150)) + "px"; 
					}else if(data.dvcId==6 || data.dvcId==20 || data.dvcId==15){
						wifi.style.cssText +="; left : " + ($("#n" + data.dvcId).offset().left - getElSize(120)) + "px"; 
						wifi.style.cssText +="; left : " + ($("#n" + data.dvcId).offset().left - getElSize(120)) + "px"; 
					}else if(data.dvcId==65){
						wifi.style.cssText +="; left : " + ($("#n" + data.dvcId).offset().left - getElSize(60)) + "px" +
						"; top: " + ($("#n" + data.dvcId).offset().top - img_width - getElSize(70)) + "px" ; 
					}else if(data.dvcId==7){
						wifi.style.cssText +="; left : " + ($("#n" + data.dvcId).offset().left - getElSize(50)) + "px" +
						"; top: " + ($("#n" + data.dvcId).offset().top - img_width - getElSize(30)) + "px" ; 
					}else if(data.dvcId==1){
						wifi.style.cssText +="; left : " + ($("#n" + data.dvcId).offset().left - getElSize(30)) + "px" +
						"; top: " + ($("#n" + data.dvcId).offset().top - img_width - getElSize(30)) + "px" ; 
					}else if(data.dvcId==25){
						wifi.style.cssText +="; left : " + ($("#n" + data.dvcId).offset().left - getElSize(30)) + "px" +
						"; top: " + ($("#n" + data.dvcId).offset().top - img_width - getElSize(30)) + "px" ; 
					}else{
						wifi.style.cssText +="; left : " + ($("#n" + data.dvcId).offset().left - getElSize(15)) + "px" +
						"; top: " + ($("#n" + data.dvcId).offset().top - img_width - getElSize(70)) + "px" ; 
					}
					
					if(data.lastChartStatus=='IN-CYCLE' || data.lastChartStatus=='CUT'){
						wifi.style.cssText += "; background : green"
					}else if(data.lastChartStatus=='ALARM'){
						wifi.style.cssText += "; background : red"
					}else if(data.lastChartStatus=='WAIT'){
						wifi.style.cssText += "; background : yellow"
					}else{
						wifi.style.cssText += "; background : gray"
					}
					
					$("body").append(wifi)
					
				} 
				
				machineProp.push(array);
				machineColor.push(data.id);
				machineColor.push(data.lampStatus);
				newMachineStatus.push(machineColor);
				
				
			});
			
			if(!first){
				
				console.log("not First")
				reDrawMachine();
				redrawPieChart();
				
			};
			
			if(first){
				for(var i = 0; i < machineProp.length; i++){
					//array = id, name, x, y, w, h, pic, status, idx, dvcId, fontSize
					
					drawMachine(machineProp[i][0],
							machineProp[i][1],
							machineProp[i][2],
							machineProp[i][3],
							machineProp[i][4],
							machineProp[i][5],
							machineProp[i][6],
							machineProp[i][7],
							i,
							machineProp[i][8],
							machineProp[i][9],
							machineProp[i][10]);
				};
				first = false;
			};
			
			//cal totalOperationRatio
			totalOperationRatio = 0;
			for(var i = 0; i < machineArray2.length; i++){
				totalOperationRatio += machineArray2[i][20];
			};
			
			var totalMachine = 0;
			totalMachine += (inCycleMachine + waitMachine + alarmMachine);
			var totalOperationRatio = Number(inCycleMachine / totalMachine * 100).toFixed(1);
		}
	});
	
	//setTimeout(getMachineInfo, 3000);
};

function replaceAll(str, src, target){
	return str.split(src).join(target)
};

function reDrawMachine(){
	
	console.log(machineStatus.length)
	
	for(var i = 0; i < machineStatus.length; i++){
			machineList[i].remove();
			drawMachine(machineProp[i][0],
					machineProp[i][1],
					machineProp[i][2],
					machineProp[i][3],
					machineProp[i][4],
					machineProp[i][5],
					machineProp[i][6],
					newMachineStatus[i][1],
					i,
					machineProp[i][8],
					machineProp[i][9],
					machineProp[i][10]);
	};
	
	machineStatus = newMachineStatus;
};

function compare ( a, b ){
	var type = typeof a, i, j;
	 
	if( type == "object" ){
		if( a === null ){
			return a === b;
		}else if( Array.isArray(a) ){ //배열인 경우
			//기본필터
	      if( !Array.isArray(b) || a.length != b.length ) return false;
	 
	      //요소를 순회하면서 재귀적으로 검증한다.
	      for( i = 0, j = a.length ; i < j ; i++ ){
	        if(!compare(a[i], b[i]))return false;
	      	};
	      return true;
	    };
	  };
	 
	  return a === b;
};

var table_8 = true
function drawMachine(id, name, x, y, w, h, pic, status, i, dvcId, fontSize, showing){
	
	var svgFile;
	
	console.log("drawMachine")
	
	var timeStamp = new Date().getMilliseconds();
	if(status==null){
		svgFile = ".svg";
	}else{
		if(status=='CUT'){
			status='IN-CYCLE'
		}
		svgFile = "_" + status + ".svg";
	};
	
	machineList[i] = draw.image(imgPath + pic + svgFile + "?dummy=" + timeStamp).size(w, h);
	machineList[i].x(x);
	machineList[i].y(y);
	
	var text_color;
	if(status=="WAIT" || status=="NO-CONNECTION"){
		text_color = "#000000";
	}else{
		text_color = "#ffffff";
	};
	var before_x;
	var before_y;
	
	machineList[i].draggable().on('beforedrag', function(delta,evt){
		before_x = machineList[i].x();
		before_y = machineList[i].y();
	})
	
	
	machineList[i].draggable().on('dragend', function(delta,evt){
		var x = machineList[i].x();
		var y = machineList[i].y();
		
		
		if(setElSize(y)>1178 && setElSize(x)<728){
			if(showing==0){
				setMachinePos(id, setElSize(x), setElSize(y));
			}else{
				var r = confirm("이 장비를 미 표현 장비로 만드시겠습니까?")
				if(r==true){
					setMachinePos(id, 600, 1300);
					setHide(id)
				}else{
					setMachinePos(id, setElSize(before_x), setElSize(before_y));
				}
			}
		}else{
			if(showing==1){
				setMachinePos(id, setElSize(x), setElSize(y));
			}else{
				var r = confirm("이 장비를 표현 장비로 만드시겠습니까?")
				if(r==true){
					setShow(id)
					setMachinePos(id, setElSize(x), setElSize(y));
				}else{
					setMachinePos(id, setElSize(before_x), setElSize(before_y));
				}
			}
		}
	});
	
};

function setMachinePos(id, x, y){
	$('#svg').loading({
		theme: 'dark'
	});
	var url = ctxPath + "/setMachinePos.do";
	var param = "id=" + id + 
					"&x=" + x + 
					"&y=" + y;
	$.ajax({
		url : url,
		data : param,
		type : "post",
		success : function(data){
			getMachineInfo();
		}
	});
};

function setHide(id){
	$("#svg").loading({
		theme: 'dark'
	})
	
	var url = ctxPath + "/setHide.do";
	var param = "id=" + id 
	$.ajax({
		url : url,
		data : param,
		type : "post",
		success : function(data){
			getMachineInfo();
		}
	});
}

function setShow(id){
	$("#svg").loading({
		theme: 'dark'
	})
	
	var url = ctxPath + "/setShow.do";
	var param = "id=" + id 
	$.ajax({
		url : url,
		data : param,
		type : "post",
		success : function(data){
			getMachineInfo();
		}
	});
}


