const createMachine = () =>{
	const url = ctxPath + "/svg/getMachineInfo.do";
	const date = new Date();
	
	const year = date.getFullYear();
	const month = addZero(String(date.getMonth()+1));
	const day = addZero(String(date.getDate()));
	const today = year + "-" + month + "-" + day;
	
	const param = "shopId=" + shopId + 
				"&startDateTime=" + today;
	
	console.log(param)
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : (data) => {
			console.log(data)
			let boxes = "";
			
			
			
			for (var json of data.machineList){
				let bgColor = "";
				if(json.lastChartStatus == "IN-CYCLE"){
					bgColor = incycleColor;
					
				}else if(json.lastChartStatus == "WAIT"){
					bgColor = waitColor;
					
				}else if(json.lastChartStatus == "ALARM"){
					bgColor = alarmColor;
					
				}else if(json.lastChartStatus == "NO-CONNECTION" && json.notUse != 1) {
					bgColor = noConnColor;
					
				}
				
				const name = decodeURIComponent(json.name)
				
				if(json.id != 97){
					boxes += 
						`
							<div class="machine"
								dvcId="${json.id}"
								dvcName="${json.name}"
								status="${json.lastChartStatus}"
								style= 
								"	position : absolute
									; z-index : 2
									; background-color : ${bgColor}
									; width : ${getElSize(json.w)}px
									; height : ${getElSize(json.h)}px
									; border-radius : ${getElSize(4 * 2)}px
									; left : ${getElSize(json.x)}px
									; top : ${getElSize(json.y)}px
									; font-size : ${getElSize(json.fontSize)}px
									; text-align : center
									; cursor : pointer

									; color : black
								"
							>${name}</div>
							
						`	
				}
				
				//console.log(json)
			}
			
			
			
			$("#svg").append(boxes)
			
			$(".machine").draggable({
				drag : (evt)=>{
					const id = $(evt.target).attr("dvcid")
					const x = $(evt.target).offset().left;
					const y = $(evt.target).offset().top;
					
				
					setMachinePos(id, x, y)
				}
			})
			
		}, error : (e1,e2,e3) =>{
			console.log(e1,e2,e3)
		}
	});
	
	//drawGroupDiv();
};

const drawGroupDiv = () =>{
	//LFA RR
	var table = "<table class='gr_table' id='lfa_rr'>" + 
					"<tr>" + 
						"<Td class='label'>MC ED / FFM<br>VF LS</td>" +
					"</tr>" + 
					"<tr>" + 
						"<Td class='icon'></td>" +
					"</tr>" +
				"</table>";

	$("#container").append(table);
			
	$("#lfa_rr").css({
		"left" : getElSize(265) + marginWidth,
		"top" : getElSize(380 - 20) + marginHeight,	
	});
	
	$("#lfa_rr").css({
		"width" : getElSize(190)
	});
	
	//YP FRT (cnc)
	var table = "<table class='gr_table' id='yp_frt'>" + 
					"<tr>" + 
						//"<Td class='label'>YP FRT (CNC)<br>내수, 북미</td>" +
						"<Td class='label'>EN ACF<br>AC WLV 14 / 11</td>" +
					"</tr>" + 
					"<tr>" + 
						"<Td class='icon'></td>" + 
					"</tr>" +
				"</table>";

	$("#container").append(table);
			
	$("#yp_frt").css({
		"left" : getElSize(730 - 235) + marginWidth,
		"top" : getElSize(380 - 20) + marginHeight,	
	});
	
	$("#yp_frt td").css({
		"width" : getElSize(250)
	});

	//UM FRT (CNC)
	var table = "<table class='gr_table' id='um_frt'>" + 
					"<tr>" + 
						"<Td class='label'>CC RKF 16 / QV BG /<br>EF LL 16</td>" + 
					"</tr>" + 
					"<tr>" + 
						"<Td class='icon'></td>" + 
					"</tr>" +
				"</table>";

	$("#container").append(table);
			
	$("#um_frt").css({
		"left" : getElSize(1030 - 235) + marginWidth,
		"top" : getElSize(380 - 20) + marginHeight,	
	});
	
	$("#um_frt td").css({
		"width" : getElSize(400)
	});
	
	//TA RR
	var table = "<table class='gr_table' id='ta_Rr'>" + 
					"<tr>" + 
						"<Td class='label'>QMF VM /<br>AA WOL</td>" + 
					"</tr>" + 
					"<tr>" + 
						"<Td class='icon'></td>" + 
					"</tr>" +
				"</table>";

	$("#container").append(table);
			
	$("#ta_Rr").css({
		"left" : getElSize(1490 - 235) + marginWidth,
		"top" : getElSize(380 - 20) + marginHeight,	
	});
	
	$("#ta_Rr td").css({
		"width" : getElSize(250)
	});
	
	//TA FRT
	var table = "<table class='gr_table' id='ta_frt'>" + 
					"<tr>" + 
						"<Td class='label'>SM QKF /<br>VL NQO</td>" + 
					"</tr>" + 
					"<tr>" + 
						"<Td class='icon'></td>" + 
					"</tr>" +
				"</table>";

	$("#container").append(table);
			
	$("#ta_frt").css({
		"left" : getElSize(1800 - 235) + marginWidth,
		"top" : getElSize(380 - 20) + marginHeight,	
	});
	
	$("#ta_frt td").css({
		"width" : getElSize(250)
	});
	
	
	//JC RR
	var table = "<table class='gr_table' id='jc_rr'>" + 
					"<tr>" + 
						"<Td class='label'>LQ LLX /<br>MF RKG</td>" + 
					"</tr>" + 
					"<tr>" + 
						"<Td class='icon'></td>" + 
					"</tr>" +
				"</table>";

	$("#container").append(table);
			
	$("#jc_rr").css({
		"left" : getElSize(2100 - 235) + marginWidth,
		"top" : getElSize(380 - 20) + marginHeight,	
	});
	
	$("#jc_rr td").css({
		"width" : getElSize(250)
	});
	
	//
	var table = "<table class='gr_table' id='jc_rr2'>" + 
					"<tr>" + 
						"<Td class='label'>VL QQ 자동</td>" + 
					"</tr>" + 
					"<tr>" + 
						"<Td class='icon'></td>" + 
					"</tr>" +
				"</table>";

	$("#container").append(table);
			
	$("#jc_rr2").css({
		"left" : getElSize(2400 - 235) + marginWidth,
		"top" : getElSize(380 - 20) + marginHeight,	
	});
	
	$("#jc_rr2 td").css({
		"width" : getElSize(250)
	});
	
	
	//HR PU/FRT
	var table = "<table class='gr_table' id='hr_pu'>" + 
					"<tr>" + 
						"<Td class='label'>CM ED</td>" + 
					"</tr>" + 
					"<tr>" + 
						"<Td class='icon'></td>" + 
					"</tr>" +
				"</table>";

	$("#container").append(table);
			
	$("#hr_pu").css({
		"left" : getElSize(2700 - 235) + marginWidth,
		"top" : getElSize(380 - 20) + marginHeight,	
	});
	
	$("#hr_pu td").css({
		"width" : getElSize(300)
	});
	
	
	//HR PU/FRT
	var table = "<table class='gr_table' id='hr_pu2'>" + 
					"<tr>" + 
						"<Td class='label'>QE KRM 13</td>" + 
					"</tr>" + 
					"<tr>" + 
						"<Td class='icon'></td>" + 
					"</tr>" +
				"</table>";

	$("#container").append(table);
			
	$("#hr_pu2").css({
		"left" : getElSize(3040 - 235) + marginWidth,
		"top" : getElSize(380 - 20) + marginHeight,	
	});
	
	$("#hr_pu2 td").css({
		"width" : getElSize(250)
	});
	
	//HR PU/FRT
	var table = "<table class='gr_table' id='hr_pu3'>" + 
					"<tr>" + 
						"<Td class='label'>MS EKF</td>" + 
					"</tr>" + 
					"<tr>" + 
						"<Td class='icon'></td>" + 
					"</tr>" +
				"</table>";

	$("#container").append(table);
			
	$("#hr_pu3").css({
		"left" : getElSize(3340 - 235) + marginWidth,
		"top" : getElSize(380 - 20) + marginHeight,	
	});
	
	$("#hr_pu3 td").css({
		"width" : getElSize(250)
	});
	
	
	//TQ FRT
	var table = "<table class='gr_table' id='tq_frt'>" + 
					"<tr>" + 
						"<Td class='label'>EV VFV /<br>RF BM</td>" + 
					"</tr>" + 
					"<tr>" + 
						"<Td class='icon'></td>" + 
					"</tr>" +
				"</table>";

	$("#container").append(table);
			
	$("#tq_frt").css({
		"left" : getElSize(3640 - 235) + marginWidth,
		"top" : getElSize(380 - 20) + marginHeight,	
	});
	
	$("#tq_frt td").css({
		"width" : getElSize(190)
	});
	
	
	
	
	
	
	var table = "<table class='gr_table' id='yp_rr'>" + 
						"<tr>" + 
							"<Td class='label'>RLF 26 / VMV 55</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
				
				$("#container").append(table);
				
				$("#yp_rr").css({
					"left" : getElSize(2330  - 235) + marginWidth,
					"top" : getElSize(1180 - 10) + marginHeight,	
				});
				
				$("#yp_rr td").css({
					"width" : getElSize(470)
				});

	var table = "<table class='gr_table' id='yp_rr2'>" + 
				"<tr>" + 
					"<Td class='label'>RM SK LV/<br>RM CD RF</td>" + 
				"</tr>" + 
				"<tr>" + 
					"<Td class='icon'></td>" + 
				"</tr>" +
			"</table>";
		
		$("#container").append(table);
		
		$("#yp_rr2").css({
			"left" : getElSize(2850 - 235) + marginWidth,
			"top" : getElSize(1180 - 10) + marginHeight,	
		});
		
		$("#yp_rr2 td").css({
			"width" : getElSize(150)
		});
		
		
		var table = "<table class='gr_table' id='yp_rr3'>" + 
				"<tr>" + 
					"<Td class='label'>EMF LV</td>" + 
				"</tr>" + 
				"<tr>" + 
					"<Td class='icon'></td>" + 
				"</tr>" +
			"</table>";
		
		$("#container").append(table);
		
		$("#yp_rr3").css({
			"left" : getElSize(3040 - 235) + marginWidth,
			"top" : getElSize(1180 - 10) + marginHeight,	
		});
		
		$("#yp_rr3 td").css({
			"width" : getElSize(250)
		});
	
		
		var table = "<table class='gr_table' id='yp_rr4'>" + 
				"<tr>" + 
					"<Td class='label'>CM QD / LV LCQ</td>" + 
				"</tr>" + 
				"<tr>" + 
					"<Td class='icon'></td>" + 
				"</tr>" +
			"</table>";
		
		$("#container").append(table);
		
		$("#yp_rr4").css({
			"left" : getElSize(3340 - 235) + marginWidth,
			"top" : getElSize(1180 - 10) + marginHeight,	
		});
		
		$("#yp_rr4 td").css({
			"width" : getElSize(250)
		});
		
		
		//UM FRT
		var table = "<table class='gr_table' id='yp_rr5'>" + 
		"<tr>" + 
			"<Td class='label'>FM QEK /<br>CV OKL</td>" + 
		"</tr>" + 
		"<tr>" + 
			"<Td class='icon'></td>" + 
		"</tr>" +
		"</table>";
	
		$("#container").append(table);
		
		$("#yp_rr5").css({
			"left" : getElSize(3640 - 235) + marginWidth,
			"top" : getElSize(1180 - 10) + marginHeight,	
		});
		
		$("#yp_rr5 td").css({
			"width" : getElSize(190)
		});
		
	
	var table = "<table class='gr_table' id='tmp'>" + 
			"<tr>" + 
				"<Td class='label'>SAMPLE</td>" + 
			"</tr>" + 
			"<tr>" + 
				"<Td class='icon'></td>" +
			"</tr>" +
		"</table>";
	
	$("#container").append(table);
	
	$("#tmp").css({
		"left" : getElSize(500 - 235) + marginWidth,
		"top" : getElSize(1180 - 10) + marginHeight,	
	});
	
	

	
	$(".gr_table").css({
		"position" : "absolute",
		"border-spacing" : "0px",
		//"border-collapse" : "collapse",
		"z-index" : 1
	});
	
	
	
	
	$(".gr_table .label").css({
		"background-color" : "#373737",
		"text-align" : "center",
		"height" : getElSize(70),
		//"border" : getElSize(3) + "px solid #535556",
		"border-top-left-radius" : getElSize(4 * 2) + "px",
		"border-top-right-radius" : getElSize(4 * 2) + "px",
	});
	
	$(".gr_table .icon").css({
		//"height" : getElSize(530),
		"height" : getElSize(730),
		"border-left" : getElSize(3) + "px solid #535556",
		"border-right" : getElSize(3) + "px solid #535556",
		"border-bottom" : getElSize(3) + "px solid #535556",
		"border-bottom-left-radius" : getElSize(4 * 2) + "px",
		"border-bottom-right-radius" : getElSize(4 * 2) + "px",

	});
	
	$("#tmp .icon").css({
		"width" : getElSize(190),
		"height" : getElSize(630)
	});
	
	$("#lfa_rr .icon,#yp_frt .icon,  #ta_frt .icon, #jc_rr .icon, #hr_pu .icon").css({
		"height" : getElSize(730),

	});
	
	$("#tq_frt .icon").css({
		"height" : getElSize(1700),

	});
	
	$("#yp_rr, #yp_rr2, #yp_rr3, #yp_rr4, #yp_rr5").css({
		"height" : getElSize(980),
	})
	
	$("#fs_rr .icon").css({
		"height" : getElSize(790),

	});
	
	$(".gr_table td").css({
		"color" : "white",
		"font-size" : getElSize(30),
	});
	
	
};

const setMachinePos = (id, x, y) =>{
	const url = ctxPath + "/svg/setMachinePos.do";
	const param = "dvcId=" + id
				+ "&x=" + (setElSize(x - $("#svg").offset().left))
				+ "&y=" + (setElSize(y - $("#svg").offset().top))
	
			console.log(param)
	$.ajax({
		url : url,
		data : param,
		type : "get",
		success : ()=>{
			
		}
	})
}