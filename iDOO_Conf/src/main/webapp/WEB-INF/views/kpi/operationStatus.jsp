<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";	
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden; 
	background-color: black;
  	font-family:'Helvetica';
}
html{
	overflow : hidden;
}
.k-grid tbody > tr
{
 background : #363A45 !important;
 text-align: center;
}

.k-grid tbody > tr:hover
{
 background :  darkgray !important;
}
#detailGrid{
	background : #2C3031;
}
.k-grid tbody > .k-alt
{
 background :  #2C3031	 !important;
 text-align: center;
}
.k-grid tbody > .k-alt:hover
{
 background :  darkgray !important;
 text-align: center;
}
.k-grid td{
	color:white;
	font-weight: bolder;
}
.k-grid th.k-header,
.k-grid-header
{
    background: linear-gradient( to bottom, #121212, #2A2926 ) !important;
    color : white !important;
    text-align: center;
}
input#dailyRadio{
	 background: linear-gradient( to bottom, #007FFE, #004080 );
}
input#dailyRadio:hover{
	background : #004080;
}

input#monthlyRadio{
	 background: linear-gradient( to bottom, #007FFE, #004080 );
}
input#monthlyRadio:hover{
	background : #004080;
}
input#rangeRadio{
	 background: linear-gradient( to bottom, #007FFE, #004080 );
}
input#rangeRadio:hover{
	background : #004080;
}
button#search{
	background : linear-gradient(lightgray, gray);
}
button#search:hover{
	background : gray;
}
input#statusId{
	background : linear-gradient(lightgray, gray);
}
input#statusId:hover{
	background : gray;
}
</style> 
<script type="text/javascript">
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	var handle = 0;
	
	$(function(){
		$( document ).ajaxStart(function() {
		    //마우스 커서를 로딩 중 커서로 변경
			$.showLoading();
		});
		//AJAX 통신 종료
		$( document ).ajaxStop(function() {
		    //마우스 커서를 원래대로 돌린다
			$.hideLoading();
		});

		createNav("kpi_nav", 5);
		getOpdataTime();
		setEl();
		time();
		
		daily(getToday().substring(0,10));
		setShift();

		
		getoperationStatus();
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	function setShift(){
		
		var day = moment(new Date(moment().year(), moment().month(), moment().date(), 8, 30, 0));
    	var night = moment(new Date(moment().year(), moment().month(), moment().date(), 20, 30, 0));
    	if(moment().isAfter(day)){
    		if(moment().isAfter(night)){
    			console.log("현재 오후 야간근무 시간, 직전 shift : 주간")
    			$("#workIdx").val(2)
    			$("input#sDate2").val(moment().format("YYYY-MM-DD"))
    			$("input#sDate").val(moment().format("YYYY-MM-DD"))
    			$("input#eDate").val(moment().format("YYYY-MM-DD"))
    		}else{
    			console.log("현재 주간근무 시간, 직전 shift : 전날 야간")//
    			$("#workIdx").val(1)
    			$("input#sDate2").val(moment().subtract(1, 'day').format("YYYY-MM-DD"))
    			$("input#sDate").val(moment().subtract(1, 'day').format("YYYY-MM-DD"))
    		 	$("input#eDate").val(moment().subtract(1, 'day').format("YYYY-MM-DD"));
    			
    			var aa = new Date();
    			var dayLabel = aa.getDay();

    			if(dayLabel==1){
    				console.log('월요일')
    				$("input#sDate2").val(moment().subtract(3, 'day').format("YYYY-MM-DD"))
        			$("input#sDate").val(moment().subtract(3, 'day').format("YYYY-MM-DD"))
        		 	$("input#eDate").val(moment().subtract(3, 'day').format("YYYY-MM-DD"));
    			}
    		}
    	}else{
    		console.log("현재 오전 야간근무 시간 , 직전 shift : 주간")//
    		$("#sDate2").val(moment().subtract('day', 1).format("YYYY-MM-DD"))
    		$("#sDate").val(moment().subtract('day', 1).format("YYYY-MM-DD"))
    		$("input#eDate").val(moment().subtract(1, 'day').format("YYYY-MM-DD"));
    		$("#workIdx").val(2)
    		
    		var aa = new Date();
			var dayLabel = aa.getDay();
			if(dayLabel==1){
				$("#workIdx").val(1)
				$("input#sDate2").val(moment().subtract(3, 'day').format("YYYY-MM-DD"))
    			$("input#sDate").val(moment().subtract(3, 'day').format("YYYY-MM-DD"))
    		 	$("input#eDate").val(moment().subtract(3, 'day').format("YYYY-MM-DD"));
				console.log('월요일')
			}
    	}
		
		
	}
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : contentWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#banner").css({
			"font-size" : getElSize(80),
			"width" : getElSize(2000)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		$("#dailyRadio, #monthlyRadio, #rangeRadio").css({
			"border" : getElSize(6) +"px solid white",
			"display" : "-webkit-inline-box"
		})
		
		$("input#sDate, input#sMonth, input#sDate2, input#eDate").css({
			"font-size" : getElSize(60),
			"position": "relative",
	    	"top": -getElSize(8),
	    	"width" : getElSize(350)
		})
		
		$("select#workIdx").css({
			"font-size": getElSize(63),
		    "margin-left": -getElSize(27),
		    "position": "relative",
		    "margin-right": getElSize(20),
		    "top": getElSize(5)
		})
		
		$("input#dailyRadio, input#monthlyRadio, input#rangeRadio").css({
			"font-size" : getElSize(80),
			"margin-left" : -getElSize(1),
		    "border": "0px",
		    "color": "white",
		    "font-weight": "bolder",
		    "padding-right": getElSize(13),
		    "padding-bottom": getElSize(8)
		})
		
		$("#divBtn").css({
			"float": "right",
	    	"display": "-webkit-inline-box"
		})
		
		$("button#search").css({
		    "font-size": getElSize(80),
		    "width": getElSize(181),
		    "border": "0px",
		    "border-radius": getElSize(18),
		    "position": "relative",
		    "top": getElSize(5)
		})
		
		$("input#statusId").css({
			"font-size": getElSize(80),
		    "border-radius": getElSize(18),
		    "border": "0px",
		    "position": "relative",
		    "top": getElSize(5),
		    "padding": "0px "+getElSize(8)+"px "+getElSize(8)+"px "+getElSize(8)+"px"
		})
		
	};

	var dataTime="";
	function getOpdataTime(){
		console.log('dd')
		var url = "${ctxPath}/kpi/getOpdataTime.do";
		$.ajax({
			url : url,
			type : "post",
			success : function(data){
				dataTime=data
			}
		});
	}
	
	function time(){
		$("#time").html("데이터수신 시간: " + dataTime + " 현재 시간: " + getToday());
		 handle = requestAnimationFrame(time)
	};
	
	function numberWithCommas(x) {
	    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}

	var division = 0;
	
	//일자별
	function daily(today){
		division = 0;
		$("#workIdx").attr("disabled", false);
		
		$("#workIdx").css({
			"background" : "white"
		})
		
	 	if(today!=undefined || today!=null){
		 	
		 	$("#rangeRadio #sDate2").val(today)
	 	}
	 	$("input#eDate").val($("input#sDate").val())
	 	$("#dailyRadio #sDate").css({
	 		"background" : "white",
	 		"pointer-events": "auto"
	 	})
		$("#monthlyRadio #sMonth ,#rangeRadio #sDate2 ,#rangeRadio #eDate").css({
			"background" : "gray",
			"pointer-events": "none"
		})
		
		$("#dailyRadio input#sDate").datepicker({
	        "maxDate": 0,
			onSelect:function(e){
				$("#rangeRadio #sDate2").val(e)
				$("input#sDate").val(e)
				$("input#eDate").val(e)
			}
		})
	}
	
	
	
	//월별
	function monthly(){
		division = 2;
		$("#workIdx").attr("disabled", true);
		$("#workIdx").css({
			"background" : "lightgray"
		})
		
		
		$('input#sMonth').datepicker( {
//			"minDate": new Date(2017, 12-1, 1),
	        "maxDate": 0,
	        changeMonth: true,
	        changeYear: true,
	        showButtonPanel: true,
	        dateFormat: 'yy mm',
	        onClose: function(dateText, inst) { 
	            var month = Math.abs($("#ui-datepicker-div .ui-datepicker-month :selected").val()) + 1;
	            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
	            if(month<10){
	            	month = '0'+month.toString()
	            }
	            $('input#sMonth').val(year+'년'+month.toString()+'월');
	            $('input#sDate').val(year+'-'+month.toString()+'-'+'01');
	            $('input#sDate2').val(year+'-'+month.toString()+'-'+'01');
	            $('input#eDate').val(year+'-'+month.toString()+'-'+31);
	        }
	    })
	    
		$("#monthlyRadio #sMonth ").css({
			"pointer-events": "auto",
	 		"background" : "white"
	 	});
		$("#dailyRadio #sDate ,#rangeRadio #sDate2 ,#rangeRadio #eDate").css({
			"pointer-events": "none",
			"background" : "gray"
		});
	}
	
	//기간별
	function range(){
		division = 1;
		$("#workIdx").attr("disabled", true);
		$("#workIdx").css({
			"background" : "lightgray"
		})
		$("#rangeRadio #sDate2 ,#rangeRadio #eDate").css({
			"pointer-events": "auto",
	 		"background" : "white"
	 	})
		$("#dailyRadio #sDate ,#monthlyRadio #sMonth ").css({
			"pointer-events": "none",
			"background" : "gray"
		})
		
		$("#rangeRadio #sDate2").datepicker({
//			"minDate": new Date(2017, 12-1, 1),
	        "maxDate": 0,
			onSelect:function(e){
				console.log("선택")
				$("input#sDate").val(e);
			}
		})
		
		$("#rangeRadio #eDate").datepicker({
//			"minDate": new Date(2017, 12-1, 1),
	        "maxDate": 0,
			onSelect:function(e){
				$("#rangeRadio input#eDate").val(e)
			}
		})

	}

	var i=0;
	
	$(document).ready(function(){
		getoperationStatus();
		
		//단순화면 그리그 그리기  (기준달성장비)
		kendotable=$("#grid").kendoGrid({
			height:getElSize(1650),
			sortable: false,
			columns:[{title : "기준 달성 장비",headerAttributes: {
   	    		 style: "font-size:"+getElSize(50),
    	     },columns:[
				{
					title : "순위" ,
					field : "rank",
					width : getElSize(125) ,
					attributes: {
		        	      style: "font-size:"+getElSize(35),
	        	    },headerAttributes: {
	        	    	 style: "font-size:"+getElSize(37),
	        	    }
				},{
					field : "name" , title : "장비"  ,width : getElSize(290) ,
					attributes: {
		        	      style: "font-size:"+getElSize(35),
	        	    },headerAttributes: {
	        	    	 style: "font-size:"+getElSize(37),
	        	    }
				},{
					title : "목표시간" , 
					width : getElSize(220) ,
					field : "targetRuntime",
					template : "#=numberWithCommas((targetRuntime/3600).toFixed(2))# 시간",
					attributes: {
		        	      style: "font-size:"+getElSize(35),
	        	    },headerAttributes: {
	        	    	 style: "font-size:"+getElSize(37),
	        	    }
				},{
					title : "가동" , 
					width : getElSize(200) ,
					field : "optime",
					template : "#=numberWithCommas((inCycleTime/3600).toFixed(2))# 시간",
					attributes: {
		        	      style: "font-size:"+getElSize(35),
	        	    },headerAttributes: {
	        	    	 style: "font-size:"+getElSize(37),
	        	    }
				},{
					title : "순절삭" , 
					width : getElSize(200) ,
					field : "inCycleTime",
					template : "#=numberWithCommas((cuttingTime/3600).toFixed(2))# 시간",
					attributes: {
		        	      style: "font-size:"+getElSize(35),
	        	    },headerAttributes: {
	        	    	 style: "font-size:"+getElSize(37),
	        	    }
				},{
					field : "" , 
					title : "가동율" , 
					width : getElSize(210) ,
					template : "#=(isNaN((inCycleTime/targetRuntime)*100))?'0.00':((inCycleTime/targetRuntime)*100).toFixed(2)#%",
					attributes: { style: "font-weight:bolder;  text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black; font-size:"+getElSize(45)+'px !important; overflow: initial;' +'#=(opRatio>=100)?"color :rgb(79, 169, 95)":(opRatio<100 &&opRatio>=80)?"color: rgb(0, 255, 45)":(opRatio<80 &&opRatio>=60)?"color: yellow":"color: rgb(255, 122, 122)"#'},
					headerAttributes: {
	        	    	 style: "font-size:"+getElSize(37),
	        	    }
				},{
					field : "cuttingRatio" , 
					title : "순절삭율" , 
					width : getElSize(210) ,
					template : "#=(isNaN((cuttingTime/inCycleTime)*100))?'0.00':((cuttingTime/inCycleTime)*100).toFixed(2)#%",
					attributes: { style: "font-weight:bolder;  text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black; font-size:"+getElSize(45)+'px !important; overflow: initial;' +'#=(cuttingRatio>=100)?"color :rgb(79, 169, 95)":(cuttingRatio<100 &&cuttingRatio>=80)?"color: rgb(0, 255, 45)":(cuttingRatio<80 &&cuttingRatio>=60)?"color: yellow":"color: rgb(255, 122, 122)"#'},
					headerAttributes: {
	        	    	 style: "font-size:"+getElSize(37),
	        	    }
				}
			]}]
		}).data("kendoGrid")
		
		//단순화면 그리그 그리기	(기준 미달 장비)
		kendotable2=$("#grid2").kendoGrid({
			height:getElSize(1650),
			sortable: false,
			columns:[{title : "기준 미달 장비",headerAttributes: {
   	    		 style: "font-size:"+getElSize(50),
    	     },columns:[
				{
					title : "순위" ,
					field : "rank",
					width : getElSize(125) ,
					attributes: {
		        	      style: "font-size:"+getElSize(35),
	        	    },headerAttributes: {
	        	    	 style: "font-size:"+getElSize(37),
	        	    }
				},{
					field : "name" , title : "장비"  ,width : getElSize(290) ,
					attributes: {
		        	      style: "font-size:"+getElSize(35),
	        	    },headerAttributes: {
	        	    	 style: "font-size:"+getElSize(37),
	        	    }
				},{
					title : "목표시간" , 
					width : getElSize(220) ,
					field : "targetRuntime",
					template : "#=numberWithCommas((targetRuntime/3600).toFixed(2))# 시간",
					attributes: {
		        	      style: "font-size:"+getElSize(35),
	        	    },headerAttributes: {
	        	    	 style: "font-size:"+getElSize(37),
	        	    }
				},{
					title : "가동" , 
					width : getElSize(200) ,
					field : "optime",
					template : "#=numberWithCommas((inCycleTime/3600).toFixed(2))# 시간",
					attributes: {
		        	      style: "font-size:"+getElSize(35),
	        	    },headerAttributes: {
	        	    	 style: "font-size:"+getElSize(37),
	        	    }
				},{
					title : "순절삭" , 
					width : getElSize(200) ,
					field : "inCycleTime",
					template : "#=numberWithCommas((cuttingTime/3600).toFixed(2))# 시간",
					attributes: {
		        	      style: "font-size:"+getElSize(35),
	        	    },headerAttributes: {
	        	    	 style: "font-size:"+getElSize(37),
	        	    }
				},{
					field : "opRatio" , 
					title : "가동율" , 
					width : getElSize(210) ,
					template : "#=(isNaN((inCycleTime/targetRuntime)*100))?'0.00':((inCycleTime/targetRuntime)*100).toFixed(2)#%",
					attributes: { style: "font-weight:bolder;  text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black; font-size:"+getElSize(45)+'px !important; overflow: initial;' +'#=(opRatio>=100)?"color :rgb(79, 169, 95)":(opRatio<100 &&opRatio>=80)?"color: rgb(0, 255, 45)":(opRatio<80 &&opRatio>=60)?"color: yellow":"color: rgb(255, 122, 122)"#'},
					headerAttributes: {
	        	    	 style: "font-size:"+getElSize(37),
	        	    }
				},{
					field : "cuttingRatio" , 
					title : "순절삭율" , 
					width : getElSize(210) ,
					template : "#=(isNaN((cuttingTime/inCycleTime)*100))?'0.00':((cuttingTime/inCycleTime)*100).toFixed(2)#%",
					attributes: { style: "font-weight:bolder;  text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black; font-size:"+getElSize(45)+'px !important; overflow: initial;' +'#=(cuttingRatio>=100)?"color :rgb(79, 169, 95)":(cuttingRatio<100 &&cuttingRatio>=80)?"color: rgb(0, 255, 45)":(cuttingRatio<80 &&cuttingRatio>=60)?"color: yellow":"color: rgb(255, 122, 122)"#'},
					headerAttributes: {
	        	    	 style: "font-size:"+getElSize(37),
	        	    }
				}
			]}]
		}).data("kendoGrid")
		
		//상세화면 그리드 그리기
		detailtabl=$("#detailGrid").kendoGrid({
			height:getElSize(1700),
			sortable: false,
			columns:[{
					title : "순위" ,
					field : "rank",
					width : getElSize(170) ,
					attributes: {
		        	      style: "font-size:"+getElSize(40),
	        	    },headerAttributes: {
	        	    	 style: "font-size:"+getElSize(43),
	        	    }
				},{
					field : "name" , title : "장비"  ,width : getElSize(390) ,
					attributes: {
		        	      style: "font-size:"+getElSize(40),
	        	    },headerAttributes: {
	        	    	 style: "font-size:"+getElSize(43),
	        	    }
				},{
					title : "근무일수"  ,
					width : getElSize(250) ,
					template : '#=diffDate#일',
					attributes: {
		        	      style: "font-size:"+getElSize(40),
	        	    },headerAttributes: {
	        	    	 style: "font-size:"+getElSize(43),
	        	    }
				},{
					title : "목표시간"  ,
					width : getElSize(250) ,
					field : 'targetRuntime',
					template : '#=numberWithCommas((targetRuntime/3600).toFixed(2))#시간',
					attributes: {
		        	      style: "font-size:"+getElSize(40),
	        	    },headerAttributes: {
	        	    	 style: "font-size:"+getElSize(43),
	        	    }
				},{
					field : "waitTime" ,
					template : "#=numberWithCommas((waitTime/3600).toFixed(2))#시간",
					title : "대기"  ,
					width : getElSize(250) ,
					attributes: {
		        	      style: "font-size:"+getElSize(40),
	        	    },headerAttributes: {
	        	    	 style: "font-size:"+getElSize(43),
	        	    }
				},{
					field : "alarmTime" ,
					template : '#=numberWithCommas((alarmTime/3600).toFixed(2))#시간',
					title : "알람"  ,
					width : getElSize(250) ,
					attributes: {
		        	      style: "font-size:"+getElSize(40),
	        	    },headerAttributes: {
	        	    	 style: "font-size:"+getElSize(43),
	        	    }
				},{
					title : "Power Off"  ,
					width : getElSize(250) ,
					template : "#=numberWithCommas((noConnectionTime/3600).toFixed(2))#시간",
					attributes: {
		        	      style: "font-size:"+getElSize(40),
	        	    },headerAttributes: {
	        	    	 style: "font-size:"+getElSize(43),
	        	    }
				},{
					title : "기타손실"  ,
					width : getElSize(250) ,
					template : '#=((optime-(waitTime+alarmTime+(inCycleTime)))/3600).toFixed(2)#시간',
					attributes: {
		        	      style: "font-size:"+getElSize(40),
	        	    },headerAttributes: {
	        	    	 style: "font-size:"+getElSize(43),
	        	    }
				},{
					title : "가동시간"  ,
					width : getElSize(250) ,
					field : "optime",
					template : "#=(inCycleTime/3600).toFixed(2)#시간",
					attributes: {
		        	      style: "font-size:"+getElSize(40),
	        	    },headerAttributes: {
	        	    	 style: "font-size:"+getElSize(43),
	        	    }
				},{
					title : "무부하"  ,
					width : getElSize(250) ,
					template : "#=numberWithCommas(((inCycleTime-cuttingTime)/3600).toFixed(2))#시간",
					attributes: {
		        	      style: "font-size:"+getElSize(40),
	        	    },headerAttributes: {
	        	    	 style: "font-size:"+getElSize(43),
	        	    }
				},{
					title : "순절삭"  ,
					width : getElSize(250) ,
					field : "cuttingTime",
					template : "#=numberWithCommas((cuttingTime/3600).toFixed(2))#시간",
					attributes: {
		        	      style: "font-size:"+getElSize(40),
	        	    },headerAttributes: {
	        	    	 style: "font-size:"+getElSize(43),
	        	    }
				},{
					field : "opRatio" , 
					title : "가동율"  ,
					width : getElSize(250) ,
					template : "#=(isNaN((inCycleTime/targetRuntime)*100))?'0.00':((inCycleTime/targetRuntime)*100).toFixed(2)#%",
					attributes: { style: "font-weight:bolder;  text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black; font-size:"+getElSize(45)+'px !important; overflow: initial;' +'#=(opRatio>=100)?"color :rgb(79, 169, 95)":(opRatio<100 &&opRatio>=80)?"color: rgb(0, 255, 45)":(opRatio<80 &&opRatio>=60)?"color: yellow":"color: rgb(255, 122, 122)"#'},
					headerAttributes: {
	        	    	 style: "font-size:"+getElSize(43),
	        	    }
				},{
					field : "cuttingRatio" , 
					title : "순절삭율"  ,
					width : getElSize(250) ,
					template : "#=(isNaN((cuttingTime/inCycleTime)*100))?'0.00':((cuttingTime/inCycleTime)*100).toFixed(2)#%",
					attributes: { style: "font-weight:bolder;  text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black; font-size:"+getElSize(45)+'px !important; overflow: initial;' +'#=(cuttingRatio>=100)?"color :rgb(79, 169, 95)":(cuttingRatio<100 &&cuttingRatio>=80)?"color: rgb(0, 255, 45)":(cuttingRatio<80 &&cuttingRatio>=60)?"color: yellow":"color: rgb(255, 122, 122)"#'},
	        	    headerAttributes: {
	        	    	 style: "font-size:"+getElSize(43),
	        	    }
			}]
		}).data("kendoGrid")
	});
	
	//테이블 데이터 가져오기
	var standdardOp;
	var kendodataBottom;
	var kendodataTop;
	var kendotable;
	var kendotable2;
	function getoperationStatus(){
		
		
		$.showLoading();
		//deviceStandard
		var url = "${ctxPath}/kpi/getopearationStatus.do";
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				console.log("----json-----");
				console.log(json);
//				workerStandard=json[0].workerStandard
				standdardOp=json[0].deviceStandard
				
				$("#standdardOp").val(standdardOp)
//				$("#deviceStandard").val(deviceStandard)
			}
		})
		
		
		
		
		
		var sDate = $("#sDate").val();
		var eDate = $("#eDate").val();
		
		var sDateMoment = moment(sDate)
		var eDateMoment = moment(eDate)
		var diffDate = eDateMoment.diff(sDateMoment, 'days') + 1
		
		var url = "${ctxPath}/kpi/getoperationStatus.do";
		var param = "sDate=" + sDate + 
					"&eDate=" + eDate +
					"&shopId=" + shopId ;
		console.log(diffDate)
		console.log("데이터 획득")
		console.log(param)
		if(division==0){
			if($("#workIdx").val()==1){
				if(sDate==moment().format("YYYY-MM-DD")){
					console.log(111111111111111)	//오늘야간
					url = "${ctxPath}/kpi/getoperationStatusToNight.do";
				}else{
					console.log(222222222)	//오늘 아닌 야간

//					console.log("오늘이 아님")
					url = "${ctxPath}/kpi/getoperationStatusNd.do";
					param += "&workIdx=" + $("#workIdx").val()
				}
			}else if($("#workIdx").val()==2){
				if(sDate==moment().format("YYYY-MM-DD")){
					console.log(3333333333)	//오늘 주간
				
				}else{
					console.log(44444444444)	//오늘아닌 주간
					url = "${ctxPath}/kpi/getoperationStatusNd.do";
					param += "&workIdx=" + $("#workIdx").val()
				}
			}
		}
		
		console.log(url + "?" + param)
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				console.log(json)
				
				var kendodata = new kendo.data.DataSource({})
				kendodataBottom = new kendo.data.DataSource({})//기준이하
				kendodataTop = new kendo.data.DataSource({})//기준이상
				var length = json.length
				for(var i=0;i<length;i++){
					json[i].rank=Number(json[i].rank);
					json[i].cuttingRatio = json[i].cuttingRatio.toFixed(2)
					json[i].opRatio = Number(json[i].opRatio.toFixed(2))
					json[i].optime = Number(json[i].optime)
					json[i].diffDate = diffDate
					if($("#workIdx").val()!=0){
						if($("#sDate").val()==moment().format("YYYY-MM-DD")){
							json[i].targetRuntime = json[i].targetRuntime/2
						}
					}
					
					//기준점 으로 그리드 나눠담기
					if(json[i].inCycleTime/json[i].targetRuntime*100<standdardOp){//기준미만
						kendodataBottom.add(json[i])
					}else if(json[i].inCycleTime/json[i].targetRuntime*100>=standdardOp){//기준이상
						kendodataTop.add(json[i])
					}
					kendodata.add(json[i])//모든 데이터
				}
				
				console.log(standdardOp);
				console.log(kendodataTop)
				console.log(kendodataBottom)
				
				
				/* "#=(isNaN((inCycleTime/targetRuntime)*100))?'0.00':((inCycleTime/targetRuntime)*100).toFixed(2)#%",
				template : "#=(isNaN((cuttingTime/inCycleTime)*100))?'0.00':((cuttingTime/inCycleTime)*100).toFixed(2)#%",
	 */
				kendotable.setDataSource(kendodataTop);
				kendotable2.setDataSource(kendodataBottom);
				detailtabl.setDataSource(kendodata);
				
//				kendotable.fetch();
			}
		})
	}
	
	var viewStatus="simple";
	
	function detailView(){
		
		//상세화면 사이즈 조정
		var gridElement = $("#detailGrid");
    
		if(viewStatus=="simple"){ //지금화면이 단순보기
			//임의로 kendogrid 사이즈 조정
			var height=$("#grid .k-grid-content.k-auto-scrollable")[0].offsetHeight
			
			$("#grid").css("display","none");
			$("#detailGrid").css("display","");
			$("#statusId").val("단순보기")
			viewStatus="detail";

			//임의로 kendogrid 사이즈 조정
			$(".k-grid-content.k-auto-scrollable").css("height",height);
		}else if(viewStatus=="detail"){ //지금화면이 상세보기
			$("#grid").css("display","");
			$("#detailGrid").css("display","none");
			$("#statusId").val("상세보기")
			viewStatus="simple";
		}
	}
	
	function updateOpStandard(){
		console.log($("#standdardOp").val())
		var str=$("#standdardOp").val().toString();
		if(str.length>3){
			$("#standdardOp").val(Number(str.substring(0,3)))
			return;
		}



		standdardOp=$("#standdardOp").val();
		
		var url = "${ctxPath}/kpi/updateOpStandard.do";
		var param ="deviceStandard=" + $("#standdardOp").val()
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			success : function(data){
				var grid = $("#grid").data("kendoGrid");
				var grid2 = $("#grid2").data("kendoGrid");
				var table2 = grid2.dataSource.data();
				var table = grid.dataSource.data();
				
				
				kendodataBottom = new kendo.data.DataSource({})//기준이하
				kendodataTop = new kendo.data.DataSource({})//기준이상

				for(i=0; i<table2.length; i++){
 					//장비 미달에서 기준보다 높은것들을 장비 달성으로 옮기기
					if(table2[i].inCycleTime/table2[i].targetRuntime*100>=standdardOp){
						kendodataTop.add(table2[i]);
					}
					if(table2[i].inCycleTime/table2[i].targetRuntime*100<standdardOp){
						kendodataBottom.add(table2[i]);
					}
				}
	 			
				/* if(json[i].inCycleTime/json[i].targetRuntime*100<standdardOp){//기준미만
					kendodataBottom.add(json[i])
				}else if(json[i].inCycleTime/json[i].targetRuntime*100>=standdardOp){//기준이상
					kendodataTop.add(json[i])
				} */
				
 				for(i=0; i<table.length; i++){
 					console.log(table2[i])
					//장비 초과에서 기준보다 낮은것들을 장비 미달로 옮기기
					if(table[i].inCycleTime/table[i].targetRuntime*100<standdardOp){
						kendodataBottom.add(table[i]);
					}else{
						kendodataTop.add(table[i]);
					}
				}

 				kendodataTop.sort([
					{ field: "rank", dir: "asc" }
				])
				kendodataBottom.sort([
					{ field: "rank", dir: "asc" }
				])
				
				kendotable.setDataSource(kendodataTop);
				kendotable2.setDataSource(kendodataBottom);
				
//				getoperationStatus();
			}
		});
	}
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="time"></div>
	<div id="title_right"></div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/kpi_left.png" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/green_right.png" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/selected_purple.png" class='menu_left'>
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top; ">
					<div id="dailyRadio">
						<input type="button" id="dailyRadio" name="datePicker" value="<spring:message code="By_date"></spring:message>" onclick="daily()" checked="checked">
						<input type="text" id="sDate" readonly>
						<select id="workIdx">
							<option value="0">전체</option>
							<option value="2">주간</option>
							<option value="1">야간</option>
						</select>
					</div>
					
					<div id="monthlyRadio">
						<input type="button" id="monthlyRadio" name="datePicker" value="<spring:message code="monthly"></spring:message>" onclick="monthly()">
						<input type="text" id="sMonth" readonly>
					</div>
					
					<div id="rangeRadio">
						<input type="button" id="rangeRadio" name="datePicker" value="<spring:message code="By_period"></spring:message>" onclick="range()">
						<input type="text" id="sDate2" readonly>~
						<input type="text" id="eDate" readonly>
					</div>
					
					<div id="divBtn">
						<input type="button" id="statusId" value="상세보기" onclick="detailView()"> 
 						<button id="search" onclick="getoperationStatus()" style="cursor: pointer; " ><i class="fa fa-search" aria-hidden="true"></i></button>
					</div>
 					
 					<div id="simplegrid" style="position: absolute; background: #121212">
	 					<div id="grid" style="width: 44%; background:white; float: left;">
	 					</div>
	 					
	 					<div style="width: 11.5%; background : black; color:white;float: left; margin-top: 250px; padding-top: 20px ;padding-bottom: 20px; " align="center">
	 					<h1>기준</h1><br>
	 					가동율:<input type="number" min="0" max="100" size="3px" id ="standdardOp" onkeyup="updateOpStandard()">%
	 					<!-- <br><br>
	 					순적살:<input type="number" min="0" max="100" size="3px" id ="" onkeyup="">% -->
	 					</div>
	 					
	 					<div  id="grid2" style="width: 44%; background:#2C3031; float: left;">
	 					</div>
 					
 					</div>
 					
					<!-- <div id="grid" style="display: inline-table; width: 40% ; " >
					</div>
					<div id="dd" style="background: white ; display: inline-table; width: 10% ;">
					</div>
					<div id="abc" style="display: inline-table; width: 40% ;background:white ">
					</div> -->
					
					<div id="detailGrid" style="display: none;">
					</div>
				</td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	