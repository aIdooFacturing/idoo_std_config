<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden; 
	background-color: black;
  	font-family:'Helvetica';
}
html{
	overflow : hidden;
}
#grid thead th{
	background: linear-gradient( to bottom, gray, black ) !important;
	color : white;
	text-align: center;	
}
#grid .k-grid-content{
	background: black !important;
}
#grid2 .k-grid-content{
	background: gray !important;
}
#grid2 thead th{
	background: linear-gradient( to bottom, gray, black ) !important;
	color : white !important;
	/* color : linear-gradient( to bottom, gray, black ) !important; */
	text-align: center;	
}
#grid2.k-grid-pager, #grid2.k-grid-header {
    border: 0 !important;
    background:gray;
}
/* #grid2 tbody td{
	background : black;
	border: 0 !important;
	font-weight: bold;
	
} */
#grid .k-grid-pager, #grid .k-grid-header {
    border: 0 !important;
    background:black;
}
#grid tbody td{
	background : black;
	border: 0 !important;
	font-weight: bold;
/* 	font-family: fantasy; */
	
}

.grouping {
	background : linear-gradient( to bottom, gray, black ) !important;
	text-align: left !important;
}
.minus{
	font-weight:bold;
/* 	font-family: fantasy; */
	color : #1312FF;
}

.plus{
	font-weight:bold;
	color : #DB0000;
}
.arrow{
	font-family: fantasy;
}
</style> 
<script type="text/javascript">
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	var handle = 0;
	
	$(function(){
		$("#sDate").datepicker({ maxDate: 0});
		createNav("kpi_nav", 6);
		
		setEl();
		time();
		setDate();
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	var todayChk="";
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var time =addZero(String(date.getHours()))+":"+addZero(String(date.getMinutes()));
		var Yday=new Date(date.setDate(date.getDate()-1));
		
		if(time<="08:30"){	//8시 30분이전일때 전날의 데이터 보여주기
			month = addZero(String(Yday.getMonth()+1));
			day = addZero(String(Yday.getDate()));
			todayChk="true";
		}
		$("#sDate").val(year + "-" + month + "-" + day);
	};
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : contentWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#banner").css({
			"font-size" : getElSize(80),
			"width" : getElSize(2000)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		$(".text2").css({
			"font-size" : getElSize(45),
			"padding-top" : getElSize(280),
			"padding-left" : getElSize(360)
		});
		
		$(".text1").css({
			"font-size" : getElSize(60),
			"background" : "linear-gradient( to bottom, #00A04B, #005026 )",
			"padding" : getElSize(14),
			
		});
		
		$(".minus").css({
			"font-size" : getElSize(45),
		})
		 	
		$(".plus").css({
			"font-size" : getElSize(45),
		})
		
		$("select").css("font-size",getElSize(47));
		$("select").css("height",getElSize(88))
		$("input").css("height",getElSize(88))
		$("input").css("font-size",getElSize(47));
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	function comma(x) {
	    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
	
	function IdxName(name){
		if(name==1){
			return "야간"
		}else if(name==2){
			return "주간"
		}
	}
	
	function c(x) {
	    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
	

	var kendotable;
	
	$(document).ready(function(){
		getWorkerList();
	})
    var date;

	function getTotalRatio(){
		$.showLoading()
		var url = "${ctxPath}/kpi/getTotalRatio.do";
		
		var checkedStandard="false";
//	   var date = new Date('2018-02-01');
		var performance;
		var Availability;
		var Quality;
		var OEE;
		
		
		//하루전 데이터 구하기
		var dayValue=1;	//기본 하루전날의 데이터구하기
	    date = new Date($("#sDate").val())
		var dayLabel = date.getDay();
		// 오늘이 월요일일시 3일전 금요일 날짜 구하기
		if(dayLabel==1){
			dayValue=3;
		}
		
	    var workIdx = Number($("#workIdx").val());
	    date.setDate(date.getDate() - dayValue);	//전날의 데이터 구하기
	    var arr=date.toLocaleString().split(".")
	    if(arr[1].trim().length==1){
	    	arr[1]="0"+arr[1].trim()
	    }
	    if(arr[2].trim().length==1){
	    	arr[2]="0"+arr[2].trim()
	    }
	    date=arr[0]+"-"+arr[1].trim()+"-"+arr[2].trim();
		monthDate=$("#sDate").val();
	    monthDate=monthDate.substring(0,7)
		
		if(today==$("#sDate").val()){
			checkedStandard="true";
		}
	    
		var param = "shopId=" + shopId +
					"&worker=" + $("#worker").val() +
					"&sDate=" + $("#sDate").val() +
					"&date=" + date +
					"&monthDate=" + monthDate +
					"&dvcId=" + $("#dvclist").val() +
					"&checkedStandard=" + checkedStandard +
					"&workIdx=" + workIdx;
		console.log(param)
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json1 = data.dataList1;	//위 테이블
				json2 = data.dataList2;	//아래 테이블
				var arr={};
				
				//게이지 부분
				performance=json1[0].goalRatio
				Availability=json1[0].operatingRate
				Quality=json1[0].faultCntRatio;
				OEE=(performance*Availability*Quality/10000).toFixed(0);
				
				
				var year=$("#sDate").val().substr(0,4);
				var mon=$("#sDate").val().substr(5,2);
				var day=$("#sDate").val().substr(8,2);

				//금월예상실적수량 구하기
				if(year==(new Date()).getFullYear() && mon==(new Date()).getMonth()+1){
					
					var lastDay = ( new Date( year, mon, 0) ).getDate();	//마지막날짜
					
					var holCnt=0;
					var PassCnt=0;
					console.log(lastDay)
					
					
					for(i=1; i<=(new Date()).getDate(); i++){
						var hoilyDay = new Date(year+"-"+mon+"-"+i)
						if(Number(hoilyDay.getDay())==0 || Number(hoilyDay.getDay())==6){
							PassCnt++;
						}
					}
					
					for(i=1; i<=lastDay; i++){
						var hoilyDay = new Date(year+"-"+mon+"-"+i)
						if(Number(hoilyDay.getDay())==0 || Number(hoilyDay.getDay())==6){
							holCnt++;
						}
					}
					console.log(json2[3].cnt)
					
					json2[3].cnt=json2[3].cnt/((new Date()).getDate())*(lastDay-holCnt+PassCnt)
			
					console.log(PassCnt)
					console.log(holCnt)
					console.log((new Date()).getDate()-PassCnt)
					console.log(lastDay-holCnt)
					
				}
				
				
				if(getToday().substr(0,10)==$("#sDate").val() || todayChk=="true"){
					//금일예상 퍼센트 계산하기
					var timeCheck=json1[0].timeCheck;
					if(workIdx==0){
						totalCnt=1440
					}else{
						totalCnt=720
					}
					
					json2[0].cnt=Number(json2[0].cnt/timeCheck*totalCnt).toFixed(0)
					json2[0].opTime=Number(json2[0].opTime/timeCheck*totalCnt).toFixed(0)
				}

				//상,하강 퍼센트 구하기
				for(i=0;i<3;i++){
					json2[i].expCnt=Number((json2[i].cnt-json2[2].cnt)/json2[2].cnt*100).toFixed(0)
					json2[i].expTime=Number((Number(json2[i].opTime)-Number(json2[2].opTime))/Number(json2[2].opTime)*100).toFixed(0)
					json2[i].expFault=Number((json2[i].faultCnt-json2[2].faultCnt)/json2[2].faultCnt*100).toFixed(0)
					//0번 자리까지 표시
					json2[i].cnt=Number(json2[i].cnt).toFixed(0)
					json2[i].opTime=Number(json2[i].opTime).toFixed(0)
				}
				for(i=3;i<json2.length;i++){
					json2[i].expCnt=Number((json2[i].cnt-json2[5].cnt)/json2[5].cnt*100).toFixed(0)
					json2[i].expTime=Number((Number(json2[i].opTime)-Number(json2[5].opTime))/Number(json2[5].opTime)*100).toFixed(0)
					json2[i].expFault=Number((json2[i].faultCnt-json2[5].faultCnt)/json2[5].faultCnt*100).toFixed(0)
				}
				if(json2[0].cnt=="NaN"){
					console.log("들어옴")
					json2[0].cnt="일생산계획 미등록"
				}
				if(json2[0].opTime=="NaN"){
					console.log("들어옴11")
					json2[0].opTime="일생산계획 미등록"
				}
				
				//진도 뿌리기
				var dataTime=json1[0].tgDate;
				var progress=0;
				if(getToday().substr(0,10)==$("#sDate").val() || todayChk=="true"){
					progress=json1[0].progress;
					dataTime+=" "+json1[0].dataTime.substr(11,8);
/* 					console.log(dataTime)
					console.log(json1[0].dataTime.substr(11,8))
					console.log(json1[0].dataTime) */
				}else if(getToday().substr(0,10)!=$("#sDate").val()){
					progress=100
				}
				if(json1[0].progress==null){
					progress=0;
				}
				console.log(dataTime)
				$("#progress").html(dataTime.substr(0,10)+"<br>"+dataTime.substr(11,8)+"<br>(진도 :" + progress + " %)")
				
				//달성율 테이블 뿌리기
				$("#perforM").html(Number(performance).toFixed(0)+" %")
				$("#AvailaB").html(Number(Availability).toFixed(0)+" %")
				$("#Quality").html(Number(Quality).toFixed(2)+" %")
				
				//실적 테이블 뿌리기
				$("#perCnt").html(comma(Number(json1[0].cnt).toFixed(0))+" EA")
				$("#AvailTime").html(comma(Number(json1[0].opTime).toFixed(0))+" Hr")
				$("#QuaCnt").html(comma(Number(json1[0].faultCnt).toFixed(0))+" EA")
				
 				performanceGauge(performance);
				AvailabilityGauge(Availability);
				createGauge2(Quality);
				createGauge3(OEE);
				
				console.log("----json1----")
				console.log(json1)
				console.log("----json2----")
				console.log(json2)
				
				
				//일자료
				$("#dayDate").empty();
				$("#monthDate").empty();
				
				var dayTable="<table border='8' class='tableCss' style='height:100%; width:100%'><tr style='height :33%'><td rowspan='3' style='width:7%'>일자료</td>";
				for(var i=0, length=3; i<length; i++){
					if(i==0){
						json2[i].name="금일 예상"
					}else if(i==1){
						json2[i].name="전일 실적"
					}else if(i==2){
						json2[i].name="지난 30일"
					}
					var expCnt= Number(json2[i].expCnt);
					var expTime= Number(json2[i].expTime);
					var expFault= Number(json2[i].expFault);
					dayTable += "<td style='width:13%'>" + json2[i].name +"</td>" ;
					dayTable +=	"<td style='width:26%'>" + comma(json2[i].cnt) ;
					// 상,하강 if문  -->생산수량
					if(i==2){
						dayTable += "</td>";
					}else if(expCnt>=0){
						dayTable += "  <label class='plus'>  ( "+ expCnt + "%";
							if(expCnt>=15){
								dayTable += "<label class='arrow'> ↑ </label> ) " + "</label> </td>" ;
							}else{
								dayTable += "<label class='arrow'> ▲ </label> ) " + "</label> </td>" ;
							}		

					}else if(expCnt<0){
						dayTable += "  <label class='minus'>  ( "+ expCnt + "%";
							if(expCnt>=-15){
								dayTable += "<label class='arrow'> ▼ </label> ) " + "</label> </td>" ;
							}else{
								dayTable += "<label class='arrow'> ↓ </label> ) " + "</label> </td>" ;
							}	
					}
					dayTable +=	"<td style='width:26%'>" + comma(json2[i].opTime) ;
					// 상,하강 if문  -->가동시간
					if(i==2){
						dayTable += "</td>";
					}else if(expTime>=0){
						dayTable += "  <label class='plus'>  ( "+ expTime + "%";
							if(expTime>=15){
								dayTable += "<label class='arrow'> ↑ </label> ) " + "</label> </td>" ;
							}else{
								dayTable += "<label class='arrow'> ▲ </label> ) " + "</label> </td>" ;
							}		

					}else if(expTime<0){
						dayTable += "  <label class='minus'>  ( "+ expTime + "%";
							if(expTime>=-15){
								dayTable += "<label class='arrow'> ▼ </label> ) " + "</label> </td>" ;
							}else{
								dayTable += "<label class='arrow'> ↓ </label> ) " + "</label> </td>" ;
							}	
					}else{
						dayTable += "</td>"
					}
					// 상,하강 if문  -->불량 수량
					dayTable +=	"<td style='width:26%'>" + comma(Number(json2[i].faultCnt).toFixed(0)) + " EA" ;
					expFault=expFault*-1;
					if(i==2){
						dayTable += " </td></tr><tr style='height :33%'>" ;
					}else if(expFault>=0){
						dayTable += "  <label class='plus'>  ( "+ expFault + "%";
							if(expFault>=15){
								dayTable += "<label class='arrow'> ↑ </label> ) " + "</label> </td></tr><tr style='height :33%'>" ;
							}else{
								dayTable += "<label class='arrow'> ▲ </label> ) " + "</label> </td></tr><tr style='height :33%'>" ;
							}		

					}else if(expFault<0){
						dayTable += "  <label class='minus'>  ( "+ expFault + "%";
							if(expFault>=-15){
								dayTable += "<label class='arrow'> ▼ </label> ) " + "</label> </td></tr><tr style='height :33%'>" ;
							}else{
								dayTable += "<label class='arrow'> ↓ </label> ) " + "</label> </td></tr><tr style='height :33%'>" ;
							}	
					}else{
						dayTable += "</td></tr><tr style='height :33%'>" ;
					}
					
				}
				
				dayTable +="</tr></table>"
				$("#dayDate").append(dayTable)
				
				//월자료
				var monthTable="<table border='8' class='tableCss' style='height:100%; width:100%'><tr style='height :33%'><td rowspan='3' style='width:7%'>월자료</td>";
				for(var i=3, length=json2.length; i<length; i++){
					if(i==3){
						json2[i].name="금월 예상"
					}else if(i==4){
						json2[i].name="전월 실적"
					}else if(i==5){
						json2[i].name="지난5개월"
					}
					var expCnt= Number(json2[i].expCnt);
					var expTime= Number(json2[i].expTime);
					var expFault= Number(json2[i].expFault);
					monthTable += "<td style='width:13%'>" + json2[i].name + "</td>" ;
					monthTable += "<td style='width:26%'>" + comma(Number(json2[i].cnt).toFixed(0)) ;
					// 상,하강 if문 
					if(i==5){
						monthTable += "</td>";
					}else if(expCnt>=0){
						monthTable += "  <label class='plus'>  ( "+ expCnt + "%";
							if(expCnt>=15){
								monthTable += "<label class='arrow'> ↑ </label> ) " + "</label> </td>" ;
							}else{
								monthTable += "<label class='arrow'> ▲ </label> ) " + "</label> </td>" ;
							}		

					}else if(expCnt<0){
						monthTable += "  <label class='minus'>  ( "+ expCnt + "%";
							if(expCnt>=-15){
								monthTable += "<label class='arrow'> ▼ </label> ) " + "</label> </td>" ;
							}else{
								monthTable += "<label class='arrow'> ↓ </label> ) " + "</label> </td>" ;
							}	
					}else{
						monthTable += "</td>"
					}
					monthTable +=	"<td style='width:26%'>" + comma(Number(json2[i].opTime).toFixed(0)) ;
					// 상,하강 if문  -->가동시간
					if(i==5){
						monthTable += "</td>";
					}else if(expTime>=0){
						monthTable += "  <label class='plus'>  ( "+ expTime + "%";
							if(expTime>=15){
								monthTable += "<label class='arrow'> ↑ </label> ) " + "</label> </td>" ;
							}else{
								monthTable += "<label class='arrow'> ▲ </label> ) " + "</label> </td>" ;
							}		

					}else if(expTime<0){
						monthTable += "  <label class='minus'>  ( "+ expTime + "%";
							if(expTime>=-15){
								monthTable += "<label class='arrow'> ▼ </label> ) " + "</label> </td>" ;
							}else{
								monthTable += "<label class='arrow'> ↓ </label> ) " + "</label> </td>" ;
							}	
					}else{
						monthTable += "</td>"
					}
					// 상,하강 if문  -->불량 수량
					monthTable +=	"<td style='width:26%'>" + comma(Number(json2[i].faultCnt).toFixed(0)) + " EA";
					expFault=expFault*-1;
					if(i==5){
						monthTable += " </td></tr><tr style='height :33%'>" ;
					}else if(expFault>=0){
						monthTable += "  <label class='plus'>  ( "+ expFault + "%";
							if(expFault>=15){
								monthTable += "<label class='arrow'> ↑ </label> ) " + "</label> </td></tr><tr style='height :33%'>" ;
							}else{
								monthTable += "<label class='arrow'> ▲ </label> ) " + "</label> </td></tr><tr style='height :33%'>" ;
							}		

					}else if(expFault<0){
						monthTable += "  <label class='minus'>  ( "+ expFault + "%";
							if(expFault>=-15){
								monthTable += "<label class='arrow'> ▼ </label> ) " + "</label> </td></tr><tr style='height :33%'>" ;
							}else{
								monthTable += "<label class='arrow'> ↓ </label> ) " + "</label> </td></tr><tr style='height :33%'>" ;
							}	
					}else{
						monthTable += "</td></tr><tr style='height :33%'>" ;
					}
				}
				
				monthTable +="</tr></table>"
				$("#monthDate").append(monthTable)
				
				$(".tableCss").css("text-align","center");
				$(".tablecss").css("font-size",getElSize(52))
				
				$.hideLoading()
				/* 				var option = "<option value='ALL'>${total}</option>";
				
				$(json).each(function(idx, data){
					option += "<option value='" + data.prdNo + "'>" + data.prdNo + "</option>"; 
				});
				
				$("#dvclist").html(option);
 */				
//				getFaultyList();
			},error:function(request,status,error){
	       		alert("등록된 작업이 없습니다.")
	       		$.hideLoading();
			}

		});
	}
	var today
	function getDvcList(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		today = year + "-" + month + "-" + day;
		
		var url =  ctxPath + "/chart/getDvcNameList.do";
		var param = "shopId=" + shopId + 
					"&line=ALL" + 
					"&sDate=" + today + 
					"&eDate=" + today + " 23:59:59" + 
					"&fromDashboard=" + fromDashboard;
		
		dvcArray = [];
		
		$.ajax({
			url : url,
			data :param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var list = "<option value='ALL'>${total}</option>";
				
				$(json).each(function(idx, data){
					dvcArray.push(data.id)
					list += "<option value='" + data.id + "'>" + decodeURIComponent(data.name).replace(/\+/gi," ") + "</option>";					
				});

				$("#dvclist").html(list);
				getTotalRatio();
			}
		});	
	};
	function getWorkerList(){
		var url = ctxPath + "/common/getWorkerList.do";
		var param = "shopId=" + shopId;
		$.showLoading();
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
				var option = "<option value='ALL'>${total}</option>";
				
				$(json).each(function(idx, data){
					option += "<option value='" + data.id + "'>" + decode(data.name) + "</option>"; 
				});
				
				$("#worker").html(option);
				getDvcList();
				
			}
		});
	}
	
	function performanceGauge(data) {
		$("#gauge").kendoRadialGauge({
            theme: "black",

		    pointer: [{
		        value: data,
		        color: "#c20000",
		        cap: { size: 0.05 }
		    }],
		    title: {
            	font:getElSize(48) + "px sans-serif",	
            	position: "right",
                text: "현상"
            },
		    scale: {
		    	labels:{
        			font:getElSize(48) + "px sans-serif",	
		        },
                majorUnit: 10, //큰기준
                majorTicks: { //큰기준
                    width: 1,
                    size: 7
                },
                minorTicks: {
                    size: 5
                },
		        minorUnit: 2, //작은바늘
		        startAngle: -30,
		        endAngle: 210,
		        max: 100,
		        label:{
		        	color:"blue"
		        },
		        color:"red",
		        ranges: [{
	                from: 00,
	                to: 9.5,
	                color: "red"
	            },{
	                from: 10.5,
	                to: 19.5,
	                color: "#FF3636"
	            },{
	                from: 20.5,
	                to: 29.5,
	                color: "#FF5E00"
	            },{
	                from: 30.5,
	                to: 39.5,
	                color: "#FF8224"
	            },{
	                from: 40.5,
	                to: 49.5,
	                color: "#FFE400"
	            },{
	                from: 50.5,
	                to: 59.5,
	                color: "#FFFF24"
	            },{/////////////////
	                from: 60.5,
	                to: 69.5,
	                color: "#9FC93C"
	            },{
	                from: 70.5,
	                to: 79.5,
	                color: "#6B9900"
	            },{
	                from: 80.5,
	                to: 89.5,
	                color: "#2F9D27"
	            },{
	                from: 90.5,
	                to: 99.5,
	                color: "#22741C"
	            }]
		    }
		});
	}
	function AvailabilityGauge(data) {
		$("#gauge1").kendoRadialGauge({
            theme: "black",

		    pointer: [{
		        value: data,
		        color: "#c20000",
		        cap: { size: 0.05 }
		    }],
		    title: {
            	font:getElSize(48) + "px sans-serif",	
            	position: "right",
                text: "현상"
            },
		    scale: {
		    	labels:{
        			font:getElSize(48) + "px sans-serif",	
		        },
                majorUnit: 10, //큰기준
                majorTicks: { //큰기준
                    width: 1,
                    size: 7
                },
                minorTicks: {
                    size: 5
                },
		        minorUnit: 2, //작은바늘
		        startAngle: -30,
		        endAngle: 210,
		        max: 100,
		        label:{
		        	color:"blue"
		        },
		        color:"red",
		        ranges: [{
	                from: 00,
	                to: 9.5,
	                color: "red"
	            },{
	                from: 10.5,
	                to: 19.5,
	                color: "#FF3636"
	            },{
	                from: 20.5,
	                to: 29.5,
	                color: "#FF5E00"
	            },{
	                from: 30.5,
	                to: 39.5,
	                color: "#FF8224"
	            },{
	                from: 40.5,
	                to: 49.5,
	                color: "#FFE400"
	            },{
	                from: 50.5,
	                to: 59.5,
	                color: "#FFFF24"
	            },{/////////////////
	                from: 60.5,
	                to: 69.5,
	                color: "#9FC93C"
	            },{
	                from: 70.5,
	                to: 79.5,
	                color: "#6B9900"
	            },{
	                from: 80.5,
	                to: 89.5,
	                color: "#2F9D27"
	            },{
	                from: 90.5,
	                to: 99.5,
	                color: "#22741C"
	            }]
		    }
		});
	}
	function createGauge2(data) {
		$("#gauge2").kendoRadialGauge({
            theme: "black",

		    pointer: [{
		        value: data,
		        color: "#c20000",
		        cap: { size: 0.05 }
		    }],
		    title: {
            	font:getElSize(48) + "px sans-serif",	
            	position: "right",
                text: "현상"
            },
		    scale: {
		    	labels:{
        			font:getElSize(48) + "px sans-serif",	
		        },
                majorUnit: 10, //큰기준
                majorTicks: { //큰기준
                    width: 1,
                    size: 7
                },
                minorTicks: {
                    size: 5
                },
		        minorUnit: 2, //작은바늘
		        startAngle: -30,
		        endAngle: 210,
		        max: 100,
		        label:{
		        	color:"blue"
		        },
		        color:"red",
		        ranges: [{
	                from: 00,
	                to: 9.5,
	                color: "red"
	            },{
	                from: 10.5,
	                to: 19.5,
	                color: "#FF3636"
	            },{
	                from: 20.5,
	                to: 29.5,
	                color: "#FF5E00"
	            },{
	                from: 30.5,
	                to: 39.5,
	                color: "#FF8224"
	            },{
	                from: 40.5,
	                to: 49.5,
	                color: "#FFE400"
	            },{
	                from: 50.5,
	                to: 59.5,
	                color: "#FFFF24"
	            },{/////////////////
	                from: 60.5,
	                to: 69.5,
	                color: "#9FC93C"
	            },{
	                from: 70.5,
	                to: 79.5,
	                color: "#6B9900"
	            },{
	                from: 80.5,
	                to: 89.5,
	                color: "#2F9D27"
	            },{
	                from: 90.5,
	                to: 99.5,
	                color: "#22741C"
	            }]
		    }
		});
	}
	function createGauge3(data) {
		$("#gauge3").kendoRadialGauge({
			theme: "black",

		    pointer: [{
		        value: data,
		        color: "#c20000",
		        cap: { size: 0.05 }
		    }],
		    scale: {
		    	labels:{
        			font:getElSize(48) + "px sans-serif",	
		        },
                majorUnit: 10, //큰기준
                majorTicks: { //큰기준
                    width: 1,
                    size: 7
                },
                minorTicks: {
                    size: 5
                },
		        minorUnit: 2, //작은바늘
		        startAngle: -30,
		        endAngle: 210,
		        max: 100,
		        label:{
		        	color:"blue"
		        },
		        color:"red",
		        ranges: [{
	                from: 00,
	                to: 9.5,
	                color: "red"
	            },{
	                from: 10.5,
	                to: 19.5,
	                color: "#FF3636"
	            },{
	                from: 20.5,
	                to: 29.5,
	                color: "#FF5E00"
	            },{
	                from: 30.5,
	                to: 39.5,
	                color: "#FF8224"
	            },{
	                from: 40.5,
	                to: 49.5,
	                color: "#FFE400"
	            },{
	                from: 50.5,
	                to: 59.5,
	                color: "#FFFF24"
	            },{/////////////////
	                from: 60.5,
	                to: 69.5,
	                color: "#9FC93C"
	            },{
	                from: 70.5,
	                to: 79.5,
	                color: "#6B9900"
	            },{
	                from: 80.5,
	                to: 89.5,
	                color: "#2F9D27"
	            },{
	                from: 90.5,
	                to: 99.5,
	                color: "#22741C"
	            }]
		    }
		});
	}
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="time"></div>
	<div id="title_right"></div>
	
	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/mainten_left.png" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/purple_right.png" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/selected_purple.png" class='menu_left'>
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top; ">
					<label class="text1">기준일 </label> <input type="text" id="sDate" size="9%" readonly> <label class="text1"> 주/야 </label> <select id="workIdx"><option value='0'>전체</option><option value=2>주간</option><option value=1>야간</option></select>
					<label class="text1"> 작업자 </label> <select id="worker"></select><label class="text1"> 장비 </label><select id="dvclist"><option></option></select>
					<input type="button" value="조회" onclick="getTotalRatio()">
					
<!-- 					<div style="background: red; position: absolute; width: 100%; height: 30% ;float: left;">
						asd
					</div>
 -->					
					<div style="position: absolute; float: left; background: #121212; height: 44%; width: 87%" >
						<table class='tablecss' border="1" style="width: 100% ;height: 100%; text-align: center;">
							<tr style="height: 20%; width: 100%">
								<td rowspan="4" style="width: 7%">생<br>산<br>현<br>황</td>
								<td id='progress' rowspan="2" style="height: 100px; width: 13%">( 진도 : %)</td>
								<td style="width: 26%">Performance<br>(달성율)</td>
								<td style="width: 26%">Availability<br>(가동율)</td>
								<td style="width: 26%">Quality<br>(양품율)</td>
							</tr>
							<tr style="height: 50%; width: 100%">
								<td style="width: 26%"><div style="margin: 0; height: 100% ;width: 100%" id="gauge"></div></td>
								<td style="width: 26%"><div style="margin: 0; height: 100% ;width: 100%" id=gauge1></div></td>
								<td style="width: 26%"><div style="margin: 0; height: 100% ;width: 100%" id="gauge2"></div></td>
							</tr>
	
							<tr style="height: 10%; width: 100%">
								<td>달성율</td>
								<td id='perforM' style="width: 26% ;">  </td>
								<td id='AvailaB' style="width: 26% ;">  </td>
								<td id='Quality' style="width: 26% ;">  </td>
							</tr>
	
							<tr style="height: 20%; width: 100%">
								<td>실적</td>
								<td id='perCnt' style="width: 26% ;"><div ></div></td>
								<td id='AvailTime' style="width: 26% ;"><div ></div></td>
								<td id='QuaCnt' style="width: 26% ;"><div ></div></td>
							</tr>
						</table>
					</div>
					<div style="float: left ; width: 87%; height: 38%; position: absolute; margin-top: 23.6%">
						<div id="dayDate" style="height: 50%; width: 100%; background: linear-gradient( to bottom, gray, gray )">	<!-- 일자료 -->
						</div>
						<div id="monthDate" style="height: 50%; width: 100%; background: linear-gradient( to bottom, gray, gray )">	<!-- 월자료 -->
						</div>
						
					</div>

				</td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	