<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ include file="/WEB-INF/views/include/unomiclib.jsp"%>
<%@ page import="com.unomic.dulink.common.domain.CommonCode"%>
<%@ page import="com.unomic.dulink.chart.domain.*"%>
<%@ page session = "true" %>
<c:set var="ctxPath" value="${pageContext.request.contextPath}" scope="request"/>

<script src="${ctxPath }/js/jquery.js"></script>
<script src="${ctxPath }/js/jquery-ui.min.js"></script>
<script src="${ctxPath }/js/jquery.loading.min.js"></script>
<script src="${ctxPath }/js/moment.js"></script>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<style>
body{
	background: black;
	margin: 0;
	color: white;
	width: 100%;
	height: 100%;
	position: absolute;
}
#section1{
	width: 50%;
	height: 50%;
	float: left;
}
#section2{
	width: 50%;
	height: 50%;
	float: left;
}
#section3{
	width: 50%;
	height: 50%;
	float: left;
}
#section4{
	width: 50%;
	height: 50%;
	float: left;
}
.division{
	display: table;
	width: 100%;
	height: 100%;
}
.division div{
	width:100%;
	height:100%;
	display: table-cell;
	vertical-align: middle;
	text-align: center;
	float: inherit;
}
.division div span{
	display: inline-block;
	background: darkorchid;
	height: 70%;
	width: 70%;
	vertical-align: middle;
	cursor: pointer;
	font-size: 380%;
	border-radius: 7%;
	border-style: outset;
	border-width: 3%;
}
</style>

<script>
	function goWork(){
		location.href="${ctxPath}/pop/moveGoWork.do"
	}
	
	function offWork(){
		location.href="${ctxPath}/pop/moveOffWork.do"
	}
	
	function startJob(){
		location.href="${ctxPath}/pop/moveStartJob.do"
	}
	
	function endJob(){
		location.href="${ctxPath}/pop/moveEndJob.do"
	}
</script>



<body>
	<div id="section1">
		<div class='division'>
			<div>
				<span onclick="goWork()">
					<div style="display: table">
						<div style="display: table-cell;vertical-align: middle;">
							출근 보고
						</div>
					</div>
				</span>
			</div>
		</div>
	</div>
	
	<div id="section2">
		<div class='division'>
			<div>
				<span onclick="offWork()">
					<div style="display: table">
						<div style="display: table-cell;vertical-align: middle;">
							퇴근 보고
						</div>
					</div>
				</span>
			</div>
		</div>
	</div>
	
	<div id="section3">
		<div class='division'>
			<div>
				<span onclick="startJob()">
					<div style="display: table">
						<div style="display: table-cell;vertical-align: middle;">
							작업 시작
						</div>
					</div>
				</span>
			</div>
		</div>
	</div>
	
	<div id="section4">
		<div class='division'>
			<div>
				<span onclick="endJob()">
					<div style="display: table">
						<div style="display: table-cell;vertical-align: middle;">
							작업 종료
						</div>
					</div>
				</span>
			</div>
		</div>
	</div>
	
</body>
</html>