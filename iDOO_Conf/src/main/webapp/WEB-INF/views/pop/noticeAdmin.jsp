<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctxPath" value="${pageContext.request.contextPath}" scope="request"/>
<%@ include file="/WEB-INF/views/pop/lib.jsp"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script type="text/javascript">
	var addFaulty = "${addFaulty}";
	var $prdNo = "${prdNo}";
	var $cnt = "${cnt}";
	
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;

	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);

	var screen_ratio = getElSize(240);

	if (originHeight / screen_ratio < 9) {
		contentWidth = targetWidth / (targetHeight / originHeight)
		contentHeight = originHeight;
	};

	function getElSize(n) {
		return contentWidth / (targetWidth / n);
	};

	function setElSize(n) {
		return targetWidth / (contentWidth / n);
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>

<script src="${ctxPath }/js/jquery.js"></script>
<script src="${ctxPath }/js/jquery-ui.min.js"></script>
<script src="${ctxPath }/js/jquery.loading.min.js"></script>
<script src="${ctxPath }/js/moment.js"></script>

<style type="text/css">
body{
	margin : 0;
}
/* #logo{
    height: 33.732px;
    margin-right: 7.44792px;
    border-radius: 3.72396px;
    float: right;
    background-color: white;
}  */
</style>


<script type="text/javascript">

	var evtMsg;
	var nm = '<%=(String)session.getAttribute("nm")%>';
		nm = decode(nm);
	var empCd = '<%=(String)session.getAttribute("empCd")%>';
	
	
    var totalColor;

	$(function(){
        createCorver()
		setEl();
		sessionChk();
		getTable();
		<%-- <% 
			session.removeAttribute("empCd");
			System.out.println(session.getAttribute("empCd"));
			System.out.println("cmpCd:"+session.getAttribute("empCd"));
		%> --%>
		//focus TEXT 맞추기
		
		$("#time").html(moment().format("YYYY-MM-DD HH:mm:ss"))
		
		setInterval(function() {
			$("#time").html(moment().format("YYYY-MM-DD HH:mm:ss"))
		}, 1000);
	})

	//시작메뉴 집어넣기
	function ready(){
		
	}
	
	const createCorver = () => {
	    const corver = document.createElement("div")
	    corver.setAttribute("id", "corver")
	    corver.style.cssText =
	        "position : absolute;" +
	        "width : " + originWidth + "px;" +
	        "height : " + originHeight + "px;" +
	        "background-color : rgba(0, 0, 0, 0);" +
	        "transition : 1s;" +
	        "z-index : -1;";

	    $("body").prepend(corver)
	}
	
	const showCorver = () =>{
		$.showLoading();
		
	    $("#corver").css({
	        "background-color" : "rgba(0, 0, 0, 0.7)",
	        "z-index": 2
	    })
	    
		setTimeout(()=>{
			$.hideLoading();
		}, 1000)
	   
	}

	const hideCorver = () => {
	    $("#corver").css({
	        backgroundColor : "rgba(0, 0, 0, 0)"
	    })

	    setTimeout(()=>{
	        $("#corver").css("z-index", -1)
	    }, 1000)
	}
	
	//팝업창 띄우기
    function openPopup(){
        showCorver();

        $("#popup").css({
	        "transition" : "1s",
	        "opacity" : 1,
	        "display" : "",
	        "position": "absolute",
	        "background": "aliceblue",
	        "top": $("#content").height() * 0.16,
	        "left": $("#content").width() * 0.08,
	        "height": $("#content").height() * 0.80,
	        "width": $("#content").width() * 0.83,
	        "z-index" : 10,
	    })

    }
    
    // 색상표 그리기
    function getTable(){
    	var colorTable = "<table style='height: 85%; width: 99%;'>"

   		var r = 0;
		var g = 0;
		var b = 0;
    	
		var rp = 10;
		var gp = 20;
		var bp = 30;
    	// 가로 만들기 위해
    	for(i=0; i<=8; i++){
    		colorTable += "<tr>";
    		//세로 만들기 위해
    		for(j=0; j<=11; j++){
        		r += rp;
        		g += gp;
        		b += bp;
        		if(r>=255){
        			r = r-255;
        			rp = 30; 
        		}else if(g>=225){
        			g = g-255;
        			gp = 10; 
        		}else if(b>=225){
        			b = b-255;
        			bp = 20; 
        		}
    			
    			color = "rgb("+ r +"," + g +"," + b +")";
    			colorTable +="<td id=" + i+j +" style='background:" + color +"'; onclick='backgroundChange(this)'> " + "" + "</td>";
    			/* if(j<=7){
        			if(i<4){
            			b += 36;
            			color = "rgb("+ r +"," + g +"," + b +")";
            			colorTable +="<td style='background:" + color +"'> " + color + "</td>";
        			}else if(i==4){
        				r = 0;
        			}else{
        				g += 36;
            			color = "rgb("+ r +"," + g +"," + b +")";
            			colorTable +="<td style='background:" + color +"'> " + color + "</td>";
        			}
    				
    			}else{
        			if(i<4){
        				gg+=30;
        				
        				bb += 40;
            			color = "rgb("+ 0 +"," + gg +"," + bb +")";
            			colorTable +="<td style='background:" + color +"'> " + color + "</td>";
        			}else if(i==4){
        				bb = 255;
        				gg = 255;
        			}else{
            			color = "rgb("+ 255 +"," + gg +"," + bb +")";
            			colorTable +="<td style='background:" + color +"'> " + color + "</td>";
        				gg -= 40;
        				bb -= 30;
        			}
    			} */
    		}
    		colorTable += "</tr>";
    		
    		r += 50;
    	}
    	
    	colorTable +="</table>"
    	
		$("#colorTable").empty();
		$("#colorTable").append(colorTable);
    }   
    
    function cancelRow(){
		$("#popup").css({
	        "opacity" : 0,
	        "display" : "none",
	        "z-index" : 1,
	    })
	    
	    hideCorver();
	}

    
    //테스트 함수
    function testF(){
        totalColor
        
       /*  chk = true
		text = "나는 공지사항이다. 공지사항 공지사항";
		color = "red"
		if(chk){
			$(".noticText").html(text);
			
			$(".noticText").css({
				"font-size" : $("#logo2").height() *  0.8
				,"background" : color
			})
		} */
		console.log(totalColor)
		
        $(".noticeText").html($("#notice").val());
        $(".noticetext").css({
        	"background" : totalColor
        })

        
    }
    // 색변경
    function backgroundChange(color){
    	var id = color.id
    	color = $("#" +id).css("background");
    	console.log(color)
    	var color = color;
    	totalColor = color
    	$("#notice").css({
    		"background" : color
    	})
    	
    	cancelRow();
    }

	//사용자 session Chk 
	function sessionChk(){
		if(empCd==null || empCd=="null"){
			location.href='${ctxPath}/pop/popIndex.do';
		}
		$("#userId").html(nm);
	}
	//한글 인코딩
	function decode(str){
		return decodeURIComponent(str).replace(/\+/gi, " ")
	};
	
	function setEl(){
		
		$("#header").css({
/*             "position" : "absolute",
			"width" : getElSize(targetWidth), */
			"height" : originHeight * 0.06,
			"background" : "black",
			"color" : "white",
			"display": "block",
            "font-size" : getElSize(50)
		})
		
		$("#logo").css({
			"height": $("#header").height() * 0.5,
			"float": "left",
			"margin-top":  $("#header").height()/2 - $("#header").height() * 0.25,
			"margin-left": getElSize(30),
			"border-radius": getElSize(30),
			"cursor": "pointer"
		})

		$("#userId").css({
			"float":"left",
			"margin-left": getElSize(50) +"px",
			"margin-top":  $("#header").height()/2 - $("#header").height() * 0.30,
			"font-size": $("#logo").height() *  0.7
		})

		$("#logout").css({
			"cursor" : "pointer",
			"float":"left",
			"margin-top":  $("#header").height()/2 - $("#header").height() * 0.20,
			"margin-left": getElSize(30) +"px",
			"height": $("#logo").height() *  0.7
		})
		
		$("#logo2").css({
			"height":  $("#header").height() * 0.65,
			"float": "right",
			"margin-top": $("#header").height()/2 - $("#header").height() * 0.32,
			"margin-right": getElSize(30),
			"border-radius": getElSize(30),
			"background-color": "white"
		})
		
		$("#time").css({
//		    "position": "absolute",
		    "float":"right",
 			"margin-top":  $("#header").height()/2 - $("#header").height() * 0.32,
 			"margin-right": getElSize(50),
// 			"margin-right": getElSize($("#logo2").width()*15),
 			/*			"left": getElSize($("#logo2").width()*15), */
			"font-size": $("#logo2").height() *  0.7
		});

		 // 
	    $("#content").css({
	    	"height" : originHeight - $("#header").height() + "px"	
	    })
	    
		$("#headTitle").css({
			"position":"absolute",
			"top":  $("#header").height()/2 - $("#header").height() * 0.36,
//			"left": getElSize(2000),
			
			"display" : "inline-block",
			"background-color" : "rgb(255, 0, 0)",
	        "border" : getElSize(7) + "px solid white",
	        "border-radius" : getElSize(30) + "px",
	        "padding-left" : getElSize(15),
	        "padding-right" : getElSize(15),
	        "cursor" : "pointer",
	        
			"font-size": $("#logo2").height() *  0.7
		})

		$("#headTitle").css({
			"left" : (originWidth / 2) - ($("#headTitle").width() / 2),
		})
		
		// 네모 박스 위치 글자
	    $(".menu").css({
	        "font-size" : getElSize(150) + "px",
	        "border" : getElSize(7) + "px solid white",
	        "height" : originHeight * 0.3 + "px",
	        "width" : originWidth * 0.35 + "px",
	        "vertical-align" : "middle",
	        "cursor" : "pointer",
//	        "padding-top" : getElSize(50) + "px",
	        "transition" : "1s",
	        "margin" : originWidth * 0.04 + "px",
	        "margin-top" : (originHeight - $("#header").height()) * 0.06 + "px",
	        "border-radius" : getElSize(30) + "px",
	        "display" : "inline-block",
	        "color" : "white",
	        //"position" : "relative"
	    })

	    $(".menu").css({
	        "opacity" : 1,
	        "text-align" : "center"
	    })
	    
	    //근태관리 배경색
	    $("#box1").css({
	        "background-color" : "rgb(102,117,206)"
	    })

	    //작업관리 배경색
	    $("#box2").css({
	        "background-color" : "rgb(185, 56, 79)"
	    })

	    //이력조회 배경색
	    $("#box3").css({
	        "background-color" : "rgb(223, 208, 2)"
	    })
	    
	    $("#CTitle").css({
	    	"font-size": getElSize(200) + "px",
	    })
	    
	    $(".unos_input").css({
	        "background-color" : "rgba(0,0,0,0)",
	        "border" : "0",
	        "color" : "black",
	        "inline" : "0",
	        "outline" : "0",
	        "border-bottom" : getElSize(10) + "px solid lightgray",
	        "margin-top" : $("#logo").height() * 1.8 + "px",
	        "padding" : getElSize(15) + "px",
	        "font-size": getElSize(170) + "px",
	        "transition" : "1s"
        	
	    }).focus(()=>{
	        $(".unos_input").css({
	            "border-bottom" : getElSize(10) + "px solid red"
	        })
	    }).blur(()=>{
	        $(".unos_input").css({
	            "border-bottom" : getElSize(10) + "px solid lightgray"
	        })
	    })
	    
	    $(".btn").css({
	    	/* "top" : $("#content").height() * 0.96,
	    	"left" : getElSize(30),
	    	"position" : "absolute" , */
	    	"margin-top" : originHeight * 0.03 + "px",
	    	"margin-left" : originHeight * 0.02 + "px",
	        "border-radius": originWidth * 0.004 + "px",
	        "padding-left": originWidth * 0.012 + "px", 
	        "padding-right": originWidth * 0.012 + "px", 
	        "padding-top": originWidth * 0.004 + "px", 
	        "padding-bottom": originWidth * 0.004 + "px", 
	        "font-size": $("#logo").width() * 0.18 + "px",
	        "color": "white",
	        "cursor": "pointer",
	        "text-align": "center",
	    })
	    
	    $("#btnTest").css({
	    	"background" : "blue"
	    })
	    
	    $("#btnSave").css({
	    	"background" : "green"
	    })
	    
	    $("#btnDel").css({
	    	"background" : "red"
	    })
	    
	    
	    $(".btn").hover(function(){
	    	color = $(this).css("background");
	        $(this).css({
	            "background-color" : "black"
	        })
	    }, function(){
	        $(this).css({
	            "background-color" : color
	        })
	    });
		
		/* $(".unos_input").css({
			"margin-left" : (originWidth / 2) - ($(".unos_input").width() / 2),
			"margin-top" : (originHeight / 2) - ($(".unos_input").height() / 2) - $("#header").height(),
			"display" : "inline"
		}).focus() */
	}
	
</script>

</head>
<body>
    <div id="header">
			<img id="logo" src="https://www.digitaltwincloud.com/assets/imgs/default/logo.png" onclick="location.href='${ctxPath}/pop/popIndex.do'"/>
			<div id="userId"></div>
			<img id="logout" src="https://www.digitaltwincloud.com/EA/assets/imgs/default/logout.png"  onclick="location.href='${ctxPath}/pop/popIndex.do'"/>
			<div id="headTitle" onclick="location.href='${ctxPath}/pop/adminSetting.do'">4-2. 공지사항</div>
			
			<img id="logo2" src="http://www.bukwangpi.com/img_up/shop_pds/bukwang/design/img/logo.png"/>
			<div id="time"></div>
<%-- 		<div id="backBtn" onclick="location.href='${ctxPath}/pop/popIndex.do'" style="cursor: pointer;">
		</div> --%>
		<!-- <div id="rightH">
			<span id="time"></span>
		</div>
		<div id="aside">
		</div> -->
	</div>
	<div id="content">
		<table style="width: 100%; height: 100%; vertical-align: middle; text-align: center;">
			<tr>
				<td>
					<div>
						<span id = "CTitle">공지 사항 입력 (기능 구현중..)</span>
					</div>
					<div>
						<span>
							<input type="text" id="notice" class="unos_input"> <img src="${ctxPath}/images/colorTable.PNG" class="menu_icon" onclick = "openPopup()" style="width: 5%; cursor: pointer;">
						</span>
					</div>
					<div>
						<button id="btnTest" onclick="testF()" class="btn"> 테스트 </button>
						<button id="btnSave" class="btn"> 저장 </button>
						<button id="btnDel" class="btn"> 삭제 </button>
					</div>
				</td>
			</tr>
		</table>
	</div>

    <div id="popup" style="display: none; opacity: 0;">
		<div id="cTitle" style="height: 15%; width: 100%; font-size: 350%; font-weight: bold; text-align: center;">
			<span>
			</span>
			<div style="float: right;">
<!-- 				<button id="save" class="btn" onclick="backgroundChange()">확인</button> -->
				<button id="cancle" class="btn" onclick="cancelRow()">취소</button>
			</div>
		</div>
		<div id="colorTable" style="height: 85%; width: 99%;">
		</div>
		
	</div>
</body>
</html>