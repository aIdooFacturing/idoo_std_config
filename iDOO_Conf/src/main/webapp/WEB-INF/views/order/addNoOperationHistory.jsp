<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden; 
	background-color: black;
  	font-family:'Helvetica';
}

.table_title{
	background-color : #222222;
	border-color: black;
}

</style> 
<script type="text/javascript">
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		$(".date").val(year + "-" + month + "-" + day);
	};
	
	var handle = 0;
	
	$(function(){
		getPrdNo();
		createNav("kpi_nav", 6);
		
		setEl();
		setDate();	
		time();
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(80),
		});
		
		$("#content_table td").css({
			"color" : "#BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#delDiv").css({
			"position" : "absolute",
			"width" : getElSize(700),
			"height" :getElSize(200),
			"background-color" : "#444444",
			"color" : "white"
		});
		
		$("#delDiv").css({
			"top" : (window.innerHeight/2) - ($("#delDiv").height()/2), 
			"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
			//"z-index" : -1,
			"display" : "none",
			"border-radius" : getElSize(10),
			"padding" : getElSize(20)
		});
		
		$(".alarmTable").css({
			"font-size": getElSize(40),
		});
		
		$(".alarmTable td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		$("#wrapper").css({
			"height" :getElSize(1550),
			"width" : $(".right").width(),
			"overflow" : "auto"
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	

	function getPrdNo(){
		var url = "${ctxPath}/common/getPrdNoList.do";
		var param = "shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
				
				var option = "";
				
				$(json).each(function(idx, data){
					option += "<option value='" + data.prdNo + "'>" + data.prdNo + "</option>";
				});
				
				$("#group").html(option);
				$("#update #prdNo").html(option).change(function (){ getOprNm($("#update #prdNo").val())});
				$("#insert #prdNo").html(option).change(function (){ getOprNm($("#insert #prdNo").val())});
				
				getNonOpInfo();
				//getOprNm();
			}
		});
	};
	
	function getNonOpInfo(){
		classFlag = true;   
		var url = "${ctxPath}/chart/getNonOpInfo.do";
		var sDate = $("#sDate").val();
		
		var param = "sDate=" + sDate +
					"&prdNo=" + $("#group").val() 
					
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				$("#tbody").empty();
				
				var tr;
							
				$(json).each(function(idx, data){
					if(data.seq!="."){
						if(classFlag){
							className = "row2"
						}else{
							className = "row1"
						};
						classFlag = !classFlag;
						
						var nonTy;
						if(data.ty==1){
							nonTy = "야간";
						}else{
							nonTy = "주간"
						}
						
						var oprNo;
						if(data.oprNm=="0010"){
							oprNo = "R삭";
						}else if(data.oprNm=="0020"){
							oprNo = "MCT 1차";
						}else if(data.oprNm=="0030"){
							oprNo = "CNC 2차";
						}
						
						tr += "<tr class='contentTr " + className + "' id='tr" + data.id + "'>" +
									/* "<td >" + data.prdNo + "</td>" +
									"<td>" + oprNo + "</td>" + */ 
									"<td>" + decodeURIComponent(data.name).replace(/\+/gi, " ") + "</td>" +
									"<td>" + data.date + "</td>" + 
									"<td>" + nonTy + "</td>" + 
									"<td>" + Math.round(data.cuttingTime/60) + "</td>" + 
									"<td>" + Math.round(data.inCycleTime/60) + "</td>" + 
									"<td>" + Math.round(data.waitTime/60) + "</td>" + 
									"<td>" + Math.round(data.alarmTime/60) + "</td>" + 
									"<td>" + Math.round(data.noConnectionTime/60) + "</td>" +
									"<td>" + data.opRatio + "%</td>" +
									"<td>" + data.cuttingTimeRatio + "%</td>" +
									"<td>" + decodeURIComponent(data.worker).replace(/\+/gi, " ") + "</td>" +
									"<td>" + data.totalTime + "</td>" +
									"<td>" + data.nonOpTime + "</td>" +
									"<td>" + (data.totalTime - data.nonOpTime) + "</td>" +
									"<td>" + Math.round((data.totalTime - data.nonOpTime) / data.totalTime * 100) + "%</td>" +
									"<td>" + decodeURIComponent(data.nonOpTyTxt).replace(/\+/gi, " ") + "</td>" +
									"<td>" + data.startTime + "</td>" +
									"<td>" + data.endTime + "</td>" +
									"<td>" + data.nonOpTime + "</td>" +
									/* "<td><button onclick='showUpdateForm(" + data.id + ")'>수정</button></td>" +
									"<td><button onclick='chkDel(\"" + data.id + "\")'>삭제</button></td>" + */
							"</tr>";					
					}
				});
				
				
				$("#tbody").append(tr);
				
				$(".alarmTable").css({
					"font-size": getElSize(40),
				});
				
				$(".alarmTable td").css({
					"padding" : getElSize(20),
					"font-size": getElSize(40),
					"border": getElSize(5) + "px solid black"
				});
				
				
				$(".row1").not(".tr_table_fix_header").css({
					"background-color" : "#222222"
				});

				$(".row2").not(".tr_table_fix_header").css({
					"background-color": "#323232"
				});
				
				$("button").css("font-size", getElSize(40))
				$("#wrapper div:last").remove();
				//scrolify($('#table2'), getElSize(1450));
				$("#wrapper div:last").css("overflow", "auto")
				
				$("body, html").css({
					"overflow" : "hidden"	
				})
				
				$.hideLoading();
			}
		});
	};
	
	$(function() {
    	$("#sDate").datepicker({
    		onSelect:function(){
    			$.showLoading();
    			console.log($("#sDate").val());
    			getNonOpInfo();
    		}
    	})
	    	
	});
	
	</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="delDiv">
		<Center>
			<font><spring:message code="chk_del" ></spring:message></font><Br><br>
			<button id="resetOk" onclick="okDel();"><spring:message code="del" ></spring:message></button>&nbsp;&nbsp;
			<button id="resetNo" onclick="noDel();"><spring:message code="cancel" ></spring:message></button>
		</Center>
	</div>
	
	
	<div id="time"></div>
	<div id="title_right"></div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/order_left.png" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/purple_right.png" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/selected_purple.png" class='menu_left'>
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top;">
					<table id="content_table" style="width: 100%"> 
						<Tr>
							<td style="text-align: right;"> 
								<%-- <spring:message code="prdct_machine_line"></spring:message>
								<select id="group"> </select> --%>
								<spring:message code="date_"></spring:message>
								<input type="text" class="date" id="sDate">
								<img alt="" src="${ctxPath }/images/search.png" id="search" onclick="getNonOpInfo()">
							</td>
						</Tr>		
					</table> 
					<div id="wrapper">
						<center>
							<table style="width: 100%; color: white; text-align: center; border-collapse: collapse;" class="alarmTable" border="1" id="table2">
								<thead>
									<Tr class="table_title">
										<%-- <Td rowspan="2"><spring:message code="prdct_machine_line"></spring:message></Td>
										<Td rowspan="2"><spring:message code="operation"></spring:message></Td> --%>
										<%-- <Td rowspan="2"><spring:message code="machine_order"></spring:message></Td> --%>
										<Td rowspan="2"><spring:message code="device"></spring:message></Td>
										<Td rowspan="2"><spring:message code="date"></spring:message></Td>
										<Td rowspan="2"><spring:message code="division"></spring:message></Td>
										<Td colspan="7"><spring:message code="opratio"></spring:message></Td>
										<Td colspan="5"><spring:message code="worker"></spring:message><spring:message code="op_ratio"></spring:message></Td>
										<Td colspan="6"><spring:message code="non_op"></spring:message></Td>
									</Tr>
									<tr class="table_title">
										<Td><spring:message code="cutting_time_br"></spring:message> </Td>
										<Td><spring:message code="op_time_br"></spring:message></Td>
										<Td><spring:message code="wait_time_br"></spring:message></Td>
										<Td><spring:message code="alarm_time_br"></spring:message></Td>
										<Td><spring:message code="power_off_br"></spring:message></Td>
										<Td><spring:message code="op_ratio"></spring:message></Td>
										<Td><spring:message code="cuttingratio"></spring:message></Td>
										<Td><spring:message code="worker"></spring:message></Td>
										<Td><spring:message code="total_time"></spring:message></Td>
										<Td><spring:message code="snum_non_op"></spring:message></Td>
										<Td><spring:message code="op_time_minute"></spring:message>
										<Td><spring:message code="op_ratio"></spring:message><Br>(%)</Td>
										<Td><spring:message code="non_op_ty_br"></spring:message></Td>
										<Td><spring:message code="start"></spring:message></Td>
										<Td><spring:message code="end"></spring:message></Td>
										<Td><spring:message code="time_"></spring:message> <Br>(<spring:message code="minute"></spring:message> )</Td>
									<!-- 	<Td>수정</Td>
										<Td>삭제</Td> -->
									</tr>
								</thead>
								<tbody id="tbody">
								</tbody>
							</table>
						</center>
					</div>
				</td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'></Td>
			</Tr>
			<Tr>
				<Td><span class="nav_span"></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'></Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'></Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	