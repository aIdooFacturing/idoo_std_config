<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ include file="/WEB-INF/views/include/unomiclib.jsp"%>
<%@ page import="com.unomic.dulink.common.domain.CommonCode"%>
<%@ page import="com.unomic.dulink.chart.domain.*"%>
<%@ page session = "true" %>

<%-- <% 
	request.setCharacterEncoding("UTF-8"); 
	UserVo userVo = (UserVo) request.getAttribute("userInfo"); 
	System.out.println(userVo);
	String empCd = "";
	String name = "";
	try{
		System.out.println(userVo.getName());
		System.out.println(userVo.getName());
		System.out.println(userVo.getName());
		System.out.println(userVo.getName());
	}catch(NullPointerException e){
		System.out.println(" 값 없음.");
	}
	
	try{
		if(userVo.getName() != null && userVo.getName() != "000"){
			session.setAttribute("empCd", userVo.getEmpCd());
			session.setAttribute("name", userVo.getName());
		}else{
			String resultMsg = ""; 
			resultMsg += "<script type='text/javascript'>"; 
			resultMsg += "alert('아이디 또는 비밀번호를 잘못 입력하셨습니다.');";	
			resultMsg += "</script>"; 
			out.print(resultMsg); 
		}
	}catch(Exception e){
		
	}
	try{
		empCd = session.getAttribute("empCd").toString();
		name = (String) session.getAttribute("name");
		if(empCd==null||empCd.equals("") && userVo.getName() != "000"){
			if(request.getRequestURL().toString().indexOf("main.jsp")==-1){
				response.sendRedirect(request.getContextPath() + "/chart/index.do");
			}
		}else{
		}
	}catch(Exception e){
		if(request.getRequestURL().toString().indexOf("main.jsp")==-1){
			response.sendRedirect(request.getContextPath() + "/chart/index.do");
		}
	} 
%> --%>
<spring:message var="layout" code="layout"></spring:message>
<spring:message var="name" code="name"></spring:message>
<spring:message var="avrg_of_day" code="avrg_of_day"></spring:message>
<spring:message var="kr" code="kr"></spring:message>
<spring:message var="depart" code="depart"></spring:message>
<spring:message var="pwd" code="pwd"></spring:message>
<spring:message var="chk_del" code="chk_del"></spring:message>
<spring:message var="worker_num" code="worker_num"></spring:message>
<spring:message var="workingReportTotalMenu" code="workingReportTotalMenu"></spring:message>
<spring:message var="opratio_against_target" code="opratio_against_target"></spring:message>
<spring:message var="na" code="na"></spring:message>
<spring:message var="up" code="up"></spring:message>
<spring:message var="am" code="am"></spring:message>
<spring:message var="pm" code="pm"></spring:message>
<spring:message var="line" code="line"></spring:message>
<spring:message var="deliverer" code="deliverer"></spring:message>
<spring:message var="standard" code="standard"></spring:message>
<spring:message var="performance_insert" code="performance_insert"></spring:message>
<spring:message var="user" code="user"></spring:message>
<spring:message var="manager" code="manager"></spring:message>
<spring:message var="target_ratio" code="target_ratio"></spring:message>
<spring:message var="faulty_ratio" code="faulty_ratio"></spring:message>
<spring:message var="target_op_time" code="target_op_time"></spring:message>
<spring:message var="total_op_time" code="total_op_time"></spring:message>
<spring:message var="number_of_op_day" code="number_of_op_day"></spring:message>
<spring:message var="n_d" code="n_d"></spring:message>
<spring:message var="target" code="target"></spring:message>
<spring:message var="incycle" code="incycle"></spring:message>
<spring:message var="work" code="work"></spring:message>
<spring:message var="avg_cycle_time" code="avg_cycle_time"></spring:message>
<spring:message var="assiduity" code="assiduity"></spring:message>
<spring:message var="attendace_div" code="attendace_div"></spring:message>
<spring:message var="worker_manage" code="worker_manage"></spring:message>
<spring:message var="attendance_ty" code="attendance_ty"></spring:message>
<spring:message var="machine_list" code="machine_list"></spring:message>
<spring:message var="cancel" code="cancel"></spring:message>
<spring:message var="alarm_manage" code="alarm_manage"></spring:message>
<spring:message var="check_result_manage" code="check_result_manage"></spring:message>
<spring:message var="op_ratio" code="op_ratio"></spring:message>
<spring:message var="check" code="check"></spring:message>
<spring:message var="check1" code="check1"></spring:message>
<spring:message var="in_check" code="in_check"></spring:message>
<spring:message var="js_check" code="js_check"></spring:message>
<spring:message var="jr_check" code="jr_check"></spring:message>
<spring:message var="add_prdct_target" code="add_prdct_target"></spring:message>
<spring:message var="add_cmpl_prdct" code="add_cmpl_prdct"></spring:message>
<spring:message var="month" code="month"></spring:message>
<spring:message var="non_op" code="non_op"></spring:message>
<spring:message var="end_time" code="end_time"></spring:message>
<spring:message var="fixed_time" code="fixed_time"></spring:message>
<spring:message var="content" code="content"></spring:message>
<spring:message var="worker" code="worker"></spring:message>
<spring:message var="alarm" code="alarm"></spring:message>
<spring:message var="start" code="start"></spring:message>
<spring:message var="end" code="end"></spring:message>
<spring:message var="start_time" code="start_time"></spring:message>
<spring:message var="unnormal" code="unnormal"></spring:message>
<spring:message var="date_" code="date_"></spring:message>
<spring:message var="op_program" code="op_program"></spring:message>
<spring:message var="spd_err_g" code="spd_err_g"></spring:message>
<spring:message var="cycle_time" code="cycle_time"></spring:message>
<spring:message var="cycle_time_avrg" code="cycle_time_avrg"></spring:message>
<spring:message var="cycle_time_sd" code="cycle_time_sd"></spring:message>
<spring:message var="spd_load_sum" code="spd_load_sum"></spring:message>
<spring:message var="cuttingratio" code="cuttingratio"></spring:message>
<spring:message var="no_data" code="no_data"></spring:message>
<spring:message var="normal" code="normal"></spring:message>
<spring:message var="sum" code="sum"></spring:message>
<spring:message var="pre_release_cnt" code="pre_release_cnt"></spring:message>
<spring:message var="release_cnt" code="release_cnt"></spring:message>
<spring:message var="spd_load_sum_avrg" code="spd_load_sum_avrg"></spring:message>
<spring:message var="spd_load_sum_avrg_sd" code="spd_load_sum_avrg_sd"></spring:message>
<spring:message var="sample_cnt" code="sample_cnt"></spring:message>
<spring:message var="cycle_start" code="cycle_start"></spring:message>
<spring:message var="add_cmpl_prdct_history" code="add_cmpl_prdct_history"></spring:message>
<spring:message var="add_non_operation" code="add_non_operation"></spring:message>
<spring:message var="add_non_operation_hist" code="add_non_operation_hist"></spring:message>
<spring:message var="total" code="total"></spring:message>
<spring:message var="end_of_chart" code="end_of_chart"></spring:message>
<spring:message var="first_of_chart" code="first_of_chart"></spring:message>
<spring:message var="plan" code="plan"></spring:message>
<spring:message var="machine_performance" code="machine_performance"></spring:message>
<spring:message var="worker_performance" code="worker_performance"></spring:message>
<spring:message var="lot_tracer" code="lot_tracer"></spring:message>
<spring:message var="add_faulty_hist" code="add_faulty_hist"></spring:message>
<spring:message var="faulty" code="faulty"></spring:message>
<spring:message var="reoperation_cycle" code="reoperation_cycle"></spring:message>
<spring:message var="opstatus" code="opstatus"></spring:message>
<spring:message var="dailydevicestatus" code="dailydevicestatus"></spring:message>
<spring:message var="devicestatus" code="devicestatus"></spring:message>
<spring:message var="tool_manage" code="tool_manage"></spring:message>
<spring:message var="prdct_board" code="prdct_board"></spring:message>
<spring:message var="barchart" code="24barchart"></spring:message>
<spring:message var="performance_chart" code="performance_chart"></spring:message>
<spring:message var="operation_chart" code="operation_chart"></spring:message>
<spring:message var="prdct_chart" code="prdct_chart"></spring:message>
<spring:message var="operation_graph" code="operation_graph"></spring:message>
<spring:message var="operation_graph_daily" code="operation_graph_daily"></spring:message>
<spring:message var="check_program_error" code="check_program_error"></spring:message>
<spring:message var="income_manage" code="income_manage"></spring:message>
<spring:message var="income_history" code="income_history"></spring:message>
<spring:message code="device" var="device"></spring:message>
<spring:message code="program" var="program"></spring:message>
<spring:message code="stop" var="stop"></spring:message>
<spring:message code="noconnection" var="noconnection"></spring:message>
<spring:message code="cycle" var="cycle"></spring:message>
<spring:message code="ophour" var="ophour"></spring:message>
<spring:message code="wait" var="wait"></spring:message>
<spring:message code="avrg" var="avrg"></spring:message>
<spring:message code="confirm" var="confirm"></spring:message>
<spring:message code="confirm" var="confirm"></spring:message>
<spring:message code="ophour" var="ophour"></spring:message>
<spring:message code="confirm" var="confirm"></spring:message>
<spring:message var="release_manage" code="release_manage"></spring:message>
<spring:message var="release_history" code="release_history"></spring:message>
<spring:message var="ship_manage" code="ship_manage"></spring:message>
<spring:message var="income_no" code="income_no"></spring:message>
<spring:message var="prd_no" code="prd_no"></spring:message>
<spring:message var="com_name" code="com_name"></spring:message>
<spring:message var="lot_cnt" code="lot_cnt"></spring:message>
<spring:message var="check_cnt" code="check_cnt"></spring:message>
<spring:message var="faulty_cnt" code="faulty_cnt"></spring:message>
<spring:message var="faulty_history" code="faulty_history"></spring:message>
<spring:message var="add_faulty" code="add_faulty"></spring:message>
<spring:message var="income_date" code="income_date"></spring:message>
<spring:message var="del" code="del"></spring:message>
<spring:message var="selection" code="selection"></spring:message>
<spring:message var="target_prdct_cnt" code="target_prdct_cnt"></spring:message>
<spring:message var="prdct_per_cycle" code="prdct_per_cycle"></spring:message>
<spring:message var="working_time" code="working_time"></spring:message>
<spring:message var="uph_analysis" code="uph_analysis"></spring:message>
<spring:message var="prdct_machine_line" code="prdct_machine_line"></spring:message>
<spring:message var="performance_analysis" code="performance_analysis"></spring:message>
<spring:message var="working_capa" code="working_capa"></spring:message>
<spring:message var="prd_plan" code="prd_plan"></spring:message>
<spring:message var="ok_ratio" code="ok_ratio"></spring:message>
<spring:message var="spec" code="spec"></spring:message>
<spring:message var="achievement_ratio" code="achievement_ratio"></spring:message>
<spring:message var="worker_performance2" code="worker_performance2"></spring:message>
<spring:message var="diff" code="diff"></spring:message>
<spring:message var="prdct_performance" code="prdct_performance"></spring:message>
<spring:message var="op_performance" code="op_performance"></spring:message>
<spring:message var="day" code="day"></spring:message>
<spring:message var="copy_data" code="copy_data"></spring:message>
<spring:message var="minute" code="minute"></spring:message>
<spring:message var="second" code="second"></spring:message>
<spring:message var="night" code="night"></spring:message>
<spring:message var="income_cnt" code="income_cnt"></spring:message>
<spring:message var="release_prd" code="release_prd"></spring:message>
<spring:message var="prd_stock" code="prd_stock"></spring:message>
<spring:message var="complete_stock" code="complete_stock"></spring:message>
<spring:message var="ship_lot_no" code="ship_lot_no"></spring:message>
<spring:message var="lot_no" code="lot_no"></spring:message>
<spring:message var="release_count" code="release_count"></spring:message>
<spring:message var="op_order" code="op_order"></spring:message>
<spring:message var="order" code="order"></spring:message>
<spring:message var="order" code="order"></spring:message>
<spring:message var="income" code="income"></spring:message>
<spring:message var="release_num" code="release_num"></spring:message>
<spring:message var="cmpl" code="cmpl"></spring:message>
<spring:message var="unit" code="unit"></spring:message>
<spring:message var="count" code="count"></spring:message>
<spring:message var="reg_time" code="reg_time"></spring:message>
<spring:message var="ship_date" code="ship_date"></spring:message>
<spring:message var="ship_cnt" code="ship_cnt"></spring:message>
<spring:message var="cnt_more_than_complete_stock" code="cnt_more_than_complete_stock"></spring:message>
<spring:message var="save_ok" code="save_ok"></spring:message>
<spring:message var="ok" code="ok"></spring:message>
<spring:message var="divide" code="divide"></spring:message>
<spring:message var="ship_history" code="ship_history"></spring:message>
<spring:message var="non_operation_ty" code="non_operation_ty"></spring:message>
<spring:message var="move_operation" code="move_operation"></spring:message>
<spring:message var="current_operation" code="current_operation"></spring:message>
<spring:message var="stock" code="stock"></spring:message>
<spring:message var="operation" code="operation"></spring:message>
<spring:message var="move_cnt" code="move_cnt"></spring:message>
<spring:message var="cnt_more_than_stock" code="cnt_more_than_stock"></spring:message>
<spring:message var="release_date" code="release_date"></spring:message>
<spring:message var="divide_lot" code="divide_lot"></spring:message>
<spring:message var="mat_prd_no" code="mat_prd_no"></spring:message>
<spring:message var="current_operation" code="current_operation"></spring:message>
<spring:message var="stock_status" code="stock_status"></spring:message>
<spring:message var="division" code="division"></spring:message>
<spring:message var="stock_cnt" code="stock_cnt"></spring:message>
<spring:message var="lead_time" code="lead_time"></spring:message>
<spring:message var="time_" code="time_"></spring:message>
<spring:message var="operating_time_sec" code="operating_time_sec"></spring:message>
<spring:message var="operation_time_ratio" code="operation_time_ratio"></spring:message>
<spring:message var="cutting_time_sec" code="cutting_time_sec"></spring:message>
<spring:message var="cutting_time_ratio_sec" code="cutting_time_ratio_sec"></spring:message>
<spring:message var="non_operation_time_sec" code="non_operation_time_sec"></spring:message>
<spring:message var="non_operation_time_ratio" code="non_operation_time_ratio"></spring:message>
<spring:message var="operating_wait_sec" code="operating_wait_sec"></spring:message>
<spring:message var="operating_wait_ratio" code="operating_wait_ratio"></spring:message>
<spring:message var="total_sec" code="total_sec"></spring:message>
<spring:message var="faulty_ratio_customr_and_operation" code="faulty_ratio_customr_and_operation"></spring:message>
<spring:message var="prd_cnt" code="prd_cnt"></spring:message>
<spring:message var="faulty_cnt_operation" code="faulty_cnt_operation"></spring:message>
<spring:message var="faulty_cnt_customer" code="faulty_cnt_customer"></spring:message>
<spring:message var="operation_faulty_operation" code="operation_faulty_operation"></spring:message>
<spring:message var="operation_faulty_customer" code="operation_faulty_customer"></spring:message>
<spring:message var="f_m_l_check" code="f_m_l_check"></spring:message>
<spring:message var="report2Attendance" code="report2Attendance"></spring:message>
<spring:message var="total_machine_prd_status" code="total_machine_prd_status"></spring:message>
<spring:message var="production_Plan_Quantity" code="production_Plan_Quantity"></spring:message>
<spring:message var="production_quantity" code="production_quantity"></spring:message>
<spring:message var="worker_Noon_Operation" code="worker_Noon_Operation"></spring:message>
<spring:message var="delivery_Plan" code="delivery_Plan"></spring:message>
<spring:message var="routing_Manager" code="routing_Manager"></spring:message>
<spring:message var="manage_Item_Master" code="manage_Item_Master"></spring:message>
<spring:message var="noon_Operationtimestart" code="noon_Operationtimestart"></spring:message>
<spring:message var="noon_Operationtimeend" code="noon_Operationtimeend"></spring:message>
<spring:message var="noon_Operation_time" code="noon_Operation_time"></spring:message>
<spring:message var="no_Worker" code="no_Worker"></spring:message>
<spring:message var="search_Placeholder" code="search_Placeholder"></spring:message>
<spring:message var="password_Placeholder" code="password_Placeholder"></spring:message>
<spring:message var="delete_Confirm" code="delete_Confirm"></spring:message>
<spring:message var="report_to_Work" code="report_to_Work"></spring:message>
<spring:message var="go2Work" code="go2Work"></spring:message>
<spring:message var="report_to_Work" code="report_to_Work"></spring:message>
<spring:message var="test" code="test"></spring:message>
<spring:message var="report2NonOp" code="report2NonOp"></spring:message>
<spring:message var="material_Number" code="material_Number"></spring:message>
<spring:message var="worker_status" code="worker_status"></spring:message>
<spring:message var="maintenance_Request_List" code="maintenance_Request_List"></spring:message>
<spring:message var="worker_Performance" code="worker_Performance"></spring:message>
<spring:message var="worker_attandant" code="worker_attandant"></spring:message>
<spring:message var="search_term" code="search_term"></spring:message>
<spring:message var="add" code="add"></spring:message>
<spring:message var="save" code="save"></spring:message>
<spring:message var="product_rank" code="product_rank"></spring:message>
<spring:message var="product_rank_detail" code="product_rank_detail"></spring:message>
<spring:message var="chartStatus" code="chartStatus"></spring:message>
<spring:message var="work_report_totalmenu" code="work_report_totalmenu"></spring:message>
<spring:message var="report_on_work_performance" code="report_on_work_performance"></spring:message>

<spring:message var="price_one" code="price_one"></spring:message>
<spring:message var="item_class" code="item_class"></spring:message>
<spring:message var="Original_material_number" code="Original_material_number"></spring:message>
<spring:message var="Materialname" code="Materialname"></spring:message>


<spring:message var="view_current_status" code="view_current_status"></spring:message>
<spring:message var="work_center" code="work_center"></spring:message>
<spring:message var="elevated_code" code="elevated_code"></spring:message>
<spring:message var="group_count" code="group_count"></spring:message>
<spring:message var="Actioner" code="Actioner"></spring:message>
<spring:message var="Report_content" code="Report_content"></spring:message>

<spring:message var="Delivery_date" code="Delivery_date"></spring:message>
<spring:message var="Delivery_number" code="Delivery_number"></spring:message>
<spring:message var="cancel_" code="cancel_"></spring:message>
<spring:message var="delivery_note" code="delivery_note"></spring:message>
<spring:message var="unit_price" code="unit_price"></spring:message>
<spring:message var="price_" code="price_"></spring:message>

<spring:message var="Processing" code="Processing"></spring:message>
<spring:message var="routing_manual" code="routing_manual"></spring:message>

<spring:message var="By_car" code="By_car"></spring:message>
<spring:message var="by_Equipment" code="by_Equipment"></spring:message>
<spring:message var="By_date" code="By_date"></spring:message>
<spring:message var="monthly" code="monthly"></spring:message>
<spring:message var="By_period" code="By_period"></spring:message>
<spring:message var="Detail_screen" code="Detail_screen"></spring:message>
<spring:message var="Simple_screen" code="Simple_screen"></spring:message>
<spring:message var="print" code="print"></spring:message>
<spring:message var="Search" code="Search"></spring:message>
<spring:message var="ranking" code="ranking"></spring:message>
<spring:message var="Performance" code="Performance"></spring:message>
<spring:message var="Achievement_rate" code="Achievement_rate"></spring:message>
<spring:message var="Car_type" code="Car_type"></spring:message>
<spring:message var="Quantity" code="Quantity"></spring:message>
<spring:message var="work_Performance" code="work_Performance"></spring:message>
<spring:message var="total_cnt" code="total_cnt"></spring:message>
<spring:message var="Search_criteria" code="Search_criteria"></spring:message>
<spring:message var="Search_value" code="Search_value"></spring:message>
<spring:message var="By_worker" code="By_worker"></spring:message>
<spring:message var="reason" code="reason"></spring:message>
<spring:message var="getAttendanceList" code="getAttendanceList"></spring:message>
<spring:message var="Current_process" code="Current_process"></spring:message>
<spring:message var="Basic_stock" code="Basic_stock"></spring:message>
<spring:message var="Stock_number" code="Stock_number"></spring:message>
<spring:message var="Material_warehouse" code="Material_warehouse"></spring:message>
<spring:message var="Current_Inventory" code="Current_Inventory"></spring:message>
<spring:message var="Current_equipment" code="Current_equipment"></spring:message>
<spring:message var="mat_lot" code="mat_lot"></spring:message>
<spring:message var="Finished_number" code="Finished_number"></spring:message>
<spring:message var="moving_Machine" code="moving_Machine"></spring:message>
<spring:message var="operation_move" code="operation_move"></spring:message>
<spring:message var="new_lotNo" code="new_lotNo"></spring:message>
<spring:message var="total_amount" code="total_amount"></spring:message>
<spring:message var="Inventory_Master" code="Inventory_Master"></spring:message>
<spring:message var="part_Cyle" code="part_Cyle"></spring:message>
<spring:message var="check_ty" code="check_ty"></spring:message>
<spring:message var="reporter" code="reporter"></spring:message>
<spring:message var="part" code="part"></spring:message>
<spring:message var="divide_situ" code="divide_situ"></spring:message>
<spring:message var="cause" code="cause"></spring:message>
<spring:message var="situ" code="situ"></spring:message>
<spring:message var="gch_ty" code="gch_ty"></spring:message>
<spring:message var="gch" code="gch"></spring:message>
<spring:message var="action" code="action"></spring:message>
<spring:message var="operationStatusRank" code="operationStatusRank"></spring:message>
<spring:message var="machinePerformance" code="machinePerformance"></spring:message>

<spring:message var="Movetype" code="Movetype"></spring:message>
<spring:message var="Movestock" code="Movestock"></spring:message>
<spring:message var="Currentwarehouse" code="Currentwarehouse"></spring:message>
<spring:message var="Movingwarehouse" code="Movingwarehouse"></spring:message>
<spring:message var="totalCount" code="totalCount"></spring:message>
<spring:message var="exportdocument" code="exportdocument"></spring:message>
<spring:message var="Exportnumber" code="Exportnumber"></spring:message>
<spring:message var="ExportManagement" code="ExportManagement"></spring:message>
<spring:message var="Importmanagement" code="Importmanagement"></spring:message>
<spring:message var="Referencequantity" code="Referencequantity"></spring:message>
<spring:message var="Shipment" code="Shipment"></spring:message>
<spring:message var="Historysearch" code="Historysearch"></spring:message>

<spring:message var="check_type" code="check_type"></spring:message>
<spring:message var="character_type" code="character_type"></spring:message>
<spring:message var="character_cd" code="character_cd"></spring:message>
<spring:message var="character_name" code="character_name"></spring:message>
<spring:message var="dp" code="dp"></spring:message>
<spring:message var="unit" code="unit"></spring:message>
<spring:message var="drawing" code="drawing"></spring:message>
<spring:message var="target_val" code="target_val"></spring:message>
<spring:message var="min_val" code="min_val"></spring:message>
<spring:message var="max_val" code="max_val"></spring:message>
<spring:message var="measurer" code="measurer"></spring:message>
<spring:message var="js_group_cd" code="js_group_cd"></spring:message>
<spring:message var="character_name" code="character_name"></spring:message>
<spring:message var="Split_work" code="Split_work"></spring:message>
<spring:message var="total_status" code="total_status"></spring:message>
<spring:message var="Base_date" code="Base_date"></spring:message>
<spring:message var="days" code="days"></spring:message>
<spring:message var="months" code="months"></spring:message>
<spring:message var="Final_goal" code="Final_goal"></spring:message>
<spring:message var="Production_Quantity2" code="Production_Quantity2"></spring:message>
<spring:message var="prediction" code="prediction"></spring:message>

<spring:message var="Today" code="Today"></spring:message>
<spring:message var="This_month" code="This_month"></spring:message>
<spring:message var="The_day_before" code="The_day_before"></spring:message>
<spring:message var="Previous_month" code="Previous_month"></spring:message>
<spring:message var="Last_3_months" code="Last_3_months"></spring:message>
<spring:message var="Last_30_days" code="Last_30_days"></spring:message>
<spring:message var="material" code="material"></spring:message>
<spring:message var="standard2" code="standard2"></spring:message>
<spring:message var="below_standard" code="below_standard"></spring:message>

<spring:message var="Manage_alarm_actions" code="Manage_alarm_actions"></spring:message>
<spring:message var="Except_alarm" code="Except_alarm"></spring:message>
<spring:message var="By_alarm" code="By_alarm"></spring:message>
<spring:message var="Alarm_distribution" code="Alarm_distribution"></spring:message>
<spring:message var="Excluded_included" code="Excluded_included"></spring:message>
<spring:message var="Action_Result" code="Action_Result"></spring:message>
<spring:message var="Action_date" code="Action_date"></spring:message>
<spring:message var="bigo" code="bigo"></spring:message>




<spring:message var="Manage_alarm_actions" code="Manage_alarm_actions"></spring:message>
<spring:message var="Except_alarm" code="Except_alarm"></spring:message>
<spring:message var="By_alarm" code="By_alarm"></spring:message>
<spring:message var="Alarm_distribution" code="Alarm_distribution"></spring:message>
<spring:message var="Excluded_included" code="Excluded_included"></spring:message>
<spring:message var="Action_Result" code="Action_Result"></spring:message>
<spring:message var="Action_date" code="Action_date"></spring:message>
<spring:message var="bigo" code="bigo"></spring:message>


<spring:message var="comname" code="comname"></spring:message>
<spring:message var="facName" code="facName"></spring:message>

<spring:message var="total_status" code="total_status"></spring:message>
<spring:message var="product_rank" code="product_rank"></spring:message>
<spring:message var="Manage_alarm_actions" code="Manage_alarm_actions"></spring:message>
<spring:message var="reoperation_cycle" code="reoperation_cycle"></spring:message>
<spring:message var="operationStatusRank" code="operationStatusRank"></spring:message>
<spring:message var="defectReport" code="defectReport"></spring:message>

<spring:message var="fileUpDown" code="fileUpDown"></spring:message>


<spring:message var="add_faulty" code="add_faulty"></spring:message>
<spring:message var="add_faulty_hist" code="add_faulty_hist"></spring:message>

<spring:message var="tool_manage" code="tool_manage"></spring:message>

<spring:message var="add_prdct_target" code="add_prdct_target"></spring:message>

<spring:message var="alarm_manage" code="alarm_manage"></spring:message>
<spring:message var="machine_list" code="machine_list"></spring:message>

<spring:message var="ProductionProcessManagement" code="ProductionProcessManagement"></spring:message>






<script>
const layout = "${layout}"
	const devicestatus = "${devicestatus}"
	const barchart = "${barchart}"
	const prdct_board = "${prdct_board}"
	
	const operation_chart = "${operation_chart}"
	const operation_graph_daily = "${operation_graph_daily}"
	
	const fileUpDown = "${fileUpDown}"
	
	const total_status = "${total_status}"
	const product_rank = "${product_rank}"
	const Manage_alarm_actions = "${Manage_alarm_actions}"
	const reoperation_cycle = "${reoperation_cycle}"
	const operationStatusRank = "${operationStatusRank}"
	const defectReport = "${defectReport}"

	const add_faulty = "${add_faulty}"
	const add_faulty_hist = "${add_faulty_hist}"

	const tool_manage = "${tool_manage}"

	const add_prdct_target = "${add_prdct_target}"

	const alarm_manage = "${alarm_manage}"
	const machine_list = "${machine_list}"
		
	const ProductionProcessManagement = "${ProductionProcessManagement}"
	const facName= "${facName}"
	const comname = "${comname}"
	
		const total = "${total}"
</script>




<%--  공통 context 부분  --%>
<c:set var="ctxPath" value="${pageContext.request.contextPath}" scope="request"/> 
<c:set var="newline" value="<%= \"\n\" %>" />

	<link rel="stylesheet" href="${ctxPath }/css/jquery-ui.css" />
    <link rel="stylesheet" href="${ctxPath }/css/loading.min.css" />
	<link rel="stylesheet" href="${ctxPath }/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.1026/styles/kendo.common.min.css">
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.1026/styles/kendo.silver.min.css"/>
	
	<script src="${ctxPath }/js/jquery.js"></script>
	<script src="${ctxPath }/js/jquery-ui.min.js"></script>
	<script src="${ctxPath }/js/jquery.loading.min.js"></script>
	<script src="${ctxPath }/js/moment.js"></script>
	
<!-- 	<link rel="stylesheet" type="text/css" href="/include/css/ui-lightness/jquery-ui-1.8.16.custom.css" /> -->
	<link rel="stylesheet" type="text/css" href="${ctxPath }/css/ui.jqgrid.css" />
	<!-- 순서가 아주 중요함 -->﻿
<%-- 	<script type="text/javascript" src="${ctxPath }/css/jquery.min.js"></script>
	
	<script type="text/javascript" src="${ctxPath }/js/jquery-ui-1.8.16.custom.min.js"></script>
	<script type="text/javascript" src="${ctxPath }/js/jquery.ui.datepicker-ko.js"></script> --%>
	
	<script type="text/javascript" src="${ctxPath }/js/jquery.jqGrid.min.js"></script>
	<script type="text/javascript" src="${ctxPath }/js/grid.celledit.js"></script>
	<script type="text/javascript" src="${ctxPath }/js/grid.formedit.js"></script>
	
<%-- 	<script type="text/javascript" src="${ctxPath }/js/jquery.jqGrid.js"></script> --%>
	<script type="text/javascript" src="${ctxPath }/js/grid.locale-kr.js"></script>﻿
	<script type="text/javascript" src="${ctxPath }/js/jquery.tablednd.js"></script>﻿
	<script type="text/javascript" src="${ctxPath }/js/jquery.searchFilter.js"></script>﻿
	
	

	<script src="https://kendo.cdn.telerik.com/2017.3.1026/js/kendo.all.min.js"></script>
	
	
<script type="text/x-kendo-template" id="windowTemplate">
	<center>
    	<button class="k-button" id="yesButton">확인</button>
    	<button class="k-button" id="noButton">취소</button>
	</center>
</script>
<script>
	jQuery.browser = {};
	(function () {
	    jQuery.browser.msie = false;
	    jQuery.browser.version = 0;
	    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
	        jQuery.browser.msie = true;
	        jQuery.browser.version = RegExp.$1;
	    }
	})();

	$.datepicker.setDefaults({
	    dateFormat: 'yy-mm-dd',
	    prevText: '이전 달',
	    nextText: '다음 달',
	    monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
	    monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
	    dayNames: ['일', '월', '화', '수', '목', '금', '토'],
	    dayNamesShort: ['일', '월', '화', '수', '목', '금', '토'],
	    dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
	    showMonthAfterYear: true,
	    yearSuffix: '년'
	});
	
	var shopId = 1;
	var ctxPath = "${ctxPath}";
	
	function chkLang(){
		var str = $("#time").html();
		var check = /[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]/;
		if(check.test(str)){
			return "ko";
		}else{
			return "other";
		}
	};
	
	function _navigator(){
		var url = $(this).attr("url");
		console.log($(this))
		location.href = ctxPath + url;
	}; 

	/* var login = window.sessionStorage.getItem("login");
	var login_time = window.sessionStorage.getItem("login_time");
	var time = new Date().getTime();
	if(login==null || (login !=null && (time - login_time) / 1000 > 60*20)) {
		if(window.location.pathname.indexOf("/chart/index.do")==-1){
			alert("세션이 만료 되었습니다.");
			location.href = "${ctxPath}/chart/index.do";			
		}
	} */
</script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>

	<%-- <script type="text/javascript" src="${ctxPath }/js/default.js"></script> --%>

 <script type="text/javascript" src="/iDOO/js/default_std.js"></script> 

<style>
body {
	color: white;
}
#logoutBtn {
	-webkit-transition-duration: 0.4s; /* Safari */
	transition-duration: 0.4s;
	background: #C41C00;
	color: black;
	border: 1px solid black;
	border-radius: 5px;
}
#logoutBtn:hover {
	-webkit-transition-duration: 0.4s; /* Safari */
	transition-duration: 0.4s;
	background: red;
	color: white;
}


.k-grid tbody > tr{
 background : #c1c1c2;
 text-align: center;
 border-color:black;
}
.k-grid tbody > .k-alt
{
 background :  #d5d5d6 ;
 text-align: center;
 border-color:black;
}

/*

.k-grid tbody > tr:hover
{
 background :  darkgray ;
}
 
.k-grid tbody > .k-alt:hover
{
 background :  darkgray ;
 text-align: center;
}
.k-grid-content{
	background: darkgray ;
}


.k-link{
	color:white !important;
}
*/

.k-grid-filter.k-state-active{
	background-color:transparent;
	color: yellow !important;
}
.k-grouping-row{
	    text-align: left !important;
}
.k-grid .k-grouping-row td, .k-grid .k-hierarchy-cell{
	/* background: linear-gradient( to bottom, gray, black ) !important; */ 
}
td.k-group-cell{
	background:dimgray;
}
.k-group-footer td{
	background:dimgray;
}
.k-grid td{
	color : black ;
}
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
    /* display: none; <- Crashes Chrome on hover */
    -webkit-appearance: none;
    margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
}

#grid tbody tr .table-cell{
	background-color: #F6F6F6;
	color: black;
	border-color:black;
}
#grid tbody tr.k-alt .table-cell{
	background-color: #EAEAEA;
	color: black;
	border-color:black;
}


#wrapper tbody tr .table-cell{
	background-color: #F6F6F6;
	color: black;
	border-color:black;
}
#wrapper tbody tr.k-alt .table-cell{
	background-color: #EAEAEA;
	color: black;
	border-color:black;
}

#toolGrid tbody tr .table-cell{
	background-color: #F6F6F6;
	color: black;
	border-color:black;
}
#toolGrid tbody tr.k-alt .table-cell{
	background-color: #EAEAEA;
	color: black;
	border-color:black;
}

.k-grid thead tr th{
/*	background : linear-gradient( to bottom, gray, black ); */
	background : #2c2c36;
	color: white;
	text-align: center;
	border-color:black;
}

.k-grid-header{
border-color:black;
}

.k-grid tbody td{
border-color:black;
}

.k-grid thead tr.k-filter-row th{
/*	background : linear-gradient( to bottom, #6696ff, #6696ff ); */
/*	background : linear-gradient( to bottom, #E8D9FF, #A566FF ); */
	background : #1a1a1a;
	color: white;
	text-align: center;
	border-color:black;
}




/********** 스크롤바 **********/

			::-webkit-scrollbar {
		  		width: 20.88888888888889px;
		  		height: 0px;
			}

			::-webkit-scrollbar-track-piece {
		  		background-color:rgba(0,0,0,0);
			}

			::-webkit-scrollbar-thumb {
				background: url(https://www.digitaltwincloud.com:7443/lib/idoo_dark/imgs/default/btn_scroll_bar_default.svg) center 50%;
			}
			
			::-webkit-scrollbar-thumb:ACTIVE {
				background: url(https://www.digitaltwincloud.com:7443/lib/idoo_dark/imgs/default/btn_scroll_bar_pushed.svg) center 50%;
			}
			

			::-webkit-scrollbar-button:start:decrement {
			 	display: block;
				height: 20.88888888888889px;
		    	background: url(https://www.digitaltwincloud.com:7443/lib/idoo_dark/imgs/default/btn_scroll_up_default.svg) ;
		    	background-size: 100% 100%
			}
			
			::-webkit-scrollbar-button:start:decrement:ACTIVE{
				display: block;
				height: 20.88888888888889px;
		    	background: url(https://www.digitaltwincloud.com:7443/lib/idoo_dark/imgs/default/btn_scroll_up_pushed.svg) ;
		    	background-size: 100% 100%
			}
			
			::-webkit-scrollbar-button:end:increment {
				display: block;
				height: 20.88888888888889px;
		    	background: url(https://www.digitaltwincloud.com:7443/lib/idoo_dark/imgs/default/btn_scroll_down_default.svg) ;
		    	background-size: 100% 100%
		 	}
		 	
		 	::-webkit-scrollbar-button:end:increment:ACTIVE {
				display: block;
				height: 20.88888888888889px;
		    	background: url(https://www.digitaltwincloud.com:7443/lib/idoo_dark/imgs/default/btn_scroll_down_pushed.svg) ;
		    	background-size: 100% 100%
		 	}

			.k-auto-scrollable, .k-grid-header {
		 		background-color : rgb(38,39,43);
		 		background : rgb(38,39,43);
		 		
		 	}
			
		





</style>