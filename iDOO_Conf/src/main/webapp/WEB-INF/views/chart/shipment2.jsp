<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden; 
	background-color: black;
  	font-family:'Helvetica';
}
html{
	overflow : hidden;
}

#grid{
	border-color : #222327;
}

#grid .k-grid-header {
	border-color : #222327;
}

.k-grid-header-wrap {
	border-color : #222327;
}

</style> 
<script type="text/javascript">
	const loadPage = () =>{
		createMenuTree("im", "shipment2")	
	}
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};
	
	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

//	var handle = 0;
	
	$(function(){
//		createNav("inven_nav", 6);
		
//		setEl();
//		time();

		setEl2();
		
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
/* 	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	}; */
	
/* 	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : contentWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#banner").css({
			"font-size" : getElSize(80),
			"width" : getElSize(2000)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
	}; */
	
	
	function setEl2(){
		var width = window.innerWidth;
		var height = window.innerHeight;
/* 		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		}) */

		
		$("#container").css({
			"width" : contentWidth - getElSize(30)
		})
			
		$("#content_table").css({
			"margin-top" : getElSize(300)
		})
			
			
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight,
			"margin-top" : getElSize(250)
		});
		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
/* 		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		}); */
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(22),
			"margin-right" : getElSize(22)
		});
		
		$("select").css({
			"width" : getElSize(380),
			"margin-right" : getElSize(50)
		});
		
		$("input").css({
			"background-color" : "black",
			"border-color" : "black",
			"margin-right" : getElSize(50)
		})
		
			
		$("#td_first").css({
			"padding-left" : getElSize(50),
			"padding-bottom" : getElSize(10)
		});
		
		$(".k-header").css({
		    "font-family" : "NotoSansCJKkrBold",
		    "font-size" : getElSize(32) + "px",
		    "background-image" : "none",
		    "background-color" : "#353542"
		}); 
		
		$("#grid").css({
			"margin-left" : getElSize(50),
			"margin-right" : getElSize(50)
		})
		
		$("button").css({
			"padding" : getElSize(25),
			"margin-left" : getElSize(7),
			"margin-right" : getElSize(30),
			"margin-top" : getElSize(30),
			"font-size" : getElSize(47),
			"background" : "#909090",
			"border-color" : "#222327"
		})
		
		$("#grid").css({
			"margin-top" : getElSize(10)
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(200),
			"margin-right" : getElSize(80)
// 			"height" : getElSize(82)
		});
		
		$("#banner").css({
			"font-size" : getElSize(80),
			"width" : getElSize(2000)
		});

	};
	
	
	
/* 	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	}; */
	
	function readOnly(container, options){
		container.removeClass("k-edit-cell");
        container.text(options.model.get(options.field));
	}
	
	function checkMstmat(container, options){
		$('<input name="' + options.field + '" />')
		 .appendTo(container)
		 .kendoDropDownList({
			 valuePrimitive: true,
			 autoWidth: true,
			 height: getElSize(1300),
			 dataTextField : "matNo",
			 dataValueField  : "matNo",
			 dataSource: itemlist,
			 change: function(e){
				 var prdNo
            	 for(i=0,len=itemlist.length; i<len; i++){
            		if(options.model.item==itemlist[i].matNo){
            			if(itemlist[i].prdNo!=""){
                			prdNo=itemlist[i].prdNo
            			}else{
            				prdNo=itemlist[i].matNo
            			}
            			break;
            		}
            	 }
				console.log(prdNo)
				var url = "${ctxPath}/chart/stockTotalCntCheck.do";
				var param = "prdNo=" + prdNo +
							"&proj=" + "0005"
//							"&proj=" + options.model.proj
				
				var str;
				$.ajax({
					url : url,
					data : param,
					async :false,
					type : "post",
					dataType : "json",
					success : function(data){
						if(data.dataList.length!=0){
							options.model.set("exCnt",data.dataList[0].cnt)
						}else{
							options.model.set("exCnt",0)
						}
					}
				});
			 }
		 }).data("kendoDropDownList");

	}
	function projView(proj){
		if(proj=="0090"){
			return "완성창고"
		}else{
			return proj
		}
	}
	function checkRow(e){
		console.log("event click")
		var grid = $("#grid").data("kendoGrid");
		var gridList=grid.dataSource.data();
		var dataItem = $(e).closest("tr")[0].dataset.uid;
		 var row = $("#grid").data("kendoGrid")
         .tbody
         .find("tr[data-uid='" + dataItem + "']");
		 var initData = grid.dataItem(row)
		 var length = gridList.length
		 
		 if(initData.checkSelect){
			 initData.checkSelect=false;
			 grid.dataSource.fetch();
		 }else{
			 initData.checkSelect=true;
			 grid.dataSource.fetch();
		 }
		 
		 //grid.dataSource.fetch()
	}
	function checkAll(e){
		console.log($("#checkall").is(":checked"))
		if($("#checkall").is(":checked")){
			gridlist=grid.dataSource.data();
        	$(gridlist).each(function(idx,data){
        		data.checkSelect = true;
        	})
		}else{
			gridlist=grid.dataSource.data();
        	$(gridlist).each(function(idx,data){
        		data.checkSelect = false;
        	})
		}
		grid.dataSource.fetch();
	}
	
	function insertRow(){
/* 		 grid.dataSource.add({
	         "dvcId" : 0
	     });   */
	     grid.dataSource.insert(0, {
	    	checkSelect : true,
			insertdata : "insert",
			proj: "0090",
			date: "미저장 데이터",
			vndNo : 0
		 })
	}
	
	function saveRow(){
		var gridlist = grid.dataSource.data()
		var savelist=[]
		console.log(gridlist)
		for(i=0,length=gridlist.length; i<length; i++){
			var sumCnt=0;
			if(gridlist[i].checkSelect==true && gridlist[i].insertdata=="insert"){
				if(gridlist[i].item==undefined){
					alert("완성품번을 선택해주세요");
					return false;
				}else if(gridlist[i].shipLot==undefined){
					alert("출하로트를 입력해주세요");
					return false;
				}else if(gridlist[i].cnt==undefined){
					alert("등록수량을 입력해주세요")
					return false;
				}else if(gridlist[i].exCnt<gridlist[i].cnt){
					alert("등록수량을 확인해주세요")
					return false;
				}
				
				for(j=0,len=itemlist.length; j<len; j++){
					if(gridlist[i].item==itemlist[j].matNo){
						gridlist[i].prdNo=itemlist[j].prdNo
						gridlist[i].prce=itemlist[j].prce
					}
				}
				for(j=0,len=gridlist.length; j<len; j++){
					if(gridlist[j].prdNo==gridlist[i].prdNo && gridlist[j].insertdata=="insert"){
						sumCnt+=Number(gridlist[j].cnt)
					}
				}
				console.log(sumCnt)
				console.log(gridlist[i])
				if(gridlist[i].exCnt < sumCnt){
					alert("재고보다 이동수량이 많을수 없습니다.")
					return false;
				}
				
				savelist.push(gridlist[i])
			}
		}

		if(savelist.length==0){
			alert("저장할 항목을 선택하세요")
			return false;
		}
		
		var obj = new Object();
		obj.val = savelist;
		
		var param = JSON.stringify(obj);
		
		console.log(param)
		var url = "${ctxPath}/chart/shipMoveTrans.do"
		$.showLoading()
		$.ajax({
			url : url,
			data :"val=" + param,
			type : "post",
			dataType : "text",
			success : function(data){
				kendo.alert(savelist.length + "개의 List 저장");
				getTable();
			}
		})
		
	}
	
	function shipSave(){
		var gridlist = grid.dataSource.data()
		var shiplist=[]

		for(i=0,length=gridlist.length; i<length; i++){
			if(gridlist[i].checkSelect==true && gridlist[i].insertdata!="insert"){
				if(gridlist[i].sendCnt==undefined){
					alert("출하수량을 입력해주세요");
					return false;
				}else if(gridlist[i].sendCnt > gridlist[i].stock){
					alert("출하수량이 남은수량보다 많을수없습니다.")
					return false;
				}
				
				for(j=0,len=itemlist.length; j<len; j++){
					if(gridlist[i].item==itemlist[j].matNo){
						gridlist[i].prdNo=itemlist[j].prdNo
						gridlist[i].prce=itemlist[j].prce
					}
				}
				shiplist.push(gridlist[i])
			}
		}

		if(shiplist.length==0){
			alert("출하 항목을 선택하세요")
			return false;
		}
		
		var obj = new Object();
		obj.val = shiplist;
		
		var param = JSON.stringify(obj);
		
		console.log(param)
		var url = "${ctxPath}/chart/finishMoveTrans.do"
		$.showLoading()
		$.ajax({
			url : url,
			data :"val=" + param,
			type : "post",
			dataType : "text",
			success : function(data){
				if(data=="success"){
					kendo.alert(shiplist.length + "개의 List 저장");
					getTable();
				}else{
					kendo.alert("저장에 실패했습니다.");
					getTable();
				}
			}
		})
		
	}
	
	var kendotable
	var grid
	var itemlist=[];
	$(document).ready(function(){
		kendotable = $("#grid").kendoGrid({
			dataBound : function(e){
				var grid=$("#grid").data("kendoGrid")
				$(".k-grid-content table tbody tr td input").css({
					"height":getElSize(55)
					,"width":getElSize(150)
					,"margin":0
					,"padding":0
				})
				this.tbody.find('tr').each(function() {
			    	var item = grid.dataItem(this);
		    		asd = $(this).context.children
			    	if(item.date=="미저장 데이터"){
			    		$(this).context.children
			    		console.log($(this).context.children)
			    		$(this).context.children[1].className="table-cell"
			    		$(this).context.children[3].className="table-cell"
			    		$(this).context.children[5].className="table-cell"
			    		$(this).context.children[7].className=""
/* 			    		$(this).context.children[2].className="table-cell"
			    		$(this).context.children[4].className="table-cell"
			    		$(this).context.children[5].className="table-cell"
			    		$(this).context.children[6].className="table-cell"
			    		$(this).context.children[8].className="" */
			    	}else{
			    		
			    	}
			    })
			    
			}
			,height:getElSize(1600)
			,editable:true
			,columns:[
			{
				field:"checker"
				,title:"<input type='checkbox' id='checkall' onclick='checkAll(this)' style='text-align:center; width: "+getElSize(50)+"; height: "+getElSize(80)+";'>"
				,template: "<input type='checkbox' onclick='checkRow(this)' #= (typeof checkSelect!='undefined' && checkSelect!=false)? 'checked=checked' : '' # #= (typeof newRow!='undefined') ? \'disabled=checked\' : '' # class='checkbox' style='text-align:center; width: "+getElSize(50)+"; height: "+getElSize(80)+";'/>" 
				,width : getElSize(120)
				,style: "text-align: center; font-size:" + getElSize(35)
    				
			},{
				title :"${Finished_number} "
				,field :"item"
				,width : getElSize(400)
				,editable: function (dataItem) {
					return dataItem.date === "미저장 데이터";
		        }
				,editor : checkMstmat
				,attributes: {
    				style: "text-align: center; font-size:" + getElSize(35)
    			},headerAttributes: {
    				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(44)
    			}	
			},{
				title :"${Current_process}"
				,field :"proj"
				,template : "#=projView(proj)#"
				,width : getElSize(250)
				,attributes: {
    				style: "text-align: center; font-size:" + getElSize(35)
    			},headerAttributes: {
    				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(44)
    			}	
			},{
				title :"${ship_lot_no} "
				,field :"shipLot"
				,editable: function (dataItem) {
					return dataItem.date === "미저장 데이터";
		        }
				,width : getElSize(250)
				,attributes: {
    				style: "text-align: center; font-size:" + getElSize(35)
    			},headerAttributes: {
    				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(44)
    			}	
			},{
				title :"(${Referencequantity})"
				,field:"exCnt"
				,editor : readOnly
				,width : getElSize(300)
				,attributes: {
    				style: "text-align: center; font-size:" + getElSize(35)
    			},headerAttributes: {
    				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(44)
    			}	
			}, {
				title :"${complete_stock} *"
				,field :"cnt"
				,width : getElSize(250)
				,editable: function (dataItem) {
					return dataItem.date === "미저장 데이터";
		        },attributes: {
    				style: "text-align: center; font-size:" + getElSize(35)
    			},headerAttributes: {
    				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(44)
    			}	
/* 				,attributes: {
				      "class": "table-cell"
				 } */
			},{
				title :"${stock_cnt}"
				,field :"stock"
				,width : getElSize(250)
				,attributes: {
    				style: "text-align: center; font-size:" + getElSize(35)
    			},headerAttributes: {
    				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
    			}	
			},{
				title :"${ship_cnt} *"
				,field :"sendCnt"
				,width : getElSize(250)
				,editable: function (dataItem) {
					return dataItem.date !== "미저장 데이터";
		        }
			,attributes: {
				style: "text-align: center; font-size:" + getElSize(35)
			},headerAttributes: {
				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(44)
			}	
			},{
				title :"${reg_time}"
				,field :"date"
				,width : getElSize(500)
				,attributes: {
    				style: "text-align: center; font-size:" + getElSize(35)
    			},headerAttributes: {
    				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(44)
    			}	
			}]
		}).data("kendoGrid")
		
		getItem();
		getTable();
	})
	
	function getItem(){
		var url = "${ctxPath}/chart/getItem.do";
		var param; /* = "prdNo=" + $("#group").val() + 
					"&lotNo=" + $("#transText").val(); */

//		$("#checkall")[0].checked=false;
		var json;
		$.showLoading();
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				itemlist = json;
				console.log(json)
			}
		})
	}
	
	function getTable(){
		var url = "${ctxPath}/chart/getShipList.do";
		var param; /* = "prdNo=" + $("#group").val() + 
					"&lotNo=" + $("#transText").val(); */

//		$("#checkall")[0].checked=false;
		var json;
		$.showLoading();
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				kendodata = new kendo.data.DataSource({
					data: json,
					batch: true,
		/* 			group: { field: "prdNo" },
					sort: [{
		            	field: "prdNo" , dir:"asc" 
		            },{
		            	field: "item" , dir:"asc"
		            }], */
					height: 500,
					schema: {
						model: {
							id: "id",
							fields: {
								insertdata : {editable : true}
								,checker : {editable : false}
								,proj : {editable : false}
								,date : {editable : false}
								,item : {editable : false}
								,shipLot : {editable : true}
								,stock : {editable : false}
								,exCnt : {editable : true ,type:"number"}
								,cnt : {editable : true ,type:"number"}
								,sendCnt : {editable : true ,type:"number"}
								,prdNo: { editable: false },
								proj: { editable: false },
								vndNo: {editable: true,
									field : 'vndNo',
									defaultValue: function(e){
										return "0001"
									}
								},
								item: { editable: true,
									field : "item",
									defaultValue: "FS_RR_LH"
								},
								sendCnt: { editable: true ,type: 'number'},
								checker : { editable : false},
								action : { editable : false} 
							}
						}
					}
				}); 
				kendotable.setDataSource(kendodata);
				grid = $("#grid").data("kendoGrid");
				
				$.hideLoading()
				
			}
		})
	}
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	
	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			
			<Tr>
				
				<td rowspan="10" id="svg_td" style="vertical-align: top; position:relative; background:#26282c;">
					<div id="wrapper">
						<table style="width: 100%">
							<Tr>
								<Td id="td_first" style="text-align: center; vertical-align: middle;">
									<button onclick="insertRow()"><spring:message code="add"></spring:message></button>
									<button onclick="saveRow()"><spring:message code="save"></spring:message></button>
									<button onclick="shipSave()"><spring:message code="Shipment"></spring:message></button>
								</td>
							</tr>
							<tr>
								<td>	
									<div id="grid">
									</div>
								</Td>
							</Tr>
						</table>
					</div>
				</td>
			</Tr>

		
			
		</table>
	 </div>
	
</body>
</html>	