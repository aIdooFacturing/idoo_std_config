<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<link rel="stylesheet" href="${ctxPath }/css/jquery.timepicker.min.css">
<link rel="stylesheet" href="${ctxPath }/css/jquery-ui.min.css">
<title>Dash Board</title>
<script type="text/javascript">

const loadPage = () =>{
    /* createMenuTree("maintenance", "Alarm_Manager") */  
    createMenuTree("config", "ETC_Setting") 
}


	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<script type="text/javascript" src="${ctxPath }/js/moment-timezone.min.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery.timepicker.min.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow: hidden;
	background-color: black;
  	/* font-family:'Helvetica'; */
}
#wraper{
	overflow: scroll;
	overflow-x:hidden;
	height : 80%;
}
.mainView{
	overflow: hidden;
	overflow-x:hidden;
}
label{
	color: white;
}
.ui-slider .ui-slider-range {
    position: absolute;
    display: block;
    background: #81B8F3;
}
</style> 
<script type="text/javascript">
	
	$(function(){
		getToday();
		/* createNav("config_nav", 7); */
		
		
		/* if(window.sessionStorage.getItem("level")!='2'){
			alert("권한이 없습니다.");
			window.location.href='/aIdoo/chart/index.do';
		} */
		
		/* setEl(); */
		setEl_dark();
		getComNameInput();
		getComStartTime();
		getUtc();
		var lang = window.localStorage.getItem("lang");
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		addSelectOption();
		chkBanner();
	});
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999,
			"pointer-events": "none"
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$("#table2 td").css({
			"text-align" : "center",
			"color" : "white",
			"font-size" : getElSize(35),
			"padding" : getElSize(20),
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		$("#wraper").css({
			"width" : "100%"
		})
		
		$(".tmpTable").css({
			"width" : "100%"			
		})
		
		$("#wraper").css({
			"height" : getElSize(1750)
		})
		
		$("h1, label").css({
			"color" : "white"
		})
		
		$("#time-set-div").css({
			"height" : getElSize(500)
		})
		
		$("#save-button").css({
			"font-size" : getElSize(100),
			"margin-left" : getElSize(100),
			"width" : getElSize(500),
			"margin-top" : getElSize(100)
		})
		
		$("#return-button").css({
			"font-size" : getElSize(100),
			"margin-right" : getElSize(100),
			"margin-top" : getElSize(100),
			"width" : getElSize(500)
		})
		
		$("#start-time-label").css({
			"font-size" : getElSize(70),
			"margin-left" : getElSize(100)
		})
		
		$("#start-time").css({
			"font-size" : getElSize(70),
			"width" : getElSize(300)
		})
		
		$("#comName-div").css({
			"margin-top" : getElSize(50),
			"font-size" : getElSize(70),
			"margin-left" : getElSize(100)
		})
		
		$("#time-div").css({
			"margin-top" : getElSize(100),
			"font-size" : getElSize(70),
			"margin-left" : getElSize(100),
			"margin-bottom" : getElSize(50),
		})
		
		$("#comName-input").css({
			"font-size" : getElSize(70),
			"width" : getElSize(600)
		})
		
		$("#time-input").css({
			"font-size" : getElSize(70)
		})
		
		$(".action-btn").css({
			"font-size" : getElSize(70),
			"background" : "linear-gradient(#bfbfbf, #9e9e9e)",
			"color" : "rgb(34,34,34)",
			"border" : "1px solid white",
			"margin-left" : getElSize(50),
			"border-radius" : getElSize(10),
			"width" : getElSize(300)
		}).hover(function(){
			$(this).css({
				"color" : "white",
			})
		}, function(){
			$(this).css({
				"color" : "rgb(34,34,34)",
			})
		});
		
		$("h1").css({
			"margin-bottom" : getElSize(50)
		})
		
		$("#utc-select").css({
			"font-size" : getElSize(70)
		})
		
		$(".setting_box").css({
			"border" : getElSize(1)+"px solid white",
			"margin" : getElSize(10),
			"padding" : getElSize(10)
		})
		
		$("#time-range").css({
			"margin-top" : getElSize(50),
			"margin-bottom" : getElSize(20)
		})
		
		$(".inline").css({
			"display" : "inline",
			"color" : "white",
			"font-size" : getElSize(50)
		})
		
		$("#1rotation").css({
	    	"left" : getElSize(200),
	    	"position" : "relative"
		})
		
		$("#2rotation").css({
	    	"left" : getElSize(1050),
	    	"position" : "relative"
		})
		
		$("#3rotation").css({
	    	"left" : getElSize(3050),
	    	"position" : "relative"
		})
		
		$(".slider-time").css({
	    	"left" : getElSize(700),
	    	"position" : "relative",
	    	"color" : "gray"
		})
		
		$(".slider-time2").css({
	    	"left" : getElSize(1400),
	    	"position" : "relative",
	    	"color" : "gray"
		})
		
		$("#startTime").css({
	    	"left" : getElSize(80),
	    	"position" : "relative",
	    	"color" : "gray"
		})
		
		$("#endTime").css({
	    	"left" : getElSize(2000),
	    	"position" : "relative",
	    	"color" : "gray"
		})
		
		$("#btn-div").css({
			"margin-top" : getElSize(100),
			"margin-left" : getElSize(100)
		})
		
		$("#save-btn").css({
			"position" : "absolute",
			"right" : getElSize(150),
		})
		
		$("#rotate-div").css({
			"padding-bottom" : getElSize(19)
		})
		
		$(".title").css({
			"color" : "white",
			"font-size" : getElSize(100)
		})
		
		$(".ui-slider-horizontal").css({
			"height" : getElSize(50)
		})
		
		if(getParameterByName('lang')=='ko' || window.localStorage.getItem("lang")=="ko"){
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(100),
				"position" : "absolute",
				"top" : marginHeight + getElSize(85),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else if(getParameterByName('lang')=='en' || getParameterByName('lang')=='de' || window.localStorage.getItem("lang")=="de" || window.localStorage.getItem("lang")=="en"){
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(70),
				"position" : "absolute",
				"top" : marginHeight + getElSize(115),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else{
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(90),
				"position" : "absolute",
				"top" : marginHeight + getElSize(100),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}
		
	}
	
	function getParameterByName(name) {
	    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	};	// jane
	
	function setEl_dark(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		/* $("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		}) */
		
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999,
			"pointer-events": "none"
		})
		
		/* $("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		}); */
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			/* "top" : getElSize(100) + marginHeight */
			"top" : getElSize(400) + marginHeight	// jane
		});
		
		$("#table2 td").css({
			"text-align" : "center",
			"color" : "white",
			"font-size" : getElSize(35),
			"padding" : getElSize(20),
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		/* $("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		}); */
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		$("#wraper").css({
			"width" : "100%"
		})
		
		$(".tmpTable").css({
			"width" : "100%"			
		})
		
		$("#wraper").css({
			"height" : getElSize(1750)
		})
		
		$("h1, label").css({
			"color" : "white"
		})
		
		$("#time-set-div").css({
			"height" : getElSize(500)
		})
		
		$("#save-button").css({
			"font-size" : getElSize(100),
			"margin-left" : getElSize(100),
			"width" : getElSize(500),
			"margin-top" : getElSize(100)
		})
		
		$("#return-button").css({
			"font-size" : getElSize(100),
			"margin-right" : getElSize(100),
			"margin-top" : getElSize(100),
			"width" : getElSize(500)
		})
		
		$("#start-time-label").css({
			"font-size" : getElSize(70),
			"margin-left" : getElSize(100)
		})
		
		$("#start-time").css({
			"font-size" : getElSize(70),
			"width" : getElSize(300)
		})
		
		$("#comName-div").css({
			"margin-top" : getElSize(50),
			"font-size" : getElSize(70),
			"margin-left" : getElSize(100)
		})
		
		$("#time-div").css({
			"margin-top" : getElSize(100),
			"font-size" : getElSize(70),
			"margin-left" : getElSize(100),
			"margin-bottom" : getElSize(50),
		})
		
		$("#comName-input").css({
			"font-size" : getElSize(70),
			"width" : getElSize(600)
		})
		
		$("#time-input").css({
			"font-size" : getElSize(70)
		})
		
		$(".action-btn").css({
			"font-size" : getElSize(70),
			"background" : "linear-gradient(#bfbfbf, #9e9e9e)",
			"color" : "rgb(34,34,34)",
			"border" : "1px solid white",
			"margin-left" : getElSize(50),
			"border-radius" : getElSize(10),
			"width" : getElSize(300)
		}).hover(function(){
			$(this).css({
				"color" : "white",
			})
		}, function(){
			$(this).css({
				"color" : "rgb(34,34,34)",
			})
		});
		
		$("h1").css({
			"margin-bottom" : getElSize(50)
		})
		
		$("#utc-select").css({
			"font-size" : getElSize(70)
		})
		
		$(".setting_box").css({
			"border" : getElSize(1)+"px solid white",
			"margin" : getElSize(10),
			"padding" : getElSize(10)
		})
		
		$("#time-range").css({
			"margin-top" : getElSize(50),
			"margin-bottom" : getElSize(20)
		})
		
		$(".inline").css({
			"display" : "inline",
			"color" : "white",
			"font-size" : getElSize(50)
		})
		
		$("#1rotation").css({
	    	"left" : getElSize(200),
	    	"position" : "relative"
		})
		
		$("#2rotation").css({
	    	"left" : getElSize(1050),
	    	"position" : "relative"
		})
		
		$("#3rotation").css({
	    	"left" : getElSize(3050),
	    	"position" : "relative"
		})
		
		$(".slider-time").css({
	    	"left" : getElSize(700),
	    	"position" : "relative",
	    	"color" : "gray"
		})
		
		$(".slider-time2").css({
	    	"left" : getElSize(1400),
	    	"position" : "relative",
	    	"color" : "gray"
		})
		
		$("#startTime").css({
	    	"left" : getElSize(80),
	    	"position" : "relative",
	    	"color" : "gray"
		})
		
		$("#endTime").css({
	    	"left" : getElSize(2000),
	    	"position" : "relative",
	    	"color" : "gray"
		})
		
		$("#btn-div").css({
			"margin-top" : getElSize(100),
			"margin-left" : getElSize(100)
		})
		
		$("#save-btn").css({
			"position" : "absolute",
			"right" : getElSize(150),
		})
		
		$("#rotate-div").css({
			"padding-bottom" : getElSize(19)
		})
		
		$(".title").css({
			"color" : "white",
			"font-size" : getElSize(100)
		})
		
		$(".ui-slider-horizontal").css({
			"height" : getElSize(50)
		})
		
		if(getParameterByName('lang')=='ko' || window.localStorage.getItem("lang")=="ko"){
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(100),
				"position" : "absolute",
				"top" : marginHeight + getElSize(85),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else if(getParameterByName('lang')=='en' || getParameterByName('lang')=='de' || window.localStorage.getItem("lang")=="de" || window.localStorage.getItem("lang")=="en"){
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(70),
				"position" : "absolute",
				"top" : marginHeight + getElSize(115),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else{
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(90),
				"position" : "absolute",
				"top" : marginHeight + getElSize(100),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}
		
	}
	
	
	var defaultComName;
	
	function getComNameInput(){
		var url = ctxPath + "/chart/getComName.do";
		var param = "shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				defaultComName=decode(data)
				$("#comName-input").val(defaultComName);
			}
		});
	};
	
	function setComName(){
		
		var r = confirm("명칭을 '" + $("#comName-input").val() + "' 로 바꾸시겠습니까?");
		
		if(r==true){
			var url = ctxPath + "/chart/setComName.do";
			var param = "shopId=" + shopId + 
						"&name=" + $("#comName-input").val();
			
			$.ajax({
				url : url,
				data : param,
				type : "post",
				dataType : "text",
				success : function(data){
					location.reload();
				}
			});
		}else{
			
		}
		
		
	};
	
	function getComStartTime(){
		var url = ctxPath + "/chart/getComStartTime.do";
		var param = "shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				data = data.replace(/\-/gi, ":") + "0"
				
				$("#start-time").val(data)
				
				
				
				var split = data.split(":")
				var division
				if(split[0]>12){
					split[0]=split[0]-12
					division = "PM"
				}else{
					division = "AM"
				}
				
				startHour = split[0]
				
				$("#startTime").text(split[0] + ":" + split[1] +" "+ division)
				$("#endTime").text(split[0] + ":" + split[1] +" "+ division)
				
				var startHour = $("#start-time").val()
				var split = startHour.split(":")
				
				for(var i=0;i<=24;i++){
					
					var hour = Number(split[0])+i
					var minute = split[1]
					if(hour <24){
						
					}else if(hour>=24){
						hour = hour-24
					}
					
					if(hour<10){
						hour = "0" + hour
					}
					
					/* if(i==0){
						$("#legend").append("<label style='position: absolute; left:"+getElSize(620)+"; font-size:"+getElSize(30)+"; color:"+'gray'+" '>"+hour+":"+minute+"</label>")	
					}else{
						$("#legend").append("<label style='position: absolute; left:"+(getElSize(620) + getElSize(i*126))+"; font-size:"+getElSize(30)+"; color:"+'gray'+" '>"+hour+":"+minute+"</label>")
					} */
					
					if(i==0){
						$("#legend").append("<label style='position: absolute; left:"+getElSize(130)+"; font-size:"+getElSize(30)+"; color:"+'gray'+" '>"+hour+":"+minute+"</label>")	
					}else{
						$("#legend").append("<label style='position: absolute; left:"+(getElSize(130) + getElSize(i*146))+"; font-size:"+getElSize(30)+"; color:"+'gray'+" '>"+hour+":"+minute+"</label>")
					}
				}
			}
		});
	};
	
	function setComStartTime(){
		var url = ctxPath + "/chart/setComStartTime.do";
		var param = "shopId=" + shopId +
					"&startTime=" + $("#start-time").val();
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				if(data=='success'){
					alert("저장 되었습니다.")
				}
			}
		});
	};
	
	function returnComName(){
		$("#comName-input").val(defaultComName);
	}
	
	function addSelectOption(){
		for(var i=-12;i<13;i++){
			$('#utc-select').append('<option value=' + i + '>'+ i +':00</option>')
		}
	}
	
	function setUtc(){
		var url = ctxPath + "/chart/setUtc.do";
		var param = "shopId=" + shopId +
					"&utc=" + valueSelected;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				
			}
		});
	}
	
	function getUtc(){
		var url = ctxPath + "/chart/getUtc.do";
		var param = "shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				$("#utc-select").val(data)
				
				setNowTime = setInterval(function(){
					var time = moment().utcOffset(Number(data)).format(moment.defaultFormat);
				    var splitArray = time.split('-');
				    if(splitArray.length<4){
				    	splitArray = time.split('+');
				    	$("#time-input").val(splitArray[0])
				    }else{
				    	$("#time-input").val(splitArray[0]+'-'+splitArray[1]+'-'+splitArray[2])
				    }
				},1000)
				
			}
		});
	}
	
	var valueSelected;
	$(function (){
		
		$('select').on('change', function (e) {
		    var optionSelected = $("option:selected", this);
		    valueSelected = parseInt(this.value);
		    clearInterval(setNowTime);
		    setInterval(function(){
		    	 var time = moment().utcOffset(valueSelected).format(moment.defaultFormat);
				    var splitArray = time.split('-');
				    if(splitArray.length<4){
				    	splitArray = time.split('+');
				    	$("#time-input").val(splitArray[0])
				    }else{
				    	$("#time-input").val(splitArray[0]+'-'+splitArray[1]+'-'+splitArray[2])
				    }
		    },1000)
		});
		
		$('#start-time').timepicker({
			 'disableTextInput': true,
			 'show2400' : true,
			 'timeFormat': 'H:i',
			 'disableTimeRanges': [
			        ['23:31am', '24:01am']
			    ]
		});
		
	})
	
	var rotate1
	var rotate2
	var rotate3
	
	var amountRotate1
	var amountRotate2
	var amountRotate3
	
	
	
	$(function(){
		
		$("#slider-range").slider({
		    range: true,
		    min: 0,
		    max: 1440,
		    step: 15,
		    values: [600, 720],
		    change: function (e, ui) {
		    	var date1 = ui.values[0]
		    	var date2 = ui.values[1] - ui.values[0]
		    	var date3 = 1440 - ui.values[1]
		    	
		    	var date1H = Math.floor((date1)/60)
		    	var date1M = (date1) - (date1H * 60)
		    	rotate1 =  date1H + "시간" + date1M + "분"
		    	amountRotate1 =  (date1H*60) + (date1M)
		    	
		    	var date2H = Math.floor((date2)/60)
		    	var date2M = (date2) - (date2H * 60)
		    	rotate2 =  date2H + "시간" + date2M + "분"
		    	amountRotate2 =  (date2H*60) + (date2M)
		    	
		    	var date3H = Math.floor((date3)/60)
		    	var date3M = (date3) - (date3H * 60)
		    	rotate3 =  date3H + "시간" + date3M + "분"
		    	amountRotate3 =  (date3H*60) + (date3M)
		    	
		    	var hours1
		    	if(ui.values[0]+1080>1440){
		    		hours1 = Math.floor(((ui.values[0]+1080)-1440)/60)
		    	}else{
		    		hours1 = Math.floor((ui.values[0]+1080) / 60);
		    	}
		    	
		    	var minutes1
		    	if(ui.values[0]+1080>1440){
		    		minutes1 = Math.floor((ui.values[0]+1080-1440) - (hours1 * 60))
		    	}else{
		    		minutes1 = Math.floor((ui.values[0]+1080) - (hours1 * 60))
		    	}

		        if (hours1.length == 1) hours1 = '0' + hours1;
		        if (minutes1.length == 1) minutes1 = '0' + minutes1;
		        if (minutes1 == 0) minutes1 = '00';
		        if (hours1 >= 12) {
		            if (hours1 == 12) {
		                hours1 = hours1;
		                minutes1 = minutes1 + " PM";
		            }else if (hours1 == 24) {
		            	console.log("else if")
		                hours1 = 0;
		                minutes1 = "00 AM";
		            } else {
		                hours1 = hours1 - 12;
		                minutes1 = minutes1 + " PM";
		            }
		        } else {
		            hours1 = hours1;
		            minutes1 = minutes1 + " AM";
		        }
		        /* if (hours1 == 0) {
		            hours1 = 12;
		            minutes1 = minutes1;
		        } */



		        $('.slider-time').html(hours1 + ':' + minutes1);
				
		        var hours2
		    	if(ui.values[1]+1080>1440){
		    		hours2 = Math.floor(((ui.values[1]+1080)-1440)/60)
		    	}else{
		    		hours2 = Math.floor((ui.values[1]+1080) / 60);
		    	}
		        
		        var minutes2
		    	if(ui.values[1]+1080>1440){
		    		minutes2 = Math.floor((ui.values[1]+1080-1440) - (hours2 * 60))
		    	}else{
		    		minutes2 = Math.floor((ui.values[1]+1080) - (hours2 * 60))
		    	}

		        if (hours2.length == 1) hours2 = '0' + hours2;
		        if (minutes2.length == 1) minutes2 = '0' + minutes2;
		        if (minutes2 == 0) minutes2 = '00';
		        if (hours2 >= 12) {
		            if (hours2 == 12) {
		                hours2 = hours2;
		                minutes2 = minutes2 + " PM";
		            } else if (hours2 == 24) {
		                hours2 = 0;
		                minutes2 = "00 AM";
		            } else {
		                hours2 = hours2 - 12;
		                minutes2 = minutes2 + " PM";
		            }
		        } else {
		            hours2 = hours2;
		            minutes2 = minutes2 + " AM";
		        }
		        $('.slider-time2').html(hours2 + ':' + minutes2);
		        
		        if($('.slider-time2').text()==$('.slider-time').text()){
		        	$("#2rotation").text(hours2 + ':' + minutes2)
		        	$("#2rotation").css({
		        		"color" : "gray"
		        	})
		        	
		        	$('.slider-time2').css({
		        		"display" : "none"
		        		
		        	})
		        	
		        	$('.slider-time').css({
		        		"display" : "none"
		        	})
		        	
		        	$("#3rotation").text("2교대")
		        	
		        	$("#1rotation").css({
				    	/* "left" : getElSize(500), */
		        		"left" : getElSize(600),	// jane
					})
					
		        	$("#3rotation").css({
				    	/* "left" : getElSize(1800), */
		        		"left" : getElSize(2150)	// jane
					})
					
		        	$("#2rotation").css({
				    	/* "left" : getElSize(1250), */
		        		"left" : getElSize(1520)	// jane
					})
					
					$("#endTime").css({
						/* "left" : getElSize(2350) */
						"left" : getElSize(2900)
					})
		        	
		        }else{
		        	$('.slider-time').css({
		        		"display" : "inline"
		        	})
		        	
		        	$('.slider-time2').css({
		        		"display" : "inline"
		        	})
		        	
		        	$("#2rotation").css({
		        		"color" : "white"
		        	})
		        	$("#2rotation").text("2교대")
		        	$("#3rotation").text("3교대")
		        	
		        	$("#1rotation").css({
				    	"left" : getElSize(400),
					})
					
					$("#2rotation").css({
				    	"left" : getElSize(1050),
					})
					
		        	$("#3rotation").css({
		        		"left" : getElSize(1700),
					})
					
					$("#endTime").css({
						"left" : getElSize(2000)
					})
		        	
					
		        }
		        
		    },slide: function (e, ui) {
		    	var date1 = ui.values[0]
		    	var date2 = ui.values[1] - ui.values[0]
		    	var date3 = 1440 - ui.values[1]
		    	
		    	var date1H = Math.floor((date1)/60)
		    	var date1M = (date1) - (date1H * 60)
		    	rotate1 =  date1H + "시간" + date1M + "분"
		    	
		    	var date2H = Math.floor((date2)/60)
		    	var date2M = (date2) - (date2H * 60)
		    	rotate2 =  date2H + "시간" + date2M + "분"
		    	
		    	var date3H = Math.floor((date3)/60)
		    	var date3M = (date3) - (date3H * 60)
		    	rotate3 =  date3H + "시간" + date3M + "분"
		    	
		    	var hours1
		    	if(ui.values[0]+1080>1440){
		    		hours1 = Math.floor(((ui.values[0]+1080)-1440)/60)
		    	}else{
		    		hours1 = Math.floor((ui.values[0]+1080) / 60);
		    	}
		    	
		    	var minutes1
		    	if(ui.values[0]+1080>1440){
		    		minutes1 = Math.floor((ui.values[0]+1080-1440) - (hours1 * 60))
		    	}else{
		    		minutes1 = Math.floor((ui.values[0]+1080) - (hours1 * 60))
		    	}

		        if (hours1.length == 1) hours1 = '0' + hours1;
		        if (minutes1.length == 1) minutes1 = '0' + minutes1;
		        if (minutes1 == 0) minutes1 = '00';
		        if (hours1 >= 12) {
		            if (hours1 == 12) {
		                hours1 = hours1;
		                minutes1 = minutes1 + " PM";
		            }else if (hours1 == 24) {
		            	console.log("else if")
		                hours1 = 0;
		                minutes1 = "00 AM";
		            } else {
		                hours1 = hours1 - 12;
		                minutes1 = minutes1 + " PM";
		            }
		        } else {
		            hours1 = hours1;
		            minutes1 = minutes1 + " AM";
		        }
		        /* if (hours1 == 0) {
		            hours1 = 12;
		            minutes1 = minutes1;
		        } */



		        $('.slider-time').html(hours1 + ':' + minutes1);
				
		        var hours2
		    	if(ui.values[1]+1080>1440){
		    		hours2 = Math.floor(((ui.values[1]+1080)-1440)/60)
		    	}else{
		    		hours2 = Math.floor((ui.values[1]+1080) / 60);
		    	}
		        
		        var minutes2
		    	if(ui.values[1]+1080>1440){
		    		minutes2 = Math.floor((ui.values[1]+1080-1440) - (hours2 * 60))
		    	}else{
		    		minutes2 = Math.floor((ui.values[1]+1080) - (hours2 * 60))
		    	}

		        if (hours2.length == 1) hours2 = '0' + hours2;
		        if (minutes2.length == 1) minutes2 = '0' + minutes2;
		        if (minutes2 == 0) minutes2 = '00';
		        if (hours2 >= 12) {
		            if (hours2 == 12) {
		                hours2 = hours2;
		                minutes2 = minutes2 + " PM";
		            } else if (hours2 == 24) {
		                hours2 = 0;
		                minutes2 = "00 AM";
		            } else {
		                hours2 = hours2 - 12;
		                minutes2 = minutes2 + " PM";
		            }
		        } else {
		            hours2 = hours2;
		            minutes2 = minutes2 + " AM";
		        }
		        $('.slider-time2').html(hours2 + ':' + minutes2);
		        
		        if($('.slider-time2').text()==$('.slider-time').text()){
		        	$("#2rotation").text(hours2 + ':' + minutes2)
		        	$("#2rotation").css({
		        		"color" : "gray"
		        	})
		        	
		        	$('.slider-time2').css({
		        		"display" : "none"
		        		
		        	})
		        	
		        	$('.slider-time').css({
		        		"display" : "none"
		        	})
		        	
		        	$("#3rotation").text("2교대")
		        	
		        	$("#1rotation").css({
				    	"left" : getElSize(500),
					})
					
		        	$("#3rotation").css({
				    	"left" : getElSize(1800),
					})
					
		        	$("#2rotation").css({
				    	"left" : getElSize(1250),
					})
					
					$("#endTime").css({
						"left" : getElSize(2350)
					})
		        	
		        }else{
		        	$('.slider-time').css({
		        		"display" : "inline"
		        	})
		        	
		        	$('.slider-time2').css({
		        		"display" : "inline"
		        	})
		        	
		        	$("#2rotation").css({
		        		"color" : "white"
		        	})
		        	$("#2rotation").text("2교대")
		        	$("#3rotation").text("3교대")
		        	
		        	$("#1rotation").css({
				    	"left" : getElSize(400),
					})
					
					$("#2rotation").css({
				    	"left" : getElSize(1050),
					})
					
		        	$("#3rotation").css({
		        		"left" : getElSize(1700),
					})
					
					$("#endTime").css({
						"left" : getElSize(2000)
					})
		        	
		        }
		    }
		})
		
		
		$("#slider-range").slider('values',0,600);
		$("#slider-range").slider('values',1,1200);
		
		$("#slider-range").css({
			/* "width" : getElSize(3000), */
			"width" : getElSize(3500),	// jane
			"margin-left" : getElSize(150)
		})
		
		$(".ui-slider-horizontal").css({
			"height" : getElSize(30)
		})
		
	})
	
	function saveRotation(){
		var r
		if($('.slider-time').text()==$('.slider-time2').text()){
			r=confirm("교대시간을 확인하여 주세요."+
					"\n\n 1교대 : " + $("#startTime").text() + " ~ " + $('.slider-time').text() +"  "+ rotate1 + 
					"\n 2교대 : " + $('.slider-time2').text() + " ~ " + $("#endTime").text() +"  "+  rotate3 + 
					"\n\n이 시간으로 저장합니까?"
			)
		}else{
			r=confirm("교대시간을 확인하여 주세요."+
					"\n\n 1교대 : " + $("#startTime").text() + " ~ " + $('.slider-time').text() +"  "+ rotate1 + 
					"\n 2교대 : " + $('.slider-time').text() + " ~ " + $('.slider-time2').text() +"  "+ rotate2 + 
					"\n 3교대 : " + $('.slider-time2').text() + " ~ " + $("#endTime").text() +"  "+  rotate3 + 
					"\n\n이 시간으로 저장합니까?"
			)
		}
		
		
		
		if(r==true){
			
			var divisionArray = new Array();
			var divisionInfo = new Object();
			var hour
			
			divisionInfo.division = 1
				/* if($("#startTime").text().split(" ")[1]=='PM'){
					hour = Number($("#startTime").text().split(" ")[0].split(":")[0])+Number(12)
				}else{
					hour = $("#startTime").text().split(" ")[0].split(":")[0]
				}
			divisionInfo.sTime = hour+":"+$("#startTime").text().split(" ")[0].split(":")[1]
				if( $('.slider-time').text().split(" ")[1]=='PM'){
					hour =  Number($('.slider-time').text().split(" ")[0].split(":")[0])+12
				}else{
					hour =  $('.slider-time').text().split(" ")[0].split(":")[0]
				}
			divisionInfo.eTime = hour+":"+ $('.slider-time').text().split(" ")[0].split(":")[1] */
			divisionInfo.amountMin = amountRotate1
			divisionInfo.shopId = 1
			divisionArray.push(divisionInfo)
			
			var divisionInfo = new Object();
			divisionInfo.division = 2
				/* if($(".slider-time").text().split(" ")[1]=='PM'){
					hour = Number($(".slider-time").text().split(" ")[0].split(":")[0])+Number(12)
				}else{
					hour = $(".slider-time").text().split(" ")[0].split(":")[0]
				}
			divisionInfo.sTime = hour+":"+$(".slider-time").text().split(" ")[0].split(":")[1]
				if( $('.slider-time2').text().split(" ")[1]=='PM'){
					hour =  Number($('.slider-time2').text().split(" ")[0].split(":")[0])+Number(12)
				}else{
					hour = $(".slider-time2").text().split(" ")[0].split(":")[0]
				}
			divisionInfo.eTime = hour+":"+ $('.slider-time2').text().split(" ")[0].split(":")[1] */
			divisionInfo.amountMin = amountRotate2
			divisionInfo.shopId = 1
			divisionArray.push(divisionInfo)
			
			var divisionInfo = new Object();
			divisionInfo.division = 3
				/* if($(".slider-time2").text().split(" ")[1]=='PM'){
					hour = Number($(".slider-time2").text().split(" ")[0].split(":")[0])+12
				}else{
					hour = $(".slider-time2").text().split(" ")[0].split(":")[0]
				}
			divisionInfo.sTime = hour+":"+$(".slider-time2").text().split(" ")[0].split(":")[1]
				if($("#endTime").text().split(" ")[1]=='PM'){
					hour = Number($("#endTime").text().split(" ")[0].split(":")[0])+12
				}else{
					hour = $("#endTime").text().split(" ")[0].split(":")[0]
				}
			divisionInfo.eTime = hour+":"+$("#endTime").text().split(" ")[0].split(":")[1] */
			divisionInfo.amountMin = amountRotate3
			divisionInfo.shopId = 1
			divisionArray.push(divisionInfo)
			
			var jsonInfo = JSON.stringify(divisionArray)
			
			var url = ctxPath + "/chart/setDivision.do";
			var param = "list=" + jsonInfo
			
			console.log(url + "?" + param)
			$.ajax({
				url : url,
				data : param,
				type : "post",
				dataType : "text",
				success : function(data){
					alert("저장 되었습니다.");
					getDivision();
				},error : function(e1,e2,e3){
					console.log(e1)
					console.log(e2)
					console.log(e3)
				}
			});
		}
	}
	
	function resetRotation(){
		
		$("#slider-range").slider('values', 0, defaultMin1);
		$("#slider-range").slider('values', 1, defaultMin2);
		$("#slider-range").slider('values', 2, defaultMin3);
	}
	
	var defaultMin1
	var defaultMin2
	var defaultMin3
	
	function getDivision(){
		
		var url = ctxPath + "/chart/getDivision.do";
		var param = "shopId=" + shopId
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				
				defaultMin1 = data[0].amountMin;
				defaultMin2 = defaultMin1 + data[1].amountMin;
				defaultMin3 = defaultMin2 + data[2].amountMin;
				
				var amount=0;
				for(var i=0;i<data.length;i++){
					amount+=data[i].amountMin;
					$("#slider-range").slider('values',i,amount);
				}
				
			}
		});
		
	}
	
	$(function(){
		getDivision();
	})
	
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	
	<!-- <div id="time"></div> -->
	<div id="title_right"></div>
	
	<div id="dashBoard_title">${otherSetting}</div>
	
	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<%-- <Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/config_left.png" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/sky_right.png" class='menu_right'>
				</td>
			</tr> --%>
			<Tr>
				<%-- <Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td> --%>
				<td class="mainView" rowspan="10" id="svg_td" style="vertical-align: top;">
					<div id="wraper">
						
						<div id="time-set-div" class="setting_box">
							<div class="title"><spring:message  code="statusTime"></spring:message></div>
							<hr>
							<div id="input-set-div">
								<label for="start-time" id="start-time-label"><spring:message  code="startTime"></spring:message> : </label><input id="start-time" type="text">
								<button class="action-btn" id="save-button" onclick="setComStartTime()"><spring:message  code="save"></spring:message></button><!-- <button class="action-btn" id="return-button">되돌리기</button> -->
							</div>
						</div>
						
						<div id="config-div" class="setting_box">
							<div class="title"><spring:message  code="basicSetting"></spring:message></div>
							<hr>
							<div id="comName-div">
								<label for="comName-input" id="comName-label"><spring:message  code="companyName"></spring:message> : </label><input id="comName-input" type="text">
								<button class="action-btn" id="comName-save-button" onclick="setComName()"><spring:message  code="save"></spring:message></button><!-- <button onclick="returnComName()" class="action-btn" id="comName-return-button">되돌리기</button> -->
							</div>
							<div id="time-div"> 
								<label for="time-input" id="time-label"><spring:message  code="time"></spring:message> : </label><input id="time-input" disabled type="datetime-local"> <label for="utc-select"><spring:message  code="timeZone"></spring:message> : </label><select id="utc-select"></select>
								<button class="action-btn" id="time-input-save-button" onclick="setUtc()"><spring:message  code="save"></spring:message></button>
							</div>
						</div>
						
						<div id="rotate-div" class="setting_box">
							<div class="title">교대 시간 설정</div>
							<hr>
							<div class="inline" id="startTime">startTime</div><div class="inline" id="1rotation">1교대</div><div  class="inline slider-time">10:00 AM</div><div class="inline" id="2rotation">2교대</div><div class="inline slider-time2">12:00 PM</div><div class="inline" id="3rotation">3교대</div><div class="inline" id="endTime">endTime</div>
							<div id="time-range">
							    <div class="sliders_step1">
							        <div id="slider-range"></div>
							    </div>
							</div>
							<div id="legend"></div>
							<div id="btn-div">
								<button id="reset-btn" class="action-btn" onclick="resetRotation()"><spring:message  code="returnBtn"></spring:message></button><button class="action-btn" id="save-btn" onclick="saveRotation()"><spring:message  code="save"></spring:message></button>
							</div>
							
						</div>
						
					</div>
				</td>
			</Tr>
			
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	