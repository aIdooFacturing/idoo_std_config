<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden; 
	background-color: black;
}
.k-dialog.k-alert .k-dialog-titlebar{
	display:none;
}
#delete{
	color: white;
    background: red;
    border: 1px solid black;
    border-radius: 5px;
    width: 100%;
    height: 100%;
}

#delete:hover{

}
/* .k-calendar, .k-calendar td
{ 
  background: gray;
}
 */
 .k-calendar .k-link {
    color: black !important;
}
.k-calendar .k-header .k-link {
    color: black;
    background: #D5D5D5;
}
.k-calendar .k-footer .k-link {
    color: black;
    background: #D5D5D5;
}
/*요일 */
.k-calendar th {
  background:#D5D5D5;
  color : black; 
}
/* 몸통 날짜 
.k-calendar, .k-calendar td{
	background : black;
} */
/* 주말 날짜
.k-calendar, .k-calendar td.k-weekend{
	background : red;
}
/* 이전 다음달 날짜 */
.k-calendar, .k-calendar td.k-other-month{
	background : #EAEAEA;
}

.k-footer-template td{
	color: black ;
}
/* .k-calendar th {
  background:gray;
  color : blue; 
} */

#grid{
	background: black;
	border-color : #222327;
}

#wrapper{
	border-color : #222327;
}

.k-grid-header {
	border-color : #222327;
}

.k-grid-header-wrap {
	border-color : #222327;
}

</style> 
<script type="text/javascript">
	
	const loadPage = () =>{
		createMenuTree("im", "transferLine")	
	}
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		$(".date").val(year + "-" + month + "-" + day);
	};
	
	var dvc = [];
	
	function dvcList(){
		var url = "${ctxPath}/common/dvcList.do";
		
		$.ajax({
			url : url,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
				$(json).each(function(idx,data){
					dvc.push(data)
				})
				getLotInfo();
			}
		});
	}
	
	function dvcIdtoName(dvcId){
		for(var i=0;i<dvc.length;i++){
			if(dvc[i].dvcId==dvcId){
				return dvc[i].dvcName
			}
		}
		return dvcId;
	}
	
//	var handle = 0;
	
	var autoSave = true;
	function getDeliverInfo(evt){
		if(evt.keyCode==13){
			$("#transText").focus();
			$("#transText").trigger('click');
			$("#transText").select();
			$("#transText").focus()

			$.showLoading()
			
			var grid = $("#wrapper").data("kendoGrid");
					var rows = grid.dataSource.data();
					
			$(rows).each(function(idx, data){
				var lotNo = data.lotNo;
				
				if(lotNo == $("#transText").val()){
					console.log(data)
					grid.select(data);
					//var tr = $(data).closest("tr");
					//$(tr).children("td:nth(0)").children("input").attr("checked", true);
					//console.log()
					data.checkSelect=true
					data.set("checkSelect", true);
				}
			
			});
			grid.dataSource.fetch()
			saveRow()
			/* getLotInfo();
			
			setTimeout(function() {
				if(autoSave){
					var grid = $("#wrapper").data("kendoGrid");
					var rows = grid.dataSource.data();
					
					$(rows).each(function(idx, data){
						var lotNo = data.groupLotNo;
						
						if(lotNo == $("#transText").val()){
							console.log(data)
							grid.select(data);
							//var tr = $(data).closest("tr");
							//$(tr).children("td:nth(0)").children("input").attr("checked", true);
							//console.log()
							data.checkSelect=true
							data.set("checkSelect", true);
						}
					
					});
					grid.dataSource.fetch()
					

				}else{
					alert("1111")
					getIncomStock()
				}
			}, 1500); */
			
			
			

			
		}
	}
	
	$(function(){
		
		var target_input = $('#keyword'); // 포커스 인풋
	    var chk_short = true;
		 
	    $(document).bind("keydown keyup", function(e) {
	        var key = e.keyCode;
	        var tg = e.target;
	        if(tg.tagName == "INPUT" ||  tg.tagName == "TEXTAREA") return true;
	        
	        var specific = key >= 8 && key <= 46;
	        if(e.type == "keydown") {
	            if(specific) {
	                chk_short = false;
	                return true;
	            }
	            if(!specific && chk_short) {
	            	 $("#transText").focus().select();
	                //target_input.focus().select(); return false;
	            }
	            if(e.ctrlKey && e.keyCode == 86){
	            	 $("#transText").focus().select();
	            }
	        } else {
	            if(specific) {
	                chk_short = true;
	            }
	        }
	    });
	    
	    $("input, textarea").bind("blur", function() {
	        chk_short = true;
	    });
		$("#transText").focus()
		
		finishPrdNo();
		dvcList();
		getPrdNo();

//		createNav("inven_nav", 2);

//		setEl();
		setDate();	
//		time();
		setEvt();

		setEl2();
		
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	var finishPrdNo;
	function finishPrdNo(){
		var url = "${ctxPath}/chart/getbertmstmatno.do";
		var param ;
		var json;
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				finishPrdNo=data.dataList;
				console.log(finishPrdNo)
			}
		});
	}
	function setEvt(){

	};
	
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	
/* 	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
 */	
 
 
 
	/* function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(200),
		});
		
		$("#content_table td").css({
			"color" : "##BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#delDiv").css({
			"position" : "absolute",
			"width" : getElSize(700),
			"height" :getElSize(200),
			"background-color" : "#444444",
			"color" : "white"
		});
		
		$("#delDiv").css({
			"top" : (window.innerHeight/2) - ($("#delDiv").height()/2), 
			"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
			"display" : "none",
			"border-radius" : getElSize(10),
			"padding" : getElSize(20)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
	}; */
	
	
	
	
	function setEl2(){
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		
/* 		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		}) */
		
		
		
		$("#container").css({
			"width" : contentWidth - getElSize(30)
		})
			
		$("#content_table").css({
			"margin-top" : getElSize(280)
		})
		
	
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$("#wrapper").css({
			"margin-left" : getElSize(50),
			"margin-right" : getElSize(50),
			"margin-top" : getElSize(30)
		});

		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		
/* 		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		}); */
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(22),
			"margin-right" : getElSize(22)
		});
		
		$("select").css({
			"width" : getElSize(380),
			"margin-right" : getElSize(50)
		});
		
		$("input").css({
			"background-color" : "black",
			"border-color" : "black",
			"margin-right" : getElSize(50),
			"height" : getElSize(80)
		})
			
		$("#td_first").css({
			"padding-left" : getElSize(50),
			"padding-bottom" : getElSize(10),
			"padding-top" : getElSize(10),
			"vertical-align" : "middle"
		});
		
		$(".k-header").css({
		    "font-family" : "NotoSansCJKkrBold",
		    "font-size" : getElSize(32) + "px",
		    "background-image" : "none",
		    "background-color" : "#353542"
		}); 
		
		$("button").css({
			"padding" : getElSize(25),
			"margin-left" : getElSize(7),
			"margin-right" : getElSize(7),
			"font-size" : getElSize(47),
			"background" : "#909090",
			"border-color" : "#222327"
		})
		
		$("#search").css({
			"padding-bottom" : getElSize(40),
			"cursor" : "pointer",
			"width" : getElSize(200),
			"margin-right" : getElSize(160),
			"height" : getElSize(90),
			"text-align" : "center",
			"background" : "#909090",
			"border-color" : "#222327"
		}); 
		
		$(".fa fa-search").css({
			"padding-bottom" : getElSize(80)
		})
		
		$("#save").css({
			"padding-bottom" : getElSize(40),
			"cursor" : "pointer",
			"width" : getElSize(200),
			"margin-right" : getElSize(160),
			"padding-bottom" : getElSize(20),
			"height" : getElSize(90),
			"text-align" : "center",
			"vertical-align" : "middle",
			"background" : "#909090",
			"border-color" : "#222327"
		});
		
		$("#content_table td").css({
			"color" : "##BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#delDiv").css({
			"position" : "absolute",
			"width" : getElSize(700),
			"height" :getElSize(200),
			"background-color" : "#444444",
			"color" : "white"
		});
		
		$("#delDiv").css({
			"top" : (window.innerHeight/2) - ($("#delDiv").height()/2), 
			"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
			"display" : "none",
			"border-radius" : getElSize(10),
			"padding" : getElSize(20)
		});
		
	};
	
	
	
	
	
/* 	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	}; */
	
	var grid;
	var dialog;
	
	function checkAll(e){
		console.log($("#checkall").is(":checked"))
		if($("#checkall").is(":checked")){
			gridlist=grid.dataSource.data();
        	$(gridlist).each(function(idx,data){
        		data.checkSelect = true;
        	})
		}else{
			gridlist=grid.dataSource.data();
        	$(gridlist).each(function(idx,data){
        		data.checkSelect = false;
        	})
		}
		grid.dataSource.fetch();
	}
	
	$(function(){
		
		dialog = $("#dialog").kendoDialog({
			 actions: [{
		          text: "OK",
		          action: function(e){
		              return false;
		          },
		          primary: true
		      },{
		          text: "Cancel"
		      }],
		      visible: false
		}).data("kendoDialog");
		
		grid = $("#wrapper").kendoGrid({
			columns: [
				{
					field:"checker",
					title:"<input type='checkbox' id='checkall' onclick='checkAll(this)' style='width: "+getElSize(80)+"; height: "+getElSize(96)+";'>"
					,width:getElSize(250),
					template: "<input type='checkbox' onclick='checkRow(this)' #= (typeof checkSelect!='undefined' && checkSelect!=false)? 'checked=checked' : '' # #= (typeof newRow!='undefined') ? \'disabled=checked\' : '' # class='checkbox' style='width: "+getElSize(80)+"; height: "+getElSize(80)+";'/>" 
					,attributes: {
           				style: "text-align: center; font-size:" + getElSize(35)
           			},headerAttributes: {
           				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(37)
           			}						
				},
		        { field: "prdNo",
		          headerTemplate : "${mat_prd_no}",
		          width:getElSize(500),
		          headerAttributes: {
         				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(42)+ "; color:white;"
         		  },
	        	  attributes: {
          				style: "text-align: center; font-size:" + getElSize(39) + "; color:black;"
          		  }
		        },
		        { field: "groupLotNo",
		          headerTemplate : "${mat_lot}",
		          width:getElSize(250),
		          headerAttributes: {
       				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(42)+ "; color:white;"
       		  		},
	        	  attributes: {
        				style: "text-align: center; font-size:" + getElSize(39) + "; color:black;"
        		  }
		        },
		        { field: "lotNo",
			          headerTemplate : "barCode",
			          width:getElSize(250),
			          headerAttributes: {
	       				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(42)+ "; color:white;"
	       		  		},
		        	  attributes: {
	        				style: "text-align: center; font-size:" + getElSize(39) + "; color:black;"
	        		  }
		        },
		        { field: "nowDvcName",
			          headerTemplate : "${Current_equipment}",
			          width:getElSize(250),
			          template : "#=(typeof nowDvcName =='undefined' || nowDvcName==null || nowDvcName==0)? '장비없음' : dvcIdtoName(nowDvcName)#",
			          headerAttributes: {
	         				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(42)+ "; color:white;"
	         		  },
		        	  attributes: {
	          				style: "text-align: center; font-size:" + getElSize(39) + "; color:black;"
	          		  }
		        },
		        { field: "oprNo",
		          headerTemplate : "${current_operation}",
		          width:getElSize(250),
		          headerAttributes: {
       				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(42)+ "; color:white;"
	       		  },
	        	  attributes: {
        				style: "text-align: center; font-size:" + getElSize(39) + "; color:black;"
        		  }
		        },
		        { field: "lotStock",
		          headerTemplate : "${stock}",
		          width:getElSize(250),
		          footerTemplate: "#=footer(data.lotStock)#",
		          headerAttributes: {
       				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(42)+ "; color:white;"
	       		  },
	        	  attributes: {
        				style: "text-align: center; font-size:" + getElSize(39) + "; color:black;"
        		  }
		        },
		        /* { field: "productName",
		          headerTemplate : "${Finished_number}",
		          editor : checkMstmat,
		          width:getElSize(380),
		          headerAttributes: {
       				style: "text-align: center; background-color:black; font-size:" + getElSize(42)+ "; color:white;"
       		  	},
	        	  attributes: {
        				style: "text-align: center; font-size:" + getElSize(39) + "; color:white;"
        		  }
		        },
		        { field: "dvcName",
		          headerTemplate : "${moving_Machine}",
		          editor : getOprList,
		          width: getElSize(250),
		          template : "#=(typeof dvcName =='undefined' || dvcName==null)? '' : dvcIdtoName(dvcName)#",
		          headerAttributes: {
       				style: "text-align: center; background-color:black; font-size:" + getElSize(42)+ "; color:white;"
       		  		},
	        	  attributes: {
        				style: "text-align: center; font-size:" + getElSize(39) + "; color:white;"
        		  }
		        },
		        { field: "movedOprNo",
		          headerTemplate : "${operation_move}",
		          editor:readOnly,
		          width:getElSize(210),
		          headerAttributes: {
         				style: "text-align: center; background-color:black; font-size:" + getElSize(42)+ "; color:white;"
         		  },
	        	  attributes: {
          				style: "text-align: center; font-size:" + getElSize(39) + "; color:white;"
          		  }
		        }, */
		        { 
		          field: "cnt",
		          headerTemplate : "${move_cnt}",
		          width:getElSize(250),
		          footerTemplate: "#=footer(data.cnt)#",
		          headerAttributes: {
         				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(42)+ "; color:white;"
         		  },
	        	  attributes: {
				      "class": "table-cell",
          				style: "text-align: center; font-size:" + getElSize(39) + "; color:black;"
          		  }
		        },{
		        	title :"${reg_time}",
		        	field:"regdate",
	        		width:getElSize(500),
			          headerAttributes: {
	         				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(42)+ "; color:white;"
	         		  },
		        	  attributes: {
	          				style: "text-align: center; font-size:" + getElSize(39) + "; color:black;"
	          		  }
		        }
		       /*  { field: "aftLot",
		          headerTemplate : "${new_lotNo}",
	        	  width:getElSize(239),
		          headerAttributes: {
         				style: "text-align: center; background-color:black; font-size:" + getElSize(42)+ "; color:white;"
         		  },
	        	  attributes: {
          				style: "text-align: center; font-size:" + getElSize(39) + "; color:white;"
          		  }
		        }, */
		        /* { field:"datetime", 
//		          format:"{0:yyyy-MM-dd HH:mm}" ,
		        //  editor: dateTimeEditor,
		          headerTemplate : "${release_date}",
		          width:getElSize(390),
		          headerAttributes: {
         				style: "text-align: center; background-color:black; font-size:" + getElSize(42)+ "; color:white;"
         		  },
	        	  attributes: {
          				style: "text-align: center; font-size:" + getElSize(39) + "; color:white;"
          		  }
		        } *//* ,
		        {
		        	field:"action",	
		        	headerTemplate : "",
		        	template:"#=(typeof action=='undefined')?'<button onclick=insertRow(this)>${divide}</button>':action#",
		        	width:getElSize(180),
					headerAttributes: {
						style: "text-align: center; background-color:black; font-size:" + getElSize(42)+ "; color:white;"
					 },
					attributes: {
							style: "text-align: center; font-size:" + getElSize(39) + "; color:white;"
					  }
		        } */
		    ],
		    editable:true,
		    cellClose:function(e){
		    	console.log(e)
		    	var grid = this;
	      		var fieldName = grid.columns[e.container.index()].field;
	      		if(fieldName=='productName'){
	      			if(e.model.productName=='완성품'){
	      				e.model.set('dvcName', '없음');
		      			e.model.set('movedOprNo', '완성창고');
		      			e.model.set('newOprNo','1000');
	      			}else{
	      				e.model.set('dvcName',null);
		      			e.model.set('movedOprNo',null);
		      			e.model.set('newOprNo','');
	      			}
	      		}
	      		if(fieldName=='dvcName'){
	      			console.log("변경 대기 시점")
	      			var dataSource = dropdownList.dataSource.data();
	      			if(typeof dropdownList.dataSource.data()[dropdownList.select()]=='undefined'){
	      			}else{
	      				var newOprNo = dropdownList.dataSource.data()[dropdownList.select()].oprNo;
		      			e.model.set('newOprNo',newOprNo);
		      			if(newOprNo=='0020'){
		      				e.model.set('movedOprNo', 'MCT삭')
		      			}
		      			else if(newOprNo=='0010'){
		      				e.model.set('movedOprNo', 'R삭')
		      			}
		      			else if(newOprNo=='0030'){
		      				e.model.set('movedOprNo', 'CNC삭')
		      			}
		      			else if(newOprNo=='1000'){
		      				e.model.set('movedOprNo', '완성창고')
		      			}
	      			}
	      		}
				if(e.model.lotStock<e.model.cnt){
					kendo.alert("이동 수량이 재고 수량보다 많습니다.")
					e.model.cnt=0;
				}else{
					gridList=grid.dataSource.data();
					var checkList = [];
		        	$(gridList).each(function(idx,data){
		        		if(e.model.lotNo == data.lotNo){
		        			checkList.push(data)
		        		}
		        	})
		        	var lotNo = e.model.lotNo;
		        	var cnt = 0;
		        	var length = gridList.length
		        	for(var i=0; i<length; i++){
		        		if(lotNo==gridList[i].lotNo){
		        			if(typeof gridList[i].cnt=='undefined'){
		        				cnt += 0 
		        			}else{
		        				cnt += gridList[i].cnt
		        			}
		        			if(cnt > gridList[i].lotStock){
		        				kendo.alert(gridList[i].prdNo + "의 공정이동에서"+"<br>"+"<font style='color:red;'>재고</font>보다 <font style='color:red;'>이동수량</font>이 많습니다.<br>재고 : " + gridList[i].lotStock + "  이동수량 : " + cnt)
		        				e.model.cnt=0;
		        				return false;
		        			}
		        		}
		        	}
				}
		    },
		    height:getElSize(1610)
		}).data("kendoGrid")
	})
	
	function footer(cnt){
		console.log(cnt)
		if(cnt!=undefined){
			return cnt.sum;
		}
	}
	
	function checkRow(e){
		console.log("event click")
		var gridList=grid.dataSource.data();
		var dataItem = $(e).closest("tr")[0].dataset.uid;
		 var row = $("#wrapper").data("kendoGrid")
         .tbody
         .find("tr[data-uid='" + dataItem + "']");
		 var initData = grid.dataItem(row)
		 var lotNo = initData.lotNo;
		 var length = gridList.length
		 for(var i=0; i<length; i++){
			 if(lotNo==gridList[i].lotNo){
				 if(gridList[i].checkSelect){
					 gridList[i].checkSelect=false;
					 grid.dataSource.fetch();
				 }else{
					 gridList[i].checkSelect=true;
					 grid.dataSource.fetch();
				 }
			 }
		 }
		 //grid.dataSource.fetch()
	}
	
	function insertRow(e){
		var dataItem = $(e).closest("tr")[0].dataset.uid;
		 var row = $("#wrapper").data("kendoGrid")
         .tbody
         .find("tr[data-uid='" + dataItem + "']");
		 var initData = grid.dataItem(row)
		 var idx = grid.dataSource.indexOf(grid.dataItem(row));
		 grid.dataItem(row).checkSelect=true
		 grid.dataSource.insert(idx + 1, {
			prdNo: initData.prdNo,
	        lotNo: initData.lotNo,
	        oprNo: initData.oprNo,
	        oprNm: initData.oprNm,
	        lotStock: initData.lotStock,
	        productName: initData.productName,
	        dvcName: initData.dvcName,
	        cnt: 0,
	        aftLot: initData.aftLot,
	        datetime: new Date(),
	        action:"<button id='delete' onclick=deleteRow(this)>${del}</button>",
	        checker:"<input type='check' disabled='disabled' checked='checked'>",
	        checkSelect:true,
	        newRow:true
		 })
	}
	
	function deleteRow(e){
		var dataItem = $(e).closest("tr")[0].dataset.uid;
		 var row = $("#wrapper").data("kendoGrid")
        .tbody
        .find("tr[data-uid='" + dataItem + "']");
		 var idx = grid.dataSource.indexOf(grid.dataItem(row));
		 var dataItem = dataSource.at(idx);
		 grid.dataSource.remove(dataItem)
	}
	
	function getPrdNo(){
		var url = "${ctxPath}/common/getPrdNo.do";
		var param = "shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
				
				var option = "<option value='ALL'>${total}</option>";
				
				$(json).each(function(idx, data){
					option += "<option value='" + decode(data.prdNo) + "'>" + decode(data.prdNo) + "</option>"; 
				});
				
				$("#group").html(option);
			}
		});
	};
	var dataSource;
	function getLotInfo(){
		classFlag = true;
		dataSource = new kendo.data.DataSource({
			schema: {
			    model: {
			      id: "ProductID",
			      fields: {
			    	 checker:{
			    		 editable:false
			    	 },
			    	 checkSelect:{
			    		 type: "Boolean",
			    		 defaultValue: "false",
			    		 editable:false
			    	 },
			    	 idx:{
			    		 editable: true
			    	 },
			    	  prdNo: {
			          		editable: false,
			        },
			         lotNo:{
			        	 editable:false
			         },
			         groupLotNo:{
			        	 editable:false
			         },
			        oprNo: {
			          		editable: false,
			        },
			        newOprNo: {
			          		editable: false,
			        },
			        movedOprNo:{
			        	
			        },
			        lotStock: {
			          		editable: false,
			        },
			        productName: {
			          		editable: true,
			          		type: "string"
			        },
			        nowDvcName:{
			        	editable: false,
			        },
			      	dvcName: {
			          		editable: true,
			          		type: "string"
			        },
			        cnt: {
			          		editable: true,
			          		type: "number"
			        },
			        aftLot: {
			          		editable: true
			        },
			        datetime: {
			          		editable: false,
//			          		type:"date",
//			          		defaultValue:  new Date()
			        },
			        regdate: {
			          		editable: false,
			        },
			        devide: {
			          		editable: false,
			        },
			        action: {
			          		editable: false,
			        },
			        newRow: {
			          		editable: false,
			          		 type: "Boolean"
			        }
			        
			      }
			    }
			  }
		});
		
		var url = "${ctxPath}/chart/getStockInfoByTableLine.do";
		
		var param = "prdNo=" + $("#group").val() + 
					"&lotNo=" + $("#transText").val();

		$("#checkall")[0].checked=false;
		$.showLoading()
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				cntArray = [];

				console.log(json)
				
				$(json).each(function(idx, data){
					json[idx].cnt=json[idx].lotStock;
					json[idx].productName=json[idx].prdNo;
					json[idx].dvcName=0;
					
					if(data.seq!="."){
						if(classFlag){
							className = "row2"
						}else{
							className = "row1"
						};
						classFlag = !classFlag;
							
						if(data.stock==0) return;
						var oprNo;
						if(data.oprNm=="0010"){
							data.oprNo = "R삭";
						}else if(data.oprNm=="0020"){
							data.oprNo = "MCT 1차";
						}else if(data.oprNm=="0030"){
							data.oprNo = "CNC 1차";
						}else if(data.oprNm=="0040"){
							data.oprNo = "CNC 2차";
						}else if(data.oprNm=="0000"){
							data.oprNo = "자재창고";
						}else if(data.oprNm=="1000"){
							data.oprNo = "완제품"
						}
					}
					data.idx=data.id;
					data.datetime = "";
					dataSource.add(data);
				});
				console.log("---")
				console.log(dataSource);
				dataSource.aggregate([
					{ field: "lotStock", aggregate: "sum" },
					{ field: "cnt", aggregate: "sum" }
				]);
				
				grid.setDataSource(dataSource);
				
				$(".alarmTable td").css({
					"padding" : getElSize(20),
					"font-size": getElSize(40),
					"border": getElSize(5) + "px solid black"
				});
				
				$("button, input, select").css({
					"font-size" : getElSize(40)
				})
				
				$(".row1").not(".tr_table_fix_header").css({
					"background-color" : "#222222"
				});

				$(".row2").not(".tr_table_fix_header").css({
					"background-color": "#323232"
				});
				
				$("body, html").css({
					"overflow" : "hidden"	
				})
				
				$.hideLoading()
				setToday();
			}
		});
	};
	
	
	var dropdownList;
	function getOprList(container, options){
		
		if(typeof options == "undefined"){
		}else{
			
			var url = "${ctxPath}/chart/OprNmList.do";
			var param = "prdNo=" + options.model.prdNo;
			
			if(options.model.productName !=null ){
				param = "prdNo=" + options.model.productName;
			}
			$.ajax({
				url : url,
				data : param,
				type : "post",
				dataType : "json",
				async : false,
				success : function(result){
					var json = result.dataList;
					console.log(json)
					
					if(options.model.oprNm!="1000"){
						if(json.length>0){
							var arr={};
							arr.oprNo=1000;
							arr.dvcName="완제품";
							json.push(arr);
							
							dropdownList = $('<input name="' + options.field + '" />')
							 .appendTo(container)
							 .kendoDropDownList({
								 autoWidth: true,
								 valuePrimitive: true,
								 height: 3300,
								 dataTextField : "dvcName",
								 dataValueField  : "dvcId",
								 dataSource: json
							}).data("kendoDropDownList");
						}else{
							dropdownList = $('<input name="' + options.field + '" />')
							 .appendTo(container)
							 .kendoDropDownList({
								 autoWidth: true,
								 height: 3300,
								 dataSource: {data:["장비없음"]}
							}).data("kendoDropDownList");
						}
					}else{
						dropdownList = $('<input name="' + options.field + '" />')
						 .appendTo(container)
						 .kendoDropDownList({
							 autoWidth: true,
							 height: 3300,
							 dataSource: {data:[{"dvcName" : "완제품", "oprNo" : "1000"}]}
						}).data("kendoDropDownList");
					}
				}
			});
		}
	}
	
	function dateTimeEditor(container, options) {
		 $('<input id="datePicker" data-text-field="' + options.field + '" data-value-field="' + options.field + '" data-bind="value:' + options.field + '" data-format="' + options.format + '"/>')
         .appendTo(container)
         .kendoDateTimePicker({

         });
	}
	
	function checkMstmat(container, options){
		var list=[];
		for(i=0; i<finishPrdNo.length; i++){
			if(finishPrdNo[i].RWMATNO==options.model.prdNo){
				var arr={};
				arr.MATNO=finishPrdNo[i].MATNO;
				list.push(arr);
			}
		}
		if(list.length==0){
			var arr={};
			arr.MATNO=options.model.prdNo
			list.push(arr);
		}
				if(typeof options == "undefined"){
				}else{
					console.log(finishPrdNo);
					
					$('<input name="' + options.field + '" />')
					 .appendTo(container)
					 .kendoDropDownList({
						 valuePrimitive: true,
						 autoWidth: true,
						 height: 3300,
						 dataTextField : "MATNO",
						 dataValueField  : "MATNO",
						 dataSource: list
					 }).data("kendoDropDownList");
				}
		
	}
	
	function readOnly(container, options) {
		container.removeClass("k-edit-cell");
		container.text(options.model.get(options.field));
	}
	
	var cntArray = [];
	function addNewLot(obj){
		var lotName = $(obj).attr("class"); 
		var preOpr = $(obj).parent("td").parent("tr").children("td:nth(1)").attr("oprNm");
		var parent = $(".parent" + lotName)[0];
		
		var className = $(obj).parent("td").parent("tr").children("td:nth(4)").children("input").attr("class");
		var prdNo = $(obj).parent("td").parent("tr").children("td:nth(0)").html();
		var date = "<input type='date'  class='date'><input type='time'  class='time'>";
		var dividLot = "<button onclick='delRow(this);' class='" + lotName + "' style='padding : " + getElSize(10) + "'>${del}</button>";
		
		
		var tr = "<tr class='contentTr'>" + 	
					"<td>" + prdNo + "</td>" + 
					"<td oprNm='" + preOpr + "'> </td>" +
					"<td> </td>" +
					"<td><select>" + getOprList + "</select></td>" +
					"<td><input type='text' value='0' class='" + className + "'></td>" + 
					"<td>" + date + "</td>" + 
					"<td>" + dividLot + "</td>" + 
				"</tr>";
				
		$(parent).after(tr);
		setToday();
		
		$("button, input, select").css({
			"font-size" : getElSize(40)
		})
	};

	function delRow(el){
		$(el).parent("td").parent("Tr").remove();
	}

	function getOprNm(prdNo, id){
		var url = "${ctxPath}/chart/getOprNmList.do";
		var rw = prdNo.indexOf("RW");

		if(rw!=-1){
			rw = "true";
		}else{
			rw = "false";
		}
		
		prdNo = prdNo.replace("유럽","UR");
		
		var param = "prdNo=" + prdNo + 
					"&rw=" + rw;
		
		var options="";
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.val + "'>" + data.val + "</option>";
				});
				$("#" + id).html(options)
			}
		});
	};
	
	function setToday(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		
		$(".date").val(year + "-" + month + "-" + day);
		$(".time").val(hour + ":" + minute);
	};
	
	function saveRow(){
		var $grid = $("#wrapper").data("kendoGrid")
		
		var dataSource = $grid.dataSource.data();
		var saveList=[];
		var length = dataSource.length;
		for(var i=0; i<length ; i++){
			if(dataSource[i].checkSelect){
				if(dataSource[i].productName != null && typeof dataSource[i].productName != 'undefined' && dataSource[i].dvcName !=null && typeof dataSource[i].dvcName!= 'undefined'){
					console.log(dataSource[i].movedOprNo)
					if(dataSource[i].movedOprNo=="R삭"){
						dataSource[i].afterProj = "0010";
					}else if(dataSource[i].movedOprNo=="MCT삭"){
						dataSource[i].afterProj = "0020";
					}else if(dataSource[i].movedOprNo=="CNC삭"){
						dataSource[i].afterProj = "0030";
					}else if(dataSource[i].movedOprNo=="완성창고"){
						dataSource[i].afterProj = "1000";
						if(dataSource[i].dvcName="완제품"){
							dataSource[i].dvcName=dataSource[i].nowDvcName
						}
					}else if(dataSource[i].movedOprNo==undefined){
						dataSource[i].afterProj = "0005"
					}
					if(dataSource[i].afterProj=='1000'){
						console.log("DataSource is : ");
						console.log(dataSource[i]);
						dataSource[i].productName=dataSource[i].prdNo;
					}
					dataSource[i].prdNo=dataSource[i].productName;
					saveList.push(dataSource[i])
				}else{
					kendo.alert("장비와 완성 품번을 선택해주세요.")
					return false;
				}
			}
		}
		
		var length = saveList.length
		if(length<=0){
			kendo.alert("이동할 항목을 선택하여 주십시오");
			$.hideLoading();
			return false;
		}
		var lotNo;
		var cnt = 0;
		for(var i=0;i<length;i++){
				if(typeof lotNo == 'undefined' || lotNo==null){
					lotNo=saveList[i].lotNo
					cnt += saveList[i].cnt
				}else if(lotNo == saveList[i].lotNo){
					cnt += saveList[i].cnt
					if(cnt>saveList[i].lotStock){
						kendo.alert("이동 수량이 재고보다 많습니다.")
						return false;
					}
				}
				
				if(saveList[i].dvcName=='장비없음' || saveList[i].dvcName=='없음'){
					saveList[i].productName=saveList[i].prdNo
					saveList[i].dvcName=0
				}
				
				saveList[i].datetime = moment().format("YYYY-MM-DD HH:mm:ss");

		}
		if(cnt<=0){
			kendo.alert("이동 수량이 0개 입니다. <br> 이동 수량을 적어 주십시오.")
			return false;
		}
		console.log(saveList);
		var obj = new Object();
		obj.val = saveList;
		
		var param = JSON.stringify(obj);
		
		
		
		console.log(param)
		$.showLoading()
		
		var url = "${ctxPath}/chart/moveStockWithTrans.do"
		$.ajax({
			url : url,
			data :"val=" + param,
			type : "post",
			dataType : "text",
			success : function(data){
			//	kendo.alert(saveList.length + "개의 List 저장");
				$("#transText").val("")
				getLotInfo();
			}
		}); 
	};
</script>
</head>
<body>
	<div id="delDiv">
		<Center>
			<font><spring:message code="chk_del" ></spring:message></font><Br><br>
			<button id="resetOk" onclick="okDel();"><spring:message code="del" ></spring:message></button>&nbsp;&nbsp;
			<button id="resetNo" onclick="noDel();"><spring:message code="cancel" ></spring:message></button>
		</Center>
	</div>
	
	
	<div id="dialog"></div>
	
	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			
			
			<Tr>
				
				<td rowspan="10" id="svg_td" style="vertical-align: top; position:relative; background:#26282c;">
					<table id="content_table" style="width: 100%"> 
						<Tr>
							<td id="td_first"> 
								<spring:message code="prd_no"></spring:message>
								<select id="group"></select>
								<input type="text" id="transText" style="color:#ffffff"; size="20%" onkeyup="getDeliverInfo(event)">
								<button id="search" onclick="getLotInfo()" style="cursor: pointer;"><i class="fa fa-search" aria-hidden="true"></i></button>
								<button id="save" onclick="saveRow()"><spring:message code="save"></spring:message></button>
							</td>
						</Tr>		
						<tr>
							<td>
								<div id="wrapper"></div>
							</td>
						</tr>			
					</table> 
				</td>
			</Tr>
			
	
		</table>
	 </div>
	 
</body>
</html>	