<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden; 
	background-color: black;
}
html{
	overflow : hidden;
}

#grid{
	border-color : #222327;
}

#grid .k-grid-header {
	border-color : #222327;
}

.k-grid-header-wrap {
	border-color : #222327;
}

</style> 
<script type="text/javascript">

	const loadPage = () =>{
//		createMenuTree("im", "exportManagement")	
		createMenuTree("tm", "toolLifeManager")	
	}
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};
	
	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

//	var handle = 0;
	
	$(function(){
//		createNav("inven_nav", 4);
		
//		setEl();
//		time();

		setEl2();
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	/* function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	}; */
	
	/* function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$("#iframe").css({
			"width" : getElSize(3500),
			"height" : getElSize(1800),
			"position" : "absolute",
			"z-index" : 999,
			"display" : "none"
		});
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : contentWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#banner").css({
			"font-size" : getElSize(80),
			"width" : getElSize(2000)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
	}; */
	
	
	function setEl2(){
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		
/* 		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		 */
		
		 $("#container").css({
				"width" : contentWidth - getElSize(30)
			})
			
			$("#content_table").css({
				"margin-top" : getElSize(300)
			})
			

		 
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight,
			"margin-top" : getElSize(250)
		});
		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		
		$("#grid").css({
			"margin-left" : getElSize(50),
			"margin-right" : getElSize(50)
		})

		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(22),
			"margin-right" : getElSize(22)
		});
		
		$("select").css({
			"width" : getElSize(380),
			"margin-right" : getElSize(50)
		});		
			
		$("#td_first").css({
			"padding-left" : getElSize(50),
			"padding-top" : getElSize(20),
			"padding-bottom" : getElSize(10),
			"margin-top" : getElSize(50)
		});
		
		$(".k-header").css({
		    "font-family" : "NotoSansCJKkrBold",
		    "font-size" : getElSize(32) + "px",
		    "background-image" : "none",
		    "background-color" : "#353542"
		}); 
		
		$("button").css({
			"padding" : getElSize(25),
			"margin-left" : getElSize(7),
			"margin-right" : getElSize(30),
			"margin-top" : getElSize(7),
			"font-size" : getElSize(47),
			"background" : "#909090",
			"border-color" : "#222327"
		})
		
		$("#divition").css({
			"padding" : getElSize(25),
			"margin-left" : getElSize(7),
			"margin-right" : getElSize(30),
			"margin-top" : getElSize(7),
			"font-size" : getElSize(47),
			"background" : "#909090",
			"border-color" : "#222327"
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(200),
			"margin-right" : getElSize(80)
// 			"height" : getElSize(82)
		});
		
		$("#export").css({
			"cursor" : "pointer",
			"width" : getElSize(1000),
			"margin-right" : getElSize(80),
			"margin-top" : getElSize(10)
// 			"height" : getElSize(82)
		});
		
		$("#banner").css({
			"font-size" : getElSize(80),
			"width" : getElSize(2000)
		});
		
	};
	
	
/* 	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
 */	
	var finishPrdNo;
	var companylist=[];
	var prdcomlist=[];
	function finishPrdNo(){
		var url = "${ctxPath}/chart/getbertmstmatno.do";
		var param ;
		var json;
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				finishPrdNo=data.dataList;
				console.log(finishPrdNo)
			}
		});
	}
	
	function checkMstmat(container, options){
		var list=[];
		for(i=0; i<finishPrdNo.length; i++){
			if(finishPrdNo[i].RWMATNO==options.model.prdNo){
				var arr={};
				arr.MATNO=finishPrdNo[i].MATNO;
				list.push(arr);
			}
		}
		if(list.length==0){
			var arr={};
			arr.MATNO=options.model.prdNo
			list.push(arr);
		}
		
		if(typeof options == "undefined"){
		}else{
//			console.log(finishPrdNo);
			$('<input name="' + options.field + '" />')
			 .appendTo(container)
			 .kendoDropDownList({
				 valuePrimitive: true,
				 autoWidth: true,
				 height: 3300,
				 dataTextField : "MATNO",
				 dataValueField  : "MATNO",
				 dataSource: list,
				 change : function(e){
//					 console.log("체인지")
//					 console.log(options.model)
					 for(i=0; i<finishPrdNo.length; i++){
							if(finishPrdNo[i].MATNO==options.model.item){
								 options.model.set("ITEMNO",finishPrdNo[i].ITEMNO);								
							}
					}
//					 options.model.ITEMNO="하이";
				 }
			 }).data("kendoDropDownList");
		}
		
	}
	//업체 리스트(선택박스)
	function getComList(){
		var url = "${ctxPath}/chart/getComList.do";
		
		$.ajax({
			url :url,
			dataType :"json",
			type : "post",
			success : function(data){
				var json = data.dataList;	//업체 총 리스트
				var json1 = data.dataList1;	// 라우팅 걸린 업체 리스트
				console.log(json)
				var option = "<select >";
				$(json).each(function(idx, data){
					var arr=new Object();
					option += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
					arr.prdNo=data.prdNo;
					arr.id=data.id;
					arr.name=decode(data.name);
					companylist.push(arr);
				});
				$(json1).each(function(idx, data){
					var arr=new Object();
					option += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
					arr.prdNo=data.prdNo;
					arr.id=data.id;
					arr.name=decode(data.name);
					arr.afterProj=data.afterProj;
					prdcomlist.push(arr);
				});
			
				option += option + "</select>";
				
				console.log("list--")
				console.log(prdcomlist)
			}
		});
	};
	//delbert 2017-09-27 15:40 업체명 이름으로변환
	function changename(abc){
		var name=abc;
		for(i=0;i<companylist.length;i++){
			if(companylist[i].id==abc)
				name=companylist[i].name
		}
		if(name==0){
			return "--선택--"
		}
		return name;
	}
	//업체명 리스트 뽑기 kendo 170925 delbert
	function dropdownlist(container, options){
//		prdcomlist
		var prdlist=[];
	
		for(i=0,length=prdcomlist.length; i<length; i++){
			if(prdcomlist[i].prdNo==options.model.item){
				prdlist.push(prdcomlist[i]);
			}
		}
			
		$('<input required name="' + options.field + '" />')
		.appendTo(container)
		.kendoDropDownList({
			autoWidth: true,
			autoBind: false,
			dataTextField: "name",
			height: 3300,
			dataValueField: "id",
			dataSource: prdlist,
			value:prdlist[0],
			change : function(e){
				console.log("--chage--")
//				console.log(options.model)

			/* var param = "item="+options.model.item +
						"&vndNo="+options.model.vndNo;		
//			console.log(param)
			var url = "${ctxPath}/chart/processCnt.do"
			$.showLoading()
			$.ajax({
				url : url,
				data : param,
				type : "post",
				dataType : "json",
				success : function(data){
					$.hideLoading()
					if(data.dataList.length>0){
						console.log(data.dataList[0].cnt)
						options.model.cnt=data.dataList[0].cnt
						$("#grid").data("kendoGrid").dataSource.fetch()
					}
				}
			}) */
			}
		});
		
//		console.log(container);
//		console.log(options);
	}
	
	function checkRow(e){
		console.log("event click")
		var grid = $("#grid").data("kendoGrid");
		var gridList=grid.dataSource.data();
		var dataItem = $(e).closest("tr")[0].dataset.uid;
		 var row = $("#grid").data("kendoGrid")
         .tbody
         .find("tr[data-uid='" + dataItem + "']");
		 var initData = grid.dataItem(row)
		 var date = initData.date;
		 var prdNo = initData.prdNo;
		 var proj = initData.proj;
		 var length = gridList.length
		 for(var i=0; i<length; i++){
			 if(date==gridList[i].date && prdNo==gridList[i].prdNo && proj==gridList[i].proj){
				 if(gridList[i].checkSelect){
					 gridList[i].checkSelect=false;
					 grid.dataSource.fetch();
				 }else{
					 gridList[i].checkSelect=true;
					 grid.dataSource.fetch();
				 }
			 }
		 }
		 //grid.dataSource.fetch()
	}
	function checkAll(e){
		console.log($("#checkall").is(":checked"))
		if($("#checkall").is(":checked")){
			gridlist=grid.dataSource.data();
        	$(gridlist).each(function(idx,data){
        		data.checkSelect = true;
        	})
		}else{
			gridlist=grid.dataSource.data();
        	$(gridlist).each(function(idx,data){
        		data.checkSelect = false;
        	})
		}
		grid.dataSource.fetch();
	}
	
	function insertRow(e){
		var dataItem = $(e).closest("tr")[0].dataset.uid;
		var grid = $("#grid").data("kendoGrid")
		 var row = $("#grid").data("kendoGrid")
         .tbody
         .find("tr[data-uid='" + dataItem + "']");
		 
		 console.log(row)
		 var initData = grid.dataItem(row)
		 var idx = grid.dataSource.indexOf(grid.dataItem(row));
		 grid.dataItem(row).checkSelect=true
		 grid.dataSource.insert(idx + 1, {
			prdNo: initData.prdNo,
	        proj: initData.proj,
	        date: initData.date,
	        cnt: initData.cnt,
	        vndNo : 0,
	        action:"<button id='delete' onclick=deleteRow(this)>${del}</button>",
	        checker:"<input type='check' disabled='disabled' checked='checked'>",
	        checkSelect:true,
	        newRow:true
		 })
	}
	
	function deleteRow(e){
		console.log(e)
		console.log($(e).closest("tr"))
		var dataItem = $(e).closest("tr")[0].dataset.uid;
		var grid = $("#grid").data("kendoGrid")
		 var row = $("#grid").data("kendoGrid")
        .tbody
        .find("tr[data-uid='" + dataItem + "']");
		 var idx = grid.dataSource.indexOf(grid.dataItem(row));
		 console.log(idx)
		 var dataItem = kendodata.at(idx);
		 grid.dataSource.remove(dataItem)
	}
	function makeid(){
	    var text = "";
	    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	    for( var i=0; i < 5; i++ )
	        text += possible.charAt(Math.floor(Math.random() * possible.length));
	    return text;
	}
	
	function saveRow(){
		var savelist=[];
		var gridlist=grid.dataSource.data();
		for(i=0,length=gridlist.length; i<length; i++){
			if(gridlist[i].checkSelect){
				if(gridlist[i].vndNo==0){
					alert("업체를 선택해주세요")
					return false
				}else if(gridlist[i].item==undefined){
					alert("완성품번을 선택하여 주세요")
					return false;
				}else if(gridlist[i].lotNo==undefined){
					alert("lotNo를 입력해주세요")
					return false;
				}else if(gridlist[i].sendCnt==null){
					alert("이동수량을 입력해주세요")
					return false;
				}else if(gridlist[i].cnt < gridlist[i].sendCnt){
					alert("재고보다 이동수량이 많을수 없습니다.")
					return false;
				}
				gridlist[i].beforeProj=gridlist[i].proj
				gridlist[i].action=undefined
				gridlist[i].checker=undefined
				
				for(j=0,len=prdcomlist.length;j<len;j++){
					if(gridlist[i].item==prdcomlist[j].prdNo && gridlist[i].vndNo==prdcomlist[j].id){
						gridlist[i].afterProj=prdcomlist[j].afterProj
					}
				}
				
				savelist.push(gridlist[i])
				
			}
		}
		
		if(savelist.length==0){
			alert("반출할 항목을 선택해주세요.")
			return false;
		}
		
		var obj = new Object();
		obj.val = savelist;
		
		var param = JSON.stringify(obj);
		
		console.log(param);
		var url = "${ctxPath}/chart/exportMoveTrans.do"
		$.showLoading()
		$.ajax({
			url : url,
			data :"val=" + param,
			type : "post",
			dataType : "text",
			success : function(data){
				if(data=="success"){
					kendo.alert(savelist.length + "개의 List 저장");
					getTable();
					$.hideLoading();
//					getTable();
				}else{
					kendo.alert("저장에 실패하였습니다.")
					$.hideLoading();
				}
			}
		})
		
		
	
	}
	function textWirte(){
		$("#dialog").kendoDialog({
			actions: [{
				text: "반출"
				,action: function(e){
					exportPrint();
				}
			},{
				text: "취소"
				,action: function(e){
					$("#dialog").data("kendoDialog").close()
					return false
				}
			}]
			,title: "비고"
	    });
	    $("#dialog").data("kendoDialog").open()
	}
	
	function exportPrint(){

		var barCode=new Date()
		a=addZero(String(barCode.getFullYear()))
		b=addZero(String(barCode.getMonth()+1))
		c=addZero(String(barCode.getDate()))
		d=addZero(String(barCode.getHours()))
		e=addZero(String(barCode.getMinutes()))
		f=addZero(String(barCode.getSeconds()))
		barCode = a+b+c+d+e+f

		var savelist=[];
		var gridlist=grid.dataSource.data();
		for(i=0,length=gridlist.length; i<length; i++){
			var sumCnt=0;
			gridlist[i].remarks=encodeURI($("#remarksText").val().replace(/(?:\r\n|\r|\n)/g, '<br />'))
			if(gridlist[i].checkSelect){
				if(gridlist[i].vndNo==0){
					alert("업체를 선택해주세요")
					return false
				}else if(gridlist[i].item==undefined){
					alert("완성품번을 선택하여 주세요")
					return false;
				}else if(gridlist[i].lotNo==undefined){
					alert("lotNo를 입력해주세요")
					return false;
				}else if(gridlist[i].sendCnt==null){
					alert("이동수량을 입력해주세요")
					return false;
				}else if(gridlist[i].cnt < gridlist[i].sendCnt){
					alert("재고보다 이동수량이 많을수 없습니다.")
					return false;
				}
				for(j=0,len=gridlist.length; j<len; j++){
					if(gridlist[j].prdNo==gridlist[i].prdNo){
						sumCnt+=gridlist[j].sendCnt
					}
				}
				if(gridlist[i].cnt < sumCnt){
					alert("재고보다 이동수량이 많을수 없습니다.")
					return false;
				}
				gridlist[i].beforeProj=gridlist[i].proj
				gridlist[i].deliveryNo=barCode
				gridlist[i].barcode=gridlist[i].lotNo
				gridlist[i].action=undefined
				gridlist[i].checker=undefined
				for(j=0;j<companylist.length;j++){
					if(companylist[j].id==gridlist[i].vndNo){
						gridlist[i].vndNm=encodeURI(companylist[j].name)
					}
				}
				
				for(j=0,len=prdcomlist.length;j<len;j++){
					if(gridlist[i].item==prdcomlist[j].prdNo && gridlist[i].vndNo==prdcomlist[j].id){
						gridlist[i].afterProj=prdcomlist[j].afterProj
					}
				}
				
				savelist.push(gridlist[i])
				
			}
		}
		
		if(savelist.length==0){
			alert("반출할 항목을 선택해주세요.")
			return false;
		}

		var url = "${ctxPath}/chart/createBarcode.do";
		$.ajax({
			url : url,
			data : "deliveryCd=" + barCode,
			type :"post",
			dataType : "text",
			success :function(data){
				if(data == "success"){
					checkedIds = {};
/* 					console.log(savelist)
					console.log(JSON.stringify(savelist)) */
					/* console.log("---------")

					console.log(savelist[0])
					console.log(encodeURIComponent(JSON.stringify(savelist)).length)
					console.log(encodeURIComponent(JSON.stringify(savelist)))
					console.log("---------") */
					$("#iframe").attr("src", "${ctxPath}/chart/printExport.do?json=" + encodeURIComponent(JSON.stringify(savelist))).css({
						"display" : "inline",
						"background-color" : "white"
					});
					
					var close_btn = document.createElement("img");
					close_btn.setAttribute("src", "${ctxPath}/images/close_btn.png");
					close_btn.setAttribute("id", "close_btn");
					
					$("body").append(close_btn);						
											
					$("#close_btn").css({
						"width" : getElSize(100) + "px",
						"height" : getElSize(100) + "px",
						"position" : "absolute",
						"cursor" : "pointer",
						"top" : $("#iframe").offset().top,
						"left" : $("#iframe").offset().left + $("#iframe").width() - getElSize(100),
						"z-index" : 9999
					}).click(function(){
						$(this).remove();
						$("#print_btn").remove();
						$("#iframe").css("display" , "none")
					})
							
					
					var print_btn = document.createElement("img");
					print_btn.setAttribute("src", "${ctxPath}/images/printer.png");
					print_btn.setAttribute("id", "print_btn");
					
					$("body").append(print_btn);						
											
					$("#print_btn").css({
						"width" : getElSize(100) + "px",
						"height" : getElSize(100) + "px",
						"position" : "absolute",
						"cursor" : "pointer",
						"top" : $("#iframe").offset().top,
						"left" : $("#iframe").offset().left + $("#iframe").width() - getElSize(250),
						"z-index" : 9999
					}).click(function(){
						var frm = document.getElementById("iframe").contentWindow;
						console.log("--start--")
						console.log(frm)
						console.log("--end--")
						frm.focus();// focus on contentWindow is needed on some ie versions
						frm.print();	
					})
					saveRow()
				}else{
					alert("실패")
				}
			},error : function(){
				alert("에러")
			}
		});
		
	}
	
	function readOnly(container, options){
		container.removeClass("k-edit-cell");
		container.text(options.model.get(options.field));
	}
	
	function projView(proj){
		if(proj=="0005"){
			return "공정창고"
		}else{
			return proj
		}
	}
	
	/* function itemNoView(item){
		if(item)
		console.log(item)
	} */
	var kendotable;
	$(document).ready(function(){
		finishPrdNo();
		getComList();
		
		kendotable =$("#grid").kendoGrid({
			height : getElSize(1630),
			editable : true,
			dataBound : function(e){
				$("#grid thead tr th").css("font-size",getElSize(44))
				$("#grid thead tr th").css("font-family","NotoSansCJKkrBold")
				$("#grid thead tr th").css("vertical-align","middle")
				$("#grid thead tr th").css("text-align","center")
				
				$("#grid tbody tr td").css("font-size",getElSize(38))
				
				
			},
			filterable: {
			      mode: "row",
			      style : "text-align: center;"
			},
			columns:[{
				field:"checker"
				,title:"<input type='checkbox' id='checkall' onclick='checkAll(this)' style='text-align:center; width: "+getElSize(50)+"; height: "+getElSize(80)+";'>"
				,template: "<input type='checkbox' onclick='checkRow(this)' #= (typeof checkSelect!='undefined' && checkSelect!=false)? 'checked=checked' : '' # #= (typeof newRow!='undefined') ? \'disabled=checked\' : '' # class='checkbox' style='width: "+getElSize(80)+"; height: "+getElSize(80)+";'/>" 
				 ,filterable: {
	                    cell: {
	                        enabled: false
	                    }
	             }
				,width : getElSize(120)
			},{
				title:'${mat_prd_no}'
				,field:'prdNo'
				,filterable: {
				 cell: {
				                    suggestionOperator: "contains"
			             }
			      }
				,width : getElSize(400)
			},{
				title:'${Current_process}'
				,field:'proj'
				,filterable: {
	                    cell: {
	                        enabled: false
	                    }
	            }
				,template : "#=projView(proj)#"
				,width : getElSize(280)
			},{
				title:'${Finished_number} *'
				,field:'item'
				,editor : checkMstmat
				 ,filterable: {
	                    cell: {
	                        enabled: false
	                    }
	             }
				,attributes: {
				      "class": "table-cell"
				 }
				,width : getElSize(380)
			},{
				
				title:'${prd_no}'
				,field:"ITEMNO"
				,editor : readOnly
				,filterable: {
                    cell: {
                        enabled: false
                    }
	             }
				,width : getElSize(300)
//				,template : "#=itemNoView(item)#"
			},{
				field:"vndNo", title:"${com_name} *",editor : dropdownlist ,template:"#=changename(vndNo)#",width:getElSize(350)
				 ,filterable: {
	                    cell: {
	                        enabled: false
	                    }
	             }
				,attributes: {
				      "class": "table-cell"
				 }
				,width : getElSize(230)
				/* ,attributes: {
    				style: "text-align: center; background-color:black; font-size:" + getElSize(35) +"; color : black;"
    			},headerAttributes: {
    				style: "text-align: center; background-color:black; font-size:" + getElSize(37) + "; color : white;"
    			} */
			},{
				field:"lotNo"
				,title:"lotNo *"
				,filterable: {
                    cell: {
                        enabled: false
                    }
	             }
				,attributes: {
					"class" : "table-cell"
				}
				,width : getElSize(230)
			},{
				title:'${totalCount}'
				,field:'cnt'
				 ,filterable: {
	                    cell: {
	                        enabled: false
	                    }
	             }
				,width : getElSize(220)
			},{
				title:'${move_cnt} *'
				,field:'sendCnt'
				 ,filterable: {
	                    cell: {
	                        enabled: false
	                    }
	             }
				,attributes: {
				      "class": "table-cell"
				 }
				,width : getElSize(220)
			},{
	        	field:"action",	
	        	headerTemplate : "",
	        	template:"#=(typeof action=='undefined')?'<button id=divition onclick=insertRow(this)>${divide}</button>':action#",
	        	width:getElSize(150)
				/* headerAttributes: {
					style: "text-align: center; background-color:black; font-size:" + getElSize(37)+ "; color:white;"
				 }
	        	,attributes: {
						style: "text-align: center; font-size:" + getElSize(30) + "; color:white;"
				  }*/ 
				 ,filterable: {
	                    cell: {
	                        enabled: false
	                    }
	             }
				 ,width : getElSize(250)
	        }]
		}).data("kendoGrid")
		
		getTable();
	})
	
	var kendodata
	var grid 
	function getTable(){
		var url = "${ctxPath}/chart/getExportList.do";
		var param; /* = "prdNo=" + $("#group").val() + 
					"&lotNo=" + $("#transText").val(); */

//		$("#checkall")[0].checked=false;
		var json;
		$.showLoading()
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				console.log(data)
				json = data.dataList;
				$(json).each(function(idx,data){
					data.vndNo=0;
				})
				
				kendodata = new kendo.data.DataSource({
					data: json,
					batch: true,
		/* 			group: { field: "prdNo" },
					sort: [{
		            	field: "prdNo" , dir:"asc" 
		            },{
		            	field: "item" , dir:"asc"
		            }], */
					height: 500,
					schema: {
						model: {
							id: "id",
							fields: {
								prdNo: { editable: false },
								proj: { editable: false },
								vndNo: {editable: true,
									field : 'vndNo',
									defaultValue: function(e){
										return "0001"
									}
								},
								item: { editable: true,
									field : "item",
									defaultValue: "FS_RR_LH"
								},
								cnt: { editable: false },
								sendCnt: { editable: true ,type: 'number'},
								checker : { editable : false},
								action : { editable : false}
							}
						}
					}
				}); 
				kendotable.setDataSource(kendodata);
				grid = $("#grid").data("kendoGrid");

				$.hideLoading()
			}
		})

	}
	
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	
	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td rowspan="10" id="svg_td" style="vertical-align: top; position:relative; background:#26282c;">
					<div id="wrapper">
						<table style="width: 100%">
							<Tr>
								<Td id="td_first" style="text-align: center; vertical-align: middle;">
									<button id="search" onclick="getTable()" style="cursor: pointer;"><i class="fa fa-search" aria-hidden="true"></i></button>									
<!-- 									<button onclick="saveRow()" > 저장 </button> -->
<%-- 									<button id="export" onclick="textWirte()"><spring:message code="exportdocument"></spring:message></button> --%>
										<button id="export" onclick="textWirte()">Issuance of export document</button>
								</td>
							<tr>
							<tr>
								<td>	
									<div id="grid">
									</div>
								</Td>
							</Tr>
						</table>
					</div>
				</td>
			</Tr>
		
		</table>
	 </div>
	 
	<div id="dialog"><textarea id="remarksText" rows="10" cols="25"></textarea> </div>

</body>
</html>	