<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden; 
	background-color: black;
}

#grid{
	border-color : #222327;
}

#grid .k-grid-header {
	border-color : #222327;
}

.k-grid-header-wrap {
	border-color : #222327;
}

#wrapper.k-grid.k-widget.k-display-block.k-editable{
    background-color:black;
    border-color : #222327;
}
#detail table *{
	color: black !important;
}
.k-grid td{
	color:white ;
}

</style> 
<script type="text/javascript">
//여기

	const loadPage = () =>{
		createMenuTree("im", "incomeStock")	
	}
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};




	var ActionList="";//조치
	var comList = "";//선택
	var CheckTyList="";//검사구분
	var ProcessList="";//공정
	var WorkerList="";//작업자
	var PartList="";//부위
	var SituationList="";//현상구분
	var SituationTyList="";//현상 왼쪽
	var CauseList="";//현상 오른쪽
	var GChTyList="";//귀책구분
	var faultylist=[]; //불량등록리스트
	$( function() {
	    $( "#sDate" ).datepicker({
		    onSelect : function(e){
		    	//e == 날짜 
		    	getIncomStock();
		    	$("#sDate").val(e);
		    }
	    })
	    $( "#eDate" ).datepicker({
		    onSelect : function(e){
		    	//e == 날짜 
		    	getIncomStock();
		    	$("#eDate").val(e);
		    }
	    })

   });
	
	//조치리스트(선택박스)
	function getActionList(){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 10;
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var options="<select>"
				var json = data.dataList;
				options += "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
								
				options+="</select>";
				ActionList=options;
				return options;
			}
		});
	};
	//업체 리스트(선택박스)
	function getComList(){
		var url = "${ctxPath}/chart/getComList.do";
		
		$.ajax({
			url :url,
			dataType :"json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var option = "<select >";
				$(json).each(function(idx, data){
					var arr=new Object();
					option += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
					arr.id=data.id;
					arr.name=decode(data.name);
					companylist.push(arr);
				});
			
				comList += option + "</select>";
			}
		});
	};
	//검사구분 리스트(선택박스)
	function getCheckTyList(){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 2;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<select><option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				options+="</select>"				
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
				CheckTyList=options;
				return options;
			}
		});	
	};
	//공정 리스트(선택박스)
	function getProcessList(){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 3; 	
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				var options = "<select><option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				options+="</select>"
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
				ProcessList=options;
			}
		});	
	};
	//작업자 리스트(선택박스)
	function getWorkerList(){
		var url = "${ctxPath}/common/getAllWorkerList.do";

		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<select>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decodeURIComponent(data.name).replace(/\+/gi, " ") + "</option>";
				});
				options+="</select>"
				WorkerList=options;
			}
		});	
	};
	
	//부위 리스트(선택박스)
	function getPartList(){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 4;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				var options = "<select><option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				options+="</select>"
				
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
				PartList=options;
			}
		});	
	};
	//현상구분 리스트 (선택박스)
	function getSituationList(){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 6;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<select><option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				options+="</select>"
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
				SituationList=options;
			}
		});	
	};
	//현상 왼쪽 리스트(선택박스)
	function getSituationTyList(){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 5;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<select><option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				options+="</select>"
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
				SituationTyList=options;
			}
		});	
	};
	//현상 오른쪽 리스트(선택박스)
	function getCauseList(){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 7;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<select><option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				options+="</select>"
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
				CauseList=options;
			}
		});	
	};	
	//귀책구분 리스트(선택박스)	
	function getGChTyList(){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 8;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<select id='GChTyname' onchange='GChTychange()'><option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				options+="</select>"
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
				GChTyList=options;
			}
		});	
	};
	
	//장비 선택 리스트(선택박스)
	function getDevieList(prdNo){
		var url = ctxPath + "/chart/getDevieList.do"
		var param = "shopId=" + shopId + 
					"&prdNo=" + prdNo;
		var devielist="";
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			async: false,
			success : function(data){
				var json = data.dataList;
				var options = "<select><option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				options+="</select>"
				devielist=options;
			}
		});	
		return devielist;
	};
	
	
	//귀책구분 채인지됬을때 귀책 select
	function GChTychange(){
		var ty;
		console.log($("#GChTyname").val());
		var val=$("#GChTyname").val();				
		if(val=="47"){			//업체
			ty = "com";
		}else if(val=="48"){	//작업자  
			ty = "worker"	
		};
		
		getGChList(ty);				
	}
	//귀책 선택 리스트(선택 박스)
	function getGChList(ty){
		var url;
		var options = "<select><option value='0'>${selection}</option>";	
		if(ty=="com"){
			url = ctxPath + "/chart/getComList.do"		
		}else if(ty=="worker"){
			url = ctxPath + "/common/getWorkerList.do"
		}else{
			options+="</select>"
			$("#GChTych").html(options);
			return options; 
		}
		
		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				console.log(json)
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
					
				});
				options+="</select>";
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
				$("#GChTych").html(options);
			}
		});	
	};
	//테이블 만듬	
	var wnd,detailsTemplate,idx;
	function getIncomStock(){
		$.showLoading(); 
		classFlag = true;
		var tablelist=[];
		var url = "${ctxPath}/chart/getDeliverInfo.do";
		var param = "deliveryNo=" + $("#deliveryNo").val()+
					"&sDate=" + $("#sDate").val() +
					"&eDate=" + $("#eDate").val() ; 
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				console.log(json);
				$(json).each(function(idx, data){
					 var selection = "<select>" +
										"<option value='" + data.id + "'>" + decode(data.name) + "</option>" + 
									"</select>"
					
					if(classFlag){
						className = "row2"
					}else{
						className = "row1"
					}; 
					var arr=new Object();
					
					arr.totaldelivery=data.totaldelivery;
					arr.stockid=data.stockid;
					arr.selection=data.id;
					arr.prdNo=data.prdNo;
					arr.size=data.spec; //규격;
					arr.lotNo=data.lotNo;
					arr.cnt= data.cnt;
					arr.check=0;//검사수량
					arr.defective=0;//불량수량
					arr.incnt=data.cnt;
					arr.faillist="불량등록";//불량내역
					arr.date=getTime(); 
					arr.date=data.date; 
					arr.barcode=data.barcode
					tablelist.push(arr);
					classFlag = !classFlag;
				});				
				
				var dataSource = new kendo.data.DataSource({
               	    data: tablelist,
                	autoSync: true,
  					schema: {
                    	model: {
	                        id: "Stocktable",
	                        fields: {
	                        	id: { editable: false, nullable: true },
	                        	prdNo: { editable: false, nullable: true },
	                        	totaldelivery:{ editable: false, nullable: true },
	                        	date1: { editable: true, nullable: true },
								lotCnt: { editable: false, nullable: true },
	                            smplCnt: { editable: false, nullable: true },
	                            notiCnt: { editable: true, nullable: true },
	                            incnt: { editable: false, nullable: true },
	                            
	                            lotNo: { editable: false, nullable: true },
	                            vndNo: { editable: false, nullable: true },
	                            date: { editable: false },
	                            selection: { editable: false, nullable: true },
	                            
	                            deliveryNo: { editable: false, nullable: true },
	                            cnt: { editable: false, nullable: true },
	                            faillist: { editable: false, nullable: true },
	                            size: { editable: false, nullable: true },
	                            check: { editable: false, nullable: true },
	                            Discontinued:{ type: "boolean" }, //체크박스 선택 기억하기
	                            checkdisable:{ type: "boolean"}, //checkbox disable
	                            defective:{editable:false,nullable:true},
	                            barcode:{editable:false,nullable:true}
                        }
                      }
                  }
               });
				"<Td>" +
				"$" +
			"</Td>" +
			"<Td>" +
				"${prd_no}" +
			"</Td>" +
			"<Td>" +
				"${spec}" +
			"</Td>" +
			"<Td>" +
				"Lot No" +
			"</Td>" +
			"<Td>" +
				"${lot_cnt}" +
			"</Td>" +
			"<Td>" +
				"${check_cnt}" +
			"</Td>" +
			"<Td>" +
				"${faulty_cnt}" +
			"</Td>" +
			"<Td>" +
				"${income_cnt}" +
			"</Td>" +
			"<Td>" +
				"${faulty_history}" +
			"</Td>" +
			"<Td>" +
				"${income_date}" +
			"</Td>" +
			"<Td>" +
				"${del}" +
				$("#wrapper").kendoGrid({
					dataSource:dataSource,
					editable:true,
					pageable:false,
					height:getElSize(1600),
					columns:[{
						title:"<input type='checkbox' id='checkall' style='width: "+getElSize(80)+"; height: "+getElSize(96)+";'>",
						width:getElSize(110),template: '<input type="checkbox" #= Discontinued ? \'checked="checked"\' : "" # #= checkdisable ? \'disabled="checked"\' : "" # class="checkbox"    style="width: '+getElSize(80)+'; height: '+getElSize(80)+';"/>' 
							,attributes: {
	            				style: "text-align: center; font-size:" + getElSize(35)
	            			},headerAttributes: {
	            				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
	            			}						 
						},{
						field: "totaldelivery", title:"${Delivery_number}",width:getElSize(280)
							,attributes: {
	            				style: "text-align: center; color:black; font-size:" + getElSize(35)
	            			},headerAttributes: {
	            				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40) + "; color : white;"
            				}
					},{
						field:"selection", title:"${com_name}",editor : dropdownlist ,template:"#=changename(selection)#",width:getElSize(280)
							,attributes: {
	            				style: "text-align: center; background-color:black; font-size:" + getElSize(35) +"; color : white;"
	            			},headerAttributes: {
	            				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40) + "; color : white;"
	            			}
					},{
						field: "prdNo", title:"${prd_no}",width:getElSize(310)
							,attributes: {
	            				style: "text-align: center; color:black; font-size:" + getElSize(35)
	            			},headerAttributes: {
	            				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40) + "; color : white;"
	            			}
					},{
						field: "size", title:"${spec}",width:getElSize(170)
							,attributes: {
	            				style: "text-align: center; color:black; font-size:" + getElSize(35)
	            			},headerAttributes: {
	            				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40) + "; color : white;"
	            			}
					},{
						field: "lotNo", title:"Lot No" ,width:getElSize(170)
							,attributes: {
	            				style: "text-align: center; background-color:black; font-size:" + getElSize(35)+"; color :white;"
	            			},headerAttributes: {
	            				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40) + "; color : white;"
	            			}
					},{
						field: "cnt",title : "${lot_cnt}",width:getElSize(180)
							,attributes: {
	            				style: "text-align: center; color:black; font-size:" + getElSize(35)
	            			},headerAttributes: {
	            				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40) + "; color : white;"
	            			}
					},{
						field: "check", title : "${check_cnt}",width:getElSize(180)
							,attributes: {
	            				style: "text-align: center; color:black; font-size:" + getElSize(35)
	            			},headerAttributes: {
	            				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40) + "; color : white;"
	            			}
					},{
						field: "defective", title : "${faulty_cnt}",width:getElSize(180)
							,attributes: {
	            				style: "text-align: center; color:black; background-color:gray; font-size:" + getElSize(35)
	            			},headerAttributes: {
	            				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40) + "; color : white;"
	            			}
					},{
						field: "incnt", title: "${income_cnt}",template:"#=calculation(cnt,defective)#",width:getElSize(180)
							,attributes: {
	            				style: "text-align: center; color:black; background-color:gray; font-size:" + getElSize(35)
	            			},headerAttributes: {
	            				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40) + "; color : white;"
	            			}
					},{
						title: "${add_faulty}",width:getElSize(350),template:"<input type='button' id='#=totaldelivery#' value='${add_faulty}' onclick=windowcreate(this)>"
							,attributes: {
	            				style: "text-align: center; font-size:" + getElSize(35)
	            			},headerAttributes: {
	            				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40) + "; color : white;"
	            			}
					},{
						field: "date",title: "${income_date}",width:getElSize(230)
							,attributes: {
	            				style: "text-align: center; color:black; font-size:" + getElSize(35)
	            			},headerAttributes: {
	            				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(37) + "; color : white;"
	            			}
					},{
						title: "${del}" ,width:getElSize(200),template:"<input type='button' id='#=stockid#' value='${del}' onclick=deldate(this)>" 
							,attributes: {
	            				style: "text-align: center; color:black; font-size:" + getElSize(35)
	            			},headerAttributes: {
	            				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40) + "; color : white;"
	            			}
					}],change: function(e) {
					    console.log("TL")
					  }
				})

		       
				//수정시 체크박스 초기화되서 초기화방지
				$("#wrapper .k-grid-content").off("change").on("change", "input.checkbox", function(e) {
					var grid = $("#wrapper").data("kendoGrid");
					dataItem = grid.dataItem($(e.target).closest("tr"));
			        dataItem.Discontinued = this.checked;
			        console.log(dataItem.Discontinued)
			        //dataItem.set("Discontinued", this.checked);
			    });
				
				//170929 delbert checkboxAll선택 
				$("#checkall").click(function(e){
					var grid = $("#wrapper").data("kendoGrid");
					//클릭되었으면
			        if($("#checkall").prop("checked")){
			        	gridlist=grid.dataSource.data();
			        	$(gridlist).each(function(idx,data){
			        		data.Discontinued = true
			        	})	        	
			        }else{
			        	gridlist=grid.dataSource.data();
			        	$(gridlist).each(function(idx,data){
			        		data.Discontinued = false
			        	})
			        	
			        }
			        grid.dataSource.fetch();
			    })
				
				//170925 delbert window창 만들기

				wnd = $("#details")
                .kendoWindow({
	                title: "불량등록",
	                modal: true,
	                visible: false,
	                resizable: false,
	                width: 700
                }).data("kendoWindow");
				
				$(".alarmTable").css({
					"font-size": getElSize(40),
				});
				
				$("#details table").css({
					"color" : "black"
				})
				
				$(".alarmTable td").css({
					"padding" : getElSize(20),
					"font-size": getElSize(40),
					"border": getElSize(5) + "px solid black"
				});
				
/* 				$("#wrapper").css({
					"height" :getElSize(1550),
					"overflow" : "hidden"
				}); */
				
				$(".row1").not(".tr_table_fix_header").css({
					"background-color" : "#222222"
				});

				$(".row2").not(".tr_table_fix_header").css({
					"background-color": "#323232"
				});
				
				
				scrolify($('.alarmTable'), getElSize(1450));

				//$("#wrapper div:last").css("overflow", "auto");
				
				$("button, input[type='time'], select").css({"font-size": getElSize(40), "padding" : getElSize(15)});
				
				$("body, html").css({
					"overflow" : "hidden"	
				})
				$.hideLoading(); 
			}
		});
	};
	
	//delbert 2017-09-27 15:40 업체명 이름으로변환
	function changename(abc){
		var name=abc;
		for(i=0;i<companylist.length;i++){
			if(companylist[i].id==abc)
				name=companylist[i].name
		}
		return name;
	}
	//delbert 2017-09-26 11:42 삭제버튼
	function deldate(e){
		id=e.id;
		var delidx;
		var grid = $("#wrapper").data("kendoGrid");		
		var griddata=grid.dataSource.data();
		for(i=0;i<griddata.length;i++){
			if(griddata[i].stockid==e.id)
				delidx=i;
		}
		var url = "${ctxPath}/chart/delDeliverHistory.do";
		var param = "id=" + id;	
		if (confirm("정말로 삭제하시겠습니까?") == true){    //확인
	 		$.ajax({
				url : url,
				data : param,
				type : "post",
				dataType : "text",
				success : function(data){
					//삭제시 불량등록 담겨있는것 제거
					if(faultylist.length!=0){
						for(i=0;i<faultylist.length;i++){
							if(faultylist[i].prikey==griddata[delidx].stockid){//이미 불량등록되었는지 확인		
								console.log("-----중복----")
								var zz=faultylist.splice(i,1);//되었으면 빼고
								console.log(zz);
							}else{
								console.log("-----중복없음----")
							}
						}
					}
					grid.dataSource.remove(griddata[delidx]);
					console.log(data)
				}
			});
		}else{  //취소
		    return;
		}

 	}

	//불량등록 테이블
	function windowcreate(item){
		var grid= $("#wrapper").data("kendoGrid");
		var gridinfo=grid.dataSource.data();
		//클릭한 행의 값정보 가져오기 위해 grid for문 돌림
		for(i=0;i<gridinfo.length;i++){
			if(item.id==gridinfo[i].totaldelivery){
				idx=i;
				break;
			}
		}
		console.log(idx);
		console.log(gridinfo[idx]);

		//작업중1
		//팝업창에 정보 넣기
		wnd.content(
				"<table width=100% height=100%><tr><td>검사구분</td><td id='wndCheckTyList'>"+CheckTyList+"</td><td>공정</td><td id='wndProcessList'>"+ProcessList+"</td></tr>"+
				"<tr><td>품번</td><td id='wndprdNo'>"+gridinfo[idx].prdNo+"</td><td>장비</td><td id='wndDevieList'>"+getDevieList(gridinfo[idx].prdNo)+"</td></tr>"+
				"<tr><td>발생일시</td><td id='wnddate'>"+"<input type='date' id='sDate' class='date'>"+"</td><td>신고자</td><td id='wndWorkerList'>"+WorkerList+"</td></tr>"+
				"<tr><td>부위</td><td id='wndPartList'>"+PartList+"</td><td>현상구분</td><td id='wndSituationList'>"+SituationList+"</td></tr>"+
				"<tr><td>현상</td><td id='wndSituationTyList'>"+SituationTyList+"</td><td>현상</td><td id='wndCauseList'>"+CauseList+"</td></tr>"+
				"<tr><td>귀책구분</td><td id='wndGChTyList'>"+GChTyList+"</td><td>귀책</td><td id='GChTych'>"+getGChList('undefined')+"</td></tr>"+
				"<tr><td>수량</td><td id='wndnumber'>"+"<input type='number' size='5%'>"+"</td><td>조치</td><td id='wndActionList'>"+ActionList+"</td></tr>"+
				"<tr align=center><td colspan='4' align=center><input type='button' value='등록' id='register'> <input type='button' value='취소' id='cancel'></td></tr>"
		);
		
		$("#details table").css({
		    "color":"black"})
		
		$("#cancel").click(function(){
			wnd.close();
		})
		
		$("#register").click(function(){
			var obj = new Object();
			
			obj.chkTy=$("#wndCheckTyList option:selected").val();//검사구분
			obj.prdPrc=$('#wndProcessList option:selected').val();//공정
			obj.prdNo=$('#wndprdNo').html();//품번
			obj.dvcId=$('#wndDevieList option:selected').val();//장비
			obj.date=$('#wnddate input').val();//발생일시
			obj.checker=$('#wndWorkerList option:selected').val();//신고자
			obj.part=$('#wndPartList option:selected').val();//부위
			obj.situationTy=$("#wndSituationList option:selected").val();//현상구분
			obj.situation=$("#wndSituationTyList option:selected").val();//현상 왼쪽
			obj.cause=$("#wndCauseList option:selected").val();// 현상오른쪽
			obj.gchTY=$("#wndGChTyList option:selected").val();//귀책구분
			obj.gch=$("#GChTych option:selected").val();//귀책
			obj.cnt=$("#wndnumber input").val();//수량
			obj.action=$("#wndActionList option:selected").val();//조치
			obj.prikey=gridinfo[idx].stockid;
			obj.sendCnt=$("#wndnumber input").val();//수량
			
			console.log(obj)
			//불량등록 조건문
			if($("#wndCheckTyList option:selected").val()==0){
				alert("검사구분 선택하십시오.")
				return;
			}else if($('#wndProcessList option:selected').val()==0){
				alert("공정를 선택하십시오.")
				return;
			}else if($('#wndDevieList option:selected').val()=="0"){
				alert("장비를 선택하십시오.");
				$('#wndDevieList option:selected').focus();
				return;
			}else if($('#wndPartList option:selected').val()==0){
				alert("부위를 선택하십시오.")
				return;
			}else if($("#wndSituationList option:selected").val()==0){
				alert("현상구분를 선택하십시오.")
				return;
			}else if($("#wndSituationTyList option:selected").val()==0||$("#wndCauseList option:selected").val()==0){
				alert("현상를 선택하십시오.")
				return;
			}else if($("#wndGChTyList option:selected").val()==0){
				alert("귀책구분를 선택하십시오.")
				return;
			}else if($("#GChTych option:selected").val()==0){
				alert("귀책를 선택하십시오.")
				return;
			}else if($("#wndActionList option:selected").val()==0){
				alert("조치를 선택하십시오.")
				return;
			}else if($("#wndnumber input").val()==""){
				alert("수량을 입력하세요.");
				$("#wndnumber input").focus();
				return;
			}else if( $("#wndnumber input").val()<0){
				alert("수량값이 잘못되었습니다.");
				$("#wndnumber input").focus();	
				return;
			}
			
			//불량등록 담기전 이미 담겨있는지 확인 담겨 있으면 그것을 지웠다 아님 변경
			if(faultylist.length==0){	
				console.log("----처음----")
				faultylist.push(obj)
			}else{
				for(i=0;i<faultylist.length;i++){
					if(faultylist[i].prikey==gridinfo[idx].stockid){//이미 불량등록되었는지 확인		
						console.log("-----중복----")
						var zz=faultylist.splice(i,1);//되었으면 빼고
						console.log(zz);
					}else{
						console.log("-----중복없음----")
					}
				}
				faultylist.push(obj)//다시 집어넣는다

			}
			console.log(faultylist);

			gridinfo[idx].defective=$("#wndnumber input").val();
			grid.saveChanges();	
	        gridinfo[idx].set("Discontinued", true);	//불량등록시 자동 체크박스 선택해주기	
	        gridinfo[idx].set("checkdisable",true);			
			wnd.close();
		})
        wnd.center().open();
		setDate();
	}
		
	
	//test button 170925 delbert 테스트 버튼
	var $checked;
	var autoSave = true;
	
	function checkRow(){
		var grid = $("#wrapper").data("kendoGrid");
		var rows = grid.dataSource.data();
		
		$(rows).each(function(idx, data){
			var deliveryNo = data.totaldelivery;
			
			if(deliveryNo == $("#deliveryNo").val()){
				grid.select(data);
				data.set("Discontinued", true);
				
			}
		
		});
		
	
	};
	
	
	function saveRow2(){
		var ty="";
		var direct=0;
		var deliveryNo="";
		var itemlist=[];
		var dataItem="";
		$checked = $('input:checkbox[class=checkbox]:checked');
		//입고처리시 선택 걸러내기
		if($checked.length==0){
			if($("#deliveryNo").val()!=""){
				deliveryNo=$("#deliveryNo").val();
				direct=1;
			}else{
			alert("입고처리 항목을 선택하지 않으셨습니다.")
			$.hideLoading();
			return
			}
		}
			

			if($("#deliveryNo").val()!=""){
				deliveryNo=$("#deliveryNo").val();
				direct=2;
			}
			console.log("direct :::"+direct);
			//납품번호 값이 있어 바로 입고처리할경우
			if(direct==1){
				var grid = $("#wrapper").data("kendoGrid");		
				var dataItem=grid.dataSource.data();
				$(dataItem).each(function(idx,data){
					if(deliveryNo==data.totaldelivery){
						var arr=new Object();
						arr.id=data.stockid;
						arr.prdNo=data.prdNo;
						arr.date1=data.date;
						arr.date=data.date
//						arr.date=$("#sDate").val() + " " + data.date;
						arr.lotNo=data.lotNo; //Lot No
						arr.lotCnt=data.cnt; //로트수량
						arr.smplCnt=data.check; //검사수량
						arr.notiCnt=data.defective; //불량수량
						arr.rcvCnt=data.cnt-data.defective; //입고수량
						arr.vndNo=data.selection; //업체명
						arr.deliveryNo=data.totaldelivery; //납품서 번호
						arr.cnt=data.cnt;
						arr.faillist=data.faillist;
						arr.selection=data.selection;
						arr.size=data.size;
						arr.barcode-data.barcode;
						itemlist.push(arr);
					}
				});
				if(itemlist.length==0){
					alert("납품서번호 일치하는 항목이 없습니다.");
					$.hideLoading();
				}
				//납품번호 같은거 찾기
			}else{
				$($checked).each(function(idx, data){
					var checked = data,
			        row = $(data).closest("tr"),
			        grid = $("#wrapper").data("kendoGrid"),
			        dataItem = grid.dataItem(row);

					var arr=new Object();
					arr.id=dataItem.stockid;
					arr.prdNo=dataItem.prdNo;
					arr.date1=dataItem.date;
					arr.date=dataItem.date;
//					arr.date=$("#sDate").val() + " " + dataItem.date;
					arr.lotNo=dataItem.lotNo; //Lot No
					arr.lotCnt=dataItem.cnt; //로트수량
					arr.smplCnt=dataItem.check; //검사수량
					arr.notiCnt=dataItem.defective; //불량수량
					arr.rcvCnt=dataItem.cnt-dataItem.defective; //입고수량
					arr.vndNo=dataItem.selection; //업체명
					arr.deliveryNo=dataItem.totaldelivery; //납품서 번호
					arr.cnt=dataItem.cnt;
					arr.faillist=dataItem.faillist;
					arr.selection=dataItem.selection;
					arr.size=dataItem.size;
					arr.barcode=dataItem.barcode;
					itemlist.push(arr);
				});
			}
			var obj = new Object();
			obj.val = itemlist;
			var url = "${ctxPath}/chart/addStock.do";
			var param = "val=" + JSON.stringify(obj);
			
			console.log(param)
			//보내는데이터가 0개일때
			console.log(itemlist.length)
			
			
		$("button").attr("disabled",true)
		if(faultylist.length!=0){	//불량등록0이아니면 확인하기
			if (confirm("입고처리 하면 수정 불가능 합니다. 저장하시겠습니까? "+"\n"+" 불량등록      "+faultylist.length+"     건") == true){
				$.showLoading();
				$.ajax({
					url : url,
					data : param,
					type : "post",
					dataType : "text",
					success : function(data){
						$.hideLoading();
						if(data=="success") {
							$("#deliveryNo").val("");
							if(ty!="faulty"){
								if(faultylist.length!=0){
									var url="${ctxPath}/chart/saveRow.do";
									var obj=new Object();
									obj.val=faultylist;
									var param = "val=" + JSON.stringify(obj)
									console.log("-------fail list------");
									console.log(param);
									$.ajax({
										url : url,
										data : param,
										type : "post",
										dataType : "text",
										success : function(data){
											setTimeout(function() {
												autoSave = true;
											},300)
											$("button").attr("disabled",false)
											if(data=="success"){
												alert("${save_ok}");
												alert("불량등록");
												console.log("---------fail-----")
												console.log(faultylist);									
												getIncomStock();
												$.hideLoading();
												$("button").attr("disabled",false)
											}
										}
									});
								}else{
									alert("${save_ok}");
									getIncomStock();
									$.hideLoading();
									$("button").attr("disabled",false)
									setTimeout(function() {
										autoSave = true;
									},300)
								}
							};
						}
					}
				}); 
				console.log(itemlist);
			}else{
				return;
			}
		}else{//0이면 바로저장하기
			$.showLoading();			
			$.ajax({
				url : url,
				data : param,
				type : "post",
				dataType : "text",
				success : function(data){
					$.hideLoading();
					setTimeout(function() {
						autoSave = true;
					},300)
					$("button").attr("disabled",false)

					if(data=="success") {
						$("#deliveryNo").val("");
						if(ty!="faulty"){
							if(faultylist.length!=0){
								var url="${ctxPath}/chart/saveRow.do";
								var obj=new Object();
								obj.val=faultylist;
								var param = "val=" + JSON.stringify(obj)
								console.log("-------fail list------");
								console.log(param);
								$.ajax({
									url : url,
									data : param,
									type : "post",
									dataType : "text",
									success : function(data){
										if(data=="success"){
											alert("${save_ok}");
											alert("불량등록");
											console.log("---------fail-----")
											console.log(faultylist);									
											getIncomStock();
											$.hideLoading();
											$("button").attr("disabled",false)
											setTimeout(function() {
												autoSave = true;
											},300)

										}
									}
								});
							}else{
								getIncomStock();
								$.hideLoading();
								$("button").attr("disabled",false)
								setTimeout(function() {
									autoSave = true;
								},300)
							}
						};
					}
				}
			}); 
			console.log(itemlist);
		}

	}
	
	//입고수량 계산해서 바로 반영하기 170925 delbert
	function calculation(cnt,defective){
		var total=cnt-defective;
		return cnt-defective;
	}
	
	//업체명 리스트 뽑기 kendo 170925 delbert
	function dropdownlist(container, options){
		$('<input required name="' + options.field + '" />')
		 .appendTo(container)
		 .kendoDropDownList({
			 autoWidth: true,
			 autoBind: false,
			 dataTextField: "name",
			 height: 3300,
			 dataValueField: "id",
			 dataSource: companylist
		});
		console.log(container);
		console.log(options);
	}
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
 	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		$(".date").val(year + "-" + month + "-" + day);
	}; 
	
/* 	function caldate(day){
		 var caledmonth, caledday, caledYear;
		 var loadDt = new Date();
		 var v = new Date(Date.parse(loadDt) - day*1000*60*60*24);
		 
		 caledYear = v.getFullYear();
		 
		 if( v.getMonth() < 9 ){
		  caledmonth = '0'+(v.getMonth()+1);
		 }else{
		  caledmonth = v.getMonth()+1;
		 }
		 if( v.getDate() < 9 ){
		  caledday = '0'+v.getDate();
		 }else{
		  caledday = v.getDate();
		 }
		 return caledYear + "-" + caledmonth+ '-' + caledday;
		} */
		
	var prdnolist=[];
	var companylist=[];
	function getPrdNo(){
		var url = "${ctxPath}/chart/getMatInfo.do";
		var param = "shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
				
				var option = "";
				
				$(json).each(function(idx, data){
					var arr=new Object();
					option += "<option value='" + decode(data.prdNo) + "'>" + decode(data.prdNo) + "</option>"; 
					arr.prdNo=decode(data.prdNo);
					prdnolist.push(arr);
				});
				
				$("#group").html(option);
				
				getIncomStock();
			}
		});
	};
	
//	var handle = 0;
	$(function(){
		//여기
		var target_input = $('#keyword'); // 포커스 인풋
	    var chk_short = true;
		 
	    $(document).bind("keydown keyup", function(e) {
	        var key = e.keyCode;
	        var tg = e.target;
	        if(tg.tagName == "INPUT" ||  tg.tagName == "TEXTAREA") return true;
	        
	        var specific = key >= 8 && key <= 46;
	        if(e.type == "keydown") {
	            if(specific) {
	                chk_short = false;
	                return true;
	            }
	            if(!specific && chk_short) {
	            	 $("#deliveryNo").focus().select();
	                //target_input.focus().select(); return false;
	            }
	            if(e.ctrlKey && e.keyCode == 86){
	            	 $("#deliveryNo").focus().select();
	            }
	        } else {
	            if(specific) {
	                chk_short = true;
	            }
	        }
	    });
	    
	    $("input, textarea").bind("blur", function() {
	        chk_short = true;
	    });
		$("#deliveryNo").focus()
		
//		createNav("inven_nav", 1);
		
		getPrdNo();
		getComList();//업체
		getActionList();//조치
		getCheckTyList();//검사구분
		getProcessList();//공정
//		getDevieList();
		getWorkerList();//작업자
		setDate($(sDate));
		getPartList();//부위
		getSituationList();//현상구분
		getSituationTyList();//현상 왼쪽
		getCauseList();//현상 오른쪽
		getGChTyList();//귀책구분
		/*getGChList(7, $(gch));
		getActionList($(action)); 
		getPrdNoList($(prdNo)); */
		
//		setEl();
		setDate();	
//		time();

		setEl2();
		
		setEvt();
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	function setEvt(){
		$("#addBtn").click(addRow);
		$("#save").click(addStock);
		$("#modify").click(updateStock);
	};
	
	function addStock(){
		var url = "${ctxPath}/chart/addStock.do";
		var param = "prdNo=" + $("#insert #prdNo").html() + 
					"&sDate=" + $("#sDate").val() + " " + $("#insert input[type='time']").val() + 
					"&lotNo=" + $("#insert #lotNo").val() + 
					"&lotCnt=" + $("#insert #lotCnt").val() + 
					"&smplCnt=" + $("#insert #smplCnt").html() +
					"&notiCnt=" + $("#insert #notiCnt").val() +
					"&rcvCnt=" + $("#insert #rcvCnt").html();
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				if(data=="success"){
					$("#insert input").not("input[type=time]").val("");
					closeInsertForm();
					getIncomStock();
				}	
			}
		});
		
		return false;
	};


	var a;
	function calcAQL(el){
		var lotCnt = $(el).parent("td").parent("tr").children("td:nth(5)").children("input").val();
		var notiCnt = $(el).parent("td").parent("tr").children("td:nth(7)").children("input").val();
		
		$(el).parent("td").parent("tr").children("td:nth(6)").html("5");
		if(notiCnt>0){
			$(el).parent("td").parent("tr").children("td:nth(8)").html("0").css("color","red");	
		}else{
			$(el).parent("td").parent("tr").children("td:nth(8)").html(lotCnt).css("color","white");
		}
	};

	var preRCV_QTY = 0;
	var preNOTI_QTY = 0;

	function updateStock(){
		var url = "${ctxPath}/chart/updateStock.do";
		var param = "id=" + matId + 
					"&lotNo=" + $("#update #lotNo").val() + 
					"&lotCnt=" + $("#update #lotCnt").val() +
					"&notiCnt=" + $("#update #notiCnt").val() +
					"&rcvCnt=" + $("#update #rcvCnt").html() +
					"&prdNo=" + $("#update #prdNo").html() + 
					"&updatedNotiCnt=" + ($("#update #notiCnt").val() - preNOTI_QTY) +
					"&updatedRcvCnt=" + ($("#update #rcvCnt").html() - preRCV_QTY) +
					"&sDate=" + $("#sDate").val() + " " + $("#update input[type='time']").val();
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success :function(data){
				if(data=="success"){
					$("#insert input").not("input[type=time]").val("");
					closeInsertForm();
					getIncomStock();
				}
			}
		});
		
		return false;
	};
	
 	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	}; 
	
	function calcAQL_update(){
		var lotCnt = $("#update #lotCnt").val();
		var notiCnt = $("#update #notiCnt").val();
		
		$("#update #smplCnt").html("5");
		if(notiCnt>0){
			$("#update #rcvCnt").html("0").css("color","red");	
		}else{
			$("#update #rcvCnt").html(lotCnt).css("color","white");
		}
	};
	
	function addRow(){
		var tr = "<tr class='contentTr2'>" + 
					"<td></td>" +
					"<td>" + comList + "</td>" +
					"<td>" + $("#group option:selected").html() + "</td>" +
					"<td></td>" +
					"<td><input type='text' size='5' ></td>" +
					"<td><input type='text' size='5' value='0' onkeyup='calcAQL(this)'></td>" +
					"<td>0</td>" +
					"<td><input type='text' size='5' value='0' onkeyup='calcAQL(this)'></td>" +
					"<td>0</td>" +
					"<td><button onclick='addFaulty(this)'>${add_faulty}</button></td>" +
					"<td><input type='time' value='" + getTime() + "'></td>" +
					"<td><button onclick='chkDel(\"0\", this)'>${del}</button></td>" +
				"</tr>";
		 
		if($(".contentTr").length==0){
			$("#table2").append(tr);
		}else{
			$(".alarmTable").last().append(tr);
		}
		
		$("button, input[type='time'], select").css("font-size", getElSize(50));
	};

	var valueArray = [];
	/* function saveRow(ty){
		$.showLoading(); 
		valueArray=[];		
		$(".contentTr").each(function(idx, data){
			var obj = new Object();
			obj.id = $(data).children("td:nth(0)").html();
			obj.prdNo = $(data).children("td:nth(2)").html()
			obj.date = $("#sDate").val() + " " + $(data).children("td:nth(10)").children("input").val();
			obj.lotNo = $(data).children("td:nth(4)").children("input").val();
			obj.lotCnt = $(data).children("td:nth(5)").children("input").val();
			obj.smplCnt = $(data).children("td:nth(6)").html();
			obj.notiCnt = $(data).children("td:nth(7)").children("input").val();
			obj.rcvCnt = $(data).children("td:nth(8)").html();
			obj.vndNo =  $(data).children("td:nth(1)").children("select").val();
			if($("#deliveryNo").val()==""){
				obj.deliveryNo = $(data).children("td:nth(0)").attr('id');
			}else{
				obj.deliveryNo = $("#deliveryNo").val();				
			}
	
			
			valueArray.push(obj);
		});
		
		var obj = new Object();
		obj.val = valueArray;
		console.log(valueArray);
		var url = "${ctxPath}/chart/addStock.do";
		var param = "val=" + JSON.stringify(obj);
	
		console.log(param)
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				$.hideLoading();
				if(data=="success") {
					if(ty!="faulty"){
						alert("${save_ok}");
						getIncomStock();					
					};
				}else if(data=="overlap"){
					alert("이미 입고처리 된 항목이 포함되어있습니다.")
				}
			}
		});
	}; */

	var matId;
	var delObj;
	function chkDel(id, el){
		$("#delDiv").css({
			"z-index" : 9,
			"display" : "block"
		});
		
		matId = id;
		delObj = el;
	};

	function noDel(){
		$("#delDiv").css({
			"z-index" : -1,
			"display" : "none"
		});
	};

	function okDel(){
		if(matId==0){
			$(delObj).parent("td").parent("tr").remove();
			noDel();
			return;
		}
		var url = ctxPath + "/chart/okDelStock.do";
		var param = "id=" + matId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				noDel();
				getIncomStock();
			}
		});
	};

	
	function addFaulty(el){
		var prdNo = $(el).parent("td").parent("tr").children("td:nth(2)").html();
		var cnt = $(el).parent("td").parent("tr").children("td:nth(7)").html();
		if(cnt.length>10){
			cnt = $(el).parent("td").parent("tr").children("td:nth(7)").children("input").val();
		}
		
		saveRow("faulty");
		var url = "${ctxPath}/chart/addFaulty.do?addFaulty=true&prdNo=" + prdNo + "&cnt="+ cnt;
		
		location.href = url;
	};
	
/* 	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	}; */
	
/* 	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(200),
		});
		
		$("#content_table td").css({
			"color" : "##BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#delDiv").css({
			"position" : "absolute",
			"width" : getElSize(700),
			"height" :getElSize(200),
			"background-color" : "#444444",
			"color" : "white" 
		});
		
		$("#delDiv").css({
			"top" : (window.innerHeight/2) - ($("#delDiv").height()/2), 
			"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
			//"z-index" : -1,
			"display" : "none",
			"border-radius" : getElSize(10),
			"padding" : getElSize(20)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
	}; */
	
	
	
	
	function setEl2(){
		var width = window.innerWidth;
		var height = window.innerHeight;
		

/*		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
*/


		$("#container").css({
			"width" : contentWidth - getElSize(30)
		})
		
		$("#content_table").css({
			"margin-top" : getElSize(300)
		})



		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});

		
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
/* 		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		}); */
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(22),
			"margin-right" : getElSize(22)
		});
		
		$("select").css({
			"width" : getElSize(380),
			"margin-right" : getElSize(50)
		});
		
		$("#sDate").css({
			"width" : getElSize(380),
			"text-align" : "center",
			"margin-right" : getElSize(20)
		});
		
		$("#eDate").css({
			"width" : getElSize(380),
			"text-align" : "center",
			"margin-left" : getElSize(20)
		});
		
		$("input").css({
			"background-color" : "black",
			"border-color" : "black",
			"margin-right" : getElSize(50),
			"height" : getElSize(80)
		})
		
			
		$("#td_first").css({
			"padding-left" : getElSize(50),
			"padding-bottom" : getElSize(30)
		});
		
		$(".k-header").css({
		    "font-family" : "NotoSansCJKkrBold",
		    "font-size" : getElSize(32) + "px",
		    "background-image" : "none",
		    "background-color" : "#353542"
		}); 
		
		$("button").css({
			"margin-left" : getElSize(7),
			"margin-right" : getElSize(7),
			"margin-top" : getElSize(10),
			"font-size" : getElSize(47),
			"background" : "#909090",
			"border-color" : "#222327"
		})
		
 		$("#search").css({
			"padding" : getElSize(25),
			"cursor" : "pointer",
			"width" : getElSize(200),
			"margin-right" : getElSize(160),
			"height" : getElSize(80),
			"background" : "#909090",
			"border-color" : "#222327"
		}); 
		
		$(".fa fa-search").css({
			"padding" : getElSize(30)
		})
		
		$("#wrapper").css({
			"margin-left" : getElSize(50),
			"margin-right" : getElSize(50)
		});
		
		$("#content_table td").css({
			"color" : "##BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#delDiv").css({
			"position" : "absolute",
			"width" : getElSize(700),
			"height" :getElSize(200),
			"background-color" : "#444444",
			"color" : "white" 
		});
		
		$("#delDiv").css({
			"top" : (window.innerHeight/2) - ($("#delDiv").height()/2), 
			"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
			//"z-index" : -1,
			"display" : "none",
			"border-radius" : getElSize(10),
			"padding" : getElSize(20)
		});
		

	};
	
	
	
	
	
	
	
	
	
/* 	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	}; */
	
	function goGraph(){
		location.href = "${ctxPath}/chart/jigGraph.do"
	};
	
	function getDeliverInfo(evt){
		if(evt.keyCode==13){
			$("#deliveryNo").focus();
			$("#deliveryNo").trigger('click');
			$("#deliveryNo").select();
			$("#deliveryNo").focus()
			if(autoSave){
				var grid = $("#wrapper").data("kendoGrid");
				var rows = grid.dataSource.data();
				autoSave = false;
				$(rows).each(function(idx, data){
					var deliveryNo = data.totaldelivery;
					
					if(deliveryNo == $("#deliveryNo").val()){
						grid.select(data);
						//var tr = $(data).closest("tr");
						//$(tr).children("td:nth(0)").children("input").attr("checked", true);
						//console.log()
						data.set("Discontinued", true);
					}
				
				});
				saveRow2();
				

			}else{
//				alert("다시시도해 주세요")
//				autoSave = true
//				getIncomStock()
			}
		}
	}
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="delDiv">
		<Center>
			<font><spring:message code="chk_del" ></spring:message></font><Br><br>
			<button id="resetOk" onclick="okDel();"><spring:message code="del" ></spring:message></button>&nbsp;&nbsp;
			<button id="resetNo" onclick="noDel();"><spring:message code="cancel" ></spring:message></button>
		</Center>
	</div>
	
	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			
			
			<Tr>
				<td rowspan="10" id="svg_td" style="vertical-align: top; position:relative; background:#26282c;">
					<table id="content_table" style="width: 100%"> 
						<Tr>
							<td id="td_first">
								<spring:message code="Delivery_number"></spring:message>
								<input type="text" id="deliveryNo" size="50" onkeyup="getDeliverInfo(event)" style="color:#ffffff;" >
								<%-- <spring:message code="prd_no"></spring:message> --%>
								<!-- <select id="group"></select> -->
								<spring:message code="income_date"></spring:message> <input type='text' id='sDate' class="date" readonly="readonly"> ~ <input type="text" id='eDate' class="date" readonly="readonly">
								<button id="search" onclick="getIncomStock()" style="cursor: pointer;"><i class="fa fa-search" aria-hidden="true"></i></button>
								<%-- <button id="addBtn"><spring:message code="add" ></spring:message></button> --%> 
								<button onclick="saveRow2()"><spring:message code="Processing"></spring:message></button>
							</td>
						</Tr>		
						<tr>
							<td>
								<div id="details"></div>
								<div id="wrapper">
<!-- 									<table style="width: 100%; color: white; text-align: center; border-collapse: collapse;" class="alarmTable" id="table2" border="1" >
									</table>
 -->								</div>
							</td>
						</tr>			
					</table> 
				</td>
			</Tr>
			
		
		</table>
	 </div>
	 
</body>
</html>	