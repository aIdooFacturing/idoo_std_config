<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden; 
	background-color: black;
  	font-family:'Helvetica';
}
html{
	overflow : hidden;
}



/* select{ 
	padding: .7em .7em; 
	font-family: inherit; 
	background : #1a1a1a;
	background: url(${ctxPath}/images/FL/default/btn_drop_menu_default.svg) no-repeat 96% 50%; 
	z-index : 999;
	border-radius: 0px;
	border-color : #1a1a1a;
	-webkit-appearance: none; 
    -moz-appearance: none;
	appearance:none;
	color:#ffffff;
	broder : 1px solid transparent;
}	
select::-ms-expand{
	display : none;
} */

#sDate{
	background : #1a1a1a;
	color:#ffffff;
}
#eDate{
	background : #1a1a1a;
	color:#ffffff;
}

.k-grid tbody > tr{
 background : #c1c1c2;
 text-align: center;
 border-color:black;
}
.k-grid tbody > .k-alt
{
 background :  #d5d5d6 ;
 text-align: center;
 border-color:black;
}

/*

.k-grid tbody > tr:hover
{
 background :  darkgray ;
}
 
.k-grid tbody > .k-alt:hover
{
 background :  darkgray ;
 text-align: center;
}
.k-grid-content{
	background: darkgray ;
}


.k-link{
	color:white !important;
}
*/

.k-grid-filter.k-state-active{
	background-color:transparent;
	color: yellow !important;
}
.k-grouping-row{
	    text-align: left !important;
}
.k-grid .k-grouping-row td, .k-grid .k-hierarchy-cell{
	/* background: linear-gradient( to bottom, gray, black ) !important; */ 
}
td.k-group-cell{
	background:dimgray;
}
.k-group-footer td{
	background:dimgray;
}
.k-grid td{
	color : black ;
}
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
    /* display: none; <- Crashes Chrome on hover */
    -webkit-appearance: none;
    margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
}

#grid tbody tr .table-cell{
	/* background-color: #F6F6F6; */
	color: black;
	
}
#grid tbody tr.k-alt .table-cell{
	/* background-color: #EAEAEA; */
	color: black;
	border-color:black;
}


#wrapper tbody tr .table-cell{
	background-color: #F6F6F6;
	color: black;
	border-color:black;
}
#wrapper tbody tr.k-alt .table-cell{
	background-color: #EAEAEA;
	color: black;
	border-color:black;
}

#toolGrid tbody tr .table-cell{
	background-color: #F6F6F6;
	color: black;
	border-color:black;
}
#toolGrid tbody tr.k-alt .table-cell{
	background-color: #EAEAEA;
	color: black;
	border-color:black;
}

.k-grid thead tr th{
/*	background : linear-gradient( to bottom, gray, black ); */
	background : #2c2c36;
	color: white;
	text-align: center;
	border-color:black;
}

.k-grid-header{
/* border-color:black; */
}

.k-grid tbody td{
border-color:black;
}

.k-grid thead tr.k-filter-row th{
/*	background : linear-gradient( to bottom, #6696ff, #6696ff ); */
/*	background : linear-gradient( to bottom, #E8D9FF, #A566FF ); */
	background : #1a1a1a;
	color: white;
	text-align: center;
	border-color:black;
}

#grid tbody {

/* 	border-color:black; */
}

/* .k-list-container{
   height: 100px !important;
   width: 250px !important;
}
.k-list-scroller {
    height: 100px !important;
    width: 250px !important;
} */

</style> 
<script type="text/javascript">

	const loadPage = () =>{
		createMenuTree("tm", "toolLifeManager")	
	}
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};


	
	$(function(){
		
//		createNav("tool_nav", 0);
		
//		setEl();
//		time();

		setEl2();
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
//		chkBanner();
	});
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
/* 	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	}; */

/*
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : contentWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#banner").css({
			"font-size" : getElSize(80),
			"width" : getElSize(2000)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		$("#resetT").css({
			"width" : getElSize(3710),
			"height" :getElSize(320)
		})
		$("#resetT input").css("width",getElSize(1560))

		$("#dialog").css({
			"width" : getElSize(3710),
			"height" : getElSize(2100),
			"background" : '#ceced2'
		})
		
		$("#ToolGrid thead tr th").css("font-size",getElSize(40))
		$("#ToolGrid thead tr th").css("text-align","center")
		
		$(".Tcss").css({
			"background" : "#242424", 
			"color" : "white",
			"height" : getElSize(250),
			"font-size" : getElSize(80)
		})
		
		$(".Hcss").css({
			"background" : "linear-gradient( to bottom, gray, black)",
			"color" : "white",
			"height" : getElSize(140)
			
		})
		
		$(".Bcss").css({
			"color" : "white",
			"height" : getElSize(140)
		})
		
		$("#grid").css("width",getElSize(3800))
	};
*/	
	
	function setEl2(){
	$("#container").css({
		"width" : contentWidth - getElSize(30)
	})
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"height" : $("#container").height() + getElSize(5),
			"top" : getElSize(360) + marginHeight,
			"padding-top" : getElSize(30)
		});
		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		$("#contentTable").css("margin-top", getElSize(30));
		
		$("#wrappe").css("margin-top", getElSize(100));
		
		$(".text1").css({
			"font-size" : getElSize(50),
			"padding" : getElSize(14),
			"margin-left" : getElSize(30)
		});
		
	/* 	$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		}); */
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(300),
			"height" : getElSize(88),
			"font-size" : getElSize(47),
			"margin-top" : getElSize(10),
			"background" : "#909090",
			"border-color" : "#909090"
		});
		
		$("#saverow").css({
			"cursor" : "pointer",
			"width" : getElSize(300),
			"height" : getElSize(88),
			"font-size" : getElSize(47),
			"margin-top" : getElSize(10),
			"margin-right" : getElSize(55),
			"background" : "#909090",
			"border-color" : "#909090"
		});
		
		
		$("select").css("font-size",getElSize(47));
		$("select").css("height",getElSize(150));
		$("select").css("width",getElSize(200));
		$("select").css("border-color","#222327");

		
		$("select").css({
            "border": "1px black #999",
            "width" : getElSize(600),
            "height" : getElSize(80),
            "font-family": "inherit",
            "background": "url(${ctxPath}/images/FL/default/btn_drop_menu_default.svg) no-repeat 95% 50%",
            "background-color" : "black",
            "z-index" : "999",
            "border-radius": "0px",
            "-webkit-appearance": "none",
            "-moz-appearance": "none",
            "appearance":"none",
            "background-size" : getElSize(60),
            "color" : "white",
            "border" : "none"
        })
        
        $("select option").css({
            "background-color" : "rgba(0,0,0,5)",
            "color" : "white",
            "border" : "1",
            "transition-delay" : 0.05,
            "border-bottom" : getElSize(10) + "px solid red",
            "text-align-last": "center",
/*             "border-top-color": "rgb(215, 0, 0)",
            "border-right-color": "rgb(215, 0, 0)",
            "border-left-color": "rgb(215, 0, 0)", */
            "padding" : getElSize(15) + "px",
            "font-size": getElSize(52) + "px",
            "transition" : "1s"
        })
		
        
        
		$(".k-list-container").css({
                   "width" : getElSize(600),
        });
		
        $(".k-list-scroller").css({
                   "max-height" : getElSize(600),
        });
      
        
        
/* 		$("select").css("font-size",getElSize(47));
		$("select").css("height",getElSize(150));
		$("select").css("width",getElSize(200));
 */		
		$("input").css("height",getElSize(88))
		$("input").css("font-size",getElSize(47));

		
		
		$("#svg_td").css({
			"font-size" : getElSize(50),
			"padding" : getElSize(14),
			"margin-left" : getElSize(80)
		});
		
		
		
		
		$(".k-list-scroller").css({
			"font-size" : getElSize(50),
			"padding" : getElSize(14),
			"margin-left" : getElSize(50)
		});
		
		
		
		
		$("#resetT").css({
			"width" : getElSize(3710),
			"height" :getElSize(320)
		})
		$("#resetT input").css("width",getElSize(1560))

		$("#dialog").css({
			"width" : getElSize(3710),
			"height" : getElSize(2100),
			"background" : '#ceced2'
		})
		
		$("#ToolGrid thead tr th").css("font-size",getElSize(100))
		$("#ToolGrid thead tr th").css("text-align","center")
		
		$(".Tcss").css({
			"background" : "#242424", 
			"color" : "white",
			"height" : getElSize(250),
			"font-size" : getElSize(80)
		})
		
		$(".Hcss").css({
			/* "background" : "linear-gradient( to bottom, gray, black)", */
			"color" : "white",
			"height" : getElSize(140)
			
		})
		
		$(".Bcss").css({
			"color" : "white",
			"height" : getElSize(140)
		})
		
//		$("#grid").css("width",getElSize(3800))
		$("#grid").css("top", getElSize(50));
		
		$("#grid tbody td").css("font-size",getElSize(18 * 2));
		$("#grid tbody td").css("text-align","center");
		$("#grid tbody td").css("color","#000000");
		$("#grid tbody td").css("border-color","#1c1c20");
		$("#grid thead th").css("font-size",getElSize(18 * 2));
		$("#grid thead th").css("text-align","center");
		$("#grid thead th").css("background","#2c2c36");
		$("#grid tbody th").css("border-color","#1c1c20");
		
		$("#grid").css("width",getElSize(3750))
		$("#grid").css("height",getElSize(2000))
		$("#grid").css("margin-left",getElSize(50))
		$("#grid").css("margin-right",getElSize(50))
	 	//$("#grid").css("border-color", "#222327") 
	 	$("#grid").css("border-color", "rgb(38,39,43)")
	 	
	};
	
	
	
	
/* 	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	}; */
	function getDvcList(){
		var url = "${ctxPath}/chart/getDvcNameList.do";
		var param = "shopId=" + shopId + 
					"&line=ALL" + 
					"&fromDashboard=true" ;
		
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var options = "";
				var json = data.dataList;
				options+="<option value='ALL'>${total}</option>"
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decodeURIComponent(data.name).replace(/\+/gi," ") + "</option>" 
				});		
				$("#dvcId").html(options);
				getTable()
			}, error : function(e1,e2,e3){
				console.log(e1)
			}
		});
		
	}; 
	
	function readOnly(container, options){
		container.removeClass("k-edit-cell");
        container.text(options.model.get(options.field));
	}
//	style='border-radius:50%; cursor : pointer; background-color : " + status + "; width: " + getElSize(70) + "px; height:" + getElSize(70) + "px'
// template: "<input type='checkbox' onclick='checkRow(this)' #= (typeof checkSelect!='undefined' && checkSelect!=false)? 'checked=checked' : '' # #= (typeof newRow!='undefined') ? \'disabled=checked\' : '' # class='checkbox' style='width: "+getElSize(80)+"; height: "+getElSize(80)+";'/>" 

	function statusView(dvcId,name,portNo,lmtC,cnt){
		var rate=Number(cnt)/Number(lmtC)*100;
		
		if(rate < 90){
			return "<div onclick=reset("+dvcId+","+portNo+") class='fs' style='background-color:green; color:white; border-radius:1%; cursor : pointer;'>RESET</div>"
//			return "3333"
		}else if(rate >=90 && rate <100){
			return "<div onclick=reset("+dvcId+","+portNo+") class='fs' style='background-color:yellow; color:white; border-radius:1%; cursor : pointer;'>RESET</div>"
		}
		else {
			return "<div onclick=reset("+dvcId+","+portNo+") class='fs' style='background-color:red; color:white; border-radius:1%; cursor : pointer;'>RESET</div>"
		}
		
	}
	
	var reDvcId
	var rePortNo
	var reName
	var reLmtC
	var filterName
	var filterStatus
	function reset(dvcId,portNo){
		console.log(dvcId)
		for (i=0,len=grid.dataSource.data().length; i<len; i++){
			if(dvcId==grid.dataSource.data()[i].dvcId){
				reName=grid.dataSource.data()[i].name
				reLmtC=grid.dataSource.data()[i].lmtC
				break;
			}
		}
		if(confirm("초기화 하시겠습니까?")){
			reDvcId=dvcId;
			rePortNo=portNo;
			$("#preCnt").val(0);
			$("#preSec").val(0);
			$("#ToolGrid").data("kendoGrid").dataSource.remove($("#ToolGrid").data("kendoGrid").dataSource.data()[0])
			insertRow();
			$("#dialog").data("kendoDialog").open();
			$("#preCnt").focus();
			$("#preCnt").select();
			
		}else{
			return;
		}	
		
		// alert(dvcId +" 장비 초기화 하자 "+ portNo)
	}
	
	var kendotable
	var beforeData=[];
	
	function insertRow(){
	     $("#ToolGrid").data("kendoGrid").dataSource.insert(0, {
	    	date : moment().format("YYYY-MM-DD"),
	    	time : moment().format("HH:mm"),
			prdNo : reName,
			oprNm : "",
			lmtC : reLmtC,
			chk1 : false,
			chk2 : false,
			chk3 : false,
			workFault : 0,
			cnt1 : 0,
			leaderFault : 0,
			cnt2 : 0,
			worker : "",
			leader : "",
		 })
	}
	function checkRow1(e){
		console.log("event click")
		var grid = $("#ToolGrid").data("kendoGrid");
		var gridList=grid.dataSource.data();
		var dataItem = $(e).closest("tr")[0].dataset.uid;
		 var row = $("#ToolGrid").data("kendoGrid")
         .tbody
         .find("tr[data-uid='" + dataItem + "']");
		 var initData = grid.dataItem(row)
		 var barcode = initData.barcode;
		 var length = gridList.length
		 initData.chk2=false
		 initData.chk3=false
		 initData.chk1=true
		 $("#ToolGrid").data("kendoGrid").dataSource.fetch()
 /* 		 for(var i=0; i<length; i++){
			 if(barcode==gridList[i].barcode){
				 if(gridList[i].checkSelect){
					 gridList[i].checkSelect=false;
					 grid.dataSource.fetch();
				 }else{
					 gridList[i].checkSelect=true;
					 grid.dataSource.fetch();
				 }
			 }
		 } */
		 //grid.dataSource.fetch()
	}
	function checkRow2(e){
		console.log("event click")
		var grid = $("#ToolGrid").data("kendoGrid");
		var gridList=grid.dataSource.data();
		var dataItem = $(e).closest("tr")[0].dataset.uid;
		 var row = $("#ToolGrid").data("kendoGrid")
         .tbody
         .find("tr[data-uid='" + dataItem + "']");
		 var initData = grid.dataItem(row)
		 var barcode = initData.barcode;
		 var length = gridList.length
		 initData.chk1=false
		 initData.chk3=false
		 initData.chk2=true
		 $("#ToolGrid").data("kendoGrid").dataSource.fetch()
	}
	function checkRow3(e){
		console.log("event click")
		var grid = $("#ToolGrid").data("kendoGrid");
		var gridList=grid.dataSource.data();
		var dataItem = $(e).closest("tr")[0].dataset.uid;
		 var row = $("#ToolGrid").data("kendoGrid")
         .tbody
         .find("tr[data-uid='" + dataItem + "']");
		 var initData = grid.dataItem(row)
		 var barcode = initData.barcode;
		 var length = gridList.length

		 initData.chk2=false
		 initData.chk1=false
		 initData.chk3=true
		 $("#ToolGrid").data("kendoGrid").dataSource.fetch()
	}
	var selectlist=[]
	function getWorkerList(){
		var url = ctxPath + "/common/getWorkerList.do";
		var param = "shopId=" + shopId;
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				var table = "<tbody>";
							
				$(json).each(function(idx, data){
					
					
					var arr=new Object();
					arr.text=decode(data.name);
					arr.value=decode(data.id);
					arr.textname=decode(data.name);
					selectlist.push(arr)
				});
					console.log(selectlist)
			}
		});
	}
	
	function workauto(container, options, a){
		autoComplete = $('<input data-bind="value:' + options.field + '"/>')
		.appendTo(container)
		.kendoComboBox({
        	dataSource: selectlist,
			autoWidth: true,
			dataTextField: "text",
			dataValueField: "value",
            placeholder: "작업자 선택",
            select:function(e){
            }
        }).data("kendoComboBox");
	}
	function worKerName(id){
		console.log(id)
		var chk=0;
		for(var i = 0; i < selectlist.length; i++){
			if(selectlist[i].value==id){
				chk++;
				return selectlist[i].textname;
			}
		}
		if(chk==0){
			return "-미선택-"
		}
	}
	$(document).ready(function(){
		getWorkerList()
		var dataSource = new kendo.data.DataSource({
			autoSync: true,
		
			schema: {
				model: {
					id: "ProductID",
					fields: {
						ToolCnt: { type:"number"}
						,found1: { type:"number"}
						,found2: { type:"number"}
						,before: { type:"number"}
						,after: { type:"number"}
						,workFault: { type:"number"}
						,cnt1: { type:"number"}
						,leaderFault: { type:"number"}
						,cnt2: { type:"number"}
				}
			}
		}
});

		$("#ToolGrid").kendoGrid({
			dataSource : dataSource
			,editable : true
			,height : getElSize(1000)
			,overlay: { gradient: "none" }
			,columns : [{
				title : "일자"
				,field : "date"
				,width : getElSize(250)
				,editor : readOnly
			},{
				title : "시간"
				,field : "time"
				,width : getElSize(180)
				,editor : readOnly
			},{
				title : "차종"
				,field : "prdNo"
				,width : getElSize(250)
				,editor : readOnly
			}/* ,{
				title : "공정명"
				,field : "oprNm"
			} */,{
				title : "Tool명"
				,field : "toolName"
				,width : getElSize(220)
				,attributes: {
				      "class": "table-cell"
				 }
			},{
				title : "Tool<br>수명"
				,field : "lmtC"
				,width : getElSize(200)
				,editor : readOnly
			},{
				title : "Tool <br> 교체 <br> 시점"
				,field : "ToolCnt"
				,width : getElSize(240)
				,attributes: {
				      "class": "table-cell"
				 }
			},{
				title : "Tool 교환 사유"
				,columns : [{
					title : "정기"
					,width : getElSize(150)
					,template: "<input type='checkbox' onclick='checkRow1(this)' #= (typeof chk1!='undefined' && chk1!=false)? 'checked=checked' : '' # #= (typeof newRow!='undefined') ? \'disabled=checked\' : '' # id='chk1' class='checkbox' style='width: "+getElSize(80)+"; height: "+getElSize(80)+";'/>" 
				},{
					title : "마모"
					,width : getElSize(150)
					,template: "<input type='checkbox' onclick='checkRow2(this)' #= (typeof chk2!='undefined' && chk2!=false)? 'checked=checked' : '' # #= (typeof newRow!='undefined') ? \'disabled=checked\' : '' # id='chk2' class='checkbox' style='width: "+getElSize(80)+"; height: "+getElSize(80)+";'/>" 
				},{
					title : "파손"
					,width : getElSize(150)
					,template: "<input type='checkbox' onclick='checkRow3(this)' #= (typeof chk3!='undefined' && chk3!=false)? 'checked=checked' : '' # #= (typeof newRow!='undefined') ? \'disabled=checked\' : '' # id='chk3' class='checkbox' style='width: "+getElSize(80)+"; height: "+getElSize(80)+";'/>" 
				}]
			},{
				title : "측정 결과"
				,columns : [{
					title : "Spec"
					,field : "spec"
					,width : getElSize(220)
					,attributes: {
					      "class": "table-cell"
					 }
				},{
					title : "실측치"
					,columns : [{
						title : "#1"
						,field : "found1"
						,width : getElSize(240)
						,attributes: {
						      "class": "table-cell"
						 }
					},{
						title : "#2"
						,field : "found2"
						,width : getElSize(240)
						,attributes: {
						      "class": "table-cell"
						 }
					}]
				}]
			},{
				title : "Tool 교환시"
				,columns : [{
					title : "작업자"
					,columns : [{
						title : "시리얼 번호 <br> (Tool교환 전<br>/후 이력관리)"
						,columns : [{
							title : "교환전"
							,field : "before"
							,width : getElSize(240)
							,attributes: {
							      "class": "table-cell"
							 }
						},{
							title : "교환후"
							,field : "after"
							,width : getElSize(240)
							,attributes: {
							      "class": "table-cell"
							}
						}]
					}]
				}]
			},{
				title : "Tool 파손시 재검사 결과"
				,columns : [{
					title : "1차 검사<br>(작업자)"
					,columns : [{
						title : "검사결과"
						,columns : [{
							title : "불량"
							,field : "workFault"
							,width : getElSize(200)
							,attributes: {
							      "class": "table-cell"
							}
						},{
							title : "검사수"
							,field : "cnt1"
							,width : getElSize(200)
							,attributes: {
							      "class": "table-cell"
							}
						}]
					}]
				},{
					title : "2차 검사<br>(팀장)"
					,columns : [{
						title : "검사결과"
						,columns : [{
							title : "불량"
							,field : "leaderFault"
							,width : getElSize(200)
							,attributes: {
							      "class": "table-cell"
							}
						},{
							title : "검사수"
							,field : "cnt2"
							,width : getElSize(200)
							,attributes: {
							      "class": "table-cell"
							}
						}]
					}]
				}]
			},{
				title : "확인"
				,columns : [{
					title : "작업자<br>이름"
					,editor: workauto
					,template:"#=worKerName(worker)#"
					,field : "worker"
					,width : getElSize(300)
					,attributes: {
					      "class": "table-cell"
					}
				},{
					title : "팀장"
					,field : "leader"
					,editor: workauto
					,template:"#=worKerName(leader)#"
					,width : getElSize(300)
					,attributes: {
					      "class": "table-cell"
					}
				}]
			}]
		})
		
		kendotable = $("#grid").kendoGrid({
			// 테이블 높이 
			height : getElSize(1550)
			,dataBound : function(e){
				$("#grid thead tr th").css("font-size", getElSize(18 * 2))
				$("#grid tbody tr td").css("font-size", getElSize(18 * 2))

				$(".k-grid-header").css({
							 /* "background-color" : "black",
							"background" : "black" */
							"background-color" : "rgb(38,39,43)",
							"background" : "rgb(38,39,43)"
				})		
				//				$("#grid tbody tr td").css("padding",0)
			}
			,filterable: false
			,editable : true
			,columns:[/* {
				title : "dvcId"
				,field : "dvcId"
			}, */{
				title : "Name"
				,field : "name"
				,width : getElSize(500)
			},{
				title : "Tool<br>No"
				,field : "portNo"
				,width : getElSize(130)
				,filterable: {
                    cell: {
                        enabled: false
                    }
	             }
			},{
				title : "Spec"
				,field : "spec"
				,width : getElSize(470)
				,filterable: {
                    cell: {
                        enabled: false
                    }
	             }
				,attributes: {
				      "class": "table-cell"
				}
			},{
				title : "Port<br>No"
				,field : "portNo"
				,width : getElSize(130)
				,filterable: {
                    cell: {
                        enabled: false
                    }
	             }
			},{
				title : "Status"
				,field : "status"
				,width : getElSize(300)
				//,editor : readOnly
				,template : "#=statusView(dvcId,name,portNo,lmtC,cnt)#"
//				,field : "toolNo"
			},{
				title : "Tool Cycle Count"
				,width : getElSize(600)
				,columns:[{
					title : "Lmt"
					,field : "lmtC"
					,width : getElSize(170)
					,filterable: {
	                    cell: {
	                        enabled: false
	                    }
		             }
					,attributes: {
					      "class": "table-cell"
					}
				},{
					title : "Current"
					,field : "cnt"
					,width : getElSize(170)
					,filterable: {
	                    cell: {
	                        enabled: false
	                    }
		             }
				},{
					title : "Remain"
					,template : "#=lmtC-cnt#"
					,width : getElSize(180)
					,filterable: {
	                    cell: {
	                        enabled: false
	                    }
		             }
				},{
					title : "Used <br> rate(%)"
					,template:kendo.template("#if (lmtC == 0) {# #='0'# %#} else {# #=(cnt/lmtC*100).toFixed(0)# %#} #")
					,width : getElSize(180)
					,filterable: {
	                    cell: {
	                        enabled: false
	                    }
		             }
				}]
			},{
				title : "RunTime(h)"
				,width : getElSize(600)
				,columns:[{
					title : "Lmt"
					,field : "lmtT"
					,width : getElSize(170)
					,filterable: {
	                    cell: {
	                        enabled: false
	                    }
		             }
					,attributes: {
					      "class": "table-cell"
					}
				},{
					title : "Current"
					,field : "time"
					,width : getElSize(170)
					,filterable: {
	                    cell: {
	                        enabled: false
	                    }
		             }
				},{
					title : "Remain"
					,template : "#=(lmtT-time).toFixed(1)#"
					,width : getElSize(180)
					,filterable: {
	                    cell: {
	                        enabled: false
	                    }
		             }
				},{
					title : "Used <br> rate(%)"
					//,template : "#=(time/lmtT*100).toFixed(0)# %"
					,template:kendo.template("#if (lmtT == 0) {# #='0'# %#} else {# #=(time/lmtT*100).toFixed(0)# %#} #")
					,width : getElSize(180)
					,filterable: {
	                    cell: {
	                        enabled: false
	                    }
		             }
				}]
			},{
					title : "Reset Date"
					,field : "resetDt"
					,width : getElSize(450)
					,filterable: {
	                    cell: {
	                        enabled: false
	                    }
		             }
			}]
		}).data("kendoGrid")
		
		getDvcList()
		
		$("#dialog").kendoDialog({
			height: getElSize(2100)
			,width: getElSize(3800)
		 	/* ,content: "<table border=1 style='color:black;'> <tr> <th> Tool Cycle Count </th> <th> RunTime (h) </th> </tr>" +
		 			 "<tr> <td> <input id='preCnt' type='number' value=0> </td> <td>  <input id='preSec' type='number' value=0> </td> </tr> </table>"
			 */,actions: [{
				text: "OK"
				,action: function(e){
					// e.sender is a reference to the dialog widget object
					// OK action was clicked
					// Returning false will prevent the closing of the dialog
					console.log(reDvcId)
					console.log(rePortNo)
					console.log($("#asd").val())
					var toollist = $("#ToolGrid").data("kendoGrid").dataSource.data()
					console.log(toollist[0].worker)
					
					for(i=0,len=toollist.length; i<len; i++){
						var chk=0
						for(j=0,length=selectlist.length; j<length; j++){
							if(toollist[i].worker==selectlist[j].value){
								chk++
							}
						}
						if(toollist[i].toolName==undefined){
							alert("Tool명 을 입력해주세요")
							return false;
						}else if(toollist[i].ToolCnt==undefined){
							alert("Tool교체 시점을 입력해주세요")
							return false;
						}else if(toollist[i].chk1==false && toollist[i].chk2==false && toollist[i].chk3==false){
							alert("교환 사유를 선택해주세요")
							return false;
						}else if(toollist[i].spec==undefined){
							alert("spec 를 입력해주세요")
							return false;
						}else if(toollist[i].found1==undefined || toollist[i].found2==undefined){
							alert("실측치를 입력해주세요")
							return false;
						}else if(toollist[i].before==undefined || toollist[i].after==undefined){
							alert("교환 전/후를 입력해주세요")
							return false;
						}else if(chk==0){
							alert("작업자를 입력해주세요")
							return false;
						}
						if(toollist[i].chk1==true){
							toollist[i].cause="정기"
						}else if(toollist[i].chk2==true){
							toollist[i].cause="마모"
						}else if(toollist[i].chk3==true){
							toollist[i].cause="파손"
						}
					}
					var obj = new Object();
					obj.val = toollist;
					
					console.log(toollist)
					console.log(JSON.stringify(toollist));
					var url = "${ctxPath}/chart/toolResetSave.do";
					var param = "dvcId=" + reDvcId + 
								"&portNo=" + rePortNo +
								"&cnt1=" + $("#preCnt").val() +
								"&sec=" + $("#preSec").val() +
								"&val=" + JSON.stringify(obj);
			
					console.log(param)
					var json;
					$.showLoading();
					$.ajax({
						url : url,
						data : param,
						type : "post",
						success : function(data){
							console.log(data);
							$.hideLoading();
							
							grid = $("#grid").data("kendoGrid");
							//filterStatus = JSON.parse(JSON.stringify(grid.dataSource.filter()))
							
							getTable()
							//$("#dialog").data("kendoDialog").close();
							//getTable();

						}
					})
					
				},
				primary: true
			},{
				text: "Cancel"
			}]
		});

		$("#dialog").data("kendoDialog").close();

	})
	
	function saveRow(){
		console.log("save");
		savelist=[]
		var kendolist = $("#grid").data("kendoGrid").dataSource.data()
		for(i=0,len=beforeData.length; i<len; i++){
			for(j=0, length = kendolist.length; j<length; j++){
				if(beforeData[i].dvcId == kendolist[j].dvcId && beforeData[i].portNo == kendolist[j].portNo){
					if(beforeData[i].spec!=kendolist[j].spec){
						savelist.push(kendolist[j])	
					}else if(beforeData[i].lmtC!=kendolist[j].lmtC){
						savelist.push(kendolist[j])	
					}else if(beforeData[i].lmtT!=kendolist[j].lmtT){
						savelist.push(kendolist[j])	
					}
				}
			}
		}
		console.log("---savelist---")
		console.log(savelist)
		
		if(savelist.length==0){
			alert("변경된 사항이 없습니다.")
			return false;
		}
		
		var obj = new Object();
		obj.val = savelist;
		
		var param = JSON.stringify(obj);
		
		console.log(param)
		var url = "${ctxPath}/chart/toolLimitSave.do"
		$.showLoading()
		$.ajax({
			url : url,
			data :"val=" + param,
			type : "post",
			success : function(data){
				console.log(data)
				if(data=="success"){
					//filterStatus = JSON.parse(JSON.stringify(grid.dataSource.filter()))
					getTable();
				}else{
					alert("eror")
					$.hideLoading();
				}
			}
		})
		
	}
	
	function getTable(){
		var url = "${ctxPath}/chart/getToolLifeList.do";
		var param = "&dvcId=" + $("#dvcId").val();
		console.log($("#dvcId").val())
//		$("#checkall")[0].checked=false;
		var json;
		$.showLoading();
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				$(json).each(function(idx,data){
					var status;
					if(Number(data.cnt)/Number(data.lmtC)*100 < 90){
						status = "green"
					}else if(Number(data.cnt)/Number(data.lmtC)*100 >=90 && Number(data.cnt)/Number(data.lmtC)*100 <100){
						status = "yellow"
					}else{
						status = "red"
					}
					data.name=decode(data.name);
					data.spec=decode(data.spec);
					data.status=status;
				})
				
				beforeData=json
				kendodata = new kendo.data.DataSource({
					data: json,
					batch: true,
		/* 			group: { field: "prdNo" },
					sort: [{
		            	field: "prdNo" , dir:"asc" 
		            },{
		            	field: "item" , dir:"asc"
		            }], */
					schema: {
						model: {
							id: "id",
							fields: {
								name : {editable : false}
								,portNo : {editable : false}
								,cnt : {editable : false}
								,remain : {editable : false}
								,status : {editable : false}
								,lmtT : {editable : false}
								,time : {editable : false}
								,resetDt : {editable : false}
								/* prdNo: { editable: false },
								proj: { editable: false },
								vndNo: {editable: true,
									field : 'vndNo',
									defaultValue: function(e){
										return "0001"
									}
								},
								item: { editable: true,
									field : "item",
									defaultValue: "FS_RR_LH"
								},
								cnt: { editable: false },
								sendCnt: { editable: true ,type: 'number'},
								checker : { editable : false},
								action : { editable : false} */
							}
						}
					}
				}); 
				kendotable.setDataSource(kendodata);
				grid = $("#grid").data("kendoGrid");
				grid.hideColumn("dvcId");
				
				if($("#dvcId").val()=="ALL"){
					grid.dataSource.filter({ 
						logic: "or"
						,filters: [{
								field: "status", operator: "eq", value: "red" 
							},{
								field: "status", operator: "eq", value: "yellow"
						}] 
					});
				}else{
					grid.dataSource.filter({ 
					});

				}
				
				var options = grid.options;
				
				/* if(filterStatus!=undefined){
					grid.options.dataSource.filter(filterStatus)
				} */
				
				
				$.hideLoading()
			}
		})
	}
	
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td rowspan="10" id="svg_td" style="vertical-align: top; position:relative; background:#26282c;">
					<label class='text1'><spring:message code="device"></spring:message></label>
<!-- 					<spring:message code="device"></spring:message> :  -->
					<select id="dvcId" ></select> 
					<button id="search" onclick="getTable()"> Search </button> 
					<button id="saverow" onclick="saveRow()" style="float: right;">Save</button>
					<div id="wrapper">
						<table style="width: 100%">
							<Tr>
								<Td style="text-align: center; vertical-align: middle;">
									<div id="grid">
									</div>
								</Td>
							</Tr>
						</table>
					</div>
				</td>
			</Tr>
		</table>
		
	 </div>
	<div id="dialog">
<!-- 		<button onclick="insertRow()"> 추가 </button> -->
		<table id='resetT' border=1 style='color:black; text-align: center'>
			<tr>
				<th colspan="2" class='Tcss'> Tool 교환 및 검사 일지</th>
			</tr>
			<tr> 
				<th class='Hcss'> Tool Cycle Count </th>
				<th class='Hcss'> RunTime (h) </th> 
			</tr>
			<tr> 
				<td class='Bcss'> <input id='preCnt' type='number' value=0> </td> 
				<td class='Bcss'>  <input id='preSec' type='number' value=0> </td> 
			</tr>
			
			<!-- <tr>
				<td rowspan="4"> 일자 </td>
				<td rowspan="4"> 시간 </td>
				<td rowspan="4"> 차종 </td>
				<td rowspan="4"> 공정명 </td>
				<td rowspan="4"> Tool명 </td>
				<td rowspan="4"> Tool 수명 </td>
				<td rowspan="4"> Tool <br> 교체시점 </td>
				<td rowspan="2" colspan="3"> Tool교환사유 </td>
				<td rowspan="2" colspan="3"> 측정 결과 </td>
				<td colspan="2"> Tool 교환시 </td>
				<td colspan="4"> Tool 파손시 재검사 결과 </td>
				<td rowspan="2"  colspan="2"> 확인 </td>
			</tr>
			
			<tr>
				<td colspan="2"> 작업자 </td>
				<td colspan="2"> 1차 검사 (작업자) </td>
				<td colspan="2"> 2차 검사 (팀장) </td>
			</tr>
			
			<tr>
				<td rowspan="2"> 정기 </td>
				<td rowspan="2"> 마모 </td>
				<td rowspan="2"> 파손 </td>
				<td rowspan="2"> Spec </td>
				<td colspan="2"> 실측치 </td>
				<td colspan="2"> 시리얼 번호 </td>
				<td colspan="2"> 검사 결과 </td>
				<td colspan="2"> 검사 결과 </td>
				<td rowspan="2"> 작업자 이름 </td>
				<td rowspan="2"> 팀장 </td>
			</tr>
			
			<tr>
				<td> #1 </td>
				<td> #2 </td>
				<td> 교환전 </td>
				<td> 교환후 </td>
				<td> 불량 </td>
				<td> 검사수량 </td>
				<td> 불량 </td>
				<td> 검사수량 </td>
			</tr> -->
		</table>
		<div id="ToolGrid"></div>
	</div>
	
</body>
</html>	