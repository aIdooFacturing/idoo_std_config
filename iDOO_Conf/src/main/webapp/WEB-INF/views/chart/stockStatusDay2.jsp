<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>

<script src="http://kendo.cdn.telerik.com/2018.2.516/js/jszip.min.js"></script>

<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden; 
	background-color: black;
}
#wrapper thead {
	font-size: 12;
}

#wrapper tbody {
	font-size: 12;
	font-weight : bolder;
	background-color: white;
}

.k-grid-header th.k-header {
	background: linear-gradient( to top, black, gray);
	color: white;
}

.k-grouping-row td {
	background: linear-gradient( to top, #5D5D5D, #8C8C8C);
	color: white;
}

#wrapper tbody tr td.k-group-cell {
	background: #8C8C8C;
}

.k-header.k-grid-toolbar {
	background: linear-gradient( to top, black, gray);
	color: white;
}

a.k-button.k-button-icontext.k-grid-cancel-changes {
	background: linear-gradient( to top, red, #ff6363, #ff6300);
	transition-duration: 0.5s;
}

a.k-button.k-button-icontext.k-grid-cancel-changes:hover {
	background: red;
	color: white;
	transition-duration: 0.5s;
}

a.k-button.k-button-icontext.k-grid-finish {
	float: right;
	background: greenyellow;
	transition-duration: 0.5s;
}

.k-button.k-button-icontext.k-grid-print {
	float: right;
	background: greenyellow;
	transition-duration: 0.5s;
}

a.k-button.k-button-icontext.k-grid-finish:hover {
	background: #486b13;
	transition-duration: 0.5s;
	color: white; 
}

.k-button.k-button-icontext.k-grid-print:hover {
	background: #486b13;
	transition-duration: 0.5s;
	color: white; 
}

.k-icon.k-i-collapse{
	display:none;
} 
#wrapper tbody tr.k-grouping-row td{
	font-size:0px;
	padding :5px;
}

html{
	overflow : hidden;
}

.k-grid-footer td{
	color : black;
}
.k-grid-footer td{
	background : #E4E4E4;
}

#grid{
	background: black;
	border-color : #222327;
}

#wrapper{
	border-color : #222327;
}

#wrapperTable{
	border-color : #222327;
}

.k-grid-header-locked{
	border-color : #222327;
}

.k-grid-content-locked{
	border-color : #222327;
}
.k-grid-content-locked td{
	border-color : #222327;
}

.k-grid-content-locked tbody{
	border-color : #222327;
}

.k-grid-footer{
	border-color : #222327;
}

.k-header {
	border-color : #222327;
}

.k-grid-header-wrap {
	border-color : #222327;
}

</style> 
<script type="text/javascript">
	
	const loadPage = () =>{
		createMenuTree("tm", "toolLifeManager")	
	}
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};
	
	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

//	var handle = 0;
	
	$(function(){
//		createNav("inven_nav", 9);		
//		setEl();
//		time();
		
		setEl2();
		
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
/* 	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	 */
/* 	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : contentWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#banner").css({
			"font-size" : getElSize(80),
			"width" : getElSize(2000)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		$("#wrapper").css({
			"width": getElSize(3350)
		})
		
		
	}; */
	
	
	function setEl2(){
		var width = window.innerWidth;
		var height = window.innerHeight;
	/* 	
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		}) */
		
		$("#container").css({
			"width" : contentWidth - getElSize(30)
		})
			
		$("#content_table").css({
			"margin-top" : getElSize(300)
		})
			
			
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight,
			"margin-top" : getElSize(250)
		});
		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#banner").css({
			"font-size" : getElSize(80),
			"width" : getElSize(2000)
		});
	
		$("#wrapper").css({
			"width": getElSize(3800)
		})
		
		
	};
	
	
	function readOnly(container, options){
		container.removeClass("k-edit-cell");
		container.text(options.model.get(options.field));
	}
	function date() {
		return '날짜  :<input type="text" id="sDate" class="date" readonly="readonly"> ~ <input type="text" id="eDate" class="date" readonly="readonly">'
	}
	function sumCnt(sum){
		if(sum!=undefined){
			return sum.sum
		}else {
			return 0
		}
		
	}
	var BrcvCntR;
	var BrcvCntM;
	var colName="";
	var kendotable;
	
	//총 합계 변수선언
	var sumAcRcv=0;	var sumAcNoti=0;	var sumAcIn=0;	var sumAcIss=0;	// 소재
	var sumAcRcvL=0	//라인
	var sumAcRcvR=0;	var sumAcInR=0;	var sumAcIssR=0;	var sumAcNotiR=0;	//R삭
	var sumAcRcvM=0;	var sumAcInM=0;	var sumAcIssM=0;	var sumAcNotiM=0;	//M삭
	var sumAcRcvC=0;	var sumAcInC=0;	var sumAcIssC=0;	var sumAcNotiC=0;	//C삭
	var sumAcRcvF=0;	var sumAcInF=0;	var sumAcIssF=0;	var sumAcNotiF=0;	//도금
	var sumAcRcvP=0;	var sumAcInP=0;	var sumAcIssP=0;	var sumAcOutP=0;	var sumAcNotiP=0;	//완제품
	
	$(document).ready(function(){
		
		kendotable = $("#wrapper").kendoGrid({
			height: getElSize(1785),
			editable: true,	
			groupable: false,
			navigatable: true,
			dataBound: onDataBound,
			columns: [{
				field: "prdNo",
				title: "차종",
				encoded: true,
				locked: true,
				lockable: false,
				groupHeaderTemplate: ' ',
				width: getElSize(410)
			}, {
				field: "item",
				title: "부품명",
				locked: true,
				lockable: false,
				width: getElSize(390)
				,footerTemplate: "총합계 ==>"
			}, {
				title: "소재",
				columns: [{
					field: "iniohdCnt"
					, title: "전일 재고"
					, width: getElSize(170)
					, attributes: {
						class: "cellMerge" ,
//						style: "text-align: center; background-color:yellow; color:black !important; font-size:" + getElSize(38)
					}

				}, {
					field: "rcvCnt"
					, title: "입고"
					, width: getElSize(170)
					,footerTemplate: "#=sumAcRcv#"
					, attributes: {
						class: "cellMerge" ,
					}
//					, template : '<input type="text" class="k-input k-textbox" name="rcvCnt" value="#=rcvCnt#" data-bind="value:rcvCnt">'
				}, {
					field: "notiCnt"
					, title: "불량"
					, width: getElSize(170)
					, attributes: {
						class: "cellMerge" ,
					}
					,footerTemplate: "#=sumAcNoti#"
				}, {
					field:"sumIn"
					,title: "누적입고",
					width: getElSize(170)
					, attributes: {
						class: "cellMerge" ,
					}
					,footerTemplate: "#=sumAcIn#"
				}, {
					field: "issCnt"
					, title: "출고"
					,footerTemplate: "#=sumAcIss#"
						, attributes: {
							class: "cellMerge" ,
						}
					, width: getElSize(170)
				}, {
					field: "ohdCnt"
					, template: "<div class='#=ohdCnt#'>#=iniohdCnt+rcvCnt-notiCnt-issCnt#"
					, title: "창고 재고"
					, width: getElSize(170)
					, attributes: {
						class: "cellMerge" ,
					}
					, editor: readOnly
				}]
			}, {
				width: getElSize(137)
				, attributes: {
					style: "text-align: center; background-color:white; color:black; font-size:" + getElSize(38)
				}, headerAttributes: {
					style: "text-align: center; background-color:white; color:black; font-size:" + getElSize(38)
				}
			}, {
				title: "라인소재재고",
				columns: [{
					field: "iniohdCntL"
					, title: "전일재고"
					, width: getElSize(170)
					, editor: readOnly
					, attributes: {
						class: "cellMerge" ,
					}

				}, {
					field: "rcvCntL"
					, title: "입고"
					, width: getElSize(170)
					, editor: readOnly
					,footerTemplate: "#=sumAcRcvL#"
					, attributes: {
						class: "cellMerge" ,
					}

				}, {
					field: "ohdCntL"
//					,template:  "#=Number(iniohdCntL)+Number(rcvCntL)-Number(rcvCntM)#"
					, title: '현재고'
					, width: getElSize(170)
					, editor: readOnly
					, attributes: {
						class: "cellMerge" ,
					}

				}]
			}, {
				width: getElSize(137)
				, attributes: {
					style: "text-align: center; background-color:white; color:black; font-size:" + getElSize(38)
				}, headerAttributes: {
					style: "text-align: center; background-color:white; color:black; font-size:" + getElSize(38)
				}
			},{
				title: "R삭",
				columns: [{
					field: "iniohdCntR"
					, title: "전일재고"
					, width: getElSize(170)
					, attributes: {
						class: "cellMerge" ,
					}
				}, {
					field: "rcvCntR"
					, title: "입고"
					, width: getElSize(170)
					,footerTemplate: "#=sumCnt(data.rcvCntR)#"
//					,footerTemplate: "#=sumAcRcvR#"
					, attributes: {
						class: "editable-cell" ,
						style: "text-align: center; background-color:yellow; color:black !important; font-size:" + getElSize(38)
					}
				}, {
					field:"sumInR"
					,title: "누적<br>생산수량"
					, attributes: {
						class: "cellMerge" ,
					}
					,footerTemplate: "#=sumAcInR#"
					, width: getElSize(170)
				}, {
					field: "issCntR"
					, title: "출고"
					, width: getElSize(170)
					,footerTemplate: "#=sumCnt(data.issCntR)#"
//					,footerTemplate: "#=sumAcIssR#"
					, attributes: {
						class: "editable-cell" ,
						style: "text-align: center; background-color:yellow; color:black !important; font-size:" + getElSize(38)
					}
				}, {
					field: "notiCntR"
					, title: "불량"
					, width: getElSize(170)
					,footerTemplate: "#=sumAcNotiR#"
					, attributes: {
						class: "editable-cell" ,
						style: "text-align: center; background-color:yellow; color:black !important; font-size:" + getElSize(38)
					}
				}, {
					title: "불량 누계"
					, width: getElSize(170)
				}, {
					field: "ohdCntR"
//					template: "#=Number(iniohdCntR)+Number(rcvCntR)-Number(issCntR)-Number(notiCntR)#"
					, title: "재고"
					, width: getElSize(170)
					, editor: readOnly
					, attributes: {
						class: "cellMerge" ,
					}

				}]
			}, {
				width: getElSize(137)
				, attributes: {
					style: "text-align: center; background-color:white; color:black; font-size:" + getElSize(38)
				}, headerAttributes: {
					style: "text-align: center; background-color:white; color:black; font-size:" + getElSize(38)
				}
			},{
				title: "1차 가공(MCT)",
				columns: [{
					field: "iniohdCntM"
					, title: "전일재고"
					, width: getElSize(170)
				}, {
					field: "rcvCntM"
					, title: "입고"
					, width: getElSize(170)
//					,footerTemplate: "#=sumAcRcvM#"
					,footerTemplate: "#=sumCnt(data.rcvCntM)#"
					, attributes: {
						class: "editable-cell" ,
						style: "text-align: center; background-color:yellow; color:black !important; font-size:" + getElSize(38)
					}
				}, {
					field:"sumInM"
					,title: "누적<br>생산수량"
//					,footerTemplate: "#=sumAcInM#"
					,footerTemplate: "#=sumCnt(data.sumInM)#"
					, width: getElSize(170)
				}, {
					field: "issCntM"
					, title: "출고"
					, width: getElSize(170)
//					,footerTemplate: "#=sumAcIssM#"
					,footerTemplate: "#=sumCnt(data.issCntM)#"
					, attributes: {
						class: "editable-cell" ,
						style: "text-align: center; background-color:yellow; color:black !important; font-size:" + getElSize(38)
					}
				}, {
					field: "notiCntM"
					, title: "불량"
					, width: getElSize(170)
//					,footerTemplate: "#=sumAcNotiM#"
					,footerTemplate: "#=sumCnt(data.notiCntM)#"
					, attributes: {
						class: "editable-cell" ,
						style: "text-align: center; background-color:yellow; color:black !important; font-size:" + getElSize(38)
					}
				}, {
					title: "불량 누계"
					, width: getElSize(170)
				}, {
					field: "ohdCntM"
//					template: "#=Number(iniohdCntM)+Number(rcvCntM)-Number(issCntM)-Number(notiCntM)#"
					, title: "재고"
					, width: getElSize(170)
					, editor: readOnly
				}]
			}, {
				width: getElSize(137)
				, attributes: {
					style: "text-align: center; background-color:white; color:black; font-size:" + getElSize(38)
				}, headerAttributes: {
					style: "text-align: center; background-color:white; color:black; font-size:" + getElSize(38)
				}
			}, {
				title: "2차 가공(CNC)",
				columns: [{
					field: "iniohdCntC"
					, title: "전일재고"
					, width: getElSize(170)
				}, {
					field: "rcvCntC"
					, title: "입고"
					, width: getElSize(170)
//					,footerTemplate: "#=sumAcRcvC#"
					,footerTemplate: "#=sumCnt(data.rcvCntC)#"
					, attributes: {
						class: "editable-cell" ,
						style: "text-align: center; background-color:yellow; color:black !important; font-size:" + getElSize(38)
					}
				}, {
					field:"sumInC"
					,title: "누적입고"
//					,footerTemplate: "#=sumAcInC#"
					,footerTemplate: "#=sumCnt(data.sumInC)#"
					, width: getElSize(170)
				}, {
					field: "issCntC"
					, title: "출고"
					, width: getElSize(170)
//					,footerTemplate: "#=sumAcIssC#"
					,footerTemplate: "#=sumCnt(data.issCntC)#"
				}, {
					title: "누적출고"
					, width: getElSize(170)
				}, {
					field: "notiCntC"
					, title: "불량"
					, width: getElSize(170)
//					,footerTemplate: "#=sumAcNotiC#"
					,footerTemplate: "#=sumCnt(data.notiCntC)#"
					, attributes: {
						class: "editable-cell" ,
						style: "text-align: center; background-color:yellow; color:black !important; font-size:" + getElSize(38)
					}
				}, {
					title: "불량 누계"
					, width: getElSize(170)
				}, {
					field: "ohdCntC"
//					template: "#=Number(iniohdCntC)+Number(rcvCntC)-Number(issCntC)-Number(notiCntC)#"
					, title: "재고"
					, width: getElSize(170)
					, editor: readOnly
				}]
			}, {
				width: getElSize(137)
				, attributes: {
					style: "text-align: center; background-color:white; color:black; font-size:" + getElSize(38)
				}, headerAttributes: {
					style: "text-align: center; background-color:white; color:black; font-size:" + getElSize(38)
				}
			}, {
				title: "도금",
				columns: [{
					field: "iniohdCntF"
					, title: "전일재고"
					, width: getElSize(170)
				}, {
					field: "rcvCntF"
					, title: "입고"
					, width: getElSize(170)
//					,footerTemplate: "#=sumAcRcvF#"
					,footerTemplate: "#=sumCnt(data.rcvCntF)#"
/* 					, attributes: {
						class: "editable-cell" ,
						style: "text-align: center; background-color:yellow; color:black !important; font-size:" + getElSize(38)
					}
 */				}, {
					field:"sumInF"
					,title: "누적입고"
//					,footerTemplate: "#=sumAcInF#"
					,footerTemplate: "#=sumCnt(data.sumInF)#"
					, width: getElSize(170)
				}, {
					field: "issCntF"
					, title: "출고"
					, width: getElSize(170)
//					,footerTemplate: "#=sumAcIssF#"
					,footerTemplate: "#=sumCnt(data.issCntF)#"
				}, {
					title: "누적출고"
					, width: getElSize(170)
				}, {
					field: "notiCntF"
					, title: "불량"
					, width: getElSize(170)
//					,footerTemplate: "#=sumAcNotiF#"
					,footerTemplate: "#=sumCnt(data.notiCntF)#"
/* 					, attributes: {
						class: "editable-cell" ,
						style: "text-align: center; background-color:yellow; color:black !important; font-size:" + getElSize(38)
					}
 */				}, {
					title: "불량 누계"
					, width: getElSize(170)
				}, {
					field: "ohdCntF"
//					template: "#=Number(iniohdCntC)+Number(rcvCntC)-Number(issCntC)-Number(notiCntC)#"
					, title: "재고"
					, width: getElSize(170)
					, editor: readOnly
				}]
			}, {
				width: getElSize(137)
				, attributes: {
					style: "text-align: center; background-color:white; color:black; font-size:" + getElSize(38)
				}, headerAttributes: {
					style: "text-align: center; background-color:white; color:black; font-size:" + getElSize(38)
				}
			}, {
				title: "완제품",
				columns: [{
					field: "iniohdCntP"
					, title: "전일재고"
					, width: getElSize(170)
				}, {
					field: "rcvCntP"
					, title: "입고"
					, width: getElSize(170)
//					,footerTemplate: "#=sumAcRcvP#"
					,footerTemplate: "#=sumCnt(data.rcvCntP)#"
				}, {
					field:"sumInP"
					,title: "누적입고"
//					,footerTemplate: "#=sumAcInP#"
					,footerTemplate: "#=sumCnt(data.sumInP)#"
					, width: getElSize(170)
				}, {
					field: "issCntP"
					, title: "출고"
					, width: getElSize(170)
//					,footerTemplate: "#=sumAcIssP#"
					,footerTemplate: "#=sumCnt(data.issCntP)#"
				}, {
					field: "sumOutP"
					, title: "누적출고"
					, width: getElSize(170)
//					,footerTemplate: "#=sumAcOutP#"
					,footerTemplate: "#=sumCnt(data.sumOutP)#"
				}, {
					field: "notiCntP"
					, title: "불량"
					, width: getElSize(170)
//					,footerTemplate: "#=sumAcNotiP#"
					,footerTemplate: "#=sumCnt(data.notiCntP)#"
				}, {
					title: "불량 누계"
					, width: getElSize(170)
				}, {
					field: "ohdCntP",
					template: "#=Number(iniohdCntP)+Number(rcvCntP)-Number(issCntP)-Number(notiCntP)#"
					, title: "재고"
					, width: getElSize(170)
					, editor: readOnly
				}]
			}, {
				width: getElSize(137)
				, attributes: {
					style: "text-align: center; background-color:white; color:black; font-size:" + getElSize(38)
				}, headerAttributes: {
					style: "text-align: center; background-color:white; color:black; font-size:" + getElSize(38)
				}
			}, {
				title: "통합 재고",width:getElSize(500)
				, columns: [{
					title: "총 재고"
					, template: "#=Number(ohdCnt)+Number(ohdCntL)+Number(ohdCntR)+Number(ohdCntM)+Number(ohdCntC)+Number(ohdCntF)+Number(ohdCntP)#"
					, width: getElSize(170)
					, editor: readOnly
				}, {
					field: "totalday"
					, title: "재고 일수"
					, template: "-"
					, width: getElSize(170)
					, editor: readOnly
				}]
			}],
		//	navigatable: true,
    							
			toolbar: [{name: "선택", template: "<label class='chkRadio'><input type='radio' name='rdate' value='term'>기간별 조회</label> <label class='chkRadio'><input type='radio' name='rdate' value='day' checked>일자별 조회 </label>"}
					,{ name: "날짜", template: '#=date()#' }
					,{ name: "custom", text: "저장", iconClass: "k-icon k-i-save" }
					,{ name: "cancel", text: "취소" } /* { name: "finish", text: "마감 처리", iconClass: "k-icon k-i-check" }, *//*  {name:"pdf"} , */ 
					,{name:"print" , text: "인쇄" ,iconClass: "k-icon k-i-pinterest"}
					,{name:"excel"}],
			excel : {
				fileName : $(".date ").val()+"_재고.xlsx"
			},
			pdf :{
				margin : '0cm'
			},
 			cellClose:  function(e) {
//				console.log(e.model)
//				e.model.set("issCntR",55)
				if(colName==""){
					return false;
				}

				var gridlist = $("#wrapper").data("kendoGrid").dataSource.data();
//				console.log(gridlist[2])
				for(i=0,len=gridlist.length;i<len;i++){
					if(e.model.prdNo==gridlist[i].prdNo){
//						gridlist[i].set("issCntR",13);
					}
				}
//				$("#wrapper").data("kendoGrid").dataSource.fetch()
				
//1				console.log("이전 R입고 값:"+BrcvCntR)
//1				console.log("이후 :"+e.model.rcvCntR)
//1				console.log("이전 M입고 값:"+BrcvCntM)
//1				console.log("이후 :"+e.model.rcvCntM)
//1				console.log("열 이름 :"+colName)
//1				console.log(colName=="")
//1				console.log(minOpr)

				var Lcnt=0;
				var Rcnt=0;
				
				var minusR=0;	// R삭 출고 수정시 L,R 합쳐서 재고 계산하기 위해서
				var plusR=0;	// R삭 입고 수정시 L,R 합쳐서 재고 계산하기 위해서
				var MnotiR=0;	// R삭 불량 수정시 L,R 합쳐서 재고 계산하기 위해서
				
				for(i=0,len=gridlist.length;i<len;i++){
					if(e.model.prdNo==gridlist[i].prdNo){
						Lcnt=Lcnt+Number(gridlist[i].rcvCntM)
					}
					if(e.model.prdNo==gridlist[i].prdNo){
						Rcnt=Rcnt+Number(gridlist[i].rcvCntR)
					}
					if(e.model.prdNo==gridlist[i].prdNo){
						minusR += gridlist[i].issCntR;
						plusR += gridlist[i].rcvCntR;
						MnotiR += gridlist[i].notiCntR;
					}
/* 					if(e.model.item==gridlist[i].item){
						Rcnt=Rcnt+Number(gridlist[i].rcvCntR)
					}
 */				}
				
				if(minOpr=="0020"){
					e.model.set("ohdCntL",e.model.iniohdCntL+e.model.rcvCntL-e.model.notiCntL-Lcnt)
					if(colName=="rcvCntM"){	// M 입고시 L출하
						e.model.set("issCntL",e.model.rcvCntM)
					}else if(colName=="issCntM"){	// M 출하시 C입고
						e.model.set("rcvCntC",e.model.issCntM)
					}
				}else{
//1					console.log("오나?")
//1					console.log(e.model.iniohdCntL)
//1					console.log(e.model.rcvCntR)
//1					console.log(e.model.notiCntL)
//1					console.log(Rcnt)
					e.model.set("ohdCntL",e.model.iniohdCntL+e.model.rcvCntL-e.model.notiCntL-Rcnt)
					if(colName=="rcvCntR"){	//R 입고 수정시 L출하
						e.model.set("issCntL",e.model.rcvCntR);
					/* 
						for(i=0,len=gridlist.length;i<len;i++){
							if(e.model.prdNo==gridlist[i].prdNo){
								console.log(gridlist[i].item + " :: " + gridlist[i].rcvCntR);
								
								gridlist[i].rcvCntR = e.model.rcvCntR
							}
						} */
						console.log("--R입고--")
					}else if(colName=="notiCntR"){
						e.model.set("ohdCntR",e.model.iniohdCntR + plusR - minusR - MnotiR)
						console.log("--R불량--")
					}else if(colName=="issCntR"){ // R 출고시 M입고
						
						e.model.set("ohdCntR",e.model.iniohdCntR + plusR - minusR - MnotiR)

						for(i=0,len=gridlist.length;i<len;i++){
							if(e.model.prdNo==gridlist[i].prdNo){
								
								gridlist[i].ohdCntR = e.model.ohdCntR
							}
						}
						console.log("--R출고--")
						console.log(minusR);
						e.model.set("rcvCntM",e.model.issCntR)
						
					}else if(colName=="rcvCntM"){	// M 입고시 R출하
						
						e.model.set("ohdCntR",e.model.iniohdCntR + plusR - minusR - MnotiR)

						for(i=0,len=gridlist.length;i<len;i++){
							if(e.model.prdNo==gridlist[i].prdNo){
								
								gridlist[i].ohdCntR = e.model.ohdCntR
							}
						}
						
						e.model.set("issCntR",e.model.rcvCntM)
					}else if(colName=="issCntM"){	// M 출하시 C입고
						e.model.set("rcvCntC",e.model.issCntM)
					}else if(colName=="rcvCntC"){	// C 입고시 M출하
						e.model.set("issCntM",e.model.rcvCntC)
					}
				}
				
//1				e.model.set("ohdCntR",e.model.iniohdCntR + e.model.rcvCntR - e.model.issCntR - e.model.notiCntR)
				e.model.set("ohdCntR",e.model.iniohdCntR + plusR - minusR - MnotiR)
				e.model.set("ohdCntM",e.model.iniohdCntM + e.model.rcvCntM - e.model.issCntM - e.model.notiCntM)
				e.model.set("ohdCntC",e.model.iniohdCntC + e.model.rcvCntC - e.model.issCntC - e.model.notiCntC)
				e.model.set("ohdCntP",e.model.iniohdCntP + e.model.rcvCntP - e.model.issCntP - e.model.notiCntP)

			},
			edit: function (e) {
//				console.log(e.container)
//				console.log(e.model.minOpr)
				$(".k-select").css({
					"display":"none",
					"width" : getElSize(1)
				});
				
				minOpr=e.model.minOpr
				maxOpr=e.model.maxOpr
				editE=e
				var input = e.container.find(".k-input");
				var value = input.val();
				//기존 저장된 값 들고오기
				currentValue = Number(value);

				BrcvCntR=e.model.rcvCntR
				BrcvCntM=e.model.rcvCntM
//				currentValue = Number(e.model.rcvCnt); 
				//edit 들어가서 키입력시
				//같은 아이디값 복사하기
				var col = $(this).closest("td");
				
				input.keyup(function (e) {
					console.log(e.keyCode);
					
					if(e.keyCode==9){
						return false;
					}
					value = input.val();

					var kendolist = kendotable.dataSource.data();
					var grid = $("#wrapper").data("kendoGrid");
					var col = $(this).closest("td");
					colName=col.context.name

					if(minOpr=="0"){
						editE.container.removeClass("k-edit-cell");
						editE.container.text(editE.model.get(editE.field));
						editE.model.set(col.context.name,0);
						colName=""
						kendo.alert("라우팅이 등록되지 않았습니다.")
						return false;
					}else if(minOpr=="0020" && (col.context.name=="rcvCntR" || col.context.name=="issCntR" || col.context.name=="notiCntR")){
						editE.container.removeClass("k-edit-cell");
						editE.container.text(editE.model.get(editE.field));
						editE.model.set(col.context.name,0);
						colName=""
						kendo.alert("R삭이 등록되지 않았습니다.")
						return false;
					}
					if(maxOpr!="0040" && (col.context.name=="rcvCntF" || col.context.name=="issCntF" || col.context.name=="notiCntF")){
						editE.container.removeClass("k-edit-cell");
						editE.container.text(editE.model.get(editE.field));
						editE.model.set(col.context.name,0);
						colName=""
						kendo.alert("도금 작업이 등록되지 않았습니다.")		
						return false;
					}
					
					$(grid.tbody).off("change").on("change", "td", function (e) {
						/* var row = $(this).closest("tr");
						dataItem = kendotable.dataItem(row);
						var clone = $.extend({}, dataItem);
						var rowIdx = $("tr", grid.tbody).index(row);
						var colIdx = $("td", row).index(this);
						var colName = $('#wrapper').find('th').eq(colIdx).text();
						 */
						
						var returnChk = 0;	// returnChk == 0일때만 경고창발생 (한번하기위함)
						
					})
					/* console.log("-----key update-----")
					console.log(e.keyCode)
					if(e.keyCode ==52){
						$(".editable-cell")[1].click()
					}else if(e.keyCode == 13){
						alert("enter")
					}else if(e.keyCode == 50){
					alert("2222")
					} */
					
					value = input.val();
					
					/* $(grid.tbody).off("change").on("change", "td", function (e) {
						var row = $(this).closest("tr");
						dataItem = kendotable.dataItem(row);
						var clone = $.extend({}, dataItem);
						var rowIdx = $("tr", grid.tbody).index(row);
						var colIdx = $("td", row).index(this);
						var colName = $('#wrapper').find('th').eq(colIdx).text();
						var returnChk = 0;	// returnChk == 0일때만 경고창발생 (한번하기위함)
						//창고
						
						$(kendolist).each(function (idx, data) {
							if (clone.prdNo == kendolist[idx].prdNo && (col.context.name == "rcvCnt" || col.context.name == "issCnt" || col.context.name == "notiCnt")) {
								kendolist[idx].set(col.context.name, Number(clone.get(col.context.name)));
								console.log("입력한 값 : " + clone.get(col.context.name))
								console.log("비교 값 : " + (clone.get(col.context.name) - currentValue));
								if (col.context.name == "rcvCnt") {
									data.set("ohdCnt", Number(clone.ohdCnt) + Number(clone.get(col.context.name) - currentValue))
								} else if (col.context.name == "issCnt") {
									//자재 출고 수정시 라인소재 입고 수정
									data.set("rcvCntL",Number(clone.get(col.context.name)))
									data.set("ohdCntL", Number(clone.ohdCntL) + Number(clone.get(col.context.name) - currentValue))

									data.set("ohdCnt", Number(clone.ohdCnt) - Number(clone.get(col.context.name) - currentValue))
								} else if (col.context.name == "notiCnt") {
									
									data.set("ohdCnt", Number(clone.ohdCnt) - Number(clone.get(col.context.name) - currentValue))
								}
							}else if (clone.prdNo == kendolist[idx].prdNo && clone.idM == kendolist[idx].idM && (col.context.name == "rcvCntR" || col.context.name == "issCntR" || col.context.name == "notiCntR")) {
								console.log("R Modify");
								
								if (col.context.name == "rcvCntR") {
									if(clone.minOpr=="0010"){
										//R입고 수정시 L출고
										data.set("ohdCntL", Number(clone.ohdCntL) - Number(clone.get(col.context.name) - currentValue))
										data.set("issCntL", Number(clone.get(col.context.name)));
									}else{
										if(returnChk==0){
											console.log("----------------")
											console.log(data)
											console.log(data.rcvCntR)
											data.set("rcvCntR",0);
											console.log(data)
											console.log(data.rcvCntR)
											kendo.alert("R작업이 등록되어있지않습니다.")
											returnChk=1;
										}
										data.set("rcvCntR",0);
									}
									
									if(clone.item == kendolist[idx].item && returnChk!=1){
										data.set("ohdCntR", Number(clone.ohdCntR) + Number(clone.get(col.context.name) - currentValue))
									}
								} else if (col.context.name == "issCntR") {
									
									if(clone.item == kendolist[idx].item){
										console.log(clone.minOpr);
										
										if(clone.minOpr=="0010"){
											//R 출고 수정시 M입고 수정
											data.set("rcvCntM", Number(clone.get(col.context.name)))
											data.set("ohdCntM", Number(clone.ohdCntM) + Number(clone.get(col.context.name) - currentValue))

											data.set("ohdCntR", Number(clone.ohdCntR) - Number(clone.get(col.context.name) - currentValue))
										}else{
											if(returnChk==0){
												kendo.alert("R작업이 등록되어있지않습니다.")
												returnChk=1;
											}
											data.set("issCntR",0);
										}
									}
								} else if (col.context.name == "notiCntR") {
									if(clone.item == kendolist[idx].item){
										console.log("---자재출고시 라인소재 재고구하기")
										console.log(clone.ohdCntM)
										console.log( Number(clone.get(col.context.name)))
										console.log(currentValue)
										if(clone.minOpr=="0010"){
											data.set("ohdCntR", Number(clone.ohdCntR) - Number(clone.get(col.context.name) - currentValue))
										}else{
											if(returnChk==0){
												kendo.alert("R작업이 등록되어있지않습니다.")
												returnChk=1;
											}
											data.set("notiCntR",0);
										}
									}
								}
							}
							else if (clone.prdNo == kendolist[idx].prdNo && clone.idM == kendolist[idx].idM && (col.context.name == "rcvCntM" || col.context.name == "issCntM" || col.context.name == "notiCntM")) {
								console.log("MCT Modify")
								if (col.context.name == "rcvCntM") {
									console.log(clone.minOpr)
									if(clone.minOpr=="0020"){
										//M입고 수정시 L출고
										data.set("ohdCntL", Number(clone.ohdCntL) - Number(clone.get(col.context.name) - currentValue))
									}else if(clone.minOpr=="0"){
										if(returnChk==0){
											kendo.alert("라우팅이 등록되어있지않습니다.")
											returnChk=1;
										}
		
										if(clone.item == kendolist[idx].item){
											data.set("rcvCntM",0);
										}
									}else{
										if(clone.item == kendolist[idx].item && returnChk!=1){
											//M입고 수정시 R출고
											data.set("ohdCntR", Number(clone.ohdCntR) - Number(clone.get(col.context.name) - currentValue))
											data.set("issCntR", Number(clone.get(col.context.name)));
										}
									}
									
									if(clone.item == kendolist[idx].item && returnChk!=1){
										data.set("ohdCntM", Number(clone.ohdCntM) + Number(clone.get(col.context.name) - currentValue))
									}
								} else if (col.context.name == "issCntM") {
									
									
									if(clone.item == kendolist[idx].item){
										//M 출고 수정시 C입고 수정
										data.set("rcvCntC", Number(clone.get(col.context.name)))
										data.set("ohdCntC", Number(clone.ohdCntC) + Number(clone.get(col.context.name) - currentValue))

										console.log("")
										data.set("ohdCntM", Number(clone.ohdCntM) - Number(clone.get(col.context.name) - currentValue))
									}
								} else if (col.context.name == "notiCntM") {
									if(clone.item == kendolist[idx].item){
										console.log("---자재출고시 라인소재 재고구하기")
										console.log(clone.ohdCntM)
										console.log( Number(clone.get(col.context.name)))
										console.log(currentValue)
										
										data.set("ohdCntM", Number(clone.ohdCntM) - Number(clone.get(col.context.name) - currentValue))
									}
								}
							} else if (clone.item == kendolist[idx].item && clone.idC == kendolist[idx].idC && (col.context.name == "rcvCntC" || col.context.name == "issCntC" || col.context.name == "notiCntC")) {
								console.log("CNC Modify")
								if (col.context.name == "rcvCntC") {
									//C 입고 수정시 M출고 수정
									data.set("issCntM", Number(clone.get(col.context.name)));
									data.set("ohdCntM", Number(clone.ohdCntM) - Number(clone.get(col.context.name) - currentValue));

									data.set("ohdCntC", Number(clone.ohdCntC) + Number(clone.get(col.context.name) - currentValue));
									console.log(data.ohdCntC);
								} else if (col.context.name == "issCntC") {
									//C 출고 수정시 P입고 수정
									data.set("rcvCntP", Number(clone.get(col.context.name)))
									data.set("ohdCntP", Number(clone.ohdCntP) + Number(clone.get(col.context.name) - currentValue))
									
									data.set("ohdCntC", Number(clone.ohdCntC) - Number(clone.get(col.context.name) - currentValue))
								} else if (col.context.name == "notiCntC") {
									data.set("ohdCntC", Number(clone.ohdCntC) - Number(clone.get(col.context.name) - currentValue))
								}
							} else if (clone.item == kendolist[idx].item && clone.idP == kendolist[idx].idP && (col.context.name == "rcvCntP" || col.context.name == "issCntP" || col.context.name == "notiCntP")) {
								console.log("Product Modify")
								if (col.context.name == "rcvCntP") {
									console.log("CNC Modify")
									
									//P 입고 수정시 C출고 수정
									data.set("issCntC", Number(clone.get(col.context.name)))
									data.set("ohdCntC", Number(clone.ohdCntC) - Number(clone.get(col.context.name) - currentValue))
									
									data.set("ohdCntP", Number(clone.ohdCntP) + Number(clone.get(col.context.name) - currentValue))
								} else if (col.context.name == "issCntP") {
									data.set("ohdCntP", Number(clone.ohdCntP) - Number(clone.get(col.context.name) - currentValue))
								} else if (col.context.name == "notiCntP") {
									data.set("ohdCntP", Number(clone.ohdCntP) - Number(clone.get(col.context.name) - currentValue))
								}
							} else {
								console.log("속하지 않음.");
							}
						})

						var scroll = $("div.k-grid-content").scrollTop();
						$("#wrapper").data("kendoGrid").dataSource.fetch();
						$("div.k-grid-content").scrollTop(scroll);
						
						
					});  */
					
				});
			}


		}).data("kendoGrid");
		
		
		$("#sDate").val(moment().format("YYYY-MM-")+"01");

		if(moment().format("HH:mm")<"09:00"){
			$("#eDate").val(moment().subtract(2, 'day').format("YYYY-MM-DD"))
		}else{
			$("#eDate").val(moment().subtract(1, 'day').format("YYYY-MM-DD"))
		}

		$("#sDate").datepicker({
			onSelect: function (e) {
				if (e > moment().format("YYYY-MM-DD")) {
					$("#sDate").val(nowDateInsert);
					alert("오늘 이후의 날짜를 선택할수 없습니다.")
		
					return false;
				} else {
					//e == 날짜 
					$("#sDate").val(e);
					getTable();
				}
			}
		})
		$("#eDate").datepicker({
			onSelect: function (e) {
				if (e > moment().format("YYYY-MM-DD")) {
					$("#eDate").val(nowDateInsert);
					alert("오늘 이후의 날짜를 선택할수 없습니다.")
		
					return false;
				} else {
					//e == 날짜 
					$("#eDate").val(e);
					getTable();
				}
			}
		})
		
		$(".chkRadio").css({
			"background": "#353535",
			"margin-right": getElSize(30),
			"padding-top": getElSize(20),
			"padding-bottom": getElSize(30),
			"padding-left": getElSize(10),
			"padding-left": getElSize(10)
		})
		//기본 css
		$("#sDate").css({
			"pointer-events": "none",
			"background": "gray"
		})
		
		$("input[type=radio][name=rdate]").change(function(){
			if($("input[type=radio][name=rdate]:checked").val()=="term"){
				$("#sDate").css({
					"pointer-events": "auto",
					"background": "white"
				})
			}else{
				$("#sDate").css({
					"pointer-events": "none",
					"background": "gray"
				})
				
			}
		})

		
		getTable();
		
		//저장 버튼	saveRow()
		$(".k-grid-custom").click(function () {
			var kendolist = kendotable.dataSource.data();
			console.log("=====저장할 리스트 목록=====")
			var changelist=[];
			for(i=0,len=kendolist.length; i<len; i++){
				//라인소재
				var arr={};
				arr.item=kendolist[i].prdNo;
				arr.proj="0005";
				arr.iniohdCnt=kendolist[i].iniohdCntL;
				arr.rcvCnt=kendolist[i].rcvCntL;
				arr.issCnt=kendolist[i].issCntL;
				arr.notiCnt=kendolist[i].notiCntL;
				arr.ohdCnt=kendolist[i].ohdCntL;
				arr.date=kendolist[i].date;
				changelist.push(arr);

				// R삭 일때 총 입고,불출,불량 수량 구하기
				var totalIn=0;
				var totalOut=0;
				var totalFail=0;
				
				for(j=0,lenth = kendolist.length; j<lenth; j++){
					if(kendolist[i].prdNo==kendolist[j].prdNo){
						totalIn += kendolist[j].rcvCntR;
						totalOut += kendolist[j].issCntR;
						totalFail += kendolist[j].notiCntR;
					}
				}
				
				//R삭
				var arr={};
				arr.item=kendolist[i].prdNo;
				arr.proj="0010";
				arr.iniohdCnt=kendolist[i].iniohdCntR;
				arr.rcvCnt=totalIn;
				arr.issCnt=totalOut;
				arr.notiCnt=totalFail;
/* 				arr.rcvCnt=kendolist[i].rcvCntR;
				arr.issCnt=kendolist[i].issCntR;
				arr.notiCnt=kendolist[i].notiCntR; */
				arr.ohdCnt=kendolist[i].ohdCntR;
				arr.date=kendolist[i].date;
				changelist.push(arr);
				
				//R삭	LH,RH 입고,불출,불량
				var arr={};
				arr.item=kendolist[i].item;
				arr.proj="0010";
				arr.iniohdCnt=0;
				/* arr.rcvCnt=totalIn;
				arr.issCnt=totalOut;
				arr.notiCnt=totalFail; */
				arr.rcvCnt=kendolist[i].rcvCntR;
				arr.issCnt=kendolist[i].issCntR;
				arr.notiCnt=kendolist[i].notiCntR;
				arr.ohdCnt=0;
				arr.date=kendolist[i].date;
				changelist.push(arr);
				
				
				//M삭
				var arr={};
				arr.item=kendolist[i].item;
				arr.proj="0020";
				arr.iniohdCnt=kendolist[i].iniohdCntM;
				arr.rcvCnt=kendolist[i].rcvCntM;
				arr.issCnt=kendolist[i].issCntM;
				arr.notiCnt=kendolist[i].notiCntM;
				arr.ohdCnt=kendolist[i].ohdCntM;
				arr.date=kendolist[i].date;
				changelist.push(arr);

				//C삭
				var arr={};
				arr.item=kendolist[i].item;
				arr.proj="0030";
				arr.iniohdCnt=kendolist[i].iniohdCntC;
				arr.rcvCnt=kendolist[i].rcvCntC;
				arr.issCnt=kendolist[i].issCntC;
				arr.notiCnt=kendolist[i].notiCntC;
				arr.ohdCnt=kendolist[i].ohdCntC;
				arr.date=kendolist[i].date;
				changelist.push(arr);
			}

			savelist=[]
			//0을 제외하고 넣기
			for(i=0,len=changelist.length; i<len; i++){
				if(changelist[i].rcvCnt==0 && changelist[i].issCnt==0 && changelist[i].notiCnt==0 && changelist[i].ohdCnt==0){
				}else{
					savelist.push(changelist[i]);
				}
			}
			console.log(savelist)
			$.showLoading()
			var url = "${ctxPath}/chart/stockProcessSave.do";
				var obj = new Object();
				obj.val = savelist;

				var param = "val=" + JSON.stringify(obj)
					+ "&date=" + $(".hasDatepicker").val();
				console.log(param);
				$.ajax({
					url: url,
					data: param,
					type: "post",
					success: function (data) {
						
						if(data=="success"){
							alert("저장완료 되었습니다.");
							getTable();
						}else{
							alert("실패했습니다.")
							$.hideLoading();
						}
						
					}
			})
			
		});
		//프린트 버튼
		$(".k-grid-print").click(function () {
			printChkWin=true;
			win = window.open("","","height=" + screen.height + ",width=" + screen.width + "fullscreen=yes");
	        self.focus();
	        win.document.open();
	        win.document.write('<'+'html'+'><'+'head'+'><'+'style'+'>');
	        win.document.write('body, td { font-family: Verdana; font-size: 10pt;}');
	        win.document.write('#adb { background-color:red	}');
	        win.document.write('<'+'/'+'style'+'><'+'/'+'head'+'><'+'body'+'>');
	        win.document.write(document.getElementById('printD').innerHTML);
	        win.document.write('<'+'/'+'body'+'><'+'/'+'html'+'>');
	        win.document.close();
	        
	        if(printChkWin==true){
				$(window).click(function() {
					//Hide the menus if visible
					alert('프린터 창이 열려 있습니다')
				});
			}
	        win.print();
	       
	        printChkWin=false;
			$(window).off("click");
	        
			win.close();
		})
	})
	
	function onGridKeydown(e) {
	    if (e.keyCode === kendo.keys.TAB) {
	        var grid = $(this).closest("[data-role=grid]").data("kendoGrid");
	        var current = grid.current();
	        
	        console.log("-----grid, current-----")
	        console.log(grid);
	        console.log(current)
	        console.log(current.hasClass("editable-cell"))
	        
	        if (!current.hasClass("editable-cell")) {
	          console.log(1)
	            var nextCell;
	            if (e.shiftKey) {
	                nextCell = current.prevAll(".editable-cell");
	                if (!nextCell[0]) {
	                    //search the next row
	                    var prevRow = current.parent().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev();
	                    var nextCell = prevRow.children(".editable-cell:last");
	                  console.log(2)
	                }
	              console.log(3)
	            } else {
	              console.log(4)
	                nextCell = current.nextAll(".editable-cell");
	                if (!nextCell[0]) {
	                  console.log(5)
	                    //search the next row
	                    var nextRow = current.parent().next();
	                    var nextCell = nextRow.children(".editable-cell:first");
	                }
	            }
	            
/* 	            console.log(nextCell)
	            console.log(nextCell[0]) */
	            grid.current(nextCell);
	            grid.editCell(nextCell[0]);
	        }
	
	    }
	};  
	
	var mergeChk=[];
	var mergeList = [];
	// cell 합치기 위해 class 추가
	function onDataBound(){
		/* $("#wrapper").data("kendoGrid").dataSource.getByUid("48ed3c39-7cba-4412-8387-5edbb66a186e")
		
		$(".cellMerge")[0].closest("tr") */
		
		mergeChk=[]; mergeList=[];
		var grid = $("#wrapper").data("kendoGrid").dataSource

		$(".cellMerge").each(function(idx,data){
			// tr값 가져오기
			var uid = $(".cellMerge")[idx].closest("tr").dataset.uid
			var RowItem = 	grid.getByUid(uid)
			
			
			
			/* var dataItem = $(container).closest("tr")[0].dataset.uid;
			var grid = $("#wrapper").data("kendoGrid");
			var row = $("#wrapper").data("kendoGrid").tbody.find("tr[data-uid='" + dataItem + "']");
			var idx = grid.dataSource.indexOf(grid.dataItem(row));
			 */
			
			
	//		var row = $("#wrapper").data("kendoGrid").tbody.find("tr[data-uid='" + uid + "']");
			 
			var row = $(this).closest("tr");
		    var rowIdx = $("tr", $("#wrapper").data("kendoGrid").tbody).index($(".cellMerge")[idx].closest("tr"));

			if(mergeChk.indexOf(RowItem.prdNo)==-1){
				mergeChk.push(rowIdx)
			}
			
			if(mergeChk.indexOf(RowItem.prdNo)==-1 && mergeChk.indexOf(rowIdx)!=-1){
				var count=0;
				// rowspan 몇으로 할지 정하기 위해서
				
				for(i=0, len=grid.data().length; i<len; i++){
//					console.log(data.prdNo + " ,, " + grid.data()[i].prdNo)
					if(RowItem.prdNo==grid.data()[i].prdNo){
						count ++;
					}
				}
				
				var arr={};
					arr.prdNo = RowItem.prdNo;
					arr.count = count;
				
				mergeChk.push(RowItem.prdNo);
				mergeList.push(arr);
				
			}else{
				if(mergeChk.indexOf(rowIdx)==-1)
				$(".cellMerge:eq(" + idx + ")").addClass("hidden");
//				$(".cellMerge:eq(" + idx + ")").attr("class","hidden")
			}
			
		})
		
		$(".cellMerge.hidden").attr("class","hidden");

 		$(".hidden").css("display","none");
		
		$(".cellMerge").each(function(idx,data){
			var uid = $(".cellMerge")[idx].closest("tr").dataset.uid
			var RowItem = 	grid.getByUid(uid)
			
			for(i=0, len=mergeList.length; i<len; i++){
//				console.log(data.prdNo + " ,, " + grid.data()[i].prdNo)
				if(RowItem.prdNo==mergeList[i].prdNo){
					$(".cellMerge:eq(" + idx + ")").attr("rowspan",mergeList[i].count);
				}
			}
		})
		
		console.log("wow")
	}
	function getTable() {

		$(".k-button").css({
		    "display":"inline"
	    })
			    
		classFlag = true;
		var url;
		var nowDate;
		var minusDate;
		if(moment().format("HH:mm")<"10:00"){
			nowDate=moment().subtract(1, 'day').format("YYYY-MM-DD")
			minusDate=moment().subtract(2, 'day').format("YYYY-MM-DD")
		}else{
			nowDate=moment().format("YYYY-MM-DD")
			minusDate=moment().subtract(1, 'day').format("YYYY-MM-DD")
		}
		
		//옜날에 쓰던 데이터
		
		var chkD = $("input[type=radio][name=rdate]:checked").val()
		//기간별 조회
		if(chkD=="term"){
			url = "${ctxPath}/chart/getTermStockStatus.do";

			$(".k-button").css({
			    "display":"none"});
			$(".k-button.k-button-icontext.k-grid-print").css({
			    "display":"inline"});
			$(".k-button.k-button-icontext.k-grid-excel").css({
			    "display":"inline"});
			    
		}else{	// 기본 조회
			url = "${ctxPath}/chart/getfinishedStockStatus.do";

			if($("#eDate").val()==minusDate){
				console.log("날자가 어제입니다.")
		
		//				url = "${ctxPath}/chart/getfinishedStockStatus.do"; 어제
				$(".k-button").css({
				    "display":"inline"
				})
			}else if( $("#eDate").val()==nowDate ){
				console.log("날자가 오늘입니다.")
		
		//				url = "${ctxPath}/chart/getStockStatusDay.do"; 오늘
				$(".k-button").css({
				    "display":"none"})
			}else{
				console.log("옛날꺼다")
				$(".k-button").css({
				    "display":"none"})
				    
				$(".k-button.k-button-icontext.k-grid-print").css({
				    "display":"inline"})
				$(".k-button.k-button-icontext.k-grid-excel").css({
				    "display":"inline"})
			}
		}
		
//		url = "${ctxPath}/chart/getfinishedStockStatus.do";
		$.showLoading()
		
		var param = "eDate=" + $("#eDate").val() +
					"&sDate=" + $("#sDate").val();
		nowDateInsert = $("#eDate").val();
		console.log(param)
		console.log(nowDateInsert)
		$.ajax({
			url: url,
			data: param,
			dataType: "json",
			type: "post",
			success: function (data) {
				var json = data.dataList;
				console.log(json)
				if(json.length==0){
					
				}else if(json.length!=0){
					if(json[0].chk==1){
						$(".k-button").css({
						    "display":"none"})
					}
				}
				
				chkDuple=[];
				
				//초기화
				sumAcRcv=0;	sumAcNoti=0;	sumAcIn=0;	sumAcIss=0;			//소재
				sumAcRcvL=0;												//L라인
				sumAcRcvR=0;	sumAcInR=0;	sumAcIssR=0;	sumAcNotiR=0;	//R삭
				sumAcRcvM=0;	sumAcInM=0;	sumAcIssM=0;	sumAcNotiM=0;	//M삭
				sumAcRcvC=0;	sumAcInC=0;	sumAcIssC=0;	sumAcNotiC=0;	//C삭
				sumAcRcvF=0;	sumAcInF=0;	sumAcIssF=0;	sumAcNotiF=0;	//도금
				sumAcRcvP=0;	sumAcInP=0;	sumAcIssP=0;	sumAcOutP=0;	sumAcNotiP=0;	//완제품
				
				
				$(json).each(function (idx, data) {
					var Lcnt=0;
					var Rcnt=0;
					data.ohdCntP = data.iniohdCntP+data.rcvCntP-data.issCntP-data.notiCntP;
			
					//총합계 중복 제거
					if(chkDuple.indexOf(data.prdNo)==-1){
						chkDuple.push(data.prdNo)
						// 총합계 누적 소재
						sumAcRcv+=data.rcvCnt;
						sumAcNoti+=data.notiCnt;
						sumAcIn+=data.sumIn;
						sumAcIss+=data.issCnt;
						
						//라인
						sumAcRcvL+=data.rcvCntL;
						
						//R삭
						sumAcRcvR+=data.rcvCntR;	sumAcInR+=data.sumInR;	sumAcIssR+=data.issCntR;	sumAcNotiR+=data.notiCntR;	//R삭
						sumAcRcvM+=data.rcvCntM;	sumAcInM+=data.sumInM;	sumAcIssM+=data.issCntM;	sumAcNotiM+=data.notiCntM;	//M삭
						sumAcRcvC+=data.rcvCntC;	sumAcInC+=data.sumInC;	sumAcIssC+=data.issCntC;	sumAcNotiC+=data.notiCntC;	//C삭
						sumAcRcvF+=data.rcvCntF;	sumAcInF+=data.sumInF;	sumAcIssF+=data.issCntF;	sumAcNotiF+=data.notiCntF;	//도금
						sumAcRcvP+=data.rcvCntP;	sumAcInP+=data.sumInP;	sumAcIssP+=data.issCntP;	sumAcOutP+=data.sumOutP;	sumAcNotiP+=data.notiCntP;	//완제품
					}
					
					//라인,R삭 총재고 합칠려고;
					$(json).each(function (idx1, data1) {

						//라인,R삭 총재고 합칠려고;
						if(data.prdNo==data1.prdNo){
							Lcnt=Lcnt+Number(data1.rcvCntM)
						}
	//							if(data.item==data1.item){
						if(data.prdNo==data1.prdNo){
							Rcnt=Rcnt+Number(data1.rcvCntR)
						}
						
						
					})
					if(data.minOpr=="0020"){
 						data.ohdCntL = data.iniohdCntL+data.rcvCntL-Lcnt;
 						
					}else{
						data.ohdCntL = data.iniohdCntL+data.rcvCntL-Rcnt;
						
					}
					
					
					
//					data.ohdCntR = data.iniohdCntR+data.rcvCntR-data.issCntR-data.notiCntR;
					data.ohdCntC = data.iniohdCntC+data.rcvCntC-data.issCntC-data.notiCntC;
					data.ohdCntM = data.iniohdCntM+data.rcvCntM-data.issCntM-data.notiCntM;
					
					data.ohdCntF = data.iniohdCntF+data.rcvCntF-data.issCntF-data.notiCntF;
					
					data.ohdCnt = data.iniohdCnt+data.rcvCnt-data.issCnt-data.notiCnt;
					
				});
				
				
				
				kendodata = new kendo.data.DataSource({
					data: json,
					batch: true,
					editable: false,
					group: [{field:"ex",dir:"asc"},{ field: "prdNo" ,dir: "asc"}],
					aggregate: [
						
						{ field: "sumIn", aggregate: "sum" },
						{ field: "sumInL", aggregate: "sum" },
						{ field: "sumInR", aggregate: "sum" },
						{ field: "sumInM", aggregate: "sum" },
						{ field: "sumInC", aggregate: "sum" },
						{ field: "sumInF", aggregate: "sum" },
						{ field: "sumInP", aggregate: "sum" },
						
				        { field: "rcvCnt", aggregate: "sum" },
				        { field: "issCnt", aggregate: "sum" },
				        { field: "notiCnt", aggregate: "sum" },

				        { field: "rcvCntL", aggregate: "sum" },
				        { field: "issCntL", aggregate: "sum" },
				        { field: "notiCntL", aggregate: "sum" },
				        
				        { field: "rcvCntR", aggregate: "sum" },
				        { field: "issCntR", aggregate: "sum" },
				        { field: "notiCntR", aggregate: "sum" },
				        
				        { field: "rcvCntM", aggregate: "sum" },
				        { field: "issCntM", aggregate: "sum" },
				        { field: "notiCntM", aggregate: "sum" },
				        
				        { field: "rcvCntC", aggregate: "sum" },
				        { field: "issCntC", aggregate: "sum" },
				        { field: "notiCntC", aggregate: "sum" },
				        
				        { field: "rcvCntF", aggregate: "sum" },
				        { field: "issCntF", aggregate: "sum" },
				        { field: "notiCntF", aggregate: "sum" },
				        
				        { field: "rcvCntP", aggregate: "sum" },
				        { field: "issCntP", aggregate: "sum" },
				        { field: "notiCntP", aggregate: "sum" },
				        { field: "sumOutP", aggregate: "sum" },
				    ],
					sort: [{
                    	field: "prdNo" , dir:"asc" 
                    },{
                    	field: "item" , dir:"asc"
                    },{
                    	field: "ex" , dir:"asc" 
                    }],
					height: 500,
					schema: {
						model: {
							id: "id",
							fields: {
								prdNo: { editable: false },
								item: { editable: false },
								
								sumIn: { editable: false },
								sumInL: { editable: false },
								sumInR: { editable: false },
								sumInC: { editable: false },
								sumInM: { editable: false },
								sumInF: { editable: false },
								sumInP: { editable: false },
								
								iniohdCnt: { editable: false },
								rcvCnt: { editable: false },
								issCnt: { editable: false },
								notiCnt: { editable: false },
								ohdCnt: { editable: false },

								iniohdCntL: { editable: false },
								rcvCntL: { editable: false },
								issCntL: { editable: true },
								notiCntL: { editable: false },
								ohdCntL: { editable: true },

								iniohdCntR: { editable: false },
								rcvCntR: { editable: true, type: "number" },
								issCntR: { editable: true, type: "number"},
								notiCntR: { editable: true, type: "number"},
								ohdCntR: { editable: true },

								iniohdCntM: { editable: false },
								rcvCntM: { editable: true ,type:"number"},
								issCntM: { editable: true, type: "number"},
								notiCntM: { editable: true, type: "number"},
								ohdCntM: { editable: true },
								
								iniohdCntC: { editable: false },
								rcvCntC: { editable: true ,type:"number"},
								issCntC: { editable: false },
								notiCntC: { editable: true, type: "number"},
								ohdCntC: { editable: true },

								iniohdCntF: { editable: false },
								rcvCntF: { editable: false },
								issCntF: { editable: false },
								notiCntF: { editable: false },
								ohdCntF: { editable: false },

								iniohdCntP: { editable: false },
								rcvCntP: { editable: false },
								issCntP: { editable: false },
								notiCntP: { editable: false },
								ohdCntP: { editable: true },
							}
						}
					}
				});
				
	
				kendotable.setDataSource(kendodata);

				$("button, input[type='time']").css("font-size", getElSize(40));

				$(".alarmTable td").css({
					"padding": getElSize(20),
					"font-size": getElSize(40),
					"border": getElSize(5) + "px solid black"
				});

				scrolify($('#table2'), getElSize(1450));


				$("body, html").css({
					"overflow": "hidden"
				})
				
				var scroll = $("div.k-grid-content").scrollTop();
				kendotable.dataSource.fetch();
				$("div.k-grid-content").scrollTop(scroll);
				
				$(".date").css("font-size",getElSize(45))
				
			
				console.log("--print--")
				console.log(json)
				
				var color="white";
				var FlagC=1;
				var days=new Date(nowDateInsert)
				var week= new Array('일요일','월요일','화요일','수요일','목요일','금요일','토요일')
				days=days.getDay();
				days=week[days]
				
				
				var table="<table id='printT' align='center' border='4' width='95%'>";
				table +="<thead><tr style='background-color:#D5D5D5;'><th colspan='46' rowspan='3' class='Thead'> 수불장 ( "+ nowDateInsert +"  " + days + " )</th><th colspan='9' class='sign' colspan='2'>결제</th></tr><tr><th class='sign' colspan='2'>담당</th><th class='sign' colspan='2'>과장</th><th class='sign' colspan='2'>상무이사</th><th class='sign' colspan='3'>부사장</th></tr><tr><th class='sign' colspan='2'>　</th><th class='sign' colspan='2'>　</th><th class='sign' colspan='2'>　</th><th class='sign' colspan='3'>　</th></tr>"
				table+= "<tr style='background-color:#D5D5D5;'>" +
								"<th rowspan='2' class='Ptitle'>차종(_RW)</th>" +
								"<th rowspan='2' class='Pitem'>부품명</th>" +
								"<th colspan='7' class='Pitem'>소재</th>" +
								"<th colspan='4' class='Pitem'>라인재고</th>" +
								"<th colspan='8' class='Pitem'>R삭</th>" +
								"<th colspan='8' class='Pitem'>MCT삭</th>" +
								"<th colspan='8' class='Pitem'>CNC삭</th>" +
								"<th colspan='8' class='Pitem'>도금</th>" +
								"<th colspan='8' class='Pitem'>완제품</th>" +
								"<th colspan='2' class='Pitem'>통합 재고</th>" +
							"</tr>"  +
						"<tr style='background-color:#D5D5D5;'>" +
							"<th class='Pbox'>전일재고</th>" +
							"<th class='Pbox'>입고</th>" +
							"<th class='Pbox'>불량</th>" +
							"<th class='Pbox'>누적입고</th>" +
							"<th class='Pbox'>출고</th>" +
							"<th class='Pbox'>창고재고</th>" +
							"<th class='Pspace'>　</th>" +
							"<th class='Pbox'>전일재고</th>" +
							"<th class='Pbox'>입고</th>" +
							"<th class='Pbox'>현재고</th>" +
							"<th class='Pspace'>　</th>" +
							"<th class='Pbox'>전일재고</th>" +
							"<th class='Pbox'>입고</th>" +
							"<th class='Pbox'>누적생산수량</th>" +
							"<th class='Pbox'>출고</th>" +
							"<th class='Pbox'>불량</th>" +
							"<th class='Pbox'>불량 누계</th>" +
							"<th class='Pbox'>재고</th>" +
							"<th class='Pspace'>　</th>" +
							"<th class='Pbox'>전일재고</th>" +
							"<th class='Pbox'>입고</th>" +
							"<th class='Pbox'>누적생산수량</th>" +
							"<th class='Pbox'>출고</th>" +
							"<th class='Pbox'>불량</th>" +
							"<th class='Pbox'>불량 누계</th>" +
							"<th class='Pbox'>재고</th>" +
							"<th class='Pspace'>　</th>" +
							"<th class='Pbox'>전일재고</th>" +
							"<th class='Pbox'>입고</th>" +
							"<th class='Pbox'>누적생산수량</th>" +
							"<th class='Pbox'>출고</th>" +
							"<th class='Pbox'>불량</th>" +
							"<th class='Pbox'>불량 누계</th>" +
							"<th class='Pbox'>재고</th>" +
							"<th class='Pspace'>　</th>" +
							"<th class='Pbox'>전일재고</th>" +
							"<th class='Pbox'>입고</th>" +
							"<th class='Pbox'>누적생산수량</th>" +
							"<th class='Pbox'>출고</th>" +
							"<th class='Pbox'>불량</th>" +
							"<th class='Pbox'>불량 누계</th>" +
							"<th class='Pbox'>재고</th>" +
							"<th class='Pspace'>　</th>" +
							"<th class='Pbox'>전일재고</th>" +
							"<th class='Pbox'>입고</th>" +
							"<th class='Pbox'>누적생산수량</th>" +
							"<th class='Pbox'>출고</th>" +
							"<th class='Pbox'>불량</th>" +
							"<th class='Pbox'>불량 누계</th>" +
							"<th class='Pbox'>재고</th>" +
							"<th class='Pspace'>　</th>" +
							"<th class='Pbox'>총 재고</th>" +
							"<th class='Pbox'>재고일수</th>" +
						"</tr>"
						"</thead><tbody>";

				$(json).each(function(idx, data){
						
					if(idx!=0){
						if(json[idx].prdNo==json[idx-1].prdNo){
							color=color
						}else{
							if(FlagC==1){
								color="white"
								FlagC=2
							}else{
								color="#D5D5D5"
								FlagC=1
							}
						}
					}
					//40개 빈공간 빼면
	//						var arr=new Object();
	//						var name = decodeURIComponent(data.name).replace(/\+/gi, " ");
						//예외 리스트
						if(data.prdNo=="FS_FRT_RH_RW" || data.prdNo=="FS_FRT_LH_RW" || data.prdNo=="PM_FRT_17_KR_LH_RW" || data.prdNo=="PM_FRT_17_KR_RH_RW" || data.prdNo=="TQ_FRT_4WD_RW" || data.prdNo=="VQ_RR_LH_RW" || data.prdNo=="VQ_RR_RH_RW"){
						}else{

							table+="<tr style='background-color:"+color+";'><td align='center' >"
							table+=data.prdNo.substr(0,data.prdNo.lastIndexOf("_"))+"</td><td align='center'>";
							table+=data.item.substr(data.item.lastIndexOf("_")+1,2)+"</td><td align='center'>";
							table+=data.iniohdCnt + "</td><td align='center'>";
							table+=data.rcvCnt+"</td><td align='center'>";
							table+=data.notiCnt+"</td><td align='center'>";
							table+=data.issCnt+"</td><td align='center'>";
							table+=data.issCnt+"</td><td align='center'>";
							table+=data.ohdCnt+"</td><td align='center' class='Pspace'>";
							
							table+="　"+"</td><td align='center'>";
							table+=data.iniohdCntL+"</td><td align='center'>";
							table+=data.rcvCntL+"</td><td align='center'>";
							table+=data.ohdCntL+"</td><td align='center' class='Pspace'>";
							
							table+="　"+"</td><td align='center'>";
							table+=data.iniohdCntR + "</td><td align='center'>";
							table+=data.rcvCntR+"</td><td align='center'>";
							table+='-'+"</td><td align='center'>";
							table+=data.issCntR+"</td><td align='center'>";
							table+=data.notiCntR+"</td><td align='center'>";
							table+='-'+"</td><td align='center'>";
							table+=data.ohdCntR+"</td><td align='center' class='Pspace'>";
							
							table+="　"+"</td><td align='center'>";
							table+=data.iniohdCntM + "</td><td align='center'>";
							table+=data.rcvCntM+"</td><td align='center'>";
							table+='-'+"</td><td align='center'>";
							table+=data.issCntM+"</td><td align='center'>";
							table+=data.notiCntM+"</td><td align='center'>";
							table+='-'+"</td><td align='center'>";
							table+=data.ohdCntM+"</td><td align='center' class='Pspace'>";
							
							table+="　"+"</td><td align='center'>";
							table+=data.iniohdCntC + "</td><td align='center'>";
							table+=data.rcvCntC+"</td><td align='center'>";
							table+='-'+"</td><td align='center'>";
							table+=data.issCntC+"</td><td align='center'>";
							table+=data.notiCntC+"</td><td align='center'>";
							table+='-'+"</td><td align='center'>";
							table+=data.ohdCntC+"</td><td align='center' class='Pspace'>";

							table+="　"+"</td><td align='center'>";
							table+=data.iniohdCntC + "</td><td align='center'>";
							table+=data.rcvCntF+"</td><td align='center'>";
							table+='-'+"</td><td align='center'>";
							table+=data.issCntF+"</td><td align='center'>";
							table+=data.notiCntF+"</td><td align='center'>";
							table+='-'+"</td><td align='center'>";
							table+=data.ohdCntF+"</td><td align='center' class='Pspace'>";
							
							table+="　"+"</td><td align='center'>";
							table+=data.iniohdCntP + "</td><td align='center'>";
							table+=data.rcvCntP+"</td><td align='center'>";
							table+='-'+"</td><td align='center'>";
							table+=data.issCntP+"</td><td align='center'>";
							table+=data.notiCntP+"</td><td align='center'>";
							table+='-'+"</td><td align='center'>";
							table+=data.ohdCntP+"</td><td align='center' class='Pspace'>";
							
							table+="　"+"</td><td align='center'>";
							table+=data.ohdCnt + data.ohdCntL + data.ohdCntR + data.ohdCntM + data.ohdCntC + data.ohdCntP+"</td><td align='center'>";
							table+='-'+"</td>";
							
							table+="</tr>"
						}

				});
				
				table+="</tbody></table>"
						
				
				$("#printD").empty();
				
				$("#printD").append(table);
				
				$("#printT tr th.Ptitle").css("width","5%");
				$("#printT tr th.Pitem").css("width","3%");
				$("#printT tr th.Pbox").css("width","2.3%");
				$("#printT tr th.sign").css("height","2.3%");
				$("#printT tr th.sign").css("height",30);
				$("#printT tr th").css("font-size",1)
				$("#printT tr td").css("font-size",1)
				$("#printT tr th.Thead").css("font-size",getElSize(74))
				$("#printT tr th.sign").css("font-size",12);

				$("#printT tr th").css("padding",0)
				$("#printT tr td").css("padding",0)

				$("#printT tr td.Pspace").css("background","yellow")
				
				$("#wrapper").find("table").on("keydown", onGridKeydown);

		        //////////	
				$.hideLoading();
			}, error: function (request, status, error) {
				alert("code:" + request.status + "\n" + "message:" + request.responseText + "\n" + "error:" + error);
			}
		});
		};
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	
	<div id="container">
		<table id="table" style="border-collapse: collapse; ">
		
			<Tr>
				
				<td rowspan="10" id="svg_td" style="vertical-align: top; position:relative; background:#26282c;">
					<div id="wrapperTable">
						<table style="width: 100%">
							<Tr>
								<Td style="text-align: center; vertical-align: middle;">
									<div id="wrapper">
									</div>
								</Td>
							</Tr>
						</table>
					</div>
				</td>
			</Tr>
			
		</table>
	 </div>
	<div id="printD" style="display: none;">
	</div>

</body>
</html>	