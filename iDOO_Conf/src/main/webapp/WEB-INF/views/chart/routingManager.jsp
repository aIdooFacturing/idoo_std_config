<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/x-kendo-template" id="windowTemplate">
   <center>
       <button class="k-button" id="yesButton">${check}</button>
       <button class="k-button" id="noButton">${cancel}</button>
   </center>
</script>
<script type="text/javascript">
   var ctxPath = "${ctxPath}";
   var chk_del = "${chk_del}";
   var fromDashboard = "${fromDashBoard}";
   var targetWidth = 3840;
   var targetHeight = 2160;

   var originWidth = window.innerWidth;
   var originHeight = window.innerHeight;
   
   var contentWidth = originWidth;
   var contentHeight = targetHeight/(targetWidth/originWidth);
   
   var screen_ratio = getElSize(240);
   
   if(originHeight/screen_ratio<9){
      contentWidth = targetWidth/(targetHeight/originHeight)
      contentHeight = originHeight; 
   };
   
   function getElSize(n){
      return contentWidth/(targetWidth/n);
   };
   
   function setElSize(n) {
      return Math.floor(targetWidth / (contentWidth / n));
   };
   var marginWidth = (originWidth-contentWidth)/2;
   var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<script src="${ctxPath }/js/kendo_lib.js"></script>
<style>
*{
   margin: 0px;
   padding: 0px;
}
body{
   margin: 0px;
   padding: 0px;
   width: 100%;
   height: 100%;
   overflow : hidden;
   background-color: black;
}

#grid .k-grid-header th.k-header>.k-link{
	color:white;
}

#grid{
	background: black;
	border-color : #222327;
}

#grid .k-grid-header {
	border-color : #222327;
}

#grid .k-grid-header-wrap {
	border-color : #222327;
}

</style> 
<script type="text/javascript">

	const loadPage = () =>{
		createMenuTree("config", "routingManager")	
	}
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};
	
	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};


   function setDate(){
      var date = new Date();
      date.setDate(date.getDate()-2)
      var year = date.getFullYear();
      var month = addZero(String(date.getMonth()+1));
      var day = addZero(String(date.getDate()));
      
      
      $(".date").val(year + "-" + month + "-" + day);
   };
   
//   var handle = 0;
   
   function getPrdNo(){
      var url = "${ctxPath}/chart/getMatInfo.do";
      var param = "shopId=" + shopId;
      
      $.ajax({
         url : url,
         dataType : "json",
         type : "post",
         success : function(data){
            var json = data.dataList;
            
            var options = "";
            $(json).each(function(idx, data){
               options += "<option value='" + data.prdNo + "'>" + data.prdNo + "</option>";
            });
            
            $("#prdNo").html(options).change(getRoutingInfo);
            
            getRoutingInfo()
            //getOprNo();
         }
      });
   }
   
   function getOprNo(){
      var url = "${ctxPath}/chart/getOprNo4Routing.do";
      var param = "prdNo=" + $("#prdNo").val();
      
      $.ajax({
         url : url,
         data : param,
         type : "post",
         dataType : "json",
         success : function(data){
         var json = data.dataList;
               
            var options = "";
            $(json).each(function(idx, data){
               options += "<option value='" + data.oprNo + "'>" + data.oprNo + "</option>";
            });
            
            if(json.length==0){
               options += "<option>공정 없음</option>";
            }
            
            $("#oprNo").html(options).change(getDvc);
            
            getDvc();
            
         }
      });
   };
   
   var dvcIdArray = [];
    function categoryDropDownMachineEditor(container, options) {
       $('<input required name="' + options.field + '"/>')
       .appendTo(container)
       .kendoDropDownList({
          autoBind: false,
          dataTextField: "dvcName",
          dataValueField: "dvcId",
          dataSource: dvcIdArray
      });
   }
    
   function getDvc(){
      var url = "${ctxPath}/chart/getDvc.do";
      var param = "prdNo=" + $("#prdNo").val() + 
               "&oprNo=" + $("#oprNo").val();
      
      $.ajax({
         url : url,
         data : param,
         type : "post",
         dataType : "json",
         success : function(data){
            var json = data.dataList;
            
            var options = "";
            $(json).each(function(idx, data){
               options += "<option value='" + data.dvcId + "'>" + decode(data.dvcName) + "</option>";
            });
            
            if(json.length==0){
               options += "<option value='0'>장비 없음</option>";
            }
            
            $("#dvcId").html(options).change(getRoutingInfo);
            getRoutingInfo();
         }
      });
      
   };
   
   
   function getRoutingInfo(){
      var list=[];
      var url = "${ctxPath}/chart/getRoutingInfo.do";
      var param = "prdNo=" + $("#prdNo").val() + 
               "&oprNo=" + $("#oprNo").val() + 
               "&dvcId=" + $("#dvcId").val();
      
      $.ajax({
         url : url,
         data : param,
         type : "post",
         dataType : "json",
         success : function(data){
            ty = "update"
            var json = data.dataList;
            for(i=0;i<json.length;i++){
               var arr=new Object();
               arr.CVT=json[i].CVT;
               arr.cmpCd=json[i].cmpCd
               arr.cylTmSe=json[i].cylTmSe
               arr.dvcId=json[i].dvcId
               arr.grpCd=json[i].grpCd
               arr.id=json[i].id
               arr.machCd=json[i].machCd
               arr.mnPrgNo=json[i].mnPrgNo
               arr.oprNo=json[i].oprNo
               arr.seq=json[i].seq
               arr.wcCd=json[i].wcCd
               arr.beforeId=json[i].dvcId
               list.push(arr);
            }
            console.log(json);
            console.log(list);
            
            $("#grid").kendoGrid({
                  dataSource: new kendo.data.DataSource({
                      data: list,
                      batch: true,
                      schema: {
                              model: {
                                id: "dvcId",
                                fields: {
                                   id: { editable: false},
                                   oprNo: { editable: true},
                                   dvcId : { editable: true},
                                   wcCd : { editable : true},
                                   //machCd: { editable : true },
                                   mnPrgNo: { editable : true },
                                   cmpCd : { editable : true },
                                   //cylTmSe : { editable : true },
                                   //CVT : { editable : true },
                                   grpCd : { editable : true },
                                   seq : { editable : true },
                                }
                              }
                          }
                 }),
                 change: function() {
                      var cell = this.select();
                      var cellIndex = cell[0].cellIndex;
                      var column = this.columns[cellIndex];
                      var dataItem = this.dataItem(cell.closest("tr"));
                               
                      selectedRow = dataItem            
                    
                 },
                      
                 selectable: "row",
                 
                  height: getElSize(1560),
                  groupable: false,
                  sortable: true,
                  
                /*   pageable: {
                      refresh: true,
                      pageSizes: true,
                      buttonCount: 5
                  }, */
                  columns: [
                        {
                           field: "oprNo"
                          ,title: "${operation} *"
                          ,headerAttributes: {
               				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(42)+ "; color:white;"
                 		  },attributes: {
                              style: "text-align: center; font-size: " + getElSize(35) + "px"
                          },
                        },
                        {
                           field: "dvcId",
                           title: "${device} *",
                           editor: categoryDropDownMachineEditor,
                           template: "#= dvcName(dvcId) #",
                           headerAttributes: {
                				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(42)+ "; color:white;"
                		  },attributes: {
                              style: "text-align: center; font-size: " + getElSize(35) + "px"
                              }
                        },
                        {
                               field: "wcCd",
                               title: "${work_center} *",
                               headerAttributes: {
                    				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(42)+ "; color:white;"
                    		  },attributes: {
                                   style: "text-align: center; font-size: " + getElSize(35) + "px"
                                 }
                           },
                           {
                               field: "mnPrgNo",
                               title: "${program}",
                               headerAttributes: {
                    				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(42)+ "; color:white;"
                    		  },attributes: {
                                   style: "text-align: center; font-size: " + getElSize(35) + "px"
                                 }
                           },
                           {
                               field: "cmpCd",
                               title: "${elevated_code}",
                               headerAttributes: {
                    				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(42)+ "; color:white;"
                    		  },attributes: {
                                   style: "text-align: center; font-size: " + getElSize(35) + "px"
                                 }
                           },
                           {
                               field: "grpCd",
                               title: "${group_count} *",
                               headerAttributes: {
                    				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(42)+ "; color:white;"
                    		  },attributes: {
                                   style: "text-align: center; font-size: " + getElSize(35) + "px"
                                 }
                           },
                           {
                               field: "seq",
                               title: "SEQ *",
                               headerAttributes: {
                    				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(42)+ "; color:white;"
                    		  },attributes: {
                                   style: "text-align: center; font-size: " + getElSize(35) + "px"
                                 }
                           },
                           {
//참조
                        command : [{
                                 name : "${del}",
                                 click : function(e){
                                    //del_btn_evt(e,this,"delRouting()")
                                    //delRouting(e,this)
                                 }
                              }],
                              headerAttributes: {
                   				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(42)+ "; color:white;"
                   		  },attributes: {
                                   style: "text-align: center; font-size: " + getElSize(35) + "px;"
                                 }
                           }
                        ],
                        editable: true
                        
              });//kendo
            
            $("#grid th").css({
            	"font-family" : "NotoSansCJKkrBold",
        		   "font-size" : getElSize(44) + "px",
        		   "background-image" : "none",
        		   "background-color" : "#353542"
            });
            
            $(".k-button").css({
               "padding" : getElSize(10)
            })
            
            $("#grid tr:odd").css({
               "background-color":"rgb(185, 185, 186)",
               "color" : "white"
            });
            
            $("#grid tr:even").css({
               "background-color":"rgb(207, 207, 208)",
               "color" : "white"
            });
            
            $(".checkbox").css({
               "width" : getElSize(70) + "px",
               "height" : getElSize(70) + "px"
            })
            grid = $("#grid").data("kendoGrid");
            //grid.table.on("click", ".checkbox" , selectRow);
            grid.bind("dataBound", function(){
               $("#grid th").css({
            	   "font-family" : "NotoSansCJKkrBold",
           		   "font-size" : getElSize(44) + "px",
           		   "background-image" : "none",
           		   "background-color" : "#353542"
               });
               
               $(".k-button").css({
                  "padding" : getElSize(10)
               })
               
               $("#grid tr:odd").css({
                  "background-color":"rgb(185, 185, 186)",
                  "color" : "white"
               });
               
               $("#grid tr:even").css({
                  "background-color":"rgb(207, 207, 208)",
                  "color" : "white"
               });
               
               $("#grid .k-button").click(function(e){
                  del_btn_evt(e,this,"delRouting()")
               });
            });
            
            $("#grid .k-button").click(function(e){
               del_btn_evt(e,this,"delRouting()")
            });
            
            $("#grid").css("background-color","gray")
            
            $("#grid td").on("keyup", function (e) {
                /*  //if current key is Enter
                 var row = $(this).closest("tr");
                 grid = $("#grid").data("kendoGrid"),
                 dataItem = grid.dataItem(row);
                       
                 console.log(dataItem.cnt,dataItem.prc)
                dataItem.set("totalPrc", dataItem.cnt * dataItem.prc)     */        
            })
            
            grid.refresh();
         }
      });
   };
   
   
   var evt_dupl = false;
   
   var ty;
   function addRow(){
      ty = "add";
      /* if(grid.dataSource.data().length>0){
         alert("이미 라우팅 데이터가 있습니다.");
         return;
      } */
      grid.dataSource.add({
         "dvcId" : 0
      });         
   };
   
   $("#grid tr:odd").css({
      "background-color":"rgb(185, 185, 186)",
      "color" : "white"
   });
   
   $("#grid tr:even").css({
      "background-color":"rgb(207, 207, 208)",
      "color" : "white"
   });
   
   var insertRow = [];
   var updateRow = [];
   
   function saveRow(){
      insertRow = [];
      updateRow = [];
       var grid = $("#grid").data("kendoGrid");
      var ssss=grid.dataSource.data();       
/*      alert(ssss.length);
 */      $(grid.dataSource.data()).each(function(idx, data){   
/*          for(j=0;j<ssss.length;j++){
            if(data.dvcId==ssss[j].dvcId){
               alert("장비 중복입니다");
            }
         }
 */         if(data.id==0){
            var obj = {};
            obj.oprNo = data.oprNo;
            obj.dvcId = data.dvcId;
            obj.wcCd = data.wcCd;
            if(typeof(data.cmpCd)=="undefined"){
            	obj.cmpCd=""
            }else{
                obj.cmpCd = data.cmpCd;//완료코드
            }
            if(typeof(data.mnPrgNo)=="undefined"){
            	obj.mnPrgNo=""
            }else{
                obj.mnPrgNo = data.mnPrgNo;//프로그램
            }
            obj.grpCd = data.grpCd;
            obj.seq = data.seq;
            obj.prdNo = $("#prdNo").val();
            obj.beforeId =data.beforeId;
            insertRow.push(obj)
            
            if(typeof(data.oprNo)=="undefined"){
               alert("공정이 입력되지 않았습니다.");
               return
            }else if(data.dvcId==0){
               alert("장비가 선택되지 않았습니다.");
               return
            }else if(typeof(data.wcCd)=="undefined"){
               alert("워크센터가 입력되지 않았습니다.");
               return
            }else if(typeof(data.grpCd)=="undefined"){
               alert("크룹카운트가 입려되지 않았습니다.");
               return
            }else if(typeof(data.seq)=="undefined"){
               alert("SEQ 입력되지 않았습니다.");
               return
            }/* else if(typeof(data.cmpCd)=="undefined"){
               alert("완료코드가 입력되지 않았습니다.");
               return
            }else if(typeof(data.mnPrgNo)=="undefined"){
               alert("프로그램이 입력되지 않았습니다.");
               return
            } */else if(isNaN(data.grpCd)){
               alert("그룹카운트가 숫자가 아닙니다.");
               return;
            }
            
         }else{
            var obj = {};
            obj.oprNo = data.oprNo;
            obj.dvcId = data.dvcId;
            obj.wcCd = data.wcCd;
            obj.mnPrgNo = data.mnPrgNo;
            obj.cmpCd = data.cmpCd;
            obj.grpCd = data.grpCd;
            obj.seq = data.seq;
            obj.prdNo = $("#prdNo").val();
            obj.beforeId =data.beforeId;
            
            if(typeof(data.oprNo)=="undefined"){   
               alert("공정이 입력되지 않았습니다.");
               return
            }else if(data.dvcId==0){
               alert("장비가 선택되지 않았습니다.");
               return
            }else if(typeof(data.wcCd)=="undefined"){
               alert("워크센터가 입력되지 않았습니다.");
               return
            }else if(typeof(data.grpCd)=="undefined"){
               alert("크룹카운트가 입려되지 않았습니다.");
               return
            }else if(typeof(data.seq)=="undefined"){
               alert("SEQ 입력되지 않았습니다.");
               return
            }else if(typeof(data.cmpCd)=="undefined"){
               alert("완료코드가 입력되지 않았습니다.");
               return
            }else if(typeof(data.mnPrgNo)=="cmpCd"){
               alert("프로그램이 입력되지 않았습니다.");
               return
            }else if(isNaN(data.grpCd)){
               alert("그룹카운트가 숫자가 아닙니다.");
               return;
            }
            
            updateRow.push(obj)
         }
      })
      
      console.log(updateRow);
      
      if(updateRow.length!=0){
         updatRouting();   
      }else{
         addRoutingRow();
         
      }
      
      /* if(ty=="update"){
         updateRow();
      }else{
         addRoutingRow();
      } */
   };
   
   function dvcName(dvcId) {
	   console.log(dvcId)
      for (var i = 0; i < dvcIdArray.length; i++) {
         if (dvcIdArray[i].dvcId == dvcId) {
            return dvcIdArray[i].dvcName;
         }
       }
   }
   
   function delRouting(e,el){
      
      /* console.log("call del func")
      if(!confirm("삭제 하시겠습니까?")){
         return;
      } */
   
      var url = "${ctxPath}/chart/delRouting.do";
      
      var param = "prdNo=" + $("#prdNo").val() + 
               "&dvcId=" + del_id;
      
      $.ajax({
         url : url,
         data: param,
         type : "post",
         dataType : "text",
         success : function(data){
            if(data=="success"){
               //getRoutingInfo();
            }
         }
      });
      
   };
   
   var del_id;
   function addRoutingRow(){
      var obj = new Object();
      obj.val = insertRow;
   
      var param = "val=" + JSON.stringify(obj);
      
      var url = "${ctxPath}/chart/addRouting.do";

      $.ajax({
         url : url,
         data : param,
         type : "post",
         dataType : "text",
         success : function(data){
            if(data=="success"){
               alert("저장 되었습니다.");
               getRoutingInfo();
            }else if(data=="fail"){
               alert("공정,장비 중복된 값이 있습니다");
               return;
            }
         }
      });   
   };
   
   function updatRouting(){
      var obj = new Object();
      obj.val = updateRow;
   
      var param = "val=" + JSON.stringify(obj);
      
      var url = "${ctxPath}/chart/updatRouting.do";
   
      $.ajax({
         url : url,
         data : param,
         type : "post",
         dataType : "text",
         success : function(data){
            if(data=="success"){
               if(insertRow.length==0){
                  alert("저장 되었습니다.");
                  getRoutingInfo();
               }else{
                  addRoutingRow();
               }
               //alert("수정 되었습니다.");
               //getRoutingInfo();
            }else if(data=="fail"){
               alert("공정, 장비 중복된 값이 있습니다");
               return;
            }
         }
      });
   };
   
   function getDvcIDs(){
      var url = "${ctxPath}/chart/getDvc.do";
      
      $.ajax({
         url : url,
         dataType : "json",
         type : "post",
         success : function(data){
            var json = data.dataList;
            
            var prdNo = {"dvcId" : 0,
                  "dvcName" : "선택"}
            dvcIdArray.push(prdNo)
               
            $(json).each(function(idx, data){
               var prdNo = {"dvcId" : data.dvcId,
                        "dvcName" : decode(data.dvcName),}
               dvcIdArray.push(prdNo)
            });
            
            getAllDeliverer()
         }
      });
   };
   
   function getAllDeliverer(){
		var url = "${ctxPath}/chart/getAllDeliverer.do";
		
		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				$(json).each(function(idx, data){
					var prdNo = {
							"dvcId" : data.delivererId,
							"dvcName" : decode(data.delivererName)
					}
					dvcIdArray.push(prdNo);
				})
			},
			error : function(e1,e2,e3){
			}
		});
	 };
   
   $(function(){
      getPrdNo();
      setDate();
      getDvcIDs();
//      createNav("mainten_nav", 3);
      
//      setEl();
      time();
      setEl2();
      
      $("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
      
      window.setInterval(function(){
         var width = window.innerWidth;
         var height = window.innerHeight;

         if(width!=originWidth || height!=originHeight){
            location.reload();
         };
      },1000*10);
      
      
      chkBanner();
   });
   
   function getTime(){
      var date = new Date();
      var hour = addZero(String(date.getHours()));
      var minute = addZero(String(date.getMinutes()));
      var second = addZero(String(date.getSeconds()));
      
      return hour + ":" + minute;
   };
   
/*    function addZero(n){
      if(n.length=="1"){
         n = "0" + n;
      };
      return n;
   }; */
   
  /*  function setEl(){
      var neonColor = "#0096FF";
      
      var width = window.innerWidth;
      var height = window.innerHeight;
      
      $(".right").css({
         "height" : getElSize(120)
      });
      
      $(".left, .menu_left").css({
         "width" : getElSize(495)         
      })
      
      $("#container").css({
         "width" : contentWidth,
         "height" : contentHeight,
      });
      
      $("#container").css({
         "margin-left" : (originWidth/2) - ($("#container").width()/2),
         "margin-top" : (originHeight/2) - ($("#container").height()/2)
      })
      
      
      $("#intro").css({
         "position" : "absolute",
         "bottom" : 0 + marginHeight,
         "font-size" : getElSize(140),
         "font-weight" : "bolder",
         "z-index" : 9999
      });
      
      $("#intro_back").css({
         "width" : originWidth,
         "display" : "none",
         "height" : getElSize(180),
         "opacity" : 0.5,
         "position" : "absolute",
         "background-color" : "black",
         "bottom" : 0 + marginHeight,
         "z-index" : 9999
      })
      
      $("#time").css({
         "color" : "white",
         "position" : "absolute",
         "font-size" : getElSize(40),
         "top" : getElSize(25) + marginHeight,
         "right" : getElSize(30) + marginWidth
      });
      
      $("#table").css({
         "position" : "absolute",
         "width" : $("#container").width(),
         "top" : getElSize(100) + marginHeight
      });
      
      $("#table2 td").css({
         "padding" : getElSize(20),
         "font-size": getElSize(40),
         "border": getElSize(5) + "px solid black"
      });
      
      $(".right").css({
         "width" : contentWidth - $(".left").width() 
      });
      
      $(".menu_right").css({
         "width" : $(".right").width()
      })
      
      $("#home").css({
         "cursor" : "pointer"
      })
      
      $("#title_right").css({
         "position" : "absolute",
         "z-index" : 2,
         "color" : "white",
         "font-size" : getElSize(40),
         "top" : getElSize(130) + marginHeight,
         "right" : getElSize(30) + marginWidth
      });
      
      $(".nav_span").css({
         "color" : "#8D8D8D",
         "position" : "absolute",
         "font-size" : getElSize(45),
         "margin-top" : getElSize(20),
         "margin-left" : getElSize(20)
      });
      
      $("#selected").css({
         "color" : "white",
      });
      
      $("span").parent("td").css({
         "cursor" : "pointer"
      });
      
      $(".title_span").css({
         "color" : "white",
         "font-size" : getElSize(40),
         "background-color" : "#353535",
         "padding" : getElSize(15)
      });
      
      
      $("select, button, input").css({
         "font-size" : getElSize(40),
         "margin-left" : getElSize(20),
         "margin-right" : getElSize(20)
      });
      
      $("button").css({
         "padding" : getElSize(15),
      })
      
      $("#search").css({
         "cursor" : "pointer",
         "width" : getElSize(80),
      });
      
      $("#content_table td, #content_table2 td").css({
         "color" : "#BFBFBF",
         "font-size" : getElSize(50)
      });
      
      $(".tmpTable, .tmpTable tr, .tmpTable td").css({
         "border": getElSize(5) + "px solid rgb(50,50,50)"
      });
      
      $(".tmpTable td").css({
         "padding" : getElSize(10),
         "height": getElSize(100)
      });
      
      $(".contentTr").css({
         "font-size" : getElSize(60)
      });
      
      
      $(".wrapper").css({
         "width" : $(".menu_right").width(),
         "margin-top" : getElSize(120),
         "position" : "absolute"   
      });
      
      $("#intro").css({
         "font-size" : getElSize(100)
      });
      
      $("#popup2 table td").css({
         "color" : "white",
         "font-size" : getElSize(70) + "px"
      });
      
      $("#popup2").css({
         "padding" : getElSize(50) + "px",
         "left" : (originWidth/2) - ($("#popup2").width()/2),
         "top" : (originHeight/2) - ($("#popup2").height()/2)
      });
      
      $("select").css({
         "font-size" : getElSize(80) + "px"
      });
      
      $("#save, #add").css({
         "font-size" : getElSize(80) + "px"
      });
   }; */
   
   
   	function setEl2(){
	  var width = window.innerWidth;
	  var height = window.innerHeight;
	      
	  $("#container").css({
			"width" : contentWidth - getElSize(20),
			"margin-top" : getElSize(100)
		})
				
		$("#content_table").css({
			"margin-top" : getElSize(100)
		})
				
				
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight,
			"margin-top" : getElSize(250)
		});

	      
/* 	      $("#time").css({
	         "color" : "white",
	         "position" : "absolute",
	         "font-size" : getElSize(40),
	         "top" : getElSize(25) + marginHeight,
	         "right" : getElSize(30) + marginWidth
	      }); */
	      
	      $("#table").css({
	         "position" : "absolute",
	         "width" : $("#container").width(),
	         "top" : getElSize(100) + marginHeight
	      });
	      
	      $("#table2 td").css({
	         "padding" : getElSize(20),
	         "font-size": getElSize(40),
	         "border": getElSize(5) + "px solid black"
	      });
	      
	      $("#home").css({
	         "cursor" : "pointer"
	      })
	      
	      $("#title_right").css({
	         "position" : "absolute",
	         "z-index" : 2,
	         "color" : "white",
	         "font-size" : getElSize(40),
	         "top" : getElSize(130) + marginHeight,
	         "right" : getElSize(30) + marginWidth
	      });
	      
	      $(".nav_span").css({
	         "color" : "#8D8D8D",
	         "position" : "absolute",
	         "font-size" : getElSize(45),
	         "margin-top" : getElSize(20),
	         "margin-left" : getElSize(20)
	      });
	      
	      $("#selected").css({
	         "color" : "white",
	      });
	      
	      $("span").parent("td").css({
	         "cursor" : "pointer"
	      });
	      
	      $(".title_span").css({
	         "color" : "white",
	         "font-size" : getElSize(40),
	         "background-color" : "#353535",
	         "padding" : getElSize(15)
	      });
	      
	      
	  	$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(22),
			"margin-right" : getElSize(22)
		});
		
		$("select").css({
			"width" : getElSize(1000),
			"margin-right" : getElSize(50)
		});
		
		$("input").css({
			"background-color" : "black",
			"border-color" : "black",
			"margin-right" : getElSize(50)
		})
		
			
		$("#td_first").css({
			"padding-left" : getElSize(50),
			"padding-bottom" : getElSize(10)
		});
		
		$(".k-header").css({
		    "font-family" : "NotoSansCJKkrBold",
		    "font-size" : getElSize(32) + "px",
		    "background-image" : "none",
		    "background-color" : "#353542"
		}); 
		
		$("button").css({
			"padding" : getElSize(25),
			"margin-left" : getElSize(7),
			"margin-right" : getElSize(30),
			"margin-top" : getElSize(7),
			"font-size" : getElSize(30),
			"background" : "#909090",
			"border-color" : "#222327",
			"text-align" : "center",
			"vertical-align" : "middle",
			"height" : getElSize(120)
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(200),
			"margin-right" : getElSize(80)
// 			"height" : getElSize(82)
		});
		
	      
	      $("#content_table td, #content_table2 td").css({
	         "color" : "#BFBFBF",
	         "font-size" : getElSize(50)
	      });
	      
	      $(".tmpTable, .tmpTable tr, .tmpTable td").css({
	         "border": getElSize(5) + "px solid rgb(50,50,50)"
	      });
	      
	      $(".tmpTable td").css({
	         "padding" : getElSize(10),
	         "height": getElSize(100)
	      });
	      
	      $(".contentTr").css({
	         "font-size" : getElSize(60)
	      });
	      
	      
	      $(".wrapper").css({
	         "width" : $(".menu_right").width(),
	         "margin-top" : getElSize(120),
	         "position" : "absolute"   
	      });
	      
	      $("#popup2 table td").css({
	         "color" : "white",
	         "font-size" : getElSize(60) + "px"
	      });
	      
	      $("#popup2").css({
	         "padding" : getElSize(30) + "px",
	         "left" : (originWidth/2) - ($("#popup2").width()/2),
	         "top" : (originHeight/2) - ($("#popup2").height()/2)
	      });
	      
	      $("select").css({
	         "font-size" : getElSize(40) + "px"
	      });
	      
	      $("#save, #add").css({
	         "font-size" : getElSize(50) + "px",
	         "height" : getElSize(120),
	         "margin-bottom" : getElSize(30)
	      });
	      
	      
	     $("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
			
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
				"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
	      
	      
	      
	   };
   
   
/*    function time(){
      $("#time").html(getToday());
       handle = requestAnimationFrame(time)
   };
   
 */
   function getToday2(){
      var date = new Date();
      var year = date.getFullYear();
      var month = addZero(String(date.getMonth()+1));
      var day = addZero(String(date.getDate()));
      
      return year + "-" + month + "-" + day   
   };
   
   </script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
  
   <div id="container">
      <table id="table" style="border-collapse: collapse;">
         
         
         <Tr>
            <td rowspan="10" id="svg_td" style="vertical-align: top; position:relative; background:#26282c;">
               <div id="popup2">
                  <center>
                     <table style="width: 95%">
                        <tr>
                           <td>
                           		<spring:message code="prdct_machine_line"></spring:message>
                           </td>
                           <td>
                           		<select id="prdNo"></select>
                           </td>
                        
                           <%-- <td><spring:message code="operation"></spring:message></td>
                           <td><select id="oprNo"></select></td>
                           <td><spring:message code="device"></spring:message></td>
                           <td><select id="dvcId"></select></td> --%>
                           <td>
                           		<button id="add" onclick="addRow()"><spring:message code="add"></spring:message></button>
                              	<button id="save" onclick="saveRow()"><spring:message code="save"></spring:message></button>
                           </td>
                        </tr>
                     </table>
                     
                     <div id="grid"></div>
                     <%-- <button><spring:message code="cancel"></spring:message></button> --%>
                  </center>
               </div>
            </td>
         </Tr>
        
      </table>
    </div>
    
    <div id="intro_back"></div>
	<span id="intro"></span>
   
</body>
</html>   