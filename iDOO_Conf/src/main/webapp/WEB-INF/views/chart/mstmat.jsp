<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>

<link rel="stylesheet" href="${ctxPath }/resources/styles/kendo.common.min.css" />
<link rel="stylesheet" href="${ctxPath }/resources/styles/kendo.default.min.css" />
<link rel="stylesheet" href="${ctxPath }/resources/styles/kendo.default.mobile.min.css" />
<script src="${ctxPath }/resources/js/jquery.min.js"></script> 
<script src="${ctxPath }/resources/js/kendo.all.min.js"></script>
<script src="${ctxPath }/js/kendo.all.min.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/loading.min.css" />
<script src="${ctxPath }/js/jquery.loading.min.js"></script>


<style>
*{
	margin: 0px;
	padding: 0px;
}

body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden; 
	background-color: black;
}
html{
	overflow : hidden;
}

#hiddendata{
	visibility: hidden;
}	

#grid{
	border-color : #222327;
}

.k-grid-header {
	border-color : #222327;
}

.k-grid-header-wrap {
	border-color : #222327;
}

#content_table{
	border-color : #222327;
}

</style> 
<script type="text/javascript">

	const loadPage = () =>{
		createMenuTree("config", "mstmat")	
	}
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};
	
	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		$(".date").val(year + "-" + month + "-" + day);
	};
	
/* 	function caldate(day){
		 var caledmonth, caledday, caledYear;
		 var loadDt = new Date();
		 var v = new Date(Date.parse(loadDt) - day*1000*60*60*24);
		 
		 caledYear = v.getFullYear();
		 
		 if( v.getMonth() < 9 ){
		  caledmonth = '0'+(v.getMonth()+1);
		 }else{
		  caledmonth = v.getMonth()+1;
		 }
		 if( v.getDate() < 9 ){
		  caledday = '0'+v.getDate();
		 }else{
		  caledday = v.getDate();
		 }
		 return caledYear + "-" + caledmonth+ '-' + caledday;
		}
 */		
	
//	var handle = 0;
	var className = "";
	var classFlag = true;
	
	$(function(){
		setDate();
//		createNav("mainten_nav", 4);
//		setEl();
		setEl2();
		time();
		getData();
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
//	var beforelist=[];

	$( document ).ajaxStart(function() {
		//$.showLoading(); 
	    //마우스 커서를 로딩 중 커서로 변경
//	    $("#hiddendata").show();
	    $('html').css("cursor", "wait");
//	    $.showLoading();  
	});
	//AJAX 통신 종료
	$( document ).ajaxStop(function() {
	    //마우스 커서를 원래대로 돌린다
//	    $("#hiddendata").hidden();
	    $('html').css("cursor", "auto"); 	    
	    //$.hideLoading();   
	})

	var jsonlist=[];
	function getData(){
		jsonlist=[];
		var url = ctxPath + "/chart/getmstmat.do";
		var param = "shopId=" + shopId;
		var list=[];
		var sel=new Object();
		var sel1=new Object();
		sel.a='P';
		sel1.a='R';
		var selectlist=[];
		selectlist.push(sel);
		selectlist.push(sel1);
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				jsonlist=json;
				var table = "<tbody>";
				$(json).each(function(idx, data){
					if(classFlag){
						className = "row2"
					}else{
						className = "row1"
					};
					classFlag = !classFlag;
					
					var jig = decodeURIComponent(data.JIG);
					var isAuto = decodeURIComponent(data.isAuto);
					
					if(chkLang!="ko"){
						jig = "";
						isAuto = "";
					};

					var arr=new Object();
					
					arr.a=data.MATNO;
					arr.b=data.MANDT;
					arr.c=data.PRCE;
					arr.d=data.ITVDV;
					arr.e=data.SPLNM;
					arr.f=data.PLNT;
					arr.g=data.RWMATNO;
					arr.h=data.SPEC;
					arr.i=decode(data.MATNM);
					arr.j=data.VNDNM;
					arr.k=data.SEQ;
					arr.l=data.UNIT;
					arr.before=data.MATNO;
					arr.idx=idx;
					arr.ITEMNO=data.ITEMNO;
					list.push(arr);					
//					beforelist.push(arr);
				});//json
				
				var dataSource = new kendo.data.DataSource({
//                  pageSize: 20,
                  data: list,
                  autoSync: true,
                  schema: {
                      model: {
                        id: "TraceManger",
                        fields: {
                           a: { editable: true, nullable: true },
                           b: { editable: true, nullable: true },
                           c: { editable: true, nullable: true },
//                         d: { type: "number", validation: { required: true, min: 1} },
                           d: { editable: true, nullable: true },
                           e: { editable: true, nullable: true },
                           f: { editable: true, nullable: true },
                           g: { editable: true, nullable: true },
                           h: { editable: true, nullable: true },
                           i: { editable: true, nullable: true },
                           j: { editable: true, nullable: true },
                           k: { editable: true, nullable: true },
                           l: { editable: false, nullable: true },
                           z: { editable: false, nullable: true }
                        }
                      }
                  }
               });
	
				$("#content_table").kendoGrid({
					dataSource:dataSource,
					height:getElSize(1630),
					editable:true,
//					toolbar:[{name : "Create",template:'#=create()#'},{name : "save",template:'#=save()#'}],
					columns:[{
						field:"a",title:"${material_Number}"
						,width:getElSize(280)
						,attributes: {
	            				style: "text-align: center; font-size:" + getElSize(39)
	            			},headerAttributes: {
	            				style: "text-align: center; background-color:black; font-size:" + getElSize(47)
	            			}							
					}/* ,{
						field:"b",title:"회사구분",width:getElSize(170),attributes: {
	            				style: "text-align: center; background-color:black; font-size:" + getElSize(45)
	            			},headerAttributes: {
	            				style: "text-align: center; background-color:black; font-size:" + getElSize(47)
	            			}							
					} */,{
						field:"c",title:"${price_one}"
						,width:getElSize(200)
						,attributes: {
	            				style: "text-align: center; font-size:" + getElSize(39)
	            			},headerAttributes: {
	            				style: "text-align: center; background-color:black; font-size:" + getElSize(47)
	            			}							
					},{
						field:"d",title:"${item_class}"
						,width:getElSize(200)
						,editor: lDropDownEditor
						, template: "#=d#"
						,attributes: {
	            				style: "text-align: center; font-size:" + getElSize(39)
	            			},headerAttributes: {
	            				style: "text-align: center; background-color:black; font-size:" + getElSize(47)
	            			}							
					}/* ,{
						field:"e",title:"납품대상업체",width:getElSize(220),attributes: {
	            				style: "text-align: center; background-color:black; font-size:" + getElSize(45)
	            			},headerAttributes: {
	            				style: "text-align: center; background-color:black; font-size:" + getElSize(47)
	            			}							
					},{
						field:"f",title:"공장구분",width:getElSize(170),attributes: {
	            				style: "text-align: center; background-color:black; font-size:" + getElSize(45)
	            			},headerAttributes: {
	            				style: "text-align: center; background-color:black; font-size:" + getElSize(47)
	            			}							
					} */,{
						field:"g",title:"${Original_material_number}"
						,width:getElSize(280)
						,attributes: {
	            				style: "text-align: center; font-size:" + getElSize(39)
	            			},headerAttributes: {
	            				style: "text-align: center; background-color:black; font-size:" + getElSize(47)
	            			}							
					},{
						field:"ITEMNO",title:"${prd_no}"
						,width:getElSize(230)
						,attributes: {
            				style: "text-align: center; font-size:" + getElSize(39)
            			},headerAttributes: {
            				style: "text-align: center; background-color:black; font-size:" + getElSize(47)
            			}		
					},{
						field:"h",title:"${spec}"
						,width:getElSize(120)
						,attributes: {
	            				style: "text-align: center; font-size:" + getElSize(39)
	            			},headerAttributes: {
	            				style: "text-align: center; background-color:black; font-size:" + getElSize(47)
	            			}							
					},{
						field:"i",title:"${Materialname}"
						,width:getElSize(300)
						,attributes: {
	            				style: "text-align: center; font-size:" + getElSize(39)
	            			},headerAttributes: {
	            				style: "text-align: center; background-color:black; font-size:" + getElSize(47)
	            			}							
					}/* ,{
						field:"j",title:"소재업체",width:getElSize(190),attributes: {
	            				style: "text-align: center; background-color:black; font-size:" + getElSize(45)
	            			},headerAttributes: {
	            				style: "text-align: center; background-color:black; font-size:" + getElSize(47)
	            			}							
					},{
						field:"k",title:"seq",width:getElSize(150),attributes: {
	            				style: "text-align: center; background-color:black; font-size:" + getElSize(45) 
	            			},headerAttributes: {
	            				style: "text-align: center; background-color:black; font-size:" + getElSize(47)
	            			}							
					} */,{
						field:"l",title:$("#unit").val()
						,width:getElSize(120)
						,attributes: {
	            				style: "text-align: center; font-size:" + getElSize(39) 
	            			},headerAttributes: {
	            				style: "text-align: center; background-color:black; font-size:" + getElSize(47)
	            			}							
					},{
						field:"z",title:$("#del").val()
						,width:getElSize(120)
						,template:" <input type='button' id='#=before##=idx#' class='btnbtn' value='삭제' onclick=test1('#=before#','#=before##=idx#') >"
						,headerAttributes: {
            				style: "text-align: center; background-color:black; font-size:" + getElSize(47)
            			},attributes: {
            				style: "text-align: center; font-size:" + getElSize(39) 
            			}
					}],
					dataBound: function (e) {
						console.log('dd')
						$('.btnbtn').css('font-size',getElSize(38))
						$('.btnbtn').css('border-radius',getElSize(8))
						$('.btnbtn').css('padding',getElSize(5))
						$('.btnbtn').css('padding-left',getElSize(10))
						$('.btnbtn').css('padding-right',getElSize(10))
						
						
						/* $(".del").click(function (event) {
				            event.preventDefault();
				            if (confirm("Do you want to delete this?")) {
				                //If you un-comment the line below, 
				                //   the row disapears form the table but remaims in the data source
				               // $(this).closest("tr").hide("slow"); 
				                
				                //This is KendoUI sugestion
				                var grid = $("#content_table").data("kendoGrid");
				                var dataItem = grid.dataItem($(this).closest("tr")); // retrieve the data record for the row
				                var row = $(this).closest("tr");
				                var curRowIdx = $("tr", grid.tbody).index(row);
//				                alert("CUR :"+curRowIdx);
				                tdelete(dataItem.a,this,curRowIdx);
				                grid.dataSource.remove(dataItem);
				                
				                $('.k-grid-content table tbody').css({
				        			"color":"white",
				        			"background-color":"black"
					        		});
					        		$('.k-grid-header-wrap table thead tr th').css({
					        			"color":"white",
					        			"background-color":"black"					
					        		})
					        		$('.k-grid-header-wrap table thead tr th a').css({
					        			"color":"white",
					        			"background-color":"black"										
					        		})
					        		$('.k-alt').css("background-color","#5D5D5D");
					        		$('.k-header').css("background-color","black");
					        		$('.k-button').css("background-color","#5D5D5D");

				            }
				            return false;
				        }); */
					}

				});//kendo
				
				/* grid = $("#content_table").data("kendoGrid");
				//grid.table.on("click", ".checkbox" , selectRow);
				$('.k-grid-content .k-button').click(function(){
					alert('No bind');
				}) */
				
				function lDropDownEditor(container, options) {
                    $('<input required name="' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            autoBind: false,
                            dataTextField: "a",
                            dataValueField: "a",
//                            dataSource: selectlist
							dataSource: selectlist
                        });
                }
				
				$('.k-grid-content table tbody').css({
					"color":"white",
					"background-color":"black"
				});
				/* $('.k-grid-header-wrap table thead tr th').css({
					"color":"white",
					"background-color":"black"					
				})
				$('.k-grid-header-wrap table thead tr th a').css({
					"color":"white",
					"background-color":"black"										
				}) */
				$('.k-alt').css("background-color","#D5D5D5");
				$(".k-header").css({
				    "font-family" : "NotoSansCJKkrBold",
				    "font-size" : getElSize(44) + "px",
				    "background-image" : "none",
				    "background-color" : "#353542"
				});
				$('.k-button').css("background-color","#D5D5D5");
				$('.k-edit-cell').css("background-color","red");
				

				$(".k-link").css({
					"color" : "white"
				})
				
				//저장 클릭시
/* 				$(".del").click(function(){
					var grid = $("content_table").data("kendoGrid");
				   	console.log(grid);
			 		grid.removeRow("tr:eq(115)");
					
				})//click
 */ 				
				/* $('.del').click(function(){
					$(this).parent().parent().remove(); 	
				}) */
				
				
				
				
				

			}//success
			, error : function(e1,e2,e3){
				console.log(e1,e2,e3)
			}
		});
	};
	var idx=0;
	function test1(delinfo,delId){
		var grid = $("#content_table").data("kendoGrid");		
		var ssss=grid.dataSource.data();
		for(i=0; i<jsonlist.length;i++){
			if(jsonlist[i].MATNO==delinfo){
				idx=i;
			}
		}
		dataItem=ssss[idx];
		if(delinfo=='before'){
			delId=delId.substring(6,delId.length);
			 for(i=0; i<ssss.length;i++){
				if(ssss[i].idx==delId){
					idx=i;
				    grid.dataSource.remove(ssss[i]);
				}
			}
			console.log(idx);
		}else{
			tdelete(delinfo,idx,dataItem);
		}
	}
	
	function tdelete(matno,idx,dataItem){
		var url=ctxPath + "/chart/mstmatDelete.do";
		var param="delname="+matno;
        if (confirm("정말 삭제하시겠습니까")) {

			$.ajax({
				url:url,
				data:param,
				type:"post",
				dataType:"text",
				success: function(data){
					console.log(jsonlist.splice(idx,1));
					var grid = $("#content_table").data("kendoGrid");		
					grid.dataSource.remove(dataItem);
				}
			})		//ajax
        }return false;
	}
	
	function create(e){
		return '<a class="k-button" href="#" id="toolbar-add_user" onclick="t_create()">Create</a>';
	}
	var createidx=0;
	 function t_create(){
		var grid = $("#content_table").data("kendoGrid");
		var ssss=grid.dataSource.data();
		var a;
		$(ssss).each(function(idx, data){
			a=data.a    //filed f 에 c값 넣기
		});
		
		var length=Number(a)+4;
		checkedIds = {}
		  grid.dataSource.add({
			"b" : "100"
			,"c" : "4000"
			,"d" : "-"
			,"e" : ""
			,"f" : "1000"
			,"g" : ""
			,"h" : "SPEC"
			,"i" : ""
			,"j" : " "
			,"k" : ""
			,"l" : "EA",
			"before" : "before",
			"ITEMNO" : "",
			"idx" : "before"+createidx++
		});  
		var cl=ssss.length-1;
		$('.k-grid-content table tbody tr:eq('+cl+') td:eq(0)').trigger('click');
		
		$(".k-button").css({
			"padding" : getElSize(10)
		})
		
		$(".k-alt").css("background","#D5D5D5");

		$("#grid td").on("keyup", function (e) {
			        //if current key is Enter
			        var row = $(this).closest("tr");
			        grid = $("#content_table").data("kendoGrid"),
			        dataItem = grid.dataItem(row);
		})	
		$('.k-grid-content table tbody').css({
			"color":"white",
			"background-color":"black"
		});
		$('.k-grid-header-wrap table thead tr th').css({
			"color":"white",
			"background-color":"black"					
		})
		$('.k-grid-header-wrap table thead tr th a').css({
			"color":"white",
			"background-color":"black"										
		})
		$('.k-alt').css("background-color","#D5D5D5");
		$('.k-header').css("background-color","#353542");
		
		$('.k-button').css("background-color","#D5D5D5");
	} 
	function save(e){
		return '<a class="k-button" href="#" id="toolbar-add_user" onclick="t_save()">save</a>';
	}
	
	var ischk=false;
	function t_save(){
		$("#hiddensave").attr("disabled",true)
		grid = $("#content_table").data("kendoGrid");
		var ssss=grid.dataSource.data();
		var valueArray=[];
		var updatelist=[];
		var update = new Object();

		var obj1 = new Object();
		obj1.val = jsonlist;

		$(ssss).each(function(idx, data){
		    var obj = new Object();
			obj.MATNO= data.a;
			obj.MANDT= data.b;
			obj.PRCE= data.c;
			obj.ITVDV= data.d;
			obj.SPLNM= data.e;
			obj.PLNT= data.f;
			obj.RWMATNO= data.g;
			obj.SPEC=data.h;
			obj.MATNM=data.i;
			obj.VNDNM=data.j;
			obj.SEQ=data.k;
			obj.UNIT=data.l;
			obj.ITEMNO=data.ITEMNO;
			if(idx<jsonlist.length){
			obj.before=jsonlist[idx].MATNO;
			}else{
				obj.before=data.a;
			}
			valueArray.push(obj);
		});//json	
		
		//update
		var obj = new Object();
		obj.val = valueArray;
		//수정한거만 들고가기	
		for(i=0;i<jsonlist.length;i++){
			var str="";
			var str1="";
			str=String(obj1.val[i].MATNO);
			str+=String(obj1.val[i].MANDT);
			str+=String(obj1.val[i].PRCE);
			str+=String(obj1.val[i].ITVDV);
			str+=String(obj1.val[i].SPLNM);
			str+=String(obj1.val[i].PLNT);
			str+=String(obj1.val[i].RWMATNO);
			str+=String(obj1.val[i].SPEC);
			str+=String(decode(obj1.val[i].MATNM));
			str+=String(obj1.val[i].VNDNM);
			str+=String(obj1.val[i].SEQ);
			str+=String(obj1.val[i].UNIT);
			str+=String(obj1.val[i].ITEMNO);
			if(obj.val[i]==undefined){
				update.val=updatelist;
			}
			str1=String(obj.val[i].MATNO);//바뀐데이
			str1+=String(obj.val[i].MANDT);
			str1+=String(obj.val[i].PRCE);
			str1+=String(obj.val[i].ITVDV);
			str1+=String(obj.val[i].SPLNM);
			str1+=String(obj.val[i].PLNT);
			str1+=String(obj.val[i].RWMATNO);
			str1+=String(obj.val[i].SPEC);
			str1+=String(obj.val[i].MATNM);
			str1+=String(obj.val[i].VNDNM);
			str1+=String(obj.val[i].SEQ);
			str1+=String(obj.val[i].UNIT);
			str1+=String(obj.val[i].ITEMNO);

			//수정된거 찾기
			if(str!=str1){
				updatelist.push(obj.val[i]);
			}
		}
		console.log("---update----")
		console.log(updatelist);
		for(i=jsonlist.length;i<valueArray.length;i++){
			console.log("---obj---")
			console.log(obj.val[i]);
			updatelist.push(obj.val[i]);
		}
		//조건걸기
		
		
		for(i=0;i<updatelist.length;i++){
			if(updatelist[i].ITVDV.length>1){
				alert("품목구분 한글자 입력하세요");
				$("#hiddensave").attr("disabled",false)
				return;
			}else if(updatelist[i].MATNO==null){
				alert('자재번호 입력하세요');
				$("#hiddensave").attr("disabled",false)
				return;
			}else if(updatelist[i].ITVDV=='-'){
				alert("품목구분 입력하세요");
				$("#hiddensave").attr("disabled",false)
				return;
			}
			 for(j=0;j<valueArray.length;j++){
				
				 if(j>=jsonlist.length){
					 for(k=0;k<jsonlist.length;k++){
						if(updatelist[i].MATNO==jsonlist[k].MATNO){
							alert(updatelist[i].MATNO+" ,중복 또는 기존에 있던 데이터인지 확인하세요");
							$("#hiddensave").attr("disabled",false)
						 	return;
						}
				 	}
				}else{
					//updatelist새로업데이트되는 기본키와 원래 있던 기본키가 같으면
					if(updatelist[i].MATNO==valueArray[j].before){
						if(updatelist[i].MATNO==updatelist[i].before){
//							alert('Zzz')
						}else{//두개의 기본키가 다른경우에는
							for(l=0; l<updatelist.length;l++){
								if(updatelist[i].MATNO==updatelist[l].before){
									alert(updatelist[i].MATNO+" ,중복 또는 기존에 있던 데이터인지 확인하세요")
									$("#hiddensave").attr("disabled",false)
									return;
								}
							}
							for(l=0; l<updatelist.length;l++){
								if(updatelist[i].MATNO!=updatelist[l].before){
									alert(updatelist[i].MATNO+" ,중복 또는 기존에 있던 데이터인지 확인하세요");
									$("#hiddensave").attr("disabled",false)
									return;
								}
							}
							}
					}
				} 
			}  
		}
		var w;
//		w.document.write( "처리중입니다","" ,"width=400, height=300" );
		update.val=updatelist;
//		console.log(JSON.stringify(obj));
		var url =ctxPath + "/chart/mstmatUpdate.do";
		var param = "val="+JSON.stringify(update)+
					"&length="+valueArray.length;
		$.ajax({
			url:url,
			data:param,
			type:"post",
			dataType:"text",
			beforeSend:function(data){
//				w = window.open( "${ctxPath }/images/loadingDong.gif","","width=100,heigth=100");				
			},
			success: function(data){
//				w.close(); // 페이지 로딩 끝.
				if(data=="success"){
//					w.close(); // 페이지 로딩 끝.
					alert("${save_ok}");
					$("#hiddensave").attr("disabled",false)
//				    $.hideLoading();   
				}else if(updatelist.length==0){
//					w.close(); // 페이지 로딩 끝
					alert("변경된 사항이 없습니다");
					$("#hiddensave").attr("disabled",false)
//				    $.hideLoading();
					return;
				}else{
					alert("저장에 실패하였습니다.");
					$("#hiddensave").attr("disabled",false)
				}
				getData();
			}
		})		//ajax	
		$('.k-grid-content table tbody').css({
			"color":"white",
			"background-color":"black"
		});
		$('.k-grid-header-wrap table thead tr th').css({
			"color":"white",
			"background-color":"black"					
		})
		$('.k-grid-header-wrap table thead tr th a').css({
			"color":"white",
			"background-color":"black"										
		})
		$('.k-alt').css("background-color","#D5D5D5");
		$('.k-header').css("background-color","black");
		$('.k-button').css("background-color","#D5D5D5");
	}
	
	function delDeliverHistory(a){
		alert(a);
	}
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	
/* 	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	}; */
	
/* 	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#bannerDiv").css({
			"position" : "absolute",
			"width" : getElSize(1500),
			"display" : "none",
			"height" : getElSize(200),
			"border-radius" : getElSize(20),
			"padding" : getElSize(50),
			"background-color" : "lightgray",
			"z-index" : -9
		});

	
		$("#bannerDiv").css({
			"left" : (originWidth/2) - ($("#bannerDiv").width()/2),
			"top" : (originHeight/2) - ($("#bannerDiv").height()/2)
		});
		
		$("#bannerDiv input[type='text']").css({
			"width" : getElSize(1200),
			"font-size" : getElSize(50)
		});
		
		$("#bannerDiv button").css({
			"margin" : getElSize(50),
			"font-size" : getElSize(50)
		});
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(50),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(80),
		});
		
		$("#content_table td").css({
			"color" : "##BFBFBF",
			"font-size" : getElSize(40)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#table2 td").css({
			"text-align" : "center",
			"color" : "white",
			"font-size" : getElSize(35),
			"padding" : getElSize(20),
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
	}; */
	
	
	function setEl2(){
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$("#container").css({
			"width" : contentWidth - getElSize(30)
		})
			
		$("#content_table").css({
			"margin-top" : getElSize(5)
		})
		
		$("#content_table").css({
			"margin-left" : getElSize(50),
			"margin-right" : getElSize(50)
		})
			
			
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(110) + marginHeight,
			"margin-top" : getElSize(270)
		});

		
		$("#bannerDiv").css({
			"position" : "absolute",
			"width" : getElSize(1500),
			"display" : "none",
			"height" : getElSize(200),
			"border-radius" : getElSize(20),
			"padding" : getElSize(50),
			"background-color" : "lightgray",
			"z-index" : -9
		});

	
		$("#bannerDiv").css({
			"left" : (originWidth/2) - ($("#bannerDiv").width()/2),
			"top" : (originHeight/2) - ($("#bannerDiv").height()/2)
		});
		
		$("#bannerDiv input[type='text']").css({
			"width" : getElSize(1200),
			"font-size" : getElSize(50)
		});
		
		$("#bannerDiv button").css({
			"margin" : getElSize(50),
			"font-size" : getElSize(50)
		});
		
		
/* 		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		}); */
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
	/* 	$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		}); */
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(22),
			"margin-right" : getElSize(22)
		});
	
			
		$("#td_first").css({
			"padding-left" : getElSize(50),
			"padding-bottom" : getElSize(10)
		});
		
		$(".k-header").css({
		    "font-family" : "NotoSansCJKkrBold",
		    "font-size" : getElSize(32) + "px",
		    "background-image" : "none",
		    "background-color" : "#353542"
		}); 
		
		$("input").css({
			"padding" : getElSize(25),
			"height" : getElSize(110),
			"margin-left" : getElSize(7),
			"margin-right" : getElSize(50),
			"margin-top" : getElSize(10),
			"font-size" : getElSize(35),
			"background" : "#909090",
			"border-color" : "#222327",
			"vertical-align" : "middle",
			"text-align" : "center"
		})
		
		$("#content_table td").css({
			"color" : "##BFBFBF",
			"font-size" : getElSize(40)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#table2 td").css({
			"text-align" : "center",
			"color" : "white",
			"font-size" : getElSize(35),
			"padding" : getElSize(20),
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
			
		$("#intro_back").css({
			"width" : contentWidth,
			"display" : "none",
			"height" : getElSize(180),
				"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		$(".btnbtn_").css({
			"margin-right" : getElSize(60),
			"width" : getElSize(230),
			"height" : getElSize(100),
			"border-radius" : getElSize(8),
			"padding-bottom" : getElSize(10),
 			"padding-top" : getElSize(10),
			"font-size" : getElSize(47)
		})
		
	};
	
	
/* 	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
 */	
	function goGraph(){
		location.href = "${ctxPath}/chart/jigGraph.do"
	};
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			
			<Tr>
				<td rowspan="10" id="svg_td" style="vertical-align: top;  top; position:relative; background:#26282c;">
					<table style="width: 100%">
					<tr>
						<td id="td_first">
							<input type="button" value='<spring:message code="add"></spring:message>' class='btnbtn_' onclick='t_create()'>
							<input type='button' class='btnbtn_' id='hiddensave' value='<spring:message code="save"></spring:message>' onclick='t_save()'>
						</td>
					</tr>
					<tr>
						<td>
							 <div id="content_table" > 
								<input type="hidden" id=del value='<spring:message code="del"></spring:message>'>
								<input type="hidden" id=unit value='<spring:message code="unit"></spring:message>'>
								<input type="hidden" id=order value='<spring:message code="order"></spring:message>'>
								<input type="hidden" id=machine_cd value='<spring:message code="machine_cd"></spring:message>'>
								<input type="hidden" id=machine_name value='<spring:message code="machine_name"></spring:message>'>
								<input type="hidden" id=machine_name_min value='<spring:message code="machine_name_min"></spring:message>'>
								<input type="hidden" id=wc_cd value='<spring:message code="wc_cd"></spring:message>'>
								<input type="hidden" id=jig_list value='<spring:message code="jig_list"></spring:message>'>
								<input type="hidden" id=auto_chk value='<spring:message code="auto_chk"></spring:message>'>
								<input type="hidden" id=machine_ty value='<spring:message code="machine_ty"></spring:message>'>
								<input type="hidden" id=existed_machine_cd value='<spring:message code="existed_machine_cd"></spring:message>'>
								<input type="hidden" id="NC" value="NC">
								<input type="hidden" id=program value='<spring:message code="program"></spring:message>'>
								<input type="hidden" id=m_cd value='<spring:message code="m_cd"></spring:message>'>
								<input type="hidden" id=daily_prd_target value='<spring:message code="daily_prd_target"></spring:message>'>
								<input type="hidden" id=dauly_ophour_target value='<spring:message code="dauly_ophour_target"></spring:message>'>
								<input type="hidden" id=bigo value='<spring:message code="bigo"></spring:message>'>
							</div>  
						</td>
					</Tr>

    
				</table>
	 </div>
	 </table>
	 
	 
	<div id="intro_back"></div>
	<span id="intro"></span>
	 
</body>
</html>