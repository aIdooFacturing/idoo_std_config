<!-- 
최소한의 화면으로 구성된
시작화면 Template
 -->
 <%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden; 
	background-color: black;
  	font-family:'Helvetica';
}

.k-grid-header-wrap.k-auto-scrollable table thead tr th{
	text-align: center;
	vertical-align: middle;
}

.k-header.k-grid-toolbar {
	background: linear-gradient( to top, black, gray);
	color: white;
	text-align: center;
}
.k-grid-footer-wrap tbody tr td{
	color : black;
}

#grid{
	border-color : #222327;
}

#grid .k-grid-header {
	border-color : #222327;
}

.k-grid-header-wrap {
	border-color : #222327;
}

</style> 
<script type="text/javascript">

	const loadPage = () =>{
		createMenuTree("im", "stockStatus2")	
	}
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};
	
	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	///****	
	//	전역변수 선언
	//****/

	//	선택할 품번 리스트를 담기위한 변수
	var prdNos=[];
	
	
	$(function(){
//		createNav("inven_nav", 8);
		
		//품번 조회하기
		getPrdNo();
		
		//grid table 그리기
		gridTable();

//		setEl();
//		time();

		setEl2();
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
		
	});
	
/* 	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
 		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
 	
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		$("#grid1").css("margin-top",getElSize(25))

		$("#grid2").css("margin-top",getElSize(25))
		
		$("#subMenu").css({
			"position" : "absolute"
			,"top" : getElSize(155)
			,"left" : getElSize(575)
		})
		
		$(".k-icon.k-i-reload").css({
			"color":"white"
			,"background" : "#363636"
			,"cursor":"pointer"
			,"margin":0
			,"margin-left" : getElSize(40)
			,"width" : getElSize(110)
			,"height":getElSize(90)
		})
	}; */
	
	
	function setEl2(){
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
/* 		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
 */		
 	$("#container").css({
		"width" : contentWidth - getElSize(30)
	})
		
	$("#content_table").css({
		"margin-top" : getElSize(700)
	})
		
		
	$("#table").css({
		"position" : "absolute",
		"width" : $("#container").width(),
		"top" : getElSize(100) + marginHeight,
		"margin-top" : getElSize(250)
	});
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
/* 		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		}); */
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		$("#grid1").css("margin-top",getElSize(25))

		$("#grid2").css("margin-top",getElSize(25))
		
		$("#subMenu").css({
			"position" : "absolute"
			,"top" : getElSize(155)
			,"left" : getElSize(575)
		})
		
		$(".k-icon.k-i-reload").css({
			"color":"white"
			,"background" : "#363636"
			,"cursor":"pointer"
			,"margin":0
			,"margin-left" : getElSize(40)
			,"width" : getElSize(110)
			,"height":getElSize(90)
		})
	};
	
	
/* 	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
 */	
	function getPrdNo(){
		
		var url = "${ctxPath}/chart/getMatInfo.do";
		var param = "shopId=" + shopId;
		
		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;

//				console.log(json);
				
				$(json).each(function(idx, data){
					var prdNo = {"prdNo" : data.prdNo, "ITEMNO" : data.ITEMNO}
					prdNos.push(prdNo)
				});
				
				$("#combobox").kendoComboBox({
					dataSource : prdNos,
					autoWidth : true,
					dataTextField: "prdNo",
					dataValueField: "prdNo",
					clearButton: false,
					value: prdNos[4].prdNo,
					//품번 변경되었을시
					change: function(e){
						var value = this.value();
						
						getTable(value)
					}
				});
				
				getTable(prdNos[4].prdNo);
			}
		});
	}
	var aa;

	function gridTable(){
		kendotable = $("#grid").kendoGrid({
//			height:getElSize(1680)
			height:getElSize(665)
			,width:getElSize(2800)
			,editable:true
			,cellClose:  function(e) {
//				e.model.set("iniohdCnt",e.model.ohdCnt - e.model.rcvCnt + e.model.issCnt + e.model.notiCnt);
			}
			,dataBound:function(e){
				gridCss();
			}
			,columns:[{
					field: "prdNo"
					,title :"소재품번"
					,width: getElSize(320)
					,footerTemplate:" 합계 "
				},{
				width: getElSize(20)
				, attributes: {
					style: "text-align: center; background-color:white; color:black;"
				}, headerAttributes: {
					style: "text-align: center; background-color:white; color:black;"
				}
				},{
				title: "소재창고",
				columns: [ {
					field: "vndNm"
					, title: "업체"
					, editor: readOnly
					, width: getElSize(170)
				},{
					field: "barcode"
					, title: "barCode"
					, editor: readOnly
					, width: getElSize(150)
				},{
					field: "iniohdCnt"
					, title: "기초 재고"
					, editor: readOnly
					, width: getElSize(125)
					,footerTemplate: "#=cntFooter(data.iniohdCnt)#"
				},{
					field: "ohdCnt"
					, title: "창고 재고"
					, width: getElSize(125)
					,footerTemplate: "#=cntFooter(data.ohdCnt)#"
					, attributes: {
						class: "editable-cell" ,
						style: "text-align: center; background-color:yellow; color:black !important;"
					}
				},{
					field: "date"
					, title: "날짜"
					, editor: readOnly
					, width: getElSize(220)
				},{
					field: "bigo"
					,title: "사유"
					,width: getElSize(380)
					, attributes: {
						style: "text-align: center; background-color:white; color:black !important;"
					}
				},{
    				
    				command :[{
    					
    					name :"수정하기",
    					click : function(e){
    						updateStock(e,this);
//    						del_btn_evt(e,this,"updateStock(id)")
    					}
    				}],
    				width: getElSize(151),
/*     				attributes: {
        				style: "text-align: center; color:white; font-size:" + getElSize(35)
        			},headerAttributes: {
        				style: "text-align: center; background-color:black; color:white; font-size:" + getElSize(37)
        			} */
    			}]
			}]
		}).data("kendoGrid");
		
		//2번째 그리드
		kendotablePro = $("#grid1").kendoGrid({
//			height:getElSize(1680)
			height:getElSize(370)
			,editable:true
			,dataBound:function(e){
				//공통 CSS
				gridCss();
				//cell Merge 셀 합치기 위해
				onDataBound();
			}
			,cellClose:function(e){
				//기초재고 수정시 현재재고 업데이트 시키기 위한 로직
				e.model.set("ohdCnt",e.model.iniohdCnt + e.model.rcvCnt - e.model.issCnt - e.model.notiCnt);
				// 2번째 테이블 값
				var dataItem = $("#grid1").data("kendoGrid").dataSource.data();
				
				for(i=0,len = dataItem.length; i<len; i++){
					dataItem[i].ohdCnt = e.model.ohdCnt;
					dataItem[i].iniohdCnt = e.model.iniohdCnt;
					dataItem[i].ohdCntR = e.model.ohdCntR;
				}
				
//				$("#grid1").data("kendoGrid").dataSource.fetch();

				

			}
			,columns:[{
					field: "item"
					,title :"소재품번"
					,width: getElSize(320)
				},{
					title : "공정재고 (반출 시)"
					,columns:[{
						field: "iniohdCnt"
						,title : "기초재고"
						,width : getElSize(180)
						, attributes: {
							class: "cellMerge" ,
							style: "text-align: center; background-color:yellow; color:black !important;"
						}
					},{
						field: "ohdCnt"
						,title : "현재재고"
						, editor: readOnly
						,width : getElSize(180)
						, attributes:{
							class: "cellMerge"
						}
					}]
				},{
				width: getElSize(20)
				, attributes: {
					style: "text-align: center; background-color:white; color:black;"
				}, headerAttributes: {
					style: "text-align: center; background-color:white; color:black;"
				}
				},{
					title: "공정창고 ( 재고마감 현재고)",
					columns: [{
						field: "ohdCntR"
						,title :"R삭"
						,width: getElSize(180)
						, attributes: {
							class: "cellMerge" ,
							style: "text-align: center; background-color:rgb(255,236,18); color:black !important;"
						}
					},{
						field: "ohdCntM"
						,title : "MCT"
						,width : getElSize(180)
						, attributes: {
							class: "editable-cell" ,
							style: "text-align: center; background-color:yellow; color:black !important;"
						}
					},{
						field: "ohdCntC"
						,title : "CNC"
						,width : getElSize(180)
						, attributes: {
							class: "editable-cell" ,
							style: "text-align: center; background-color:rgb(255,236,18); color:black !important;"
						}
					},{
						field: "ohdCntP"
						,title : "도금"
						,width : getElSize(180)
						, attributes: {
							class: "editable-cell" ,
							style: "text-align: center; background-color:yellow; color:black !important;"
						}
					},{
						field: "bigo"
						,title: "사유"
						,width: getElSize(430)
						, attributes: {
							style: "text-align: center; background-color:white; color:black !important;"
						}
					},{
    				
	    				command :[{
	    					
	    					name :"수정",
	    					click : function(e){
	    						UptPro(e,this);
	    					}
	    				}],
	    				width: getElSize(151),
	    			}]
				}
			]
		}).data("kendoGrid");
		//3번째 그리드
		kendotableRt = $("#grid2").kendoGrid({
//			height:getElSize(1680)
			height:getElSize(670)
			,editable:true
			,dataBound:function(e){
				gridCss();
			}
			,cellClose:function(e){
				e.model.set("iniohdCnt",e.model.ohdCnt - e.model.rcvCnt + e.model.issCnt + e.model.notiCnt);
				$("#grid2").data("kendoGrid").dataSource.fetch()
			}
			,columns:[{
					field: "prdNo"
					,title :"소재품번"
					,footerTemplate:" 합계 "
					,width: getElSize(320)
				},{
					field: "shipLot"
					,title : "출하번호"
					,width : getElSize(180)
				},{
				width: getElSize(20)
				, attributes: {
					style: "text-align: center; background-color:white; color:black;"
				}, headerAttributes: {
					style: "text-align: center; background-color:white; color:black;"
				}
				},{
				title: "완성창고",
				columns: [{
					field: "iniohdCnt"
					, title: "기초 재고"
					, width: getElSize(151)
					, editor: readOnly
					,footerTemplate: "#=cntFooter(data.iniohdCnt)#"
					

				}, {
					field: "rcvCnt"
					, title: "입고"
					,footerTemplate: "#=cntFooter(data.rcvCnt)#"
					, width: getElSize(135)
//					, template : '<input type="text" class="k-input k-textbox" name="rcvCnt" value="#=rcvCnt#" data-bind="value:rcvCnt">'
				},{
					field: "issCnt"
					, title: "불출"
					,footerTemplate: "#=cntFooter(data.issCnt)#"
					, width: getElSize(135)
				},{
					field: "notiCnt"
					, title: "불량"
					,footerTemplate: "#=cntFooter(data.notiCnt)#"
					, width: getElSize(135)
				},{
					field: "ohdCnt"
					, title: "창고 재고"
					, width: getElSize(151)
					,footerTemplate: "#=cntFooter(data.ohdCnt)#"
					, attributes: {
						class: "editable-cell" ,
						style: "text-align: center; background-color:yellow; color:black !important;"
					}
				},{
					field: "bigo"
					,title: "사유"
					,width: getElSize(430)
					, attributes: {
						style: "text-align: center; background-color:white; color:black !important;"
					}
				},{
    				
    				command :[{
    					
    					name :"수정",
    					click : function(e){
    						UptSuccess(e,this);
    					}
    				}],
    				width: getElSize(151),
/*     				attributes: {
        				style: "text-align: center; color:white; font-size:" + getElSize(35)
        			},headerAttributes: {
        				style: "text-align: center; background-color:black; color:white; font-size:" + getElSize(37)
        			} */
    			}]
			}]
		}).data("kendoGrid");
		
	}
	
	function gridCss(){
		$(".grid thead tr th").css("font-size",getElSize(36))
		$(".grid tbody tr td").css({
			"font-size" : getElSize(36)
			,"height" : getElSize(96)
		})
	}
	
	//공정 창고 수정 버튼 클릭시
	function UptPro(e,item){
		// kendogrid uid find
		var uid = e.currentTarget.parentNode.closest("tr").dataset.uid;
		// row find
		var row = $("#grid1").data("kendoGrid")
        .tbody
        .find("tr[data-uid='" + uid + "']");
		// 선택한 행의 값
		var dataItem = $("#grid1").data("kendoGrid").dataItem(row);
		
		//변경된 값이 없을경우 리턴
		if(dataItem.biniohdCnt == dataItem.iniohdCnt && dataItem.bohdCnt == dataItem.ohdCnt && dataItem.bohdCntR == dataItem.ohdCntR && dataItem.bohdCntM == dataItem.ohdCntM && dataItem.bohdCntC == dataItem.ohdCntC && dataItem.bohdCntP == dataItem.ohdCntP){
			alert("변경된 사항이 없습니다._2");
			return;
		}
		//사유 입력 필수
		if(dataItem.bigo==undefined || dataItem.bigo==""){
			alert("사유를 입력해주세요.")
			return;
		}
		//공정재고 저장할 리스트 담기
		var savelistTotal = [];
		//공정창고 저장할 리스트 담기
		var savelistPro = [];
		//변경된 값만 가져가기  (공정재고[기초재고])
		if((dataItem.biniohdCnt != dataItem.iniohdCnt) || (dataItem.bohdCnt != dataItem.ohdCnt)){
			var arr = {};
			arr.prdNo = dataItem.prdNo;
			arr.biniohdCnt = dataItem.biniohdCnt;
			arr.iniohdCnt = dataItem.iniohdCnt;
			arr.bohdCnt = dataItem.bohdCnt;
			arr.ohdCnt = dataItem.ohdCnt;
			arr.proj = "0005";
			arr.date = dataItem.eDate;
			arr.bigo = dataItem.bigo;
			savelistTotal.push(arr);
		}
		/* //변경된 값만 가져가기  (공정재고[현재고])
		if(dataItem.bohdCnt != dataItem.ohdCnt){
			var arr = {};
			arr.prdNo = dataItem.prdNo;
			arr.bohdCnt = dataItem.bohdCnt;
			arr.ohdCnt = dataItem.ohdCnt;
			arr.proj = "0005";
			arr.date = dataItem.eDate;
			arr.bigo = dataItem.bigo;
			savelistTotal.push(arr);
		} */
		//R삭 비교후 공정창고
		if(dataItem.bohdCntR != dataItem.ohdCntR){
			var arr = {};
			arr.prdNo = dataItem.prdNo;
			arr.bohdCnt = dataItem.bohdCntR;
			arr.ohdCnt = dataItem.ohdCntR;
			arr.proj = "0010";
			arr.date = dataItem.date;
			arr.bigo = dataItem.bigo;
			savelistPro.push(arr);
		}
		//MCT 비교후 공정창고
		if(dataItem.bohdCntM != dataItem.ohdCntM){
			var arr = {};
			arr.prdNo = dataItem.item;
			arr.bohdCnt = dataItem.bohdCntM;
			arr.ohdCnt = dataItem.ohdCntM;
			arr.proj = "0020";
			arr.date = dataItem.date;
			arr.bigo = dataItem.bigo;
			savelistPro.push(arr);
		}
		//CNC 비교후 공정창고
		if(dataItem.bohdCntC != dataItem.ohdCntC){
			var arr = {};
			arr.prdNo = dataItem.item;
			arr.bohdCnt = dataItem.bohdCntC;
			arr.ohdCnt = dataItem.ohdCntC;
			arr.proj = "0030";
			arr.date = dataItem.date;
			arr.bigo = dataItem.bigo;
			savelistPro.push(arr);
		}
		//도금 비교후 공정창고
		if(dataItem.bohdCntP != dataItem.ohdCntP){
			var arr = {};
			arr.prdNo = dataItem.item;
			arr.bohdCnt = dataItem.bohdCntP;
			arr.ohdCnt = dataItem.ohdCntP;
			arr.proj = "0040";
			arr.date = dataItem.date;
			arr.bigo = dataItem.bigo;
			savelistPro.push(arr);
		}
		console.log(savelistTotal)
		console.log(savelistPro)
		
		$.showLoading()
		var url = "${ctxPath}/chart/stockdeadLineSave.do";
		
		// TOTAL 공정 창고 변수
		var obj = new Object();
		obj.val = savelistTotal;
		
		// PROCESS ( R M C 도금) 창고 변수
		var objPro = new Object();
		objPro.val = savelistPro;

		var param = "total=" + JSON.stringify(obj)	+
				 	"&pro=" + JSON.stringify(objPro);
		
		console.log(param);
		$.ajax({
			url: url,
			data: param,
			type: "post",
			success: function (data) {
				
				if(data=="success"){
					alert("저장완료 되었습니다.");
					getTable();
					$.showLoading()

				}else{
					alert("실패했습니다.")
					$.hideLoading();
				}
				
			}
	})
		
	}
	//완성 창고 수정 버튼 클릭시
	function UptSuccess(e,item){
		
		// kendogrid uid find
		var uid = e.currentTarget.parentNode.closest("tr").dataset.uid;
		// row find
		var row = $("#grid2").data("kendoGrid")
        .tbody
        .find("tr[data-uid='" + uid + "']");
		// 선택한 행의 값
		var dataItem = $("#grid2").data("kendoGrid").dataItem(row);
		
		//수정된 값이 없을 경우 리턴
		if(dataItem.biniohdCnt==dataItem.iniohdCnt && dataItem.bohdCnt==dataItem.ohdCnt){
			alert("변경된 사항이 없습니다._3");
			return;
		}
		//사유 입력 필수
		if(dataItem.bigo==undefined || dataItem.bigo==""){
			alert("사유를 입력해주세요.")
			return;
		}
		$.showLoading();
		var url = "${ctxPath}/chart/stockSuccessInohdCntSave.do";
		var param = "prdNo=" + dataItem.prdNo +
					"&date=" + dataItem.date +
					"&vndNo=" + dataItem.vndNo +
					"&shipLot=" + dataItem.shipLot +
					"&proj=" + dataItem.proj +
					"&iniohdCnt=" + dataItem.iniohdCnt +
					"&biniohdCnt=" + dataItem.biniohdCnt +
					"&bohdCnt=" + dataItem.bohdCnt +
					"&bigo=" + dataItem.bigo +
					"&ohdCnt=" + dataItem.ohdCnt ;
		
		console.log(param);
		
		$.ajax({
			url: url,
			data: param,
			type: "post",
			success: function (data) {
				
				if(data=="success"){
					$.hideLoading();
					getTable();
					alert("저장완료 되었습니다.");
				}else{
					$.hideLoading();
					alert("저장 실패하였습니다.");
				}
			}
		})
		//
		
		
		
	}
	//소재 재고 수정하기 버튼 클릭시
	function updateStock(e,item){
		var dataItem = item._data[0];
		console.log(dataItem)
		
		// 재고가 변경되었는지 확인하고 변경된것이 없다면 리턴
		if(dataItem.biniohdCnt == dataItem.iniohdCnt && dataItem.bohdCnt == dataItem.ohdCnt){
			alert("변경된 사항이 없습니다._1");
			return;
		}
		// 사유입력은 필수
		if(dataItem.bigo==undefined || dataItem.bigo==""){
			alert("사유를 입력해주세요.");
			return;
		}
		$.showLoading();
		var url = "${ctxPath}/chart/stockInohdCntSave.do";

		var param = "prdNo=" + dataItem.prdNo +
					"&date=" + dataItem.date +
					"&idx=" + dataItem.idx +
					"&barcode=" + dataItem.barcode +
					"&proj=" + "0000" +
					"&iniohdCnt=" + dataItem.iniohdCnt +
					"&biniohdCnt=" + dataItem.biniohdCnt +
					"&bohdCnt=" + dataItem.bohdCnt +
					"&bigo=" + dataItem.bigo +
					"&ohdCnt=" + dataItem.ohdCnt ;
		
		console.log(param);
		
		$.ajax({
			url: url,
			data: param,
			type: "post",
			success: function (data) {
				
				if(data=="success"){
					$.hideLoading();
					getTable();
					alert("저장완료 되었습니다.");
				}else{
					$.hideLoading();
					alert("저장 실패하였습니다.");
				}
			}
		})
		
	}
	
	function readOnly(container, options){
		container.removeClass("k-edit-cell");
		container.text(options.model.get(options.field));
	}
	
	function numberWithCommas(x) {
		if(x==undefined){
			return 0
		}
	    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
	
	function cntFooter(sum){
		if(sum!=undefined){
			var prceSum=numberWithCommas(sum.sum)
			return prceSum 
		}else{
			return "0"
		}
	}
	var mergeChk=[];
	var mergeList = [];
	
	// cell 합치기 위해 class 추가
	function onDataBound(){
		/* $("#wrapper").data("kendoGrid").dataSource.getByUid("48ed3c39-7cba-4412-8387-5edbb66a186e")
		
		$(".cellMerge")[0].closest("tr") */
		
		mergeChk=[]; mergeList=[];
		var grid = $("#grid1").data("kendoGrid").dataSource

		$(".cellMerge").each(function(idx,data){
			// tr값 가져오기
			var uid = $(".cellMerge")[idx].closest("tr").dataset.uid
			var RowItem = 	grid.getByUid(uid)
			
			
			
			/* var dataItem = $(container).closest("tr")[0].dataset.uid;
			var grid = $("#wrapper").data("kendoGrid");
			var row = $("#wrapper").data("kendoGrid").tbody.find("tr[data-uid='" + dataItem + "']");
			var idx = grid.dataSource.indexOf(grid.dataItem(row));
			 */
			
			
	//		var row = $("#wrapper").data("kendoGrid").tbody.find("tr[data-uid='" + uid + "']");
			 
			var row = $(this).closest("tr");
		    var rowIdx = $("tr", $("#grid1").data("kendoGrid").tbody).index($(".cellMerge")[idx].closest("tr"));

			if(mergeChk.indexOf(RowItem.prdNo)==-1){
				mergeChk.push(rowIdx)
			}
			
			if(mergeChk.indexOf(RowItem.prdNo)==-1 && mergeChk.indexOf(rowIdx)!=-1){
				var count=0;
				// rowspan 몇으로 할지 정하기 위해서
				
				for(i=0, len=grid.data().length; i<len; i++){
//					console.log(data.prdNo + " ,, " + grid.data()[i].prdNo)
					if(RowItem.prdNo==grid.data()[i].prdNo){
						count ++;
					}
				}
				
				var arr={};
					arr.prdNo = RowItem.prdNo;
					arr.count = count;
				
				mergeChk.push(RowItem.prdNo);
				mergeList.push(arr);
				
			}else{
				if(mergeChk.indexOf(rowIdx)==-1)
				$(".cellMerge:eq(" + idx + ")").addClass("hidden");
//				$(".cellMerge:eq(" + idx + ")").attr("class","hidden")
			}
			
		})
		
		$(".cellMerge.hidden").attr("class","hidden");

 		$(".hidden").css("display","none");
		
		$(".cellMerge").each(function(idx,data){
			var uid = $(".cellMerge")[idx].closest("tr").dataset.uid
			var RowItem = 	grid.getByUid(uid)
			
			for(i=0, len=mergeList.length; i<len; i++){
//				console.log(data.prdNo + " ,, " + grid.data()[i].prdNo)
				if(RowItem.prdNo==mergeList[i].prdNo){
					$(".cellMerge:eq(" + idx + ")").attr("rowspan",mergeList[i].count);
				}
			}
		})
		
	}
	function getTable(){
		$.showLoading(); 

		var url = "${ctxPath}/chart/getUpdateOhdCnt.do";
		
		var param = "deliveryNo=" + $("#deliveryNo").val()+
 					"&sDate=" + moment().subtract(6,"month").format("YYYY-MM-DD") +
					"&prdNo=" + $("#combobox").val() ;  
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				console.log("완성")
				console.log(data)
				//소재 데이터
				var jsonRW = data.dataList;
				//공정창고 데이터
				var jsonPro = data.dataListPro;
				
				// 완성창고 데이터
				var jsonResult = data.dataListRt;
				
				
				//수정전 데이터를 관리하기 위해서 반복문 돌려서 관리;
				$(jsonRW).each(function(idx,data){
					data.vndNm = decode(data.vndNm);
					data.biniohdCnt = data.iniohdCnt;
					data.bohdCnt = data.ohdCnt;
				})
				//수정전 데이터를 관리하기 위해서 반복문 돌려서 관리;
				$(jsonPro).each(function(idx,data){
					data.biniohdCnt = data.iniohdCnt;
					data.bohdCnt = data.ohdCnt;
					data.bohdCntR = data.ohdCntR;
					data.bohdCntM = data.ohdCntM;
					data.bohdCntC = data.ohdCntC;
					data.bohdCntP = data.ohdCntP;
				})
				
				//수정전 데이터를 관리하기 위해서 반복문 돌려서 관리;
				$(jsonResult).each(function(idx,data){
					data.biniohdCnt = data.iniohdCnt;
					data.bohdCnt = data.ohdCnt;
				})
				
				kendodata = new kendo.data.DataSource({
					data: jsonRW,
					batch: true,
					editable: false,
					aggregate: [
				          { field: "iniohdCnt", aggregate: "sum" }
				          ,{ field: "ohdCnt", aggregate: "sum" }
			        ],
					schema: {
						model: {
							id: "id",
							fields: {
								prdNo: { editable: false },
								item: { editable: false },
								
								iniohdCnt: { editable: true ,type:"number"},
								rcvCnt: { editable: false },
								issCnt: { editable: false },
								notiCnt: { editable: false },
								ohdCnt: { editable: true ,type:"number"},


							}
						}
					}
				});
				
				kendodataPro = new kendo.data.DataSource({
					data: jsonPro,
					batch: true,
					editable: false,
					schema: {
						model: {
							id: "id",
							fields: {
								prdNo: { editable: false },
								item: { editable: false },
								
								iniohdCnt: { editable: true ,type:"number"},
								ohdCnt: { editable: true ,type:"number"},

								ohdCntR: { editable: true ,type:"number"},
								ohdCntM: { editable: true ,type:"number"},
								ohdCntC: { editable: true ,type:"number"},
								ohdCntP: { editable: true ,type:"number"},


							}
						}
					}
				});
				
				kendodataRt = new kendo.data.DataSource({
					data: jsonResult,
					batch: true,
					editable: false,
					aggregate: [
				          { field: "iniohdCnt", aggregate: "sum" }
				          ,{ field: "rcvCnt", aggregate: "sum" }
				          ,{ field: "issCnt", aggregate: "sum" }
				          ,{ field: "notiCnt", aggregate: "sum" }
				          ,{ field: "ohdCnt", aggregate: "sum" }
			        ],
					schema: {
						model: {
							id: "id",
							fields: {
								prdNo: { editable: false },
								item: { editable: false },
								shipLot: { editable: false },
								
								iniohdCnt: { editable: true ,type:"number"},
								rcvCnt: { editable: false },
								issCnt: { editable: false },
								notiCnt: { editable: false },
								ohdCnt: { editable: true ,type:"number"},


							}
						}
					}
				});
	
				kendotable.setDataSource(kendodata);
				kendotablePro.setDataSource(kendodataPro);
				kendotableRt.setDataSource(kendodataRt);
				
				$.hideLoading();
			}
		})
		
	}
	
	</script>
</head>
<body>
	
	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			
			
			<Tr>
				
				<td rowspan="10" id="svg_td" style="vertical-align: top; top; position:relative; background:#26282c;" >
				
				<div id="subMenu">
					수정할 품번 : <input id="combobox"> <label id="refreshC" onclick="getTable()"><span  class="k-icon k-i-reload k-i-refresh k-i-recurrence"></span></label>
				</div>
				
				<div id="grid" class="grid">
				</div>
				<div id="grid1" class="grid">
				</div>
				<div id="grid2" class="grid">
				</div>
				
				<!-- 
				
				이곳에 필요한 DOM 을 작성합니다.
				실질적 화면에 표시되는 부분.
				
				 -->
				</td>
			</Tr>
			
		</table>
	 </div>
	
</body>
</html>	