<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow: hidden;
	background-color: black;
  	font-family:'Helvetica';
}
#wraper{
	overflow: scroll;
	overflow-x:hidden;
	height : 100%;
	background : #1C1C20;
	border-color : #1E1E23;
	
}
.mainView{
	overflow: hidden;
	overflow-x:hidden;
}
@keyframes redBlink {
    from {color: red;}
    to {color: rgb(167, 1, 1);}
}

/* The element to apply the animation to */
.redBlink{
    animation-name: redBlink;
    animation-duration: 0.1s;
    animation-iteration-count: infinite;
    animation-direction: alternate;
    text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;
    will-change: transform;
    transform: translateZ(0);
}
@keyframes yellowBlink {
    from {color: yellow;}
    to {color: rgb(144, 146, 3);}
}

/* The element to apply the animation to */
.yellowBlink{
    animation-name: yellowBlink;
    animation-duration: 0.1s;
    animation-iteration-count: infinite;
    animation-direction: alternate;
    text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;
    will-change: transform, opacity;
    transform: translateZ(0);
}
@keyframes greenBlink {
    from {color: green;}
    to {color: gray;}
}

/* The element to apply the animation to */
.greenBlink{
    animation-name: greenBlink;
    animation-duration: 0.1s;
    animation-iteration-count: infinite;
    animation-direction: alternate;
    text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;
    will-change: transform, opacity;
    transform: translateZ(0);
}

.green {
    color: #339933;
    text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;
}
.yellow {
    color: #ffff00;
    text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;
}
.red {
    color: #ff0000;
    text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;
}

.thead th{
    background : #353542;
    text-align: center;
    border-right : 1px solid #1E1E23 !important;
    border-bottom : 1px solid #1E1E23 !important;
}

td{
	border-bottom : 1px solid #1E1E23  !important;
	border-left : 1px solid #1E1E23 !important;
}

td.cell{
 background : #F0F0F0 ;
 text-align: center;
 color : black;
}

td.name.cell{
 background :  #DCDCDC ;
 text-align: center;
}


</style> 
<script type="text/javascript">

const loadPage = () =>{
	createMenuTree("config","lampExpression")
}

	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		$(".date").val(year + "-" + month + "-" + day);
	};
	
	$(function(){
		setDate();
		//setEl();
		//time();
		
		/* if(window.sessionStorage.getItem("level")!='2'){
			alert("권한이 없습니다.");
			window.location.href='/aIdoo/chart/index.do';
		} */
		
		//var lang = window.localStorage.getItem("lang");
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		//chkBanner();
		getLampExpresion();
	});
	
	function useModify(val){
		console.log($(val).parent().parent())
		$(val).parent().parent().find('td').find('input:text').each (function() {
			if($(val).prop("checked")){
				$(this).prop('disabled', false);
			}else{
				$(this).prop('disabled', true);
			}
		})
		
		$(val).parent().parent().find('td').find('button').each (function() {
			if($(val).prop("checked")){
				$(this).prop('disabled', false);
			}else{
				$(this).prop('disabled', true);
			}
			
		})

	}
	
	function checkRegExp(id){
	    var open = /^[(]$/;
	    var close = /^[)]$/;
	    var pattern = /^[ADEFGKRXYZ][0-9]+[.][0-9]$/;
	    var notPattern = /^[!][ADEFGKRXYZ][0-9]+[.][0-9]$/;
	    var or = /^[|]$/;
	    var and = /^[&]$/;
	    var not = /^[!]$/;

	    var stringData = $("#"+id).val();
	    
	    //console.log(stringData);
	    
	    var strArray = stringData.split(' ');
	    /* console.log("배열의 길이는 : " + strArray.length);*/
	 	
	    var matching = false;
	    for(var i=0;i<strArray.length;i++){
	       /*  console.log("배열의 각 항목 : " + strArray[i]); */
	        if(strArray[i].trim().match(open)!=null || 
	        strArray[i].trim().match(close)!=null || 
	        strArray[i].trim().match(and)!=null || 
	        strArray[i].trim().match(not)!=null || 
	        strArray[i].trim().match(or)!=null ){
	            matching = true;
	           /*  console.log("strArray[i].match(open)" + strArray[i].match(open))
	            console.log("strArray[i].match(close)" + strArray[i].match(close))
	            console.log(" strArray[i].match(or)" +  strArray[i].match(or)) */
	        }else if(  strArray[i].match(pattern)!=null || 
	        strArray[i].match(notPattern)!=null ){
	            matching = true;
	           /*  console.log("strArray[i].match(pattern)" + strArray[i].match(pattern))
	            console.log("strArray[i].match(notPattern) " + strArray[i].match(notPattern)) */
	            strArray[i] = 1;
	        }else if(stringData=='' || stringData==null){
	        	 matching = true;
	        }else{
	        	 matching = false;
	             /*  console.log("매칭 안됨") */
	             alert("올바른 수식이 아닙니다.");
	             return false;
	        }
	    }



	    var expression="";
	    if(matching){
	        /* console.log("모두 매칭됩니다.") */
	        for(var i=0;i<strArray.length;i++){
	            expression+=strArray[i]
	        }
	        //console.log(expression)
	        try{
	        	var r;
	            //console.log(eval(expression))
	            if(stringData=='' || stringData==null){
	            	r = confirm("값이 없습니다. 저장 하시겠습니까?");
	            }else{
	            	r = confirm("올바른 수식 입니다. 저장 하시겠습니까?");
	            }
				if (r == true) {
					
					//console.log(id)
					
					var extractId = id.replace(/[^0-9]/g,"");
					var extractExpression = id.replace(/[^a-zA-Z]+/,"");
					
					//console.log("id : "+ extractId +", ExpressionName : " + extractExpression+", Expression :"+  stringData);
					stringData = stringData.replace(/&/gi, "%26");
					var url = "${ctxPath}/chart/setLampExpression.do"
					var param = "dvcId=" + extractId + 
						"&expressionName=" + extractExpression +"Expression"+
						"&expression=" + stringData;
					//console.log(url+param)
					
					
					$.ajax({
						url : url,
						data: param,
						dataType : "text",
						type : "post",
						success : function(data){
							if(data=="success"){
								alert("성공적으로 저장 되었습니다.")
								location.reload();
							}
						}
					})
						
				} else {
					
				}
							
	        }catch(exception){
	            alert("올바른 수식이 아닙니다.")
	        }
	        
	    }else{
	        alert("매칭되지 않는 문자열 입니다. 제대로 기입하여 주세요.")
	    }

	}
	
	function getLampExpresion(){
		var url = "${ctxPath}/chart/getLampExpression.do"
		
		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			success : function(lamp){

				var obj = lamp.dvcId
				console.log(obj)
				
				$(".tmpTable").find("tr:gt(0)").remove();
				$(obj).each(function(i, data){
					
					var redExpression = data.redExpression
					if(redExpression!=null){
						var redExpression = redExpression.replace(/\uFFFD/g, ' ')
					}
					var redBlinkExpression = data.redBlinkExpression
					if(data.redBlinkExpression!=null){
						redBlinkExpression = redBlinkExpression.replace(/\uFFFD/g, ' ')
					}else{
						redBlinkExpression = '';
					}
					var yellowExpression = data.yellowExpression
					if(yellowExpression!=null){
						yellowExpression = yellowExpression.replace(/\uFFFD/g, ' ')
					}
					var yellowBlinkExpression = data.yellowBlinkExpression
					if(yellowBlinkExpression!=null){
						yellowBlinkExpression = yellowBlinkExpression.replace(/\uFFFD/g, ' ')
					}else{
						yellowBlinkExpression = '';
					}
					var greenExpression = data.greenExpression
					if(greenExpression!=null){
						greenExpression = greenExpression.replace(/\uFFFD/g, ' ')
					}
					var greenBlinkExpression = data.greenBlinkExpression
					if(greenBlinkExpression!=null){
						greenBlinkExpression = greenBlinkExpression.replace(/\uFFFD/g, ' ')
					}else{
						greenBlinkExpression = '';
					}
					
					var tr = "<tr>" + 
							 "<td class='name cell'>" + data.name + "</td>" +
							 "<td class='cell'><input type='checkBox' id='"+data.dvcId+"' onclick='useModify(this)'></td>" +
							 "<td class='cell'><input type='text' id='red"+data.dvcId+"' disabled value='" + redExpression + "' placeholder='입력해주세요.'><button disabled onclick=checkRegExp('red"+data.dvcId+"')>확인 / 저장</button></td>" +
							 "<td class='cell'><input type='text' id='redBlink"+data.dvcId+"' disabled value='" + redBlinkExpression + "' placeholder='입력해주세요.'><button disabled onclick=checkRegExp('redBlink"+data.dvcId+"')>확인 / 저장</button></td>" +
							 "<td class='cell'><input type='text' id='yellow"+data.dvcId+"' disabled value='" + yellowExpression + "' placeholder='입력해주세요.'><button disabled onclick=checkRegExp('yellow"+data.dvcId+"')>확인 / 저장</button></td>" +
							 "<td class='cell'><input type='text' id='yellowBlink"+data.dvcId+"' disabled value='" + yellowBlinkExpression + "' placeholder='입력해주세요.'><button disabled onclick=checkRegExp('yellowBlink"+data.dvcId+"')>확인 / 저장</button></td>" +
							 "<td class='cell'><input type='text' id='green"+data.dvcId+"' disabled value='" + greenExpression + "' placeholder='입력해주세요.'><button disabled onclick=checkRegExp('green"+data.dvcId+"')>확인 / 저장</button></td>" +
							 "<td class='cell'><input type='text' id='greenBlink"+data.dvcId+"' disabled value='" + greenBlinkExpression + "' placeholder='입력해주세요.'><button disabled onclick=checkRegExp('greenBlink"+data.dvcId+"')>확인 / 저장</button></td>" +
							 "</tr>"
									
					$(".tmpTable > tbody:last").append(tr);
				})
				
				$(".cell").css({
					"height" : getElSize(200),
					"padding" : getElSize(20)
				})
				
				$(".thead th").css({
					"background" : "#353542",
					"height" : getElSize(132),
					"font-size" : getElSize(36)
				})
				
				$("input[type='text']").css({
					 "border": "1px black #999",
			          "width" : getElSize(392),
			           "height" : getElSize(80),
			           "font-family": "inherit",
			            "background-color" : "black",
			            "z-index" : "999",
			            "border-radius": "0px",
			            "appearance":"none",
			            "background-size" : getElSize(60),
			            "color" : "white",
			           "border" : "none",
			          "font-size" : getElSize(48),
					"border-color" : "#222327"
				})
				
				$("button").css({
					"font-size" : getElSize(48),
					"margin-top" : getElSize(20),
					"width" : getElSize(240),
					"height" : getElSize(80),
	    			"border-color" : "#9B9B9B",
	    			"background" : "#9B9B9B",
	    			"color" : "black"
				})
				
				$("input[type='checkBox']").css({
					"width" : "50%",
					"height" : "50%"
				})
				
				$(".name").css({
					"font-size" : getElSize(36)
				})
				
				$("#nameHeader").css({
					"background" : "#353542",
					"font-size" : getElSize(36)
				})
				
				$("#wraper").scroll(function() {
					//$('.name').css('transform', 'translateX('+ $(this).scrollLeft() +'px)');
					$(".thead").css('transform', 'translateY('+ $(this).scrollTop() +'px)');
				});
				
				/* $("input[type='text']").focus(function(){
					$(this).css({
						"position" : "fixed",
						"width" : "25%"
					})
					
					$(this).blur(function(){
						$(this).css({
							"position" : "relative",
							"width" : "100%"
						})
					})
				}) */
			}
		})
		
	}
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999,
			"pointer-events": "none"
		})
		
		
		
		$("#table").css({
			"position" : "absolute",
			"bottom" : marginHeight,
			"height" : getElSize(1750),
			"left" : getElSize(40)	
		});
		
		$("#table2 td").css({
			"text-align" : "center",
			"color" : "white",
			"font-size" : getElSize(35),
			"padding" : getElSize(20),
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		
		$(".tmpTable").css({
			"width" : "100%"
		})
		
		$("#wraper").css({
			"width" : $("#container").width() - getElSize(90)
		})
		
		if(window.localStorage.getItem("lang")=="ko"){
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(100),
				"position" : "absolute",
				"top" : marginHeight + getElSize(85),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else if(window.localStorage.getItem("lang")=="en" || window.localStorage.getItem("lang")=="de"){
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(70),
				"position" : "absolute",
				"top" : marginHeight + getElSize(115),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else{
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(90),
				"position" : "absolute",
				"top" : marginHeight + getElSize(100),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}
		
	}
		
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	
	
	<div id="title_right"></div>
		
		<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td class="mainView" rowspan="10" id="svg_td" style="vertical-align: top;">
					<div id="wraper">
						<table style="color: white; text-align: center; border-collapse: collapse;" class="tmpTable" border="1">
							<tr style="font-weight: bolder;background-color: rgb(34,34,34)" class="thead">
								<th id="nameHeader" class="name" style="width:10%">장비명</th>
								<th style="width:5%">수정</th>
								<th class="titleHead red">빨강</th>
								<th class="titleHead redBlink">빨강 깜빡임</th>
								<th class="titleHead yellow">노랑</th>
								<th class="titleHead yellowBlink">노랑 깜빡임</th>
								<th class="titleHead green">녹색</th>
								<th class="titleHead greenBlink">녹색 깜빡임</th>
							</tr>	
						</table>
					</div>
				</td>
			</Tr>

		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	