<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<link rel="stylesheet" href="${ctxPath }/css/jquery.loading.min.css">
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.516/styles/kendo.common.min.css"/>
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.516/styles/kendo.rtl.min.css"/>
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.516/styles/kendo.silver.min.css"/>
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery.loading.min.js"></script>
<script src="https://kendo.cdn.telerik.com/2018.2.516/js/kendo.all.min.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow: hidden;
	background-color: black;
  	font-family:'Helvetica';
}
#wraper{
	height : 100%;
	border-color : #1E1E23;
}
.mainView{
	overflow: hidden;
	overflow-x:hidden;
}
.k-dialog-titlebar{
	display : none
}
.inline{
	display : inline-block;
}
#elementArea{
	border : 1px solid white;
}
.btn{
    width: 10em;
    margin: 1em;
    height: 2em;
    font-size: 1em;
}
#operator-select{
	font-size:1.5em;
	margin-left:1em;
	margin-right:1em;
}
#value-input{
	font-size:1.5em;
	width: 5em;
}
.title{
	font-size:1.5em;
    text-align: -webkit-center;
}
.center{
	text-align: center;
	display: inline-flex;
	position: absolute;
}
#status-table{
    overflow: hidden;
}


</style> 
<script type="text/javascript">
	

const loadPage = () =>{
	createMenuTree("config","status-Setting")
}

	$(function(){
		
		getToday();
		setEl();
		/* 
		if(window.sessionStorage.getItem("level")!='2'){
			alert("권한이 없습니다.");
			window.location.href='/aIdoo/chart/index.do';
		} */
		
		var lang = window.localStorage.getItem("lang");
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
		
		getDvcList();
	});
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
/* 		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		}) */
		
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999,
			"pointer-events": "none"
		})
		
				
		$("#table").css({
			"position" : "absolute",
			"bottom" : marginHeight,
			"height" : getElSize(1750),
			"left" : $("#container").offset().left + getElSize(50)	
		});
		
		$("#table2 td").css({
			"text-align" : "center",
			"color" : "white",
			"font-size" : getElSize(35),
			"padding" : getElSize(20),
			"border": getElSize(1) + "px solid rgb(50,50,50)"
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		$("#status-table").css({
			"width" : $("#container").width() - getElSize(130),
			"color" : "black",
			"border" : getElSize(1)+"px solid #1E1E23",
			"display" : "block",
			"table-layout": "fixed",
			"height" : getElSize(1550)
		})
		
		$("#status-table td").css({
			"height" : getElSize(200),
			"width" : getElSize(80),
			/* "white-space": "nowrap", */
            "text-overflow": "ellipsis",
		})
		
		$("#status-table input").css({
			"width" : "100%",
			"height" : "100%",
			"font-size" : getElSize(36)
		})
		
		$(".cut").css({
			"background" : "#175501",
			"text-align" : "-webkit-center",
			"font-size" : getElSize(36),
			"width" : getElSize(80)
		})
		
		$(".incycle").css({
			"background" : "green",
			"text-align" : "-webkit-center",
			"font-size" : getElSize(36),
			"width" : getElSize(80)
		})
		
		$(".alarm").css({
			"background" : "red",
			"text-align" : "-webkit-center",
			"font-size" : getElSize(36),
			"width" : getElSize(80)
		})
		
		$(".wait").css({
			"background" : "yellow",
			"color" : "black",
			"text-align" : "-webkit-center",
			"font-size" : getElSize(36),
			"width" : getElSize(80)
		})
		
		$("button").css({
			"width" : "100%",
			"height" : "100%",
			"border-color" : "#9B9B9B",
			"background" : "#9B9B9B"
		})
		
		$("#title-div").css({
			"font-size" : getElSize(48),
			"color" : "white",
			"background" : "#2B2D32",
			"padding-top" : getElSize(40),
			"padding-left" : getElSize(40),
			"height" : getElSize(128)
		})
		
		$(".inline").css({
			"border" : getElSize(1)+"px solid #242424",
			"margin" : getElSize(10),
			"padding" : getElSize(20),
			"background" : "#F2F2F2",
			"color" : "black",
			"font-size" : getElSize(36)
		})
		
		$(".btn").css({
			"width" : getElSize(300),
			"margin-top" : getElSize(100),
			"font-size" : getElSize(50)
		})
		
		$(".plus-btn").css({
			"width" : getElSize(100),
			"height" : getElSize(80),
			"margin-left" : getElSize(30),
			"margin-top" : getElSize(30),
			"font-size" : getElSize(36),
			"font-weight" : "bolder",
			"background" : "#699CEE",
			"border" : getElSize(5)+"px solid gray"
		})
		
		$("#CUT-area, #ALARM-area").css({
			"width" : getElSize(3250),
			"height" : getElSize(380),
			"overflow-x" : "auto",
			"overflow-y": "auto",
			//"border" : getElSize(1)+"px solid #1E1E23",
			"background" : "#DCDCDC"
		})
		
		$("#IN-CYCLE-area, #WAIT-area").css({
			"width" : getElSize(3250),
			"height" : getElSize(380),
			"overflow-x" : "auto",
			"overflow-y": "auto",
			//"border" : getElSize(1)+"px solid #1E1E23",
			"background" : "#F0F0F0"
		})
		
		$("#dvc-div").css({
			"display" : "inline",
			"position" : "absolute",
			"right" : getElSize(40)
		})
		
		$("#select-dvc").css({
			"font-size" : getElSize(48),
			"width" : getElSize(392),
			"height" : getElSize(80),
			"border": "1px black #999",
           "font-family": "inherit",
            "background": "url(${ctxPath}/images/FL/default/btn_drop_menu_default.svg) no-repeat 95% 50%",
            "background-color" : "black",
            "z-index" : "999",
            "border-radius": "0px",
            "-webkit-appearance": "none",
            "-moz-appearance": "none",
            "appearance":"none",
            "background-size" : getElSize(60),
            "color" : "white",
            "border" : "none"
		})

	}
	
	function modify_element(e){
		
		if(dropdownSelect!='AND' && dropdownSelect!='OR' && dropdownSelect!='(' && dropdownSelect!=')'){
			$("div[id='"+e+"']").html(dropdownSelect +' '+ $("#operator-select").val() +' '+ $("#value-input").val());
			$("div[id='"+e+"']").attr('class', 'inline');	
		}else{
			if(dropdownSelect=='AND'){
				$("div[id='"+e+"']").attr('class', 'inline');	
				$("div[id='"+e+"']").addClass('and');
			}else if(dropdownSelect=='OR'){
				$("div[id='"+e+"']").attr('class', 'inline');	
				$("div[id='"+e+"']").addClass('OR');
			}else if(dropdownSelect=='(' || dropdownSelect==')'){
				$("div[id='"+e+"']").attr('class', 'newClass');
				$("div[id='"+e+"']").addClass('braket');
			}
			$("div[id='"+e+"']").html(dropdownSelect);
		}
		
		setStyle();
		
		input_dialog.close();
		
	}
		
		
	function getStatusExpression(){
		
		/* $("#svg_td").loading({
			theme: 'dark'
		})
 */		
		var url = ctxPath + "/chart/getStatusExpression.do";
		var param = "shopId=" + shopId + "&dvcId=" + $("#select-dvc").val() 
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				
				//$("#svg_td").loading('stop')
				var json = data.statementList
				for(var i=0;i<json.length;i++){
					if(json[i].type.trim()=="MTC" && json[i].status=="CUT"){
						getExpression(json[i].statement.trim(), json[i].status)
					}
					if(json[i].type.trim()=="MTC" && json[i].status=="IN-CYCLE"){
						getExpression(json[i].statement.trim(), json[i].status)
					}
					if(json[i].type.trim()=="MTC" && json[i].status=="WAIT"){
						getExpression(json[i].statement.trim(), json[i].status)
					}
					if(json[i].type.trim()=="MTC" && json[i].status=="ALARM"){
						getExpression(json[i].statement.trim(), json[i].status)
					}
				}
				
				if(json.length==0){
					$("#CUT-area").find('*').not('button').remove();
					$("#IN-CYCLE-area").find('*').not('button').remove();
					$("#WAIT-area").find('*').not('button').remove();
					$("#ALARM-area").find('*').not('button').not('.static').remove();
				}
				
				
			}
		});
	}
	
	
	function updateExpression(e, statement){
		
		/* $('#svg_td').loading({
			theme: 'dark'
		}); */
		
		var status;
		
		var expression ={}
		
		expression.status=e;
		expression.statment=status;
		
		var url = ctxPath + "/updateStatus.do";
		var param = "status=" + e + "&expression=" + statement + "&dvcId=" + $("#select-dvc").val()
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				//$('#svg_td').loading('stop');
				alert("저장 완료 되었습니다.");
			},error:function(e){
				//$('#svg_td').loading('stop');
			}
		})
	}
	
	var input_dialog;
	var before_input='AND';
	
	$(function(){
		/*$(".inline").kendoDraggable({
			hint:function(e){
				return e.clone();
			}
		})
		
		
		$("#area").kendoDropTargetArea({ 
			filter : "#drop-cut",
			drop:function(e){
				console.log(e.draggable.currentTarget[0].outerText)
				if(before_input!='AND' && before_input!='OR'){
					if(e.draggable.currentTarget[0].outerText=='AND' || e.draggable.currentTarget[0].outerText=='OR'){
						if(e.draggable.currentTarget[0].outerText!='AND' && e.draggable.currentTarget[0].outerText!='OR'){
							
							input_dialog.content(e.draggable.currentTarget[0].outerText + "의 값을 지정 해주십시오<br>"+
							e.draggable.currentTarget[0].outerText + "<select id='operation'><option>></option><option>>=</option><option><</option><option><=</option><option>=</option></select>"+
							"<input type='text' id='input-text'><br>"+
							"<button class='btn' onclick=input_status('"+e.draggable.currentTarget[0].outerText+"')>추가</button>"+	
							"<button class='btn close' onclick='input_dialog.close()'>닫기</button>"
							)
							input_dialog.open();
							$(".btn").css({
								"width" : getElSize(300),
								"margin-top" : getElSize(100),
								"font-size" : getElSize(50)
							})
							
							$(".close").css({
								"margin-left" : getElSize(50)
							})
							
						}else{
							$("#drop-cut").append("<div class='col col-4 px2 py1' onclick='deleteOperator(this)'>"+
									"<div class='bg-maroon py3 yellow center'>"+e.draggable.currentTarget[0].outerText+"</div>")
							before_input=e.draggable.currentTarget[0].outerText
							$(".inline").css({
								"border" : getElSize(1)+"px solid #242424",
								"margin" : getElSize(10),
								"padding" : getElSize(20),
								"line-height" : getElSize(12),
								"background" : "#F2F2F2",
								"color" : "black"
							})
							
							$(".py3").css({
								"width" : getElSize(400),
								"height" : getElSize(100),
								"line-height" : getElSize(6.5)
							})
							
							sortable('.js-grid', {
								forcePlaceholderSize: true,
								placeholderClass: 'col col-4 border border-maroon'
							});
						}
					}else{
						alert("올바른 항목을 선택하여 주십시오");
					}
				}else{
					if(!(e.draggable.currentTarget[0].outerText=='AND') && !(e.draggable.currentTarget[0].outerText=='OR')){
						if(e.draggable.currentTarget[0].outerText!='AND' && e.draggable.currentTarget[0].outerText!='OR'){
							
							input_dialog.content(e.draggable.currentTarget[0].outerText + "의 값을 지정 해주십시오<br>"+
							e.draggable.currentTarget[0].outerText + "<select id='operation'><option>></option><option>>=</option><option><</option><option><=</option><option>=</option></select>"+
							"<input type='text' id='input-text'><br>"+
							"<button class='btn' onclick=input_status('"+e.draggable.currentTarget[0].outerText+"')>추가</button>"+	
							"<button class='btn close' onclick='input_dialog.close()'>닫기</button>"
							)
							input_dialog.open();
							$(".btn").css({
								"width" : getElSize(300),
								"margin-top" : getElSize(100),
								"font-size" : getElSize(50)
							})
							
							$(".close").css({
								"margin-left" : getElSize(50)
							})
							
						}else{
							$("#drop-cut").append("<div class='col col-4 px2 py1' onclick='deleteOperator(this)'>"+
									"<div class='bg-maroon py3 yellow center'>"+e.draggable.currentTarget[0].outerText+"</div>")
							
							before_input=e.draggable.currentTarget[0].outerText
							$(".inline").css({
								"border" : getElSize(1)+"px solid #242424",
								"margin" : getElSize(10),
								"padding" : getElSize(20),
								"line-height" : getElSize(12),
								"background" : "#F2F2F2",
								"color" : "black"
							})
							
							$(".py3").css({
								"width" : getElSize(400),
								"height" : getElSize(100),
								"line-height" : getElSize(6.5)
							})
							
							sortable('.js-grid', {
								forcePlaceholderSize: true,
								placeholderClass: 'col col-4 border border-maroon'
							});
						}
					}else{
						alert("올바른 항목을 선택하여 주십시오");
					}
				}
			}
		}) */
		
		input_dialog = $("#input-dialog").kendoDialog({
			visible:false,
			title: "로직 수정 화면",
			minWidth: getElSize(1500)
		}).data("kendoDialog")
		
	})
	
	
	var randomizeId=[];
	
	function getId(){
		var idNum=Math.floor(Math.random() * 1000) + 1;
		
		if(randomizeId.length>0){
			for(var i=0;i<randomizeId.length;i++){
				if(randomizeId[i]==idNum){
					getId()
				}else{
					randomizeId.push(idNum)
					return idNum;
				}
			}
		}else{
			randomizeId.push(idNum)
			return idNum;
		}
	}
	
	function input_status(e, e1){
		var str_space = /\s/;
		
		var idNum=getId();
		
		if(typeof $("#value-input").val()!='undefined' && $("#value-input").val()!="" && !str_space.exec($("#value-input").val())){
			
			$("<div class='inline' id='"+dropdownSelect+idNum+"' onclick='modifyElement(this)'>"+dropdownSelect+
					" "+$("#operator-select").val()+" "+$("#value-input").val()+"</div>").insertBefore($("#"+e1));
			
			input_dialog.close();
			before_input=dropdownSelect
			
		}else{
			
			if(dropdownSelect=='AND'){
				$("<div class='inline and' id='"+dropdownSelect+idNum+"' onclick='modifyElement(this)'>"+dropdownSelect+
				"</div>").insertBefore($("#"+e1));
				
				input_dialog.close();
				before_input=dropdownSelect
			}else if(dropdownSelect=='OR'){
				$("<div class='inline or' id='"+dropdownSelect+idNum+"' onclick='modifyElement(this)'>"+dropdownSelect+
				"</div>").insertBefore($("#"+e1));
				input_dialog.close();
				before_input=dropdownSelect
			}else if(dropdownSelect=='(' || dropdownSelect==')'){
				$("<div class='inline braket' id='"+dropdownSelect+idNum+"' onclick='modifyElement(this)'>"+dropdownSelect+
				"</div>").insertBefore($("#"+e1));
				
				input_dialog.close();
				before_input=dropdownSelect
			}else{
				alert("값을 입력하여 주세요")
			}
		}
		
		setStyle();
	}
	
	function getDvcList(){
		
		var url = ctxPath + "/chart/getDvcList.do";
		var param = "shopId=" + shopId
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(json){
				$("#select-dvc").append('<option value="0">Default</option>')
				$(json).each(function(i, data){
					$("#select-dvc").append("<option value="+data.dvcId+">"+data.name+"</option>")
				})
				getStatusExpression();
			},error:function(e){
			}
		})
		
	}
	
	function modifyElement(e){
		
		var split = $(e)[0].outerText.split(" ");
		var id=$(e)[0].id;
		
		id = "\"" + id + "\""
		
		input_dialog.content(
		"<div class='title'>"+$(e)[0].outerText + "를 수정합니다</div><br>" +
		"<div class='center'><input id='dropdownList'>" + 
		"<select id='operator-select'><option>></option><option>>=</option><option><</option><option><=</option><option>=</option></select>"+
		"<input type='text' id='value-input'></div><br>"+
		"<button class='btn' onclick=modify_element("+id+")>수정</button>"+	
		"<button class='btn' onclick=insert_element("+id+")>이전 구문에 추가하기</button>"+	
		"<button class='btn close' onclick='deleteElement("+id+")'>삭제</button>"+		
		"<button class='btn close' onclick='input_dialog.close()'>닫기</button>"
		)
		setDropDownList();
		
		$(".center").css({
			"left" : getElSize(500),
			"bottom" : getElSize(230)
		})
		
		input_dialog.open();
	}
	
	function insert_element(e){
		
		var idNum=getId();
		
		if(dropdownSelect!='AND' && dropdownSelect!='OR' && dropdownSelect!='(' && dropdownSelect!=')'){
			$("<div class='inline' id='"+dropdownSelect+idNum+"' onclick='modifyElement(this)'>"+dropdownSelect+
					" "+$("#operator-select").val()+" "+$("#value-input").val()+"</div>").insertBefore($("div[id='"+e+"']"));
		}else{
			$("<div class='inline' id='"+dropdownSelect+idNum+"' onclick='modifyElement(this)'>"+dropdownSelect+
					"</div>").insertBefore($("div[id='"+e+"']"));
		}
		
		setStyle();
		input_dialog.close();
		
	}


	function deleteElement(e){
		
		$("div[id='"+e+"']").remove()
		input_dialog.close()
		
	}
	
	/* function deleteOperator(e){
		
		if($(e)[0].nextSibling!=null){
			var check = confirm("다음 번 객체가 존재합니다. 삭제 합니까?")
			if(check){
				$(e).remove()
				before_input="else"
			}
		}else{
			console.log("다음 객체 없음")
			$(e).remove()
			before_input="else"
		}
		
		
	} */
	
	var dropdownSelect;
	
	function insertElement(e, e1){
		
	    input_dialog.content(
    		"<div class='title'>"+"상태 로직 항목을 추가 합니다.</div><br>" +
    		"<div class='center'>"+	
	    	"<input id='dropdownList'/>"+
	    	
   			"<select id='operator-select'>"+
   			"</select>" +
   			"<input type='text' id='value-input'><br>"+
   			"</div>"+
   			"<button class='btn' onclick=input_status('"+e+"','"+$(e1)[0].id+"')>추가</button><button class='btn' onclick='input_dialog.close()'>닫기</button>"
   			
	    )
	    setDropDownList();
	    
	    
	    $(".center").css({
			"left" : getElSize(200),
			"bottom" : getElSize(250)
		})
		
	    $(".btn").css({
	    	"margin-top" : getElSize(100),
	    	"margin-left": getElSize(200)
		})
	    
		input_dialog.open();
		
	}
	
	
	var dataSource = new kendo.data.DataSource({
		  data: [
			  	{view:'Spindle_Load', name:'spd_load',group:'field'},
				{view:'Status', name:'status',group:'field'},
				{view:'Mode', name:'mode',group:'field'},
				{view:'Spindle_Actual_Speed', name:'spd_act_speed',group:'field'},
				{view:'M_Code', name:'MDL_M1',group:'field'},
				{view:'PROGRAM_NAME', name:'main_prgm_name',group:'field'},
				
				{view:'PMC_CUT', name:'pmc_cut',group:'PMC'},
				{view:'PMC_EMG', name:'PMC_EMG',group:'PMC'},
				{view:'PMC_Feed_Hold', name:'PMC_Feed_Hold',group:'PMC'},
				{view:'PMC_Single_Block', name:'PMC_Single_Block',group:'PMC'},
				{view:'PMC_STL', name:'PMC_STL',group:'PMC'},
				
				{view:'AND', name:'AND',group:'operator'},
				{view:'OR', name:'OR',group:'operator'},
				
				{view:'(', name:'(',group:'operator'},
				{view:')', name:')',group:'operator'}
		  ],
		 	 	group: { field: "group" }
		});
	
	function checkExpression(e){
		
		var string='';
		var save_string='';
		
		console.log(e)
		
		$("#"+e+"-area").find("div").each(function(index){
			var split = $(this)[0].outerText.split(" ");
			
			for(var i=0;i<dataSource.options.data.length;i++){
				if(split[0]==dataSource.options.data[i].view){
					save_string+=dataSource.options.data[i].name
				}
			}
			for(var i=1;i<split.length;i++){
				if(i==2){
					if(isNaN(Number(split[i]))){
						
						if(split[i].indexOf("'")!=-1){
							save_string+=split[i]
						}else{
							save_string+="'"+split[i]+"'"
						}
						
						
					}else{
						save_string+=split[i]
					}
				}else{
					save_string+=split[i]
				}
				
			}
			
			if(split[0]=='(' || split[0]==')'){
				
			}else if(split[0]!='AND' && split[0]!='OR' && split[0]!='Mode' && split[0]!='Status'){
				split[0]='1';
				split[2]='1';
			}else if(split[0]=='Mode' || split[0]=='Status'){
				split[2]='1';
				split[0]='1';
			}else{
				split[0]='&';
			}
			
			if(split[1]=='='){
				split[1]='=='
			}
			
			for(var i=0;i<split.length;i++){
				string+=split[i];
			}
			string+=' '
			save_string+=' '
			
		})
		
		try{
			console.log(string)
			if(e=='ALARM'){
				save_string = 'ALARM_EXIST'+save_string
			}
			console.log(save_string)
			
			eval(string)
			var r = confirm("정상 코드입니다. 저장 합니까?")
			if(r){
				updateExpression(e, save_string)
			}
		}catch (error){
			console.log(error)
			alert("문제가 있는 코드입니다.")
		}
	}
	
	function setDropDownList(){
		
		$("#dropdownList").kendoDropDownList({
	    	dataSource:dataSource,
	    	change: function(e) {
	    	    var value = this.value();
	    	    
	    	    for(var i=0;i<dataSource.options.data.length;i++){
	    	    	if(this.value()==dataSource.options.data[i].name){
	    	    		if(dataSource.options.data[i].group=='operator'){
	    	    			dropdownSelect = dataSource.options.data[i].view
	    	    			$("#operator-select").css({
	    	    				"display" : "none"
	    	    			})
	    	    			$("#value-input").css({
	    	    				"display" : "none"
	    	    			})
	    	    		}else{
	    	    			dropdownSelect = dataSource.options.data[i].view
	    	    			
	    	    			if(dropdownSelect=='Mode' || dropdownSelect=='Status' || dropdownSelect=='M_Code' || dropdownSelect.indexOf('PMC')!=-1){
	    	    				appendOption(1)
	    	    			}else{
	    	    				appendOption(2)
	    	    			}
	    	    			
	    	    			$("#operator-select").css({
	    	    				"display" : "inline"
	    	    			})
	    	    			$("#value-input").css({
	    	    				"display" : "inline"
	    	    			})
	    	    		}
	    	    	}
	    	    }
	    	    
			},
			dataBound: function(e) {
				var value = this.value();
				for(var i=0;i<dataSource.options.data.length;i++){
	    	    	if(this.value()==dataSource.options.data[i].name){
	    	    		if(dataSource.options.data[i].group=='operator'){
	    	    			dropdownSelect = dataSource.options.data[i].view
	    	    			appendOption(1)
	    	    		}else{
	    	    			appendOption(2)
	    	    			dropdownSelect = dataSource.options.data[i].view
	    	    		}
	    	    	}
	    	    }
			},
	    	dataTextField:"view",
	    	dataValueField:"name"
	    })
		
	}
	
	function appendOption(e){
		if(e==1){
			$("#operator-select").empty()
			$("#operator-select").append("<option>=</option>")
			$("#operator-select").append("<option>!=</option>")
		}else{
			$("#operator-select").empty()
			$("#operator-select").append("<option><</option>")
			$("#operator-select").append("<option><=</option>")
			$("#operator-select").append("<option>></option>")
			$("#operator-select").append("<option>>=</option>")
			$("#operator-select").append("<option>=</option>")
			$("#operator-select").append("<option>!=</option>")
		}
	}
	
	var operatorArray = ['<=','>=','!=','<','>','=']
	
	function getExpression(e, status){
		var string = e;
		var string2='';
		var before_element='AND';
		
		var logicallyElement=['AND', 'OR', '(', ')'];
		var checker=false;
		var regex = /'/g;
		
		var array = string.split(' ');
		var array2 =[];
		var save_Word
		
		for(var i=0;i<array.length;i++){
			if(checker){
				checker=false;
			}else{
				var operator='';
				for(var j=0; j<operatorArray.length;j++){
					if(array[i].indexOf(operatorArray[j])!=-1){
						if(save_Word==array[i]){
							
						}else{
							
							save_Word=array[i]
							operator=operatorArray[j]
							array2 = array[i].split(operatorArray[j]);
						}
					}
				}
			
				if(array2.length==0){
					string2=array[i]
				}else{
					for(var j=0; j<array2.length;j++){
						
						if(j>0){
							string2+=' '+operator+' '+array2[j]
						}else{
							for(var l=0;l<dataSource.options.data.length;l++){
								if(array2[j]==dataSource.options.data[l].name){
									array2[j]=dataSource.options.data[l].view
								}
							}
							if(isNaN(Number(array2[1]))){
								var results = array2[1].match(regex);
								if(results.length<2){
									checker=true;
								}
							}
							string2+=array2[j]
						}
					}
				}
				
				
				var idNum=getId();
				
				if(checker){
					
					string2+=' '+array[i+1]
				}
				
				if(status=='ALARM'){
					
					if(i==0){
						
					}else{
						$("<div class='inline' id='"+idNum+"' onclick='modifyElement(this)'>"+string2+"</div>").insertBefore($("#"+status+"-status-btn"))
					}
					
				}else{
					if(string2=='AND' || string2=='OR'){
						$("<div class='inline "+string2+"' id='"+idNum+"' onclick='modifyElement(this)'>"+string2+"</div>").insertBefore($("#"+status+"-status-btn"))
					}else if(string2=='(' || string2==')'){
						$("<div class='inline braket' id='"+idNum+"' onclick='modifyElement(this)'>"+string2+"</div>").insertBefore($("#"+status+"-status-btn"))
					}else{
						$("<div class='inline' id='"+idNum+"' onclick='modifyElement(this)'>"+string2+"</div>").insertBefore($("#"+status+"-status-btn"))
					}
				}
				
				before_element=string2;
				string2='';
				array2=[];
			}
			
			setStyle();
		}
			
	}
	
	function setStyle(){
		$(".inline").css({
			"border" : getElSize(1)+"px solid #242424",
			"margin" : getElSize(10),
			"padding" : getElSize(20),
			"background" : "#F2F2F2",
			"display" : "inline-block",
			"font-weight" : "normal",
			"color" : "black",
			"font-size" : getElSize(36),
			"height" : getElSize(80),
		})
		
		$(".and").css({
			"color" : "#4ed419",
			"background" : "#676767",
			"font-weight" : "bolder",
			"font-size" : getElSize(36)
		})
		
		$(".or").css({
			"color" : "blue",
			"background" : "#676767",
			"font-weight" : "bolder",
			"font-size" : getElSize(36)
		})
		
		$(".braket").css({
			"color" : "yellow",
			"background" : "black",
			"font-weight" : "bolder",
			"font-size" : getElSize(36)
		})
		
		if(window.localStorage.getItem("lang")=="ko"){
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(100),
				"position" : "absolute",
				"top" : marginHeight + getElSize(85),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else if(window.localStorage.getItem("lang")=="de" || window.localStorage.getItem("lang")=="en"){
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(70),
				"position" : "absolute",
				"top" : marginHeight + getElSize(115),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else{
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(90),
				"position" : "absolute",
				"top" : marginHeight + getElSize(100),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}
		
	}
	
</script>
</head>
<body>
	
	
	<div id="title_right"></div>
	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td class="mainView" rowspan="10" id="svg_td" style="vertical-align: top;">
					<div id="title-div" style="vertical-align:middle">
						※ 상태 결정 로직 
						<div id="dvc-div">장비 : <select id="select-dvc" onchange="getStatusExpression()"></select></div>
					</div>
					<table id="status-table">
						<colgroup>
				            <col width="5%" />
				            <col width="*" />
							<col width="5%" />
						</colgroup>
						<tr>
							<td class="cut">
								CUT
							</td>
							<td>
								<div id="CUT-area">
									<button class="plus-btn" id="CUT-status-btn" onclick='insertElement("CUT-area", this)'> + </button>
								</div>
							</td>
							<td>
								<button onclick='checkExpression("CUT")'>저장</button>
							</td>
						</tr>
						<tr>
							<td class="incycle">
								IN-CYCLE
							</td>
							<td>
								<div id="IN-CYCLE-area">
									<button class="plus-btn" id="IN-CYCLE-status-btn" onclick='insertElement("IN-CYCLE-area", this)'> + </button>
								</div>
							</td>
							<td>
								<button onclick='checkExpression("IN-CYCLE")'>저장</button>
							</td>
						</tr>
						<tr>
							<td  class="alarm">
								ALARM
							</td>
							<td>
								<div id="ALARM-area">
									<div class="inline static">ALARM_EXIST = 1</div>
									<button class="plus-btn" id="ALARM-status-btn" onclick='insertElement("ALARM-area", this)'> + </button>
								</div>
							</td>
							<td>
								<button onclick='checkExpression("ALARM")'>저장</button>
							</td>
						</tr>
						<tr>
							<td  class="wait">
								WAIT
							</td>
							<td>
								<div id="WAIT-area">
									<button class="plus-btn" id="WAIT-status-btn" onclick='insertElement("WAIT-area", this)'> + </button>
								</div>
							</td>
							<td>
								<button onclick='checkExpression("WAIT")'>저장</button>
							</td>
						</tr>
					</table>
				</td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
	<div id="input-dialog"></div>
</body>
</html>	