<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">

<link rel="stylesheet" href="${ctxPath }/css/jquery.loading.min.css">
<title>Dash Board</title>

<script type="text/javascript">

const loadPage = () =>{
    /* createMenuTree("maintenance", "Alarm_Manager") */  
    createMenuTree("config", "layout_Setting")
    createMachine()
    
    $("#svg").css({
    	"position" : "relative",
    	
    	"width" : $("#container").width(),
    	"height" : $("#container").height()
    	
    });
}

</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/index.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery.loading.min.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/svg.js/2.6.4/svg.min.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/svg.draggable.min.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>

<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	/* margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%; */
	overflow : hidden;
	background-color: black;
  	/* font-family:'Helvetica'; */
}

</style> 

</head>

<body>
	
	
	
	
	<div id="container">
	<div id="svg"></div>
	</div>
</body>
</html>	
