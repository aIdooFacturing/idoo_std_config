<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden; 
	background-color: black;
  	font-family:'Helvetica';
}
html{
	overflow : hidden;
}

#grid{
	border-color : #222327;
}

#grid .k-grid-header {
	border-color : #222327;
}

.k-grid-header-wrap {
	border-color : #222327;
}

.k-filter-row .k-filtercell{
	text-align : center;
}


</style> 
<script type="text/javascript">
	
	const loadPage = () =>{
		createMenuTree("im", "importManagement")
	}
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};
	
	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

//	var handle = 0;
	
	$(function(){
//		createNav("inven_nav", 5);
		
//		setEl();
//		time();

		setEl2();
		
		var target_input = $('#keyword'); // 포커스 인풋
	    var chk_short = true;
		 
	    $(document).bind("keydown keyup", function(e) {
	        var key = e.keyCode;
	        var tg = e.target;
	        if(tg.tagName == "INPUT" ||  tg.tagName == "TEXTAREA") return true;
	        
	        var specific = key >= 8 && key <= 46;
	        if(e.type == "keydown") {
	            if(specific) {
	                chk_short = false;
	                return true;
	            }
	            if(!specific && chk_short) {
	            	 $("#transText").focus().select();
	                //target_input.focus().select(); return false;
	            }
	            if(e.ctrlKey && e.keyCode == 86){
	            	 $("#transText").focus().select();
	            }
	        } else {
	            if(specific) {
	                chk_short = true;
	            }
	        }
	    });
	    
	    $("input, textarea").bind("blur", function() {
	        chk_short = true;
	    });
		$("#transText").focus()
		
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
/* 	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	}; */
	
/* 	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : contentWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		
		$(".right").css({
			"width" : contentWidth - $(".left").width()
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#banner").css({
			"font-size" : getElSize(80),
			"width" : getElSize(2000)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
	}; */
	
	
	
	function setEl2(){
		var width = window.innerWidth;
		var height = window.innerHeight;
		
/* 		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		 */
		
		 
		$("#container").css({
			"width" : contentWidth - getElSize(30)
		})
				
		$("#content_table").css({
			"margin-top" : getElSize(300)
		})
				
				
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight,
			"margin-top" : getElSize(250)
		});

		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
/* 		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		}); */
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(22),
			"margin-right" : getElSize(22)
		});
		
		$("select").css({
			"width" : getElSize(380),
			"margin-right" : getElSize(50)
		});
		
		$("input").css({
			"background-color" : "black",
			"border-color" : "black",
			"margin-right" : getElSize(50),
			"height" : getElSize(80)
		})
		
			
		$("#td_first").css({
			"padding-left" : getElSize(50),
			"padding-bottom" : getElSize(10),
			"padding-top" : getElSize(30),
			"padding-bottom" : getElSize(30)
		});
		
		$(".k-header").css({
		    "font-family" : "NotoSansCJKkrBold",
		    "font-size" : getElSize(32) + "px",
		    "background-image" : "none",
		    "background-color" : "#353542"
		}); 
		
		$("#grid").css({
			"margin-left" : getElSize(50),
			"margin-right" : getElSize(50)
		})
		
		$("button").css({
			"padding" : getElSize(25),
			"margin-left" : getElSize(7),
			"margin-right" : getElSize(30),
			"margin-top" : getElSize(0),
			"font-size" : getElSize(47),
			"background" : "#909090",
			"border-color" : "#222327"
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(200),
			"margin-right" : getElSize(80),
 			"height" : getElSize(100)
		});
		
		$("#save").css({
			"cursor" : "pointer",
			"width" : getElSize(200),
			"margin-right" : getElSize(80),
 			"height" : getElSize(100),
 			"vertical-align" : "middle"
		});
		
		$("#banner").css({
			"font-size" : getElSize(80),
			"width" : getElSize(2000)
		});
	
	};
	
	
	
/* 	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	}; */
	
	var companylist=[];
	var prdcomlist=[];
	//업체 리스트(선택박스)
	function getComList(){
		var url = "${ctxPath}/chart/getComList.do";
		
		$.ajax({
			url :url,
			dataType :"json",
			type : "post",
			success : function(data){
				var json = data.dataList;	//업체 총 리스트
				var json1 = data.dataList1;	// 라우팅 걸린 업체 리스트
				console.log(json)
				var option = "<select >";
				$(json).each(function(idx, data){
					var arr=new Object();
					option += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
					arr.prdNo=data.prdNo;
					arr.id=data.id;
					arr.name=decode(data.name);
					companylist.push(arr);
				});
				$(json1).each(function(idx, data){
					var arr=new Object();
					option += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
					arr.prdNo=data.prdNo;
					arr.id=data.id;
					arr.name=decode(data.name);
					arr.afterProj=data.afterProj;
					prdcomlist.push(arr);
				});
			
				option += option + "</select>";
				
				console.log("list--")
				console.log(prdcomlist)
			}
		});
	};
	
	var autoSave = false;
	function getDeliverInfo(evt){
		if(evt.keyCode==13){
			//getTable()
			/* setTimeout(function(){
	    
				$("#transText").focus();
				$("#transText").trigger('click');
				$("#transText").select();
				$("#transText").focus()
	
				$.showLoading()
				
				var grid = $("#grid").data("kendoGrid");
				var rows = grid.dataSource.data();
						
				$(rows).each(function(idx, data){
					var lotNo = data.barcode;
					
					console.log("---비교---")
					console.log(lotNo)
					console.log($("#transText").val())
					console.log(lotNo==$("#transText").val())
					if(lotNo == $("#transText").val()){
						console.log(data)
						grid.select(data);
						//var tr = $(data).closest("tr");
						//$(tr).children("td:nth(0)").children("input").attr("checked", true);
						//console.log()
						data.checkSelect=true
						data.set("checkSelect", true);
					}
				
				});
				$.hideLoading()
				grid.dataSource.fetch()
				autoSave=true;
			},1000); */

			barcodeSaveRow()
		}
	}
	function projView(proj){
		if(proj=="0010"){
			return "R"	
		}else if(proj=="0020"){
			return "MCT"
		}else if(proj=="0030"){
			return "CNC"
		}else if(proj=="0040"){
			return "도금"
		}else{
			return proj
		}
	}
	function checkRow(e){
		console.log("event click")
		var grid = $("#grid").data("kendoGrid");
		var gridList=grid.dataSource.data();
		var dataItem = $(e).closest("tr")[0].dataset.uid;
		 var row = $("#grid").data("kendoGrid")
         .tbody
         .find("tr[data-uid='" + dataItem + "']");
		 var initData = grid.dataItem(row)
		 var barcode = initData.barcode;
		 var length = gridList.length
		 if(initData.checkSelect){
			 initData.checkSelect=false;
//			 grid.dataSource.fetch();
		 }else{
			 initData.checkSelect=true;
//			 grid.dataSource.fetch();
		 }
//		 initData.checkSelect=true;
		 /* for(var i=0; i<length; i++){
			 if(barcode==gridList[i].barcode){
				 if(gridList[i].checkSelect){
					 gridList[i].checkSelect=false;
					 grid.dataSource.fetch();
				 }else{
					 gridList[i].checkSelect=true;
					 grid.dataSource.fetch();
				 }
			 }
		 } */
		 //grid.dataSource.fetch()
	}
	function checkAll(e){
		console.log($("#checkall").is(":checked"))
		if($("#checkall").is(":checked")){
			gridlist=grid.dataSource.data();
        	$(gridlist).each(function(idx,data){
        		data.checkSelect = true;
        	})
		}else{
			gridlist=grid.dataSource.data();
        	$(gridlist).each(function(idx,data){
        		data.checkSelect = false;
        	})
		}
		grid.dataSource.fetch();
	}
	
	
	//barcode 및 enter key 입력시
	function barcodeSaveRow(){
		// 바코드 인식시에는 $가 표기되므로..
		if($("#transText").val().indexOf("$")==-1){
			getTable();
			return;
		}
		
	
		
		var getText;
		var barcodeArr;
//		var barcode="";
		var item="";
		var prdNo="";	//품번
		var cnt="";	//수량
		var searchText="";	//barcode or 반출서 번호
		var beforeProj=""; //현재공정
		var afterProj="";	// 다음공정
		var ITEMNO=""; //대동사(자동차) 품번
		var vndNo="";	//업체 확인하기위해 고객사 번호
		
		getText = $("#transText").val();
		//barcode $ 있을경우
		
		barcodeArr = getText.split("$");
		
		ITEMNO=barcodeArr[0]
		searchText=barcodeArr[1]
		cnt=barcodeArr[2]

		//대동사는 바코드에 마지막에 고객업체 번호 없음 
		if(barcodeArr[3]==undefined || barcodeArr[3]=="undefined"){
			vndNo="0012";
		}else{// 대동사를 제외한 모든 업체는 반출시 뒤에 업체 번호 있음
			vndNo=barcodeArr[3]
		}
		
		console.log()
		
		//반입 전체리스트
		var chklist = kendodata.data()
		var chk=false	//false 면 반출항목없음	true면 리스트있어서 계속 진행

		//반입할 항목이 있는지 확인하기
		for(i=0,len=chklist;i<chklist.length; i++){
			// 품번,바코드,업체명이 같은지 확인
			if(ITEMNO==chklist[i].ITEMNO && searchText==chklist[i].barcode && vndNo==chklist[i].vndNo ){
				chk=true;
				prdNo=chklist[i].prdNo;
				item=chklist[i].item
				beforeProj=chklist[i].proj
				afterProj=chklist[i].afterProj;
				console.log("같은것");
				console.log(chklist[i]);
				if(cnt > chklist[i].stock){
					alert("반출수량보다 반입수량이 많습니다. 수량을 확인해 주세요")
					return
				}
			}
		}
		if(chk==false){
			alert("반출된 항목이 없습니다.")
			return;
		}

		var param = "prdNo=" + prdNo +
					"&vndNo=" + vndNo +
					"&ITEMNO=" + ITEMNO +
					"&item=" + item +
					"&sendCnt=" + cnt +
					"&beforeProj=" + beforeProj +
					"&afterProj=" + afterProj +
					"&barcode=" + searchText;

		console.log(param)
		var url = "${ctxPath}/chart/BarcodeImportMoveTrans.do"
		$.showLoading()
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				if(data=="success"){
					$("#transText").val("")
					getTable();
//					$.hideLoading()
					$("#textTest123").html("<marquee scrollamount='8' loop='1'>"+ item + " [ " + searchText + " ]　" + cnt +" 개가 반입되었습니다." +" </marquee>")
				}else{
					alert("저장에 실패하였습니다");
					$.hideLoading();
					return;
				}
			}
		})
	}
	
	//저장버튼 클릭시
	function saveRow(){
		gridlist = grid.dataSource.data()
		var savelist=[];
		for(i=0,length=gridlist.length; i<length; i++){
			if(gridlist[i].checkSelect==true){
				savelist.push(gridlist[i]);
			}
			if(gridlist[i].stock<gridlist[i].sendCnt || gridlist[i].stock < (Number(gridlist[i].sendCnt)+Number(gridlist[i].fault))){
				kendo.alert("이동수량을 확인해주세요")
				return false;
			}
		}
		
		if(savelist.length==0 && autoSave==false){
			alert("반입할 항목을 선택해주세요")
			setTimeout(function() {
				self.close();
				window.close();
			}, 1000)
			return false;
		}
		

		var obj = new Object();
		obj.val = savelist;
		
		var param = JSON.stringify(obj);
		
		console.log(param)
		
		console.log("수량 :" + savelist[0].sendCnt)
		console.log("불량 :" + savelist[0].fault)
		console.log("비고 :" + decode(savelist[0].remarks))
		var url = "${ctxPath}/chart/importMoveTrans.do"
		$.showLoading()
		$.ajax({
			url : url,
			data :"val=" + param,
			type : "post",
			dataType : "text",
			success : function(data){
				getTable();
			}
		})
		
	}
	
	var kendotable
	var grid
	$(document).ready(function(){
		
		getComList();
		
		kendotable = $("#grid").kendoGrid({
			height: getElSize(1620)
			,dataBound : function(e){
				$("#grid thead tr th").css("font-size",getElSize(44))
				$("#grid thead tr th").css("font-family","NotoSansCJKkrBold")
				$("#grid thead tr th").css("vertical-align","middle")
				$("#grid thead tr th").css("text-align","center")
				
				$("#grid tbody tr td").css("font-size",getElSize(40))
				
			}
			,filterable: {
			      mode: "row"
			}
			,editable : true
			,columns : [{
				field:"checker"
				,title:"<input type='checkbox' id='checkall' onclick='checkAll(this)' style='ext-align:center; width: "+getElSize(50)+"; height: "+getElSize(80)+";'>"
				,template: "<input type='checkbox' onclick='checkRow(this)' #= (typeof checkSelect!='undefined' && checkSelect!=false)? 'checked=checked' : '' # #= (typeof newRow!='undefined') ? \'disabled=checked\' : '' # class='checkbox' style='width: "+getElSize(50)+"; height: "+getElSize(80)+";'/>" 
				,filterable: {
                    cell: {
                        enabled: false
                    }
	             }
				,width:getElSize(120)
			},{
				title : "${com_name}"
					,field : "vndNm"
					,filterable: {
						 cell: {
	                  		 suggestionOperator: "contains"
				             }
				      }
					,width:getElSize(330)
			},{
				title :"${Exportnumber}"
				,field : "barcode"
				,filterable: {
                    cell: {
                        enabled: false
                    }
	             }
				,width:getElSize(300)
			},{
				title : "${prd_no}"
				,field : "item"
				,filterable: {
					 cell: {
                  		 suggestionOperator: "contains"
			             }
			      }
				,width:getElSize(400)
			},{
				title : "${Current_process}"
				,field : "proj"
				,template :"#=projView(proj)#"
				,filterable: {
                    cell: {
                        enabled: false
                    }
	             }
				,width:getElSize(300)
			},{
				title : "${totalCount}"
				,field : "cnt"
				,filterable: {
                    cell: {
                        enabled: false
                    }
	             }
				,width:getElSize(200)
			},{
				title : "${stock_cnt}"
				,field : "stock"
				,filterable: {
                    cell: {
                        enabled: false
                    }
	             }
				,width:getElSize(250)
			},{
				title : "${move_cnt} *"
				,field : "sendCnt"
				,filterable: {
                    cell: {
                        enabled: false
                    }
	             }
				,attributes: {
				      "class": "table-cell"
				 }
				,width:getElSize(200)
			},{
				title : "불량 수량 *"
				,field : "fault"
				,filterable: {
                    cell: {
                        enabled: false
                    }
	             }
				,attributes: {
				      "class": "table-cell"
				 }
				,width:getElSize(220)
			},{
				title : "반출일자"
				,field : "date"
				,filterable: {
                    cell: {
                        enabled: false
                    }
	             }
				,width:getElSize(460)
			}/* ,{
				title : "${bigo}"
				,field : "remarks"
				,template : "#=decode(remarks)#"
				,filterable: {
                    cell: {
                        enabled: false
                    }
	             }
				,attributes: {
				      "class": "table-cell"
				 }
				,width:getElSize(250)
			} */]
		}).data("kendoGrid")		

		getTable()
	})
	
	function getTable(){
		var url = "${ctxPath}/chart/getImportList.do";
		var getText;
		var barcodeArr;
//		var barcode="";
		var prdNo="";	//품번
		var cnt="";	//수량
		var searchText="";	//barcode or 반출서 번호
		getText = $("#transText").val();
		//barcode $ 있을경우
		if(getText.indexOf("$")!=-1){
			barcodeArr = getText.split("$");
			console.log("나는바코드일꺼야")
			console.log(barcodeArr)
			
			prdNo=barcodeArr[0]
			searchText=barcodeArr[1]
			cnt=barcodeArr[2]
			
		}else{
			searchText=$("#transText").val();
		}
		//그냥 일반 반출번호 할경우
		console.log($("#transText").val())
		
		var param = "searchText=" + searchText +
					"&prdNo=" + prdNo ;
//					"&lotNo=" + $("#transText").val();

					
//		$("#checkall")[0].checked=false;
		var json;
		$.showLoading();
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				console.log(data)
				var json= data.dataList
				
				$(json).each(function(idx,data){
					data.sendCnt=data.stock;
					data.vndNm=decode(data.vndNm);
					for(i=0;i<companylist.length;i++){
						if(companylist[i].id==data.vndNo)
							json[idx].vndNm=companylist[i].name
					}
				})
				
				kendodata = new kendo.data.DataSource({
					data: json,
					batch: true,
		/* 			group: { field: "prdNo" },
					sort: [{
		            	field: "prdNo" , dir:"asc" 
		            },{
		            	field: "item" , dir:"asc"
		            }], */
					height: 500,
					schema: {
						model: {
							id: "id",
							fields: {
								checker: { editable: false },
								prdNo: { editable: false },
								item: { editable: false },
								idx: { editable: false },
								proj: { editable: false },
								vndNo: {editable: false },
								vndNm: {editable: false },
								barcode: {editable: false },
								cnt: { editable: false },
								stock: { editable: false },
								sendCnt: { editable: true ,type: 'number'},
								fault: { editable: true ,type: 'number' , validation: { min: 0 ,max : {field : "stock"}} }
							}
						}
					}
				}); 

				kendotable.setDataSource(kendodata);
				grid = $("#grid").data("kendoGrid");
				
				if($("#transText").val()!=""){
					grid = $("#grid").data("kendoGrid");
					var rows = grid.dataSource.data();
							
					$(rows).each(function(idx, data){
						var lotNo = data.barcode;
						
						/* console.log("---비교---")
						console.log(lotNo)
						console.log($("#transText").val())
						console.log(lotNo==$("#transText").val())
						console.log(data) */
						grid.select(data);
						//var tr = $(data).closest("tr");
						//$(tr).children("td:nth(0)").children("input").attr("checked", true);
						//console.log()
						data.checkSelect=true
						data.set("checkSelect", true);
					
					});
					grid.dataSource.fetch()
					autoSave=true;
				}
				
				$.hideLoading()
			}
		})
	}

</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>

	<div id="container">
		<table id="table" style="border-collapse: collapse; ">
			<Tr>
				<td style="width: 100%;height: 100%">
					<div id="textTest123" style="color: white; position: absolute; height: 5%; width: 30%; left: 35%; margin-top: 0.5%;">
					<marquee scrollamount="8" loop="1">
					</marquee>
					</div>
				</td>
			</Tr>
		
			<Tr>
				<td rowspan="10" id="svg_td" style="vertical-align: top; position:relative; background:#26282c;">
					<div id="wrapper">
						<table style="width: 100%">
							<Tr>
								<Td id="td_first" style="text-align: center; vertical-align: middle;">
									<input type="text" id="transText" size="20%" onkeyup="getDeliverInfo(event)" style="color:#ffffff;">
									<button id="search" onclick="getTable()" style="cursor: pointer;"><i class="fa fa-search" aria-hidden="true"></i></button>
									<button id="save" onclick="saveRow()"><spring:message code="save"></spring:message></button>
								</td>
							</tr>
							<tr>
								<td>
									<div id="grid">
									</div>
								</Td>
							</Tr>
						</table>
					</div>
				</td>
			</Tr>
			
		</table>
	 </div>
</body>
</html>	