<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<script type="text/javascript" src="http://jsgetip.appspot.com"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden; 
	background-color: black;
  	font-family:'Helvetica';
  	text-align: center;
}
html{
	overflow : hidden;
}

</style> 
<script type="text/javascript">
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	var handle = 0;
	
	$(function(){
		createNav("inven_nav", 9);
		
		setEl();
		time();
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$("#iframe").css({
			"width" : getElSize(3500),
			"height" : getElSize(1800),
			"position" : "absolute",
			"z-index" : 999,
			"display" : "none"
		});
		
		$("#iframe").css({
			"top" : (originHeight/2) - ($("#iframe").height()/2),
			"left" : (originWidth/2) - ($("#iframe").width()/2),
		});
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : contentWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#banner").css({
			"font-size" : getElSize(80),
			"width" : getElSize(2000)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		$("#grid thead tr th").css("font-size",getElSize(40))
		$("#grid tbody tr td").css("font-size",getElSize(45))
		
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	function insertRow(){
/* 		 grid.dataSource.add({
	         "dvcId" : 0
	     });   */
	     grid.dataSource.insert(0, {
	    	checkSelect: true,
			date: "미저장 데이터",
			insertdata: "insert",
			vndNo:0
		 })
	}
	
	var finishPrdNo;
	function finishPrdNo(){
		var url = "${ctxPath}/chart/getbertmstmatno.do";
		var param ;
		var json;
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				finishPrdNo=data.dataList;
				console.log(finishPrdNo)
			}
		});
	}
	var companylist=[];
	var prdcomlist=[];
	//업체 리스트(선택박스)
	function getComList(){
		var url = "${ctxPath}/chart/getComList.do";
		
		$.ajax({
			url :url,
			dataType :"json",
			type : "post",
			success : function(data){
				var json = data.dataList;	//업체 총 리스트
				var json1 = data.dataList1;	// 라우팅 걸린 업체 리스트
				console.log(json)
				var option = "<select >";
				$(json).each(function(idx, data){
					var arr=new Object();
					option += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
					arr.prdNo=data.prdNo;
					arr.id=data.id;
					arr.name=decode(data.name);
					companylist.push(arr);
				});
				$(json1).each(function(idx, data){
					var arr=new Object();
					option += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
					arr.prdNo=data.prdNo;
					arr.id=data.id;
					arr.name=decode(data.name);
					arr.afterProj=data.afterProj;
					prdcomlist.push(arr);
				});
			
				option += option + "</select>";
				
				console.log("list--")
				console.log(prdcomlist)
			}
		});
	};
	//delbert 2017-09-27 15:40 업체명 이름으로변환
	function changename(abc){
		var name=abc;
		for(i=0;i<companylist.length;i++){
			if(companylist[i].id==abc)
				name=companylist[i].name
		}
		if(name==0){
			return "--선택--"
		}
		return name;
	}
	//업체명 리스트 뽑기 kendo 170925 delbert
	function dropdownlist(container, options){
//		prdcomlist
		var prdlist=[];
	
		for(i=0,length=prdcomlist.length; i<length; i++){
			if(prdcomlist[i].prdNo==options.model.item){
				prdlist.push(prdcomlist[i]);
			}
		}
			
		$('<input required name="' + options.field + '" />')
		.appendTo(container)
		.kendoDropDownList({
			autoWidth: true,
			autoBind: false,
			dataTextField: "name",
			height: 3300,
			dataValueField: "id",
			dataSource: prdlist,
			value:prdlist[0],
		});
		
	}
	
	function exportPrint(){
		var barCode=new Date()
		a=addZero(String(barCode.getFullYear()))
		b=addZero(String(barCode.getMonth()+1))
		c=addZero(String(barCode.getDate()))
		d=addZero(String(barCode.getHours()))
		e=addZero(String(barCode.getMinutes()))
		f=addZero(String(barCode.getSeconds()))
		barCode = a+b+c+d+e+f
		console.log(barCode)

		var savelist=[];
		var gridlist=grid.dataSource.data();
		for(i=0,length=gridlist.length; i<length; i++){
			var sumCnt=0;
			if(gridlist[i].checkSelect==true && gridlist[i].insertdata!="insert"){
				if(gridlist[i].vndNo==0){
					alert("업체를 선택해주세요")
					return false
				}else if(gridlist[i].sendCnt==null){
					alert("반출수량을 입력해주세요")
					return false;
				}else if(gridlist[i].sendCnt > gridlist[i].stockCnt){
					alert("재고보다 이동수량이 많을수 없습니다.")
					return false;
				}
				for(j=0,len=gridlist.length; j<len; j++){
					if(gridlist[j].item==gridlist[i].item){
						sumCnt+=gridlist[j].sendCnt
					}
				}
				if(gridlist[i].sendCnt > sumCnt){
					alert("재고보다 이동수량이 많을수 없습니다.")
					return false;
				}
				gridlist[i].beforeProj=gridlist[i].proj
				
				gridlist[i].deliveryNo=barCode
				gridlist[i].barcode=gridlist[i].shipLot
				gridlist[i].action=undefined
				gridlist[i].checker=undefined
				gridlist[i].beforeProj="0005"
				for(j=0;j<companylist.length;j++){
					if(companylist[j].id==gridlist[i].vndNo){
						gridlist[i].vndNm=encodeURI(companylist[j].name)
					}
				}
				console.log(gridlist[i].vndNm)
				
				for(j=0,len=prdcomlist.length;j<len;j++){
					if(gridlist[i].item==prdcomlist[j].prdNo && gridlist[i].vndNo==prdcomlist[j].id){
						gridlist[i].afterProj=prdcomlist[j].afterProj
					}
				}
				
				savelist.push(gridlist[i])
				
			}
		}
		
		if(savelist.length==0){
			alert("반출할 항목을 선택해주세요.")
			return false;
		}

		var url = "${ctxPath}/chart/createBarcode.do";
		$.ajax({
			url : url,
			data : "deliveryCd=" + barCode,
			type :"post",
			dataType : "text",
			success :function(data){
				if(data == "success"){
					checkedIds = {};
					console.log(savelist)
					console.log(JSON.stringify(savelist))
					$("#iframe").attr("src", "${ctxPath}/chart/printExport.do?json=" + encodeURIComponent(JSON.stringify(savelist))).css({
						"display" : "inline",
						"background-color" : "white"
					});
					
					var close_btn = document.createElement("img");
					close_btn.setAttribute("src", "${ctxPath}/images/close_btn.png");
					close_btn.setAttribute("id", "close_btn");
					
					$("body").append(close_btn);						
											
					$("#close_btn").css({
						"width" : getElSize(100) + "px",
						"height" : getElSize(100) + "px",
						"position" : "absolute",
						"cursor" : "pointer",
						"top" : $("#iframe").offset().top,
						"left" : $("#iframe").offset().left + $("#iframe").width() - getElSize(100),
						"z-index" : 9999
					}).click(function(){
						$(this).remove();
						$("#print_btn").remove();
						$("#iframe").css("display" , "none")
					})
							
					
					var print_btn = document.createElement("img");
					print_btn.setAttribute("src", "${ctxPath}/images/printer.png");
					print_btn.setAttribute("id", "print_btn");
					
					$("body").append(print_btn);						
											
					$("#print_btn").css({
						"width" : getElSize(100) + "px",
						"height" : getElSize(100) + "px",
						"position" : "absolute",
						"cursor" : "pointer",
						"top" : $("#iframe").offset().top,
						"left" : $("#iframe").offset().left + $("#iframe").width() - getElSize(250),
						"z-index" : 9999
					}).click(function(){
						var frm = document.getElementById("iframe").contentWindow;
						frm.focus();// focus on contentWindow is needed on some ie versions
						frm.print();	
					})
					saveRowExport(savelist)
				}
			}
		});
		
	}
	
	function saveRow(){
		var gridlist = grid.dataSource.data()
		var savelist=[]
		console.log(gridlist)
		for(i=0,length=gridlist.length; i<length; i++){
			for(j=0, len=itemlist.length; j<len; j++){
				if(gridlist[i].item==itemlist[j].matNo){
					gridlist[i].RW=itemlist[j].prdNo;
				}
			}
		}
		
		for(i=0,length=gridlist.length; i<length; i++){
			var sumCnt=0;
			if(gridlist[i].checkSelect==true && gridlist[i].insertdata=="insert"){
				if(gridlist[i].item==undefined){
					alert("완성품번을 선택해주세요")
					return 
				}else if(gridlist[i].shipLot==undefined){
					alert("출하로트번호를 입력해주세요")
					return 
				}else if(gridlist[i].cnt==undefined){
					alert("타각수량을 입력해주세요")
					return 
				}else if(gridlist[i].cnt<=0){
					alert("수량을 0개 이상 입력하세요")                             
					return;
				}
				else if(gridlist[i].cnt > gridlist[i].stockCnt){
					alert("타각수량이 재고수량보다 많을수 없습니다.")
					return;
				}
				
				
				for(j=0,len=gridlist.length; j<len; j++){
					if(gridlist[j].RW==gridlist[i].RW && gridlist[j].insertdata=="insert" && gridlist[i].insertdata=="insert"){
						sumCnt+=gridlist[j].cnt
					}
				}
				if(gridlist[i].stockCnt < sumCnt){
					alert("재고보다 이동수량이 많을수 없습니다.")
					return false;
				}
				console.log(gridlist[i].cnt)
				console.log(sumCnt)
				savelist.push(gridlist[i])
				
			}
		}

		if(savelist.length==0){
			alert("항목을 추가하세요")
			return false;
		}
		
		var obj = new Object();
		obj.val = savelist;
		
		var param = JSON.stringify(obj);
		
		console.log(param)
		var url = "${ctxPath}/chart/saveTattoo.do"
		$.showLoading()
		$.ajax({
			url : url,
			data :"val=" + param,
			type : "post",
			dataType : "text",
			success : function(data){
				if(data=="success"){
					kendo.alert(savelist.length + "개의 List 저장");
					getTable();
				}else{
					kendo.alert("저장에 실패했습니다.");
					getTable();
				}
			}
		})
		
	}
	
	function saveRowExport(savelist){
		
		var obj = new Object();
		obj.val = savelist;
		
		var param = JSON.stringify(obj);
		var url = "${ctxPath}/chart/exportMoveTrans.do"
		$.showLoading()
		$.ajax({
			url : url,
			data :"val=" + param,
			type : "post",
			dataType : "text",
			success : function(data){
				console.log(data)
				kendo.alert(savelist.length + "개의 List 저장");
				getTable();
			}
		})
		console.log("--반출--")
		console.log(savelist)
	}
	
	function readOnly(container, options){
		container.removeClass("k-edit-cell");
        container.text(options.model.get(options.field));
	}
	
	function checkRow(e){
		console.log("event click")
		var grid = $("#grid").data("kendoGrid");
		var gridList=grid.dataSource.data();
		var dataItem = $(e).closest("tr")[0].dataset.uid;
		 var row = $("#grid").data("kendoGrid")
         .tbody
         .find("tr[data-uid='" + dataItem + "']");
		 var initData = grid.dataItem(row)
		 var length = gridList.length
		 
		 if(initData.checkSelect){
			 initData.checkSelect=false;
			 grid.dataSource.fetch();
		 }else{
			 initData.checkSelect=true;
			 grid.dataSource.fetch();
		 }
		 
		 //grid.dataSource.fetch()
	}
	function checkAll(e){
		console.log($("#checkall").is(":checked"))
		if($("#checkall").is(":checked")){
			gridlist=grid.dataSource.data();
        	$(gridlist).each(function(idx,data){
        		data.checkSelect = true;
        	})
		}else{
			gridlist=grid.dataSource.data();
        	$(gridlist).each(function(idx,data){
        		data.checkSelect = false;
        	})
		}
		grid.dataSource.fetch();
	}

	var itemlist =[];
	function getItem(){
		var url = "${ctxPath}/chart/getItem.do";
		var param; /* = "prdNo=" + $("#group").val() + 
					"&lotNo=" + $("#transText").val(); */

//		$("#checkall")[0].checked=false;
		var json;
		$.showLoading();
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				itemlist = json;
				console.log(json)
			}
		})
	}
	
	function checkMstmat(container, options){
		$('<input name="' + options.field + '" />')
		 .appendTo(container)
		 .kendoDropDownList({
			 valuePrimitive: true,
			 autoWidth: true,
			 height: getElSize(1300),
			 dataTextField : "matNo",
			 dataValueField  : "matNo",
			 dataSource: itemlist,
			 change: function(e){
				 var prdNo
            	 for(i=0,len=itemlist.length; i<len; i++){
            		if(options.model.item==itemlist[i].matNo){
            			if(itemlist[i].prdNo!=""){
                			prdNo=itemlist[i].prdNo
            			}else{
            				prdNo=itemlist[i].matNo
            			}
            			break;
            		}
            	 }
				console.log(prdNo)
				var kendolist = kendotable.dataSource.data(); 
				var minusCnt=0;
				for(i=0,len=itemlist.length; i<len; i++){
            		if(prdNo==itemlist[i].prdNo){
            			console.log("idx :"+i)
            			for(j=0,length=kendolist.length; j<length; j++){
            				if(kendolist[j].item=="FS_RR_RH"){
            					console.log("---비교---")
                				console.log(kendolist[j].item+" , "+itemlist[i].matNo)
                				console.log(kendolist[j].cnt)
                				console.log(kendolist[j].idx)
            					console.log("---End---")
            				}
							if(kendolist[j].item==itemlist[i].matNo && kendolist[j].cnt !=undefined && kendolist[j].idx !=undefined){
						        minusCnt = minusCnt + kendolist[j].cnt
						        console.log(kendolist[j].cnt)
							}
					    }
            		}
            	 }
				
				var url = "${ctxPath}/chart/stockTotalCntCheck.do";
				var param = "prdNo=" + prdNo +
							"&proj=" + "0005"
//							"&proj=" + options.model.proj
				
				var str;
				$.ajax({
					url : url,
					data : param,
					async :false,
					type : "post",
					dataType : "json",
					success : function(data){
						console.log(data.dataList)
						if(data.dataList.length!=0){
							options.model.set("stockCnt",(data.dataList[0].cnt-minusCnt))
						}else{
							options.model.set("stockCnt",0)
						}
					}
				});
			 }
		 }).data("kendoDropDownList");

	}
	
	var kendotable;
	var beforeValue;
	$(document).ready(function(){
		finishPrdNo();
		getComList();
		getItem();
		
		kendotable = $("#grid").kendoGrid({
			height : getElSize(1580),
			editable : true,
			beforeEdit : function(e){
				console.log("들어올때")
				console.log(e.model.vndNo)
				beforeValue = e.model.vndNo
			},
			dataBound : function(e){
				$("#grid thead tr th").css("font-size",getElSize(40))
				$("#grid tbody tr td").css("font-size",getElSize(45))
				$("#grid thead tr th").css("text-align","center")
			},
			cellClose : function(e) {
				console.log("나갈때")
				console.log(e)
				console.log(e.model.vndNo)
				if(e.model.vndNo!=beforeValue){
					e.model.set("checkSelect",true)
					console.log("다름;")
					var grid = $("#grid").data("kendoGrid");
					grid.dataSource.fetch();	
				}
			},
			columns : [{
				field:"checker"
				,title:"<input type='checkbox' id='checkall' onclick='checkAll(this)' style='width: "+getElSize(80)+"; height: "+getElSize(80)+";'>"
				,template: "<input type='checkbox' onclick='checkRow(this)' #= (typeof checkSelect!='undefined' && checkSelect!=false)? 'checked=checked' : '' # #= (typeof newRow!='undefined') ? \'disabled=checked\' : '' # class='checkbox' style='width: "+getElSize(80)+"; height: "+getElSize(80)+";'/>" 
			},{
				title : "완성품번 *"
				,field : "item"
				,editor : checkMstmat
				,editable: function (dataItem) {
			          return dataItem.date === "미저장 데이터";
			    }
				,attributes: {
				      "class": "table-cell"
				 }
			},{
				
				title:'${prd_no}'
				,field:"ITEMNO"
				,editor : readOnly
				,filterable: {
                    cell: {
                        enabled: false
                    }
	             }
				,width : getElSize(380)
//				,template : "#=itemNoView(item)#"
			},{
				title : "출하로트번호 *"
				,field : "shipLot"
				,editable: function (dataItem) {
			          return dataItem.date === "미저장 데이터";
			    },attributes: {
				      "class": "table-cell"
				 }
			},{
				title : "타각수량 *"
				,field : "cnt"
				,editable: function (dataItem) {
			          return dataItem.date === "미저장 데이터";
			    },attributes: {
				      "class": "table-cell"
				 }
			},{
				title : "재고수량"
				,field : "stockCnt"
				,editor : readOnly
			},{
				title : "반출수량"
				,field : "sendCnt"
				,editable: function (dataItem) {
			          return dataItem.date != "미저장 데이터";
			    },attributes: {
				      "class": "table-cell"
				 }
			},{
				field:"vndNo"
				,title:"${com_name} *"
				,editor : dropdownlist 
				,template:"#=changename(vndNo)#"
				,width:getElSize(350)
				,filterable: {
	                    cell: {
	                        enabled: false
	                    }
	            }
				,attributes: {
				      "class": "table-cell"
				 }
				,width : getElSize(250)
				/* ,attributes: {
   				style: "text-align: center; background-color:black; font-size:" + getElSize(35) +"; color : black;"
   			},headerAttributes: {
   				style: "text-align: center; background-color:black; font-size:" + getElSize(37) + "; color : white;"
   			} */
			},{
				title : "등록시간"
				,field : "date"
			}]
		}).data("kendoGrid")
		getTable();
	});
	
	function test(){
		grid = $("#grid").data("kendoGrid");
	}
	function getTable(){
		var url = "${ctxPath}/chart/getTattooList.do";
		var param; /* = "prdNo=" + $("#group").val() + 
					"&lotNo=" + $("#transText").val(); */

//		$("#checkall")[0].checked=false;
		var json;
		$.showLoading()
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				console.log(data)
				json = data.dataList;
				$(json).each(function(idx,data){
					data.vndNo=0;
					data.sendCnt=data.stockCnt;
					data.item=decode(data.item);
					data.prdNo=decode(data.prdNo);
				})
				
				kendodata = new kendo.data.DataSource({
					data: json,
					batch: true,
		/* 			group: { field: "prdNo" },
					sort: [{
		            	field: "prdNo" , dir:"asc" 
		            },{
		            	field: "item" , dir:"asc"
		            }], */
					height: 500,
					schema: {
						model: {
							id: "id",
							fields: {
								checker : {editable : false},
								proj: { editable: false },
								cnt: { editable: true ,type: 'number' },
								sendCnt: { editable: true ,type: 'number'},
								date : { editable : false}
							}
						}
					}
				}); 
				kendotable.setDataSource(kendodata);
				grid = $("#grid").data("kendoGrid");

				$("#grid thead tr th").css("font-size",getElSize(40))
				$("#grid tbody tr td").css("font-size",getElSize(45))
				$.hideLoading()
			}
		})
	}
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="time"></div>
	<div id="title_right"></div>
	<iframe id="iframe"></iframe>
	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/mainten_left.png" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/purple_right.png" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/selected_purple.png" class='menu_left'>
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top; ">
					<div id="wrapper">
						<table style="width: 100%">
							<Tr>
								<Td style="text-align: center; vertical-align: middle;">
									<button onclick="insertRow()"><spring:message code="add"></spring:message></button>
									<button onclick="saveRow()"><spring:message code="save"></spring:message></button>
									<button onclick="exportPrint()"><spring:message code="exportdocument"></spring:message></button>
									
									<div id="grid">
																			
									</div>
								</Td>
							</Tr>
						</table>
					</div>
				</td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	