<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.common.min.css"/>
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.rtl.min.css"/>
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.silver.min.css"/>
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<script src="https://kendo.cdn.telerik.com/2018.1.221/js/kendo.all.min.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow: hidden;
	background-color: black;
  	font-family:'Helvetica';
}
#wraper{
	height : 100%;
	border-color : #1E1E23;
}
.mainView{
	overflow: hidden;
	overflow-x:hidden;
}

.k-grid-header{
	border-bottom : 1px solid #1E1E23  !important;
	border-left : 1px solid #1E1E23 !important;
}
.k-grid-header-wrap{
	border-right : 1px solid #1E1E23 !important;
}

.k-grid thead tr th{
    background : #353542;
    border-color : #1E1E23;
    color: white;
    text-align: center  !important;
}
.k-grid tbody > tr
{
 background : #DCDCDC ;
 text-align: center;
}
.k-grid tbody > .k-alt
{
 background : #F0F0F0 ;
 text-align: center;
}

.k-grid tbody > tr:hover
{
 background : #6699F0;
}

td {
	border-bottom : 1px solid #1E1E23  !important;
	border-left : 1px solid #1E1E23 !important;
  }
  

.k-header.k-grid-toolbar{
	background: #2B2D32;
	border-color : #2B2D32;
}

.k-button.k-button-icontext.k-grid-add, 
.k-button.k-button-icontext.k-grid-edit,
.k-button.k-button-icontext.k-grid-delete,
.k-button.k-button-icontext.k-grid-cancel{
	background : #9B9B9B;
	border-color : #9B9B9B;
	text-align : center;
	border-radius : 0px;
	color : black;
}

.k-button.k-button-icontext.k-primary.k-grid-update{
	background : #9B9B9B;
	border-color : #9B9B9B;
	text-align : center;
	border-radius : 0px;
	color : black;
}

</style> 
<script type="text/javascript">
	
const loadPage = () =>{
	createMenuTree("config","account_Setting")
}
	
	$(function(){
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999,
			//"pointer-events": "none"
		})

		$("#intro").css({
			"font-size" : getElSize(100)
		});
	
		
		//getToday();
		//setEl();
		
		/* if(window.sessionStorage.getItem("level")!='2'){
			alert("권한이 없습니다.");
			window.location.href='/aIdoo/chart/index.do';
		} */
		
		var lang = window.localStorage.getItem("lang");
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			//"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999,
			//"pointer-events": "none"
		})
		
		
		
		$("#table").css({
			"position" : "absolute",
			"bottom" : marginHeight,
			"height" : getElSize(1750),
			"left" : getElSize(40)	
		});
		
		$("#table2 td").css({
			"text-align" : "center",
			"color" : "white",
			"font-size" : getElSize(35),
			"padding" : getElSize(20),
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		$("#wraper").css({
			"width" : $("#container").width() - getElSize(90)
		})
		
		$(".tmpTable").css({
			"width" : "100%"			
		})
		$("#id-div").css({
			"position" : "absolute",
			"top" : getElSize(300),
			"left" : getElSize(600)
		})
		
		$("#id-input").val(window.sessionStorage.getItem("user_id"));
	
		$("#id-input").css({
			"font-size" : getElSize(48),
			"width" : getElSize(500)
		})
		
		$("#id-label").css({
			"color" : "white",
			"font-size" : getElSize(48)
		})
		
		$("#btn-div").css({
			"position" : "absolute",
			"bottom" : getElSize(300),
			"left" : getElSize(600),
			"width" : getElSize(2950)
		})
		
		$("#delete-btn").css({
			"font-size" : getElSize(48),
			"float" : "right"
		})
		
		$("#change-btn").css({
			"font-size" : getElSize(48)
		})
		
		$(".input-div").css({
			"height" : "50%"
		})
		
		$(".k-header.k-grid-toolbar").css({
			"height" : getElSize(128)
		})
		
		$(".k-button").css({
			"height" : getElSize(80),
			"width" : getElSize(240)
		})
		
		$(".k-header").css({
			"height" : getElSize(96),
			"font-family" : "NotoSansCJKkrBold"
		})
		
		$(".k-grid.td").css({
			"height" : getElSize(96)
		})
		
		
		if(window.localStorage.getItem("lang")=="ko"){
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(100),
				"position" : "absolute",
				"top" : marginHeight + getElSize(85),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else if(window.localStorage.getItem("lang")=="de" || window.localStorage.getItem("lang")=="en"){
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(70),
				"position" : "absolute",
				"top" : marginHeight + getElSize(115),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else{
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(90),
				"position" : "absolute",
				"top" : marginHeight + getElSize(100),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}
		
	}
	
	function showSelector(container, options, a){

		$('<input data-bind="value:' + options.field + '"/>')
		.appendTo(container)
		.kendoDropDownList({
			 dataSource: [
				    { id: 1, name: "${user}" },
				    { id: 2, name: "${manager}" },
		  	],
		  	dataTextField: "name",
		    dataValueField: "id",
	        select:function(e){
	        }
	    }).data("kendoDropDownList");
	}
	
	
	$(function(){
		
		kendodata = new kendo.data.DataSource({
			 transport: {
	            read: {
	            	 url: ctxPath + "/chart/getUserList.do?shopId="+shopId,
	                 dataType: "json",
	                 type: "GET",
	                 data:function(e){
	                	 console.log(e)
	                 }
	            },
	            destroy: {
	            	url: ctxPath + "/chart/deleteUser.do",
	                dataType: "json",
	                type: "POST",
	            },
	            update: {
	            	url: ctxPath + "/chart/updateUser.do",
	            	dataType: "json"
	            },
	            create: {
	            	url: ctxPath + "/chart/createUser.do",
	            	dataType: "json"
	            },
	            parameterMap: function(options, operation) {
	            	console.log(options)
					return {
						models: kendo.stringify(options)
						};
	            }
	        },
	       /*  change: function(e){
	            if(e.action == "itemchange"){
	                e.items[0].dirtyFields = e.items[0].dirtyFields || {};
	                e.items[0].dirtyFields[e.field] = true;
	            }
	        }, */
	        batch: false,
	       schema: {
	           model: {
	               id: "IDX",
	               fields: {
	               	name: { editable: true, nullable: false,  
	               		validation: {
	                        required: true,
	                        maxlength: 10
                    	} 
	        		},
	                password: { editable: true, nullable: false,
	                	validation: {
	                        required: true,
	                        maxlength: 12
                    	}
	                },
	                level: { defaultValue: 1, editable: true, nullable: false, type: "number"},
	                shopId : { defaultValue: 1, type: "number" }
	               }
	           }
	       }
		})
		
		//if(window.sessionStorage.getItem("level")=='2'){
			grid =  $("#wraper").kendoGrid({
				dataSource : kendodata,
				//height : getElSize(1780),
				scrollable:true,
				editable: { 
					mode: "inline",
					confirmation: "${askDeletion}"
				},
				toolbar: [{ name: "create" , text:'ID 추가'}],
				 columns: [
					 {
					    field: "name",
					    title: "${id}",
					    width : getElSize(400),
					    attributes: {
			        	      style: "font-size:"+getElSize(40),
			      	    },headerAttributes: {
			      	    	 style: "font-size:"+getElSize(40),
			      	    }
					  },{
					    field: "password",
					    title: "${password}",
					    width : getElSize(400),
					    attributes: {
			        	      style: "font-size:"+getElSize(40),
			      	    },headerAttributes: {
			      	    	 style: "font-size:"+getElSize(40),
			      	    }
					  },{
					    field: "level",
					    title: "${autority}",
					    template : function(dataItem){
					    	if(dataItem.level==1){
					    		return "${user}"
					    	}else if(dataItem.level==2){
					    		return "${manager}"
					    	}
					    },
					    width : getElSize(400),
					    editor: showSelector,
					    attributes: {
			        	      style: "font-size:"+getElSize(40),
			      	    },headerAttributes: {
			      	    	 style: "font-size:"+getElSize(40),
			      	    }
					  },
					  { command: [{ name: "edit", text: { update: '${modify} ${compl}', edit : '${modify}',cancel: "${modify} ${cancel}",} }, { name: "destroy", text: '${del}' }], title: "&nbsp;", width: getElSize(450) }
				  ]
			 }).data("kendoGrid");
	/* 	}else{
			
		} */
		
	})
		
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	
	<div id="title_right"></div>	
	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td class="mainView" rowspan="10" id="svg_td" style="vertical-align: top;">
					<div id="wraper">
					
					</div>
				</td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	