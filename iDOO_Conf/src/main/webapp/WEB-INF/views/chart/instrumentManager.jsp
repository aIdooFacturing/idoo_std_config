<!-- 
최소한의 화면으로 구성된
시작화면 Template
 -->
 <%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden; 
	background-color: black;
  	font-family:'Helvetica';
}

.tHeader{
	background: beige;	/* #C8c7ED */
	text-align: center;
	width:16%;
}
.tContent{
	width:16%;
}

#detailPopup input[type=text]{
	width: 70%;
	height: 80%;
}

#detailPopup select{
	width: 70%;
	height: 80%;
}

#detailPopup input[type=number]{
	min:0;
	width: 35%;
	height: 80%;
}

#detailKendo.k-grid thead tr th{
	background : beige;
	color: black;
	text-align: center;
}

#detailKendo thead tr:eq(0) th{
	background: red !important;
}

#detailKendo .k-grid-content{
	background: white;
}

#detailKendo.k-grid tbody > tr
{
 background :  #F0F0FA ;
 text-align: center;
 color: black;
}

#detailKendo.k-grid tbody > .k-alt
{
 background :  #F6F6F6 ;
 text-align: center;
}
#detailKendo.k-grid td{
	color:black ;
}

.divLine{
	border:1px solid #C0DEED; 
	text-decoration:none; 
}
.btnCss:hover{
	cursor: pointer;
	background: #6F6F6F !important;
}
.pageBtn:hover{
	cursor: pointer;
	background: black !important;
}
#upLoadBtn{
/* 	margin-top:3%;
	margin-right:4%; */
	margin-right: 3%;
	padding-left : 2%;
	padding-right : 2%;
	cursor: pointer;
	font-size: 100%;
    background-color: #f8585b;
    color:#fff;
/*     border: solid 2px grey; */
    border-radius: 12px;
    text-align: center;
    text-decoration: none;
}
#upLoadBtn:hover{
	background: red;
}
#btnSave{
	width: 40%;
	height: 60%;
	font-size: 250%;
	margin-right: 3%;
	padding-left : 2%;
	padding-right : 2%;
	cursor: pointer;
    background-color: #5B5AFF;
    color:#fff;
    border-radius: 12px;
    text-align: center;
    text-decoration: none;
}
#btnRemove{
	width: 40%;
	height: 60%;
	font-size: 150%;
	margin-right: 3%;
	padding-left : 2%;
	padding-right : 2%;
	cursor: pointer;
    background-color: #DF4D4D;
    color:#fff;
    border-radius: 12px;
    text-align: center;
    text-decoration: none;
}
#btnSave:hover{
	background: #3736FF;
}
#btnRemove:hover{
	background: red;
}

#detailBtn{
	position: absolute;
}

#AllView{
	position: absolute;
}
#pageSelect{
	position: absolute;
}

#detailBtn input:hover{
	background: #A3A0ED !important;
	cursor: pointer;
}
#detailKendo button:hover{
	background: #A3A0ED !important;
	cursor: pointer;
}
</style> 
<script type="text/javascript">

	function getAllselectAjax(){
		//측정기 구분
		getDivision();	
		//측정기 유형
		getType();
		//사용부서
		getDepartment();
		//상태
		getStatus();
		//고정 구분
		getFixTy();
		
		setTimeout(function() {
			$.hideLoading();
		},500)
		
	}
	var kendotable;
	$(function(){
		createNav("quality_nav", 5);
	
		setEl();
		time();
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		$( ".date1" ).datepicker({
		    onSelect : function(e){
		    	//e == 날짜 
		    	$("#chk2").prop("checked",true)
//		    	console.log("dd")
/* 		    	getDeliveryHistory();
		    	$(".date").val(e); */
		    }
	    })
		$( ".date" ).datepicker({
		    onSelect : function(e){
		    }
	    })
	    
	   //모든교정이력 default 날짜 입력
	    $("#AllsDate").val(moment().subtract(6, 'month').format("YYYY-MM-DD"))
	    $("#AlleDate").val(moment().format("YYYY-MM-DD"))
	    
	    //라디오 요소처럼 동작시킬 체크박스 그룹 셀렉터
	    $('input[type="checkbox"][name="SGROUP"]').click(function(){
	        //클릭 이벤트 발생한 요소가 체크 상태인 경우
	        if ($(this).prop('checked')) {
	            //체크박스 그룹의 요소 전체를 체크 해제후 클릭한 요소 체크 상태지정
	            $('input[type="checkbox"][name="SGROUP"]').prop('checked', false);
	            $(this).prop('checked', true);
	        }
	    });

		chkBanner();

		//select box 부르기
		getAllselectAjax();
		
		//index 계측기관리 테이블
		kendotable = $("#kendoTable").kendoGrid({
			sortable:true,
			dataBound:function(e){
				$(".k-grid tbody tr td").css("font-size",getElSize(36));
				$(".k-grid thead tr th").css({
					"text-align": "center",
					"font-size": getElSize(37)
				});
				
				//추가 저장 버튼 css
				$("#detailBtn").css({
					"left": getElSize(2900),
					"top": getElSize(4),
				})
				
			/* 	"font-size": getElSize(80),
			"padding": getElSize(20),
			"background": "linear-gradient(lightgray, gray)",
			"border": "0px solid",
			"color": "white",
			"font-weight": "bolder",
			"border-radius": "5px" */
				

			},
			columns:[/* {
					selectable: true
				},{
					field:"idx",title:"순번"
				}, */{
					field:"code",title:"관리<br>코드",width: getElSize(140)

				},{
					field:"division",
					title:"측정기구분<br>측정기유형",template:"#=decode(division)#<br>#=decode(type)#",width: getElSize(500)
				},{
					field:"name",
					title:"측정기명"/* <br>(상세보기) */,template:"#=decode(name)#"/*<br> (#=decode(CodeName)#) */,width: getElSize(300)
				},{
					field:"standard",
					title:"규격",template:"#=decode(standard)#",width: getElSize(260)
				},{
					field:"lastChk",
					title:"교정주기(개월)<br>최근교정일",template:"#=interval#<br>#=lastChk#",width: getElSize(300)
				},{
					field:"sDate",
					title:"차기 교정"
 					,width: getElSize(250)
				},/* {
//					field:"id",
					title:"게이지R&R주기<br>최근실시일",template:"#=RInterval#<br>#=RlastChk#",width: getElSize(310)
				},{
					field:"id",
					title:"차기게이지<br>R&R"
					,template:"2018-12-04",width: getElSize(260)
				}, */{
					field:"model",title:"모델번호",width: getElSize(250)
				},{
					field:"comName",title:"제조처",width: getElSize(230)
				},{
					field:"date",title:"구매일자",width: getElSize(200)
				},{
					field:"empCd",title:"사용자",width: getElSize(230)
				},{
					field:"status",title:"사용상태",width: getElSize(210)
				},{
					field:"department",title:"사용부서",width: getElSize(280)
				},{
					title:"관리<br>대장"
//			        ,template:"#='<button onclick=insertRow(this)>　dd　</button>'#"
		        	,template:"#=viewTem(this)#",width: getElSize(150)
				}/* ,{
					field:"id",title:"설비",width: getElSize(230)
				},{
					field:"id",title:"적용일자",width: getElSize(230)
				} */]
		}).data("kendoGrid")
		
		//팝업생성시 교정이력 테이블
		detailtable = $("#detailKendo").kendoGrid({
			editable : true,
			navigatable: true,
			height : getElSize(777),
			dataBound:function(){
				
				$(".k-grid tbody tr td").css("font-size",getElSize(36));
				$(".k-grid thead tr th").css({
					"text-align": "center",
					"font-size": getElSize(37)
				});
				
				grid = this;
				grid.tbody.find('tr').each(function(){            
					var item = grid.dataItem(this);
					kendo.bind(this,item);
				})
				console.log("dma..")
				$( ".date" ).datepicker({
					onSelect : function(e){
						var dataItem= $(this).closest("tr")[0].dataset.uid;
			    		var grid = $("#detailKendo").data("kendoGrid")
			    		var row = $("#detailKendo").data("kendoGrid")
			    		      .tbody
			    		      .find("tr[data-uid='" + dataItem + "']");
			    		var initData = grid.dataItem(row)
			    		console.log(initData)
			    		initData.date=e;
			    	}
		    	})
		    	
		    	$("span[name=imgPlus]").off();
		    	$("span[name=imgView]").off();
		    	//성적서 이미지 클릭시 이벤트 발생시키기
		    	$("span[name=imgPlus]").click(function(){
		    		var dataItem= $(this).closest("tr")[0].dataset.uid;
		    		var grid = $("#detailKendo").data("kendoGrid")
		    		var row = $("#detailKendo").data("kendoGrid")
		    		      .tbody
		    		      .find("tr[data-uid='" + dataItem + "']");
		    		var initData = grid.dataItem(row)
		    		
			    	$("#dtImg").off();
		    		$("#dtImg").click();
					$("#dtImg").change(function() {
						dtImgAdd(initData);
					});
//		    		
		    		
				});
		    	//성적서 이미지 클릭시 이벤트 발생시키기
		    	$("span[name=imgView]").click(function(){
		    		var dataItem= $(this).closest("tr")[0].dataset.uid;
		    		var grid = $("#detailKendo").data("kendoGrid")
		    		var row = $("#detailKendo").data("kendoGrid")
		    		      .tbody
		    		      .find("tr[data-uid='" + dataItem + "']");
		    		var initData = grid.dataItem(row)
		    		console.log(initData.img)
		    		if(initData.img==undefined || initData.img=="" || initData.img=="undefined"){
		    			alert("등록된 이미지가 없습니다.");
		    			return;
		    		}else{
						window.open("http://bkjm.iptime.org/barcode/a/"+initData.img,"성적서","width=" + getElSize(2500) + ",height= " + getElSize(1200) + ",top= " + getElSize(500) + ",left="+ getElSize(800))
		    		}
					
				});

		    	//css
		    	$("span[name=imgPlus]").css({
					"cursor":"pointer"
				})
				$("span[name=imgView]").css({
					"cursor":"pointer"
				})
				
				//교정이력 추가 저장 css
				$("#detailBtn input").css({
					"color":"black",
					"border-radius": getElSize(10),
					"font-size": getElSize(50),
					"background": "linear-gradient(#D9E5FF, #B5B2FF)",
					"padding-top": getElSize(5),
					"padding-bottom": getElSize(5),
					"padding-left": getElSize(40),
					"padding-right": getElSize(40),
					"font-size": getElSize(37)
				})
				
				//교정이력 삭제 css
				$("#detailKendo button").css({
					"color":"black",
					"border-radius": getElSize(10),
					"font-size": getElSize(50),
					"background": "linear-gradient(#D9E5FF, #B5B2FF)",
					"padding-top": getElSize(5),
					"padding-bottom": getElSize(5),
					"padding-left": getElSize(40),
					"padding-right": getElSize(40),
					"font-size": getElSize(37)
				})
				
		    },
			columns:[{
				title : "교　정　이　력 <div id='detailBtn'><form id='dtImgForm'><input type='file' id='dtImg' name='file' style='display:none'></form><input type='button' value='추가' onclick='detailAdd()'>　<input type='button' value='저장' onclick='detailSave()'></div>" 
				, columns:[{
					field:"num",title:"순번",template:"#=delTep(num)#"
				},{
					field:"vndNm",title:"교정처 *"
				},{
					field:"date", title:"교정일 *"
					,template:"<input class='date' data-role='text' style='width:100%; text-align: center;'  data-bind='value:date'/>"

				},{
					field:"prc",title:"교정비용"
				},{
					field:"cSize",title:"보정치수"
				},{
					field:"passed",title:"합격여부 *"
				},{
					field:"imgChk",title:"성적서" ,template: "#=imgDraw(imgChk)#"
				},{
					field:"bigo",title:"비고"
				}]
			}]
		}).data("kendoGrid")
		
		//모든교정이력
		recordListTable = $("#recordAll").kendoGrid({
			dataBound:function(e){
				
				$(".k-grid tbody tr td").css("font-size",getElSize(36));
				$(".k-grid thead tr th").css({
					"text-align": "center",
					"font-size": getElSize(37)
				});
				$("span[name=imgPlus]").off();
		    	$("span[name=imgView]").off();
		    	$("span[name=imgDelete]").off();
		    	
		    	//성적서 이미지 클릭시 이벤트 발생시키기
		    	$("span[name=imgPlus]").click(function(){
		    		var dataItem= $(this).closest("tr")[0].dataset.uid;
		    		var grid = $("#recordAll").data("kendoGrid")
		    		var row = $("#recordAll").data("kendoGrid")
		    		      .tbody
		    		      .find("tr[data-uid='" + dataItem + "']");
		    		var initData = grid.dataItem(row)
		    		
			    	$("#dtImg").off();
		    		$("#dtImg").click();
					$("#dtImg").change(function() {
						dtImgAdd(initData);
					});
//		    		
		    		
				});
		    	//성적서 이미지 클릭시 이벤트 발생시키기
		    	$("span[name=imgView]").click(function(){
		    		var dataItem= $(this).closest("tr")[0].dataset.uid;
		    		var grid = $("#recordAll").data("kendoGrid")
		    		var row = $("#recordAll").data("kendoGrid")
		    		      .tbody
		    		      .find("tr[data-uid='" + dataItem + "']");
		    		var initData = grid.dataItem(row)
		    		console.log(initData.img)
		    		if(initData.img==undefined || initData.img=="" || initData.img=="undefined"){
		    			alert("등록된 이미지가 없습니다.");
		    			return;
		    		}else{
						window.open("http://bkjm.iptime.org/barcode/a/"+initData.img,"성적서","width=" + getElSize(2500) + ",height= " + getElSize(1200) + ",top= " + getElSize(500) + ",left="+ getElSize(800))
		    		}
					
				});
		    	//휴지통 이미지 클릭시 이벤트 발생시키기
		    	$("span[name=imgDelete]").click(function(){
		    		var dataItem= $(this).closest("tr")[0].dataset.uid;
		    		var grid = $("#recordAll").data("kendoGrid")
		    		var row = $("#recordAll").data("kendoGrid")
		    		      .tbody
		    		      .find("tr[data-uid='" + dataItem + "']");
		    		var initData = grid.dataItem(row)
		    		
		    		//삭제시키기
		    		deleteRecord(initData)
		    		
				});

		    	//css
		    	$("span[name=imgPlus]").css({
					"cursor":"pointer"
				})
				$("span[name=imgView]").css({
					"cursor":"pointer"
				})
				$("span[name=imgDelete]").css({
					"cursor":"pointer"
				})
			}
			,height:getElSize(1480)
			,sortable:true
			,columns:[{
				field:"code",width:getElSize(200)
				,title:"관리코드"
			},{
				field:"date",width:getElSize(230)
				,title:"교정일자"
			},{
				field:"name",width:getElSize(340)
				,title:"측정기명"
			},{
				field:"model",width:getElSize(300)
				,title:"모델번호"
			},{
				field:"vndNm",width:getElSize(360)
				,title:"교정처"
			},{
				field:"prc",width:getElSize(200)
				,title:"교정비용"
			},{
				field:"cSize",width:getElSize(280)
				,title:"보정치수"
			},{
				field:"passed",width:getElSize(200)
				,title:"합격여부"
			},{
				field:"imgChk",title:"성적서" ,template: "#=imgDraw(imgChk)#",width:getElSize(230)
			},{
				field:"bigo",width:getElSize(500)
				,title:"비고"
			},{
				title:"삭제",width:getElSize(170),template: "<span name='imgDelete' class='k-icon k-i-delete k-i-trash'></span>"
			}]
		}).data("kendoGrid")
		
		//계측항목추가 테이블
		addListTable = $("#addTable").kendoGrid({
			/* filterable: {
				mode: "row"
			}, */
			dataBound:function(e){
				$(".k-header.k-grid-toolbar").css("font-size",getElSize(42));
				$(".k-grid tbody tr td").css("font-size",getElSize(42));
				$(".k-grid thead tr th").css({
					"text-align": "center",
					"font-size": getElSize(45)
				});
				
			}
			,height:getElSize(1750)
			,editable:true
			,toolbar : [{
				template: "분류명 : <input type='text' id='category' class='k-input'>　<a class='k-button' style='background:rgb(250,235,255)' onclick='getSelectTable()'>조회</a>"
			},{
				template: "<a class='k-button' style='background:rgb(250,235,255)' onclick='addSelect()'>추가</a>"
			},{
				template: "<a class='k-button' style='background:rgb(250,235,255)' onclick='saveSelect()'>저장</a>"
			}]
			,columns:[{
				field:"group",title:"순번"
				,filterable: {
                    cell: {
                        enabled: false
                    }
         	    }
			},{
				field:"nameAbb",title:"분류명"
			},{
				field:"name",title:"설비명"
			}]
		}).data("kendoGrid")

		$("#detailPopup").kendoDialog({
/* 			scrollable: true
			, */
			height: getElSize(2000)
			,width: getElSize(3500)
			,title: "측정기 관리 대장"
		 	/* ,content: "<table border=1 style='color:black;'> <tr> <th> Tool Cycle Count </th> <th> RunTime (h) </th> </tr>" +
		 			 "<tr> <td> <input id='preCnt' type='number' value=0> </td> <td>  <input id='preSec' type='number' value=0> </td> </tr> </table>"
			 */,actions: [{
				text: "OK"
				,action: function(e){
				//	alert("popup")
				},
				primary: true
			}]
		});
		$("#detailPopup").data("kendoDialog").close();
		
		getTable();

/* 		$("#detailPopup").keyup(function(e){
			if(e.keyCode==13){
				$("#detailPopup").data("kendoDialog").close();
			}
		}) */
	});
	
	function delTep(num){
		if(num==""){
			return "<button id='delete' onclick=deleteRow(this)>${del}</button>"
		}else{
			return num
		}
	}
	
	function deleteRow(e){
		console.log($(e).closest("tr"))
		var dataItem = $(e).closest("tr")[0].dataset.uid;
		var grid = $("#detailKendo").data("kendoGrid")
		 var row = $("#detailKendo").data("kendoGrid")
        .tbody
        .find("tr[data-uid='" + dataItem + "']");
		 var idx = grid.dataSource.indexOf(grid.dataItem(row));
		 var dataItem = kendodetaildata.at(idx);
		 grid.dataSource.remove(dataItem)
	}
	
	// 모든교정이력 삭제
	function deleteRecord(item){
		console.log(item)
		console.log(item.idx)

		if(confirm("선택하신 교정이력 데이터를 삭제 하시겠습니까? \n삭제시 데이터 복구가 되지 않습니다.")==false)
			return;

		var url = "${ctxPath}/chart/deleteRecordItem.do";
		var param = "idx=" + item.idx ;
		$.ajax({
			url : url,
			data : param,
			type : "post",
			success : function(data){
				if(data=="success"){
					getAllTable();
					alert("삭제가 완료 되었습니다.")		
				}
			}
		})
	}
	
	// 교정이력 성적서 추가
	function dtImgAdd(item){
		console.log(item)
		
		//이미지 파일 이름
		var imgName=$("#dtImg").val();
		//이미지 확장자 확인
		var ext=imgName.substring(imgName.lastIndexOf(".")+1,imgName.length);
		if(chkAdd=="add"){
			alert("데이터를 저장후 이미지를 등록 해주세요.");
			return;
		}
		if(imgName==""){
			alert("업로드할 이미지를 선택하여 주세요.")
			return;
		}else if(ext!="png" && ext!="jpg" && ext!="bmp" && ext!="PNG" && ext!="JPG" && ext!="JPEG"){
			alert("png,jpg,bmp 파일의 형식만 업로드만 가능합니다.");
			return;
		}
		
		if(confirm("이미지를 업로드 하시겠습니까? \n업로드 하는데 시간이 소요될 수 있습니다.")==false)
			return;

		$.showLoading();

		var frm = document.getElementById('dtImgForm');
	    frm.method = 'POST';
	    frm.enctype = 'multipart/form-data';
	  
	    var fileData = new FormData(frm);

	    fileData.append("code",$("#code").val());
	    fileData.append("idx",item.idx);
	    fileData.append("imgTy","detail");

		var url = "${ctxPath}/chart/imgUpload.do";
//		var param = "code=" + $("#code").val() +
//					"fileData=" + new FormData(frm);
		$.ajax({
			url : url,
			data : fileData,
			processData : false,
			contentType: false,
			type : "post",
			success : function(data){
				if(data=="success"){
					console.log(data)
					
					$.showLoading();
					setTimeout(function() {
						if($("#content").css("display")=="block"){
							getTable();
							detailView();
						}else if($("#contentAll").css("display")=="block"){
							getAllTable();
						}
						
						for(i=0,len=kendotable.dataSource.data().length ;i<len;i++){
							console.log("----비교----")
							console.log($("#code").val() + " , " + kendotable.dataSource.data()[i].code)
							console.log($("#code").val()==kendotable.dataSource.data()[i].code)
							if($("#code").val()==kendotable.dataSource.data()[i].code){
								console.log(kendotable.dataSource.data()[i].img);
								$("#Timg img")[0].src="http://bkjm.iptime.org/barcode/a/"+kendotable.dataSource.data()[i].img;
							}
						}
						
						$("#dtImg").val("")
						$.hideLoading();
					}, 1000)
				}else{
					alert("저장에 실패하였습니다");
					$.hideLoading();
				}
			}
		})
	}
	//교정이력 성적서 template
	function imgDraw(chk){
		if(chk){
			return "<span name='imgPlus' class='k-icon k-i-plus-circle'></span>　<span name='imgView' class='k-icon k-i-zoom-actual-size'></span>" 
		}else {
			return "미저장"
		}
	}

	//계측항목추가 에서 항목 추가하기
	function addSelect(){
		console.log("추가")
		var grid = $("#addTable").data("kendoGrid");
		var dataList =  $("#addTable").data("kendoGrid").dataSource.data();
		/* if(grid.dataSource.filter()!=undefined){
			var len = grid.dataSource.filter().filters.length;
			var datajson = grid.dataSource.filter().filters
		} */
		var nameAbb="　";
		var group=$("#category").val();
		var cd=0;	// cd 기본키이기 때문에 현재존재하는 cd 값보다 +1 하여 새로 생성되는것에대해 기본키 겹치지 않도록
		
		$(categoryList).each(function(idx,data){
			console.log(data.id + " , " + $("#category").val())
			console.log(data.id==$("#category").val())
			if(data.id==$("#category").val()){
				nameAbb=data.nameAbb;
			}
		})
		
		//cd 젤큰값 찾기
		$(dataList).each(function(idx,data){
			if(Number(data.cd)>=cd){
				cd=Number(data.cd);
			}
		})
		
		grid.dataSource.add({
			"nameAbb":nameAbb
			,"group":group
			,"cd":cd+1
		})
		
	}
	
	//계측항목추가 에서 저장하기
	function saveSelect(){
		var dataList = addListTable.dataSource.data();
		
		/* for(i=0 ,len=dataList.length; i<len; i++){
			
		}
		 */
		var param = dataList;
		
		var obj = new Object();
		obj.val = dataList;
		
		console.log(dataList)
		console.log(JSON.stringify(dataList));
		var url = "${ctxPath}/chart/saveSelectItem.do";
		var param = "val=" + JSON.stringify(obj);

		$.showLoading();
		$.ajax({
			url : url,
			data : param,
			type : "post",
			success : function(data){
				$.hideLoading();
			}
		})


	}
	
	//측정기 구분
	function getDivision(){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 18;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				var options = "<option value='ALL'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + decode(data.name) + "'>" + decode(data.name) + "</option>";
				});
				
				$(".selBox1").html(options)
			}
		});	
	}

	//측정기 유형
	function getType(){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 19;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				var options = "<option value='ALL'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + decode(data.name) + "'>" + decode(data.name) + "</option>";
				});
				
				$(".selBox2").html(options)
			}
		});	
	}

	//상태
	function getStatus(){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 21;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				var options = "<option value='ALL'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + decode(data.name) + "'>" + decode(data.name) + "</option>";
				});
				$(".selBox3").html(options)
				
			}
		});	
	}
	//고정 구분
	function getFixTy(){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 22;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				var options = "<option value='ALL'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + decode(data.name) + "'>" + decode(data.name) + "</option>";
				});
				$(".selBox4").html(options)
				
			}
		});	
	}
	//사용부서
	function getDepartment(){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 20;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				var options = "<option value='ALL'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + decode(data.name) + "'>" + decode(data.name) + "</option>";
				});
				$(".selBox5").html(options)
				
			}
		});	
	}
	function viewTem(e){
		return "<div onclick = 'detailRow(this)' style='cursor: pointer;'><span class='k-icon k-i-track-changes-enable'></span>";
	}
	function detailRow(e){
		
		chkAdd ="";
		var dataItem = $(e).closest("tr")[0].dataset.uid;
		var grid = $("#kendoTable").data("kendoGrid")
		var row = $("#kendoTable").data("kendoGrid")
		      .tbody
		      .find("tr[data-uid='" + dataItem + "']");
		console.log(row)
		
		var initData = grid.dataItem(row)
		console.log(initData)
		var idx = grid.dataSource.indexOf(grid.dataItem(row));
		console.log(idx)
		
		var imgSrc="";
		if(initData.img==undefined){
			imgSrc = "undefined.jpg";
		}else{
			imgSrc = initData.img
		}
		
		$("#name").val(decode(initData.name));
		$("#code").val(decode(initData.code));
		$("#division").val(decode(initData.division));
		$("#interval").val(decode(initData.interval));
		$("#type").val(decode(initData.type));
		$("#RInterval").val(decode(initData.RInterval));
		$("#purpose").val(decode(initData.purpose));
		$("#useLine").val(decode(initData.useLine));
		$("#standard").val(decode(initData.standard));
		$("#comName").val(decode(initData.comName));
		$("#model").val(decode(initData.model));
		$("#status").val(decode(initData.status));
		$("#VNDNM").val(decode(initData.VNDNM));
		$("#date").val(initData.date);
		$("#department").val(decode(initData.department));
		$("#empCd").val(decode(initData.empCd));
		
		$("#Timg img")[0].src="http://bkjm.iptime.org/barcode/a/"+imgSrc;
		
		$("#code").attr("readOnly",true)
		$("#detailPopup").data("kendoDialog").title("측정기 관리 대장 (수정)")
		$("#detailPopup").data("kendoDialog").open();
		
		detailView();
		
	}
	
	//detail 교정 이력 보여주기
	function detailView(){
		var url = ctxPath + "/chart/detailInstrument.do"
		var param = "code=" + $("#code").val();
		
		$.showLoading();
		
		$.ajax({
			url : url,
			data : param,
//			async : false,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				console.log("--교정이력--")
				console.log(json);
				
				$(json).each(function(idx,data){
					
					data.num = idx+1;
					data.vndNm = decode(data.vndNm);
					data.passed = decode(data.passed);
					data.bigo = decode(data.bigo);
					data.imgChk = true
				})
				
				kendodetaildata = new kendo.data.DataSource({
					data: json,
					batch: true,
					editable: false,
					/* sort: [{
                    	field: "prdNo" , dir:"asc" 
                    },{
                    	field: "item" , dir:"asc"
                    }], */
					schema: {
						model: {
							id: "idx",
							fields: {
								num: { editable: false },
								date: { editable: false},
								imgChk: { editable: false}
							}
						}
					}
				});
				
				detailtable.setDataSource(kendodetaildata);

				
				$.hideLoading();
			},error:function(data){
				console.log("er")
			}
		})
	}
	//전체(관리대장) 이미지 업로드 시키기
	function imgUpload(){
		//이미지 파일 이름
		var imgName=$("#imgFile").val();
		//이미지 확장자 확인
		var ext=imgName.substring(imgName.lastIndexOf(".")+1,imgName.length);
		if(chkAdd=="add"){
			alert("데이터를 저장후 이미지를 등록 해주세요.");
			return;
		}
		if(imgName==""){
			alert("업로드할 이미지를 선택하여 주세요.")
			return;
		}else if(ext!="png" && ext!="jpg" && ext!="bmp" && ext!="PNG" && ext!="JPG" && ext!="JPEG"){
			alert("png,jpg,bmp 파일의 형식만 업로드만 가능합니다.");
			return;
		}
		
		if(confirm("이미지를 업로드 하시겠습니까? \n업로드 하는데 시간이 소요될 수 있습니다.")==false)
			return;

		$.showLoading();

		var frm = document.getElementById('fileLoading');
	    frm.method = 'POST';
	    frm.enctype = 'multipart/form-data';
	  
	    var fileData = new FormData(frm);

	    fileData.append("code",$("#code").val());
	    fileData.append("imgTy","basic");

		var url = "${ctxPath}/chart/imgUpload.do";
//		var param = "code=" + $("#code").val() +
//					"fileData=" + new FormData(frm);
		$.ajax({
			url : url,
			data : fileData,
			processData : false,
			contentType: false,
			type : "post",
			success : function(data){
				if(data=="success"){
					console.log(data)
					getTable();
					
					$.showLoading();
					setTimeout(function() {
						for(i=0,len=kendotable.dataSource.data().length ;i<len;i++){
							console.log("----비교----")
							console.log($("#code").val() + " , " + kendotable.dataSource.data()[i].code)
							console.log($("#code").val()==kendotable.dataSource.data()[i].code)
							if($("#code").val()==kendotable.dataSource.data()[i].code){
								console.log(kendotable.dataSource.data()[i].img);
								$("#Timg img")[0].src="http://bkjm.iptime.org/barcode/a/"+kendotable.dataSource.data()[i].img;
							}
						}
						$.hideLoading();
					}, 1000)
				}else{
					alert("저장에 실패하였습니다");
					$.hideLoading();
				}
			}
		})
		
	}
	
	function resetTable(){
		$("#name").val("")
		$("#code").val("")
		$("#division").val("")
		$("#interval").val("")
		$("#type").val("")
		$("#RInterval").val("")
		$("#purpose").val("")
		$("#standard").val("")
		$("#comName").val("")
		$("#model").val("")
		$("#status").val("")
		$("#VNDNM").val("")
		$("#date").val("")
		$("#department").val("")
		$("#empCd").val("")
		$("#purpose").val("")
		$("#Timg img")[0].src="http://bkjm.iptime.org/barcode/a/undefined.jpg";
	}
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		$("#content").css({
			"height" : getElSize(1765),
			"width" : getElSize(3340),
			"background" : "red",
		  	"position": "absolute",
		  	"float": "left"
		})
		$("#contentAll").css({
			"height" : getElSize(1765),
			"width" : getElSize(3340),
			"background" : "red",
		  	"position": "absolute",
		  	"float": "left",
		  	"display":"none"
		})
		$("#contentUp").css({
			"height" : getElSize(1765),
			"width" : getElSize(3340),
			"background" : "black",
		  	"position": "absolute",
		  	"float": "left",
		  	"display":"none"
		})
		$("#selectSearch").css({
			"height" : getElSize(280),
			"width" : getElSize(3340),
		  	"float": "left",
		})
			
		$("#recordSearch").css({
			"height" : getElSize(280),
			"width" : getElSize(3340),
		  	"float": "left",
		})
		
		$("#recordAll").css({
		  	"float": "left",
		})
		
		$(".div1").css({
			"height" : getElSize(140),
			"width" : getElSize(3340),
			"background" : "#393939",
		  	"vertical-align": "middle",
	  		"display": "table-cell",
	  		"font-size": getElSize(40)
		})
		

		$(".div2").css({
			"height" : getElSize(140),
			"width" : getElSize(3340),
			"background" : "#4B4B4B",
		  	"vertical-align": "middle",
	  		"display": "table-cell",
	  		"font-size": getElSize(40)
		})
		
		$(".div1 select").css({
		/* 	"width" : getElSize(300),
			"height" : getElSize(80), */
			"font-size": getElSize(32),
			"padding-top": getElSize(10),
			"padding-bottom": getElSize(10)
		});

		$(".div1 input").css({
			"font-size": getElSize(32),
			"padding-top": getElSize(10),
			"padding-bottom": getElSize(10)
		});
		
		$("input[type=checkbox]").css({
			"width" : "1.5%",
			"height" : "40%",
			"vertical-align": "middle",
/* 			"margin-top" : "2%" */
		})
			
		$(".div2 input").css({
			"font-size": getElSize(32),
			"padding-top": getElSize(10),
			"padding-bottom": getElSize(10)
		});
		
		$(".div3").css({
			"position": "absolute",
			"margin-left": getElSize(2885),
			"margin-top": getElSize(67),
			"font-size": getElSize(30),
		})
		$(".div3 input").css({
			"width" : getElSize(190),
			"height" : getElSize(150),
			"font-size": getElSize(80),
			"padding": getElSize(20),
			"background": "linear-gradient(lightgray, gray)",
			"border": "0px solid",
			"color": "white",
			"font-weight": "bolder",
			"border-radius": "5px"
		})

		$(".btnSearch").css("margin-left",getElSize(222))
		
		$("#kendoTable").css({
			"height" : getElSize(1485),
			"width" : getElSize(3340),
//			"background" : "yellow",
		  	"float": "left"
		})
		
		$("#pageSelect").css({
			"top": getElSize(135),
			"width" : "100%"
		})

	 	$(".pageBtn").css({
			"font-size": getElSize(70),
			"padding": getElSize(20),
			"background": pageBtnCss,
			"border": "0px solid",
			"color": "white",
			"font-weight": "bolder",
			"border-radius": "5px"
		})
		
		$("#indexView").css({
			"margin-left": getElSize(30),
		})
		
		$("#AllView").css({
			"margin-left": getElSize(30),
		}) 

		$("#upSelect").css({
			"margin-left": getElSize(525),
		})
		
		$("#detailPopup table:eq(0) tr").css("font-size",getElSize(38))

		$("#detailPopup table:eq(0) tr input").css("font-size",getElSize(38))

		$("#detailPopup table:eq(0) tr input[type='button']").css("font-size",getElSize(60))
		
		$("#detailPopup table:eq(0) tr select").css("font-size",getElSize(38))

		$(".divLine:eq(0)").css("font-size",getElSize(38))
		
		$(".divLine:eq(0) input").css("font-size",getElSize(38))
		
	};
	
	var pageBtnCss = "linear-gradient(black, #321D6E)"
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	// chkAdd 는 현재 저장하는 데이터가 새로 추가되는건지 수정이 되는건지 확인하기 위해
	// 이것을 하지 않을경우에는 DB에서 ON DUPLICATE 를 쓰므로 기존 코드번호가 있는것을 추가할경우 수정되는것을 방지하기 위함..
	var chkAdd="";
	function add(){
		console.log("추가")
		chkAdd ="add"
		//테이블 val값 초기화
		resetTable();
		
		$("#code").attr("readOnly",false)
		$("#detailPopup").data("kendoDialog").title("측정기 관리 대장 (추가)")
		$("#detailPopup").data("kendoDialog").open();

		detailView();
		/* var grid = $("#kendoTable").data("kendoGrid");

		grid.dataSource.insert(0,{
			"division":"",
			"type":"",
			"CodeName":"",
			"name":"",
			"standard":"",
			"interval":"",
			"lastChk":"",
			"RInterval":"",
			"RlastChk":"",
		}) */
	}
	function detailAdd(){
		var grid = $("#detailKendo").data("kendoGrid");
		grid.dataSource.add({
			"num":""
			,"prc":""
			,"img":"undefined"
			,"cSize":""
			,"bigo":""
			,"imgChk":false
		})
	}
	
	function detailSave(){
		
		if(chkAdd=="add"){
			alert("데이터를 저장후 교정이력을 입력 해주세요.");
			return;
		}
		
		var grid = $("#detailKendo").data("kendoGrid");
		var dataItem = grid.dataSource.data();
		var updateData = [];
		var insertData = [];
		console.log("---Save Test---")
		
		// 조건 검사하기
		for(i=0,len=dataItem.length; i<len; i++){
			if(dataItem[i].vndNm==undefined || dataItem[i].vndNm==""){
				alert("교정처를 입력해주세요.")
				return;
			}else if(dataItem[i].date==undefined || dataItem[i].date==""){
				alert("교정일을 입력해주세요.")
				return;
			}else if(dataItem[i].passed==undefined || dataItem[i].passed==""){
				alert("합격여부를 입력해주세요.")
				return;
			}
		}
		
		$(dataItem).each(function(idx,data){
			if(data.num==""){
				data.code=$("#code").val();
				insertData.push(data)
			}else {
				updateData.push(data)
			}
		})
		console.log("신규")
		console.log(insertData)
		console.log("변경")
		console.log(updateData)
		
		var obj = new Object();
		//신규
		obj.valIn = insertData;
		//업데이트
		obj.valUp = updateData;
		
		var url = "${ctxPath}/chart/detailInstrumentSave.do";
		
		var param = "val=" + JSON.stringify(obj) ;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				if(data=="success"){
					detailView();
					alert("저장완료되었습니다.");
				}else{
					alert("저장에 실패하였습니다.");
				}
			}
		})
		
	}
	
	function detailChk(dataItem){
		
	}
	function enterSearch(e){
		if(e.keyCode==13){
			search();
		}
//		console.log(e.keyCode)
	}
	function search(){
		console.log("조회")
		
		if($("#chk2").prop("checked")){
			if($("#sDate").val()=="" || $("#eDate").val()==""){
				alert("시작,끝 날짜를 선택해주세요.")
				return;
			}
		}
					

		getTable();
	}
	
	//관리대장 저장하기
	function savePopup(){
		if($("#name").val()==""){
			alert("측정기명 입력하세요")
			$("#name").focus();
			return;
		}else if($("#code").val()==""){
			alert("관리코드를 입력하세요")
			$("#code").focus();
			return;
		}else if($("#division").val()=="" || $("#division").val()==null || $("#division").val()=="ALL"){
			alert("측정기 구분을 입력하세요")
			$("#division").focus();
			return;
		}else if($("#interval").val()==""){
			alert("교정주기를 입력하세요")
			$("#interval").focus();
			return;
		}else if($("#type").val()==""  || $("#type").val()==null || $("#type").val()=="ALL"){
			alert("측정기 유형을 입력하세요")
			$("#type").focus();
			return;
		}else if($("#RInterval").val()==""){
			alert("게이지R&R 주기를 입력하세요")
			$("#RInterval").focus();
			return;
		}
		
		var url = ctxPath + "/chart/saveInstrument.do"
		var param = "name=" + $("#name").val() +
					"&code=" + $("#code").val() +	
					"&division=" + $("#division").val() +
					"&interval=" + $("#interval").val() +
					"&type=" + $("#type").val() +
//					"&RInterval=" + $("#RInterval").val() +
					"&purpose=" + $("#purpose").val() +
					"&useLine=" + $("#useLine").val() +
					"&standard=" + $("#standard").val() +
					"&comName=" + $("#comName").val() +
					"&model=" + $("#model").val() +
					"&status=" + $("#status").val() +
					"&VNDNM=" + $("#VNDNM").val() +
					"&date=" + $("#date").val() +
					"&department=" + $("#department").val() +
					"&empCd=" + $("#empCd").val() +
					"&chkAdd=" + chkAdd;
					
		$.showLoading()
		$.ajax({
			url : url,
			data : param,
//			dataType : "json",
			type : "post",
			success : function(data){
				console.log(data)
				if(data=="duple"){
					alert("관리코드가 중복될수 없습니다.")
					$.hideLoading();
				}else if(data=="success"){
					getTable();
					chkAdd="";
					alert("저장 완료되었습니다.");
				}else{
					alert("저장에 실패하였습니다.");
					$.hideLoading();
				}
			}
		})
	}
	
	//관리대장 삭제하기
	function deletePopup(){
		if(chkAdd=="add"){
			alert("저장되지 않은 데이터 입니다.");
			return;
		}
		
		if(confirm("관리대장 삭제시 관련된 교정이력 데이터도 모두 삭제 됩니다. \n삭제를 진행하시겠습니까 ?")==false)
			return;
		
		
		var url = ctxPath + "/chart/deleteManagementItem.do"
		var param = "code=" + $("#code").val() ;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			success : function(data){
				if(data=="success"){
					$("#detailPopup").data("kendoDialog").close();
					getTable();
					alert("삭제 완료되었습니다.");
				}
			}
		})
		
	}
	// 모든교정이력 클릭시
	function AllView(){
		//setEl();
		getAllTable();
		//영역 숨기기
		$("#content").css({
			"display":"none",
		});
		$("#contentAll").css({
			"display":"",
		});
		$("#contentUp").css("display","none");
		
		//버튼 색상 변경detailBtn
		$("#upSelect").css({
			"background" : pageBtnCss
		})
		
		$("#indexView").css({
			"background" : pageBtnCss
		})
		
		$("#AllView").css({
			"background" : "darkorchid",
		})

	}
	//계측항목추가 클릭시
	function upSelect (){
		//setEl();
		getSelectTable();
		$("#content").css("display","none");
		$("#contentAll").css("display","none");
		$("#contentUp").css("display","");
		
		$("#upSelect").css({
			"background" : "darkorchid"
		})
		
		$("#indexView").css({
			"background" : pageBtnCss
		})
		
		$("#AllView").css({
			"background" : pageBtnCss
		})
		
	}
	//계측기관리 클릭시
	function indexView (){
		//setEl();
		getAllselectAjax();
		getTable();
		$("#content").css("display","");
		$("#contentAll").css("display","none");
		$("#contentUp").css("display","none");
		$("#upSelect").css({
			"background" : pageBtnCss
		})
		
		$("#indexView").css({
			"background" : "darkorchid",
		})
		
		$("#AllView").css({
			"background" : pageBtnCss
		})
		
	}
	
	//모든교정이력 클릭시
	function getAllTable(){
		var url = ctxPath + "/chart/getRecordItemList.do"
		var param = "codeName=" + $("#codeName").val() +
					"&modelName=" + $("#modelName").val() +
					"&sDate=" + $("#AllsDate").val() +
					"&eDate=" + $("#AlleDate").val +
					"&cs1=" + $("#Allc1").val() +
					"&cs2=" + $("#Allc2").val() ;
		
		$.showLoading()
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				console.log(data.dataList)
				var json = data.dataList;
				$(json).each(function(idx,data){
					data.vndNm=decode(data.vndNm)
					data.passed=decode(data.passed)
					data.name=decode(data.name)
					data.model=decode(data.model)
					data.bigo=decode(data.bigo)
					data.imgChk=true
				})
				recordListdata = new kendo.data.DataSource({
					data: json,
					batch: true,
					editable: false,
					/* sort: [{
                    	field: "prdNo" , dir:"asc" 
                    },{
                    	field: "item" , dir:"asc"
                    }], */
					schema: {
						model: {
							id: "id",
							fields: {
								group: { editable: false },
								nameAbb: { editable: false },
							}
						}
					}
				});
				
				recordListTable.setDataSource(recordListdata);
				
				$.hideLoading()
			}
		})
	}
	
	var categoryList=[];
	var SelectGroup=18;
	//계측항목추가 클릭시
	function getSelectTable(){
		categoryList=[];
		
		var url = ctxPath + "/chart/getSelectItemList.do"
		var param = "index=" + SelectGroup;
		
		$.showLoading()
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				console.log(data.dataList)
				json = data.dataList
				jsoncategory = data.dataList1;
				$(json).each(function(idx,data){
					data.name=decode(data.name);
					data.nameAbb=decode(data.nameAbb)
				})
				$(jsoncategory).each(function(idx,data){
					data.name=decode(data.name);
					data.nameAbb=decode(data.nameAbb)
					if(categoryList.length==0){
						var arr={};
						arr.id=data.group
						arr.nameAbb=data.nameAbb
						categoryList.push(arr);
					}else{
						if(categoryList[categoryList.length-1].id!=data.group){
							var arr={};
							arr.id=data.group
							arr.nameAbb=data.nameAbb
							categoryList.push(arr);
						}
					}
				})
				selectListdata = new kendo.data.DataSource({
					data: json,
					batch: true,
					editable: false,
					/* sort: [{
                    	field: "prdNo" , dir:"asc" 
                    },{
                    	field: "item" , dir:"asc"
                    }], */
					schema: {
						model: {
							id: "id",
							fields: {
								group: { editable: false },
								nameAbb: { editable: false },
							}
						}
					}
				});
				
				addListTable.setDataSource(selectListdata);
				
				console.log(categoryList)
				var dropDown = $("#addTable").find("#category").kendoDropDownList({
		            dataTextField: "nameAbb",
		            dataValueField: "id",
//		            autoBind: false,
//		            optionLabel: "All",
		            dataSource: categoryList,
		            change: function() {
		                var value = this.value();
		                if (value) {
		                	SelectGroup = value;
		                	getSelectTable();
//		                	$("#addTable").data("kendoGrid").dataSource.filter({ field: "group", operator: "eq", value: parseInt(value) });
		                } else {
//		                	$("#addTable").data("kendoGrid").dataSource.filter({});
		                }
		            }
		        });
				
				$.hideLoading();
			}
		});
	}
	function getTable(){
		var url = ctxPath + "/chart/getInstrument.do"
		
		console.log($("#c1").val());
		var param = "index=" + 18 +
					"&cs1=" + $("#c1").val() +
					"&cs2=" + $("#c2").val() +
					"&cs3=" + $("#c3").val() +
					"&cs4=" + $("#c4").val() +
					"&cs5=" + $("#c5").val() +
					"&chk1=" + $("#chk1").prop("checked") +
					"&chk2=" + $("#chk2").prop("checked") +
					"&chk3=" + $("#chk3").prop("checked") +
					"&chk4=" + $("#chk4").prop("checked") +
					"&chk5=" + $("#chk5").prop("checked") +
					"&sDate=" + $("#sDate").val() +
					"&eDate=" + $("#eDate").val() +
					"&searchText=" + $("#searchText").val() ;
		
		$.showLoading()
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				console.log("------");
				console.log(json);
				
				$(json).each(function(idx,data){
					data.model=decode(data.model);
					data.comName=decode(data.comName);
					data.empCd=decode(data.empCd);
					data.status=decode(data.status);
					data.department=decode(data.department);
				})
				
				kendodata = new kendo.data.DataSource({
					data: json,
					batch: true,
					editable: false,
					/* sort: [{
                    	field: "prdNo" , dir:"asc" 
                    },{
                    	field: "item" , dir:"asc"
                    }], */
					schema: {
						model: {
							id: "id",
							fields: {
								prdNo: { editable: false },
							}
						}
					}
				});
				
				kendotable.setDataSource(kendodata);


				$("#indexView").css({
					"background" : "darkorchid",
				})
				
				$.hideLoading()
				
			}
		})
	}
	</script>
</head>
<body>
	
	<div id="time"></div>
	<div id="title_right"></div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/order_left.png" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/purple_right.png" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/selected_purple.png" class='menu_left'>
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top; ">
				<div id="pageSelect">
					<input type="button" value="계측기관리" id="indexView" class="pageBtn" onclick="indexView()"> <input type="button" value="모든교정이력" id="AllView" class="pageBtn" onclick="AllView()"> <input type="button" value="계측항목추가" id="upSelect" class="pageBtn" onclick="upSelect()">
				</div>
<%-- 				<input type="button" id="AllView" onclick="getDetailProduction()" value="${Detail_screen}"> --%>

				<!-- 계측기관리 탭 -->
				<div id="content">
					<div id="selectSearch">
						<div style="display: table;">
							<div class="div1">
								 측정기 구분 : <select id="c1" class="selBox1"></select> 측정기 유형 : <select id="c2" class="selBox2"></select> 　상태 : <select id="c3" class="selBox3"></select> <label><input type="checkbox" id="chk1"> 폐기제외</label> 　교정 구분 : <select id="c4" class="selBox4"></select> 사용부서 : <select id="c5" class="selBox5"></select>
							</div>
						</div>
						<div style="display: table;">
							<div class="div2">
								<label><input type="checkbox" id="chk2"> 교정월 검색 <input type="text" id="sDate" class="date1">~<input type="text" id="eDate" class="date1"></label> 　측정기 검색 : <label><input type="checkbox" id="chk3" name="SGROUP"> 측정기명</label> 　<label><input type="checkbox" id="chk4" name="SGROUP"> 모델번호</label>　 <label><input type="checkbox" id="chk5" name="SGROUP"> 관리코드</label> <input type="text" id="searchText" onkeypress="enterSearch(event)">
							</div>
						</div>
					</div>
					<div class="div3">
						<input type="button" value="추가" onclick="add()" class="btnCss" >　<input type="button" onclick="search()" value="조회"class="btnCss">
					</div>
					<div id="kendoTable">
					</div>
				</div>
				
				<!-- 모든교정이력 탭 -->
				<div id="contentAll">
					<div id="recordSearch">
						<div style="display: table;">
							<div class="div1">
					 			 측정기 구분 : <select id="Allc1" class="selBox1"></select> 측정기 유형 : <select id="Allc2" class="selBox2"></select> 관리코드 검색 <input type="text" id="codeName">　모델번호 <input type="text" id="modelName"> 
							</div>
						</div>
						<div style="display: table;">
							<div class="div2">
								<label>기간 : <input type="text" id="AllsDate" class="date">~<input type="text" id="AlleDate" class="date"></label>
							</div>
						</div>
					</div>
					<div class="div3">
						<input type="button" onclick="getAllTable()" value="조회" class="btnSearch">
					</div>
					
					<div id="recordAll"></div>
				</div>

				<!-- 계측항목추가 탭 -->
				<div id="contentUp">
					<div id="addTable">
					
					</div>
				</div>
				<!-- 
				
				이곳에 필요한 DOM 을 작성합니다.
				실질적 화면에 표시되는 부분.
				
				 -->
				</td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
	<div id ="detailPopup" style="height: 100%;width: 97%; float: left;">
		<div style="height: 50%; width: 60%; float: left;">
			<table border=1 style="height: 100%;width: 100%; color: black; border:1px solid #C0DEED; text-decoration:none; border-collapse: collapse;">
				<tr style="width: 100%">
					<td class="tHeader">측정기명 *</td>
					<td class="tContent" colspan="3">　<input type="text" id="name"></td>
					<td class="tHeader">관리코드 *</td>
					<td class="tContent">　<input type="number" id="code">  <label style="color: red"> (수정X)</label></td>
				</tr>
				<tr>
					<td class="tHeader"> 측정기 구분 *</td>
					<td class="tContent" colspan="3">　<select id="division" class="selBox1"></select><!-- <input type="text" id="division">  --></td>
					<td class="tHeader">교정주기 *</td>
					<td class="tContent">　<input type="number" id="interval"> 개월</td>
				</tr>
				<tr>
					<td class="tHeader"> 측정기 유형 *</td>
					<td class="tContent" colspan="3">　<select id="type" class="selBox2"></select><!-- <input type="text" id="type"> --> </td>
					<td class="tHeader">규격</td>
					<td class="tContent">　<input type="text" id="standard"></td>
<!-- 					<td class="tHeader">게이지R&R주기 *</td>
					<td class="tContent">　<input type="number" id="RInterval"> 개월</td> -->
				</tr>
				<tr>
					<td class="tHeader">사용용도</td>
					<td class="tContent" colspan="3">　<input type="text" id="purpose"></td>
					<td class="tHeader">사용상태</td>
					<td class="tContent">　<select id="status" class="selBox3"></select><!-- <input type="text" id="status"> --></td>
				</tr>
				<tr>
					<td class="tHeader">사용라인</td>
					<td class="tContent" colspan="3">　<input type="text" id="useLine"></td>
					
				</tr>
				<tr>
					<td class="tHeader">제조처</td>
					<td class="tContent">　<input type="text" id="comName"></td>
					<td class="tHeader">모델번호</td>
					<td class="tContent">　<input type="text" id="model"></td>
<!-- 					<td class="tHeader" colspan="2" rowspan="3" style="height: 100%;"> <input type="button" value="저장하기" id="btnSave" onclick="savePopup()"></td> -->
					<td class="tHeader" colspan="2" rowspan="3" style="height: 100%;"> <input type="button" value="저장하기" id="btnSave" onclick="savePopup()"><input type="button" value="삭제" id="btnRemove" onclick="deletePopup()"></td>
				</tr>
				<tr>
					<td class="tHeader">구매처</td>
					<td class="tContent">　<input type="text" id="VNDNM"></td>
					<td class="tHeader">구매일자</td>
					<td class="tContent">　<input type="text" id="date" class="date" readonly="readonly"></td>
				</tr>
				<!-- <tr>
					<td class="tHeader">교정주기</td>
					<td class="tContent">　12개월</td>
					<td class="tHeader">사용상태</td>
					<td class="tContent">　사용</td>
					<td class="tHeader">　게이지R&R 주기</td>
					<td class="tContent">　6개월</td>
				</tr> -->
				<tr>
					<td class="tHeader">부서</td>
					<td class="tContent">　<select id="department" class="selBox5"></select><!-- <input type="text" id="department"> --></td>
					<td class="tHeader">사용자명</td>
					<td class="tContent">　<input type="text" id="empCd"></td>
				</tr>
				
			</table>
		</div>
		<div style="background: white; height: 50%; width: 40%; float: left;">
			<form id="fileLoading"<%--  action="${ctxPath}/chart/imgUpload.do" method="post" enctype="multipart/form-data" --%>>
				<div class="divLine" style="height: 13%;width:100%;width:100%; display: table;">
					<div style="height: 13%;width:100%; display: table-cell; vertical-align: middle; margin: 0;">
						 이미지 변경 : <input type="file" id="imgFile" name="file"> <input type="button" id="upLoadBtn" value="업로드" style="float: right;" onclick="imgUpload()">
					</div>
				</div>
				<div class="divLine" id ="Timg" style="height: 87%; width: 100%">
					<img src="/barcode/a/de12.jpg" width="100%" height="100%">
				</div>
			</form>
			
		</div>
		<div id="detailKendo" style="height: 50%; width: 100%; float: left;">
			
		</div>
	</div>
</body>
</html>	