package com.unomic.cnc;


import java.io.ByteArrayOutputStream;

public class StringUtils
{
	/**
	 * �޽����� ���̸� ��ȯ�Ѵ�.
	 * str �����Ϳ� ���� null ������ ä�����.
	 * @param str
	 * @param len
	 * @return
	 */
	public static String resize(String str, int len)
	{
		char[] idc = new char[len];
		int length = 0;
		if(len<str.length())
		{
			length = len;
		}
		else
		{
			length = str.length();
		}
		
		for(int i=0; i<length; ++i)
		{
			idc[i] = str.charAt(i);
		}
		
		String strt = "";
		for(int i=0; i<len; ++i)
		{
			strt += idc[i];
		}
		return strt;
	}
	
	public static final byte[] Int2Byte(long l)
	{
		byte[] dest = new byte[8];
		
		dest[3] = (byte) (l & 0xFF);
		dest[2] = (byte) ((l >> 8) & 0xFF);
		dest[1] = (byte) ((l >> 16) & 0xFF);
		dest[0] = (byte) ((l >> 24) & 0xFF);
		
		return dest;
	}
	
	/**
	 * Header(Command, Length)�� Byte�� ��ȯ�Ѵ�. 
	 * @param l
	 * @param i
	 * @return
	 */
	public static final byte[] Int2Byte(int l,int i)
	{
		byte[] dest = new byte[8];

		dest[3] = (byte) (l & 0xFF);
		dest[2] = (byte) ((l >> 8) & 0xFF);
		dest[1] = (byte) ((l >> 16) & 0xFF);
		dest[0] = (byte) ((l >> 24) & 0xFF);
		
		dest[7] = (byte)(i & 0xff);
		dest[6] = (byte)((i>>8) & 0xff);
		dest[5] = (byte)((i>>16) & 0xff);
		dest[4] = (byte)((i>>24) & 0xff);

		return dest;
	}
	
	public static final byte[] sendHeaderToByte(int l,int i)
	{
		byte[] dest = new byte[8];

		dest[0] = (byte) (l & 0xFF);
		dest[1] = (byte) ((l >> 8) & 0xFF);
		dest[2] = (byte) ((l >> 16) & 0xFF);
		dest[3] = (byte) ((l >> 24) & 0xFF);
		
		dest[4] = (byte)(i & 0xff);
		dest[5] = (byte)((i>>8) & 0xff);
		dest[6] = (byte)((i>>16) & 0xff);
		dest[7] = (byte)((i>>24) & 0xff);

		return dest;
	}
	
	/**
	 * 
	 * @param b
	 * @return
	 */
	public static final int byteToInt(byte[] b)
	{
		int i;
		i = (int)b[0] & 0xFF;
		i |= ((int)b[1] << 8) & 0xFF00;
		i |= ((int)b[2] << 16) & 0xFF0000;
		i |= ((int)b[3] << 24) & 0xFF000000;
		return i;
	}
	
	/**
	 * ����� Size �κ��� �о int�� �����Ѵ�.
	 * @param b
	 * @return
	 */
	public static final int headerSizeToInt(byte[] b)
	{
		int i;
		i = (int)b[0] & 0xFF;
		i |= ((int)b[1] << 8) & 0xFF00;
		i |= ((int)b[2] << 16) & 0xFF0000;
		i |= ((int)b[3] << 24) & 0xFF000000;
		return i;
	}
	
	/**
	 * ����� Command �κ��� �о int�� �����Ѵ�.
	 * @param b
	 * @return
	 */
	public static final int headerCommandToInt(byte[] b)
	{
		int i;
		i = (int)b[4] & 0xFF;
		i |= ((int)b[5] << 8) & 0xFF00;
		i |= ((int)b[6] << 16) & 0xFF0000;
		i |= ((int)b[7] << 24) & 0xFF000000;
		return i;
	}
	
	/**
	 * ��� 8����Ʈ ���ĸ� �о String���� �����Ѵ�.
	 * @param b
	 * @param i
	 * @return
	 */
	public static final String Byte2String(byte[] b,int i)
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] buff = null;
		
		baos.write(b, 8, i);
		buff = baos.toByteArray();
		
		int j = (int)buff[0];
		baos = new ByteArrayOutputStream();
		baos.write(b, 9, i-1);
		buff = baos.toByteArray();
		String s = "" + j + new String(buff).trim();
		System.out.println("Byte2String : " + s);
		return s;
	}
	
	/**
	 * Content�� ù��° ����Ʈ�� �����Ѵ�.
	 * ù��° ����Ʈ�� result�� �ȴ�.
	 * @return
	 */
	public static final int getResultCode(byte[] b,int i)
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] buff = null;
		
		baos.write(b, 8, i);
		buff = baos.toByteArray();
		
		int j = (int)buff[0];
		return j;
	}
}
