<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden; 
	background-color: black;
  	font-family:'Helvetica';
}
html{
	overflow : hidden;
}
#grid .k-grid-header th.k-header{
	background-color: black;
	color:white;
}
</style> 
<script type="text/javascript">
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		$(".date").val(year + "-" + month + "-" + day);
	};
	
	var handle = 0;
	
	var grid;
	
	
	function getActionList(){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 7;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				$(json).each(function(idx, data){
					var arr={};
					arr.id=data.id;
					arr.name=decode(data.name);
					causeList.push(arr);
				});
				
			}
		});	
	};

	function getResultList(){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 16;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				$(json).each(function(idx, data){
					var arr={};
					arr.id=data.id;
					arr.name=decode(data.name);
					resultList.push(arr);
				});
				
			}
		});	
	};
	
	function causeName(name){
		console.log(name)
		name = Number(name);
		console.log("원인 변경::"+name)
		for(i=0; i<causeList.length; i++){
			if(causeList[i].id==name){
				return causeList[i].name
			}
		}
		return "";
	}
	function resultName(name){
		name = Number(name);
		for(i=0; i<resultList.length; i++){
			if(resultList[i].id==name){
				return resultList[i].name
			}
		}
		return "";
	}
	function workerName(name){
		name = Number(name);
		for(i=0; i<worker.length; i++){
			if(worker[i].id==name){
				return worker[i].name
			}
		}
		return "";
	}
	function getWorkerList(){
		var url = ctxPath + "/common/getWorkerList.do";
		var param = "shopId=" + shopId;
		$.showLoading();
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
				console.log(json)
				$(json).each(function(idx, data){
					var arr={};
					arr.id=data.id;
					arr.name=decode(data.name);
					worker.push(arr);
				});
				
			}
		});
	}
	//원인 선택박스
	function dropdowncauseList(container, options){
		$('<input name="' + options.field + '" />')
		 .appendTo(container)
		 .kendoDropDownList({
			 autoWidth: true,
			 autoBind: false,
			 dataTextField: "name",
			 height: 3300,
			 dataValueField: "id",
			 dataSource: causeList
		});
		console.log(container);
		console.log(options);
	}
	//조치결과 선택박스
	function dropdownresultList(container, options){
		$('<input name="' + options.field + '" />')
		 .appendTo(container)
		 .kendoDropDownList({
			 autoWidth: true,
			 autoBind: false,
			 dataTextField: "name",
			 height: 3300,
			 dataValueField: "id",
			 dataSource: resultList
		});
		console.log(container);
		console.log(options);
	}
	//조치자 선택박스
	function dropdownworkerList(container, options){
		$('<input name="' + options.field + '" />')
		 .appendTo(container)
		 .kendoDropDownList({
			 autoWidth: true,
			 autoBind: false,
			 dataTextField: "name",
			 height: 300,
			 dataValueField: "id",
			 dataSource: worker
		});
		console.log(container);
		console.log(options);
	}
	function dateTimeEditor(container, options) {
		 $('<input id="datePicker" data-text-field="' + options.field + '" data-value-field="' + options.field + '" data-bind="value:' + options.field + '" data-format="' + options.format + '"/>')
       .appendTo(container)
       .kendoDateTimePicker({
       	format:"{0:yyyy-MM-dd HH:mm}"
       });
	}
	
	var causeList=[];
	var resultList=[];
	var worker=[];

	
	$(function(){
		setDate();
		//getGroup();
		getDvcList();
		
		getActionList();
		getWorkerList();
		getResultList();

		
		createNav("mainten_nav", 5);
		
		setEl();
		time();
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
		
		
		
		
		grid = $("#grid").kendoGrid({
			height:getElSize(1675),
			editable:true,
			columns: [{
			    field: "device",
			    title: "${device}" ,
		    	headerAttributes:{
                	style: "text-align: center; color: white !important; font-size:" + getElSize(35) +"px; white-space: initial; background:gray; vertical-align: middle;"
                },attributes: {
                    style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                }
			  }, {
			    field: "name",
			    title: "${worker}" ,
		    	headerAttributes:{
                	style: "text-align: center; color: white !important; font-size:" + getElSize(35) +"px; white-space: initial; background:gray; vertical-align: middle;"
                },attributes: {
                    style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                }
			  }, {
			    field: "solver_Name",
			    title: "${Actioner}" ,
		    	headerAttributes:{
                	style: "text-align: center; color: white !important; font-size:" + getElSize(35) +"px; white-space: initial; background:gray; vertical-align: middle;"
                },attributes: {
                    style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                }
			  }
			  , {
			    field: "report",
			    title: "${Report_content}",
		    	headerAttributes:{
                	style: "text-align: center; color: white !important; font-size:" + getElSize(35) +"px; white-space: initial; background:gray; vertical-align: middle;"
                },attributes: {
                    style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                }
			  }
			  , {
			    field: "regDate",
			    title: "${date_}",
		    	headerAttributes:{
                	style: "text-align: center; color: white !important; font-size:" + getElSize(35) +"px; white-space: initial; background:gray; vertical-align: middle;"
                },attributes: {
                    style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                }
			  },{
	            	title : "원인",
	            	field : "cause",
	            	template : "#=causeName(cause)#",
	            	editor : dropdowncauseList,
	            	width: getElSize(380),
	            	attributes: {
	                    style: "color:black; text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
	                },headerAttributes:{
	                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
	                }
	            },{
	            	title : "조치결과",
	            	field : "result",
	            	template : "#=resultName(result)#",
	            	editor : dropdownresultList,
	            	width: getElSize(360),
	            	attributes: {
	                    style: "color:black; text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
	                },headerAttributes:{
	                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
	                }
	            },{
	            	title : "조치자",
	            	field : "empCd",
	            	editor : dropdownworkerList,
	            	template : "#=workerName(empCd)#",
	            	width: getElSize(210),
	            	attributes: {
	                    style: "color:black; text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
	                },headerAttributes:{
	                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
	                }
	            },{
	            	title : "조치일자",
	            	field : "date"	,
	            	format:"{0:yyyy-MM-dd HH:mm}" ,
	            	editor : dateTimeEditor,
	            	width: getElSize(620),
	            	attributes: {
	                    style: "color:black; text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
	                },headerAttributes:{
	                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
	                }
	            }
			  ],
			  dataBound : function(e){
				  var items = e.sender.items();
					items.each(function (index) {
						var dataItem = grid.dataItem(this);
						if(index%2==0){
							$("#grid tbody tr:eq("+index+") td:eq(5)").css("background","#E7E7E7")
							$("#grid tbody tr:eq("+index+") td:eq(6)").css("background","#E7E7E7")
							$("#grid tbody tr:eq("+index+") td:eq(7)").css("background","#E7E7E7")
							$("#grid tbody tr:eq("+index+") td:eq(8)").css("background","#E7E7E7")
						}else{
							$("#grid tbody tr:eq("+index+") td:eq(5)").css("background","#F6F6F6")
							$("#grid tbody tr:eq("+index+") td:eq(6)").css("background","#F6F6F6")
							$("#grid tbody tr:eq("+index+") td:eq(7)").css("background","#F6F6F6")
							$("#grid tbody tr:eq("+index+") td:eq(8)").css("background","#F6F6F6")
						}
					})
			  }
			}).data("kendoGrid");
		
		
	});
	
	function saveRow(){
		var savelist=[]
		var gridlist=grid.dataSource.data()
		for(i=0,length=gridlist.length; i<length; i++){
			console.log(gridlist[i].date)
			if(gridlist[i].date!="" || isNaN(gridlist[i].date)){
				if(gridlist[i].date.length>10){
				}else{
					var utfYear=addZero(String(gridlist[i].date.getFullYear()));
					var utfMon=addZero(String(gridlist[i].date.getMonth()+1));
					var utfDate=addZero(String(gridlist[i].date.getDate()));
					var utfHours=addZero(String(gridlist[i].date.getHours()));
					var utfMin=addZero(String(gridlist[i].date.getMinutes()));
					var lastDate=utfYear+"-"+utfMon+"-"+utfDate+" "+utfHours+":"+utfMin
					
					gridlist[i].date=lastDate
					console.log("===최종===")
					console.log(lastDate)
						/* var utfDay=gridlist[i].date.substr(0,10); // "2018-04-30"
						var utfTime=gridlist[i].date.substr(11,5); // "09:00"
						var utfDate=new Date(utfDay+" "+utfTime)
						utfDate = new Date(utfDate.setHours(utfDate.getHours()-9)) */
				}
			}

				
			savelist.push(gridlist[i]);
		}
		var obj=new Object();
		obj.val = savelist;

		var url = "${ctxPath}/chart/MaintenanceSave.do";
		var param = "val=" + JSON.stringify(obj)
		console.log(savelist)
		console.log(param)
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			success : function(data){
				alert("저장완료됬습니다")
			}
		});	
		
	}
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(200),
		});
		
		$("#content_table td, #content_table2 td").css({
			"color" : "#BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#delDiv").css({
			"position" : "absolute",
			"width" : getElSize(700),
			"height" :getElSize(200),
			"background-color" : "#444444",
			"color" : "white"
		});
		
		$("#delDiv").css({
			"top" : (window.innerHeight/2) - ($("#delDiv").height()/2), 
			"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
			//"z-index" : -1,
			"display" : "none",
			"border-radius" : getElSize(10),
			"padding" : getElSize(20)
		});
		
		$("#contentDiv").css({
			"overflow" : "auto",
			"width" : $(".menu_right").width()
		});
		
		$("#insertForm").css({
			"width" : getElSize(3000),
			"position" : "absolute",
			"z-index" : 999,
		});
		
		$("#insertForm").css({
			"left" : $(".menu_right").offset().left + ($(".menu_right").width()/2) - ($("#insertForm").width()/2) - marginWidth,
			"top" : getElSize(400)
		});
		
		$("#insertForm table td").css({
			"font-size" : getElSize(70),
			"padding" : getElSize(15),
			"background-color" : "#323232"
		});
		
		$("#insertForm button, #insertForm select, #insertForm input").css({
			"font-size" : getElSize(60),
			"margin" : getElSize(15)
		});
		
		$(".table_title").css({
			"background-color" : "#222222",
			"color" : "white"
		});
		
		$("#contentTable td").css({
			"color" : "white",
			"font-size" : getElSize(50)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	
	var className = "";
	var classFlag = true;
	
	function getDvcList(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		
		var url =  ctxPath + "/chart/getDvcNameList.do";
		var param = "shopId=" + shopId + 
					"&line=ALL" + 
					"&sDate=" + today + 
					"&eDate=" + today + " 23:59:59" + 
					"&fromDashboard=" + fromDashboard;
		
		dvcArray = [];
		
		$.ajax({
			url : url,
			data :param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var list = "<option value='all'>${total}</option>";
				
				$(json).each(function(idx, data){
					dvcArray.push(data.id)
					list += "<option value='" + data.id + "'>" + decodeURIComponent(data.name).replace(/\+/gi," ") + "</option>";					
				});

				$("#group").html(list);
				maintenance_report_list()
			}
		});	
	};
	
	
	 function maintenance_report_list(){
		 	$.showLoading()
			var url = "${ctxPath}/chart/getMaintenanceReportListPageList.do";
			var param = "dvcId=" + $("#group").val()
		    var report_List = new kendo.data.DataSource();
			$.ajax({
				url : url,
				type : "post",
				data : param,
				dataType : "json",
				success : function(data){
					var json = data.dataList;
					var options = "";
					$(json).each(function(idx, data){
						
						var object = {
								"dvcId" : data.dvcId
								,"name" : decode(data.name)
								,"device" : data.device
								,"solver" : data.solver
								,"solver_Name" : decode(data.solver_Name)
								,"lastAlarmMsg" : data.lastAlarmMsg
								,"regDate" : data.regDate
								,"report" : decode(data.report)
								,"alarmCode" : data.alarmCode
								,"cause" : data.cause
								,"result" : data.result
								,"empCd" : data.actionEmpCd
								,"date" : data.actionDate
								,"idx" : data.idx
						}
						report_List.add(object)
					});
					
					grid.setDataSource(report_List)
					
					$("#grid").css({
						"background-color":"black",
						"color" : "white"
					})
					
					$("#grid tr:odd").css({
						"background-color":"rgb(50,50,50)",
					      "color" : "white"
				   });
					
					$("#grid tr:even").css({
					    "background-color":"rgb(33,33,33)",
					    "color" : "white"
					});
					$.hideLoading()
				}
			});
	    }
	
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="delDiv">
		<Center>
			<font><spring:message code="chk_del" ></spring:message></font><Br><br>
			<button id="resetOk" onclick="okDel();"><spring:message code="del" ></spring:message></button>&nbsp;&nbsp;
			<button id="resetNo" onclick="noDel();"><spring:message code="cancel" ></spring:message></button>
		</Center>
	</div>
	
	<div id="time"></div>
	<div id="title_right"></div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/mainten_left.png" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/purple_right.png" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/selected_purple.png" class='menu_left'>
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top; ">
					<table id="contentTable">
						<Tr>
							<td>
								<spring:message code="device"></spring:message>
								<select id="group" onchange="maintenance_report_list()"></select>
								<%-- <spring:message code="op_period"></spring:message> --%>
								<!-- <input type="date" class="date" id="sDate"> ~ <input type="date" class="date" id="eDate"> -->
								<%-- <button> <spring:message code="excel"></spring:message></button> --%>
								<button id="search" onclick="maintenance_report_list()" style="cursor: pointer;"><i class="fa fa-search" aria-hidden="true"></i></button>
								<button id="saveRow" onclick="saveRow()" style="cursor: pointer;">저장</button>
							</td>
						</Tr>
					</table>
					<div id="grid" style="background-color:black;"></div>
					<!-- <div id="wrapper">
						<table style="width: 100%; color: white; text-align: center; border-collapse: collapse;" class="alarmTable" border="1">
						</table>
					</div> -->
				</td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	