<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden; 
	background-color: black;
}
html{
	overflow : hidden;
}

</style> 
<script type="text/javascript">

	const loadPage = () =>{
		createMenuTree("config", "catch_Phrase")	
	}
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

//	var handle = 0;
	
	$(function(){
//		createNav("mainten_nav", 1);
		
//		setEl();
		setEl2();

//		time();

		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
/* 	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	}; */

	function cancelBanner(){
		var url = "${ctxPath}/chart/cancelBanner.do";
		var param = "msg=" + encodeURIComponent($("#banner").val()) + 
					"&rgb=" + $("#color").val() + 
					"&shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			success : function(data){
				window.localStorage.setItem("banner", "false");
				$("#bannerDiv").css("z-index",-9);
				chkBanner();
				
				location.reload();
				/* $("#bannerDiv").css("z-index",-9);
				chkBanner(); */
			}
		});
		
		
		
	}

	function addBanner(){
		var url = "${ctxPath}/chart/addBanner.do";
		var param = "msg=" + encodeURIComponent($("#banner").val()) + 
					"&rgb=" + $("#color").val() + 
					"&shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			success : function(data){
				window.localStorage.setItem("banner", "true");
				location.reload();
				/* $("#bannerDiv").css("z-index",-9);
				chkBanner(); */
			}
		});
	};
	/* function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : contentWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#banner").css({
			"font-size" : getElSize(80),
			"width" : getElSize(2000)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
	}; */
	
	function setEl2(){
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		 $("#container").css({
				"width" : contentWidth - getElSize(30),
				"background-color" : "#26282c"
			})
			
			$("#content_table").css({
				"margin-top" : getElSize(300),
				"background-color" : "#26282c"
			})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#wrapper").css({
			"margin-top" : getElSize(320),
			"margin-bottom" : getElSize(70)
		})
		
/* 		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		}); */
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
/* 		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		}); */
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
			"background-color" : "#858585",
			"color" : "black",
			"border-color" : "#858585",
			"font-size" : getElSize(50),
			"vertical-align" : "middle",
			"text-align" : "center",
			"height" : getElSize(120),
			"width" :  getElSize(250),
		})
		
		
		$("#wrapper").css({
			"vertical-align" : "middle",
			"text-align" : "center"
		})
		
		$("#banner").css({
			"font-size" : getElSize(80),
			"width" : getElSize(2000),
			"vertical-align" : "middle",
			"padding-bottom" : getElSize(10)
  		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
	};
	
	
/* 	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
 */	
 
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>

	<div id="container" style="background:#26282c;">
		<table id="table" style="border-collapse: collapse; background:#26282c;">
			
			<Tr>
				
				<td rowspan="10" id="svg_td" style="vertical-align: top; position:relative; background:#26282c;">
					<div id="wrapper" style="background:#26282c;">
						<table style="width: 100%">
							<Tr>
								<Td style="text-align: center; vertical-align: middle;">
									<input type="text" id="banner" placeholder="문구를 입력하고 확인을 눌러주세요">
									<input type="color" id="color" > 
									<button onclick="addBanner();"><spring:message code="check"></spring:message> </button>
									<button onclick="cancelBanner();"><spring:message code="cancel"></spring:message> </button>
								</Td>
							</Tr>
						</table>							
					</div>
				</td>
			</Tr>
			
			
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	