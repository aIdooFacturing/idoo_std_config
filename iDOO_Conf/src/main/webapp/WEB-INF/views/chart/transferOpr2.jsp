<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden; 
	background-color: black;
}
html{
	overflow : hidden;
}

#grid{
	border-color : #222327;
}

#grid .k-grid-header {
	border-color : #222327;
}

.k-grid-header-wrap {
	border-color : #222327;
}

</style> 
<script type="text/javascript">
	
	const loadPage = () =>{
		createMenuTree("im", "transferOpr2")	
	}

	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

//	var handle = 0;
	
	$(function(){
//		createNav("inven_nav", 3);
		
		selectMenu();
		getPrdNo();
		
//		setEl();
		time();
		
		
		setEl2();
		
		
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	function selectMenu(){
		var url = "${ctxPath}/chart/selectMenu.do";
		var param ;
		var json;
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				transMenu=data.dataList;
				console.log(transMenu)
			}
		});
	}
	//품번 리스트 불러오기
	function getPrdNo(){
		var url = "${ctxPath}/chart/getMatInfo.do";
		var param = "shopId=" + shopId;
		
		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				itemlist=json;
				//prdNos = json;
				
				var choice = {"prdNo" : "선택"}
				prdNos.push(choice)
					
				$(json).each(function(idx, data){
					var prdNo = {"prdNo" : data.prdNo}
					prdNos.push(prdNo)
				});
				
			}
		});
	}
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
/* 	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	}; */
	
	
	/* 
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : contentWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#banner").css({
			"font-size" : getElSize(80),
			"width" : getElSize(2000)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		$("#wrapper").css({
			"height": getElSize(1660),
			"margin": getElSize(58)
		}) 
	}; 
*/

	
	function setEl2(){
	
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		/* 
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		}) */
				
 		
		
/*		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		 */
	
		 $("#table").css({
				"position" : "absolute",
				"width" : $("#container").width(),
				"top" : getElSize(150) + marginHeight,
				"margin-top" : getElSize(220)
			});
		 
		 
		$("#container").css({
			"top" : getElSize(30),
			"width" : contentWidth,
			"margin-top" : getElSize(300),
			"color" : "#222327"
		})
	
		 
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
/* 		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		}); */
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(22),
			"margin-right" : getElSize(22)
		});
		
		$("select").css({
			"width" : getElSize(380),
			"margin-right" : getElSize(50)
		});
		
		$("input").css({
			"background-color" : "black",
			"border-color" : "black",
			"margin-right" : getElSize(50)
		})
		
			
		$("#td_first").css({
			"padding-left" : getElSize(50),
			"padding-bottom" : getElSize(10)
		});
		
		$(".k-header").css({
		    "font-family" : "NotoSansCJKkrBold",
		    "font-size" : getElSize(25) + "px",
		    "background-image" : "none",
		    "background-color" : "#353542",
		    "text-align" : "center"
		}); 
		
		$("#grid").css({
			"margin-left" : getElSize(50),
			"margin-right" : getElSize(50)
		})
		
		$("button").css({
			"padding" : getElSize(25),
			"margin-left" : getElSize(15),
			"margin-right" : getElSize(120),
			"margin-top" : getElSize(7),
			"font-size" : getElSize(47),
			"background" : "#909090",
			"border-color" : "#222327",
			"height" : getElSize(100),
			"text-align" : "center",
			"vertical-align" : "middle"
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(200),
			"margin-right" : getElSize(80)
// 			"height" : getElSize(82)
		});
		
		$("#banner").css({
			"font-size" : getElSize(80),
			"width" : getElSize(2000)
		});
	
	};







/* 	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	}; */
	
	var list=[]
	var arr={}
	arr.value=1
	arr.text="반품"
	list.push(arr);
	var arr={}
	arr.value=2
	arr.text="반납"
	list.push(arr);
	var arr={}
	arr.value=3
	arr.text="재사용"
	list.push(arr);
	var arr={}
/* 	arr.value=4
	arr.text="입고취소"
	list.push(arr); */
	var arr={}
	arr.value=5
	arr.text="완성취소"
	var arr={}
	arr.value=6
	arr.text="출하취소"
	list.push(arr);
	var arr={}
	arr.value=7
	arr.text="클레임"
	list.push(arr);
	
	function checkMstmat(container, options){
		console.log(list)
		$('<input name="' + options.field + '" />')
		 .appendTo(container)
		 .kendoDropDownList({
			 valuePrimitive: true,
			 autoWidth: true,
			 height: 3300,
			 dataTextField : "text",
			 dataValueField  : "text",
			 dataSource: list,
			 change: function(e){
				 if(options.model.trans=="반품"){
					 options.model.set("proj","0080")	//불량창고
					 options.model.set("afterProj","0")	//업체		 
				 }else if(options.model.trans=="반납"){
					 options.model.set("proj","0005")
					 options.model.set("afterProj","0000")					 
				 }else if(options.model.trans=="재사용"){
					 options.model.set("proj","0080")
					 options.model.set("afterProj","--선택--")					 
				 }else if(options.model.trans=="입고취소"){
					 options.model.set("proj","0000")
					 options.model.set("afterProj","0")					 
				 }else if(options.model.trans=="출하취소"){
					 options.model.set("proj","0")
					 options.model.set("afterProj","0090")					 
				 }else if(options.model.trans=="클레임"){
					 options.model.set("proj","0")
					 options.model.set("afterProj","0080")
				 }
				 console.log("change")
				 console.log(options.model.trans)
				 
				 console.log(options.model.prdNo)

				 var prdNo
            	 for(i=0,len=itemlist.length; i<len; i++){
            		if(options.model.prdNo==itemlist[i].prdNo){
            			if(itemlist[i].RWMATNO!=""){
                			prdNo=itemlist[i].RWMATNO
            			}else{
            				prdNo=itemlist[i].prdNo
            			}
            			break;
            		}
            	 }
				console.log(prdNo)
				var url = "${ctxPath}/chart/stockTotalCntCheck.do";
				var param = "prdNo=" + prdNo +
							"&proj=" + options.model.proj
				
				var str;
				$.ajax({
					url : url,
					data : param,
					async :false,
					type : "post",
					dataType : "json",
					success : function(data){
						if(data.dataList.length!=0){
							options.model.set("cnt",data.dataList[0].cnt)
						}else{
							options.model.set("cnt",0)
						}
					}
				});
				 
			 }
		 }).data("kendoDropDownList");
	}
	
	function prdNoauto(container, options, a){
		console.log("dfd")
		autoComplete = $('<input data-bind="value:' + options.field + '"/>')
		.appendTo(container)
		.kendoComboBox({
        	dataSource: prdNos,
			autoWidth: true,
			dataTextField: "prdNo",
			dataValueField: "prdNo",
            placeholder: "품번 선택",
            change:function(e){
            	console.log(options.model.prdNo)
				var item=options.model.prdNo
            	var prdNo
            	for(i=0,len=itemlist.length; i<len; i++){
            		if(options.model.prdNo==itemlist[i].prdNo){
            			if(itemlist[i].RWMATNO!=""){
                			prdNo=itemlist[i].RWMATNO
            			}else{
            				prdNo=itemlist[i].prdNo
            			}
            			break;
            		}
            	}
            	console.log(prdNo)
            	console.log(item)
            	var url = "${ctxPath}/chart/stockTotalCntCheck.do";
        		var param = "prdNo=" + prdNo +
        					"&proj=" + options.model.proj +
        					"&item=" + item +
        					"&afterProj=" + options.model.afterProj;
        		
        		var str;
        		$.ajax({
        			url : url,
        			data : param,
        			async :false,
        			type : "post",
        			dataType : "json",
        			success : function(data){
        				if(data.dataList.length!=0){
							options.model.set("cnt",data.dataList[0].cnt)
						}else{
							options.model.set("cnt",0)
						}
        			}
        		});
            }
        }).data("kendoComboBox");
	}

	function projView(proj){
		if(proj=="0000"){
			return "자재창고"
		}else if(proj=="0005"){
			return "공정창고"
		}else if(proj=="0030"){
			return "CNC외주"
		}else if(proj=="0080"){
			return "불량창고"	
		}else if(proj=="0090"){
			return "출하창고"
		}else if(proj=="0"){
			return "업체"
		}else{
			return proj
		}
	}
	
	function insertRow(){
/* 		 grid.dataSource.add({
	         "dvcId" : 0
	     });   */
	     grid.dataSource.insert(0, {
	    	proj: "",
	    	prdNo: "",
			cause: "　",
			lotNo:"",
			afterProj :"　"
		 })
	}
	
	function saveRow(){
		var gridlist = grid.dataSource.data()
		var savelist=[]
		var prdNochk;
		console.log(gridlist)
		for(i=0,length=gridlist.length; i<length; i++){
			if(gridlist[i].trans=="재사용"){
				if(gridlist[i].afterProj=="자재창고"){
					gridlist[i].afterProj="0000"
				}else if(gridlist[i].afterProj=="공정창고(R)"){
					gridlist[i].afterProj="0010"
				}else if(gridlist[i].afterProj=="공정창고(MCT)"){
					gridlist[i].afterProj="0020"
				}else if(gridlist[i].afterProj=="공정창고(CNC)"){
					gridlist[i].afterProj="0030"
				}else if(gridlist[i].afterProj=="완성창고"){
					gridlist[i].afterProj="0090"
				}
			}
			if(gridlist[i].afterProj=="0090" && gridlist[i].prdNo.indexOf("RW")!=-1){
				alert("완성창고일땐 소재품번일수 없습니다.")
				return false;
			}
			if(gridlist[i].afterProj=="0000" && (gridlist[i].prdNo.indexOf("RW")==-1)){
				alert("소재창고일땐 완성품번일수 없습니다.")
				return false;
			}
			
			if(gridlist[i].trans==undefined){
				alert("이동유형을 선택해주세요");
				return false;
			}else if(gridlist[i].sendCnt==undefined){
				alert("이동수량을 입력해주세요");
				return false;
			}else if(gridlist[i].afterProj=="--선택--"){
				alert("이동창고를 선택해주세요");
				return false;
			}else if((gridlist[i].proj=="0005" || gridlist[i].proj=="0080") && gridlist[i].cnt<gridlist[i].sendCnt){
				alert("재고수량을 다시 확인해주세요")
				return false;
			}else if(gridlist[i].afterProj=="0000" && gridlist[i].lotNo==""){
				alert("로트번호를 입력해주세요")
				return false;
			}else if(gridlist[i].afterProj=="0090" && gridlist[i].lotNo==""){
				alert("로트(출하)번호를 입력해주세요")
				return false;
			}else if(gridlist[i].sendCnt==0){
				alert("0개의 수량을 보낼수 없습니다");
				return false;
			}
			
			prdNochk=0;
			for(j=0,len=itemlist.length; j<len; j++){
				console.log(i,j)
				if(gridlist[i].prdNo==itemlist[j].prdNo){
					prdNochk++
				}
				
				if(gridlist[i].prdNo==itemlist[j].prdNo){
					if(itemlist[j].RWMATNO==""){
						gridlist[i].item="";
						gridlist[i].prce=itemlist[j].prce
						break;
					}else{
						gridlist[i].item=itemlist[j].prdNo;
						gridlist[i].prdNo=itemlist[j].RWMATNO;
						gridlist[i].prce=itemlist[j].prce
						break;
					}
				}
			}
			if(prdNochk==0){
				alert("품번을 제대로 입력해주세요");
				return false;
			}
			
			savelist.push(gridlist[i])
		}

		if(savelist.length==0){
			alert("저장할 데이터를 추가해주세요")
			return false;
		}
		
		var obj = new Object();
		obj.val = savelist;
		
		var param = JSON.stringify(obj);
		
		console.log(param)
		
		var url = "${ctxPath}/chart/stockMoveTrans.do"
		$.showLoading()
		$.ajax({
			url : url,
			data :"val=" + param,
			type : "post",
			dataType : "text",
			success : function(data){
				if(data=="success"){
					kendo.alert(savelist.length + "개의 List 저장 <br>\n");
					getTable();
				}else{
					kendo.alert("저장에 실패했습니다");
				}
				$.hideLoading();
//				getTable();
			}
		}) 
		
	}
	
	var projlist=[]
	var arr={}
	arr.text="자재창고";arr.value="0000";
	projlist.push(arr)
	var arr={}
	arr.text="공정창고(R)";arr.value="0010";
	projlist.push(arr)
	var arr={}
	arr.text="공정창고(MCT)";arr.value="0020";
	projlist.push(arr)
	var arr={}
	arr.text="공정창고(CNC)";arr.value="0030";
	projlist.push(arr)
	var arr={}
	arr.text="완성창고";arr.value="0090";
	projlist.push(arr)
	
	function readOnly(container, options){
		if(options.model.trans=="재사용"){
			$('<input name="' + options.field + '" />')
			 .appendTo(container)
			 .kendoDropDownList({
				 valuePrimitive: true,
				 autoWidth: true,
				 height: 3300,
				 dataTextField : "text",
				 dataValueField  : "text",
				 dataSource: projlist
			 }).data("kendoDropDownList");
		}else{
			container.removeClass("k-edit-cell");
	        container.text(options.model.get(options.field));
		}
	}
	
	function readOnly1(container, options){
		container.removeClass("k-edit-cell");
        container.text(options.model.get(options.field));
	}
	
	var kendotable
	var grid;
	var prdNos=[];
	var itemlist;
	$(document).ready(function(){
		kendotable = $("#grid").kendoGrid({
			editable:true
			,height: getElSize(1610)
			
			,columns:[{
				title : "${Movetype} *"
				,field : "trans"
				,editor : checkMstmat
				,attributes: {
				      "class": "table-cell",
				      style: "text-align: center; font-size:" + getElSize(45)
				 },headerAttributes: {
        				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
				 }	
			},{
				title : "${prd_no} *"
				,field: "prdNo"
				,editor : prdNoauto
				,attributes: {
				      "class": "table-cell",
				      style: "text-align: center; font-size:" + getElSize(45)
				 },headerAttributes: {
        				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
				 }	
			},{
				title : "${Currentwarehouse}"
				,field : "proj"
				,template : "#=projView(proj)#"
				,editor:readOnly1
				,attributes: {
				      style: "text-align: center; font-size:" + getElSize(45)
				 },headerAttributes: {
        				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
				 }	
			},{
				title : "${Movingwarehouse}"
				,field : "afterProj"
				,editor:readOnly
				,template : "#=projView(afterProj)#"
				,attributes: {
					  style: "text-align: center; font-size:" + getElSize(45)
				},headerAttributes: {
    				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
				 }	
			},{
				title : "${stock_cnt}"
				,field : "cnt"
				,editor:readOnly
				,attributes: {
					  style: "text-align: center; font-size:" + getElSize(45)
				},headerAttributes: {
    				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
				 }	
			},{
				title : "${move_cnt} *"
				,field : "sendCnt"
				,attributes: {
				      "class": "table-cell",
				      style: "text-align: center; font-size:" + getElSize(45)
				 },headerAttributes: {
        				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
				 }	
			},{
				title : "이동사유"
				,field : "cause"
			},{
				title : "lotNo"
				,field : "lotNo"
				,attributes: {
				      "class": "table-cell",
				      style: "text-align: center; font-size:" + getElSize(45)
				 },headerAttributes: {
        				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
				 }	
				
			},{
				
				command :[{
					text :"${del}",
					name :"destroy"
				}],
				attributes: {
    				style: "text-align: center; color:white; font-size:" + getElSize(35)
    			},headerAttributes: {
    				style: "text-align: center; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
				 }	
			}]
		}).data("kendoGrid")
		
		getTable()
	})
	
	function removeRow(e,row){
		console.log(e)
		console.log(row)
		grid.removeRow(row)
		grid.removeRow(this)
	}
	
	function getTable(){
		
		kendodata = new kendo.data.DataSource({
			batch: true,
			scrollable:true,
/* 			group: { field: "prdNo" },
			sort: [{
            	field: "prdNo" , dir:"asc" 
            },{
            	field: "item" , dir:"asc"
            }], */
			height: 500,
			schema: {
				model: {
					id: "id",
					fields: {
						insertdata : {editable : true}
						,checker : {editable : false}
						,prdNo : {editable : true}
						,cnt : {editable : true}
						,item : {editable : true}
						,proj : {editable : true}
						,date : {editable : false}
						,stock : {editable : false}
						,sendCnt : {editable : true,type: 'number'}
						/* prdNo: { editable: false },
						proj: { editable: false },
						vndNo: {editable: true,
							field : 'vndNo',
							defaultValue: function(e){
								return "0001"
							}
						},
						item: { editable: true,
							field : "item",
							defaultValue: "FS_RR_LH"
						},
						cnt: { editable: false },
						sendCnt: { editable: true ,type: 'number'},
						checker : { editable : false},
						action : { editable : false} */
					}
				}
			}
		}); 
		kendotable.setDataSource(kendodata);
		
		grid = $("#grid").data("kendoGrid");
		grid.hideColumn("cause");
		$.hideLoading()
	}
	
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			
			<Tr>
				
				<td rowspan="10" id="svg_td" style="vertical-align: top; width: 100%; height: 100%; position:relative; background:#222327;">
					<table id="content_table" style="width: 100%">
					<tr>
						<td id="td_first">
							<button onclick="insertRow()"><spring:message code="add"></spring:message></button><button onclick="saveRow()"><spring:message code="save"></spring:message></button>
						<td>
					</tr>
					
					<tr>
						<td>
							<div id="grid" style="background: gray">
							</div>
						<td>
					<tr>
					</table>
				</td>
			</Tr>
		
		</table>
	 </div>
	
</body>
</html>	