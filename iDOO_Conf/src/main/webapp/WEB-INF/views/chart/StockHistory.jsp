<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>

<script src="http://kendo.cdn.telerik.com/2018.2.516/js/jszip.min.js"></script>
<link rel="stylesheet" href="${ctxPath }/resources/styles/kendo.common.min.css" />
<link rel="stylesheet" href="${ctxPath }/resources/styles/kendo.default.min.css" />
<link rel="stylesheet" href="${ctxPath }/resources/styles/kendo.default.mobile.min.css" />
<script src="${ctxPath }/resources/js/kendo.all.min.js"></script>
<script type="text/javascript" src="http://jsgetip.appspot.com"></script>

<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden; 
	background-color: black;
  	font-family:'Helvetica';
}
html{
	overflow : hidden;
}
span.k-link{
	color:black !important;
}

.k-grid-footer-wrap tbody tr td{
	color : black;
}
#downLoad:hover{
	background : #CB6CFF !important;
}
</style> 
<script type="text/javascript">

	const loadPage = () =>{
		createMenuTree("tm", "toolLifeManager")	
	}
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

//	var handle = 0;
	
	$(function(){
		$("#sDate").val(getToday().substring(0,10));
		$("#eDate").val(getToday().substring(0,10));
		
//		createNav("inven_nav", 7);
		
//		setEl();
//		time();

		setEl2();
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		$( "#sDate" ).datepicker({
		    onSelect : function(e){
		    	//e == 날짜 
		    	$("#sDate").val(e);
		    }
	    })
	    $( "#eDate" ).datepicker({
		    onSelect : function(e){
		    	//e == 날짜 
		    	$("#eDate").val(e);
		    }
	    })
		
		chkBanner();
	});
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
/* 	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
 */	
 
 
	/* function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$("#iframe").css({
			"width" : getElSize(3500),
			"height" : getElSize(1800),
			"position" : "absolute",
			"z-index" : 999,
			"display" : "none"
		});
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : contentWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#banner").css({
			"font-size" : getElSize(80),
			"width" : getElSize(2000)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		$("#tabstrip").css("height",getElSize(1656))
		$("#tabstrip").css("height",getElSize(1656))
		
		$("#downLoad").css({
			"background" : "rgb(250,235,255)",
			"padding" : getElSize(10),
			"color" : "black",
			"font-size" : getElSize(45),
			"margin-left" : getElSize(1000),
			"cursor":"pointer"
		})
		
	}; */
	
	
	function setEl2(){
		var width = window.innerWidth;
		var height = window.innerHeight;
	/* 	
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		 */
		 
			$("#container").css({
				"width" : contentWidth - getElSize(30)
			})
				
			$("#content_table").css({
				"margin-top" : getElSize(300)
			})
				
				
			$("#table").css({
				"position" : "absolute",
				"width" : $("#container").width(),
				"top" : getElSize(100) + marginHeight,
				"margin-top" : getElSize(250)
			});
		 
		 
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#banner").css({
			"font-size" : getElSize(80),
			"width" : getElSize(2000)
		});
		
		$("#tabstrip").css("height",getElSize(1656))
		$("#tabstrip").css("height",getElSize(1656))
		
		$("#downLoad").css({
			"background" : "rgb(250,235,255)",
			"padding" : getElSize(10),
			"color" : "black",
			"font-size" : getElSize(45),
			"margin-left" : getElSize(250),
			"margin-right" : getElSize(250),
			"cursor" : "pointer",
			"text-align" : "center"
		})
		
	};
	
	
	
	
/* 	function time(){
		$("#time").html(getToday());
//		 handle = requestAnimationFrame(time)
	}; */
	
	function numberWithCommas(x) {
		if(x==undefined){
			return 0
		}
	    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
	function totalFooter(sum){
		if(sum!=undefined){
			var prceSum=numberWithCommas(sum.sum)
			return "총금액 : " + prceSum 
		}else{
			return "총금액 : 0"
		}
	}
	
	function cntFooter(sum){
		if(sum!=undefined){
			var prceSum=numberWithCommas(sum.sum)
			return "총수량 : " + prceSum 
		}else{
			return "총수량 : 0"
		}
	}
	
	// 납품서 재발행 
	function InStockPrint(){
		chk = 0;
		
		// 납품서 번호 확인하기
		for(i=0,len=inList.length;i<len;i++){
			console.log(inList[i])
			if(inList[i].id==$("#comboboxIn").val()){
				chk++;
			}
		}
		if(chk==0){
			alert("조회된 납품서 번호가 없습니다. \n날짜 및 납품서 번호를 확인해주세요")
			return;
		}
		
		//프린터를 위한 데이터 담기
		savelist=[]
		for(i=0,len=gridIn.dataSource.data().length; i<len; i++){
			if($("#comboboxIn").val()==gridIn.dataSource.data()[i].deliveryNo){
				var arr={};
				arr.deliveryNo = gridIn.dataSource.data()[i].deliveryNo;
				// 납품회사
				arr.deliverer = encodeURIComponent(gridIn.dataSource.data()[i].vndNm);
				// 바코드 
				arr.barcode = gridIn.dataSource.data()[i].barcode;
				// 로트번호
				arr.lotNo = gridIn.dataSource.data()[i].lotNo;
				// 납품 날짜
				arr.deliverDate = gridIn.dataSource.data()[i].date.substr(0,10);
//				arr.prdNo = gridIn.dataSource.data()[i].RWMATNO
				// 차종
				arr.prdNo = gridIn.dataSource.data()[i].prdNo
				//품번
				arr.ITEMNO = ""
				//스팩
				arr.spec = "SPEC"
				//단위
				arr.unit = "EA"
				//수량
				arr.cnt = gridIn.dataSource.data()[i].cnt;
				//단가
				arr.prc = gridIn.dataSource.data()[i].prce;
				//금액
				arr.totalPrc = Number(gridIn.dataSource.data()[i].cnt) * Number(gridIn.dataSource.data()[i].prce)
				
//				arr.item = gridIn.dataSource.data()[i].prdNo
//				arr.ITEMNO = gridIn.dataSource.data()[i].ITEMNO
//				arr.afterProj = gridIn.dataSource.data()[i].afterProj
//				arr.sendCnt = gridIn.dataSource.data()[i].cnt
//				arr.remarks = encodeURI(decode(gridIn.dataSource.data()[i].remarks))
								
				savelist.push(arr);
			}
		}
		
//		console.log(savelist)

		var url = "${ctxPath}/chart/createBarcode.do";
		$.ajax({
			url : url,
			data : "deliveryCd=" + $("#comboboxIn").val(),
			type :"post",
			dataType : "text",
			success :function(data){
				if(data == "success"){
					checkedIds = {};
							
					$("#iframe").attr("src", "${ctxPath}/chart/printInvoice.do?json=" + encodeURIComponent(JSON.stringify(savelist))).css({
							"display" : "inline",
							"background-color" : "white"
					});
							
					var close_btn = document.createElement("img");
					close_btn.setAttribute("src", "${ctxPath}/images/close_btn.png");
					close_btn.setAttribute("id", "close_btn");
							
					$("body").append(close_btn);						
													
					$("#close_btn").css({
							"width" : getElSize(100) + "px",
							"height" : getElSize(100) + "px",
							"position" : "absolute",
							"cursor" : "pointer",
							"top" : $("#iframe").offset().top,
							"left" : $("#iframe").offset().left + $("#iframe").width() - getElSize(100),
							"z-index" : 9999
					}).click(function(){
							$(this).remove();
							$("#print_btn").remove();
							$("#iframe").css("display" , "none")
					})
									
							
					var print_btn = document.createElement("img");
					print_btn.setAttribute("src", "${ctxPath}/images/printer.png");
					print_btn.setAttribute("id", "print_btn");
							
					$("body").append(print_btn);						
													
					$("#print_btn").css({
							"width" : getElSize(100) + "px",
							"height" : getElSize(100) + "px",
							"position" : "absolute",
							"cursor" : "pointer",
							"top" : $("#iframe").offset().top,
							"left" : $("#iframe").offset().left + $("#iframe").width() - getElSize(250),
							"z-index" : 9999
					}).click(function(){
							var frm = document.getElementById("iframe").contentWindow;
							frm.focus();// focus on contentWindow is needed on some ie versions
							frm.print();	
					})
							
//					getDeliveryHistory(); 
				}
			}
		});		



	}
	
	// 반출서 재발행 
	function exportPrint(){
		chk = 0;
		
		//반출서 번호 확인하기
		for(i=0,len=exportList.length;i<len;i++){
			console.log(exportList[i])
			if(exportList[i].id==$("#combobox").val() && exportList[i].plan=="반출"){
				chk++;
			}
		}
		if(chk==0){
			alert("조회된 반출번호가 없습니다. \n날짜 및 반출번호를 확인해주세요")
			return;
		}
		
		//프린터를 위한 데이터 담기
		savelist=[]
		for(i=0,len=gridOuter.dataSource.data().length; i<len; i++){
			if($("#combobox").val()==gridOuter.dataSource.data()[i].deliveryNo && gridOuter.dataSource.data()[i].plan=="반출"){
				var arr={};
				arr.deliveryNo = gridOuter.dataSource.data()[i].deliveryNo;
				arr.vndNm = encodeURIComponent(gridOuter.dataSource.data()[i].vndNm);
				arr.barcode = gridOuter.dataSource.data()[i].barcode;
				arr.lotNo = gridOuter.dataSource.data()[i].barcode;
				arr.date = gridOuter.dataSource.data()[i].date.substr(0,10);
				arr.prdNo = gridOuter.dataSource.data()[i].RWMATNO
				arr.item = gridOuter.dataSource.data()[i].prdNo
				arr.ITEMNO = gridOuter.dataSource.data()[i].ITEMNO
				arr.afterProj = gridOuter.dataSource.data()[i].afterProj
				arr.sendCnt = gridOuter.dataSource.data()[i].cnt
				arr.remarks = encodeURI(decode(gridOuter.dataSource.data()[i].remarks))
				savelist.push(arr);
			}
		}
		
		console.log(savelist)
		var url = "${ctxPath}/chart/createBarcode.do";
		$.ajax({
			url : url,
			data : "deliveryCd=" + $("#combobox").val(),
			type :"post",
			dataType : "text",
			success :function(data){
				if(data == "success"){
					checkedIds = {};
					console.log(savelist)
					console.log(JSON.stringify(savelist))
					$("#iframe").attr("src", "${ctxPath}/chart/printExport.do?json=" + encodeURIComponent(JSON.stringify(savelist))).css({
						"display" : "inline",
						"background-color" : "white"
					});
					
					var close_btn = document.createElement("img");
					close_btn.setAttribute("src", "${ctxPath}/images/close_btn.png");
					close_btn.setAttribute("id", "close_btn");
					
					$("body").append(close_btn);						
											
					$("#close_btn").css({
						"width" : getElSize(100) + "px",
						"height" : getElSize(100) + "px",
						"position" : "absolute",
						"cursor" : "pointer",
						"top" : $("#iframe").offset().top,
						"left" : $("#iframe").offset().left + $("#iframe").width() - getElSize(100),
						"z-index" : 9999
					}).click(function(){
						$(this).remove();
						$("#print_btn").remove();
						$("#iframe").css("display" , "none")
					})
							
					
					var print_btn = document.createElement("img");
					print_btn.setAttribute("src", "${ctxPath}/images/printer.png");
					print_btn.setAttribute("id", "print_btn");
					
					$("body").append(print_btn);						
											
					$("#print_btn").css({
						"width" : getElSize(100) + "px",
						"height" : getElSize(100) + "px",
						"position" : "absolute",
						"cursor" : "pointer",
						"top" : $("#iframe").offset().top,
						"left" : $("#iframe").offset().left + $("#iframe").width() - getElSize(250),
						"z-index" : 9999
					}).click(function(){
						var frm = document.getElementById("iframe").contentWindow;
						frm.focus();// focus on contentWindow is needed on some ie versions
						frm.print();	
					})
				}else{
					alert("실패")
				}
			},error : function(){
				alert("에러")
			}
		});
	}
	
	function deliveryNoSearch(event){
		if(event.keyCode == 13){
			getTable()
		}
	}	

	var downLoadGrid;
	function onSelect(e){
		var text=$(e.item).find("> .k-link").text();
		text = text.trim();
		
		if(text=="입고 현황 조회"){
			downLoadGrid = $("#gridIn").data("kendoGrid");
			console.log("입고")
			return;			
		}else if(text=="불출 현황 조회"){
			downLoadGrid = $("#gridOut").data("kendoGrid");
			console.log("불출")
			return;			
		}else if(text=="반　출입 현황 조회"){
			downLoadGrid = $("#gridOuter").data("kendoGrid");
			console.log("반출")
			return;			
		}else if(text=="출하 현황 조회"){
			downLoadGrid = $("#gridShip").data("kendoGrid");
			console.log("출하")
			return;			
		}else if(text=="재고 이동 현황 조회"){
			downLoadGrid = $("#gridMove").data("kendoGrid");
			console.log("이동현황")
			return;			
		}
	}
	var gridInlist
	var gridOutlist
	var gridOuterlist
	var gridShiplist
	var gridMovelist
	var inSum=0;
	$(document).ready(function(){
		
		$("#downLoad").click(function(e) {
			if(downLoadGrid==undefined || downLoadGrid=="undefined"){
				downLoadGrid = $("#gridIn").data("kendoGrid");
			}
			
			downLoadGrid.saveAsExcel();
			
			
//		    var grid = $("#gridIn").data("kendoGrid");
//		    grid.saveAsExcel();
		});
		
		 $("#tabstrip").kendoTabStrip({
			 select:onSelect
             ,animation:  {
                 open: {
                     effects: "fadeIn"
                 }
             }
         });
		 //입고 조회
		 gridInlist = $("#gridIn").kendoGrid({
			 toolbar : [{
				 template: "<input id='comboboxIn'>"
			 },{
				 template: "<a class='k-button' style='background:rgb(250,235,255)' onclick='InStockPrint()'>납품서 재발행</a>"
		     }],
			 filterable: {
			      mode: "row"
			 },
			 excel:{
		            fileName:"입고현황_" + getToday().substring(0,10) + ".xlsx"
	         },
			 dataBound : function(e) {
				$(".k-grid-content.k-auto-scrollable").css("height",getElSize(1050))
			 },
			 sortable: true,
			 columns:[{
				 title:"${com_name}"
				 ,field:"vndNm"
				 ,width:getElSize(500)
				 ,filterable: {
					 cell: {
	                        suggestionOperator: "contains"
	                 }
		         }
			 },{
				 title:"${prd_no}"
				 ,field:"prdNo"
				 ,width:getElSize(500)
				 ,filterable: {
					 cell: {
	                        suggestionOperator: "contains"
	                 }
		         }
			 },{
				 title:"${Movetype}"
				 ,field:"plan"
				 ,width:getElSize(375)
				 ,filterable: {
	                    cell: {
	                    	suggestionOperator: "contains"
	                    }
	             }
			 },{
				 title:"${Delivery_number}"
				 ,field:"deliveryNo"
				 ,width:getElSize(500)
				 ,filterable: {
					 cell: {
	                        suggestionOperator: "contains"
	                 }
		         }
			 },{
				 title:"lotNo"
				 ,field:"lotNo"
				 ,width:getElSize(400)
				 ,filterable: {
					 cell: {
	                        suggestionOperator: "contains"
	                 }
		         }
			 },{
				 title:"${Quantity}"
				 ,field:"cnt"
				 ,width:getElSize(205)
				 ,filterable: {
	                    cell: {
	                        enabled: false
	                    }
	             }
			 },{
				 title:"${unit_price}"
				 ,field:"prce"
				 ,template:"#=numberWithCommas(prce)#"
				 ,width:getElSize(205)
				 ,filterable: {
	                    cell: {
	                        enabled: false
	                    }
	             }
			 },{
				 title:"${reg_time}"
				 ,field:"date"
				 ,footerTemplate: "#=cntFooter(data.cnt)#"
//				 ,footerTemplate: "#=totalFooter(data.sumIn)#"
				 ,width:getElSize(455)
				 ,filterable: {
	                    cell: {
	                        enabled: false
	                    }
	             }
			 }]
		 }).data("kendoGrid")

		 //불출 조회
		 gridOutlist = $("#gridOut").kendoGrid({
			 filterable: {
			      mode: "row"
			 },
			 excel:{
		            fileName:"불출현황_" + getToday().substring(0,10) + ".xlsx"
	         },
			 sortable: true,
			 dataBound : function (e){
					$(".k-grid-content.k-auto-scrollable").css("height",getElSize(1100))
			 },
//			 height:getElSize(1330),
			 columns:[{
				 title:"품번"
				 ,field:"prdNo"
				 ,width:getElSize(500)
				 ,filterable: {
					 cell: {
	                        suggestionOperator: "contains"
	                 }
		         }
			 },{
				 title:"유형"
				 ,field:"plan"
				 ,width:getElSize(250)
				 ,filterable: {
	                    cell: {
	                        enabled: false
	                    }
	             }
			 },{
				 title:"로트번호"
				 ,field:"lotNo"
				 ,width:getElSize(500)
				 ,filterable: {
					 cell: {
	                        suggestionOperator: "contains"
	                 }
		         }
			 },{
				 title:"바코드"
				 ,field:"barcode"
				 ,width:getElSize(250)
				 ,filterable: {
	                    cell: {
	                        enabled: false
	                    }
	             }
			 },{
				 title:"수량"
				 ,field:"cnt"
				 ,width:getElSize(250)
				 ,filterable: {
	                    cell: {
	                        enabled: false
	                    }
	             }
			 },{
				 title:"가격"
				 ,field:"prce"
				 ,template:"#=numberWithCommas(prce)#"
				 ,width:getElSize(250)
				 ,filterable: {
	                    cell: {
	                        enabled: false
	                    }
	             }
			 },{
				 title:"날짜"
				 ,field:"date"
				 ,width:getElSize(500)
				 ,footerTemplate: "#=cntFooter(data.cnt)#"
				 ,filterable: {
	                    cell: {
	                        enabled: false
	                    }
	             }
			 }]
		 }).data("kendoGrid")

		 //반출입 조회
		 gridOuterlist = $("#gridOuter").kendoGrid({
			 toolbar : [{
				 template: "<input id='combobox'>"
			 },{
				 template: "<a class='k-button' style='background:rgb(250,235,255)' onclick='exportPrint()'>반출서 재발행</a>"
		     }],
		     excel:{
		            fileName:"반출입현황" + getToday().substring(0,10) + ".xlsx"
	         },
			 filterable: {
			      mode: "row"
			 },
			 sortable: true,
			 dataBound : function (e){
					$(".k-grid-content.k-auto-scrollable").css("height",getElSize(1100))
			 },
//			 height:getElSize(1330),
			 columns:[{
				 title:"회사명"
				 ,field:"vndNm"
				 ,width:getElSize(500)
				 ,filterable: {
					 cell: {
	                        suggestionOperator: "contains"
	                 }
		         }
			 },{
				 title:"품번"
				 ,field:"prdNo"
				 ,width:getElSize(500)
				 ,filterable: {
					 cell: {
	                        suggestionOperator: "contains"
	                 }
		         }
			 },{
				 title:"반출 번호"
				 ,field:"deliveryNo"
				 ,width:getElSize(500)
				 ,filterable: {
					 cell: {
	                        suggestionOperator: "contains"
	                 }
		         }
			 },{
				 title:"유형"
				 ,field:"plan"
				 ,width:getElSize(420)
				 ,filterable: {
					 cell: {
	                        suggestionOperator: "contains"
	                 }
		         }
			 },{
				 title:"공정"
				 ,field:"proj"
				 ,width:getElSize(490)
				 ,filterable: {
					 cell: {
	                        suggestionOperator: "contains"
	                 }
		         }
			 },{
				 title:"수량"
				 ,field:"cnt"
				 ,width:getElSize(230)
				 ,filterable: {
	                    cell: {
	                        enabled: false
	                    }
	             }
			 },{
				 title:"날짜"
				 ,field:"date"
				 ,width:getElSize(490)
				 ,footerTemplate: "#=cntFooter(data.cnt)#"
				 ,filterable: {
	                    cell: {
	                        enabled: false
	                    }
	             }
			 }]
		 }).data("kendoGrid")
		 
		  //출하 조회
		 gridShiplist = $("#gridShip").kendoGrid({
			 filterable: {
			      mode: "row"
			 },
			 excel:{
		            fileName:"출하현황" + getToday().substring(0,10) + ".xlsx"
	         },
			 sortable: true,
			 dataBound : function (e){
					$(".k-grid-content.k-auto-scrollable").css("height",getElSize(1050))
			 },
//			 height:getElSize(1330),
			 columns:[{
				 title:"품번"
				 ,field:"prdNo"
				 ,width:getElSize(500)
				 ,filterable: {
					 cell: {
	                        suggestionOperator: "contains"
	                 }
		         }
			 },{
				 title:"유형"
				 ,field:"plan"
				 ,width:getElSize(250)
				 ,filterable: {
	                    cell: {
	                        enabled: false
	                    }
	             }
			 },{
				 title:"출하번호"
				 ,field:"shipLot"
				 ,width:getElSize(500)
				 ,filterable: {
					 cell: {
	                        suggestionOperator: "contains"
	                 }
		         }
			 },{
				 title:"수량"
				 ,field:"cnt"
				 ,width:getElSize(250)
				 ,filterable: {
	                    cell: {
	                        enabled: false
	                    }
	             }
			 },{
				 title:"가격"
				 ,field:"prce"
				 ,footerTemplate: "#=cntFooter(data.cnt)#"
//				 ,template:"#=numberWithCommas(prce)#"
				 ,width:getElSize(250)
				 ,filterable: {
	                    cell: {
	                        enabled: false
	                    }
	             }
			 },{
				 title:"날짜"
				 ,field:"date"
				 ,width:getElSize(500)
// 				 ,footerTemplate: "#=totalFooter(data.prce)#"
				 ,filterable: {
	                    cell: {
	                        enabled: false
	                    }
	             }
			 }]
		 }).data("kendoGrid")
		 
		  //재고 이동 조회
		 gridMovelist = $("#gridMove").kendoGrid({
			 filterable: {
			      mode: "row"
			 },
			 excel:{
		            fileName:"재고이동현황" + getToday().substring(0,10) + ".xlsx"
	         },
			 dataBound : function (e){
					$(".k-grid-content.k-auto-scrollable").css("height",getElSize(1100))
			 },
//			 height:getElSize(1330),
			 sortable: true,
			 columns:[{
				 title:"품번"
				 ,field:"prdNo"
				 ,width:getElSize(500)
				 ,filterable: {
					 cell: {
	                        suggestionOperator: "contains"
	                 }
		         }
			 },{
				 title:"유형"
				 ,field:"plan"
				 ,width:getElSize(500)
				  ,filterable: {
					 cell: {
	                        suggestionOperator: "contains"
	                 }
		         }
			 },{
				 title:"로트(출하)번호"
				 ,field:"shipLot"
				 ,width:getElSize(500)
				 ,filterable: {
					 cell: {
	                        suggestionOperator: "contains"
	                 }
		         }
			 },{
				 title:"수량"
				 ,field:"cnt"
				 ,width:getElSize(250)
				 ,filterable: {
	                    cell: {
	                        enabled: false
	                    }
	             }
			 },{
				 title:"가격"
				 ,field:"prce"
				 ,template:"#=numberWithCommas(prce)#"
				 ,width:getElSize(250)
				 ,filterable: {
	                    cell: {
	                        enabled: false
	                    }
	             }
			 },{
				 title:"날짜"
				 ,field:"date"
				 ,width:getElSize(500)
				 ,footerTemplate: "#=cntFooter(data.cnt)#"
				 ,filterable: {
	                    cell: {
	                        enabled: false
	                    }
	             }
			 }]
		 }).data("kendoGrid")
		 
		 $("#combobox").kendoComboBox({
			dataSource: exportList,
			dataTextField: "id",
			dataValueField: "id"
		 });

		 $("#comboboxIn").kendoComboBox({
			dataSource: exportList,
			dataTextField: "id",
			dataValueField: "id"
		 });
		 
		 getTable();
		
	})
	var exportList=[];
	var inList=[];
	function getTable(){
		var url = "${ctxPath}/chart/getInhistory.do";
		var param  = "sDate=" + $("#sDate").val() + 
						"&eDate=" + $("#eDate").val() +
						"&deliveryNo=" + $("#deliveryNo").val();
	
	//		$("#checkall")[0].checked=false;
		var json;
		console.log(param)
		$.showLoading()
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				console.log(data)
				json = data.dataList;//입고리스트
				jsonOut = data.dataListOut;//불출리스트
				jsonOuter = data.dataListOuter;//불출리스트
				jsonShip = data.dataListShip;//출하리스트
				jsonMove = data.dataListMove;//출하리스트
				
				exportList=[]
				inList=[]
				//입고
				inSum = 0;
				$(json).each(function(idx,data){
					data.sumIn = data.cnt * data.prce;
					data.vndNm=decode(data.vndNm)
					data.plan=decode(data.plan)
					
					chk = 0;
					for(i=0,len=inList.length;i<len;i++){
						if(inList[i].id==data.deliveryNo && data.deliveryNo!=""){
							chk++
						}
					}
					if(chk==0 && data.deliveryNo!=""){
						var arr={id : data.deliveryNo, plan : data.plan}
						inList.push(arr)
					}
				})				

				console.log(inSum)
				$("#inSum").html(inSum)
				
				inlistdata = new kendo.data.DataSource({
					data: json,
					aggregate: [
				          { field: "prce", aggregate: "sum" }
				          ,{ field: "sumIn", aggregate: "sum" }
				          ,{ field: "cnt", aggregate: "sum" }
			        ],
				});
				
				//불출
				$(jsonOut).each(function(idx,data){
					data.plan=decode(data.plan)
				})
				outlistdata = new kendo.data.DataSource({
					data: jsonOut,
					aggregate: [
				          { field: "prce", aggregate: "sum" },
				          { field: "cnt", aggregate: "sum" }
			        ],
				}); 

				//반출입
				$(jsonOuter).each(function(idx,data){
					data.vndNm=decode(data.vndNm)
					data.plan=decode(data.plan)
					
					chk = 0;
					for(i=0,len=exportList.length;i<len;i++){
						if(exportList[i].id==data.deliveryNo && data.deliveryNo!="" && data.plan=="반출" && exportList[i].plan=="반출"){
							chk++
						}
					}
					if(chk==0 && data.deliveryNo!="" && data.plan=="반출"){
						var arr={id : data.deliveryNo, plan : data.plan}
						exportList.push(arr)
					}
					if(data.plan=="반출"){
						if(data.proj=="0030"){
							data.proj="CNC삭(외주)"
						}else if(data.proj=="0040"){
							data.proj="도금(외주)"
						}
					}else if(data.plan=="반입"){
						if(data.proj=="0030"){
							data.proj="CNC삭"
						}else if(data.proj=="0040"){
							data.proj="도금"
						}else if(data.proj=="0090"){
							data.proj="도금"
						}
					}
				})
				outerlistdata = new kendo.data.DataSource({
					data: jsonOuter,
					aggregate: [
				          { field: "prce", aggregate: "sum" },
				          { field: "cnt", aggregate: "sum" }
			        ],
				}); 
				
				//출하
				$(jsonShip).each(function(idx,data){
					data.plan=decode(data.plan)
				})
				shiplistdata = new kendo.data.DataSource({
					data: jsonShip,
					aggregate: [
				          { field: "prce", aggregate: "sum" },
				          { field: "cnt", aggregate: "sum" }
			        ],
				}); 

				//재고이동
				$(jsonMove).each(function(idx,data){
					data.plan=decode(data.plan)
				})
				movelistdata = new kendo.data.DataSource({
					data: jsonMove,
				}); 
				
				gridInlist.setDataSource(inlistdata);
				gridOutlist.setDataSource(outlistdata);
				gridOuterlist.setDataSource(outerlistdata);
				gridShiplist.setDataSource(shiplistdata);
				gridMovelist.setDataSource(movelistdata);
				
				gridIn = $("#gridIn").data("kendoGrid");
				gridOut = $("#gridOut").data("kendoGrid");
				gridOuter = $("#gridOuter").data("kendoGrid");
				gridShip = $("#gridShip").data("kendoGrid");
				gridMove = $("#gridMove").data("kendoGrid");
				
/* 				$("#combobox").kendoComboBox({
					dataSource: exportList,
					dataTextField: "id",
					dataValueField: "id"
				}); */
				
				var comboList = new kendo.data.DataSource({
				  data: exportList
				});
				var comboInList = new kendo.data.DataSource({
				  data: inList
				});
				var combobox = $("#combobox").data("kendoComboBox");
				combobox.setDataSource(comboList);

				var comboboxIn = $("#comboboxIn").data("kendoComboBox");
				comboboxIn.setDataSource(comboInList);
				
				$("#gridIn thead tr th").css("font-size",getElSize(39))
				$("#gridIn tbody tr td").css("font-size",getElSize(39))

				$("#gridOut thead tr th").css("font-size",getElSize(39))
				$("#gridOut tbody tr td").css("font-size",getElSize(39))
				
				$("#gridOuter thead tr th").css("font-size",getElSize(39))
				$("#gridOuter tbody tr td").css("font-size",getElSize(39))
				
				$("#gridShip thead tr th").css("font-size",getElSize(39))
				$("#gridShip tbody tr td").css("font-size",getElSize(39))

				$("#gridMove thead tr th").css("font-size",getElSize(39))
				$("#gridMove tbody tr td").css("font-size",getElSize(39))
				
				$(".k-grid-content.k-auto-scrollable").css("height",getElSize(1050))
				
/* 				$("#gridIn").css("height",getElSize(1500))
				$("#gridOut").css("height",getElSize(1500))
				$("#gridOuter").css("height",getElSize(1500))
				$("#gridShip").css("height",getElSize(1500))
				$("#gridMove").css("height",getElSize(1500))
 */				$.hideLoading()
			}
		})
	}
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	
	<div id="container">
		<table id="table" style="border-collapse: collapse;">
						
			<Tr>
				
				<td rowspan="10" id="svg_td" style="vertical-align: top; position:relative; background:#26282c; ">
					<div id="wrapper">
						<table style="width: 100%">
							<Tr>
								<Td style="text-align: left; vertical-align: middle;">
									날짜 : <input type='text' id='sDate' class="date" readonly="readonly"> ~ <input type="text" id='eDate' class="date" readonly="readonly"> 납품번호 : <input type="text" id="deliveryNo" onkeyup="deliveryNoSearch(event)"> <button onclick="getTable()"><spring:message code="check1"></spring:message></button>
									<div id="tabstrip">
										<ul>
			                                <li class="k-state-active">
			                                    <spring:message code="income_history"></spring:message>
			                                </li>
			                                <li>
			                                    <spring:message code="release_history"></spring:message>
			                                </li>
			                                <li>
			                                    <spring:message code="Display_status"></spring:message>
			                                </li>
			                                <li>
			                                    <spring:message code="ship_history"></spring:message>
			                                </li>
			                                <li>
			                                    <spring:message code="inventorymovementstatus"></spring:message>
			                                </li>
			                                <span id="downLoad">Excel DownLoad</span>
			                            </ul>
			                            <div id="gridIn">
			                            </div>
			                            <div id="gridOut">
			                            </div>
			                            <div id="gridOuter">
			                            </div>
			                            <div id="gridShip">
			                            </div>
			                            <div id="gridMove">
			                            </div>
									</div>
								</Td>
							</Tr>
						</table>
					</div>
				</td>
			</Tr>
			
		</table>
	 </div>
</body>
</html>	