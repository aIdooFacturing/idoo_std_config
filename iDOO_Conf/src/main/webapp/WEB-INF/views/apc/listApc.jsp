<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<thead>
	<tr>
		<th>NO</th>
		<th>설비명</th>
		<th>설비상태</th>
		<th>프로그램명</th>
		<th>시작시간</th>
		<th>종료시간</th>
		<th>프로그램종류</th>
		<th>작업기준일</th>
	</tr>
</thead>

<tfoot>
	<tr>
		<th>NO</th>
		<th>설비명</th>
		<th>설비상태</th>
		<th>프로그램명</th>
		<th>시작시간</th>
		<th>종료시간</th>
		<th>프로그램종류</th>
		<th>작업기준일</th>
	</tr>
</tfoot>

<tbody>
<c:choose>
	<c:when test="${!empty listApc }">
		<c:forEach var="apc" items="${listApc}" varStatus="status">
			<tr>
				<td>${status.count}</td>
				<td>${apc.dvcName }</td>
				<td>${apc.chartStatus }</td>
				<td>${apc.prgmName}</td>
				<td>${apc.startDateTime}</td>
				<td>${apc.endDateTime}</td>
				<td>${apc.prgmType }</td>
				<td>${apc.date }</td>
			</tr>			
		</c:forEach>
	</c:when>
		
	<c:otherwise>
	</c:otherwise>
</c:choose>
</tbody>